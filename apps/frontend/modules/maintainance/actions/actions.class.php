<?php

/**
maintainance stuff
 */
class maintainanceActions extends sfActions
{
  public function executeCalcPurchaseReceivedStatus(sfWebRequest $request)
  {
    $itemclass="Purchase";
    
    $lastitem = Doctrine_Query::create()
      ->from($itemclass.' i')
      ->orderBy('i.id desc')
      ->limit(1)
      ->fetchOne();
  
    if(!$lastitem)
    {
  	  return $this->redirect("home/error?msg=No items exist to process");
    }
  
      //default values
    $this->interval=100;
    $this->start=$lastitem->getId();

    if($request->getMethod()!="POST")
       return;
       
    if(isset($_REQUEST["interval"]))
      $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
      $this->start=$request->getParameter("start");
    $this->end=$this->start-$this->interval+1;
    
    $this->items = Doctrine_Query::create()
      ->from($itemclass.' i')
      ->where('i.id >='.$this->end)
      ->andWhere('i.id <='.$this->start)
      ->orderBy('i.id desc')
      ->execute();

    //custom processing here
    foreach ($this->items as $item) {
      $details=$item->getPurchasedetail();
      
      $item->calcReceivedStatus();
      $item->save(); 
    }

    $this->start=$this->end-1;
  }
  public function executeSetColorToPurchaseDetail(sfWebRequest $request)
  {
    $itemclass="Product";
    
    $lastitem = Doctrine_Query::create()
      ->from($itemclass.' i')
      ->orderBy('i.id desc')
      ->limit(1)
      ->fetchOne();
  
    if(!$lastitem)
    {
  	  return $this->redirect("home/error?msg=No items exist to process");
    }
  
      //default values
    $this->interval=10;
    $this->start=$lastitem->getId();

    if($request->getMethod()!="POST")
       return;
       
    if(isset($_REQUEST["interval"]))
      $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
      $this->start=$request->getParameter("start");
    $this->end=$this->start-$this->interval+1;
    
    $this->items = Doctrine_Query::create()
      ->from($itemclass.' i')
      ->where('i.id >='.$this->end)
      ->andWhere('i.id <='.$this->start)
      ->orderBy('i.id desc')
      ->execute();

    //custom processing here
    
    foreach ($this->items as $product) 
    {
      $color=0;
      foreach ($product->getPurchasedetail() as $detail) 
      {
        $color+=1;
        if($color>7)$color=1;
        $detail->setColor($color);
        $detail->save();
        foreach ($detail->getProfitdetail() as $profitdetail) 
        {
          $profitdetail->setColor($color);
          $profitdetail->save();
        }
      }
    }

    $this->start=$this->end-1;
  }
  public function executeReceiveAllPurchases(sfWebRequest $request)
  {
    $itemclass="Purchase";
    
    $lastitem = Doctrine_Query::create()
      ->from($itemclass.' i')
      ->orderBy('i.id desc')
      ->limit(1)
      ->fetchOne();
  
    if(!$lastitem)
    {
  	  return $this->redirect("home/error?msg=No items exist to process");
    }
  
      //default values
    $this->interval=10;
    $this->start=$lastitem->getId();

    if($request->getMethod()!="POST")
       return;
       
    if(isset($_REQUEST["interval"]))
      $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
      $this->start=$request->getParameter("start");
    $this->end=$this->start-$this->interval+1;
    
    $this->items = Doctrine_Query::create()
      ->from($itemclass.' i')
      ->where('i.id >='.$this->end)
      ->andWhere('i.id <='.$this->start)
      ->andWhere('i.received_status !="Fully Received"')
      ->andWhere('i.status !="Cancelled"')
      ->orderBy('i.id desc')
      ->execute();

    //custom processing here
    
    foreach ($this->items as $purchase) 
    {
      $purchase->receiveAll($purchase->getDate());
    }

    $this->start=$this->end-1;
  }
}
