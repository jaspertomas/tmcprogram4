<h1>Consignment Payments for <?php echo $vendor?> </h1>

<?php echo link_to("Generate Consignment Payment","consignment_payment/generate?vendor_id=".$vendor->getId()) ?>
<br><?php echo link_to("New Purchase Order","purchase/new?vendor_id=".$vendor->getId()) ?>
<br><?php echo link_to("Edit","vendor/edit?id=".$vendor->getId()) ?>
<br>
<br>Address: <?php echo $vendor->getAddr1()?>
<br>Address 2: <?php echo $vendor->getAddr2()?>
<br>Address 3: <?php echo $vendor->getAddr3()?>
<br>Phone: <?php echo $vendor->getPhone1()?>
<br>Tin No: <?php echo $vendor->getTinNo()?>
<br><?php //echo link_to("Add Quote","quote/new?vendor_id=".$vendor->getId()) ?>
<br>
<br>
<!--br--><?php //echo link_to("Print Payment Report","vendor/viewUnpaidPdf?id=".$vendor->getId()) ?>

<table border=1 width="1200">
<tr>
  <td width="16%"><h2><center><?php echo link_to("Purchases","vendor/view?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Unpaid Purchase Orders","vendor/viewUnpaid?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Returns and Replacements","vendor/viewReturns?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center>Consignment Payments</center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Counter Receipts","vendor/viewCounterReceipts?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Vouchers","vendor/viewVouchers?id=".$vendor->getId()) ?></center></h2></td>

</tr>
</table>

<table>
  <tr>
    <td>Start Date</td>
    <td>End Date</td>
    <td>View</td>
  </tr>
<?php foreach($vendor->getConsignmentPayment() as $p){?>
  <tr>
  <td><?php echo $p->getStartDate();?></td>
  <td><?php echo $p->getEndDate();?></td>
  <td><?php echo link_to("View","consignment_payment/viewSummary?id=".$p->getId());?></td>
  </tr>
<?php }?>
</table>

