<h1>Consignment Payment for <?php echo $consignment_payment->getVendor()?> </h1>
Dated from <?php echo MyDateTime::frommysql($consignment_payment->getStartDate())->toshortdate()?>
to <?php echo MyDateTime::frommysql($consignment_payment->getEndDate())->toshortdate()?>
<br>Total Payable: <?php echo MyDecimal::format($totaltotalbuyprice)?>
<br>Total Sale: <?php echo MyDecimal::format($totaltotalsellprice)?>
<br>Total Profit: <?php echo MyDecimal::format($totaltotalsellprice-$totaltotalbuyprice)?>
<br><?php echo link_to("View Details","consignment_payment/viewDetailed?id=".$consignment_payment->getId())?>

<br>
<br>

<table>
  <tr>
    <td>Product</td>
    <td>Qty</td>
    <td>Unit Buy Price</td>
    <td>Total Buy Price</td>
    <td>Unit Sell Price</td>
    <td>Total Sell Price</td>
    <td>Total Profit</td>
    <!--td>Sell Price</b></td>
    <td>Profit</b></td-->
  </tr>
<?php foreach($profitdetailsbyproduct as $array){?>
  <?php
  $totalqty=0;
  $totalbuyprice=0;
  $totalsellprice=0;
  foreach($array as $profitdetail){
    $product=$profitdetail->getProduct();
    $totalqty+=$profitdetail->getQty();
    $totalbuyprice+=$profitdetail->getBuyprice()*$profitdetail->getQty();
    $totalsellprice+=$profitdetail->getSellprice()*$profitdetail->getQty();
  }?>
  <tr>
  <td><?php echo $product->getName();?></td>
  <td align=right><?php echo $totalqty;?></td>
  <td align=right><?php echo MyDecimal::format($totalbuyprice/$totalqty);?></td>
  <td align=right><?php echo MyDecimal::format($totalbuyprice);?></td>
  <td align=right><?php echo MyDecimal::format($totalsellprice/$totalqty);?></td>
  <td align=right><?php echo MyDecimal::format($totalsellprice);?></td>
  <td align=right><?php echo MyDecimal::format($totalsellprice-$totalbuyprice);?></td>
  <td><?php //echo link_to($product->getProducttype(),"producttype/view?id=".$product->getProducttypeId());?></td>
  </tr>
<?php }?>
<!--tr>
<td></td>
<td>Total:</td>
<td align=right><b><?php //echo MyDecimal::format($totaltotalbuyprice);?></b></td>
<td align=right><?php //echo MyDecimal::format($totaltotalsellprice);?></td>
<td align=right><?php //echo MyDecimal::format($totaltotalsellprice-$totaltotalbuyprice);?></td>
</tr-->
</table>

