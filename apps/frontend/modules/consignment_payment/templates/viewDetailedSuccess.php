<h1>Consignment Payment for <?php echo $consignment_payment->getVendor()?> </h1>
Dated from <?php echo MyDateTime::frommysql($consignment_payment->getStartDate())->toshortdate()?>
to <?php echo MyDateTime::frommysql($consignment_payment->getEndDate())->toshortdate()?>
<br>Total Payable: <?php echo MyDecimal::format($totaltotalbuyprice)?>
<br><?php echo link_to("View Summary","consignment_payment/viewSummary?id=".$consignment_payment->getId())?>

<br>
<br>

<table>
<?php foreach($profitdetailsbyproduct as $array){?>
    <tr>
      <td>Invoice</td>
      <td>PO</td>
      <td>Edit</td>
      <td>Product</td>
      <td>Unit Buy Price</td>
      <td>Unit Sell Price</td>
      <td>Qty</td>
      <td>Total Buy Price</td>
      <td>Total Sell Price</td>
    </tr>
  <?php
  $totalbuyprice=0;
  $totalsellprice=0;
  foreach($array as $profitdetail){
    $invoicedetail=$profitdetail->getInvoicedetail();
    $invoice=$invoicedetail->getInvoice();
    $purchasedetail=$profitdetail->getPurchasedetail();
    $purchase=$purchasedetail->getPurchase();
    $product=$profitdetail->getProduct();
    $totalbuyprice+=$profitdetail->getBuyprice()*$profitdetail->getQty();
    $totalsellprice+=$profitdetail->getSellprice()*$profitdetail->getQty();
  ?>
  <tr>
  <td align=right><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId());?></td>
  <td align=right><?php echo link_to($purchase->getPono(),"purchase/view?id=".$purchase->getId());?></td>
  
  <td><?php echo link_to("EditPO",'purchasedetail/edit?id='.$profitdetail->getPurchasedetailId()) ?></td>
  
  <td><?php echo $product->getName();?></td>
  <td align=right><?php echo MyDecimal::format($profitdetail->getBuyprice());?></td>
  <td align=right><?php echo MyDecimal::format($profitdetail->getSellprice());?></td>
  <td align=right><?php echo $profitdetail->getQty();?></td>
  <td align=right><?php echo MyDecimal::format($profitdetail->getBuyprice()*$profitdetail->getQty());?></td>
  <td align=right><?php echo MyDecimal::format($profitdetail->getSellprice()*$profitdetail->getQty());?></td>
  </tr>
  <?php }?>
  <tr>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td>Total:</td>
  <td align=right><b><?php echo MyDecimal::format($totalbuyprice);?></b></td>
  <td align=right><b><?php echo MyDecimal::format($totalsellprice);?></b></td>
  </tr>

<?php }?>
</table>

