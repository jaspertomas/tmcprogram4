

<hr>

<table>
  <tr>
    <td>Invoice</td>
    <td>PO</td>
    <td>Product</td>
    <td>Qty</td>
    <td>Buy Price</b></td>
    <td>Supplier</td>
  </tr>
<?php
foreach($profitdetails as $profitdetail){
  $invoicedetail=$profitdetail->getInvoicedetail();
  $invoice=$invoicedetail->getInvoice();
  $purchasedetail=$profitdetail->getPurchasedetail();
  $purchase=$purchasedetail->getPurchase();
  $product=$profitdetail->getProduct();
?>
<tr>
<td align=right><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId());?></td>
<td align=right><?php echo link_to($purchase->getPono(),"purchase/view?id=".$purchase->getId());?></td>
<td><?php echo $product->getName();?></td>
<td align=right><?php echo $profitdetail->getQty();?></td>
<td align=right><?php echo MyDecimal::format($profitdetail->getBuyprice());?></td>
<td><?php echo $purchase->getVendor();?></td>
</tr>
<?php }?>
<tr>
<td></td>
<td></td>
<td></td>
<td>Total:</td>
<td align=right><b><?php echo MyDecimal::format($totaltotalbuyprice);?></b></td>
<td></td>
<td></td>
</tr>
</table>

