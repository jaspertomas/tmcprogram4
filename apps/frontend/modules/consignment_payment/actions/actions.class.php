<?php

require_once dirname(__FILE__).'/../lib/consignment_paymentGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/consignment_paymentGeneratorHelper.class.php';

/**
 * consignment_payment actions.
 *
 * @package    sf_sandbox
 * @subpackage consignment_payment
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class consignment_paymentActions extends autoConsignment_paymentActions
{
  public function executeGenerate(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('sales','admin'),false))
    {
      return $this->redirect("home/error?msg=You have no permission to do this ");
    }
  
    $vendor_id=$request->getParameter('vendor_id');
    $this->forward404Unless($vendor = Doctrine::getTable('Vendor')->find(array($request->getParameter('vendor_id'))), sprintf('Vendor with id (%s) not found.', $request->getParameter('vendor_id')));

    if(sfConfig::get('custom_consignment_generate_by_all_products_ever_sold_by_vendor'))
    {
      //this can be used to create comparison
      //specifically used for excelite
      
      //instead of profit details based on po vendor id, 
      //take profit details of products
      //where products were bought from vendor id
      $product_ids=array();
      $purchasedetails= Doctrine_Query::create()
        ->from('Purchasedetail pr, pr.Purchase p')
        ->where('p.vendor_id ='.$vendor_id)
        ->execute();
      foreach($purchasedetails as $detail)
      {
        //omit ametek
        if($detail->getProductId()==77)continue;
        if(!isset($product_ids[$detail->getProductId()]))$product_ids[$detail->getProductId()]=$detail->getProductId();
      }

      $this->products= Doctrine_Query::create()
        ->from('Product p')
        ->whereIn('p.id',$product_ids)
        ->orderBy('name')
        ->execute();

      var_dump(count($this->products));
      foreach($this->products as $product)echo $product->getId()." ".$product->getName()."<br>";


      $this->profitdetails= Doctrine_Query::create()
        ->from('Profitdetail pr')
        ->whereIn('pr.product_id',$product_ids)
        ->andWhere('pr.consignment_payment_id is null')
        ->execute();
    }
    else
    {
    $this->profitdetails= Doctrine_Query::create()
      ->from('Profitdetail pr, pr.Purchase p')
      ->where('p.vendor_id ='.$vendor_id)
      ->andWhere('pr.consignment_payment_id is null')
      ->execute();
    }

    if(count($this->profitdetails)==0)
    {
      return $this->redirect("home/error?msg=No unpaid invoices found for vendor ".$vendor->getName());
    }

    $consignment_payment=new ConsignmentPayment();
    $consignment_payment->setVendorId($vendor_id);
    $consignment_payment->setStartDate("2015-07-01");
    $consignment_payment->setEndDate(MyDate::today());
    $consignment_payment->save();
      
    $this->ids=array();
    foreach($this->profitdetails as $profitdetail){
      $this->ids[]=$profitdetail->getId();
    }

    //add all these profit details to the consignment payment
    Doctrine_Query::create()
      ->update('Profitdetail pr')
      ->set('pr.consignment_payment_id=?',$consignment_payment->getId())
      ->whereIn('pr.id', $this->ids)
      ->execute();

    $this->redirect("consignment_payment/viewSummary?id=".$consignment_payment->getId());
  }
  public function executeViewDetailed(sfWebRequest $request)
  {
    $this->forward404Unless(
      $this->consignment_payment=Doctrine_Query::create()
        ->from('ConsignmentPayment cp, cp.Vendor v')
      	->where('cp.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Consigment Payment with id (%s) not found.', $request->getParameter('id')));
  
    $this->ids=array();
    $this->totaltotalbuyprice=0;
    $this->totaltotalsellprice=0;
    $this->profitdetailsbyproduct=array();
    $this->profitdetails= Doctrine_Query::create()
      ->from('Profitdetail pr, pr.Purchasedetail pd, pd.Purchase p, pr.Invoicedetail id, id.Invoice i, pr.Product pro')
      ->where('pr.consignment_payment_id = '.$this->consignment_payment->getId())//3=excelites
//      ->where('p.vendor_id =3')//3=excelites
      ->execute();

    foreach($this->profitdetails as $profitdetail){
      //add buyprice to totalbuyprice and totalsellprice
      $this->totaltotalbuyprice+=$profitdetail->getBuyprice()*$profitdetail->getQty();
      $this->totaltotalsellprice+=$profitdetail->getSellprice()*$profitdetail->getQty();
      $this->ids[]=$profitdetail->getId();
      
      //if product array does not exist, create
      if(!isset($this->profitdetailsbyproduct[$profitdetail->getProductId()]))
        $this->profitdetailsbyproduct[$profitdetail->getProductId()]=array();
      
      //add this to profit details by product array
      $this->profitdetailsbyproduct[$profitdetail->getProductId()][]=$profitdetail;
    }
    /*
      Doctrine_Query::create()
  ->update('Profitdetail pr')
  ->set('pr.consignment_payment_id','1')
  ->whereIn('pr.id', $this->ids)
  ->execute();
    */
  }
  public function executeViewSummary(sfWebRequest $request)
  {
    $this->executeViewDetailed($request);
  }
  public function executeIndex(sfWebRequest $request)
  {
    $this->vendor=Doctrine_Query::create()
      ->from('Vendor v, v.ConsignmentPayment cp')
    	->where('v.id = '.$request->getParameter('vendor_id'))
    	->fetchOne();
  }
}
