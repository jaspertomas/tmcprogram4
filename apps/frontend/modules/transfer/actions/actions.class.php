<?php

require_once dirname(__FILE__).'/../lib/transferGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/transferGeneratorHelper.class.php';

/**
 * transfer actions.
 *
 * @package    sf_sandbox
 * @subpackage transfer
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class transferActions extends autoTransferActions
{
    public function executeNew(sfWebRequest $request)
    {
      $this->st = new Transfer();
      $this->st->setDate(MyDate::today());
      $this->st->setIsTemporary(1);
      $this->st->setCode(TransferTable::genCode());
      $this->st->setWarehouseVectorId(sfConfig::get('custom_default_warehouse_vector_id'));
  
      //user defined code
      if($request->getParameter("code"))
        $this->st->setCode($request->getParameter("code"));
      $this->form = $this->configuration->getForm($this->st);
    }
  
    protected function processForm(sfWebRequest $request, sfForm $form)
    {
      $requestparams=$request->getParameter($form->getName());

      $warehouseVector=Fetcher::fetchOne("WarehouseVector",array("id"=>$requestparams["warehouse_vector_id"]));
      if(!$warehouseVector)$warehouseVector=Fetcher::fetchOne("WarehouseVector",array("id"=>sfConfig::get('custom_default_warehouse_vector_id')));
      $requestparams["warehouse_id"]=$warehouseVector->getFromWarehouseId();
      $requestparams["to_warehouse_id"]=$warehouseVector->getToWarehouseId();

      #if code is blank, autogenerate code
      if($requestparams["code"]=="")
      {
        $requestparams["code"]=TransferTable::genCode();
      }

      $form->bind($requestparams, $request->getFiles($form->getName()));
      if ($form->isValid())
      {
        $isnew=$form->getObject()->isNew();
        $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';
  
        try {
          $jo = $form->save();
          $user=$this->getUser()->getGuardUser();
          $jo->setUpdatedById($user->getId());
          $jo->setUpdatedAt(MyDateTime::now()->todatetime());
          if($isnew)
          {
            $jo->setCreatedById($user->getId());
            $jo->setCreatedAt(MyDateTime::now()->todatetime());
          }
          $jo->save();
        } catch (Doctrine_Validator_Exception $e) {
  
          $errorStack = $form->getObject()->getErrorStack();
  
          $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
          foreach ($errorStack as $field => $errors) {
              $message .= "$field (" . implode(", ", $errors) . "), ";
          }
          $message = trim($message, ', ');
  
          $this->getUser()->setFlash('error', $message);
          return sfView::SUCCESS;
        }
  
        $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $jo)));
        $this->getUser()->setFlash('notice', $notice);
        $this->redirect('transfer/view?id='.$jo->getId());
      }
      else
      {
        $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
      }
    }

    public function executeView(sfWebRequest $request)
    {
      $this->forward404Unless(
        $this->st=Doctrine_Query::create()
          ->from('Transfer i')
          ->where('i.id = '.$request->getParameter('id'))
          ->fetchOne()
      , sprintf('Transfer with id (%s) not found.', $request->getParameter('id')));


      $this->form = $this->configuration->getForm($this->st);

      //allow set product id by url
      $detail=new TransferDetail();
      $detail->setQty(1);
      $this->product_is_set=false;
      $this->product=null;
      if($request->getParameter("product_id"))
      {
        $detail->setProductId($request->getParameter("product_id"));
        $this->product=$detail->getProduct();
        $this->product_is_set=true;
      }
      $this->detailform = new TransferDetailForm($detail);
      $this->detail = $detail;
      $this->searchstring=$this->request->getParameter('searchstring');
    }
    public function executeFinalize(sfWebRequest $request)
    {
      $this->forward404Unless(
        $this->st=Doctrine_Query::create()
        ->from('Transfer i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
      , sprintf('Stock Transfer with id (%s) not found.', $request->getParameter('id')));
        
      //validate transfer is not cancelled
      if($this->st->getStatus()=="Cancelled")
      {
          $message=$this->st." cannot be finalized because it is already cancelled.";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
      }
    
      $this->st->setIsTemporary(0);
      $this->st->setWasClosed(1);
      $this->st->calc();
      $this->st->save();
        
      // on transfer close 
      // if no unproduced exists, stay in transfer page
      // if unproduced exists
      // if no dr, generate
      // all qtys = unreleased
      // if unproduced dr exist, update
      // if no unproduced dr, generate
      // if returns exist, show as negative
  
      //DR is required if remaining != 0
      //remaining = qty != qty_released
      //at first finalize, qty_released==0
      //and remaining = qty
      if($this->st->checkDrRequired())
      {
        //generate a dr and show it for releasing
        $jo_dr = TransferDrTable::genDrForTransferId($this->st->getId(),$this->getUser()->getGuardUser());
        return $this->redirect("transfer_dr/view?id=".$jo_dr->getId());
      }
      //else if dr not required
      else
      {
        return $this->redirect($request->getReferer());
      }
    }
    public function executeUndoclose(sfWebRequest $request)
    {
      $this->forward404Unless(
        $this->st=Doctrine_Query::create()
        ->from('Transfer i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
      , sprintf('Transfer with id (%s) not found.', $request->getParameter('id')));
    
      //validate transfer is not cancelled
      if($this->st->getStatus()=="Cancelled")
      {
          $message=$this->st." cannot be finalized because it is already cancelled.";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
      }

      $this->st->setIsTemporary(1);
      // $this->st->calc();
      $this->st->save();
    
      $this->redirect($request->getReferer());
    }

    public function executeViewPdf(sfWebRequest $request)
    {
      $this->forward404Unless($this->st=Doctrine_Query::create()
        ->from('Transfer i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne(), sprintf('Object transfer does not exist (%s).', $request->getParameter('id')));
      ; 
    
      //validate: if some items have 0 price, do not allow to print
      $zeroqtyfound=false;
      foreach($this->st->getTransferDetail() as $detail)
      {
        if($detail->getQty()==0){$zeroqtyfound=true;break;}
      }
    
      if($zeroqtyfound)
      {
        $message="Cannot print Stock Transfer with zero qty";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
    
      $this->executeView($request);
      $this->message=$request->getParameter("message");
      
      $this->download=true;//$request->getParameter('download');
      $this->setLayout(false);
      $this->getResponse()->setContentType('pdf');
    }
    /*
  public function executeDsr(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("transfer");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $jo=new Transfer();
    if(!$day or !$month or !$year)
      $jo->setDate(MyDate::today());
    else
      $jo->setDate($year."-".$month."-".$day);

    $this->form=new TransferForm($jo);
  
    $this->templates=Doctrine_Query::create()
        ->from('TransferTemplate pt')
      	->execute();
    $this->sts = TransferTable::fetchByDate($jo->getDate());
    $this->events = EventTable::fetchByDatenParentclass($jo->getDate(),"Transfer");

      $this->cashsales=0;
      $this->chequesales=0;
      $this->creditsales=0;
      $this->cashother=0;
      $this->chequeother=0;
      $this->creditother=0;
      $this->cashtotal=0;
      $this->chequetotal=0;
      $this->credittotal=0;
      $this->deducttotal=0;
      foreach($this->sts as $jo)if($jo->getStatus()!="Cancelled")
      {
        $this->cashsales+=$jo->getCash();
        $this->chequesales+=$jo->getCheque();
        $this->creditsales+=$jo->getCredit();
        $this->cashtotal+=$jo->getCash();
        $this->chequetotal+=$jo->getCheque();
        $this->credittotal+=$jo->getCredit();
        //$this->deducttotal+=$jo->getDsrdeduction();
      }
      foreach($this->events as $event)
      {
        $jo=$event->getParent();
        if($jo->getStatus()!="Cancelled")
        {
          $this->cashother+=$event->getDetail("cashamt");
          $this->chequeother+=$event->getDetail("chequeamt");
          $this->creditother+=$event->getDetail("creditamt");
          $this->cashtotal+=$event->getDetail("cashamt");
          $this->chequetotal+=$event->getDetail("chequeamt");
          $this->credittotal+=$event->getDetail("creditamt");
          $this->deducttotal+=$event->getDetail3();
        }
      }
      $this->total=$this->cashtotal+$this->chequetotal+$this->credittotal;
  }
  public function executeDsrmulti(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("startdate"))
      $invoice->setDate($request->getParameter("startdate"));
    elseif(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $requestparams=$request->getParameter("transfer");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $jo=new Transfer();
    if($request->hasParameter("enddate"))
      $jo->setDate($request->getParameter("enddate"));
    elseif(!$day or !$month or !$year)
      $jo->setDate(MyDate::today());
    else
      $jo->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->toform=new TransferForm($jo);
  
    $this->sts = TransferTable::fetchByDateRange($invoice->getDate(),$jo->getDate());
    $this->events = EventTable::fetchByDatenParentclass($jo->getDate(),"Transfer");

      foreach($this->sts as $jo)if($jo->getStatus()!="Cancelled")
      {
        $this->total+=$jo->getTotal();
      }
  }
  public function executeDsrmultiForVat(sfWebRequest $request)
  {
    $this->executeDsrmulti($request);
  }
  public function executeDsrpdf(sfWebRequest $request)
  {
    $this->executeDsr($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeDsrmultipdf(sfWebRequest $request)
  {
    $this->executeDsrmulti($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  function today() { 
    $today = getdate(); 
    return $today['year']."-".$today['mon']."-".$today['mday'];
    }

  public function executeViewSoldTo(sfWebRequest $request)
  {
    $this->forward404Unless($this->st=Doctrine_Query::create()
        ->from('Transfer i, i.Employee e, i.Vendor s')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne(), sprintf('Object transfer does not exist (%s).', $request->getParameter('id')));
    ; 
  }
  
  public function executeViewSoldToPdf(sfWebRequest $request)
  {
    $this->executeViewSoldTo($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }


public function executeAccounting(sfWebRequest $request)
{
$this->st=Doctrine_Query::create()
    ->from('Transfer i, i.Employee e, i.Vendor s')
    ->where('i.id = '.$request->getParameter('id'))
    ->fetchOne();
$this->form = $this->configuration->getForm($this->st);

$this->accountentries=$this->st->getAccountentries(true);
$this->totalsbyaccount=array();

foreach($this->st->getAccountids() as $id)
{
  $this->totalsbyaccount[$id]=0;
}
foreach($this->accountentries as $entry)
{
  $this->totalsbyaccount[$entry->getAccountId()]+=$entry->getQty();
}
}
public function executeEvents(sfWebRequest $request)
{
$this->st=Doctrine_Query::create()
    ->from('Transfer i, i.Employee e, i.Vendor s')
    ->where('i.id = '.$request->getParameter('id'))
    ->fetchOne();
$this->form = $this->configuration->getForm($this->st);
}
public function executeNew(sfWebRequest $request)
{
$this->st = new Transfer();
$this->st->setDate($this->today());
$this->st->setIsTemporary(1);

//if dr==1, this is a delivery receipt
//autogenerate dr code
if($request->getParameter("dr")==1)
{
  $this->st->setTemplateId(SettingsTable::fetch("transfer_template_delivery_receipt_id"));
  $this->st->setPono(TransferTable::genBackLoadCode());
}
else
{
  $this->st->setTemplateId(sfConfig::get('custom_default_po_template_id'));
  $this->st->genPono(); //generate po number according to template
}

//auto set employee
$employee=Doctrine_Query::create()
->from('Employee e')
->where('e.username = "'.$this->getUser()->getUsername().'"')
->fetchOne();
if($employee)
  $this->st->setEmployeeId($employee->getId());
else
    $this->st->setEmployeeId(2);//default is jonathan

if($request->getParameter("pono"))
  $this->st->setPono($request->getParameter("pono"));
//set vendor if param vendor_id
if($request->hasParameter("vendor_id"))
  $this->st->setVendorId($request->getParameter("vendor_id"));
$this->form = $this->configuration->getForm($this->st);
}
protected function processForm(sfWebRequest $request, sfForm $form)
{
$requestparams=$request->getParameter($form->getName());

#if pono is blank, autogenerate pono
if($requestparams["pono"]=="")
{
  //fetch dr / backload template id
  $delivery_receipt_template_id=SettingsTable::fetch("transfer_template_delivery_receipt_id");
  //if this is a delivery receipt / backload
  //and template is backload,
  //autogenerate backload code
  if($requestparams["template_id"]==$delivery_receipt_id)
    $requestparams["pono"]=TransferTable::genBackLoadCode();
  //else autogenerate next available po number
  else
    $requestparams["pono"]=TransferTable::genTransferOrderCode();
}

//either invno should contain a value or is_stock should be checked
if(trim($requestparams["invno"])=="" and !isset($requestparams["is_stock"]))
{
  //either invno or check box must be set 
    $message="Please enter 'Sold To' or check 'is stock' checkbox";
    $this->getUser()->setFlash('error', $message);
    return $this->redirect($request->getReferer());
}

$form->bind($requestparams,$request->getFiles($form->getName()));
$isnew=$form->getObject()->isNew();
if ($form->isValid())
{
  $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

  try {
    //set date and adjust stock entry date if necessary
    //only if transfer is not new (action is edit)
    // if(!$isnew)$form->getObject()->setDateAndUpdateStockEntry($requestparams["date"]["year"]."-".$requestparams["date"]["month"]."-".$requestparams["date"]["day"]);

    $jo = $form->save();
    $jo->calc();
    $jo->setTerms($jo->getVendor()->getTerms());
    $jo->calcDueDate();
    $jo->getUpdateChequedata();
    $jo->save();
    
  } catch (Doctrine_Validator_Exception $e) {

    $errorStack = $form->getObject()->getErrorStack();


    $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
    foreach ($errorStack as $field => $errors) {
        $message .= "$field (" . implode(", ", $errors) . "), ";
    }
    $message = trim($message, ', ');

    $this->getUser()->setFlash('error', $message);
    return sfView::SUCCESS;
  }

  $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $jo)));

  if ($request->hasParameter('_save_and_add'))
  {
    $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

    $this->redirect('@transfer_new');
  }
  else
  {
    $this->getUser()->setFlash('notice', $notice);

    $this->redirect('transfer/view?id='.$jo->getId());
  }
}
else
{
  $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
}
}
public function executeSearch(sfWebRequest $request)
{
$this->searchstring=$request->getParameter("searchstring");

//no search string, do nothing
if(trim($this->searchstring)=="")
{
  return $this->redirect("home/error?msg=Cannot search transfers: Please enter a search string");
}

$query=Doctrine_Query::create()
    ->from('Transfer i')
    ->where('i.pono = "'.trim($request->getParameter("searchstring")).'"')
    ->orWhere('i.pono          like "%'.trim($request->getParameter("searchstring")).'%"')
    ->orWhere('i.invnos          like "%'.trim($request->getParameter("searchstring")).'%"')
    ->orWhere('i.vendor_invoice like "%'.trim($request->getParameter("searchstring")).'%"')
    ->orWhere('i.memo           like "%'.trim($request->getParameter("searchstring")).'%"')
    ->orderBy('i.id desc')
    ;
$this->sts=$query->execute();

//if there is only 1 result, show page
// if(count($this->sts)==1)
// {
//   $this->st=$this->sts[0];
//   $this->redirect("transfer/view?id=".$this->st->getId());
// }
// elseif(count($this->sts)>1)

//if there are any results, even just 1, display as list
//so that if it's not the one you want, you can create new 
if(count($this->sts)>0)
{
  //nothing left to do, just let the page be displayed
}
//no search result found
else
  $this->redirect("transfer/new?pono=".$request->getParameter("searchstring"));


}
*/
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('transfer');

    $date=
      str_pad($requestparams["date"]["year"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["date"]["month"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["date"]["day"], 2, "0", STR_PAD_LEFT)
      ;

    //validate date produced
    $dateproducedvalid=true;
    if(
      $requestparams["dateproduced"]["year"]=="" or
      $requestparams["dateproduced"]["month"]=="" or
      $requestparams["dateproduced"]["day"]==""
    )
      $dateproducedvalid=false;

    $dateproduced=
      str_pad($requestparams["dateproduced"]["year"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["dateproduced"]["month"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["dateproduced"]["day"], 2, "0", STR_PAD_LEFT)
      ;

    $this->forward404Unless($jo = Doctrine::getTable('Transfer')->find(array($requestparams['id'])), sprintf('Transfer with id (%s) not found.', $request->getParameter('id')));

    //$jo->setStatus($requestparams['status']);
    $jo->setDate($date);
    $jo->setNotes($requestparams['notes']);

    $warhouseVector=Fetcher::fetchOne("WarehouseVector",array("id"=>$requestparams["warehouse_vector_id"]));
    $jo->setWarehouseVectorId($warhouseVector->getId());
    $jo->setWarehouseId($warhouseVector->getFromWarehouseId());
    $jo->setToWarehouseId($warhouseVector->getToWarehouseId());

    $jo->save();

    $this->redirect($request->getReferer());
  }
/*
public function executeBarcode(sfWebRequest $request)
{
$this->st = $this->getRoute()->getObject();

$this->details=$this->st->getTransferDetail();

$this->start=1;

}
public function executeBarcodepdf(sfWebRequest $request)
{

$this->start=$request->getParameter("start");
$qty=$request->getParameter("qty");

$this->st_id=$request->getParameter("id");
$this->st = Doctrine_Query::create()
  ->from('Transfer p')
  ->where('id = '.$this->st_id)
  ->fetchOne();
$details=$this->st->getTransferDetail();


$this->qty=array();
$this->totalqty=0;
$this->details=array();
//create qty / detail arrays, omit 0 qty items 
foreach($qty as $index=>$q)
{
  if($q!=0)
  {
    $this->totalqty+=$qty[$index];
    $this->qty[]=$qty[$index];
    $this->details[]=$details[$index];
  }
}

//create array of products
$this->products=array();
foreach($this->details as $detail)
{
  $this->products[]=$detail->getProduct();
}


$this->start--;
//$this->end=$this->start+$this->qty;


$this->download=true;//$request->getParameter('download');
$this->setLayout(false);
$this->getResponse()->setContentType('pdf');
}
public function executeInspect(sfWebRequest $request)
{
    $this->st=Doctrine_Query::create()
    ->from('Transfer i')
    ->where('i.id = '.$request->getParameter('id'))
    ->fetchOne();
    
    $value=$request->getParameter("value");
    
    $this->st->setIsInspected($value);
    $this->st->save();
    
    //update transfer details
    Doctrine_Query::create()
      ->update('TransferDetail d')
      ->set('d.is_inspected',$value)
      ->where('d.transfer_id = '.$this->st->getId())
      ->execute();
    
    //update counter receipt detail if it exists
    if($this->st->getCounterReceiptDetailId()!=null and $value==1)
      Doctrine_Query::create()
        ->update('CounterReceiptDetail d')
        ->set('is_inspected',$value)
        ->set('amount','"'.$this->st->getTotal().'"')
        ->where('d.transfer_id = '.$this->st->getId())
        ->execute();
    
    $this->redirect($request->getReferer());
}

public function executeListunpaid(sfWebRequest $request)
{
$this->sts=Doctrine_Query::create()
    ->from('Transfer p')
    ->where('status!="Paid"')
    ->andWhere('status!="Cancelled"')
    ->orderBy('p.vendor_id, p.date')
    //->limit(10)
    ->execute();

//put vendors into an array, sorted by id
$this->vendors=array();
$this->vendortransfers=array();
$this->vendortotals=array();
$this->vendorduetotals=array();
foreach($this->sts as $jo)
{
  $this->vendors[$jo->getVendorId()]=$jo->getVendor();
  //put transfers into a 2d array under the same id as the vendor
  if(!isset($this->vendortransfers[$jo->getVendorId()]))
  {
    $this->vendortransfers[$jo->getVendorId()]=array();
    $this->vendortotals[$jo->getVendorId()]=0;
    $this->vendorduetotals[$jo->getVendorId()]=0;
  }
  $this->vendortransfers[$jo->getVendorId()][]=$jo;
  $this->vendortotals[$jo->getVendorId()]+=$jo->getBalance();
  if($jo->isDue())
    $this->vendorduetotals[$jo->getVendorId()]+=$jo->getBalance();
}
}
public function executeListunproduced(sfWebRequest $request)
{
//page system
$itemsperpage=50;
$item_class="Transfer";
$this->pagepath="transfer/listunproduced";
$page=$request->getParameter("page");
if($page==null or $page<1)$page=1;
$this->page=$page;
$offset=$itemsperpage*($page-1);
$itemcount=Doctrine_Query::create()
    ->from($item_class.' p')
    ->where('produced_status!="Fully Produced"')
    ->andWhere('status!="Cancelled"')
    ->count();
$pagecount=$itemcount/$itemsperpage;
$pagecount=ceil($pagecount);
$this->pagecount=$pagecount;

$this->sts=Doctrine_Query::create()
    ->from('Transfer p')
    ->where('produced_status!="Fully Produced"')
    ->andWhere('status!="Cancelled"')
    ->orderBy('p.vendor_id desc, p.date desc')
    ->offset($offset)
    ->limit($itemsperpage)
    ->execute();

//put vendors into an array, sorted by id
$this->vendors=array();
$this->vendortransfers=array();
foreach($this->sts as $jo)
{
  if(!isset($this->vendors[$jo->getVendorId()]))
    $this->vendors[$jo->getVendorId()]=$jo->getVendor();
  //put transfers into a 2d array under the same id as the vendor
  $this->vendortransfers[$jo->getVendorId()][]=$jo;
}
}
*/
public function executeDelete(sfWebRequest $request)
{
  $request->checkCSRFProtection();
  $jo=$this->getRoute()->getObject();

  $returncount=count($jo->getTransferDr());
  if($returncount>0)
  {
    $message="Cannot delete this Stock Transfer because DRs exist, please delete them first.";
    $this->getUser()->setFlash('error', $message);
    return $this->redirect($request->getReferer());
  }

  if ($jo->cascadeDelete())
  {
    $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
  }

  $this->redirect('transfer/index');
}
/*
public function executeAdjustCollectionStatus(sfWebRequest $request)
{
$jo=Doctrine_Query::create()
    ->from('Transfer i')
    ->where('i.id = '.$request->getParameter('id'))
    ->fetchOne();
$jo->setCollectionStatus($request->getParameter('collection_status'));
$jo->save();
echo "<font color=".$jo->getColorForIsDueString($request->getParameter('collection_status')).">".$request->getParameter('collection_status')."</font>";
die();
}
*/
public function executeCancel(sfWebRequest $request)
{
  $this->st= $this->getRoute()->getObject();

  if(count($this->st->getTransferDr())>0)
  {
    $message="Cannot cancel this Stock Transfer, because delivery receipts exist.";
    $this->getUser()->setFlash('error', $message);
    return $this->redirect($request->getReferer());
  }

  $this->st->cascadeCancel();

  $this->redirect("transfer/view?id=".$this->st->getId());
}
/*
public function executeFiles(sfWebRequest $request)
{
$this->st=MyModel::fetchOne("Transfer",array('id'=>$request->getParameter("id")));
$this->file=new File();
$this->file->setParentClass('transfer');
$this->file->setParentId($this->st->getId());
$this->form=new FileForm($this->file);

$this->files=Doctrine_Query::create()
  ->from('File f')
  ->where('f.parent_class="transfer"')
  ->andWhere('f.parent_id='.$this->st->getId())
  ->execute();
}
public function executeGenDelivery(sfWebRequest $request)
{
$jo=MyModel::fetchOne("Transfer",array('id'=>$request->getParameter("id")));

$delivery=$jo->genDelivery();    

//set date produced to delivery date
//this will also hide "produce now" link in view    
$jo->setDateproduced($delivery->getDate());
$jo->save();

$this->getUser()->setFlash('notice', "Successfully generated delivery receipt");
$this->redirect('delivery/view?id='.$delivery->getId());

}
public function executeDelivery(sfWebRequest $request)
{
$this->st=MyModel::fetchOne("Transfer",array('id'=>$request->getParameter("id")));

$this->deliverys=$this->st->getDeliverys();
}
public function executeGenConversionDr(sfWebRequest $request)
{
$pc=Doctrine_Query::create()
  ->from('Transferconversion pc')
  ->where('pc.id = '.$request->getParameter('transferconversion_id'))
  ->fetchOne();   
$this->forward404Unless($pc, sprintf('TransferConversion with id (%s) not found.', $request->getParameter('transferconversion_id')));

$delivery=$pc->genConversionDr();    

$this->getUser()->setFlash('notice', "Successfully generated conversion");
$this->redirect('delivery/view?id='.$delivery->getId());
}

public function executeViewBackloadPdf(sfWebRequest $request)
{
$this->executeView($request);
$this->message=$request->getParameter("message");

$this->download=true;//$request->getParameter('download');
$this->setLayout(false);
$this->getResponse()->setContentType('pdf');
}
// list "incompete" stock transfers, meaning sold to is not set
public function executeList(sfWebRequest $request)
{
$requestparams=$request->getParameter("transfer");
$day=$requestparams["date"]["day"];
$month=$requestparams["date"]["month"];

$year=$requestparams["date"]["year"];
$jo=new Transfer();
if(!$day or !$month or !$year)
  $jo->setDate(MyDate::today());
else
  $jo->setDate($year."-".$month."-".$day);

$this->form=new TransferForm($jo);

$this->sts=Doctrine_Query::create()
  ->from('Transfer p')
  ->where('p.invno = ""')
  ->andWhere('p.date = "'.$jo->getDate().'"')
  ->execute();
}
public function executeProduceAll(sfWebRequest $request)
{
  $requestparams=$request->getParameter("transfer");
  $jo=MyModel::fetchOne("Transfer",array('id'=>$request->getParameter("id")));
  
  if($request->getParameter("submit")=="Quick Produce" || $request->getParameter("submit")=="QR")
    $dateproduced=$jo->getDate();
  else
  {
    if(
      $requestparams["dateproduced"]["year"]=="" or
      $requestparams["dateproduced"]["month"]=="" or
      $requestparams["dateproduced"]["day"]==""
    )
    {
      $message="Invalid Produce Date";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }
    $dateproduced=
      str_pad($requestparams["dateproduced"]["year"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["dateproduced"]["month"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["dateproduced"]["day"], 2, "0", STR_PAD_LEFT)
      ;
  }
  $jo->produceAll($dateproduced);

  $this->redirect($request->getReferer());
}
public function executeViewPrintable(sfWebRequest $request)
{

$jo=Fetcher::fetchOne("Transfer",array("id"=>$request->getParameter('id')));

$content=MyTransferHelper::PrintableTransferBig($jo);

$printtoscreen=false;
if($printtoscreen)
{
  $content=str_replace("\n","<br>",$content);
  $content=str_replace(" ","&nbsp;",$content);
  echo $content;
  die();
}

$content=bin2hex($content);
  
$response = $this->getResponse();
$response->clearHttpHeaders();
//$response->setContentType($mimeType);
$response->setHttpHeader('Content-Disposition', 'attachment; filename="' . basename(str_replace(" ","",$jo->getTransferTemplate()).$jo->getPono().".dmf") . '"');
$response->setHttpHeader('Content-Description', 'File Transfer');
$response->setHttpHeader('Content-Transfer-Encoding', 'binary');
$response->setHttpHeader('Content-Length', 80*39);
$response->setHttpHeader('Cache-Control', 'public, must-revalidate');
$response->setHttpHeader('Pragma', 'public');
$response->setContent($content);
$response->sendHttpHeaders();

sfConfig::set('sf_web_debug', false);
return sfView::NONE;
}
*/
  public function executeRecalc(sfWebRequest $request)
  {
    $jo=$this->getRoute()->getObject();
    $jo->calc();
    $jo->save();
    foreach($jo->getTransferDr() as $dr)
      $dr->calc();
    foreach($jo->getTransferDetail() as $detail)
      $detail->calcDr();
    $this->redirect($request->getReferer());
  }
/*
public function executePayments(sfWebRequest $request)
{
$this->st=$this->getRoute()->getObject();
$this->form=new TransferForm($this->st);
}
public function executeResetProduceDate(sfWebRequest $request)
{

die("ACCESS DENIED");

//auto detect first occurence requiring fix
$this->first = Doctrine_Query::create()
->from('Transfer p')
->where('produced_status="Fully Produced" and (dateproduced is null or dateproduced="0000-00-00")')
->fetchOne();

  //default values
$this->interval=100;
if($this->first!=null)
  $this->start=$this->first->getId()-1;
else
  $this->start=1;

//if method = get
if(!isset($_REQUEST["submit"]))
{
  $this->sts=array();  
 return;
}
 
if(isset($_REQUEST["interval"]))
    $this->interval=$request->getParameter("interval");
if(isset($_REQUEST["start"]))
    $this->start=$request->getParameter("start");
 $this->end=$this->start+$this->interval-1;

 $this->sts = Doctrine_Query::create()
  ->from('Transfer p')
  ->where('p.id <='.$this->end)
  ->andWhere('p.id >='.$this->start)
  ->andWhere('produced_status="Fully Produced" and (dateproduced is null or dateproduced="0000-00-00")')
  ->execute();

foreach($this->sts as $p)
{
  $p->setDateAndUpdateStockEntry($p->getDate());
}

 $this->start=$this->end+1;
}
public function executeDoublePaymentCheck(sfWebRequest $request)
{
// die("ACCESS DENIED");

  //default values
$this->interval=100;
$this->start=1;

//if method = get
if(!isset($_REQUEST["submit"]))
{
  $this->sts=array();  
 return;
}
 
if(isset($_REQUEST["interval"]))
    $this->interval=$request->getParameter("interval");
if(isset($_REQUEST["start"]))
    $this->start=$request->getParameter("start");
 $this->end=$this->start+$this->interval-1;

 $this->sts = Doctrine_Query::create()
  ->from('Transfer p')
  ->where('p.id <='.$this->end)
  ->andWhere('p.id >='.$this->start)
  ->execute();

foreach($this->sts as $p)
{
  $cr=$p->getCounterReceipt();
  $vouchers=$p->getVouchers();
  if($cr!=null and count($vouchers)!=0)
  {
    echo "double payment detected: po ".$p->getPono();
    die();
  }
}

 $this->start=$this->end+1;
}
public function executeDrDiagnostics(sfWebRequest $request)
{
// die("ACCESS DENIED");

  //default values
$this->interval=100;
$this->start=21000;

//if method = get
if(!isset($_REQUEST["submit"]))
{
  $this->sts=array();  
 return;
}
 
if(isset($_REQUEST["interval"]))
    $this->interval=$request->getParameter("interval");
if(isset($_REQUEST["start"]))
    $this->start=$request->getParameter("start");
 $this->end=$this->start+$this->interval-1;

 $this->sts = Doctrine_Query::create()
  ->from('Transfer p')
  ->where('p.id <='.$this->end)
  ->andWhere('p.id >='.$this->start)
  ->execute();

foreach($this->sts as $p)
{
  foreach($p->getTransferDetail() as $detail)
  {
    $produced=0;
    foreach($detail->getTransferDrDetail() as $drdetail)
    {
      if($drdetail->isProduced())
        $produced+=$drdetail->getQty();
    }
    if($produced>$detail->getQty())
    {
        echo "DR Calculation Error detected: "
          ."<br>".$p
          ."<br>PO ID ".$p->getId()
          ."<br>Released Qty: ".$released
          ."<br>Transfer Qty: ".$detail->getQty()
          ."<br>Product: ".$detail->getProduct()->getName()
          ;
      die();
    }
  }
}
 $this->start=$this->end+1;
}

public function executeCheckExists(sfWebRequest $request)
{
$jo=Fetcher::fetchOne("Transfer",array("pono"=>$request->getParameter("code")));
if($jo!=null)
  echo json_encode(array("id"=>$jo->getId(), "total"=>$jo->getTotal(), "vendor"=>$jo->getVendor()->getName()));
else 
  echo "";
die();
}

*/


}
