<?php
$red="#faa";
$yellow="#ffc";
$green="#6d6";
$plain="#eee";
?>

<?php use_helper('I18N', 'Date') ?>

<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1><?php echo $st; ?>  (<?php echo $st->getTransferStatus()?>, <font <?php if($st->getIsTemporaryString()=="Cancelled")echo "color=red"?>><?php echo $st->getIsTemporaryString()?></font>)
<?php 
  if($sf_user->hasCredential(array('admin', 'encoder', 'inventory'),false))
  {
		if($st->getIsTemporary()==0)
			echo link_to("Undo Close","transfer/undoclose?id=".$st->getId()); 
		else 
			echo link_to("Close","transfer/finalize?id=".$st->getId()); 
  }
?>
</h1>
<?php slot('transaction_id', $st->getId()) ?>
<?php slot('transaction_type', "Transfer") ?>

<?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
<?php echo form_tag_for($form,'transfer/adjust')?> <?php echo $form['id'] ?>
<?php }?>	
<table>
  <tr valign=top>
    <td>
			<table>
      <tr valign=top>
          <td>Stock Transfer No.</td>
          <td><?php echo $st; ?></td>
        </tr>
        <tr valign=top>
          <td>Date</td>
          <td><?php 
            if($st->getIsTemporary()!=0 && $sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder')))
              echo $form['date'];
            else 
              echo MyDateTime::frommysql($st->getDate())->toshortdate();
      ?></td>
        </tr>
        <tr valign=top>
          <td>Transfer Direction:</td>
          <td><?php 
            if($st->getIsTemporary()!=0 && $sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false))
              echo $form['warehouse_vector_id'];
            else
              echo $form->getObject()->getWarehouseVector()->getName(); 
          ?></td>
        </tr>
        <tr valign=top>
          <!-- <td>Date Completed</td> -->
          <td><?php /*
            if($st->getIsTemporary()!=0 && $sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false))
              echo $form['date_completed'];
            else
              echo MyDateTime::frommysql($st->getDateCompleted())->toshortdate();
            */
          ?></td>
        </tr>
			</table>
    </td>
    <td>
			<table>
				<tr valign=top>
					<td>Notes
					  <?php echo $st->getNotes() ?><br>
            <?php if($st->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <?php echo $form['notes'] ?>
            <?php }?>	
				  </td>
				</tr>
			</table>
      <?php if($st->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
        <input type=submit value=Save>
      <?php }?>	
    </td>
  </tr>
</table>
</form>

<?php if($st->getTransferStatus()!="Complete"){?>
<?php echo form_tag_for($form,'transfer/transferAll', array('style'=>'display:inline','onsubmit'=>"return confirm('Transfer all items: Are you sure?');"))?>
<input type=hidden id=id name=id value=<?php echo $st->getId()?>>
<?php 
  $form->getObject()->setDateCompleted(MyDate::today());
  $form=new TransferForm($form->getObject());
  echo $form['date_completed'];
?>
</form>
<br>
<?php } ?>

<?php echo link_to("Edit","transfer/edit?id=".$st->getId()) ?> |
<?php echo link_to('Delete','transfer/delete?id='.$st->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
<?php if($st->getIsTemporary()==0 and $st->getStatus()!="Cancelled" and $st->getStatus()!="Complete")echo link_to("Generate DR","transfer_dr/new?transfer_id=".$st->getId());?> |
 | <?php if($st->getIsTemporary()==0 and $st->getStatus()!="Cancelled")echo link_to("Print","transfer/viewPdf?id=".$st->getId());?>
<br>
<?php echo link_to("Recalculate","transfer/recalc?id=".$st->getId());?> |

<?php //echo link_to("View Sold To","transfer/viewSoldTo?id=".$st->getId());?> |
<?php //echo link_to("Print Sold To","transfer/viewSoldToPdf?id=".$st->getId());?> |

<br>
Related DRs:
<?php foreach($st->getTransferDr() as $ref){?>
  <?php echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".link_to($ref,"transfer_dr/view?id=".$ref->getId());?> 
  <?php if($ref->isCancelled()){echo "<font color=red>(Cancelled)</font>";?>
  <?php } else { ?>
    <?php //if($ref->getQtyToTransfer()==0 and $ref->getQtyToConsume()==0)echo "(Empty)"?>
    <?php if($ref->isTransferred())echo " ".MyDateTime::fromdatetime($ref->getDatetime())->toshortdate();?>
    <?php if($ref->getQtyToTransfer()>0)echo "(".$ref->getQtyToTransfer()." ".($ref->isTransferred()?"Transferred":"Pending Transfer").")"?>
  <?php } ?>
<?php } ?>

<hr>

<!-------------------------------------------------------------------------------->

<?php
//if user is salesman or encoder
if($st->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){
?>
Search Product: <input id=transferproductsearchinput autocomplete="off">


<?php echo form_tag_for($detailform,"@transfer_detail",array("id"=>"new_transfer_detail_form")); ?>
<input type=hidden name=transfer_detail[transfer_id] value=<?php echo $st->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>
<table>
	<tr>
		<td>Qty</td>
		<td>Product</td>
		<td>Notes</td>
	</tr>
	<tr>
		<td><?php echo $detailform["qty"]; ?></td>
		<td>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<!--td align=right><input type=checkbox name=transfer_detail[is_discounted] id=chk_is_discounted></td-->
		<td><?php echo $detailform["description"]; ?></td>
		<td><input type=submit name=submit id=transfer_detail_submit value=Save  ></form>
</td>
	</tr>
	<!--tr>
	  <td>Barcode: <?php //echo $detailform["barcode"]; ?></td>
	</tr-->
</table>

<?php  } ?>

<div id="transfersearchresult"></div>


<!-------------------------------------------------------------------------------->

<table border=1>
  <tr>
    <td>Product</td>
    <td>Qty</td>
    <td>Qty</td>
    <td>Notes</td>
    <?php if($st->getIsTemporary()!=0){ ?>
      <td></td>
      <td></td>
    <?php } ?>
    <td>Qty Pending</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>Transferred</td>
    <td></td>
    <?php if($st->getIsTemporary()!=0){ ?>
      <td></td>
      <td></td>
      <td></td>
    <?php } ?>
  </tr>
  <?php foreach($st->getTransferDetail() as $detail){?>
  <tr>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
    <td>
      <?php echo $detail->getQty() ?>
      <?php if($st->getIsTemporary()!=0){ ?>
        <?php echo form_tag("transfer_detail/setQty");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=1 name=value value=<?php echo $detail->getQty()?>>
        </form>
      <?php } ?>
    </td>
    <td><?php if($detail->getQtyTransferred()!=0)echo $detail->getQtyTransferred() ?></td>
    <td><?php echo $detail->getDescription() ?></td>
    <?php if($st->getIsTemporary()!=0){ ?>
      <td>
        <?php echo link_to(
          'Delete',
          'transfer_detail/delete?id='.$detail->getId(),
          array('method' => 'delete', 'confirm' => 'Are you sure?')
        ) ?>
      </td>
      <td><?php echo link_to("Edit Product","product/edit?id=".$detail->getProductId()) ?></td>
    <?php } ?>

    <td align=center>
      <?php 
        $remaining=$detail->getQty()-$detail->getQtyTransferred();
        if($remaining!=0)echo "<font color=red>".$remaining."</font>";
      ?>
    </td>
  </tr>
  <?php }?>
</table>
<hr>
<br><?php echo link_to("Cancel","transfer/cancel?id=".$st->getId()) ?>
<br><?php //echo link_to("View Files","transfer/files?id=".$st->getId());?>

<script type="text/javascript">
//for ajax
var baseurl="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/";

//set price textbox to read only
//$("#transfer_detail_price").prop('readonly', true);
//select purchno
$("#transfer_purchno").focus();
$("#transfer_purchno").select(); 
//if no product id set, disable save button
if($("#transfer_detail_product_id").val()=='')	 		  
  $("#transfer_detail_submit").prop("disabled",true);

//------Transfer (not header) product search-----------
//$("#transferproductsearchinput").keyup(function(){
//$("#transferproductsearchinput").on('input propertychange paste', function() {
$("#transferproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#transfer_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
//    if($("#transferproductsearchinput").val().length>=3)
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
	    $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+$("#transferproductsearchinput").val()+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	 		  $("#transfersearchresult").html(result);
	    }});
    //else clear
    else
 		  $("#transfersearchresult").html("");
});

//this is called when user clicks "Choose" button in product search form
function populateSubform(product_id,product_name,price,min_price,allow_zeroprice,curr_qty) {
  $("#product_min_price").val(min_price);
  $("#product_price").val(price);
  $("#product_name").html(product_name);
  
  $("#transfer_detail_price").val(min_price);
//  $("#transfer_detail_price").val(0);
//  $("#transfer_detail_with_commission").val(price);
  $("#productname").html(product_name);
  $("#transfer_detail_product_id").val(product_id);
  $("#transfersearchresult").html("");

  $("#chk_is_discounted").removeAttr("disabled");
  $("#transfer_detail_description").removeAttr("disabled");
  $("#transfer_detail_submit").removeAttr("disabled");
  $("#transfer_detail_qty").removeAttr("disabled");
  $("#transfer_detail_price").removeAttr("disabled");
  $("#transfer_detail_with_commission").removeAttr("disabled");
  if(curr_qty!="0.00")alert("Current stock available: "+curr_qty.replace(".00", "")+" units");

  $("#transfer_detail_qty").focus();
  
}
</script>
