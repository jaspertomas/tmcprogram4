<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name'));

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(20, 10, 20, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================

//hide headers by config
$showheader=true;
if($showheader)
{
// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
// $pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 100, '', 'PNG', '', '', false, 300, 'C', false, false, 0, false, false, false);
// $pdf->write(5,"\n\n\n\n");
$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->write(0,sfConfig::get('custom_company_header_text'),'',false,'C',true,0,false,false,0,0);
//$pdf->write(0,sfConfig::get('custom_company_address'),'',false,'C',true,0,false,false,0,0);
$pdf->write(0,sfConfig::get('custom_company_email'),'',false,'C',true,0,false,false,0,0);
$pdf->write(5,"\n");
}
else
{
  $pdf->write(0,sfConfig::get('custom_header_for_no_header')."\n");
}

$tbl = "";


$content=array(
  $st->getCode(),
  MyDateTime::frommysql($st->getDate())->toshortdatewithweek(),
  $st->getWarehouseVector()->getName(),
  sfConfig::get('custom_company_header_text'),
  );
  
  

$tbl .= <<<EOD
<table>
 <tr>
  <td align="left">Stock Transfer: $content[0]</td>
  <td align="right">Date: $content[1]</td>
 </tr>
 <tr>
  <td align="left">Deliver from $content[2]</td>
 </tr>
</table>
EOD;


//-------------------
//====TABLE HEADER===========================

$widths=array(10,50,20,20,20,20);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl .= <<<EOD
<hr>
<h2 align="center">$content[3] ST$content[0]</h2>
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Qty</b></td>
  <td width="$widths[1]" align="center"><b>Product</b></td>
 </tr>
</thead>
EOD;

//===TABLE BODY============================
$height=1;
  $grandtotal=0;
	foreach($st->getTransferdetails() as $detail)
  {
    $content=array(
      $detail->getQty(),
      $detail->getProduct(),
      );
  
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center">$content[0]</td>
    <td width="$widths[1]" >$content[1]</td>
  </tr>
EOD;
  }    

$tbl .= <<<EOD
  <br>
  <table>
  <tr valign="bottom">
    <td align="center"><br><br>_______________</td>
    <td align="center"><br><br>_______________</td>
  </tr>
  <tr>
    <td align="center">Prepared By:</td>
    <td align="center">Approved By:</td>
   </tr>
  </table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

//-----------------


//--------------------------------------------------

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output("StockTransfer".$st->getCode().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

