<h1>Inventory Report Issues</h1>

<?php /*
    <?php echo link_to("Yesterday","producttype/dirIndex?invoice[date][day]=".$yesterday->getDay()."&invoice[date][month]=".$yesterday->getMonth()."&invoice[date][year]=".$yesterday->getYear()); ?>
    <?php echo link_to("Tomorrow","producttype/dirIndex?invoice[date][day]=".$tomorrow->getDay()."&invoice[date][month]=".$tomorrow->getMonth()."&invoice[date][year]=".$tomorrow->getYear()); ?>
</form>
*/?>


<!--page system-->
Page
<?php echo $page;?>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."?page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."?page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo $apage." ";
  else echo link_to("$apage",$pagepath."?page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."?page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."?page=".$pagecount))." ";
?>
<!--end page system-->

<table border=1>
<tr>
  <td>Date</td>
  <td>Product</td>
  <td>Prev Balance</td>
  <td>Reported</td>
  <td>Comment</td>
  <td>Notes</td>
</tr>

<?php foreach($stockentries as $entry){ ?>

<tr>
  <td><?php echo MyDateTime::fromdatetime($entry->getDatetime())?></td>
  <td><?php echo link_to($entry->getStock()->getProduct()->getName(),"stock/view?id=".$entry->getStockId())?></td>
  <td><?php echo $entry->getBalance()?></td>
  <td><?php echo $entry->getQtyReported()?></td>
  <td><?php echo $entry->getReportComment()?></td>
  <td><?php echo $entry->getDescription()?></td>
</tr>

<?php } ?>
</table>


