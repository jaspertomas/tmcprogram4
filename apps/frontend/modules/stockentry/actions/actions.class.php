<?php

require_once dirname(__FILE__).'/../lib/stockentryGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/stockentryGeneratorHelper.class.php';

/**
 * stockentry actions.
 *
 * @package    sf_sandbox
 * @subpackage stockentry
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class stockentryActions extends autoStockentryActions
{
  public function executeNew(sfWebRequest $request){$this->redirect($request->getReferer());}
  public function executeEdit(sfWebRequest $request){$this->redirect($request->getReferer());}
  public function executeUpdate(sfWebRequest $request){$this->redirect($request->getReferer());}
  public function executeList(sfWebRequest $request){$this->redirect($request->getReferer());}
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparam=$request->getParameter('stockentry');
    $requestparam['created_at']=MyDateTime::now()->todatetime();
    $requestparams["created_by_id"]=$this->getUser()->getGuardUser()->getId();
    $stock=StockTable::fetchById($requestparam['stock_id']);

    $qty=$requestparam['qty'];
    $qty_reported=$requestparam['qty_reported'];
    $datetime=
      $requestparam['datetime']['year']."-".
      $requestparam['datetime']['month']."-".
      $requestparam['datetime']['day']." ".
      $requestparam['datetime']['hour'].":".
      $requestparam['datetime']['minute'];
    //$ref_class=$requestparam['ref_class'];
    //$ref_id=$requestparam['ref_id'];
    $type=$requestparam['type']!=""?$requestparam['type']:'Adjustment';
    //$priority=0;
    $description=$requestparam['description'];

/*
    if($qty==0)
      $this->redirect('home/error?msg="Invalid Qty"');
*/

    $form->bind($requestparam, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $stockentry=$stock->addEntry($datetime, $qty, null, null ,$type,$description,$this->getUser()->getGuardUser()->getId(),$qty_reported);

      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $stockentry)));

      $this->getUser()->setFlash('notice', $notice);

      if($request->getParameter('redirect_to')=="soldTo")
        $this->redirect('stock/soldTo?id='.$stockentry->getstockId());
      else
        $this->redirect('stock/view?id='.$stockentry->getstockId());
    }
    else
    {
      if($form['qty']->getError())
        $this->redirect('home/error?msg="Invalid Qty: '.$qty.'"');
      if($form['datetime']->getError())
        $this->redirect('home/error?msg="Invalid Date/Time: '.$datetime.'"');

      //$this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $stockentry=$this->getRoute()->getObject();
    $stock=$stockentry->getStock();
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($stockentry->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    //$stock->calc($stockentrydate);
    $this->redirect($request->getReferer());
  }
  //this is an index of report stock entries with issues (excess or missing qtys)
  public function executeReportIssues(sfWebRequest $request)
  {
    //page system
    $itemsperpage=50;
    $item_class="Stockentry";
    $this->pagepath="stockentry/reportIssues";
    
    $page=$request->getParameter("page");
    if($page==null or $page<1)$page=1;
    $this->page=$page;
    $offset=$itemsperpage*($page-1);
    $itemcount=Doctrine_Query::create()
        ->from($item_class.' s')
        ->where('s.qty_missing !=0 ')
      	->count();
    $pagecount=$itemcount/$itemsperpage;
    $pagecount=ceil($pagecount);
    $this->pagecount=$pagecount;
    //end page system

    $this->stockentries=Doctrine_Query::create()
      ->from('Stockentry s')
      ->where('s.qty_missing !=0 ')
      ->orderBy('datetime desc, id desc')
      ->offset($offset)
      ->limit($itemsperpage)
      ->execute();
  }
  public function executeInspect(sfWebRequest $request)
  {
    $se=Fetcher::fetchOne("StockEntry",array("id"=>$request->getParameter("id")));
    if($se)
    {
      $this->redirect("stock/view?id=".$se->getStockId());
    }
  }
}

