<?php use_helper('I18N', 'Date'); ?>

<?php if(count($deliverys)==0)echo "No conversions found";else{ ?>
<h1>Conversion Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Type</td>
    <td>Customer/Vendor</td>
    <td>Reference</td>
    <td>Particulars</td>
  </tr>
  
  <?php 
  foreach($deliverys as $delivery){?>
  <tr>
    <td><?php echo link_to($delivery->getName(),"delivery/view?id=".$delivery->getId()) ?></td>
    <td><?php echo $delivery->getType() ?></td>
    <td><?php $client=$delivery->getClient();if($client!=null)echo link_to($client,$delivery->getClientClass()."/view?id=".$delivery->getClientId()) ?></td>
    <td><?php $ref=$delivery->getRef();if($ref!=null)echo link_to($ref,$delivery->getRefClass()."/view?id=".$delivery->getRefId()) ?></td>
    <td><?php echo $delivery->getParticularsString() ?></td>
    <td><?php echo $delivery->getNotes() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

<hr>

<?php if(count($returnss)==0)echo "No returns found";else{ ?>
<h1>Return Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Type</td>
    <td>Customer/Vendor</td>
    <td>Reference</td>
    <td>Particulars</td>
  </tr>
  
  <?php 
  foreach($returnss as $returns){?>
  <tr>
    <td><?php echo link_to($returns->getCode(),"returns/view?id=".$returns->getId()) ?></td>
    <td><?php echo $returns->getType() ?></td>
    <td><?php $client=$returns->getClient();if($client!=null)echo link_to($client,$returns->getClientClass()."/view?id=".$returns->getClientId()) ?></td>
    <td><?php $ref=$returns->getRef();if($ref!=null)echo link_to($ref,$returns->getRefClass()."/view?id=".$returns->getRefId()) ?></td>
    <td><?php echo $returns->getParticularsString() ?></td>
    <td><?php echo $returns->getNotes() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

<hr>

<?php if(count($replacements)==0)echo "No replacements found";else{ ?>
<h1>Replacement Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Type</td>
    <td>Customer/Vendor</td>
    <td>Reference</td>
    <td>Particulars</td>
  </tr>
  
  <?php 
  foreach($replacements as $replacement){?>
  <tr>
    <td><?php echo link_to($replacement->getName(),"replacement/view?id=".$replacement->getId()) ?></td>
    <td><?php echo $replacement->getType() ?></td>
    <td><?php $client=$replacement->getClient();if($client!=null)echo link_to($client,$replacement->getClientClass()."/view?id=".$replacement->getClientId()) ?></td>
    <td><?php $ref=$replacement->getRef();if($ref!=null)echo link_to($ref,$replacement->getRefClass()."/view?id=".$replacement->getRefId()) ?></td>
    <td><?php echo $replacement->getParticularsString() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

<hr>

<?php if(count($counter_receipts)==0)echo "No counter receipts found";else{ ?>
<h1>Counter Receipt Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Supplier</td>
    <td>Purchase Orders</td>
    <td>Status</td>
  </tr>
  
  <?php 
  foreach($counter_receipts as $counter_receipt){?>
  <tr>
    <td><?php echo link_to($counter_receipt->getCode(),"counter_receipt/view?id=".$counter_receipt->getId()) ?></td>
    <td><?php echo $counter_receipt->getVendor() ?></td>
    <td><?php     foreach($counter_receipt->getPurchases() as $p)
    {
      echo link_to($p->getPono(),"purchase/view?id=".$p->getId()).", ";
    }
 ?></td>
    <td><?php echo $counter_receipt->getStatus() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

<hr>

<?php if(count($out_checks)==0)echo "No outgoing checks found";else{ ?>
<h2>Outgoing Check Search</h2>
<table border="1">
  <tr>
    <td>Date</td>
    <td>Voucher Code</td>
    <td>Account</td>
    <td>Check No</td>
    <td>Amount</td>
    <td>Status</td>
    <td>Payee</td>
    <td>Voucher Date</td>
    <td>Particulars</td>
  </tr>
  
  <?php 
  foreach($out_checks as $out_check){
    $voucher=null;
    foreach($out_check->getOutPayment() as $p)
      $voucher=$p->getParent();
      ?>
  <tr>
    <td><?php echo MyDateTime::frommysql($out_check->getCheckDate())->toshortdate(); ?></td>
    <td><?php if($voucher!=null)echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId()); ?></td>
    <td><?php echo $out_check->getPassbook(); ?></td>
    <td><?php echo link_to($out_check->getCheckNo(),"out_check/view?id=".$out_check->getId()); ?></td>
    <td><?php echo MyDecimal::format($out_check->getAmount()) ?></td>
    <td><font <?php if($voucher!=null and $voucher->getCheckStatus()=="Cancelled")echo "color=red"?>><?php if($voucher!=null)echo $voucher->getCheckStatus() ?></font></td>
    <td><?php if($voucher!=null)echo $voucher->getPayee() ?></td>
    <td><?php if($voucher!=null)echo link_to(MyDateTime::frommysql($voucher->getDate())->toshortdate(), "voucher/dashboard?date=".$voucher->getDate()) ?></td>
    <td><?php if($voucher!=null)echo $voucher->getRef().": ".$voucher->getParticulars() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

<hr>

<?php if(count($in_checks)==0)echo "No incoming checks found";else{ ?>
<h2>Incoming Check Search</h2>
<table border="1">
  <tr>
    <td>Date</td>
    <td>Check Code</td>
    <td>Check No</td>
    <td>Amount</td>
    <td>Customer</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>Invoice</td>
    <td>Date</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>
  
  <?php foreach($in_checks as $in_check){ ?>
  <tr>
    <td><?php echo MyDateTime::frommysql($in_check->getCheckDate())->toshortdate(); ?></td>
    <td><?php echo link_to($in_check->getCode(),"in_check/view?id=".$in_check->getId()) ?></td>
    <td><?php echo $in_check->getCheckNo() ?></td>
    <td><?php echo MyDecimal::format($in_check->getAmount()) ?></td>
    <td><?php echo link_to($in_check->getCustomer(),"customer/viewChecks?id=".$in_check->getCustomerId());?></td>
  </tr>
  

  <?php foreach($in_check->getInvoice() as $invoice){ ?>
  <?php if($invoice->getChequeAmt()>0){?>
    <tr>
      <td></td>
      <td></td>
      <td><?php echo link_to($invoice, "invoice/events?id=".$invoice->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
      <td><?php echo $invoice->getChequeAmt() ?></td>
      <td><?php echo $invoice->getParticularsString() ?></td>
      <td><?php echo $invoice->getInCheck()->getNotes() ?></td>
    </tr>
  <?php } ?>
<?php } ?>

<?php foreach($in_check->getEvent() as $detail){$invoice=$detail->getParent(); ?>
    <tr>
      <td></td>
      <td></td>
      <td><?php echo link_to($invoice, "invoice/events?id=".$invoice->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td><?php echo MyDecimal::format($detail->getAmount()) ?></td>
      <td><?php echo $invoice->getParticularsString() ?></td>
      <td><?php echo $detail->getNotes() ?></td>
    </tr>
  <?php }?>




  <?php } ?>
</table>
<?php } ?>

<hr>

<?php if(count($vouchers)==0)echo "No check vouchers found";else{ ?>
<h2>Check Voucher Search</h2>
<table border="1">
  <tr>
    <td>Check Date</td>
    <td>Create Date</td>
    <td>Ref</td>
    <td>Account</td>
    <td>Check No</td>
    <td>Account</td>
    <td>Type</td>
    <td>Pay To</td>
    <td>Amount</td>
    <td>Status</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>
  
  <?php foreach($vouchers as $voucher){?>
    <?php $check=null;$out_payment=$voucher->getOutPayment(); if($out_payment)$check=$out_payment->getOutCheck();?>

    <tr>
        <td><?php if($check)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
        <td><?php echo MyDateTime::frommysql($voucher->getDate())->toshortdate()?></td>
        <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
        <td><?php if($check)echo $check->getPassbook(); ?></td>
        <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo(); ?></td>
        <td><?php echo $voucher->getVoucherAccount()?></td>
        <td><?php echo $voucher->getVoucherType()?></td>
        <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
        <td><?php echo MyDecimal::format($voucher->getAmount());?></td>
        <td><font <?php if($voucher->getCheckStatus()=="Cancelled")echo "color=red"?>><?php echo $voucher->getCheckStatus() ?></font></td>
        <td><?php echo $voucher->getParticulars()?></td>
        <td><?php echo $voucher->getNotes()?></td>
    </tr>  
  <?php } ?>
</table>
<?php } ?>
