<?php

/**
 * search actions.
 *
 * @package    sf_sandbox
 * @subpackage search
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class searchActions extends sfActions
{
  public function executeSearch(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()
        ->from('Replacement d')
      	->where('d.code like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("d.code")
        ;
  	$this->replacements=$query->execute();
  	
    $query=Doctrine_Query::create()
        ->from('Returns d')
      	->where('d.code like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("d.code")
        ;
  	$this->returnss=$query->execute();
  	
    $query=Doctrine_Query::create()
        ->from('Delivery d')
      	->where('d.code like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("d.code")
        ;
  	$this->deliverys=$query->execute();
  	
    $query=Doctrine_Query::create()
        ->from('CounterReceipt d')
      	->where('d.code like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("d.code")
        ;
  	$this->counter_receipts=$query->execute();
  	
    $query=Doctrine_Query::create()
        ->from('OutCheck c')
      	->where('c.check_no like \'%'.trim($request->getParameter("searchstring")).'%\'')
      	->orWhere('c.code like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("c.check_no")
        ;
  	$this->out_checks=$query->execute();
  	
    $query=Doctrine_Query::create()
        ->from('InCheck c')
      	->where('c.check_no like \'%'.trim($request->getParameter("searchstring")).'%\'')
      	->orWhere('c.code like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("c.check_no")
        ;
  	$this->in_checks=$query->execute();
  	
    $query=Doctrine_Query::create()
        ->from('Voucher c')
      	->where('c.no like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("c.check_no")
        ;
  	$this->vouchers=$query->execute();
  	
    $this->searchstring=$request->getParameter("searchstring");
  }
}
