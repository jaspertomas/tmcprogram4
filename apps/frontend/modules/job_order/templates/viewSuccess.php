<?php
$red="#faa";
$yellow="#ffc";
$green="#6d6";
$plain="#eee";
?>

<?php use_helper('I18N', 'Date') ?>

<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1><?php echo $jo; ?>  (<?php echo $jo->getProductionStatus()?>, <?php echo $jo->getIsTemporaryString()?>)
<?php 
  if($sf_user->hasCredential(array('admin', 'encoder', 'inventory'),false))
  {
		if($jo->getIsTemporary()==0)
			echo link_to("Undo Close","job_order/undoclose?id=".$jo->getId()); 
		else 
			echo link_to("Close","job_order/finalize?id=".$jo->getId()); 
  }
?>
</h1>
<?php slot('transaction_id', $jo->getId()) ?>
<?php slot('transaction_type', "JobOrder") ?>

<?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
<?php echo form_tag_for($form,'job_order/adjust')?> <?php echo $form['id'] ?>
<?php }?>	
<table>
  <tr valign=top>
    <td>
			<table>
      <tr valign=top>
          <td>JO No.</td>
          <td><?php echo $jo; ?></td>
        </tr>
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($jo->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($jo->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['date'] ?>
            <?php }?>	
          </td>
        </tr>
        <tr valign=top>
          <td>Date Completed</td>
          <td><?php if($jo->getDateCompleted()!=null)echo MyDateTime::frommysql($jo->getDateCompleted())->toshortdate() ?></td>
        </tr>
			</table>
    </td>
    <td>
			<table>
				<tr valign=top>
					<td>Notes
					  <?php echo $jo->getNotes() ?><br>
            <?php if($jo->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <?php echo $form['notes'] ?>
            <?php }?>	
				  </td>
				</tr>
			</table>
      <?php if($jo->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
        <input type=submit value=Save>
      <?php }?>	
    </td>
  </tr>
</table>
</form>

<?php if($jo->getProductionStatus()!="Complete"){?>
<?php echo form_tag_for($form,'job_order/produceAll', array('style'=>'display:inline','onsubmit'=>"return confirm('Produce all items: Are you sure?');"))?>
<input type=hidden id=id name=id value=<?php echo $jo->getId()?>>
<?php 
  $form->getObject()->setDateCompleted(MyDate::today());
  $form=new JobOrderForm($form->getObject());
  echo $form['date_completed'];
?>
<!-- <input type=submit name=submit id=submit value="Produce All">
<?php //if($sf_user->hasCredential(array('admin'), false)){?><input type=submit name=submit id=submit value="Quick Produce"><?php //} ?> -->
</form>
<br>
<?php } ?>

<?php echo link_to("Edit","job_order/edit?id=".$jo->getId()) ?> |
<?php echo link_to('Delete','job_order/delete?id='.$jo->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
<?php //echo link_to("Create Barcodes","job_order/barcode?id=".$jo->getId()) ?> |
<?php //echo link_to("View Files","job_order/files?id=".$jo->getId());?> | 
<?php if($jo->getIsTemporary()==0 and $jo->getStatus()!="Cancelled")echo link_to("Generate DR","job_order_dr/new?job_order_id=".$jo->getId());?> |
 | <?php if($jo->getIsTemporary()==0 and $jo->getStatus()!="Cancelled")echo link_to("Print","job_order/viewPdf?id=".$jo->getId());?>
 | <?php //echo link_to("View DRs","job_order/delivery?id=".$jo->getId());?>
 | <?php //echo link_to("Create DR",'job_order/genDelivery?id='.$jo->getId(),array('confirm' => 'Create delivery receipt: Are you sure?'));?>
<br>
<?php echo link_to("Recalculate","job_order/recalc?id=".$jo->getId());?> |

<?php //echo link_to("View Sold To","job_order/viewSoldTo?id=".$jo->getId());?> |
<?php //echo link_to("Print Sold To","job_order/viewSoldToPdf?id=".$jo->getId());?> |

<br>
Related DRs:
<?php foreach($jo->getJobOrderDr() as $ref){?>
  <?php echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".link_to($ref,"job_order_dr/view?id=".$ref->getId());?> 
  <?php if($ref->isCancelled()){echo "<font color=red>(Cancelled)</font>";?>
  <?php } else { ?>
    <?php if($ref->getQtyToProduce()==0 and $ref->getQtyToConsume()==0)echo "(Empty)"?>
    <?php if($ref->isProduced())echo " ".MyDateTime::fromdatetime($ref->getDatetime())->toshortdate();?>
    <?php if($ref->getQtyToProduce()>0)echo "(".$ref->getQtyToProduce()." ".($ref->isProduced()?"Produced":"Pending Production").")"?>
    <?php if($ref->getQtyToConsume()>0)echo "(".$ref->getQtyToConsume()." ".($ref->isProduced()?"Consumed":"Pending Consumption").")"?>
  <?php } ?>
<?php } ?>

<hr>

<!-------------------------------------------------------------------------------->

<?php
//if user is salesman or encoder
if($jo->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){
?>
Search Product: <input id=job_orderproductsearchinput autocomplete="off">


<?php echo form_tag_for($detailform,"@job_order_detail",array("id"=>"new_job_order_detail_form")); ?>
<input type=hidden name=job_order_detail[job_order_id] value=<?php echo $jo->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>
<table>
	<tr>
		<td>Qty</td>
		<td>Product</td>
		<td>Notes</td>
	</tr>
	<tr>
		<td><?php echo $detailform["qty"]; ?></td>
		<td>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<!--td align=right><input type=checkbox name=job_order_detail[is_discounted] id=chk_is_discounted></td-->
		<td><?php echo $detailform["description"]; ?></td>
		<td><input type=submit name=submit id=job_order_detail_submit value=Save  ></form>
</td>
	</tr>
	<!--tr>
	  <td>Barcode: <?php //echo $detailform["barcode"]; ?></td>
	</tr-->
</table>

<?php  } ?>

<div id="job_ordersearchresult"></div>


<!-------------------------------------------------------------------------------->

<table border=1>
  <tr>
    <td>Product</td>
    <td>Qty</td>
    <td>Qty</td>
    <td>Qty</td>
    <td>Notes</td>
    <?php if($jo->getIsTemporary()!=0){ ?>
      <td></td>
      <td></td>
      <td></td>
    <?php } ?>
    <td>Qty Pending</td>
    <td>Qty Pending</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>Produced</td>
    <td>Consumed</td>
    <td></td>
    <?php if($jo->getIsTemporary()!=0){ ?>
      <td></td>
      <td></td>
      <td></td>
    <?php } ?>
    <td>Production</td>
    <td>Consumption</td>
  </tr>
  <?php foreach($jo->getJobOrderDetail() as $detail){?>
  <tr>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
    <td>
      <?php echo $detail->getQty() ?>
      <?php if($jo->getIsTemporary()!=0){ ?>
        <?php echo form_tag("job_order_detail/setQty");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=1 name=value value=<?php echo $detail->getQty()?>>
        </form>
      <?php } ?>
    </td>
    <td><?php if($detail->getQtyProduced()>0)echo $detail->getQtyProduced() ?></td>
    <td><?php if($detail->getQtyProduced()<0)echo $detail->getQtyProduced()*-1 ?></td>
    <td><?php echo $detail->getDescription() ?></td>
    <?php if($jo->getIsTemporary()!=0){ ?>
      <td><?php echo link_to("Edit","job_order_detail/edit?id=".$detail->getId()) ?></td>
      <td>
        <?php echo link_to(
          'Delete',
          'job_order_detail/delete?id='.$detail->getId(),
          array('method' => 'delete', 'confirm' => 'Are you sure?')
        ) ?>
      </td>
      <td><?php echo link_to("Edit Product","product/edit?id=".$detail->getProductId()) ?></td>
    <?php } ?>

    <td align=center>
      <?php 
        $remaining=$detail->getQty()-$detail->getQtyProduced();
        if($remaining>0)echo "<font color=red>".$remaining."</font>";
      ?>
    </td>
    <td align=center>
      <?php 
        // $remaining=$detail->getQty()-$detail->getQtyProduced();
        if($remaining<0)echo "<font color=red>".($remaining*-1)."</font>";
      ?>
    </td>
  </tr>
  <?php }?>
</table>
<hr>
<br><?php echo link_to("Cancel","job_order/cancel?id=".$jo->getId()) ?>
<br><?php //echo link_to("View Files","job_order/files?id=".$jo->getId());?>

<!----------------conversion----------------->

<hr>
<h2>Possible Conversions</h2>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Conversion</td>
    <td>Previous<br>Conversions</td>
    <td>Convert</td>
    <td></td>
  </tr>
  <?php 
  	foreach($jo->getJobOrderConversion() as $dc){?>
  <tr>
    <td><?php $possibleqty=$dc->getPossibleConversionQty($jo->getJobOrderdetail()); echo $possibleqty; ?></td>
    <td><?php echo $dc->getConversion()?></td>
    <td><?php $qty=$dc->getQty(); echo $qty; ?></td>
    <td><?php if($possibleqty>$qty) echo link_to("Convert","job_order/genConversionDr?job_order_conversion_id=".$dc->getId(), array('confirm' => 'This will generate a product conversion. Are you sure?')) ?></td>
  </tr>
  <?php }?>
</table>

<!---------------end conversion------------------>

<script type="text/javascript">
//for ajax
var baseurl="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/";

//set price textbox to read only
//$("#job_order_detail_price").prop('readonly', true);
//select purchno
$("#job_order_purchno").focus();
$("#job_order_purchno").select(); 
//if no product id set, disable save button
if($("#job_order_detail_product_id").val()=='')	 		  
  $("#job_order_detail_submit").prop("disabled",true);

//------JobOrder (not header) product search-----------
//$("#job_orderproductsearchinput").keyup(function(){
//$("#job_orderproductsearchinput").on('input propertychange paste', function() {
$("#job_orderproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#job_order_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
//    if($("#job_orderproductsearchinput").val().length>=3)
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
	    $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+$("#job_orderproductsearchinput").val()+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	 		  $("#job_ordersearchresult").html(result);
	    }});
    //else clear
    else
 		  $("#job_ordersearchresult").html("");
});

//this is called when user clicks "Choose" button in product search form
function populateSubform(product_id,product_name,price,min_price,allow_zeroprice,curr_qty) {
  $("#product_min_price").val(min_price);
  $("#product_price").val(price);
  $("#product_name").html(product_name);
  
  $("#job_order_detail_price").val(min_price);
//  $("#job_order_detail_price").val(0);
//  $("#job_order_detail_with_commission").val(price);
  $("#productname").html(product_name);
  $("#job_order_detail_product_id").val(product_id);
  $("#job_ordersearchresult").html("");

  $("#chk_is_discounted").removeAttr("disabled");
  $("#job_order_detail_description").removeAttr("disabled");
  $("#job_order_detail_submit").removeAttr("disabled");
  $("#job_order_detail_qty").removeAttr("disabled");
  $("#job_order_detail_price").removeAttr("disabled");
  $("#job_order_detail_with_commission").removeAttr("disabled");
  if(curr_qty!="0.00")alert("Current stock available: "+curr_qty.replace(".00", "")+" units");

  $("#job_order_detail_qty").focus();
  
}
</script>
