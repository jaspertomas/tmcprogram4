<?php
  function recursive($vsa, $startdate, $enddate)
  {
    $children=$vsa->getChildren();
    if(count($children)==0)return;
    foreach($children as $vsa){
      echo "<tr><td>";
      for($i=0;$i<$vsa->getDepth();$i++)echo "&nbsp;-&nbsp;";
        echo link_to($vsa->getName(),"voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate);
      echo "</td><td>";
        echo link_to("Report","voucher_subaccount/report?id=".$vsa->getId());
      echo "</td><td>";
        echo link_to("Create Voucher","voucher_subaccount/newVoucher?id=".$vsa->getId());
      echo "</td><td>";
        echo link_to("Edit","voucher_subaccount/edit?id=".$vsa->getId());
      echo "</td><td>";
      echo "</td><td>";
        echo $vsa->getVoucherAllocation();
      echo "</td><td>";
        echo $vsa->getVoucherType();
      echo "</td><td>";
        echo $vsa->getPassbook();
      echo "</td><td>";
        echo $vsa->getPayee();
      echo "</td></tr>";
      recursive($vsa, $startdate, $enddate);
    }
  }
?>

<?php use_helper('I18N', 'Date') ?>
<?php $formpath="voucher_account/tree";?>
<?php $pdfformpath="voucher_account/treePdf";?>
<?php echo form_tag_for(new InvoiceForm(),$formpath)?>
<?php 
$startdate=MyDateTime::frommysql($startdateform->getObject()->getDate())->tomysql();
$enddate=MyDateTime::frommysql($enddateform->getObject()->getDate())->tomysql();
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
$startofthisyear=$today->getstartofyear();
$startofnextyear=$startofthisyear->addyears(1);
$startoflastyear=$startofthisyear->addyears(-1);
$endofthisyear=$startofthisyear->getendofyear();
$endofnextyear=$startofnextyear->getendofyear();
$endoflastyear=$startoflastyear->getendofyear();
?>

<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $startdateform["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $enddateform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Year","$formpath?startdate=".$startoflastyear->tomysql()."&enddate=".$endoflastyear->tomysql());?> | 
<?php echo link_to("This Year","$formpath?startdate=".$startofthisyear->tomysql()."&enddate=".$endofthisyear->tomysql());?> | 
<?php echo link_to("Next Year","$formpath?startdate=".$startofnextyear->tomysql()."&enddate=".$endofnextyear->tomysql());?> | 
<br>
<?php echo link_to("Last Month","$formpath?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","$formpath?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","$formpath?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br>
    <?php echo link_to("Yesterday","$formpath?startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> | 
    <?php echo link_to("Tomorrow","$formpath?startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> | 
<br>

</form>

<h1>Voucher Account Report Index </h1>
Date: <?php echo MyDateTime::frommysql($startdate)->toshortdate(); ?> to <?php echo MyDateTime::frommysql($enddate)->toshortdate(); ?>

<br>
<?php 
$datearray=explode("-",$startdate);
$todatearray=explode("-",$enddate);
/*
echo link_to("Print","$pdfformpath?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);?>
*/
echo "<br>";
echo link_to("New Sub Account","voucher_subaccount/new");?>
<br><br>
<table border=1>
    <tr>
			<td>Name</td>
      <td>Edit</td>
      <td>Allocation</td>
      <td>Type</td>
      <td>Is Checked</td>
      <td>Bank Account</td>
      <td>Pay To</td>
		</tr>

    <?php foreach($voucher_accounts as $va){?>
		<tr>
			<td><?php echo link_to($va->getName(),"voucher_account/report?id=".$va->getId()."&startdate=".$startdate."&enddate=".$enddate)?></td>
			<td><?php echo link_to("Report","voucher_account/report?id=".$va->getId()."&startdate=".$startdate."&enddate=".$enddate)?></td>
			<td><?php echo link_to("Create Voucher","voucher/new?account_id=".$va->getId()."&date=".MyDate::today())?></td>
      <td><?php echo link_to("Edit","voucher_account/edit?id=".$va->getId());?></td>
      <td><?php 
        if($va->getIsChecked())echo "&#8730; ".link_to("(X)","voucher_account/noCheck?id=".$va->getId());
        else echo "X ".link_to("(&#8730;)","voucher_account/yesCheck?id=".$va->getId());
      ?></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
		</tr>

		<?php foreach($va->getDirectSubaccounts() as $vsa){?>
		<tr>
			<td>
				<?php for($i=0;$i<$vsa->getDepth();$i++)echo "&nbsp;-&nbsp;";?>
				<?php echo link_to($vsa->getName(),"voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate)?>
      </td>
      <td><?php echo link_to("Report","voucher_subaccount/report?id=".$vsa->getId());?></td></td>
      <td><?php echo link_to("Create Voucher","voucher_subaccount/newVoucher?id=".$vsa->getId());?></td>
      <td><?php echo link_to("Edit","voucher_subaccount/edit?id=".$vsa->getId());?></td>
      </td><td>
      <td><?php echo $vsa->getVoucherAllocation();?></td>
      <td><?php echo $vsa->getVoucherType();?></td>
      <td><?php echo $vsa->getPassbook();?></td>
      <td><?php echo $vsa->getPayee();?></td>
		</tr>

    <?php recursive($vsa, $startdate, $enddate);?>
		
		<?php } ?>
		
	<?php } ?>
</table>