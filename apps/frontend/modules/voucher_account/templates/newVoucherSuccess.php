<?php
  function recursive($vsa, $date, $enddate, $children)
  {
    if(count($children)==0)return;
    foreach($children as $vsa){
      echo "<tr><td>";
      for($i=0;$i<$vsa->getDepth();$i++)echo "&nbsp;-&nbsp;";
        echo link_to($vsa->getName(),"voucher_subaccount/newVoucher?id=".$vsa->getId()."&date=".$date);
      echo "</td><td>";
        echo link_to("Edit","voucher_subaccount/edit?id=".$vsa->getId());
      echo "</td></tr>";
      recursive($vsa, $date, $enddate);
    }
  }
?>

<?php use_helper('I18N', 'Date') ?>
<?php $formpath="voucher_account/newVoucher";?>
<?php echo form_tag_for($form,$formpath)?>
<?php 
$today=MyDateTime::frommysql($date); 
$yesterday=MyDateTime::frommysql($date); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($date); 
$tomorrow->adddays(1);
?>

<table>
  <tr>
    <td>Date</td>
    <td><?php echo $form["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<br>
    <?php echo link_to("Yesterday","$formpath?date=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> | 
    <?php echo link_to("Tomorrow","$formpath?date=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> | 
<br>

</form>

<h1>New Voucher: <?php echo link_to($va->getName(),"voucher_account/report?id=".$va->getId()."&date=".$date)?></h1>
Date: <?php echo MyDateTime::frommysql($date)->toshortdate(); ?>

<br>
<?php 
$datearray=explode("-",$date);
$todatearray=explode("-",$enddate);
?>

<br>Please select a sub account for the new voucher
<table border=1>
  <tr>
    <td>&nbsp;- (<?php echo link_to("New Sub Account","voucher_subaccount/new");?>)</td>
    <td></td>
  </tr>
    <?php foreach($subaccounts as $vsa){?>
      <?php $children=$vsa->getChildren();?>
  <tr>
    <td>
      <?php for($i=0;$i<$vsa->getDepth();$i++)echo "&nbsp;-&nbsp;";?>
      <?php 
        if(count($children)==0)
          echo link_to($vsa->getName(),"voucher_subaccount/newVoucher?id=".$vsa->getId()."&date=".$date);
        else
          echo $vsa->getName();
        ?>
    </td>
    <td>
      <?php echo link_to("Edit","voucher_subaccount/edit?id=".$vsa->getId());?>
    </td>
  </tr>

  <?php recursive($vsa, $date, $enddate, $children);?>
  
  <?php } ?>
		
</table>