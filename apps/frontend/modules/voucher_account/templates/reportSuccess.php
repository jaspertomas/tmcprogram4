<?php
  function recursive($vsa, $startdate, $enddate)
  {
    $children=$vsa->getChildren();
    if(count($children)==0)return;
    foreach($children as $vsa){
      echo "<tr><td>";
          for($i=0;$i<$vsa->getDepth();$i++)echo "&nbsp;-&nbsp;";
          echo link_to($vsa->getName(),"voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate);
      echo "</td></tr>";
      recursive($vsa, $startdate, $enddate);
    }
  }
?>

<?php use_helper('I18N', 'Date') ?>
<?php $formpath="voucher_account/report";?>
<?php $pdfformpath="voucher_account/reportPdf";?>
<?php echo form_tag_for(new InvoiceForm(),$formpath)?>
<?php 
$startdate=MyDateTime::frommysql($startdateform->getObject()->getDate())->tomysql();
$enddate=MyDateTime::frommysql($enddateform->getObject()->getDate())->tomysql();
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
$startofthisyear=$today->getstartofyear();
$startofnextyear=$startofthisyear->addyears(1);
$startoflastyear=$startofthisyear->addyears(-1);
$endofthisyear=$startofthisyear->getendofyear();
$endofnextyear=$startofnextyear->getendofyear();
$endoflastyear=$startoflastyear->getendofyear();
?>

<input type="hidden" name="id" value="<?php echo $va->getId()?>">
<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $startdateform["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $enddateform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Year","voucher_account/report?id=".$va->getId()."&startdate=".$startoflastyear->tomysql()."&enddate=".$endoflastyear->tomysql());?> | 
<?php echo link_to("This Year","voucher_account/report?id=".$va->getId()."&startdate=".$startofthisyear->tomysql()."&enddate=".$endofthisyear->tomysql());?> | 
<?php echo link_to("Next Year","voucher_account/report?id=".$va->getId()."&startdate=".$startofnextyear->tomysql()."&enddate=".$endofnextyear->tomysql());?> | 
<br>
<?php echo link_to("Last Month","voucher_account/report?id=".$va->getId()."&startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","voucher_account/report?id=".$va->getId()."&startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","voucher_account/report?id=".$va->getId()."&startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br>
    <?php echo link_to("Yesterday","voucher_account/report?id=".$va->getId()."&startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> | 
    <?php echo link_to("Tomorrow","voucher_account/report?id=".$va->getId()."&startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> | 
<br>

</form>

<h1>Voucher Account Report </h1>
<h2>
<?php echo link_to("Home","voucher_account/tree"."?startdate=".$startdate."&enddate=".$enddate);?> > 
<?php echo link_to($va,"voucher_account/report?id=".$va->getId()."&startdate=".$startdate."&enddate=".$enddate);?>
</h2>
Date: <?php echo MyDateTime::frommysql($startdate)->toshortdate(); ?> to <?php echo MyDateTime::frommysql($enddate)->toshortdate(); ?>

<br>Cash Sales: <?php echo MyDecimal::format($cashtotal)?>
<br>
<?php 
$datearray=explode("-",$startdate);
$todatearray=explode("-",$enddate);
echo link_to("Print","$pdfformpath?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);?>

<h3>Sub Accounts</h3>
<table>
  <?php foreach($va->getDirectSubaccounts() as $vsa){?>
		<tr>
			<td>
				<?php for($i=0;$i<$vsa->getDepth();$i++)echo "&nbsp;-&nbsp;";?>
				<?php echo link_to($vsa->getName(),"voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate)?>
			</td>
      <?php recursive($vsa, $startdate, $enddate); ?>
		</tr>
  <?php } ?>
<table>

<br>
<br>
<table border=1>
  <tr>
    <td>Voucher No.</td>
    <td>Date</td>
    <td>Payee</td>
    <td>Amount</td>
    <td>From Bank</td>
    <td>Allocation</td>
    <td>Account</td>
    <td>Subaccount</td>
    <td>For Year</td>
    <td>Particulars</td>
    <td>Notes</td>
    <td>Status</td>
  </tr>
    <?php foreach($vouchers as $voucher){?>
    <tr>
      <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId()) ?></td>
      <td><?php echo $voucher->getDate() ?></td>
      <td><?php echo $voucher->getPayee() ?></td>
      <td><?php echo MyDecimal::format($voucher->getAmount()) ?></td>
      <td><?php if($voucher->getOutCheckId())echo $voucher->getOutCheck()->getPassbook()->getName() ?></td>
      <td><?php echo $voucher->getVoucherAllocation() ?></td>
      <td><?php echo $voucher->getVoucherAccount() ?></td>
      <td><?php echo $voucher->getVoucherSubaccount() ?></td>
      <td><?php echo $voucher->getYear() ?></td>
      <td><?php echo $voucher->getParticulars() ?></td>
      <td><?php echo $voucher->getNotes() ?></td>
      <td><div <?php if($voucher->getStatus()=="Cancelled")echo 'style="color:red"'?>><?php echo $voucher->getStatus() ?></div></td>
      <td><?php echo link_to("Edit","voucher/edit?id=".$voucher->getId()) ?></td>
    </tr>
    <?php }?>
</table>

