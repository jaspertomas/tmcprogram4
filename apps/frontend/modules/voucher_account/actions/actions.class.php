<?php

require_once dirname(__FILE__).'/../lib/voucher_accountGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/voucher_accountGeneratorHelper.class.php';

/**
 * voucher_account actions.
 *
 * @package    sf_sandbox
 * @subpackage voucher_account
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class voucher_accountActions extends autoVoucher_accountActions
{
	public function executeTree(sfWebRequest $request)
	{
		list($this->startdateform, $this->enddateform) = MyDateRangeHelper::getForms($request);


		$this->voucher_accounts=Doctrine_Query::create()
        ->from('VoucherAccount vs')
        ->orderBy('name')
      	->execute();
	}

	public function executeReport(sfWebRequest $request)
	{
  		list($this->startdateform, $this->enddateform) = MyDateRangeHelper::getForms($request);
		$this->va=Fetcher::fetchOne("VoucherAccount",array("id"=>$request->getParameter("id")));
		$startdate=$this->startdateform->getObject()->getDate();
		$enddate=$this->enddateform->getObject()->getDate();

		$this->vouchers=Doctrine_Query::create()
			->from('Voucher v')
			->where('account_id='.$this->va->getId())
			->andWhere('date >="'.$startdate.'"')
			->andWhere('date <="'.$enddate.'"')
			->orderBy('date')
			->execute();
	}
	//this loads top level subaccounts for a given voucher_account
	public function executeGetSubaccounts(sfWebRequest $request)
	{
		$subaccounts=Fetcher::fetch("VoucherSubaccount",array(
			"voucher_account_id"=>$request->getParameter("id"),
			"depth"=>1,
		));
		$children='[';
		$first=true;
		foreach($subaccounts as $child)
		{
			if(!$first)$children.=',';
			$first=false;
			$children.='{"id":'.$child->getId().',"name":"'.$child->getName().'"}';
		}
		$children.=']';

		echo '{"id":0,"name":"","ancestors":[],"children":'.$children.'}';
		die();
	}
	public function executeNewVoucher(sfWebRequest $request)
	{
		$requestparams=$request->getParameter("voucher");
  
		$day=$requestparams["date"]["day"];
		$month=$requestparams["date"]["month"];
		$year=$requestparams["date"]["year"];
	
		$voucher=new Voucher();
		if($request->hasParameter("date"))
		  $voucher->setDate($request->getParameter("date"));
		elseif(!$day or !$month or !$year)
		  $voucher->setDate(MyDate::today());
		else
		  $voucher->setDate($year."-".$month."-".$day);

		$this->form=new VoucherForm($voucher);
		$this->voucher=$voucher;
		$this->date=$voucher->getDate();
	  
		$this->account=Fetcher::fetchOne("VoucherAccount",array(
			"id"=>$request->getParameter("account_id"),
		));
		$this->va=$this->account;
		$this->subaccounts=Fetcher::fetch("VoucherSubaccount",array(
			"voucher_account_id"=>$request->getParameter("account_id"),
			"depth"=>1,
		));
	}
	public function executeYesCheck(sfWebRequest $request)
	{
		$this->account=Fetcher::fetchOne("VoucherAccount",array(
			"id"=>$request->getParameter("id"),
		));
		$this->account->setIsChecked(true);
		$this->account->save();
		$this->redirect($request->getReferer());
	}
	public function executeNoCheck(sfWebRequest $request)
	{
		$this->account=Fetcher::fetchOne("VoucherAccount",array(
			"id"=>$request->getParameter("id"),
		));
		$this->account->setIsChecked(false);
		$this->account->save();
		$this->redirect($request->getReferer());
	}

}
