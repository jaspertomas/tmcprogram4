<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1><?php echo $returns->getPrettyType()?> Return <?php echo $returns->getCode(); ?></h1>
<?php slot('transaction_id', $returns->getId()) ?>
<?php slot('transaction_type', "Returns") ?>

<table>
  <tr valign=top>
    <td>
      <table>
          <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
          <?php echo form_tag_for($form,'returns/adjust')?> <?php echo $form['id'] ?>
          <?php }?>	
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($returns->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['date'] ?>
            <?php }?>	
          </td>
        </tr>
<?php if($returns->getType()==0){?>
        <tr valign=top>
          <td><?php echo ($returns->getIsSales()?"Customer: ":"Supplier: ")?></td>
          <td><?php echo link_to($returns->getClient(),($returns->getIsSales()?"customer":"vendor")."/view?id=".$returns->getClientId()); ?> </td>
        </tr>
<?php }else{ ?>
        <tr valign=top>
          <td>Supplier</td>
          <td><?php echo link_to($returns->getClient(),"vendor/view?id=".$returns->getClientId()); ?> </td>
        </tr>
<?php } ?>
        <tr valign=top>
          <td >Ref:</td>
          <td>
            <?php if($returns->getRefId())echo link_to($ref,strtolower($returns->getType())."/view?id=".$returns->getRefId())?>&nbsp;
          </td>
        </tr>
		</table>
    <td>
      <table>
        <tr>
          <td>Total</td>
          <td><?php echo $returns->getTotal() ?></td>
        </tr>
		    <tr>
	        <td>Status</td>
	        <td>
          <font <?php 
            switch($returns->getStatus())
            {
              case "Cancelled":echo "color=red";break;
              default: echo "color=black";break;
            }
            ?>>
            <?php 
            switch($returns->getStatus())
            {
              case "Cancelled":echo $returns->getStatus();break;
              default: break;
            }
            ?>
            </font>
          </td>
		    </tr>
        <tr>
          <td>Notes</td>
          <td><?php echo $returns->getNotes() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <textarea rows="2" cols="10" name="returns[notes]" id="returns_notes"><?php echo $returns->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <input type="submit" value="Save"></form>
            <?php }?>	
          </td>
        </tr>
		  </table>
    </td>
  </tr>
</table>

            <?php echo link_to('Delete','returns/delete?id='.$returns->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
            <?php echo link_to("Print","returns/viewPdf?id=".$returns->getId());?> |
            <?php echo link_to("View Files","returns/files?id=".$returns->getId());?> |
            <?php 
              if($returns->isTransactionBased())echo link_to("New Replacement","replacement/new?ref_id=".$returns->getRefId()."&ref_class=".$returns->getRefClass()."&returns_id=".$returns->getId());
              else echo link_to("New Replacement","replacement/new?client_id=".$returns->getClientId()."&client_class=".$returns->getClientClass()."&returns_id=".$returns->getId());
            ?> |

<br>
Related Replacements:
<?php foreach($returns->getReplacement() as $rep){?>
  <?php echo link_to($rep,"replacement/view?id=".$rep->getId())?>&nbsp;
<?php } ?>

<hr>

<h2>Items Returned</h2>

<!-----returns detail table------>

<?php if(!sfConfig::get('custom_is_discount_based')){
//--------------------------start is not discount based-----------------------
?>

<table border=1>
  <tr>
    <td>Qty</td>
    <td>Unit</td>
    <td width=50%>Description</td>
    <td>Unit Price</td>
    <td>Total</td>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td>Notes</td>
<?php } ?>
  </tr>
  <?php 
  	foreach($returns->getReturnsDetail() as $detail){?>
  <tr>
    <td><?php echo $detail->getQty() ?></td>
    <td></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>

    <td align=right >
      <?php echo MyDecimal::format($detail->getPrice()) ?>
    </td>
    <td align=right ><?php echo MyDecimal::format($detail->getTotal()); ?></td>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td><?php echo $detail->getDescription() ?></td>
    <td align=right >
<?php echo link_to('Delete','returns_detail/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
    </td>
  </tr>
  <?php }?>
  <?php }?>
  
  <tr align=right>
	  <td colspan=4>Total Return Value</td>
	  <td <?php echo MyDecimal::format($returns->getTotal())?></td>
	  <td></td>
  </tr>
</table>



<?php 
//--------------------------end is not discount based-----------------------
}else{
//--------------------------start is_discount_based-------------------------
?>

<table border=1>
  <tr>
    <td>Qty</td>
    <td>Unit</td>
    <td width=50%>Description</td>
    <td>Unit Price</td>
    <td>Total</td>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td></td>
    <td></td>
    <td>Notes</td>
<?php } ?>
  </tr>
  <?php 
  	foreach($returns->getReturnsDetail() as $detail){?>
  <tr>
    <td><?php echo $detail->getQty() ?></td>
    <td></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>

    <td align=right >
      <?php echo MyDecimal::format($detail->getPrice()) ?>
    </td>
    <td align=right ><?php echo MyDecimal::format($detail->getTotal()); ?></td>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td><?php echo $detail->getDescription() ?></td>
    <td align=right >
<?php echo link_to('Delete','returns_detail/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
    </td>
  </tr>
  <?php }?>
  <?php }?>
  
  <tr align=right>
	  <td colspan=4>Total Return Value</td>
	  <td <?php echo MyDecimal::format($returns->getTotal())?></td>
	  <td></td>
  </tr>
</table>


<?php }//-------------end is_discount_based-------------------- ?>

<hr>


<!-----invoice or PO details, choose which ones to return------>

<?php if($ref){ ?>

<h2>Return by Quantity</h2>

<?php echo form_tag("returns/setQtys");?>
<input type=hidden id=id name=id value=<?php echo $returns->getId()?>>
<table border=1>
  <tr>
    <td><?php echo $returns->getRefClass()?></td>
    <td>Return</td>
    <td>Remaining</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>Qty</td>
    <td>Qty</td>
    <td width=50%>Description</td>
    <td>Unit Price</td>
  </tr>
  <?php 
  	foreach($ref->getDetails() as $idetail){?>
  <tr>
    <td>
      <?php echo $idetail->getQty() ?>
    </td>
    <td>
      <?php 
        $rdetail=null;
        $rqty="0.00";
        if(array_key_exists($idetail->getId(), $returndetails->getRawValue()))
        {
          $rdetail=$returndetails[$idetail->getId()];
          $rqty=$rdetail->getQty();
        }
        echo $rqty;
      ?>
      <input id=qtys[<?php echo $idetail->getId()?>] size=3 name=qtys[<?php echo $idetail->getId()?>] value=<?php echo $rqty?>>

    </td>
    <td align=right ><?php echo $idetail->getQty()-$idetail->getQtyReturned(); ?></td>
    <td><?php echo link_to($idetail->getProduct(),"product/view?id=".$idetail->getProductId()) ?></td>
    <td align=right ><?php echo MyDecimal::format($idetail->getPrice()) ?></td>
  <?php }?>
  
</table>
<input type=submit value="Save">
</form>
<hr>

<?php } ?>

<h2>Choose Product to Return</h2>

<b>Search product:</b> <input id=returnsproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=returnsclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@returns_detail",array("id"=>"new_returns_detail_form")); ?>
<input type=hidden name=returns_detail[returns_id] value=<?php echo $returns->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Product</td>
		<td>Qty</td>
		<td>Price</td>
		<td>Notes</td>
	</tr>

	<tr>
		<td>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<input name="returns_detail[product_id]" id="returns_detail_product_id" type="hidden" ?>
		<td><input size="5" name="returns_detail[qty]" value="1" id="returns_detail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="8" name="returns_detail[price]" value="0.00" id="returns_detail_price" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input name="returns_detail[description]" id="returns_detail_description" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=returns_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> >
</td>
	</tr>

	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>
</form>

<div id="returnssearchresult"></div>

<hr>


<hr>
<br>
<br>
<br>
<br>

<?php 
//--------------------------end is invoice or purchase type-----------------------
?>
<hr>



<script type="text/javascript">

function changeText(id)
{
var x=document.getElementById("mySelect");
x.value=id;
}
var manager_password="<?php 
	    $setting=Doctrine_Query::create()
	        ->from('Settings s')
	      	->where('name="manager_password"')
	      	->fetchOne();
      	if($setting!=null)echo $setting->getValue();
?>";
//on qty get focus, select its contents
$("#returns_detail_qty").focus(function() { $(this).select(); } );
$("#returnsproductsearchinput").focus(function() { $(this).select(); } );
//set price textbox to read only
//$("#returns_detail_price").prop('readonly', true);
//set product name
//$("#returnsproductsearchinput").prop('value', $("#product_name").val());
//set price to default price
$("#returns_detail_price").prop('value', $("#product_price").val());
//select invno if cashier
//else set focus on product search input
var is_cashier=<?php echo $sf_user->hasCredential(array('cashier'),false)?"true":"false"?>;
var is_admin=<?php echo $sf_user->hasCredential(array('admin'),false)?"true":"false"?>;
if(is_admin)
{
$("#returnsproductsearchinput").focus();
$("#returnsproductsearchinput").select(); 
}
else if(is_cashier)
{
$("#returns_invno").focus();
$("#returns_invno").select(); 
}
else
{
$("#returnsproductsearchinput").focus();
$("#returnsproductsearchinput").select(); 
}
//if no product id set, disable save button
if($("#returns_detail_product_id").val()=='')	 		  
  $("#returns_detail_submit").prop("disabled",true);
//------returns Discount-------------------
//on page ready, var discounted is false
//var discounted=false;
//set checkbox to false as default
//$("#chk_is_discounted").prop('checked',false);
//hide all password entry boxes
$(".password_tr").attr('hidden',true);

//------returns (not header) product search-----------
//$("#returnsproductsearchinput").keyup(function(){
//$("#returnsproductsearchinput").on('input propertychange paste', function() {
$("#returnsproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#returns_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#returnsproductsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#returnsproductsearchinput").val();
      //if searchstring is all numbers except last letter, 
      //and length is 8
      //this is a barcode. 
      if(
        $.isNumeric(searchstring.slice(0, -1))
        &&
        searchstring.length>=8
        )
      {
        processBarcode(searchstring);
      }
      //else do ajax to product search, display result in returnssearchresult
      else
      {
	      $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+searchstring+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	   		  $("#returnssearchresult").html(result);
	      }});
      }
    }
    //else clear
    //else
 		//  $("#returnssearchresult").html("");
});
$("#returnsclearsearch").click(function(){
 		  $("#returnssearchresult").html("");
});
//------returns Edit-------------------
$('#returns_edit').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#returns_edit_password_tr").attr('hidden',false);
});
$('#returns_edit_password_button').click(function(event) {
	//get entered password value
    var pass=$("#returns_edit_password_input").val();
    if (pass==manager_password){
	    window.location = $('#returns_edit').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//----returns Cancel-------------------------------
/*
$('#returns_cancel').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#returns_cancel_password_tr").attr('hidden',false);
});
*/
$('#returns_cancel_password_button').click(function(event) {
	//get entered password value
    var pass=$("#returns_cancel_password_input").val();
    if (pass==manager_password){
	    window.location = $('#returns_cancel').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//----returns Undo Cancel-------------------------------
/*
$('#returns_undocancel').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#returns_undocancel_password_tr").attr('hidden',false);
});
$('#returns_undocancel_password_button').click(function(event) {
	//get entered password value
    var pass=$("#returns_undocancel_password_input").val();
    if (pass==manager_password){
	    window.location = $('#returns_undocancel').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
*/
//----returns Undo Close-------------------------------
$('#returns_undoclose').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#returns_undoclose_password_tr").attr('hidden',false);
});
$('#returns_undoclose_password_button').click(function(event) {
	//get entered password value
    var pass=$("#returns_undoclose_password_input").val();
    if (pass==manager_password){
	    window.location = $('#returns_undoclose').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//--------returns Detail Edit---------------------------
var returns_detail_edit_href="";
var detail_id="";
$('.returns_detail_edit').click(function(event) {
	//prevent form from sending
    event.preventDefault();
    //extract detail->getId() value
    detail_id=$(this).attr('detail_id');
    //save href value for later use
    returns_detail_edit_href=$(this).attr('href');
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide password entry textbox
    $("#returns_detail_edit_password_tr_"+detail_id).attr('hidden',false);
});
$('.returns_detail_edit_password_button').click(function(event) {
	//get entered password value
    var pass=$("#returns_detail_edit_password_input_"+detail_id).val();
    if (pass==manager_password){
	    window.location = returns_detail_edit_href;
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//---------New returns Detail Submit-----------------------------
//on submit new returns detail form
$("#new_returns_detail_form").submit(function(event){

  //this is when the cursor is on qty and the barcode reader is used
  //determine if qty is actually a barcode
  if(
    //if qty is too long to be a quantity, this is probably a barcode
    $("#returns_detail_qty").val().length>=8 && 
    //but only if qty is enabled
    $("#returns_detail_qty").attr("disabled")!="disabled")
  {
    processBarcode($("#returns_detail_qty").val());
    return false;
  } 

  //if discount checkbox is checked, go ahead with submit
  if($("#chk_is_discounted").prop('checked'))return true;

  //determine minimum product selling price
  var price=$("#returns_detail_price").prop('value');
  var minprice=$("#product_min_price").val();
  //if(minprice==0)minprice=$("#product_price").val();
  //if price is less than min price, complain and don't submit
  if(parseFloat(price)<parseFloat(minprice))
  {
    alert("Minimum selling price is "+minprice+".\n\nPlease check Discounted check box to request for discount, and notify manager. ");
    return false;
  }
});
//------ BARCODE -----------------------------
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
function processBarcode(barcode)
{
        //
        if(barcode.length<8)
        {
          barcode=pad(barcode, 8);
        }

		$("#pleasewait").html("<font color=red><b>PLEASE WAIT</b></font>");
	      $.ajax
	      ({
	        url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/returns/barcodeEntry?barcode="+barcode, 
	        dataType: "json",
	        success: function(result)
	        {
			$("#pleasewait").html("");
	          //if product found
	          if(result[0]!=0)
	          {
              //invoidetail input controls disabled=false
              //$("#chk_is_discounted").removeAttr('disabled');
              $("#returns_detail_description").removeAttr('disabled');
              $("#returns_detail_submit").removeAttr('disabled');
              $("#returns_detail_qty").removeAttr('disabled');
              $("#returns_detail_price").removeAttr('disabled');
              //$("#returns_detail_with_commission").removeAttr('disabled');
              $("#returns_detail_discrate").removeAttr('disabled');

	            //populate values
	            $("#returnsproductsearchinput").val("");
              $("#returns_detail_qty").val(1);
              $("#returns_detail_product_id").val(result[0]);//product_id
              $("#productname").html(result[1]);//product name
              $("#returns_detail_price").val(result[2]);//maxsellprice
              //$("#returns_detail_with_commission").val(0);
              //$("#chk_is_discounted").attr("checked",false);
              $("#returns_detail_description").val("");
              
              $("#product_min_price").val(result[3]);//minsellprice
              $("#product_price").val(result[2]);//maxsellprice
              $("#product_name").val(result[1]);//product name
              $("#returns_detail_barcode").val(barcode);
              
              //set focus to qty
              $("#returns_detail_qty").focus();
	          }
	          else
	          {
	            alert("Product not found");
              $("#returns_detail_qty").val("");
	          }
	        }
	      });

}

//this is called when user clicks "Choose" button in product search form
function populateSubform(product_id,product_name,price,min_price) {
  $("#product_min_price").val(min_price);
  $("#product_price").val(price);
  $("#product_name").html(product_name);
  
  $("#returns_detail_price").val(price);
  //$("#returns_detail_with_commission").val(0);
  $("#productname").html(product_name);
  $("#returns_detail_product_id").val(product_id);
  $("#returnssearchresult").html("");

  $("#chk_is_discounted").removeAttr("disabled");
  $("#returns_detail_description").removeAttr("disabled");
  $("#returns_detail_submit").removeAttr("disabled");
  $("#returns_detail_qty").removeAttr("disabled");
  $("#returns_detail_price").removeAttr("disabled");
  //$("#returns_detail_with_commission").removeAttr("disabled");
  $("#returns_detail_discrate").removeAttr('disabled');

  $("#returns_detail_qty").focus();
}</script>
