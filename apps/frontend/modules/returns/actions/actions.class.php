<?php

require_once dirname(__FILE__).'/../lib/returnsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/returnsGeneratorHelper.class.php';

/**
 * returns actions.
 *
 * @package    sf_sandbox
 * @subpackage returns
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class returnsActions extends autoReturnsActions
{
  public function executeFilter(sfWebRequest $request)
  {
    $this->setPage(1);

    $this->filters = $this->configuration->getFilterForm($this->getFilters());

    $requestparams=$request->getParameter($this->filters->getName());
    if($request->hasParameter("client_class"))
      $requestparams["client_class"]=$request->getParameter("client_class");

    $this->filters->bind($requestparams);
    if ($this->filters->isValid())
    {
      $this->setFilters($this->filters->getValues());

      $this->redirect('@returns');
    }

    $this->pager = $this->getPager();
    $this->sort = $this->getSort();

    $this->setTemplate('index');
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->returns = new Returns();
    
    if($request->hasParameter("ref_id"))
    {
      $this->returns->setRefId($request->getParameter("ref_id"));
      $this->returns->setRefClass($request->getParameter("ref_class"));
      $this->returns->setType($request->getParameter("ref_class"));
      
      if($request->getParameter("ref_class")=="Invoice")
      {
        $this->returns->setCode(ReturnsTable::genInCode());
        $this->ref=$this->returns->getRef();
        $this->returns->setClientId($this->ref->getCustomerId());
        $this->returns->setClientClass("Customer");
      }
      elseif($request->getParameter("ref_class")=="Purchase")
      {
        $this->returns->setCode(ReturnsTable::genOutCode());
        $this->ref=$this->returns->getRef();
        $this->returns->setClientId($this->ref->getVendorId());
        $this->returns->setClientClass("Vendor");
      }
    }
    elseif($request->hasParameter("client_id"))
    {
      if($request->getParameter("client_class")=="Customer")
        $this->returns->setCode(ReturnsTable::genInCode());
      elseif($request->getParameter("client_class")=="Vendor")
        $this->returns->setCode(ReturnsTable::genOutCode());
      $this->returns->setClientId($request->getParameter("client_id"));
      $this->returns->setClientClass($request->getParameter("client_class"));
      $this->returns->setType($request->getParameter("client_class"));
    }
    
    $this->returns->setDate(MyDate::today());

    $this->form = new ReturnsForm($this->returns);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $returns = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $returns)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect('returns/view?id='.$returns->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

  public function executeView(sfWebRequest $request)
  {
    //fetch return by id
    $this->forward404Unless(
      $this->returns=Doctrine_Query::create()
        ->from('Returns r')
      	->where('r.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    $this->form = $this->configuration->getForm($this->returns);

    $this->ref=$this->returns->getRef();

    //arrange returndetails into an array with invoicedetail as key
    //to be used as:
    //if $returndetails[invoicedetail_id] exists, do this
    //else do that
    $this->returndetails=array();
    foreach($this->returns->getReturnsDetail() as $rdetail)
    {
      $this->returndetails[$rdetail->getRefId()]=$rdetail;
    }
    
    $detail=new ReturnsDetail();
    $detail->setQty(1);
    $this->product_is_set=false;
    $this->product=null;
    $this->detailform = new ReturnsDetailForm($detail);
    $this->detail = $detail;
    $this->searchstring=$this->request->getParameter('searchstring');
  }
  public function executeSetQtys(sfWebRequest $request)
  {
    //fetch return by id
    $this->forward404Unless(
      $this->returns=Doctrine_Query::create()
        ->from('Returns r, r.ReturnsDetail d')
      	->where('r.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    //arrange returndetails into an array with invoicedetail as key
    //to be used as:
    //if $returndetails[invoicedetail_id] exists, do this
    //else do that
    $this->returndetails=array();
    foreach($this->returns->getReturnsDetail() as $rdetail)
    {
      $this->returndetails[$rdetail->getRefId()]=$rdetail;
    }

    foreach($request->getParameter('qtys') as $invoicedetail_id => $qty)
    {
      $qty=intval($qty);

      //if rdetail exists
      if(array_key_exists($invoicedetail_id, $this->returndetails))
      {
        $rdetail=$this->returndetails[$invoicedetail_id];
        $ref=$rdetail->getRef();

        //if qty is 0, adjust ref qty and delete rdetail
        if($qty==0)
        {
          //postdelete already takes care of 
          //adjusting qty returned
          //and deleting stock entry
          //$ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty());
          //$ref->save();
          //$rdetail->deleteStockEntry();
          $rdetail->delete(); 
          continue;
        }
      }
      //else if rdetail does not exist, create it
      else 
      {
        //if qty is 0, don't do anything
        if($qty==0)continue;
      
        $rdetail=new ReturnsDetail();
        $rdetail->setRefClass($this->returns->getIsSales()?"Invoicedetail":"Purchasedetail");
        $rdetail->setRefId($invoicedetail_id);
        $ref=$rdetail->getRef();
        $rdetail->setProductId($ref->getProductId());
        $rdetail->setReturnsId($this->returns->getId());
        $rdetail->setPrice($ref->getUnittotal());
      }
        
      //validate qty will not push ref qtyreturned over ref qty
      if($ref->getQtyReturned()-$rdetail->getQty()+$qty>$ref->getQty())
      {
        $error="Cannot return ".$qty." units of ".$ref->getProduct().", only ".($ref->getQty()-$ref->getQtyReturned())." units remaining";
        $this->getUser()->setFlash('error', $error);
        return $this->redirect('returns/view?id='.$this->returns->getId());
      }

      //adjust ref qty
      $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
      $ref->save();

      $rdetail->setQty($qty);
      $rdetail->setTotal($ref->getPrice()*$rdetail->getQty());
      $rdetail->save();

      $rdetail->updateStockEntry();
    }
    return $this->redirect("returns/view?id=".$this->returns->getId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $returns=$this->getRoute()->getObject();
    
    //cannot delete if replacements exist
    $replacementcount=count($returns->getReplacement());
    if($replacementcount>0)
    {
      $message="Cannot delete this Return because Replacements exist, please delete Replacements first.";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $returns)));

    if(count($returns->getReplacement())>0)
    {
      $this->getUser()->setFlash('error', "Cannot delete this return - please delete replacements first.");
      $this->redirect($request->getReferer());
    }
    
    if ($returns->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    if($returns->isClientBased())
      //$this->redirect(strtolower($returns->getClientClass())."/view?id=".$returns->getClientId());
      $this->redirect("home/index");
    else 
      $this->redirect(strtolower($returns->getRefClass())."/view?id=".$returns->getRefId());
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('returns');
    $id=$requestparams['id'];

    $this->forward404Unless(
    $this->returns=Doctrine_Query::create()
    ->from('Returns i')
    ->where('i.id = '.$id)
    ->fetchOne()
    , sprintf('Returns with id (%s) not found.', $request->getParameter('id')));

    $this->returns->setNotes($requestparams["notes"]);

    //set date and adjust stock entry date if necessary
    $this->returns->setDateAndUpdateStockEntry($requestparams["date"]["year"]."-".$requestparams["date"]["month"]."-".$requestparams["date"]["day"]);
    
    $this->returns->save();

    $this->redirect($request->getReferer());
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);

    //cannot print empty dr
    if(count($this->returns->getReturnsDetail())==0)
    {
        $message="Cannot print empty DR";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
    }

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
}
