<?php

require_once dirname(__FILE__).'/../lib/transfer_detailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/transfer_detailGeneratorHelper.class.php';

/**
 * transfer_detail actions.
 *
 * @package    sf_sandbox
 * @subpackage transfer_detail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class transfer_detailActions extends autoTransfer_detailActions
{
    public function executeNew(sfWebRequest $request)
    {
      $this->transfer_detail = new TransferDetail();
      $transfer_id=$request->getParameter("transfer_id");
      $this->transfer_detail->setTransferId($transfer_id);
      $transfer=$this->transfer_detail->getTransfer();
      $this->transfer_detail->setQty(1);
      $this->form = $this->configuration->getForm($this->transfer_detail);
    }
    protected function processForm(sfWebRequest $request, sfForm $form)
    {
      $requestparams=$request->getParameter($form->getName());
  
      $isnew=$form->getObject()->isNew();
      
      if($requestparams["qty"]==0)
      {
        $message="Invalid Quantity";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
  
      //generate slot and color if new
      if($requestparams['slot']==0 or $requestparams['color']==null)
      {
        list($slot, $color) = TransferDetailTable::getNextSlotAndColor($requestparams['$product_id']);
        $requestparams['slot']=$slot;
        $requestparams['color']=$color;
      }
  
      $form->bind($requestparams);
      if ($form->isValid())
      {
        $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';
  
        try {
          $transfer_detail = $form->save();
          // $transfer_detail->setBarcode(str_pad($transfer_detail->getId(),10,"0",STR_PAD_LEFT)."D");
          // $transfer_detail->calcRemaining();  
          //$transfer_detail->updateStockentry();
          
          //custom calculation
          // $transfer=$transfer_detail->getTransfer();
          // $transfer->calc();
          // $transfer->save();
          
          ////create transferconversion if necessary, 
          ////allowing conversion from parts to finished product in dr
          /*
          //look for the conversion that this product belongs to, if any
          $conversiondetails=$transfer_detail->getProduct()->getConversiondetail();
          $conversion=null;
          foreach($conversiondetails as $cd)
          {
            $conversion=$cd->getConversion();
  
            //if conversion exists (if product belongs to a conversion)
            //and the transfer does not have a transferconversion for it yet
            //create
            if($conversion)
            {
              $transferconversion=Doctrine_Query::create()
              ->from('Transferconversion dc')
              ->where('dc.transfer_id = '.$transfer->getId())
              ->andWhere('dc.conversion_id = '.$conversion->getId())
              ->fetchOne();
              //if $transferconversion does not exist
              if($transferconversion==null)
              {
                //create
                $transferconversion=new Transferconversion();
                $transferconversion->setTransferId($transfer->getId());
                $transferconversion->setConversionId($conversion->getId());
                $transferconversion->setQty(0);
                $transferconversion->save();
              }
            }
          }
          */
  
        } catch (Doctrine_Validator_Exception $e) {
  
          $errorStack = $form->getObject()->getErrorStack();
  
          $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
          foreach ($errorStack as $field => $errors) {
              $message .= "$field (" . implode(", ", $errors) . "), ";
          }
          $message = trim($message, ', ');
  
          $this->getUser()->setFlash('error', $message);
          return sfView::SUCCESS;
        }
  
        $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $transfer_detail)));
  
        if ($request->hasParameter('_save_and_add'))
        {
          $this->getUser()->setFlash('notice', $notice.' You can add another one below.');
          $this->redirect('@transfer_detail_new?transfer_id='.$transfer->getId());
        }
        else
        {
          $this->getUser()->setFlash('notice', $notice);
  
          $this->redirect('transfer/view?id='.$transfer_detail->getTransferId());
        }
      }
      else
      {
        $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
      }
    }
  /*
    private function processBarcode($purchdetail,$isnew)
    {
       $pds=Doctrine_Query::create()
          ->from('Profitdetail pd')
            ->where('pd.transfer_detail_id = '.$purchdetail->getId())
            ->orderBy('pd.id desc')
            ->execute();
          foreach($pds as $pd)
          {
              $invdetail=$pd->getInvoicedetail();
          
           //give back qty in profitdetail to invdetail and purchdetail
           $purchdetail->setRemaining($purchdetail->getRemaining()+$pd->getQty());
           $invdetail->setRemaining($invdetail->getRemaining()+$pd->getQty());
               
           //process barcode / create new profitdetail
           //calculate qty to process
           //it's purchdetail->remaining or invdetail->remaining, whichever is lower
             $processed=$purchdetail->getRemaining();
             if($processed>$invdetail->getRemaining())$processed=$invdetail->getRemaining();
             if($processed<0)$processed=0;
  
           //give qty to process from invdetail and purchdetail to profitdetail 
           $pd->setQty($processed);
               $purchdetail->setRemaining($purchdetail->getRemaining()-$pd->getQty());
             $invdetail->setRemaining($invdetail->getRemaining()-$pd->getQty());
           //if remaining=0, profitcalcluated=1
               $invdetail->setIsProfitcalculated($invdetail->getRemaining()==0?1:0);
             $invdetail->save();
             //end of invdetail calculation
  
               $pd->setProductId($invdetail->getProductId());
               $pd->setInvoiceId($invdetail->getInvoiceId());
               $pd->setTransferId($purchdetail->getTransferId());
               $pd->setInvoicedetailId($invdetail->getId());
               $pd->setTransferDetailId($purchdetail->getId());
               $pd->setInvoicedate($invdetail->getInvoice()->getDate());
               $pd->setTransferdate($purchdetail->getTransfer()->getDate());
               $buyprice=$purchdetail->getTotal()/$purchdetail->getQty();//buyprice per unit
               $sellprice=$invdetail->getTotal()/$invdetail->getQty();//sellprice per unit
               $pd->setSellprice($sellprice);
               $pd->setBuyprice($buyprice);
               $pd->setProfitperunit($sellprice-$buyprice);
               if($buyprice!=0)
                   $pd->setProfitrate(($sellprice/$buyprice)-1);
               //else buyprice per unit is 0 - I got it for free
               else
                   $pd->setProfitrate("100");//avoid divide by 0
               $pd->setProfit($pd->getProfitperunit()*$processed);
               $pd->save();
      }
    }
    */
    public function executeDelete(sfWebRequest $request)
    {
      $request->checkCSRFProtection();
  
      $transfer=$this->getRoute()->getObject()->getTransfer();
      $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));
  
      if ($this->getRoute()->getObject()->cascadeDelete())
      {
        $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
      }
  
      $transfer->calc();
      $transfer->save();
      $this->redirect('transfer/view?id='.$transfer->getId());
    }
    /*
    public function executeUpdateProduct(sfWebRequest $request)
    {
       $this->detail=Doctrine_Query::create()
          ->from('TransferDetail pd')
            ->where('pd.product_id = 4')//uncomment this on prod
            ->orderBy('pd.id desc')
            ->fetchOne();
        $this->detailform=new TransferDetailForm($this->detail);
        if($this->detail==null)
        {
          $this->redirect('home/error?msg='."no more records to process");
        }
        $this->transfer=$this->detail->getTransfer();
    }
    */
    public function executeProcessUpdateProduct(sfWebRequest $request)
    {
       $product=Doctrine_Query::create()
          ->from('Product p')
            ->where('p.id = '.$request->getParameter("product_id"))
            ->fetchOne();
       $purchdetail=Doctrine_Query::create()
          ->from('TransferDetail pd')
            ->where('pd.id = '.$request->getParameter("transfer_detail_id"))
            ->fetchOne();
            
        //update transfer_details that match description of selected transfer_detail 
        $products= Doctrine_Query::create()
          ->update('TransferDetail pd')
          ->set('pd.product_id',$product->getId())
          ->where('pd.description = ?', array($purchdetail->getDescription()))
          ->execute();
  
      $note=new Note();
      $note->setName($product->getId());
      $note->setDescription($purchdetail->getDescription());
      $note->save();
  
      $this->redirect($request->getReferer());
    }
    public function executeSetQty(sfWebRequest $request)
    {
      //prevent invalid qty of 0
      if($request->getParameter('value')==0)
      {
          $message="Invalid qty";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
      }
  
      //fetch transfer_detail
      $this->detail=Doctrine_Query::create()
      ->from('TransferDetail id, id.Transfer i')
      ->where('id.id = '.$request->getParameter('id'))
      ->fetchOne();
  
      if($request->getParameter('value')<0)
      {
        $error="Quantity cannot be negative";
        $this->getUser()->setFlash('error', $error);
        return $this->redirect($request->getReferer());
      }

      $this->detail->setQty($request->getParameter('value'));
      $this->detail->save();
      $jo=$this->detail->getTransfer();
      $jo->calc();//check DRs and calculate production status
      $jo->save();
  
      $this->redirect($request->getReferer());
    }
  
    public function executeView(sfWebRequest $request)
    {
      $item=Fetcher::fetchOne("TransferDetail",array("id"=>$request->getParameter("id")));
      return $this->redirect("transfer/view?id=".$item->getTransferId());
    }
  }
