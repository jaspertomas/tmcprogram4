<?php use_helper('I18N', 'Date') ?>
<h1>Unpaid Purchases <?php echo link_to("Refresh","purchase/listunpaid")?></h1>

<?php foreach($vendors as $vendor){?>
<hr>
<table>
  <tr>
    <td>Vendor</td>
    <td>Phone</td>
    <td>Total Receivable</td>
    <td><b>Total Due</b></td>
    <td>Notes</td>
  </tr>
<h3>
  <tr>
    <td><?php echo link_to($vendor,"vendor/viewUnpaid?id=".$vendor->getId()) ?></td>
    <td><?php echo $vendor->getPhone1()?></td>
    <td align=right><?php echo MyDecimal::format($vendortotals[$vendor->getId()])?></td>
    <td align=right><b><?php echo MyDecimal::format($vendorduetotals[$vendor->getId()])?></b></td>
    <td>
      <?php echo form_tag("vendor/adjustCollectionNotes",array("class"=>"collection_notes_form"))?>
      <input type=hidden name=id value=<?php echo $vendor->getId()?>><input name=collection_notes value=<?php echo $vendor->getCollectionNotes()?>>
      <input type=submit value=Save>
      </form>
    </td>
  </tr>
</table border=1>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Type</td>
    <td>Purchase</td>
    <td>Total Price</td>
    <td>Balance</td>
    <td>Notes</td>
    <td>Status</td>
  </tr>
  <?php foreach($vendorpurchases[$vendor->getId()] as $purchase){?>
  <tr>
      <td><?php echo MyDateTime::frommysql($purchase->getDate())->toprettydate() ?></td>
      <td><?php echo $purchase->getPurchaseTemplate() ?></td>
      <td><?php echo link_to($purchase->getPono(),"purchase/view?id=".$purchase->getId()) ?></td>
      <td><?php echo $purchase->getTotal() ?></td>
      <td><b><?php echo $purchase->getBalance() ?></b></td>
      <td><?php echo $purchase->getMemo() ?></td>
      <td>
        <span id=collection_status_display_<?php echo $purchase->getId()?> class=collection_status_display>
          <font color=<?php $isduestring=$purchase->getIsDueString();echo $purchase->getColorForIsDueString($isduestring)?>>
            <?php echo $isduestring;?>
          </font>
        </span>
<input type=button purchase_id="<?php echo $purchase->getId()?>" class=collection_status_button value="Due">
<input type=button purchase_id="<?php echo $purchase->getId()?>" class=collection_status_button value="Counter Received">
<input type=button purchase_id="<?php echo $purchase->getId()?>" class=collection_status_button value="Cheque Ready">
      </td>
  </tr>
  <?php }?>
</table>
<?php }?>

<script>
//on clicking Save on notes edit form
$(document).on("submit", ".collection_notes_form", function(e){
e.preventDefault();
    $.ajax({ // create an AJAX call...
        data: $(this).serialize(), // get the form data
        type: $(this).attr('method'), // GET or POST
        url: $(this).attr('action'), // the file to call
        success: function(response) { // on success..
  alert("Note Saved");
        }
    });
return false;
});
//on clicking Due, Bill Sent or Cheque REady
$(document).on("click", ".collection_status_button", function(e){
  var collection_status=$(this).val();
  var purchase_id=$(this).attr("purchase_id");
  $.ajax({ // create an AJAX call...
      data: "id="+purchase_id+"&collection_status="+collection_status, // get the form data
      type: "post", // GET or POST
      url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/purchase/adjustCollectionStatus/action",
      success: function(response) { // on success..
        $("#collection_status_display_"+purchase_id).html(response);
      }
  });
});
</script>
