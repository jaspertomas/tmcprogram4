<?php use_helper('I18N', 'Date') ?>
<h1>Unarrived Purchases <?php echo link_to("Refresh",$pagepath."")?></h1>

Page
<?php echo $page;?>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."?page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."?page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo $apage." ";
  else echo link_to("$apage",$pagepath."?page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."?page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."?page=".$pagecount))." ";

?>

<br>
<?php foreach($vendors as $vendor){?>
<hr>
<table>
  <tr>
    <td>Vendor</td>
  </tr>
<h3>
  <tr>
    <td><?php echo link_to($vendor,"vendor/view?id=".$vendor->getId()) ?></td>
  </tr>
</table border=1>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Purchase</td>
    <td>Arrived</td>
    <td>Pending</td>
    <td>Notes</td>
    <td>Status</td>
  </tr>
  <?php foreach($vendorpurchases[$vendor->getId()] as $purchase){?>
  <?php 
    $particulars=$purchase->getReceivedParticularsString();
    $receivedparticularsstring=$particulars["received"];
    $unreceivedparticularsstring=$particulars["unreceived"];
  ?>
  <tr>
      <td><?php echo MyDateTime::frommysql($purchase->getDate())->toprettydate() ?></td>
      <td><?php echo link_to($purchase->getPurchaseTemplate()." ".$purchase->getPono(),"purchase/view?id=".$purchase->getId()) ?></td>
      <td><?php echo $receivedparticularsstring ?></td>
      <td><?php echo $unreceivedparticularsstring ?></td>
      <td><?php echo $purchase->getMemo() ?></td>
      <td><?php echo $purchase->getReceivedStatus() ?></td>
  </tr>
  <?php }?>
</table>
<?php }?>

<script>
//on clicking Save on notes edit form
$(document).on("submit", ".collection_notes_form", function(e){
e.preventDefault();
    $.ajax({ // create an AJAX call...
        data: $(this).serialize(), // get the form data
        type: $(this).attr('method'), // GET or POST
        url: $(this).attr('action'), // the file to call
        success: function(response) { // on success..
  alert("Note Saved");
        }
    });
return false;
});
//on clicking Due, Bill Sent or Cheque REady
$(document).on("click", ".collection_status_button", function(e){
  var collection_status=$(this).val();
  var purchase_id=$(this).attr("purchase_id");
  $.ajax({ // create an AJAX call...
      data: "id="+purchase_id+"&collection_status="+collection_status, // get the form data
      type: "post", // GET or POST
      url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/purchase/adjustCollectionStatus/action",
      success: function(response) { // on success..
        $("#collection_status_display_"+purchase_id).html(response);
      }
  });
});
</script>
