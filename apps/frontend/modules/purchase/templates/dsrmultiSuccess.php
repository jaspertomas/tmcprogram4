<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new PurchaseForm(),"purchase/dsrmulti")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>
<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $toform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Month","purchase/dsrmulti?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql()."&template_id=".$template_id);?> | 
<?php echo link_to("This Month","purchase/dsrmulti?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql()."&template_id=".$template_id);?> | 
<?php echo link_to("Next Month","purchase/dsrmulti?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql()."&template_id=".$template_id);?> | 
<br>
    <?php echo link_to("Yesterday","purchase/dsrmulti?startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql()."&template_id=".$template_id);?> | 
    <?php echo link_to("Tomorrow","purchase/dsrmulti?startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql()."&template_id=".$template_id);?> | 
    <?php echo link_to("Go to DSR Multi for Vat","purchase/dsrmultiForVat"); ?>

</form><h1>Period Purchase Report </h1>
Date: <?php echo $form->getObject()->getDate(); $datearray=explode("-",$form->getObject()->getDate());?>

<br>Total Purchase: <?php echo MyDecimal::format($total)?>
<br><?php 
$datearray=explode("-",$form->getObject()->getDate());
$todatearray=explode("-",$toform->getObject()->getDate());
echo link_to("Print","purchase/dsrmultipdf?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);?>


<br>
Total Pumps: <?php echo MyDecimal::format($totalPumps)?><br>
Total Tanks: <?php echo MyDecimal::format($totalTanks)?><br>
Total Sheets: <?php echo MyDecimal::format($totalSheets)?><br>
Total Supplies: <?php echo MyDecimal::format($totalSupplies)?><br>
<br>
<table border=1>
  <tr>
    <td>Form</td>
    <td>Particulars</td>
    <td>Reference</td>
    <td>Code</td>
    <td>Item Description</td>
    <td>Terms</td>
    <td>Amount</td>

    <td>Pumps</td>
    <td>Tanks</td>
    <td>Sheets</td>
    <td>Supplies</td>

    <td>Salesman</td>
  </tr>
  <?php foreach(array(1,2,3,4,5) as $template_id){?>
    <?php foreach($purchases as $purchase)if($purchase->getTemplateId()==$template_id){?>
    <tr>
      <td><?php echo $purchase->getPurchaseTemplate() ?></td>
      <td><?php echo $purchase->getVendor() ?></td>
      <td><?php echo link_to($purchase->getPono(),"purchase/view?id=".$purchase->getId()) ?></td>
      <td><?php //echo $purchase->getTotal() ?></td>
      <td><?php echo $purchase->getParticularsString().($purchase->getCheque()?("; Check No.:".$purchase->getCheque().": ".$purchase->getChequedate()):"") ?></td>
      <td><?php //echo $purchase->getTotal() ?></td>
      <td align=right><?php if($purchase->getTotal()>0 and $purchase->getStatus()!="Cancelled")echo $purchase->getTotal() ?></td>

      <td align=right><?php if($purchase->getStatus()!="Cancelled" and $productCategoryTotals[$purchase->getId()][""]!=0)echo MyDecimal::format($productCategoryTotals[$purchase->getId()][""]) ?></td>
      <td align=right><?php if($purchase->getStatus()!="Cancelled" and $productCategoryTotals[$purchase->getId()]["TJL"]!=0)echo MyDecimal::format($productCategoryTotals[$purchase->getId()]["TJL"]) ?></td>
      <td align=right><?php if($purchase->getStatus()!="Cancelled" and $productCategoryTotals[$purchase->getId()]["Sheet"]!=0)echo MyDecimal::format($productCategoryTotals[$purchase->getId()]["Sheet"]) ?></td>
      <td align=right><?php if($purchase->getStatus()!="Cancelled" and $productCategoryTotals[$purchase->getId()]["Supplies"]!=0)echo MyDecimal::format($productCategoryTotals[$purchase->getId()]["Supplies"]) ?></td>

      <td><?php echo $purchase->getEmployee() ?></td>
      <td><?php echo $purchase->getStatus() ?></td>
      <td><?php echo link_to("Edit","purchase/edit?id=".$purchase->getId()) ?></td>
      <td><?php echo $purchase->getDate() ?></td>
    </tr>
    <?php }?>
    <tr>
      <td></td>
    </tr>
  <?php }?>
</table>


