<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
// half short bond paper size
$pageLayout = array(216, 140); //  or array($height, $width) 
//$pdf = new TCPDF("P", 'mm', $pageLayout, true, 'UTF-8', false);
$pdf = new TCPDF("L", PDF_UNIT, "LETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name').' Billing Statement for '.$purchase->getName());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10, 10);

//set auto page breaks
//smaller footer margin
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================



$tbl="";

$content=array(
  $purchase->__toString(),
  );
  $tbl .= <<<EOD
  <h1>$content[0]</h1>
EOD;

$content=array(
  $purchase->__toString(),
  MyDateTime::frommysql($purchase->getDate())->toshortdate(),
  );
  $tbl .= <<<EOD
<table>
<tr valign=top>
  <td>PO No.</td>
  <td>$content[0]</td>
</tr>
<tr valign=top>
  <td>Date</td>
  <td>$content[1]</td>
</tr>
<tr>
<td>Sold To</td>
EOD;

    $invoice_ids=$purchase->getInvoiceIds();
    $profit_details=$purchase->getProfitDetails();
    $sold_count=0;
    foreach($profit_details as $pd)
    {
      $sold_count+=$pd->getQty();
    }
    $purchase_details=$purchase->getPurchasedetail();
    $item_count=0;
    foreach($purchase_details as $pd)
    {
      $item_count+=$pd->getQty();
    }
    switch ($sold_count) {
      case 0: $bgcolor=$red;break;
      case $item_count: $bgcolor=$green;break;
      default: $bgcolor=$yellow;break;
    }

    $soldout_string="";
    if($sold_count==$item_count)$soldout_string.= "SOLD OUT: ";
      else $soldout_string.= $sold_count."/".$item_count.": ";
      $soldout_string.= $purchase->getInvno().": "; 
      if($invoice_ids!=null)
      {
        $invnos=explode(",",$purchase->getInvnos());
        $invoice_ids=explode(",",$invoice_ids);
        for($i=0;$i<count($invnos);$i++) 
        {
          $inv=Fetcher::fetchOne("Invoice",array("id"=>$invoice_ids[$i]));
          $soldout_string.= $invnos[$i]." (".$inv->getCustomer().")"."; ";
        }
      }

      //display links to invoices sold to
      $invoices=array();
      $invoices_string="";
      foreach($purchase->getPurchasedetail() as $detail)
      {
        $profitdetails=$detail->getProfitdetail();
        foreach($profitdetails as $pdetail)
        {
          $invoice=$pdetail->getInvoice();
          $invoice_id=$invoice->getId();
          if(!isset($invoices[$invoice_id]))
          {
            $invoices[$invoice_id]=$invoice;
          }
        }
      }
      foreach($invoices as $invoice)
      {
        // $invoices_string.= link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())." ";
        $invoices_string.= $invoice->getInvno()." ";
      }
    $content=array(
      $soldout_string,
      $invoices_string,
      );
      $tbl .= <<<EOD
      <td bgcolor="green">$content[0] $content[1]</td>
</tr>
</table>
EOD;



$content=array(
  $purchase->__toString(),
  MyDateTime::frommysql($purchase->getDate())->toshortdate(),
  );
  $tbl .= <<<EOD
<table border="1">
<tr>
    <td width="12%">Transaction<br>Date</td>
    <td width="12%">Delivery Date</td>
    <td width="8%">Status</td>
    <td width="6%">Stock<br/>In</td>
    <td width="6%">Stock<br/>Out</td>
    <!--<td>Balance</td>-->
    <td width="16%">Ref / Actual Qty</td>
    <td width="12%">Price</td>
    <!--<td>Type</td>-->
    <td width="30%">Customer / Supplier ; Notes</td>
  </tr>
EOD;

foreach($purchase_details as $pd){ 

  $content=array();
  $content[0]=$pd->getProduct()->getName();

  $tbl .= <<<EOD
  <tr><td colspan="23"><h3>$content[0]</h3></td></tr>
EOD;


    //add  stockentries from invoice details and purchase details
    $stockentries=array();
    $counter=0;//just something to make keys unique

    //foreach profit detail
    foreach($pd->getProfitdetail() as $pdetail)
    {
      //get invoice  detail
      $idetail=$pdetail->getInvoicedetail();
      //if none, do nothing
      if(!$idetail)continue;

      $idrs=$idetail->getInvoiceDrDetail();
      foreach($idrs as $idr)
      {
        if(!$idr){continue;}
        $se=$idr->getStockentry();
        if(!$se){continue;}
        $stockentries[$se->getDatetime()."-".$counter]=$se;
        $counter++;
      }
      // echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())." ";
      // $idrs=$invoice->getInvoiceDr
    }

    $pdrs=$pd->getPurchaseDrDetail();
    foreach($pdrs as $pdr)
    {
      if(!$pdr){continue;}
      $se=$pdr->getStockentry();
      if(!$se){continue;}
      $stockentries[$se->getDatetime()."-".$counter]=$se;
      $counter++;

    }

    ksort($stockentries);
    $stockentries=array_reverse($stockentries);
    foreach($stockentries as $key=>$se)
    {
      $is_conversion=false;
      $ref=$se->getRef();
      $parent=null;
      $parentref=null;
      $profitdetails=array();

      if($se->getRefClass()=="InvoiceDrDetail")
      {
        $dr=$ref->getInvoiceDr();
        // if(!$dr->isFirstDr())continue;
        $ref=$ref->getInvoicedetail();
      }
      else if($se->getRefClass()=="PurchaseDrDetail" )
      {
        $dr=$ref->getPurchaseDr();
        // if(!$dr->isFirstDr())continue;
        $ref=$ref->getPurchasedetail();
      }


      $content=array();

      //delivery date
      switch($se->getRefClass())
      {
        case "InvoiceDrDetail":
          if($ref)
          {
            $content[0]= MyDateTime::frommysql($dr->getDatetime())->toshortdate();
          }
          break;
        case "PurchaseDrDetail":
          if($ref)
          {
            $content[0]= MyDateTime::frommysql($dr->getDatetime())->toshortdate();
          }
          break;
        default: 
        $content[0]= MyDateTime::fromdatetime($se->getDatetime())->toshortdate();
      }

      //transaction date
      switch($se->getRefClass())
      {
        case "Invoicedetail":
        case "InvoiceDrDetail":
          if($ref)
          {
            $invoice=$ref->getInvoice();
            $content[1]= MyDateTime::frommysql($invoice->getDate())->toshortdate();
          }
          break;
        case "Purchasedetail":
        case "PurchaseDrDetail":
          if($ref)
          {
            $purchase=$ref->getPurchase();
            $content[1]= MyDateTime::frommysql($purchase->getDate())->toshortdate();
          }
          break;
        default: 
        // echo MyDateTime::fromdatetime($se->getDatetime())->toshortdate();
      }

      //status
      switch($se->getRefClass())
      {
        case "Purchasedetail":
        case "PurchaseDrDetail":
          $purchase=$ref->getPurchase();
          if($purchase->isCancelled())$content[2]= "<font color=red>Cancelled</font>";
          else $content[2]= $purchase->getStatus();
          break;
        case "Invoicedetail":
        case "InvoiceDrDetail":
          $invoice=$ref->getInvoice();
          if($invoice->isCancelled())$content[2]= "<font color=red>Cancelled</font>";
          else $content[2]= $invoice->getStatus();
        break;
      }

      //stock in
      if($se->getQty()>0)$content[3]= MyDecimal::format($se->getQty()); 
      //stock out
      if($se->getQty()<0)$content[4]= MyDecimal::format($se->getQty()*-1);

      //balance
      /*
      if($se->getType()=="Report")
        $content[5]= $se->getQtyReported();
      else
        $content[5]= $se->getBalance();
      */

      //reference / invoice / purchase
      switch($se->getRefClass())
      {
        case "Invoicedetail":
        case "InvoiceDrDetail":
          if($ref)
          {
            $invoice=$ref->getInvoice();
            $content[6]= $invoice;
            //$content[6]= '<button onClick={copy("'.$invoice->getInvno().'")}>C</button>';
          }
          break;
        case "Purchasedetail":
        case "PurchaseDrDetail":
          if($ref)
          {
            $purchase=$ref->getPurchase();
            $content[6]= $purchase;
            // $content[6]= '<button onClick={copy("'.$purchase->getPono().'")}>C</button>';
          }
          break;
      }

      //price
      switch($se->getType())
      {
        case "InvoiceDr":
        case "PurchaseDr":
          if(!$is_conversion)$content[7]= MyDecimal::format($ref->getUnittotal())." ";
          break;
      }

      //type?
      /*
      switch($se->getRefClass())
      {
        default:
          $content[8]= $se->getType(); if($is_conversion)$content[8].= " Conversion";
      }
      */

      $content[9]="";
      switch($se->getRefClass())
      {
        case "Purchasedetail":
        case "PurchaseDrDetail":
          $purchase=$ref->getPurchase();
          $vendor=$purchase->getVendor();
          $content[9].= "Supplier: ".link_to($vendor,"vendor/view?id=".$purchase->getVendorId())."; Sold to ".$purchase->getInvno().": "; 
          if($purchase->getMemo()!="")$content[9].= "; ".$purchase->getMemo();
          $profitdetails= $ref->getProfitdetail();
          //display links to invoices sold to
          foreach($profitdetails as $pdetail)
          {
            $invoice=$pdetail->getInvoice();
            $content[9].= $invoice->getInvno()." ";
          }
          break;
        case "Invoicedetail":
        case "InvoiceDrDetail":
          $invoice=$ref->getInvoice();
          $customer=$invoice->getCustomer();
          $content[9].= "Customer: ".link_to($customer,"customer/view?id=".$customer->getId());

          if($invoice->getNotes()!="")$content[9].= "; ".$invoice->getNotes();
          break;
        // default:
        //   if(isset($ref))
        //   {
        //     $client=$ref->getRef()->getClient();
        //     $content[9].= $client;
        //   }
          $content[9].= $se->getDescription();
      }

  $tbl .= <<<EOD
  <tr>
    <td>$content[1]</td>
    <td>$content[0]</td>
    <td>$content[2]</td>
    <td align="right">$content[3]</td>
    <td align="right">$content[4]</td>
    <!--<td align="right">$content[5]</td>-->
    <td>$content[6]</td>
    <td align="right">$content[7]</td>
    <!--<td>$content[8]</td>-->
    <td>$content[9]</td>
  </tr>
EOD;
}}

$tbl .= <<<EOD
</table>
EOD;

//-------------------

$pdf->writeHTML($tbl, true, false, false, false, '');

//-----------------


//--------------------------------------------------

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$templatenamestripped=str_replace(" ","",$templatename);
$pdf->Output($templatenamestripped.$purchase->getPono().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

