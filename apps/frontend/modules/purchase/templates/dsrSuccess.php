<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new purchaseForm(),"purchase/dsr")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
$red="#faa";
$yellow="#ffc";
$green="#6d6";
$plain="#eee";
?>
<table>
  <tr>
    <td>Date</td>
    <td><?php echo $form["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
    <?php echo link_to("Yesterday","purchase/dsr?purchase[date][day]=".$yesterday->getDay()."&purchase[date][month]=".$yesterday->getMonth()."&purchase[date][year]=".$yesterday->getYear()); ?>
    <?php echo link_to("Tomorrow","purchase/dsr?purchase[date][day]=".$tomorrow->getDay()."&purchase[date][month]=".$tomorrow->getMonth()."&purchase[date][year]=".$tomorrow->getYear()); ?>
    <?php echo link_to("Go to DSR Multi Date","purchase/dsrmulti?invoice[date][day]=".$today->getDay()."&invoice[date][month]=".$today->getMonth()."&invoice[date][year]=".$today->getYear()."&purchase[date][day]=".$today->getDay()."&purchase[date][month]=".$today->getMonth()."&purchase[date][year]=".$today->getYear()); ?>

</form><h1>Daily Purchases Report </h1>
Date: <?php echo $form->getObject()->getDate(); $datearray=explode("-",$form->getObject()->getDate());?>


<br>Cash Purchases: <?php echo MyDecimal::format($cashtotal)?>
<br>Cheque Purchases: <?php echo MyDecimal::format($chequetotal)?>
<br>Credit Purchases: <?php echo MyDecimal::format($credittotal)?>
<br>Total Purchases: <?php echo MyDecimal::format($total)?>
<br><?php echo link_to("Print","purchase/dsrpdf?purchase[date][year]=".$datearray[0]."&purchase[date][month]=".$datearray[1]."&purchase[date][day]=".$datearray[2]);?>


<br>
<br>
<table border=1>
  <tr>
    <td>Supplier</td>
    <td>Form</td>
    <td>Po No.</td>
    <td>Item Description</td>
    <td>Total</td>
    <td>Terms / Due Date</td>
    <td>Salesman</td>
    <td>Sold To</td>
    <td>Supplier<br>Invoice</td>
    <!--
    <td>Counter Receipt</td>
    -->
    <td>Received</td>
    <td>Status</td>
  </tr>
  <?php foreach($templates as $template){$template_id=$template->getId()?>
    <?php foreach($purchases as $purchase)if($purchase->getTemplateId()==$template_id){
        $purchase_details=$purchase->getPurchasedetail();
        ?>
    <tr>
      <td><?php echo $purchase->getVendor()// ?></td>
      <td><?php echo $purchase->getPurchaseTemplate() ?></td>
      <td><?php echo link_to($purchase->getPono(),"purchase/view?id=".$purchase->getId()) ?></td>
      <td><?php 
        //.($purchase->getCheque()?("; Check No.:".$purchase->getCheque().": ".$purchase->getChequedate()):"") 
        //print each product as a link
        foreach($purchase_details as $pd)
          echo link_to($pd->getProduct()->getName(),"product/view?id=".$pd->getProductId())." <br> ";
      ?></td>
      <td><?php echo MyDecimal::format($purchase->getTotal()) ?></td>
      <td><?php echo $purchase->getTerms() ?> / <?php echo MyDateTime::frommysql($purchase->getDuedate())->toshortdate() ?></td>
      <td><?php echo $purchase->getEmployee() ?></td>
      <!-----------SOLD TO--------->
      <?php 
        $invoice_ids=$purchase->getInvoiceIds();
        $profit_details=$purchase->getProfitDetails();
        $sold_count=0;
        foreach($profit_details as $pd)
        {
          $sold_count+=$pd->getQty();
        }
        $item_count=0;
        foreach($purchase_details as $pd)
        {
          $item_count+=$pd->getQty();
        }
        switch ($sold_count) {
          case 0: $bgcolor=$red;break;
          case $item_count: $bgcolor=$green;break;
          default: $bgcolor=$yellow;break;
        }
      ?>
      <td bgcolor="<?php echo $bgcolor;?>">
        <?php
          echo $sold_count."/".$item_count.": ";
          echo $purchase->getInvno().": "; 
          if($invoice_ids!=null)
          {
            $invnos=explode(",",$purchase->getInvnos());
            $invoice_ids=explode(",",$invoice_ids);
            for($i=0;$i<count($invnos);$i++) 
              echo link_to($invnos[$i],"invoice/view?id=".$invoice_ids[$i])." ";
          }
        ?>
        
      </td>
      <!-----------SUPPLIER INVOICE--------->
      <?php 
        $vi=$purchase->getVendorInvoice();
        $imagecount=$purchase->getImageCount();
        if(trim($vi)!=""){?>
          <td bgcolor="<?php echo $green;?>"><?php echo $vi?></td>
        <?php }else if($imagecount!=0){?>
          <td bgcolor="<?php echo $yellow;?>">Image taken</td>
      <?php }else{?>
          <td bgcolor="<?php echo $red;?>"><?php echo link_to("None","purchase/view?id=".$purchase->getId());?></td>
      <?php }?>
      <!--
      <td><?php //$cr=$purchase->getCounterReceipt();if($cr)echo link_to($cr->getCode(),"counter_receipt/view?id=".$cr->getId())?></td>
      -->
      <!-----------RECEIVE STATUS--------->
      <?php 
        $receivestatus=$purchase->receiveStatusString();
        switch ($receivestatus) {
          case "Not Received": $bgcolor=$red;break;
          case "Partially Received": $bgcolor=$yellow;break;
          case "Fully Received": $bgcolor=$green;break;
        }
        if($receivestatus!="Fully Received"){?>
          <td bgcolor="<?php echo $bgcolor;?>"><?php echo link_to($receivestatus,"purchase/view?id=".$purchase->getId());?></td>
        <?php }else{?>
          <td bgcolor="<?php echo $bgcolor;?>"><?php echo $receivestatus;?></td>
        <?php }?>
      <!-----------STATUS--------->
      <?php
          $status=$purchase->getStatus();
          if($status=="Cancelled") $bgcolor=$red;
          else $bgcolor=$plain;
      ?>
      <td bgcolor="<?php echo $bgcolor;?>"><?php echo $status; ?></td>
      <td><?php echo link_to("View","purchase/view?id=".$purchase->getId()) ?></td>
<!--
      <td>
        <?php //if($purchase->getIsInspected()==0)echo "<font color=red>Not Inspected</font>" ?>
	      <?php //if($purchase->getIsInspected()==1)echo "<font color=green>Inspected</font>" ?>
      </td>
-->
    </tr>

<?php 
  $vouchers=$purchase->getVouchers();
  foreach($vouchers as $voucher){
    $out_payment=$voucher->getOutPayment(); 
    if($out_payment){$check=$out_payment->getOutCheck();
    if($check){
    ?>
    <tr bgcolor="#dd0">
    <td bgcolor="#eee"></td>
    <td bgcolor="#eee"></td>
    <td>Voucher<br> <?php echo link_to($check->getCode(),"voucher/view?id=".$voucher->getId())?></td>
    <td>Check No<br> <?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getPassbook()." ".$check->getCheckNo(); ?></td>
    <td>Amount<br> <?php echo MyDecimal::format($check->getAmount())?></td>
    <td colspan=7>
    Dated <?php echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?>
    <br>Released <?php echo MyDateTime::frommysql($check->getReceiveDate())->toshortdate()?>
    </tr>
<?php }}} ?>



    <?php }?>
  <?php }?>
</table>


