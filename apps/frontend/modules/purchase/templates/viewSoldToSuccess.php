<?php
$red="#faa";
$yellow="#ffc";
$green="#6d6";
$plain="#eee";
?>

<?php use_helper('I18N', 'Date') ?>

<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1><?php echo $purchase; ?>
</h1>
<?php slot('transaction_id', $purchase->getId()) ?>
<?php slot('transaction_type', "Purchase") ?>

<table>
  <tr valign=top>
    <td>PO No.</td>
    <td><?php echo $purchase; ?></td>
  </tr>
  <tr valign=top>
    <td>Date</td>
    <td><?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate() ?></td>
  </tr>
  <tr>
    <td>Sold To</td>
    <?php 
        $invoice_ids=$purchase->getInvoiceIds();
        $profit_details=$purchase->getProfitDetails();
        $sold_count=0;
        foreach($profit_details as $pd)
        {
          $sold_count+=$pd->getQty();
        }
        $purchase_details=$purchase->getPurchasedetail();
        $item_count=0;
        foreach($purchase_details as $pd)
        {
          $item_count+=$pd->getQty();
        }
        switch ($sold_count) {
          case 0: $bgcolor=$red;break;
          case $item_count: $bgcolor=$green;break;
          default: $bgcolor=$yellow;break;
        }
      ?>
      <td bgcolor="<?php echo $bgcolor;?>">
        <?php
          if($sold_count==$item_count)echo "SOLD OUT: ";
          else echo $sold_count."/".$item_count.": ";
          echo $purchase->getInvno().": "; 
          if($invoice_ids!=null)
          {
            $invnos=explode(",",$purchase->getInvnos());
            $invoice_ids=explode(",",$invoice_ids);
            for($i=0;$i<count($invnos);$i++) 
            {
              $inv=Fetcher::fetchOne("Invoice",array("id"=>$invoice_ids[$i]));
              echo link_to($invnos[$i]." (".$inv->getCustomer().")","invoice/view?id=".$invoice_ids[$i])."; ";
            }
          }

          //display links to invoices sold to
          $invoices=array();
          foreach($purchase->getPurchasedetail() as $detail)
          {
            $profitdetails=$detail->getProfitdetail();
            foreach($profitdetails as $pdetail)
            {
              $invoice=$pdetail->getInvoice();
              if(!isset($invoices[$invoice->getId()]))
                $invoices[$invoice->getId()]=$invoice;
            }
          }
          foreach($invoices as $invoice)
          {
            echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())." ";
          }
        ?>
        
      </td>
  </tr>
</table>
<br/>
<table border="1">
<tr>
    <td>Qty Unmatched</td>
    <td>Transaction<br>Date</td>
    <td>Delivery Date</td>
    <td>Status</td>
    <td>Stock In</td>
    <td>Stock Out</td>
    <td>Balance</td>
    <td>Ref / Actual Qty</td>
    <td>Price</td>
    <td>Type</td>
    <td>Customer / Supplier ; Notes</td>
    <td>Salesman</td>
    <td>Created By</td>
    <td></td>
  </tr>
  <?php 

  foreach($purchase_details as $pd){ 
  ?>
  <tr><td colspan="23"><h3><?php echo $pd->getProduct()->getName();?></h3></td></tr>
  <?php
    //add  stockentries from invoice details and purchase details
    $stockentries=array();
    $counter=0;//just something to make keys unique

    //foreach profit detail
    foreach($pd->getProfitdetail() as $pdetail)
    {
      //get invoice  detail
      $idetail=$pdetail->getInvoicedetail();
      //if none, do nothing
      if(!$idetail)continue;

      $idrs=$idetail->getInvoiceDrDetail();
      foreach($idrs as $idr)
      {
        if(!$idr){continue;}
        $se=$idr->getStockentry();
        if(!$se){continue;}
        $stockentries[$se->getDatetime()."-".$counter]=$se;
        $counter++;
      }
      // echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())." ";
      // $idrs=$invoice->getInvoiceDr
    }

    $pdrs=$pd->getPurchaseDrDetail();
    foreach($pdrs as $pdr)
    {
      if(!$pdr){continue;}
      $se=$pdr->getStockentry();
      if(!$se){continue;}
      $stockentries[$se->getDatetime()."-".$counter]=$se;
      $counter++;

    }

    ksort($stockentries);
    $stockentries=array_reverse($stockentries);
    foreach($stockentries as $key=>$se)
    {
      $is_conversion=false;
      $ref=$se->getRef();
      $parent=null;
      $parentref=null;
      $profitdetails=array();

      if($se->getRefClass()=="InvoiceDrDetail")
      {
        $dr=$ref->getInvoiceDr();
        // if(!$dr->isFirstDr())continue;
        $ref=$ref->getInvoicedetail();
      }
      else if($se->getRefClass()=="PurchaseDrDetail" )
      {
        $dr=$ref->getPurchaseDr();
        // if(!$dr->isFirstDr())continue;
        $ref=$ref->getPurchasedetail();
      }

    ?>
    <tr>
    <td>
      <?php 
        // echo $se->getId()."<br>";
        switch($se->getRefClass())
        {
          case "Invoicedetail":
          case "InvoiceDrDetail":
            if($ref and $ref->getRemaining()>0)// and $invoice->getStatus()!="Cancelled"
            {
              echo $ref->getRemaining();
              $invoice=$ref->getInvoice();
              echo '<button onClick={copy("'.$invoice->getId().'")}>Copy</button>';
            }
            break;
          case "Purchasedetail":
          case "PurchaseDrDetail":
              if($ref and $ref->getRemaining()>0)// and $purchase->getStatus()!="Cancelled"
            {
              echo $ref->getRemaining();
              $purchase=$ref->getPurchase();
              echo form_tag("stock/connectInvoiceAndPurchase"); ?>
              <input id=invoice_id name=invoice_id size=4>
              <input type=hidden id=purchase_id name=purchase_id value=<?php echo $purchase->getId() ?>  >
            </form>
              <?php
            }
            break;
        }
      ?>
    </td>
    <td>
      <?php 
        switch($se->getRefClass())
        {
          case "Invoicedetail":
          case "InvoiceDrDetail":
            if($ref)
            {
              $invoice=$ref->getInvoice();
              echo MyDateTime::frommysql($invoice->getDate())->toshortdate();
            }
            break;
          case "Purchasedetail":
          case "PurchaseDrDetail":
            if($ref)
            {
              $purchase=$ref->getPurchase();
              echo MyDateTime::frommysql($purchase->getDate())->toshortdate();
            }
            break;
          default: 
          // echo MyDateTime::fromdatetime($se->getDatetime())->toshortdate();
        }
      ?>
    </td>
    <td>
      <?php 
        switch($se->getRefClass())
        {
          case "InvoiceDrDetail":
            if($ref)
            {
              echo MyDateTime::frommysql($dr->getDatetime())->toshortdate();
            }
            break;
          case "PurchaseDrDetail":
            if($ref)
            {
              echo MyDateTime::frommysql($dr->getDatetime())->toshortdate();
            }
            break;
          default: 
          echo MyDateTime::fromdatetime($se->getDatetime())->toshortdate();
        }
      ?>
    </td>
    <td>
      <?php 
        switch($se->getRefClass())
        {
          case "Purchasedetail":
          case "PurchaseDrDetail":
            $purchase=$ref->getPurchase();
            if($purchase->isCancelled())echo "<font color=red>Cancelled</font>";
            break;
          case "Invoicedetail":
          case "InvoiceDrDetail":
            $invoice=$ref->getInvoice();
            if($invoice->isCancelled())echo "<font color=red>Cancelled</font>";
            else
          break;
        }
      ?>
    </td>
    <td align=right>
      <?php 
      if($se->getQty()>0)echo MyDecimal::format($se->getQty()); 
      
      //remove QR - no longer relevant now that DRS are here
      /*
      if($se->getRefClass()=="Purchasedetail")
      {
        if($purchase->getReceivedStatus()!="Fully Received"){?>
          <?php echo form_tag('purchase/receiveAll', array('style'=>'display:inline','onsubmit'=>"return confirm('Receive all items: Are you sure?');"))?>
          <input type=hidden id=id name=id value=<?php echo $purchase->getId()?>>
          <?php if($sf_user->hasCredential(array('admin'), false)){?><input type=submit name=submit id=submit value="QR"><?php } ?>
          </form>
          <?php } ?>
      <?php }*/ ?>
    </td>
    <td align=right><?php if($se->getQty()<0)echo MyDecimal::format($se->getQty()*-1) ?></td>
    <td align=right>
      <?php 
        if($se->getType()=="Report")
          echo $se->getQtyReported();
        else
          echo $se->getBalance();
      ?>
    </td>
    <td>
      <?php 
        switch($se->getRefClass())
        {
          case "Invoicedetail":
          case "InvoiceDrDetail":
            if($ref)
            {
              $invoice=$ref->getInvoice();
              echo link_to($invoice,"invoice/view?id=".$ref->getInvoiceId());
              //echo '<button onClick={copy("'.$invoice->getInvno().'")}>C</button>';
            }
            break;
          case "Purchasedetail":
          case "PurchaseDrDetail":
            if($ref)
            {
              $purchase=$ref->getPurchase();
              echo link_to($purchase,"purchase/view?id=".$ref->getPurchaseId());
              // echo '<button onClick={copy("'.$purchase->getPono().'")}>C</button>';
            }
            break;
          case "Deliverydetail":
            $parent=$ref->getParent(); 
            switch($parent->getClass())
            {
              case "Delivery": 
                echo link_to($parent,strtolower($parent->getClass())."/view?id=".$parent->getId());
                echo "<br>";
                $parentref= $parent->getRef(); 
                if($parentref)echo link_to($parentref,strtolower($parent->getRefClass())."/view?id=".$parentref->getId());
                break;
              case "Invoice": 
                $is_conversion=true;
                echo link_to($parent,"invoice/view?id=".$parent->getId());
                break;
              case "Purchase": 
                $is_conversion=true;
                echo link_to($parent,"purchase/view?id=".$parent->getId());
                break;
            }
            break;
          case "ReturnsDetail":
            echo link_to($ref->getReturns(),"returns/view?id=".$ref->getReturnsId());
            break;
          case "ReplacementDetail":
            echo link_to($ref->getReplacement(),"replacement/view?id=".$ref->getReplacementId());
            break;
          case "InvoiceDrDetail":
            echo link_to($ref->getInvoice(),"invoice/view?id=".$ref->getInvoiceId());
            break;
          case "PurchaseDrDetail":
            echo link_to($ref->getPurchase(),"purchase/view?id=".$ref->getPurchaseId());
            break;
          default:
            if($se->getType()=="Report")
              echo "Prev Balance ".$se->getBalance();
            break;
        }
      ?>
    </td>
    <td>
      <?php 
        switch($se->getType())
        {
          case "Invoice":
          case "Purchase":
          case "InvoiceDr":
          case "PurchaseDr":
            if(!$is_conversion)echo MyDecimal::format($ref->getUnittotal());
            break;
          case "Report":
            echo $se->getReportComment();
            break;
          /*
          default: 
            switch($se->getRefClass())
            {
              case "InvoiceDrDetail":
                echo $ref->getInvoicedetail()->getPrice();
                break;
              case "PurchaseDrDetail":
                echo $ref->getPurchasedetail()->getPrice();
                break;
            }
          */
        }
      ?>
    </td>
    <td><?php 
        switch($se->getRefClass())
        {
          /*
          case "InvoiceDrDetail":
            echo link_to($ref->getInvoiceDr(),"invoice_dr/view?id=".$ref->getInvoiceDrId());
            if($ref->getQty()<0)echo " <font color=red>RETURN</font>";
            break;
          case "PurchaseDrDetail":
            echo link_to($ref->getPurchaseDr(),"purchase_dr/view?id=".$ref->getPurchaseDrId());
            if($ref->getQty()<0)echo " <font color=red>RETURN</font>";
            break;
            */
          default:
            echo $se->getType(); if($is_conversion)echo " Conversion";
            // echo $se->getType()!=""?$se->getType():$se->getRefClass(); if($is_conversion)echo " Conversion";
        }
    
    ?></td>
    <td>
      <!--customer/supplier/notes-->
      <?php 
        switch($se->getRefClass())
        {
          case "ReplacementDetail":
            $r=$ref->getReplacement();
            if($r->getClientClass()=="Customer")
            {
              $customer=$r->getClient();
              echo link_to($customer,"customer/view?id=".$customer->getId());
            }
            elseif($r->getClientClass()=="Vendor")
            {
              $vendor=$r->getClient();
              echo link_to($vendor,"vendor/view?id=".$vendor->getId());
            }
            break;
          case "ReturnsDetail":
            $r=$ref->getReturns();
            if($r->getClientClass()=="Customer")
            {
              $customer=$r->getClient();
              echo link_to($customer,"customer/view?id=".$customer->getId());
            }
            elseif($r->getClientClass()=="Vendor")
            {
              $vendor=$r->getClient();
              echo link_to($vendor,"vendor/view?id=".$vendor->getId());
            }
            break;
          case "Deliverydetail":
            break;
          case "Purchasedetail":
          case "PurchaseDrDetail":
            $purchase=$ref->getPurchase();
            $vendor=$purchase->getVendor();
            echo "Supplier: ".link_to($vendor,"vendor/view?id=".$purchase->getVendorId())."; Sold to ".$purchase->getInvno().": "; 
            if($purchase->getMemo()!="")echo "; ".$purchase->getMemo();
            $profitdetails= $ref->getProfitdetail();
            /*
            if($se->getRefClass()=="PurchaseDrDetail")
            {
              $purchaseDr=$ref->getPurchaseDr();
              if($purchaseDr->getNotes()!="")echo "; ".$purchaseDr->getNotes();

              $profitdetails= $ref->getPurchasedetail()->getProfitdetail();
            }
            */
            //display links to invoices sold to
            foreach($profitdetails as $pdetail)
            {
              $invoice=$pdetail->getInvoice();
              echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())." ";
            }
            break;
          case "Invoicedetail":
          case "InvoiceDrDetail":
            $invoice=$ref->getInvoice();
            $customer=$invoice->getCustomer();
            echo "Customer: ".link_to($customer,"customer/view?id=".$customer->getId());

            if($invoice->getNotes()!="")echo "; ".$invoice->getNotes();
            /*
            if($se->getRefClass()=="InvoiceDrDetail")
            {
              $invoiceDr=$ref->getInvoiceDr();
              if($invoiceDr->getNotes()!="")echo "; ".$invoiceDr->getNotes();
            }
            */
            break;
          default:
            if(isset($ref))
            {
              $client=$ref->getRef()->getClient();
              echo link_to($client,strtolower(get_class($client->getRawValue()))."/view?id=".$client->getId());
            }
            echo $se->getDescription();
        }
      ?>
    </td>
    <td>
      <?php 
        switch($se->getRefClass())
        {
          case "Invoicedetail":
          case "InvoiceDrDetail":
            if($ref)echo $ref->getInvoice()->getEmployee();
            break;
          case "Purchasedetail":
          case "PurchaseDrDetail":
              if($ref)echo $ref->getPurchase()->getEmployee();
            break;
          case "Deliverydetail":
            $parent=$ref->getParent(); 
            switch($parent->getClass())
            {
              case "Invoice": 
                $is_conversion=true;
                echo $parent->getEmployee();
                break;
            }
            break;
        }
      ?>
    </td>
    <td>
      <?php
        $created_by=false; 
        if($se->getCreatedById())
          $created_by = Doctrine_Query::create()
          ->from('SfGuardUser s')
          ->where("s.id=".$se->getCreatedById())
          ->fetchOne();
        if($created_by)echo $created_by->getUsername(); 
      ?></td>
    <td>
    <?php 
      if($sf_user->hasCredential(array('admin'), false))if($se->getType()=="Adjustment" or $se->getType()=="Report")echo link_to('Delete','stockentry/delete?id='.$se->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>


    <?php 
      switch($se->getRefClass())
      {
        case "Purchasedetail":
        case "PurchaseDrDetail":
          $purchase=$ref->getPurchase();
          $files=$purchase->getFiles();
          if(count($files)>0)echo link_to(count($files)." File".(count($files)>1?"s":""),"purchase/files?id=".$ref->getPurchaseId());
          break;
      }
    ?>
    </td>

    <?php 
      $qtyarray=array(null,null,null,null,null,null,null,null,null);
      $colorarray=array(null,null,null,null,null,null,null,null,null);
      $fontcolorarray=array(null,null,null,null,null,null,null,null,null);
      $color="";
      $bold=false;
      $openbold="";
      $closebold="";
      switch($se->getRefClass())
      {
        case "Purchasedetail":
        case "PurchaseDrDetail":
          $bold=true;
          $openbold="<b>";
          $closebold="</b>";
          $qtyarray[$ref->getSlot()]=str_replace(".00","",$ref->getQty());
          $colorarray[$ref->getSlot()]=$ref->getColor();
          $fontcolorarray[$ref->getSlot()]=($ref->getColorIndex()>3)?"white":"black";
          break;
        case "Invoicedetail":
        case "InvoiceDrDetail":
          //there's really only one, but who cares
          $profitdetails= $ref->getProfitdetail();
          foreach($profitdetails as $pdetail)
          {
            $qtyarray[$pdetail->getSlot()]=link_to(str_replace(".00","",$pdetail->getQty()),"stock/disconnectInvoiceAndPurchase?profit_detail_id=".$pdetail->getId(), array('confirm' => 'Are you sure?'));
            $colorarray[$pdetail->getSlot()]=$pdetail->getColor();
            $fontcolorarray[$pdetail->getSlot()]=($pdetail->getColorIndex()>3)?"white":"black";
          }
          break;
      }
    ?>
    <?php if($qtyarray[1]!=null)echo "<td align=center bgcolor=".$colorarray[1]."><font color=".$fontcolorarray[1].">".$openbold.$qtyarray[1].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[2]!=null)echo "<td align=center bgcolor=".$colorarray[2]."><font color=".$fontcolorarray[2].">".$openbold.$qtyarray[2].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[3]!=null)echo "<td align=center bgcolor=".$colorarray[3]."><font color=".$fontcolorarray[3].">".$openbold.$qtyarray[3].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[4]!=null)echo "<td align=center bgcolor=".$colorarray[4]."><font color=".$fontcolorarray[4].">".$openbold.$qtyarray[4].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[5]!=null)echo "<td align=center bgcolor=".$colorarray[5]."><font color=".$fontcolorarray[5].">".$openbold.$qtyarray[5].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[6]!=null)echo "<td align=center bgcolor=".$colorarray[6]."><font color=".$fontcolorarray[6].">".$openbold.$qtyarray[6].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[7]!=null)echo "<td align=center bgcolor=".$colorarray[7]."><font color=".$fontcolorarray[7].">".$openbold.$qtyarray[7].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[8]!=null)echo "<td align=center bgcolor=".$colorarray[8]."><font color=".$fontcolorarray[8].">".$openbold.$qtyarray[8].$closebold."</font></td>";else echo "<td></td>";?>

    <td><?php //echo $se->getId(); ?></td>


  </tr>
  <?php }} ?>
</table>
