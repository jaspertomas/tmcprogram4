<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
// half short bond paper size
$pageLayout = array(216, 140); //  or array($height, $width) 
$pdf = new TCPDF("L", 'mm', $pageLayout, true, 'UTF-8', false);
// $pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name').' Billing Statement for '.$purchase->getName());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10, 10);

//set auto page breaks
//smaller footer margin
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================


// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
//$pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 90, '', 'PNG', '', '', false, 300, 'L', false, false, 0, false, false, false);
$pdf->SetFont('helvetica', '', 14, '', true);
//$pdf->write(0,"Tradewind Visayas Corporation",'',false,'R',true,0,false,false,0,0);

$widths=array(500);
$content=array(
  sfConfig::get('custom_company_header_text'),
  sfConfig::get('custom_company_phone'),
  sfConfig::get('custom_company_email'),
);

$tbl="";
$tbl .= <<<EOD
<table border=1>
 <tr>
  <td width="$widths[0]"><font size="18">$content[0]</font></td>
 </tr>
 <tr>
  <td width="$widths[0]">Phone: $content[1]</td>
 </tr>
</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');
 
//$pdf->write(0,"                                                                          ".sfConfig::get('custom_company_tin'),'',false,'L',true,0,false,false,0,0);

// $pdf->write(0,"                                                                                                     ".sfConfig::get('custom_company_phone'),'',false,'L',true,0,false,false,0,0);
// $pdf->write(0,"                                                                                                        ".sfConfig::get('custom_company_email'),'',false,'L',true,0,false,false,0,0);
// $pdf->write(0,"                                                                                                        ".sfConfig::get('custom_company_tin'),'',false,'L',true,0,false,false,0,0);

$pdf->SetFont('helvetica', '', 12, '', true);
$pdf->write(5,"\n");

//move barcode here under header so it doesn't get printed into last page
$style = array(
	'border' => false,
	'padding' => false,
	// 'fgcolor' => array(128,0,0),
	// 'bgcolor' => false
);

// QRCODE,Q : QR-CODE Better error correction
$filler=100-($purchase->getId()%100);
$pdf->write2DBarcode('pur;'.$purchase->getId().";".$filler.";".sfConfig::get('custom_server_code'), 'QRCODE,L', 170, 5, 23, 23, $style, 'N');

$tbl = "";

//-------------------
$client=$purchase->getVendor();

$templatename=$purchase->getPurchaseTemplate();

$terms="";
if($client->getTerms()>0)
  $terms=$client->getTerms()." Days";
  
$invoice=$purchase->getInvoice();
if($invoice)
{
  $invno=$purchase->getInvno();
  $customer=$purchase->getInvoice()->getCustomer();  
  $customername=$customer->getName();
  $customerphone=$customer->getPhone1();
  $invoicedate=MyDateTime::frommysql($invoice->getDate())->toshortdate();
}
else
{
  if($purchase->getIsStock())
  {
    $invno=" (Stock)";
    $customername='';
  }
  else
  {
    $invno="";
    $customername=$purchase->getInvno();
  }
  $customer="";  
  $customerphone="";
  $invoicedate="";
}
$content=array(
  $purchase->getName(),
  MyDateTime::frommysql($purchase->getDate())->toshortdatewithweek(),
  $client->getName(),
  $terms,
  $client->getPhone1(),
  $client->getEmail(),
  "",
  $invno,
  $purchase->getEmployee(),
  $customername,
  $customerphone,
  $purchase->getVendorInvoice(),
  );
  
  

$tbl .= <<<EOD
<table>
 <tr>
  <td align="left" width="20%">Date:</td>
  <td align="left" width="50%"><h2>$content[1]</h2></td>
  <td align="center" width="30%" ><h2>$content[6]</h2></td>
 </tr>
 <tr>
  <td align="left" width="20%">Supplier: </td>
  <td align="left" width="80%" ><h2>$content[2]</h2></td>
 </tr>
 <tr>
  <td align="left" width="20%">Supplier Invoice: </td>
  <td align="left" width="30%">$content[11]</td>
  <td align="left" width="20%">Sold To Invoice: </td>
  <td align="left" width="30%">$content[7]</td>
 </tr>
<!--
 <tr>
  <td align="left">Sold On: </td>
  <td align="left">$invoicedate</td>
 </tr>
-->
 <tr>
  <td align="left">Salesman: </td>
  <td align="left">$content[8]</td>
  <td align="left">Sold To: </td>
  <td align="left">$content[9]</td>
 </tr>
<!--
 <tr>
  <td align="left" width="20%">Terms: </td>
  <td align="left" width="20%">$content[3]</td>
 </tr>
 <tr>
  <td align="left"></td>
  <td align="left">$content[10]</td>
 </tr>
-->
</table>
EOD;




//$pdf->write(5,"\n");
//$pdf->write(0,"\t".$message,'',false,'L',true,0,false,false,0,0);

//====TABLE HEADER===========================

$widths=array(10,70,24,15,24,27);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

//title
$title="Purchase Order: &nbsp; &nbsp; ".$templatename." ".$purchase->getPono();
if($purchase->getPurchaseTemplate()->getIsReturn())$title="Backload";



//$pdf->SetFont('helvetica', '', 6, '', true);

$tbl .= <<<EOD
<hr>
<h2 align="center">$title</h2>
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Qty</b></td>
  <td width="$widths[1]" align="center"><b>Product</b></td>
  <td width="$widths[2]" align="center"><b>List Price</b></td>
  <td width="$widths[3]" align="center"><b>Less</b></td>
  <td width="$widths[4]" align="center"><b>Net Price</b></td>
  <td width="$widths[5]" align="center"><b>Total</b></td>
 </tr>
</thead>
EOD;

//===TABLE BODY============================
$height=1;
  $grandtotal=0;
	foreach($purchase->getPurchasedetails() as $detail)
  {
    $content=array(
      $detail->getQty(),
      $detail->getProduct(),
      MyDecimal::format($detail->getPrice()),
      $detail->getDiscrate(),
      MyDecimal::format($detail->getTotal()/$detail->getQty()),
      MyDecimal::format($detail->getTotal()),
      );
  
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center"><b>$content[0]</b></td>
    <td width="$widths[1]" ><b>$content[1]</b></td>
    <td width="$widths[2]" align="right"><b>$content[2]</b></td>
    <td width="$widths[3]" align="right"><b>$content[3]</b></td>
    <td width="$widths[4]" align="right"><b>$content[4]</b></td>
    <td width="$widths[5]" align="right"><b>$content[5]</b></td>
  </tr>
EOD;
  }    

//table footer
$content=array(
  "",
  "",
  "",
  "Total:",
  MyDecimal::format($purchase->getTotal()),
  );
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center">$content[0]</td>
    <td width="$widths[1]" >$content[1]</td>
    <td width="$widths[2]" align="right">$content[2]</td>
    <td width="$widths[3]" align="right"><h2>$content[3]</h2></td>
    <td align="right" colspan="2"><h2>$content[4]</h2></td>
  </tr>
EOD;

$content=array(
sfConfig::get('custom_cheques_payable_to'),
);
$tbl .= <<<EOD
</table>

<!--
<h4>Warranty</h4>
<ul>
<li>3 Months warranty against workmanship and weld
<li>Parts are not included in the warranty
<li>There is no warranty against Pressure above the recommended (40psi)
<li>There is no warranty against Vacuum
</ul>

<h4>Payment Scheme:</h4>
<ul>
<li>Payable within 90 days upon purchase of product
<li>Payee: $content[0]
</ul>
-->
EOD;

$tbl .= <<<EOD
<br>
<table>
<tr valign="bottom">
  <td align="center"><br><br>_______________</td>
  <td align="center"><br><br>_______________</td>
  <td align="center"><br><br>_______________</td>
  <td align="center"><br><br>_______________</td>
</tr>
<tr>
  <td align="center">Prepared By:</td>
  <td align="center">Approved By:</td>
  <td align="center">Received By:</td>
  <td align="center">Supplier Rep:</td>
 </tr>
</table>



EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

//-----------------


//--------------------------------------------------

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$templatenamestripped=str_replace(" ","",$templatename);
$pdf->Output($templatenamestripped.$purchase->getPono().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

