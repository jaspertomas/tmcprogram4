<?php use_helper('I18N', 'Date') ?>

<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1><?php echo $purchase->getPurchaseTemplate() ?> <?php echo $purchase->getPono() ?> (<?php echo $purchase->getReceivedStatus()?>) (<?php echo link_to("Back","purchase/view?id=".$purchase->getId());?>)</h1>
<?php slot('transaction_id', $purchase->getId()) ?>
<?php slot('transaction_type', "Purchase") ?>

<?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
<?php echo form_tag_for($form,'purchase/adjust')?> <?php echo $form['id'] ?>
<?php }?>	
<table>
  <tr valign=top>
    <td>
			<table>
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['date'] ?>
            <?php }?>	
          </td>
        </tr>
        <?php if($purchase->getReceivedStatus()!="Not Received"){?>
        <tr valign=top>
          <td>Date Received</td>
          <td><?php echo MyDateTime::frommysql($purchase->getDatereceived())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['datereceived'] ?>
            <?php }?>	
          </td>
        </tr>
        <?php } ?>
				<tr>
					<td>Vendor</td>
					<td><?php echo link_to($purchase->getVendor(),"vendor/view?id=".$purchase->getVendorId()) ?></td>
				</tr>
				<tr>
					<td>Vendor Invoice</td>
					<td><?php echo $purchase->getVendorInvoice(); echo $form['vendor_invoice'] ?></td>
				</tr>
			</table>
    </td>
    <td>
			<table>

				<tr>
					<td>Status</td>
					<td><?php if($purchase->isCancelled())echo "<font color=red>Cancelled</font>";else echo $purchase->getStatus() ?></td>
				</tr>
        <!--tr>
          <td>Related DRs:</td>
          <td>
            <?php /*foreach($purchase->getDeliverys() as $ref){?>
              <?php echo link_to($ref,"delivery/view?id=".$ref->getId())?>&nbsp;
            <?php } */?>
          </td>
        </tr-->
        <?php /*
				<tr>
					<td>Discount Rate</td>
					<td><?php echo $purchase->getDiscrate() ?></td>
				</tr>
				<tr>
					<td>Discount Amount</td>
					<td><?php echo $purchase->getDiscamt() ?></td>

				</tr>
        
        */?>
        <tr>
          <td>Total</td>
          <td><?php echo $purchase->getTotal() ?></td>
        </tr>
				<tr>
					<td>Balance</td>
					<td><?php echo $purchase->getBalance() ?></td>
				</tr>
        <tr valign=top>
          <td>Terms: </td>
          <td>
          <?php echo $purchase->getTerms() ?> 
          Days
        </td>
        </tr>
				
        <tr valign=top>
          <td>Due Date: </td>
          <td>
            <?php echo MyDateTime::frommysql($purchase->getDuedate())->toshortdate() ?>
          </td>
        </tr>
			</table>
    </td>
    <td>
			<table>
				<tr valign=top>
					<td>Notes</td>
					<td>
					  <?php echo $purchase->getMemo() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <?php echo $form['memo'] ?>
            <?php }?>	
				  </td>
				</tr>
				<tr>
					<td>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <input type=submit value=Save>
            <?php }?>	
					</td>				
				</tr>
			</table>
    </td>
  </tr>
</table>
</form>

<?php echo link_to("Recalculate","purchase/recalc?id=".$purchase->getId()); ?>

<hr>

<h2>Payments:</h2>
<table border=1>
  <tr>
    <td>Counter<br>Receipt</td>
    <td>Date</td>
    <td>Amount</td>
    <td>Status</td>
    <td>Notes</td>
  </tr>
  <?php foreach($purchase->getCounterReceiptDetail() as $detail){$cr=$detail->getCounterReceipt()?>
    <tr>
      <td><?php echo link_to($cr->getCode(),"counter_receipt/view?id=".$cr->getId())?></td>
      <td><?php echo MyDateTime::frommysql($cr->getDate())->toshortdate() ?></td>
      <td><?php echo $detail->getAmount() ?></td>
      <td><?php echo $cr->getStatus() ?></td>
      <td><?php echo $cr->getNotes() ?></td>
  <?php }?>
</table>


