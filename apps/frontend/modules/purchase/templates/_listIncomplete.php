<h2>Incomplete Purchase Orders </h2>
<table border=1>
  <tr>
    <td>Reference</td>
    <td>Salesman</td>
    <td>Item Description</td>
    <td>Supplier</td>
    <td>Sold to Invoice</td>
    <td>Counter Receipt</td>
    <td>Cheque</td>
  </tr>
  <?php foreach($purchases as $purchase){?>
  <tr>
    <td><?php echo link_to($purchase->getPurchaseTemplate()." ".$purchase->getPono(),"purchase/view?id=".$purchase->getId()) ?></td>
    <td><?php echo $purchase->getEmployee() ?></td>
    <td><?php echo $purchase->getParticularsString().($purchase->getCheque()?("; Check No.:".$purchase->getCheque().": ".$purchase->getChequedate()):"") ?></td>
    <td><?php echo $purchase->getVendor()// ?></td>
    <td><?php echo $purchase->getInvno()// ?></td>
    <td><?php $cr=$purchase->getCounterReceipt();if($cr!=null)echo link_to($cr->getCode(),"counter_receipt/view?id=".$cr->getId())?></td>
    <td><?php //if($cr!=null)echo $cr->getChequeNumber()?></td>
  </tr>
  <?php }?>
</table>


