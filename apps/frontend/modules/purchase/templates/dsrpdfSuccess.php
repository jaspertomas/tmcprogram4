<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("L", PDF_UNIT, "GOVERNMENTLEGAL", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Tradewind Mdsg Corp Daily Purchases Report');

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

$pdf->SetFont('dejavusans', '', 20, '', true);
$pdf->write(0,"Daily Purchase Report: ".MyDateTime::frommysql($form->getObject()->getDate())->toshortdatewithweek(),
'',false,'',true,0,false,false,0,0);
$pdf->SetFont('dejavusans', '', 8, '', true);
$pdf->write(0,"",'',false,'',true,0,false,false,0,0);

$contents=array();
/*
$contents[]=array(
          "Cash Purchases: ",
          MyDecimal::format($cashsales),
          " ",
          "Cheque Purchases: ",
          MyDecimal::format($chequesales),
          " ",
          "Credit Purchases: ",
          MyDecimal::format($creditsales),
          " ",
          "Total Purchases: ",
          MyDecimal::format($cashsales+$chequesales+$creditsales),
          );
$contents[]=array(
          "Other Cash: ",
          MyDecimal::format($cashother),
          " ",
          "Other Cheque: ",
          MyDecimal::format($chequeother),
          " ",
          "Other Credit: ",
          MyDecimal::format($creditother),
          " ",
          "Total Other: ",
          MyDecimal::format($cashother+$chequeother+$creditother),
          );
$contents[]=array(
          "Total Cash: ",
          MyDecimal::format($cashtotal),
          " ",
          "Total Cheque: ",
          MyDecimal::format($chequetotal),
          " ",
          "Total Credit: ",
          MyDecimal::format($credittotal),
          " ",
          "Total: ",
          MyDecimal::format($total),
          );
$contents[]=array(
          "Less Deductions: ",
          MyDecimal::format($deducttotal*-1),
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          );
$contents[]=array(
          "Total Cash: ",
          MyDecimal::format($cashtotal-$deducttotal),
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          );

$widths=array(30,30,20,30,30,20,30,30,20,30,30,);
$height=1;
foreach($contents as $content)
{
  $pdf->MultiCell($widths[0], $height, $content[0], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[8], $height, $content[8], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[9], $height, $content[9], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[10], $height, $content[10], 0, 'R', 0, 1, '', '', true);
}

//$pdf->write(5,"\n\n");

*/

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/


// Set some content to print
$txt = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
// Multicell test
// $widths=array(40,20,30,100,20,20,30,20,20);
$widths=array(50,20,40,100,20,20,30,20,20);
$height=5;

$content=array(
    'Supplier'
    ,'PO No.'
    ,'Sold To'
    ,'Item Description'
    ,'Total'
    ,'Salesman'
    ,'Status / Due'
    ,'Supplier Inv'
    //,'Cheque'
          );
$pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[1], $height, $content[1], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[2], $height, $content[2], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[3], $height, $content[3], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[4], $height, $content[4], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[5], $height, $content[5], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[6], $height, $content[6], 1, 'C', 0, 0, '', '', true);
// $pdf->MultiCell($widths[7], $height, $content[7], 1, 'C', 0, 0, '', '', true);
// $pdf->MultiCell($widths[8], $height, $content[8], 1, 'C', 0, 1, '', '', true);
$pdf->MultiCell($widths[7], $height, $content[7], 1, 'C', 0, 1, '', '', true);

foreach($templates as $template)
{
  $template_id=$template->getId();
  $pdf->SetFont('dejavusans', '', 14, '', true);
  $pdf->MultiCell(300, 0, PurchaseTemplateTable::fetch($template_id), 1, 'L', 0, 1, '', '', true);
  $pdf->SetFont('dejavusans', '', 8, '', true);
  foreach($purchases as $purchase)if($purchase->getTemplateId()==$template_id)
  {
      $cr=$purchase->getCounterReceipt();
			$particularsstring=$purchase->getParticularsString()?$purchase->getParticularsString():" ";

			$chequestring=$purchase->getCheque()>0?$purchase->getCheque():" ";
			//if($purchase->getCheque())$chequestring=implode("; ",array($chequestring,"Cheque no.: ".$purchase->getCheque().", ".MyDateTime::frommysql($purchase->getChequeDate())->toshortdate()));

      $inv=$purchase->getInvoice();
      if($inv)$inv=$inv->getInvno()." (".$inv->getCustomer().")";
      else $inv=$purchase->getInvno();
  
      $content=array(
       $purchase->getVendor(),//,
      $purchase->getPono(),
      $inv,
       $particularsstring,
      //($purchase->getCash()>0 and $invoice->getStatus()!="Cancelled")?$purchase->getCash():" ",
      //$purchase->getStatus()!="Cancelled"?$chequestring:" ",
      //($purchase->getCredit()>0 and $purchase->getStatus()!="Cancelled")?$purchase->getCredit():" ",
      ($purchase->getStatus()!="Cancelled")?$purchase->getTotal():" ",
       $purchase->getEmployee()?$purchase->getEmployee():" ",
       //($purchase->getStatus()=="Paid Check"?$purchase->getCheque():($purchase->getStatus()?$purchase->getStatus():" "))." / ".MyDateTime::frommysql($purchase->getDuedate())->toshortdate(),

      //status
       ($purchase->getStatus()=="Cancelled"?"Cancelled / ":" ").MyDateTime::frommysql($purchase->getDuedate())->toshortdate(),
       
       $purchase->getVendorInvoice(),
//       "under construction",        
      );
      $height=1;
      foreach($content as $index=>$txt) 
      {
        $numlines=$pdf->getNumLines($txt,$widths[$index],false,true,'','');
        if($height<$numlines)$height=$numlines;
      }
      $height*=4.5;
      $pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[1], $height, $content[1], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[2], $height, $content[2], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[3], $height, $content[3], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[4], $height, $content[4], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[5], $height, $content[5], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[6], $height, $content[6], 1, 'C', 0, 0, '', '', true,0,true);
      // $pdf->MultiCell($widths[7], $height, $content[7], 1, 'C', 0, 0, '', '', true,0,true);
      // $pdf->MultiCell($widths[8], $height, $content[8], 1, 'C', 0, 1, '', '', true,0,true);
      $pdf->MultiCell($widths[7], $height, $content[7], 1, 'C', 0, 1, '', '', true,0,true);
  }
}

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('DPR-'.MyDateTime::frommysql($form->getObject()->getDate())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

