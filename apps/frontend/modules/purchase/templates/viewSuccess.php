<?php
$red="#faa";
$yellow="#ffc";
$green="#6d6";
$plain="#eee";
?>

<?php use_helper('I18N', 'Date') ?>

<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1><?php echo $purchase; ?>  (<?php echo $purchase->receiveStatusString()?>, <?php echo $purchase->getIsTemporaryString()?>)
<?php 
  if($sf_user->hasCredential(array('admin', 'encoder', 'inventory','manager'),false))
  {
		if($purchase->getIsTemporary()==0)
			echo link_to("Undo Close","purchase/undoclose?id=".$purchase->getId()); 
  }
  if($sf_user->hasCredential(array('admin', 'encoder', 'inventory'),false))
  {
		if($purchase->getIsTemporary()!=0)
      echo link_to("Close","purchase/finalize?id=".$purchase->getId()); 
  }
?>
</h1>
<?php slot('transaction_id', $purchase->getId()) ?>
<?php slot('transaction_type', "Purchase") ?>

<?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
<?php echo form_tag_for($form,'purchase/adjust')?> <?php echo $form['id'] ?>
<?php }?>	
<table>
  <tr valign=top>
    <td>
			<table>
      <tr valign=top>
          <td>PO No.</td>
          <td><?php echo $purchase; ?></td>
        </tr>
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($purchase->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['date'] ?>
            <?php }?>	
          </td>
        </tr>
        <?php if(!$purchase->isDrBased()){?>
        <tr valign=top>
          <td>Date Received</td>
          <td><?php if($purchase->getDatereceived()!=null)echo MyDateTime::frommysql($purchase->getDatereceived())->toshortdate() ?></td>
        </tr>
        <?php } ?>
				<tr>
					<td>Vendor</td>
					<td><?php echo link_to($purchase->getVendor(),"vendor/view?id=".$purchase->getVendorId()) ?></td>
				</tr>
				<tr>
					<td>Vendor Invoice</td>
					<td><?php echo $purchase->getVendorInvoice(); if($purchase->getIsTemporary()!=0)echo $form['vendor_invoice'] ?></td>
				</tr>
				<tr>
					<td>Sales Person</td>
					<td><?php echo $purchase->getEmployee() ?></td>
				</tr>
				<tr>
					<td>Sold To</td>
          <?php 
        $invoice_ids=$purchase->getInvoiceIds();
        $profit_details=$purchase->getProfitDetails();
        $sold_count=0;
        foreach($profit_details as $pd)
        {
          $sold_count+=$pd->getQty();
        }
        $purchase_details=$purchase->getPurchasedetail();
        $item_count=0;
        foreach($purchase_details as $pd)
        {
          $item_count+=$pd->getQty();
        }
        switch ($sold_count) {
          case 0: $bgcolor=$red;break;
          case $item_count: $bgcolor=$green;break;
          default: $bgcolor=$yellow;break;
        }
      ?>
      <td bgcolor="<?php echo $bgcolor;?>">
        <?php
          if($sold_count==$item_count)echo "SOLD OUT: ";
          else echo $sold_count."/".$item_count.": ";
          echo $purchase->getInvno().": "; 
          if($invoice_ids!=null)
          {
            $invnos=explode(",",$purchase->getInvnos());
            $invoice_ids=explode(",",$invoice_ids);
            for($i=0;$i<count($invnos);$i++) 
            {
              $inv=Fetcher::fetchOne("Invoice",array("id"=>$invoice_ids[$i]));
              echo link_to($invnos[$i]." (".$inv->getCustomer().")","invoice/view?id=".$invoice_ids[$i])."; ";
            }
          }

          //display links to invoices sold to
          $invoices=array();
          foreach($purchase->getPurchasedetail() as $detail)
          {
            $profitdetails=$detail->getProfitdetail();
            foreach($profitdetails as $pdetail)
            {
              $invoice=$pdetail->getInvoice();
              if(!isset($invoices[$invoice->getId()]))
                $invoices[$invoice->getId()]=$invoice;
            }
          }
          foreach($invoices as $invoice)
          {
            echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())." ";
          }
        ?>
        
      </td>
			  </tr>
			</table>
    </td>
    <td>
			<table>

				<tr>
					<td>Status</td>
					<td>
					  <font color=
					    <?php 
    					  switch($purchase->getStatus())
    					  {
    					    case "Cancelled": echo "red"; break;
    					    case "Paid": echo "green"; break;
    					    case "Pending": echo "blue"; break;
    					    case "Overpaid": echo "red"; break;
    					  }
		  			  ?>>
  					<?php echo $purchase->getStatus() ?>
  					<?php 
              $cr=$purchase->getCounterReceipt();
              if($cr!=null)
                echo "; CR ".$cr->getStatus();
            ?>
					</font>
          </td>
				</tr>
        <!--tr>
          <td>Related DRs:</td>
          <td>
            <?php /*foreach($purchase->getDeliverys() as $ref){?>
              <?php echo link_to($ref,"delivery/view?id=".$ref->getId())?>&nbsp;
            <?php } */?>
          </td>
        </tr-->
				<tr>
					<td>Discount Rate</td>
					<td><?php echo $purchase->getDiscrate() ?></td>
				</tr>
				<tr>
					<td>Discount Amount</td>
					<td><?php echo $purchase->getDiscamt() ?></td>

				</tr>
        <tr>
          <td>Total</td>
          <td><?php echo MyDecimal::format($purchase->getTotal()) ?></td>
        </tr>
				<tr>
					<td>Balance</td>
					<td><?php echo MyDecimal::format($purchase->getBalance()) ?></td>
				</tr>
        <tr>
					<td colspan=2><?php echo link_to("View Payments","purchase/payments?id=".$purchase->getId());?></td>
				</tr>
			</table>
    </td>
    <td>
			<table>
        <tr valign=top>
          <td>Terms: </td>
          <td>
          <?php echo $purchase->getTerms() ?> 
          <?php if($purchase->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['terms'] ?>
          <?php }?>	
          Days
        </td>
        </tr>
				
        <tr valign=top>
          <td>Due Date: </td>
          <td>
            <?php echo MyDateTime::frommysql($purchase->getDuedate())->toshortdate() ?>
          </td>
        </tr>

        <tr>
          <td>Counter Receipt</td>				
          <td>
            <?php 
              $cr=$purchase->getCounterReceipt();
              if($cr)echo link_to($cr->getCode(),"counter_receipt/view?id=".$cr->getId());
            ?>
          </td>
        </tr>    

        <tr valign=top>
        <td>Vouchers </td>
          <td>
            <?php 
              $vouchers=$purchase->getVouchers();
              foreach($vouchers as $voucher)
                echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())." P".MyDecimal::format($voucher->getAmount()).($voucher->isCancelled()?" <font color=red>(Cancelled)</font>":"")."<br>";
              if($purchase->getStatus()=="Pending") echo link_to("Generate Check Voucher","voucher/new?purchase_id=".$purchase->getId());
              ?>
          </td>
        </tr>
				<tr>
					<td></td>				
				</tr>
				<tr valign=top>
					<td>Notes</td>
					<td>
					  <?php echo $purchase->getMemo() ?><br>
            <?php if($purchase->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <?php echo $form['memo'] ?>
            <?php }?>	
				  </td>
				</tr>
			</table>
      <?php if($purchase->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
        <input type=submit value=Save>
      <?php }?>	
    </td>
  </tr>
</table>
</form>

<?php if(!$purchase->isDrBased() and $purchase->getReceivedStatus()!="Fully Received"){?>
<?php echo form_tag_for($form,'purchase/receiveAll', array('style'=>'display:inline','onsubmit'=>"return confirm('Receive all items: Are you sure?');"))?>
<input type=hidden id=id name=id value=<?php echo $purchase->getId()?>>
<?php 
  $form->getObject()->setDatereceived(MyDate::today());
  $form=new PurchaseForm($form->getObject());
  echo $form['datereceived'];
?>
<input type=submit name=submit id=submit value="Receive All">
<?php if($sf_user->hasCredential(array('admin'), false)){?><input type=submit name=submit id=submit value="Quick Receive"><?php } ?>
</form>
<br>
<?php } ?>

<?php echo link_to("Edit","purchase/edit?id=".$purchase->getId()) ?> |
<?php echo link_to('Delete','purchase/delete?id='.$purchase->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
<?php echo link_to("Create Barcodes","purchase/barcode?id=".$purchase->getId()) ?> |
<?php echo link_to("View Files","purchase/files?id=".$purchase->getId());?> | 
<?php if($purchase->getIsTemporary()==0 and $purchase->getIsDrBased()==1 and $purchase->getStatus()!="Cancelled")echo link_to("Generate DR","purchase_dr/new?purchase_id=".$purchase->getId());?> |
 | <?php 
 
 #show print backload button only if this is a backload
$backload_template_id=SettingsTable::fetch("purchase_template_delivery_receipt_id");

if($purchase->getTemplateId()==$backload_template_id)
  echo link_to("Print Backload","purchase/viewBackloadPdf?id=".$purchase->getId());
else 
  echo link_to("Print","purchase/viewPdf?id=".$purchase->getId());
  //echo link_to("Print","purchase/viewPrintable?id=".$purchase->getId());
?>
 | <?php //echo link_to("View DRs","purchase/delivery?id=".$purchase->getId());?>


 | <?php //echo link_to("Create DR",'purchase/genDelivery?id='.$purchase->getId(),array('confirm' => 'Create delivery receipt: Are you sure?'));?>

<br>
<?php echo link_to("Recalculate","purchase/recalc?id=".$purchase->getId());?> |
<?php //echo link_to("Generate Return","returns/new?ref_id=".$purchase->getId()."&ref_class=Purchase");?> |

<?php echo link_to("View Sold To","purchase/viewSoldTo?id=".$purchase->getId());?> |
<?php echo link_to("Print Sold To","purchase/viewSoldToPdf?id=".$purchase->getId());?> |
<br>
<?php if($purchase->getIsTemporary()==0 and $purchase->getIsDrBased()==1 and $purchase->getStatus()!="Cancelled")echo link_to("Generate Transfer DR","transfer_dr/new?purchase_id=".$purchase->getId());?> |

<br>
Related DRs:
<?php foreach($purchase->getPurchaseDr() as $ref){?>
  <?php echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".link_to($ref,"purchase_dr/view?id=".$ref->getId());?> 
  <?php if($ref->isCancelled()){echo "<font color=red>(Cancelled)</font>";?>
  <?php } else { ?>
    <?php if($ref->getQtyForReceive()==0 and $ref->getQtyForReturn()==0)echo "(Empty)"?>
    <?php if($ref->isReceived())echo " ".MyDateTime::fromdatetime($ref->getDatetime())->toshortdate();?>
    <?php if($ref->getQtyForReceive()>0)echo "(".$ref->getQtyForReceive()." ".($ref->isReceived()?"Received":"Pending Receive").")"?>
    <?php if($ref->getQtyForReturn()>0)echo "(".$ref->getQtyForReturn()." ".($ref->isReceived()?"Returned":"Pending Return").")"?>
  <?php } ?>
<?php } ?>

<br>
Related Returns:
<?php foreach($purchase->getReturns() as $ref){?>
  <?php echo link_to($ref,"returns/view?id=".$ref->getId())?>&nbsp;
<?php } ?>
<br>
Related Replacements:
<?php foreach($purchase->getReplacement() as $ref){?>
  <?php echo link_to($ref,"replacement/view?id=".$ref->getId())?>&nbsp;
<?php } ?>

<hr>
<b>Images</b>
<?php foreach($purchase->getImages() as $image){?>  
  <br><a href="<?php echo $image->getUrl()?>" target="_blank">View Image <?php echo $image->getId()?></a>
<?php } ?>
<hr>

<!-------------------------------------------------------------------------------->

<?php
//if user is salesman or encoder
if($purchase->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){
?>
Search Product: <input id=purchaseproductsearchinput autocomplete="off">


<?php echo form_tag_for($detailform,"@purchasedetail",array("id"=>"new_purchase_detail_form")); ?>
<input type=hidden name=purchasedetail[purchase_id] value=<?php echo $purchase->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>
<table>
	<tr>
		<td>Qty</td>
		<td>Qty Received</td>
		<td>Product</td>
		<!--td>Discounted</td-->
		<td>Price</td>
		<td>Discount Rate</td>
		<td>Selling Price</td>
		<td>Notes</td>
	</tr>
	<tr>
		<td><?php echo $detailform["qty"]; ?></td>
		<td><?php echo $detailform["qty_received"]; ?></td>
		<td>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<!--td align=right><input type=checkbox name=purchasedetail[is_discounted] id=chk_is_discounted></td-->
		<td><span id=pricesfromvendor></span><?php echo $detailform["price"]; ?></td>
		<td><?php echo $detailform["discrate"]; ?></td>
		<td><?php echo $detailform["sellprice"]; ?></td>
		<td><?php echo $detailform["description"]; ?></td>
		<td><input type=submit name=submit id=purchase_detail_submit value=Save  ></form>
</td>
	</tr>
	<!--tr>
	  <td>Barcode: <?php //echo $detailform["barcode"]; ?></td>
	</tr-->
</table>

<?php  } ?>

<div id="purchasesearchresult"></div>


<!-------------------------------------------------------------------------------->
*For backload, please enter negative quantity

<table border=1>
  <tr>
    <td>Barcode</td>
    <td>Product</td>
    <td>Sell Price</td>
    <td>Description</td>
    <td>Qty</td>
    <td>Qty</td>
    <td>Unit</td>
    <td>Discount</td>
    <td>Total</td>
    <td>Price</td>
    <?php if($purchase->getIsTemporary()!=0){ ?>
      <td></td>
      <td></td>
      <td></td>
    <?php } ?>
    <td>Max buy</td>
    <td>Min buy</td>
    <td>Unreceived</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Received</td>
    <td>Price</td>
    <td></td>
    <td></td>
    <td>List</td>
    <?php if($purchase->getIsTemporary()!=0){ ?>
      <td></td>
      <td></td>
      <td></td>
    <?php } ?>
    <td>price</td>
    <td>price</td>
    <td>Qty</td>
  </tr>
  <?php foreach($purchase->getPurchasedetail() as $detail){?>
  <tr>
    <td><?php echo $detail->getBarcode() ?></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
    <td><?php echo $detail->getSellprice() ?></td>
    <td><?php echo $detail->getDescription() ?></td>
    <td>
      <?php echo $detail->getQty() ?>
      <?php if($purchase->getIsTemporary()!=0){ ?>
        <?php echo form_tag("purchasedetail/setQty");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=1 name=value value=<?php echo $detail->getQty()?>>
        </form>
      <?php } ?>
    </td>
    <td><?php echo $detail->getQtyReceived() ?></td>
    <td>
      <?php echo $detail->getPrice() ?>
      <?php if($purchase->getIsTemporary()!=0){ ?>
        <?php echo form_tag("purchasedetail/setPrice");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=3 name=value value=<?php echo $detail->getPrice()?>>
        </form>
      <?php } ?>
    </td>
    <td><?php 
            if($detail->getDiscrate())foreach(explode(" ",$detail->getDiscrate()) as $discamt) echo $discamt."%";
            if($detail->getDiscamt() and $detail->getDiscrate())echo "+";
            if($detail->getDiscamt())echo "P".$detail->getDiscamt() ?></td>
    <td>
      <?php echo $detail->getTotal() ?>
      <?php if($purchase->getIsTemporary()!=0){ ?>
        <?php echo form_tag("purchasedetail/setPriceByTotal");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input type=hidden id=qty name=qty value=<?php echo $detail->getQty()?>>
        <input id=value size=3 name=total value=<?php echo $detail->getTotal()?>>
        </form>
      <?php } ?>
    </td>
    <td><?php echo link_to("Price List","producttype/view?id=".$detail->getProduct()->getProducttypeId()) ?></td>

    <?php if($purchase->getIsTemporary()!=0){ ?>
      <td><?php echo link_to("Edit","purchasedetail/edit?id=".$detail->getId()) ?></td>
      <td>
        <?php echo link_to(
          'Delete',
          'purchasedetail/delete?id='.$detail->getId(),
          array('method' => 'delete', 'confirm' => 'Are you sure?')
        ) ?>
      </td>
      <td><?php echo link_to("Edit Product","product/edit?id=".$detail->getProductId()) ?></td>
    <?php } ?>

    <td><?php echo $detail->getProduct()->getMaxbuyprice() ?></td>
    <td><?php echo $detail->getProduct()->getMinbuyprice() ?></td>
    <td align=center>
      <?php 
        $remaining=$detail->getQty()-$detail->getQtyReceived();
        if($remaining!=0)echo "<font color=red>".$remaining."</font>";
      ?>
    </td>
  </tr>
  <?php }?>
</table>
<hr>
<?php echo link_to("View Payments","purchase/events?id=".$purchase->getId()) ?> | 
<?php echo link_to("Cash Payment","event/new?parent_class=Purchase&parent_id=".$purchase->getId()."&type=CashPay") ?> | 
<?php echo link_to("Cheque Payment","event/new?parent_class=Purchase&parent_id=".$purchase->getId()."&type=ChequePay") ?> 
<br><?php echo link_to("Cancel","purchase/cancel?id=".$purchase->getId()) ?>
<?php /*
<br><?php echo link_to("View Accounting","purchase/accounting?id=".$purchase->getId()) ?>
*/ ?>
<br><?php echo link_to("View Files","purchase/files?id=".$purchase->getId());?>

<!----------------conversion----------------->

<hr>
<h2>Possible Conversions</h2>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Conversion</td>
    <td>Previous<br>Conversions</td>
    <td>Convert</td>
    <td></td>
  </tr>
  <?php 
  	foreach($purchase->getPurchaseconversion() as $dc){?>
  <tr>
    <td><?php $possibleqty=$dc->getPossibleConversionQty($purchase->getPurchasedetail()); echo $possibleqty; ?></td>
    <td><?php echo $dc->getConversion()?></td>
    <td><?php $qty=$dc->getQty(); echo $qty; ?></td>
    <td><?php if($possibleqty>$qty) echo link_to("Convert","purchase/genConversionDr?purchaseconversion_id=".$dc->getId(), array('confirm' => 'This will generate a product conversion. Are you sure?')) ?></td>
  </tr>
  <?php }?>
</table>

<!---------------end conversion------------------>

<script type="text/javascript">
//for ajax
var baseurl="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/";

//set price textbox to read only
//$("#purchasedetail_price").prop('readonly', true);
//select purchno
$("#purchase_purchno").focus();
$("#purchase_purchno").select(); 
//if no product id set, disable save button
if($("#purchasedetail_product_id").val()=='')	 		  
  $("#purchase_detail_submit").prop("disabled",true);

//------Purchase (not header) product search-----------
//$("#purchaseproductsearchinput").keyup(function(){
//$("#purchaseproductsearchinput").on('input propertychange paste', function() {
$("#purchaseproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#purchase_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
//    if($("#purchaseproductsearchinput").val().length>=3)
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
	    $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+$("#purchaseproductsearchinput").val()+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	 		  $("#purchasesearchresult").html(result);
	    }});
    //else clear
    else
 		  $("#purchasesearchresult").html("");
});

//this is called when user clicks "Choose" button in product search form
function populateSubform(product_id,product_name,price,min_price,allow_zeroprice,curr_qty) {
  $("#product_min_price").val(min_price);
  $("#product_price").val(price);
  $("#product_name").html(product_name);
  
  $("#purchasedetail_price").val(min_price);
//  $("#purchasedetail_price").val(0);
//  $("#purchasedetail_with_commission").val(price);
  $("#productname").html(product_name);
  $("#purchasedetail_product_id").val(product_id);
  $("#purchasesearchresult").html("");

  $("#chk_is_discounted").removeAttr("disabled");
  $("#purchasedetail_description").removeAttr("disabled");
  $("#purchase_detail_submit").removeAttr("disabled");
  $("#purchasedetail_qty").removeAttr("disabled");
  $("#purchasedetail_price").removeAttr("disabled");
  $("#purchasedetail_with_commission").removeAttr("disabled");
  if(curr_qty!="0.00")alert("Current stock available: "+curr_qty.replace(".00", "")+" units");

  $("#purchasedetail_qty").focus();
  
  //display prices specific to vendor
  $.ajax({
    url: baseurl+"product/buyPriceFromVendor?id="+product_id+"&vendor_id=<?php echo $purchase->getVendorId()?>", 
    success: function(result)
    {
      var vendor="<?php echo $purchase->getVendor()?>";
      //$("#purchasedetail_price").val(result);
      $("#pricesfromvendor").html(vendor+" price: "+result);
    }
  });
}
</script>
