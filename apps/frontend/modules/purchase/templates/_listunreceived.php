<h2>Not Yet Arrived Purchase Orders </h2>
This only shows max 20 results, <?php echo link_to("click here to see more","purchase/listunreceived");?>
<table border=1>
  <tr>
    <td>Supplier</td>
    <td>Date</td>
    <td>Purchase</td>
    <td>Arrived</td>
    <td>Pending</td>
    <td>Notes</td>
    <td>Status</td>
  </tr>
  <?php foreach($unreceivedpurchases as $purchase){?>
  <?php 
    $particulars=$purchase->getReceivedParticularsString();
    $receivedparticularsstring=$particulars["received"];
    $unreceivedparticularsstring=$particulars["unreceived"];
  ?>
  <tr>
      <td><?php echo $purchase->getVendor() ?></td>
      <td><?php echo MyDateTime::frommysql($purchase->getDate())->toprettydate() ?></td>
      <td><?php echo link_to($purchase->getPurchaseTemplate()." ".$purchase->getPono(),"purchase/view?id=".$purchase->getId()) ?></td>
      <td><?php echo $receivedparticularsstring ?></td>
      <td><?php echo $unreceivedparticularsstring ?></td>
      <td><?php echo $purchase->getMemo() ?></td>
      <td><?php echo $purchase->getReceivedStatus() ?></td>
  </tr>
  <?php }?>
</table>

