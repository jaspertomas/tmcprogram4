<?php if($sf_user->hasCredential(array('admin'),false)){?>
<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_customer_id">
<div>
  <label for="purchase_is_dr_based"></label>
  <div class="content">
  <?php echo link_to("Set Is DR based to ".($form->getObject()->getIsDrBased()==1?"False":"True"),"purchase/toggleIsDrBased?id=".$form->getObject()->getId());?>
  </div>
</div>
</div>
<?php }
