<?php

require_once dirname(__FILE__).'/../lib/purchaseGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/purchaseGeneratorHelper.class.php';

/**
 * purchase actions.
 *
 * @package    sf_sandbox
 * @subpackage purchase
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class purchaseActions extends autoPurchaseActions
{
  public function executeDsr(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("purchase");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $purchase=new Purchase();
    if(!$day or !$month or !$year)
      $purchase->setDate(MyDate::today());
    else
      $purchase->setDate($year."-".$month."-".$day);

    $this->form=new PurchaseForm($purchase);
  
    $this->templates=Doctrine_Query::create()
        ->from('PurchaseTemplate pt')
      	->execute();
    $this->purchases = PurchaseTable::fetchByDate($purchase->getDate());
    $this->events = EventTable::fetchByDatenParentclass($purchase->getDate(),"Purchase");

      $this->cashsales=0;
      $this->chequesales=0;
      $this->creditsales=0;
      $this->cashother=0;
      $this->chequeother=0;
      $this->creditother=0;
      $this->cashtotal=0;
      $this->chequetotal=0;
      $this->credittotal=0;
      $this->deducttotal=0;
      foreach($this->purchases as $purchase)if($purchase->getStatus()!="Cancelled")
      {
        $this->cashsales+=$purchase->getCash();
        $this->chequesales+=$purchase->getCheque();
        $this->creditsales+=$purchase->getCredit();
        $this->cashtotal+=$purchase->getCash();
        $this->chequetotal+=$purchase->getCheque();
        $this->credittotal+=$purchase->getCredit();
        //$this->deducttotal+=$purchase->getDsrdeduction();
      }
      foreach($this->events as $event)
      {
        $purchase=$event->getParent();
        if($purchase->getStatus()!="Cancelled")
        {
          $this->cashother+=$event->getDetail("cashamt");
          $this->chequeother+=$event->getDetail("chequeamt");
          $this->creditother+=$event->getDetail("creditamt");
          $this->cashtotal+=$event->getDetail("cashamt");
          $this->chequetotal+=$event->getDetail("chequeamt");
          $this->credittotal+=$event->getDetail("creditamt");
          $this->deducttotal+=$event->getDetail3();
        }
      }
      $this->total=$this->cashtotal+$this->chequetotal+$this->credittotal;
  }
  public function executeDsrmulti(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("startdate"))
      $invoice->setDate($request->getParameter("startdate"));
    elseif(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $requestparams=$request->getParameter("purchase");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $purchase=new Purchase();
    if($request->hasParameter("enddate"))
      $purchase->setDate($request->getParameter("enddate"));
    elseif(!$day or !$month or !$year)
      $purchase->setDate(MyDate::today());
    else
      $purchase->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->toform=new PurchaseForm($purchase);
  
    $this->purchases = PurchaseTable::fetchByDateRange($invoice->getDate(),$purchase->getDate());
    $this->events = EventTable::fetchByDatenParentclass($purchase->getDate(),"Purchase");

    $this->productCategoryTotals=array();
    $this->totalPumps=0;
    $this->totalTanks=0;
    $this->totalSheets=0;
    $this->totalSupplies=0;
      foreach($this->purchases as $purchase)if($purchase->getStatus()!="Cancelled")
      {
        $this->total+=$purchase->getTotal();
        $totals=$purchase->getTotalForProductCategories();
        $this->productCategoryTotals[$purchase->getId()]=$totals;
        $this->totalPumps+=$totals[""];
        $this->totalTanks+=$totals["TJL"];
        $this->totalSheets+=$totals["Sheet"];
        $this->totalSupplies+=$totals["Supplies"];
      }
  }
  public function executeDsrmultiForVat(sfWebRequest $request)
  {
    $this->executeDsrmulti($request);
  }
  public function executeDsrpdf(sfWebRequest $request)
  {
    $this->executeDsr($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeDsrmultipdf(sfWebRequest $request)
  {
    $this->executeDsrmulti($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  function today() { 
    $today = getdate(); 
    return $today['year']."-".$today['mon']."-".$today['mday'];
    }

  public function executeViewSoldTo(sfWebRequest $request)
  {
    $this->forward404Unless($this->purchase=Doctrine_Query::create()
        ->from('Purchase i, i.Employee e, i.Vendor s')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne(), sprintf('Object purchase does not exist (%s).', $request->getParameter('id')));
    ; 
  }
  
  public function executeViewSoldToPdf(sfWebRequest $request)
  {
    $this->executeViewSoldTo($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }

  public function executeView(sfWebRequest $request)
  {
    $this->forward404Unless($this->purchase=Doctrine_Query::create()
        ->from('Purchase i, i.Employee e, i.Vendor s')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne(), sprintf('Object purchase does not exist (%s).', $request->getParameter('id')));
    ; 
    $this->form = $this->configuration->getForm($this->purchase);

    //allow set product id by url
    $detail=new Purchasedetail();
    $detail->setQty(1);
    if($request->getParameter("product_id"))
    {
      $detail->setProductId($request->getParameter("product_id"));

/*
      //new design decision: let price be 0 unless actual price is known

      //set price to min buy price
      //except if this purchase is a delivery receipt (no prices by default)
      //load setting: purchase template id with name "delivery receipt"
      //and compare with purchase template id
      $dr_purch_template_id=SettingsTable::fetch("purchase_template_delivery_receipt_id");
      if($this->purchase->getTemplateId()!=$dr_purch_template_id)
        $detail->setPrice($detail->getProduct()->getMinBuyPrice());
*/
    }
    $this->detailform = new PurchasedetailForm($detail); 
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->forward404Unless($this->purchase=Doctrine_Query::create()
        ->from('Purchase i, i.Employee e, i.Vendor s')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne(), sprintf('Object purchase does not exist (%s).', $request->getParameter('id')));
    ; 

    //validate: if some items have 0 price, do not allow to print
    $zeroqtyfound=false;
    $zeropricefound=false;
    foreach($this->purchase->getPurchasedetail() as $detail)
    {
      if($detail->getPrice()==0){$zeropricefound=true;break;}
      if($detail->getQty()==0){$zeroqtyfound=true;break;}
    }
    /*
    if($zeropricefound)
  	{
        $message="Cannot print Purchase Order with missing price";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
  	}
    if($zeroqtyfound)
  	{
        $message="Cannot print Purchase Order with zero qty";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
  	}
  	*/
  
    $this->executeView($request);
    $this->message=$request->getParameter("message");

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeAccounting(sfWebRequest $request)
  {
    $this->purchase=Doctrine_Query::create()
        ->from('Purchase i, i.Employee e, i.Vendor s')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne();
    $this->form = $this->configuration->getForm($this->purchase);

    $this->accountentries=$this->purchase->getAccountentries(true);
    $this->totalsbyaccount=array();

    foreach($this->purchase->getAccountids() as $id)
    {
      $this->totalsbyaccount[$id]=0;
    }
    foreach($this->accountentries as $entry)
    {
      $this->totalsbyaccount[$entry->getAccountId()]+=$entry->getQty();
    }
  }
  public function executeEvents(sfWebRequest $request)
  {
    $this->purchase=Doctrine_Query::create()
        ->from('Purchase i, i.Employee e, i.Vendor s')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne();
    $this->form = $this->configuration->getForm($this->purchase);
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->purchase = new Purchase();
    $this->purchase->setDate($this->today());
    $this->purchase->setIsTemporary(1);
    
    //if dr==1, this is a delivery receipt
    //autogenerate dr code
    if($request->getParameter("dr")==1)
    {
      $this->purchase->setTemplateId(SettingsTable::fetch("purchase_template_delivery_receipt_id"));
      $this->purchase->setPono(PurchaseTable::genBackLoadCode());
    }
    else
    {
      $this->purchase->setTemplateId(sfConfig::get('custom_default_po_template_id'));
      $this->purchase->genPono(); //generate po number according to template
    }

    //auto set employee
    $employee=Doctrine_Query::create()
    ->from('Employee e')
  	->where('e.username = "'.$this->getUser()->getUsername().'"')
  	->fetchOne();
  	if($employee)
	    $this->purchase->setEmployeeId($employee->getId());
  	else
    		$this->purchase->setEmployeeId(2);//default is jonathan

    if($request->getParameter("pono"))
      $this->purchase->setPono($request->getParameter("pono"));
    //set vendor if param vendor_id
    if($request->hasParameter("vendor_id"))
      $this->purchase->setVendorId($request->getParameter("vendor_id"));
    $this->form = $this->configuration->getForm($this->purchase);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());
    $isnew=$form->getObject()->isNew();

    #if pono is blank, autogenerate pono
    if($requestparams["pono"]=="")
    {
      //fetch dr / backload template id
      $delivery_receipt_template_id=SettingsTable::fetch("purchase_template_delivery_receipt_id");
      //if this is a delivery receipt / backload
      //and template is backload,
      //autogenerate backload code
      if($requestparams["template_id"]==$delivery_receipt_id)
  	    $requestparams["pono"]=PurchaseTable::genBackLoadCode();
	    //else autogenerate next available po number
      else
  	    $requestparams["pono"]=PurchaseTable::genPurchaseOrderCode();
    }
    else if($isnew)
    {
      $duplicate=Fetcher::fetchOne("Purchase",array("pono"=>"\"".$requestparams["pono"]."\""));
      if($duplicate)
      {
        return $this->redirect('purchase/duplicateError?id='.$duplicate->getId());
      }
    }

    //either invno should contain a value or is_stock should be checked
    if(trim($requestparams["invno"])=="" and !isset($requestparams["is_stock"]))
  	{
  		//either invno or check box must be set 
        $message="Please enter 'Sold To' or check 'is stock' checkbox";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
  	}
  	
    $form->bind($requestparams,$request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        //set date and adjust stock entry date if necessary
        //only if purchase is not new (action is edit)
        // if(!$isnew)$form->getObject()->setDateAndUpdateStockEntry($requestparams["date"]["year"]."-".$requestparams["date"]["month"]."-".$requestparams["date"]["day"]);

        $purchase = $form->save();
        $purchase->calc();
        $purchase->setTerms($purchase->getVendor()->getTerms());
        $purchase->calcDueDate();
        $purchase->getUpdateChequedata();
        $purchase->save();
        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();


        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $purchase)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@purchase_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect('purchase/view?id='.$purchase->getId());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeSearch(sfWebRequest $request)
  {
    $this->searchstring=$request->getParameter("searchstring");

    //no search string, do nothing
  	if(trim($this->searchstring)=="")
  	{
      return $this->redirect("home/error?msg=Cannot search purchases: Please enter a search string");
  	}

    $query=Doctrine_Query::create()
        ->from('Purchase i')
      	->where('i.pono = "'.trim($request->getParameter("searchstring")).'"')
      	->orWhere('i.pono          like "%'.trim($request->getParameter("searchstring")).'%"')
      	->orWhere('i.invnos          like "%'.trim($request->getParameter("searchstring")).'%"')
      	->orWhere('i.vendor_invoice like "%'.trim($request->getParameter("searchstring")).'%"')
      	->orWhere('i.memo           like "%'.trim($request->getParameter("searchstring")).'%"')
      	->orderBy('i.id desc')
      	;
    $this->purchases=$query->execute();

    /*  	
    //if there is only 1 result, show page
  	if(count($this->purchases)==1)
  	{
      $this->purchase=$this->purchases[0];
      $this->redirect("purchase/view?id=".$this->purchase->getId());
  	}
  	elseif(count($this->purchases)>1)
  	*/
  	
  	//if there are any results, even just 1, display as list
  	//so that if it's not the one you want, you can create new 
  	if(count($this->purchases)>0)
  	{
  	  //nothing left to do, just let the page be displayed
  	}
  	//no search result found
  	else
  		$this->redirect("purchase/new?pono=".$request->getParameter("searchstring"));


  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('purchase');
    
    $date=
      str_pad($requestparams["date"]["year"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["date"]["month"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["date"]["day"], 2, "0", STR_PAD_LEFT)
      ;

    //validate date received
    $datereceivedvalid=true;
    if(
      $requestparams["datereceived"]["year"]=="" or
      $requestparams["datereceived"]["month"]=="" or
      $requestparams["datereceived"]["day"]==""
    )
      $datereceivedvalid=false;

    $datereceived=
      str_pad($requestparams["datereceived"]["year"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["datereceived"]["month"], 2, "0", STR_PAD_LEFT)
      ."-".
      str_pad($requestparams["datereceived"]["day"], 2, "0", STR_PAD_LEFT)
      ;

    $this->forward404Unless($purchase = Doctrine::getTable('Purchase')->find(array($requestparams['id'])), sprintf('Purchase with id (%s) not found.', $request->getParameter('id')));

    //if date changed but date received not changed, 
    //if date and date received are previously the same,
    //make date received equal to date
    if($purchase->getDate()!=$date)
    if($purchase->getDatereceived()==$datereceived)
    if($purchase->getDatereceived()==$purchase->getDate())
    {
      $datereceived=$date;
    }
    
    //$purchase->setStatus($requestparams['status']);
    $purchase->setDate($date);
    $purchase->setCollectionStatus($requestparams['collection_status']);
    $purchase->setMemo($requestparams['memo']);
    $purchase->setTerms($requestparams['terms']);
    $purchase->setVendorInvoice($requestparams['vendor_invoice']);
    $purchase->calcDueDate();

    //disabled - purchase no longer has its own stock entries
    //set date and adjust stock entry date if necessary
    // if($datereceivedvalid)
    //   $purchase->setDateAndUpdateStockEntry($datereceived);

    $purchase->save();

    $this->redirect($request->getReferer());
  }
  public function executeBarcode(sfWebRequest $request)
  {
    $this->purchase = $this->getRoute()->getObject();

    $this->details=$this->purchase->getPurchasedetail();

    $this->start=1;

  }
  public function executeBarcodepdf(sfWebRequest $request)
  {
    
    $this->start=$request->getParameter("start");
    $qty=$request->getParameter("qty");

    $this->purchase_id=$request->getParameter("id");
    $this->purchase = Doctrine_Query::create()
      ->from('Purchase p')
      ->where('id = '.$this->purchase_id)
      ->fetchOne();
    $details=$this->purchase->getPurchasedetail();


    $this->qty=array();
    $this->totalqty=0;
    $this->details=array();
    //create qty / detail arrays, omit 0 qty items 
    foreach($qty as $index=>$q)
    {
      if($q!=0)
      {
        $this->totalqty+=$qty[$index];
        $this->qty[]=$qty[$index];
        $this->details[]=$details[$index];
      }
    }

    //create array of products
    $this->products=array();
    foreach($this->details as $detail)
    {
      $this->products[]=$detail->getProduct();
    }


    $this->start--;
    //$this->end=$this->start+$this->qty;


    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
    public function executeInspect(sfWebRequest $request)
    {
        $this->purchase=Doctrine_Query::create()
        ->from('Purchase i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne();
        
        $value=$request->getParameter("value");
        
        $this->purchase->setIsInspected($value);
        $this->purchase->save();
        
        //update purchase details
        Doctrine_Query::create()
          ->update('Purchasedetail d')
          ->set('d.is_inspected',$value)
          ->where('d.purchase_id = '.$this->purchase->getId())
          ->execute();
        
        //update counter receipt detail if it exists
        if($this->purchase->getCounterReceiptDetailId()!=null and $value==1)
          Doctrine_Query::create()
            ->update('CounterReceiptDetail d')
            ->set('is_inspected',$value)
            ->set('amount','"'.$this->purchase->getTotal().'"')
            ->where('d.purchase_id = '.$this->purchase->getId())
            ->execute();
        
        $this->redirect($request->getReferer());
    }
    public function executeFinalize(sfWebRequest $request)
    {
      $this->forward404Unless(
        $this->purchase=Doctrine_Query::create()
        ->from('Purchase i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
      , sprintf('Purchase with id (%s) not found.', $request->getParameter('id')));
        
      //validate purchase is not cancelled
      if($this->purchase->getStatus()=="Cancelled")
      {
          $message=$this->purchase." cannot be finalized because it is already cancelled.";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
      }

      $this->purchase->setIsTemporary(0);
      $this->purchase->setWasClosed(1);
      $this->purchase->calc();
      $this->purchase->save();
        
      if($this->purchase->isDrBased())//should be always true
      {
        /*
on purchase close 
  if no unreleased exists, stay in purchase page
  if unreleased exists
      if no dr, generate
          all qtys = unreleased
      if unreleased dr exist, update
      if no unreleased dr, generate
if returns exist, show as negative
          */
        //DR is required if remaining != 0
        //remaining = qty != qty_released
        //at first finalize, qty_released==0
        //and remaining = qty
        if($this->purchase->checkDrRequired())
        {
          //generate a dr and show it for releasing
          $purchase_dr = PurchaseDrTable::genDrForPurchaseId($this->purchase->getId(),$this->getUser()->getGuardUser());
          return $this->redirect("purchase_dr/view?id=".$purchase_dr->getId());
        }
        //else if dr not required
        else
        {
          return $this->redirect($request->getReferer());
        }
      }
      return $this->redirect($request->getReferer());
  }
  public function executeUndoclose(sfWebRequest $request)
  {
      $this->forward404Unless(
        $this->purchase=Doctrine_Query::create()
        ->from('Purchase i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
      , sprintf('Purchase with id (%s) not found.', $request->getParameter('id')));

      //for non dr based purchases, stock entries are attached to purchasedetail
      //delete them whether dr based or not
      foreach($this->purchase->getPurchasedetail() as $detail)
      {
        $detail->deleteStockentry();
        if(!$this->purchase->isDrBased())
        {
          $detail->setQtyReceived(0);
          $detail->save();
        }
      }

      $this->purchase->setIsDrBased(1);
      $this->purchase->setIsTemporary(1);
      // $this->purchase->calc();
      $this->purchase->save();
      
      /*
      //update generate conversion stock entries
      foreach($this->purchase->getDeliveryconversion() as $detail)
      {
        $detail->setQty(0);
        $detail->save();
        $detail->cascadeUpdateDeliverydetailQtys();
      }
      */

      $this->redirect($request->getReferer());
  }
  public function executeListunpaid(sfWebRequest $request)
  {
    $this->purchases=Doctrine_Query::create()
        ->from('Purchase p')
      	->where('status!="Paid"')
      	->andWhere('status!="Cancelled"')
      	->orderBy('p.vendor_id, p.date')
      	//->limit(10)
      	->execute();

  	//put vendors into an array, sorted by id
  	$this->vendors=array();
  	$this->vendorpurchases=array();
  	$this->vendortotals=array();
  	$this->vendorduetotals=array();
  	foreach($this->purchases as $purchase)
  	{
  	  $this->vendors[$purchase->getVendorId()]=$purchase->getVendor();
    	//put purchases into a 2d array under the same id as the vendor
      if(!isset($this->vendorpurchases[$purchase->getVendorId()]))
      {
    	  $this->vendorpurchases[$purchase->getVendorId()]=array();
      	$this->vendortotals[$purchase->getVendorId()]=0;
      	$this->vendorduetotals[$purchase->getVendorId()]=0;
      }
  	  $this->vendorpurchases[$purchase->getVendorId()][]=$purchase;
    	$this->vendortotals[$purchase->getVendorId()]+=$purchase->getBalance();
    	if($purchase->isDue())
      	$this->vendorduetotals[$purchase->getVendorId()]+=$purchase->getBalance();
  	}
  }
  public function executeListunreceived(sfWebRequest $request)
  {
    //page system
    $itemsperpage=50;
    $item_class="Purchase";
    $this->pagepath="purchase/listunreceived";
    $page=$request->getParameter("page");
    if($page==null or $page<1)$page=1;
    $this->page=$page;
    $offset=$itemsperpage*($page-1);
    $itemcount=Doctrine_Query::create()
        ->from($item_class.' p')
      	->where('received_status!="Fully Received"')
      	->andWhere('status!="Cancelled"')
      	->count();
    $pagecount=$itemcount/$itemsperpage;
    $pagecount=ceil($pagecount);
    $this->pagecount=$pagecount;
    
    $this->purchases=Doctrine_Query::create()
        ->from('Purchase p')
      	->where('received_status!="Fully Received"')
      	->andWhere('status!="Cancelled"')
      	->orderBy('p.vendor_id desc, p.date desc')
      	->offset($offset)
      	->limit($itemsperpage)
      	->execute();

  	//put vendors into an array, sorted by id
  	$this->vendors=array();
  	$this->vendorpurchases=array();
  	foreach($this->purchases as $purchase)
  	{
      if(!isset($this->vendors[$purchase->getVendorId()]))
    	  $this->vendors[$purchase->getVendorId()]=$purchase->getVendor();
    	//put purchases into a 2d array under the same id as the vendor
  	  $this->vendorpurchases[$purchase->getVendorId()][]=$purchase;
  	}
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    $purchase=$this->getRoute()->getObject();
    
    $returncount=count($purchase->getReturns());
    if($returncount>0)
    {
      $message="Cannot delete this Purchase because Returns exist, please delete Returns first.";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    if ($purchase->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('home/index');
  }
  public function executeAdjustCollectionStatus(sfWebRequest $request)
  {
    $purchase=Doctrine_Query::create()
        ->from('Purchase i')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne();
  	$purchase->setCollectionStatus($request->getParameter('collection_status'));
  	$purchase->save();
  	echo "<font color=".$purchase->getColorForIsDueString($request->getParameter('collection_status')).">".$request->getParameter('collection_status')."</font>";
  	die();
  }
  public function executeCancel(sfWebRequest $request)
  {
    $this->purchase= $this->getRoute()->getObject();
    
    if(count($this->purchase->getCounterReceiptDetail())>0)
    {
      $message="Cannot cancel this PO, because counter receipt entries exist.";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }
    
    /*
    //if drs exist for this purchase
    $deliverys=Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.ref_class = "purchase"')
      ->andWhere('d.ref_id = '.$this->purchase->getId())
      ->andWhere('d.status != "Cancelled"')
    	->execute()
      ;
    if(count($deliverys)>0)
    {
      $this->getUser()->setFlash('error', "Purchase not cancelled: Please cancel delivery receipts first");
      return $this->redirect('purchase/delivery?id='.$this->purchase->getId());
    }
    */
    
    $this->purchase->cascadeCancel();
    $this->purchase->calc();

    $this->redirect("purchase/view?id=".$this->purchase->getId());
  }
  public function executeFiles(sfWebRequest $request)
  {
    $this->purchase=MyModel::fetchOne("Purchase",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('purchase');
    $this->file->setParentId($this->purchase->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="purchase"')
      ->andWhere('f.parent_id='.$this->purchase->getId())
      ->execute();
  }
  public function executeGenDelivery(sfWebRequest $request)
  {
    $purchase=MyModel::fetchOne("Purchase",array('id'=>$request->getParameter("id")));
    
    $delivery=$purchase->genDelivery();    

    //set date received to delivery date
    //this will also hide "receive now" link in view    
    $purchase->setDatereceived($delivery->getDate());
    $purchase->save();
   
    $this->getUser()->setFlash('notice', "Successfully generated delivery receipt");
    $this->redirect('delivery/view?id='.$delivery->getId());
    
  }
  public function executeDelivery(sfWebRequest $request)
  {
    $this->purchase=MyModel::fetchOne("Purchase",array('id'=>$request->getParameter("id")));
    
    $this->deliverys=$this->purchase->getDeliverys();
  }
  public function executeGenConversionDr(sfWebRequest $request)
  {
    $pc=Doctrine_Query::create()
      ->from('Purchaseconversion pc')
      ->where('pc.id = '.$request->getParameter('purchaseconversion_id'))
      ->fetchOne();   
    $this->forward404Unless($pc, sprintf('PurchaseConversion with id (%s) not found.', $request->getParameter('purchaseconversion_id')));
    
    $delivery=$pc->genConversionDr();    

    $this->getUser()->setFlash('notice', "Successfully generated conversion");
    $this->redirect('delivery/view?id='.$delivery->getId());
  }

  public function executeViewBackloadPdf(sfWebRequest $request)
  {
    $this->executeView($request);
    $this->message=$request->getParameter("message");

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  // list "incompete" purchase orders, meaning sold to is not set
  public function executeList(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("purchase");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];

    $year=$requestparams["date"]["year"];
    $purchase=new Purchase();
    if(!$day or !$month or !$year)
      $purchase->setDate(MyDate::today());
    else
      $purchase->setDate($year."-".$month."-".$day);

    $this->form=new PurchaseForm($purchase);
  
    $this->purchases=Doctrine_Query::create()
      ->from('Purchase p')
    	->where('p.invno = ""')
    	->andWhere('p.date = "'.$purchase->getDate().'"')
    	->execute();
  }
  public function executeReceiveAll(sfWebRequest $request)
  {
      $requestparams=$request->getParameter("purchase");
      $purchase=MyModel::fetchOne("Purchase",array('id'=>$request->getParameter("id")));
      
      if($request->getParameter("submit")=="Quick Receive" || $request->getParameter("submit")=="QR")
        $datereceived=$purchase->getDate();
      else
      {
        if(
          $requestparams["datereceived"]["year"]=="" or
          $requestparams["datereceived"]["month"]=="" or
          $requestparams["datereceived"]["day"]==""
        )
        {
          $message="Invalid Receive Date";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
        $datereceived=
          str_pad($requestparams["datereceived"]["year"], 2, "0", STR_PAD_LEFT)
          ."-".
          str_pad($requestparams["datereceived"]["month"], 2, "0", STR_PAD_LEFT)
          ."-".
          str_pad($requestparams["datereceived"]["day"], 2, "0", STR_PAD_LEFT)
          ;
      }
      $purchase->receiveAll($datereceived);

      $this->redirect($request->getReferer());
  }
  public function executeViewPrintable(sfWebRequest $request)
  {
    /*
    $this->forward404Unless(
      $purchase=Doctrine_Query::create()
        ->from('Purchase p, p.Employee e, p.Vendor v, p.Purchasedetail pd')
      	->where('p.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Purchase with id (%s) not found.', $request->getParameter('id')));
    */
    $purchase=Fetcher::fetchOne("Purchase",array("id"=>$request->getParameter('id')));

    $content=MyPurchaseHelper::PrintablePurchaseBig($purchase);

    $printtoscreen=false;
    if($printtoscreen)
    {
      $content=str_replace("\n","<br>",$content);
      $content=str_replace(" ","&nbsp;",$content);
      echo $content;
      die();
    }

    $content=bin2hex($content);
      
    $response = $this->getResponse();
    $response->clearHttpHeaders();
    //$response->setContentType($mimeType);
    $response->setHttpHeader('Content-Disposition', 'attachment; filename="' . basename(str_replace(" ","",$purchase->getPurchaseTemplate()).$purchase->getPono().".dmf") . '"');
    $response->setHttpHeader('Content-Description', 'File Transfer');
    $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
    $response->setHttpHeader('Content-Length', 80*39);
    $response->setHttpHeader('Cache-Control', 'public, must-revalidate');
    $response->setHttpHeader('Pragma', 'public');
    $response->setContent($content);
    $response->sendHttpHeaders();

    sfConfig::set('sf_web_debug', false);
    return sfView::NONE;
  }
  public function executeRecalc(sfWebRequest $request)
  {
    $purchase=$this->getRoute()->getObject();
    $purchase->calc();
    $purchase->save();
    foreach($purchase->getPurchaseDr() as $dr)
      $dr->calc();
    foreach($purchase->getPurchasedetail() as $detail)
      $detail->calcDr();
    $this->redirect($request->getReferer());
  }
  public function executePayments(sfWebRequest $request)
  {
    $this->purchase=$this->getRoute()->getObject();
    $this->form=new PurchaseForm($this->purchase);
  }
  public function executeResetReceiveDate(sfWebRequest $request)
  {

  	die("ACCESS DENIED");

    //auto detect first occurence requiring fix
    $this->first = Doctrine_Query::create()
    ->from('Purchase p')
    ->where('received_status="Fully Received" and (datereceived is null or datereceived="0000-00-00")')
    ->fetchOne();

      //default values
    $this->interval=100;
    if($this->first!=null)
      $this->start=$this->first->getId()-1;
    else
      $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->purchases=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->purchases = Doctrine_Query::create()
      ->from('Purchase p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->andWhere('received_status="Fully Received" and (datereceived is null or datereceived="0000-00-00")')
      ->execute();

    foreach($this->purchases as $p)
    {
      $p->setDateAndUpdateStockEntry($p->getDate());
    }

     $this->start=$this->end+1;
  }
  public function executeDoublePaymentCheck(sfWebRequest $request)
  {
  	// die("ACCESS DENIED");

      //default values
    $this->interval=100;
    $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->purchases=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->purchases = Doctrine_Query::create()
      ->from('Purchase p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();

    foreach($this->purchases as $p)
    {
      $cr=$p->getCounterReceipt();
      $vouchers=$p->getVouchers();
      if($cr!=null and count($vouchers)!=0)
      {
        echo "double payment detected: po ".$p->getPono();
        die();
      }
    }

     $this->start=$this->end+1;
  }
  public function executeDrDiagnostics(sfWebRequest $request)
  {
  	// die("ACCESS DENIED");

      //default values
    $this->interval=100;
    $this->start=21000;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->purchases=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->purchases = Doctrine_Query::create()
      ->from('Purchase p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();

    foreach($this->purchases as $p)
    {
      foreach($p->getPurchasedetail() as $detail)
      {
        $received=0;
        foreach($detail->getPurchaseDrDetail() as $drdetail)
        {
          if($drdetail->isReceived())
            $received+=$drdetail->getQty();
        }
        if($received>$detail->getQty())
        {
            echo "DR Calculation Error detected: "
              ."<br>".$p
              ."<br>PO ID ".$p->getId()
              ."<br>Released Qty: ".$released
              ."<br>Purchase Qty: ".$detail->getQty()
              ."<br>Product: ".$detail->getProduct()->getName()
              ;
          die();
        }
      }
    }
     $this->start=$this->end+1;
  }
  public function executeToggleIsDrBased(sfWebRequest $request)
  {
    $this->forward404Unless(
      $this->purchase=Doctrine_Query::create()
        ->from('Purchase i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
    , sprintf('Purchase with id (%s) not found.', $request->getParameter('id')));

    if($this->purchase->isDrBased())
      $this->purchase->setIsDrBased(0);
    else
      $this->purchase->setIsDrBased(1);
    $this->purchase->save();

    $this->redirect("purchase/view?id=".$this->purchase->getId());
  }
  //this is to fix a bug that creates stock entries
  //for purchase details that are dr based
  //this need to be run only at the time the bug was discovered
  public function executePurgeWronglyCreatedStockEntries(sfWebRequest $request)
  {
    $purchases=Doctrine_Query::create()
      ->from('Purchase p')
      ->where('p.is_dr_based=1')
      ->execute();
    foreach($purchases as $purchase)
    {
      foreach($purchase->getPurchasedetail() as $detail)
      {
        $stockentries=Doctrine_Query::create()
          ->from('Stockentry se')
          ->where('se.ref_class="Purchasedetail" and se.ref_id='.$detail->getId())
          ->execute();
        foreach($stockentries as $se)
          $se->delete();
      }
    }
    echo "purge successful";
    die();
  }
  public function executeCheckExists(sfWebRequest $request)
  {
    $purchase=Fetcher::fetchOne("Purchase",array("pono"=>$request->getParameter("code")));
    if($purchase!=null)
      echo json_encode(array("id"=>$purchase->getId(), "total"=>$purchase->getTotal(), "vendor"=>$purchase->getVendor()->getName()));
    else 
      echo "";
    die();
  }
  public function executeDuplicateError(sfWebRequest $request)
  {
    $this->purchase=Fetcher::fetchOne("Purchase",array("id"=>$request->getParameter("id")));
  }
}
