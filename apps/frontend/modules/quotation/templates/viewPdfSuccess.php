<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name').' Billing Statement for '.$quotation->getCode());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(20, 10, 20, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================


// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
// $pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 100, '', 'PNG', '', '', false, 300, 'C', false, false, 0, false, false, false);
// $pdf->write(5,"\n\n\n\n");
$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->write(0,sfConfig::get('custom_company_header_text'),'',false,'C',true,0,false,false,0,0);
//$pdf->write(0,sfConfig::get('custom_company_address'),'',false,'C',true,0,false,false,0,0);
$pdf->write(0,sfConfig::get('custom_company_email'),'',false,'C',true,0,false,false,0,0);
$pdf->write(5,"\n");

$tbl = "";

//-------------------
$client=$quotation->getCustomer();

$content=array(
  $quotation->getCode(),
  MyDateTime::frommysql($quotation->getDate())->toshortdate(),
  $client->getName(),
  "",
  $client->getPhone1(),
  $client->getEmail(),
  );
  
  

$tbl .= <<<EOD
<table>
 <tr>
  <td align="left">Quotation No.: $content[0]</td>
  <td align="right">Date: $content[1]</td>
 </tr>
 <tr>
  <td align="left">Customer: $content[2]</td>
 </tr>
 <tr>
  <td align="left">Contact No.: $content[4]</td>
 </tr>
 <tr>
  <td align="left">Email: $content[5]</td>
 </tr>
</table>
EOD;




//$pdf->write(5,"\n");
//$pdf->write(0,"\t".$message,'',false,'L',true,0,false,false,0,0);

//====TABLE HEADER===========================

$widths=array(10,50,50,20,20,20);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$opening="";
$closing="";
if($quotation->getOpening()!="" and $quotation->getOpening()!=null)
  $opening="<p>".str_replace("\n","<br>",$quotation->getOpening())."</p>";
if($quotation->getClosing()!="" and $quotation->getClosing()!=null)
  $closing="<p>".str_replace("\n","<br>",$quotation->getClosing())."</p>";

$tbl .= <<<EOD
<hr>
<h2 align="center">Quotation</h2>
$opening
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Qty</b></td>
  <td width="$widths[1]" align="center"><b>Product</b></td>
  <td width="$widths[2]" align="center"><b>Description</b></td>
  <td width="$widths[3]" align="center"><b>Price</b></td>
  <td width="$widths[4]" align="center"><b>Total</b></td>
 </tr>
</thead>
EOD;

//===TABLE BODY============================
$height=1;
  $grandtotal=0;
	foreach($quotation->getQuotationdetail() as $detail)
  {
    $content=array(
      $detail->getQty(),
      $detail->getProduct(),
      $detail->getDescription(),
      MyDecimal::format($detail->getPrice()),
      MyDecimal::format($detail->getTotal()),
      );
  
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center">$content[0]</td>
    <td width="$widths[1]" >$content[1]</td>
    <td width="$widths[2]" >$content[2]</td>
    <td width="$widths[3]" align="right">$content[3]</td>
    <td width="$widths[4]" align="right">$content[4]</td>
  </tr>
EOD;
  }    

//table footer
$content=array(
  "",
  "",
  "",
  "Total:",
  MyDecimal::format($quotation->getTotal()),
  );
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center">$content[0]</td>
    <td width="$widths[1]" >$content[1]</td>
    <td width="$widths[2]" >$content[2]</td>
    <td width="$widths[3]" align="right"><b>$content[3]</b></td>
    <td width="$widths[4]" align="right"><b>$content[4]</b></td>
  </tr>
EOD;

$tbl .= <<<EOD
</table>

$closing

EOD;

/*
<h4>Warranty</h4>
<ul>
<li>3 Months warranty against workmanship and weld
<li>Parts are not included in the warranty
<li>There is no warranty against Pressure above the recommended (40psi)
<li>There is no warranty against Vacuum
</ul>

<h4>Payment Scheme:</h4>
<ul>
<li>Payable within 90 days upon quotation of product
<li>Payee: $content[0]
</ul>

*/

$content=array(
sfConfig::get('custom_billing_statement_signatory_name'),
sfConfig::get('custom_billing_statement_signatory_position'),
sfConfig::get('custom_billing_statement_signatory_image'),
sfConfig::get('custom_company_phone'),
sfConfig::get('custom_company_email'),
sfConfig::get('custom_company_address'),
);
$tbl .= <<<EOD
<table border="1">
 <tr>
  <td align="center">Prepared By:</td>
  <td align="center">Warehouse:</td>
  <td align="center">Customer Confirmation:</td>
 </tr>
 <tr valign="bottom">
  <td align="center"><img src="$content[2]" width="50"><br>$content[0]<br>$content[1]</td>
  <td align="center"><br><br><br><br><br>Warehouse Man</td>
  <td align="center"></td>
 </tr>
</table>
<br>
<br><b>Tel: $content[3]</b>
<br>Email: $content[4]
<br>$content[5]



EOD;


$pdf->writeHTML($tbl, true, false, false, false, '');

//-----------------


//--------------------------------------------------

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output("Quotation-".$quotation->getCode(), 'I');

//============================================================+
// END OF FILE
//============================================================+

