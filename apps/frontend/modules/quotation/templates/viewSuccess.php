<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
	Quotation <?php echo $quotation->getCode()?> for <?php echo $quotation->getCustomer() ?>
</h1>
<?php slot('transaction_id', $quotation->getId()) ?>
<?php slot('transaction_type', "Quotation") ?>

<?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
<?php echo form_tag_for($form,'quotation/adjust')?> <?php echo $form['id'] ?>
<?php }?>	
<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Date</td>
          <td>
            <?php echo MyDateTime::frommysql($quotation->getDate())->toprettydate()?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <?php echo $form["date"]?>
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td>Customer</td>
          <td><?php echo link_to($quotation->getCustomer(),"customer/view?id=".$quotation->getCustomerId(),array("target"=>"edit_customer")); ?> <?php echo link_to("(Choose)","quotation/chooseCustomer?id=".$quotation->getId())?> <?php if(sfConfig::get('custom_is_discount_based'))echo link_to("(Edit)","customer/edit?id=".$quotation->getCustomerId())?></td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php echo link_to("Generate Invoice","quotation/generateInvoice?id=".$quotation->getId()) ?>
          </td>
        </tr>
		</table>
    <td>
      <table>
        <tr>
          <td>Sales Rep</td>
          <td><?php echo $quotation->getEmployee() ?></td>
        </tr>
        <!--tr>
          <td>Discount Rate</td>
          <td><?php //echo $quotation->getDiscrate() ?></td>
        </tr>
        <tr>
          <td>Discount Amount</td>
          <td><?php //echo $quotation->getDiscamt() ?></td>
        </tr-->
        <tr>
          <td>Total</td>
          <td><?php echo $quotation->getTotal() ?></td>
        </tr>
      </table>
    </td>
    <td>
			<table>
        <tr>
          <td>Notes</td>
          <td><?php echo $quotation->getNotes() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <textarea rows="3" cols="20" name="quotation[notes]" id="quotation_notes"><?php echo $quotation->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <input type="submit" value="Save"></form>
            <?php }?>	
          </td>
        </tr>
		  </table>
    </td>
  </tr>
</table>

            <?php echo link_to("Edit","quotation/edit?id=".$quotation->getId(),array("id"=>"quotation_edit")) ?> |
            <?php echo link_to('Delete','quotation/delete?id='.$quotation->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
            <?php echo link_to("View Files","quotation/files?id=".$quotation->getId());?> |
            <?php echo link_to("Print","quotation/viewPdf?id=".$quotation->getId());?>

<?php
//if user is salesman or encoder
if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){
?>
<?php //echo link_to("Add Detail","quotationdetail/new?quotation_id=".$quotation->getId()) ?>

<hr>

<?php if(!sfConfig::get('custom_is_discount_based')){
//--------------------------start is not discount based-----------------------
?>
<b>Search product:</b> <input id=quotationproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=quotationclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@quotationdetail",array("id"=>"new_quotation_detail_form")); ?>
<input type=hidden name=quotationdetail[quotation_id] value=<?php echo $quotation->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Product</td>
		<td>Qty</td>
		<td>Price</td>
		<td>Discounted</td>
		<!--td>Discrate</td-->
		<td>Description</td>
	</tr>

	<tr>
		<td>
      <input hidden=true name=quotationdetail[product_id] id=quotationdetail_product_id value=<?php echo $detailform->getObject()->getProductId()?>>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<td><input size="5" name="quotationdetail[qty]" value="1" id="quotationdetail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="8" name="quotationdetail[price]" id="quotationdetail_price" type="text" <?php if(!$product_is_set)echo "disabled=true"; else echo "value=\"".$detailform->getObject()->getProduct()->getMaxsellprice()."\"";?>></td>
		<td align=center><input type=checkbox name=quotationdetail[is_discounted] id=chk_is_discounted <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input name="quotationdetail[description]" id="quotationdetail_description" type="text" <?php if(!$product_is_set)echo "disabled=true"; else echo "value=\"".$detailform->getObject()->getProduct()->getDescription()."\""; ?>></td>
		<td><input type=submit name=submit id=quotation_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> ></form>
</td>
	</tr>

	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>


<?php 
//--------------------------end is not discount based-----------------------
}else{
//--------------------------start is_discount_based-------------------------
?>

<b>Search product:</b> <input id=quotationproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=quotationclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@quotationdetail",array("id"=>"new_quotation_detail_form")); ?>
<input type=hidden name=quotationdetail[quotation_id] value=<?php echo $quotation->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Product</td>
		<td>Qty</td>
		<td>Price</td>
		<td>Is Discounted</td>
		<td>Discount Rate</td>
		<td>Notes</td>
	</tr>

	<tr>
		<td>
      <input hidden=true name=quotationdetail[product_id] id=quotationdetail_product_id value=<?php echo $detailform->getObject()->getProductId()?>>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<td><input size="5" name="quotationdetail[qty]" value="1" id="quotationdetail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="8" name="quotationdetail[price]" value="0.00" id="quotationdetail_price" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td align=center><input type=checkbox name=quotationdetail[is_discounted] id=chk_is_discounted <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="5" name="quotationdetail[discrate]" value="<?php echo $quotation->getCustomer()->getDiscrate()?>" id="quotationdetail_discrate" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input name="quotationdetail[description]" id="quotationdetail_description" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=quotation_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> ></form>
</td>
	</tr>

	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>

<?php }//-------------end is_discount_based-------------------- ?>

<?php } ?>

<div id="quotationsearchresult"></div>

<hr>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo "<br>".form_tag_for($customerform,'customer/adjust')?> 
          <?php echo $customerform['id'] ?>
<?php } ?>    
<table>
  <tr>
    <td>Date: <u><?php echo $quotation->getDate()?></u></td>
  </tr>
  <tr>
    <td colspan=2>Customer: <u><?php echo $quotation->getCustomer()?></u></td>
  </tr>
  <tr>
    <td colspan=2>Address: <u><?php echo $quotation->getCustomer()->getAddress()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['address'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Address2: <u><?php echo $quotation->getCustomer()->getAddress2()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['address2'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Phone: <u><?php echo $quotation->getCustomer()->getPhone1()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['phone1'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Email: <u><?php echo $quotation->getCustomer()->getEmail()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['email'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Representative: <u><?php echo $quotation->getCustomer()->getRep()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['rep'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>TIN/SC-TIN: <u><?php echo $quotation->getCustomer()->getTinNo()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['tin_no'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr valign=top>
    <td colspan=2>Notes: <u><?php echo $quotation->getCustomer()->getNotepad()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <br><textarea cols=100 rows=2 name="customer[notepad]" id="customer_notepad" type="text"><?php echo $quotation->getCustomer()->getNotepad()?></textarea>
  <?php } ?>    
    </td>
  </tr>
</table>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
  <input type="submit" value="Save">
  </form>
<?php } ?>    

<!-----quotation detail table------>

<hr>

<?php echo form_tag_for($form,'quotation/adjust')?> <?php echo $form['id'] ?>
Opening Message:<br>
<textarea rows="5" cols="80" name="quotation[opening]" id="quotation_opening"><?php echo $quotation->getOpening()?></textarea>   
<input type="submit" value="Save"></form>

<?php if(!sfConfig::get('custom_is_discount_based')){
//--------------------------start is not discount based-----------------------
?>
<?php
$totalsale=0;
?>
<table border=1>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>Unit</td>
    <td width=30%>Product</td>
    <td>Description</td>
    <td>Unit Price</td>
    <td>Amount</td>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td></td>
    <td></td>
<?php } ?>
  </tr>
  <?php 
  	$counter=0;
  	foreach($quotation->getQuotationdetail() as $detail){
  		$counter++;?>
  <tr>
    <td>
      <?php echo $detail->getQty() ?>
      <?php echo form_tag("quotationdetail/setQty");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value>
      </form>

    </td>
    <td></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
    <td align=right>
      <?php echo $detail->getDescription() ?>
      <?php echo form_tag("quotationdetail/setDescription");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=15 name=value>
      </form>
    </td>
    <td align=right>
      <?php echo $detail->getPrice() ?>
      <?php echo form_tag("quotationdetail/setPrice");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value>
      </form>
    </td>
    <td align=right><font <?php if($detail->getIsDiscounted())echo "color=green"?>><?php echo $detail->getTotal(); $totalsale+=$detail->getTotal(); ?></font></td>

    <!--td><?php //echo link_to("Price List","producttype/view?id=".$detail->getProduct()->getProducttypeId()) ?></td-->
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td><?php echo link_to("Edit","quotationdetail/edit?id=".$detail->getId(),array("class"=>"quotation_detail_edit","detail_id"=>$detail->getId())) ?></td>
    <td>
<?php echo link_to(
  'Delete',
  'quotationdetail/delete?id='.$detail->getId(),
  array('method' => 'delete', 'confirm' => 'Are you sure?')
) ?>

    </td>
  </tr>
  <?php }?>
  <?php }?>
  
  <tr align=right>
	  <td></td>
	  <td></td>
	  <td>Total Sales (net of VAT)</td>
	  <td></td>
	  <td><?php $vat=$totalsale/1.12;echo number_format($vat,2,".",",")?></td>
  </tr>
  <tr align=right>
	  <td></td>
	  <td></td>
	  <td>Add 12% VAT</td>
	  <td></td>
	  <td><?php echo number_format($totalsale-$vat,2,".",",")?></td>
  </tr>
  <tr align=right>
	  <td></td>
	  <td></td>
	  <td>Amount Received</td>
	  <td></td>
	  <td><?php echo number_format($totalsale,2,".",",")?></td>
  </tr>
</table>

<?php 
//--------------------------end is not discount based-----------------------
}else{
//--------------------------start is_discount_based-------------------------
?>

<table border=1>
  <tr>
    <td>Qty</td>
    <td>Product</td>
    <td>Price</td>
    <td>Disc Rate</td>
    <td>Disc Price</td>
    <td>Total</td>
    <td>Notes</td>
    <td></td>
    <td></td>
  </tr>
  <?php 
  	foreach($quotation->getquotationdetails() as $detail){?>
  <tr>
    <td>
      <?php echo form_tag("quotationdetail/setQty");?>
      <?php echo $detail->getQty() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value value="<?php echo $detail->getQty() ?>">
      </form>
    </td>
    <td>
      <?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?>
    </td>
    <td align=right bgcolor=pink>
      <?php echo $detail->getPrice() ?>
      <?php echo form_tag("quotationdetail/setPrice");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value value=<?php echo $detail->getPrice()?>>
      </form>
    </td>
    <td>
      <?php echo form_tag("quotationdetail/setDiscrate");?>
      <?php echo $detail->getDiscrate() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value value="<?php echo $detail->getDiscrate() ?>">
      </form>
    </td>
    <td align=right>
      <?php echo MyDecimal::format($detail->getDiscprice()) ?>
    </td>
    <td align=right>
      <?php echo MyDecimal::format($detail->getTotal()) ?>
    </td>
    <td>
      <?php echo form_tag("quotationdetail/setDescription");?>
      <?php echo $detail->getDescription() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=10 name=value>
      </form>
    </td>
    <?php {?>
      <td>
        <?php echo link_to('Delete','quotationdetail/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
      </td>
    <?php }?>
  </tr>
  <?php }?>
</table>

<?php }//-------------end is_discount_based-------------------- ?>

<?php echo form_tag_for($form,'quotation/adjust')?> <?php echo $form['id'] ?>
Closing Message: <br>
<textarea rows="5" cols="80" name="quotation[closing]" id="quotation_closing"><?php echo $quotation->getClosing()?></textarea>   
<input type="submit" value="Save"></form>

<hr>

<script type="text/javascript">
function changeText(id)
{
var x=document.getElementById("mySelect");
x.value=id;
}
var manager_password="<?php 
	    $setting=Doctrine_Query::create()
	        ->from('Settings s')
	      	->where('name="manager_password"')
	      	->fetchOne();
      	if($setting!=null)echo $setting->getValue();
?>";
//on qty get focus, select its contents
$("#quotationdetail_qty").focus(function() { $(this).select(); } );
$("#quotationproductsearchinput").focus(function() { $(this).select(); } );
var is_cashier=<?php echo $sf_user->hasCredential(array('cashier'),false)?"true":"false"?>;
var is_admin=<?php echo $sf_user->hasCredential(array('admin'),false)?"true":"false"?>;
if(is_admin)
{
$("#quotationproductsearchinput").focus();
$("#quotationproductsearchinput").select(); 
}
else if(is_cashier)
{
$("#quotation_invno").focus();
$("#quotation_invno").select(); 
}
else
{
$("#quotationproductsearchinput").focus();
$("#quotationproductsearchinput").select(); 
}
//if no product id set, disable save button
if($("#quotationdetail_product_id").val()=='')	 		  
  $("#quotation_detail_submit").prop("disabled",true);
//------Quotation Discount-------------------
//on page ready, var discounted is false
var discounted=false;
//set checkbox to false as default
$("#chk_is_discounted").prop('checked',false);
//hide all password entry boxes

//------Quotation (not header) product search-----------
//$("#quotationproductsearchinput").keyup(function(){
//$("#quotationproductsearchinput").on('input propertychange paste', function() {
$("#quotationproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#quotation_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#quotationproductsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#quotationproductsearchinput").val();
      //if searchstring is all numbers except last letter, 
      //and length is 8
      //this is a barcode. 
      if(
        $.isNumeric(searchstring.slice(0, -1))
        &&
        searchstring.length>=8
        )
      {
        processBarcode(searchstring);
      }
      //else do ajax to product search, display result in quotationsearchresult
      else
      {
	      $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+searchstring+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	   		  $("#quotationsearchresult").html(result);
	      }});
      }
    }
    //else clear
    //else
 		//  $("#quotationsearchresult").html("");
});
$("#quotationclearsearch").click(function(){
 		  $("#quotationsearchresult").html("");
});
//---------New Quotation Detail Submit-----------------------------
//on submit new quotation detail form
$("#new_quotation_detail_form").submit(function(event){

  //this is when the cursor is on qty and the barcode reader is used
  //determine if qty is actually a barcode
  if(
    //if qty is too long to be a quantity, this is probably a barcode
    $("#quotationdetail_qty").val().length>=8 && 
    //but only if qty is enabled
    $("#quotationdetail_qty").attr("disabled")!="disabled")
  {
    processBarcode($("#quotationdetail_qty").val());
    return false;
  } 

  //if discount checkbox is checked, go ahead with submit
  if($("#chk_is_discounted").prop('checked'))return true;

  //determine minimum product selling price
  var price=$("#quotationdetail_price").prop('value');
  var minprice=$("#product_min_price").val();
  if(minprice==0)minprice=$("#product_price").val();
  //if price is less than min price, complain and don't submit
  if(parseFloat(price)<parseFloat(minprice))
  {
    alert("Minimum selling price is "+minprice+".\n\nPlease check Discounted check box to request for discount, and notify manager. ");
    return false;
  }
});
//------ BARCODE -----------------------------
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
function processBarcode(barcode)
{
        //
        if(barcode.length<8)
        {
          barcode=pad(barcode, 8);
        }

		$("#pleasewait").html("<font color=red><b>PLEASE WAIT</b></font>");
	      $.ajax
	      ({
	        url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/quotation/barcodeEntry?barcode="+barcode, 
	        dataType: "json",
	        success: function(result)
	        {
			$("#pleasewait").html("");
	          //if product found
	          if(result[0]!=0)
	          {
              /*
              //invoidetail input controls disabled=false
              $("#chk_is_discounted").removeAttr('disabled');
              $("#quotationdetail_description").removeAttr('disabled');
              $("#quotation_detail_submit").removeAttr('disabled');
              $("#quotationdetail_qty").removeAttr('disabled');
              $("#quotationdetail_price").removeAttr('disabled');
              $("#quotationdetail_with_commission").removeAttr('disabled');

	            //populate values
	            $("#quotationproductsearchinput").val("");
              $("#quotationdetail_qty").val(1);
              $("#quotationdetail_product_id").val(result[0]);//product_id
              $("#productname").html(result[1]);//product name
              $("#quotationdetail_price").val(result[2]);//maxsellprice
              $("#quotationdetail_with_commission").val(0);
              $("#chk_is_discounted").attr("checked",false);
              $("#quotationdetail_description").val("");
              */
              
              $("#product_min_price").val(result[3]);//minsellprice
              $("#product_price").val(result[2]);//maxsellprice
              $("#product_name").val(result[1]);//product name
              $("#quotationdetail_barcode").val(barcode);
              
              //set focus to qty
              $("#quotationdetail_qty").focus();
	          }
	          else
	          {
	            alert("Product not found");
              $("#quotationdetail_qty").val("");
	          }
	        }
	      });

}
</script>
<!--
          <td><?php //echo link_to("Cheque Collection","event/new?parent_class=Quotation&parent_id=".$quotation->getId()."&type=ChequeCollect") ?></td>
          <td><?php //echo link_to("Cash Collection","event/new?parent_class=Quotation&parent_id=".$quotation->getId()."&type=CashCollect") ?></td>
          <td><?php //echo link_to("Bank Expense","event/new?parent_class=Quotation&parent_id=".$quotation->getId()."&type=BankExp") ?></td>
          <td><?php //echo link_to("Cancel","quotation/cancel?id=".$quotation->getId()) ?></td>
          <td><?php //if($quotation->getSaletype()!="Cash")echo link_to("Cash sale","quotation/adjustsaletype?id=".$quotation->getId()."&type=Cash") ?></td>
          <td><?php //if($quotation->getSaletype()!="Cheque")echo link_to("Cheque sale","quotation/adjustsaletype?id=".$quotation->getId()."&type=Cheque") ?></td>
          <td><?php //if($quotation->getSaletype()!="Credit")echo link_to("Account sale","quotation/adjustsaletype?id=".$quotation->getId()."&type=Account") ?></td>
<br>
          <td><?php //echo link_to("View Details","quotation/view?id=".$quotation->getId()) ?></td>
          <td><?php //echo link_to("View Events","quotation/events?id=".$quotation->getId()) ?></td>
          <td><?php //echo link_to("View Accounting","quotation/accounting?id=".$quotation->getId()) ?></td>
-->


