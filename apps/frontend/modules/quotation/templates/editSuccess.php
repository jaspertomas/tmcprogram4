<?php use_helper('I18N', 'Date') ?>
<?php include_partial('quotation/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit Quotation '.$form->getObject()->getCode(), array(), 'messages') ?></h1>

  <?php include_partial('quotation/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('quotation/form_header', array('quotation' => $quotation, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('quotation/form', array('quotation' => $quotation, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('quotation/form_footer', array('quotation' => $quotation, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
