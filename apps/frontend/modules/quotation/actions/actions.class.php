<?php

require_once dirname(__FILE__).'/../lib/quotationGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/quotationGeneratorHelper.class.php';

/**
 * quotation actions.
 *
 * @package    sf_sandbox
 * @subpackage quotation
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class quotationActions extends autoQuotationActions
{
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());
    $requestparams["code"]=QuotationTable::getNextCode();

    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $quotation = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $quotation)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect("quotation/view?id=".$quotation->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->quotation = new Quotation();
    $this->quotation->setDate(MyDate::today());
    $chequepayee=sfConfig::get('custom_cheques_payable_to');
    $this->quotation->setClosing("Warranty

 - 3 Months warranty against workmanship and weld
 - Parts are not included in the warranty
 - There is no warranty against Pressure above the recommended (40psi)
 - There is no warranty against Vacuum

Payment Scheme:

 - Payable within 90 days upon quotation of product
 - Payee: ".$chequepayee);
    
    //auto set employee
    $employee=Doctrine_Query::create()
    ->from('Employee e')
  	->where('e.username = "'.$this->getUser()->getUsername().'"')
  	->fetchOne();
  	if($employee) $this->quotation->setSalesmanId($employee->getId());
  	else $this->quotation->setSalesmanId(SettingsTable::fetch("default_salesman_id"));

    		
    //set customer if param customer_id
    if($request->getParameter("customer_id"))
    {
      $this->quotation->setCustomerId($request->getParameter("customer_id"));
    }

/*    
    //set customer name if param customer_name
    if($request->getParameter("customer_name"))
    {
      $this->quotation->setCustomerId(1);
      $this->quotation->setCustomerName($request->getParameter("customer_name"));
    }
*/    
    $this->form = $this->configuration->getForm($this->quotation);
  }
  public function executeView(sfWebRequest $request)
  {
    $this->forward404Unless(
      $this->quotation=Doctrine_Query::create()
        ->from('Quotation i')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Quotation with id (%s) not found.', $request->getParameter('id')));


    $this->form = $this->configuration->getForm($this->quotation);
    $this->customerform = new CustomerForm($this->quotation->getCustomer());

    //allow set product id by url
    $detail=new Quotationdetail();
    $detail->setQty(1);
    $this->product_is_set=false;
    $this->product=null;
    if($request->getParameter("product_id"))
    {
      $detail->setProductId($request->getParameter("product_id"));
      $this->product=$detail->getProduct();
      $this->product_is_set=true;
    }
    $this->detailform = new QuotationdetailForm($detail);
    $this->detail = $detail;
    $this->searchstring=$this->request->getParameter('searchstring');
    
    //force commission column to appear,
    //but 1 time only
    $this->commissionOn=false;
    if($request->getParameter("commission")=="on")
      $this->commissionOn=true;
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);
    $this->message=$request->getParameter("message");

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('quotation');
    $id=$requestparams['id'];
 
    $this->forward404Unless(
    $this->quotation=Doctrine_Query::create()
    ->from('Quotation i')
    ->where('i.id = '.$id)
    ->fetchOne()
    , sprintf('Quotation with id (%s) not found.', $request->getParameter('id')));

    if(isset($requestparams["date"]))
      $this->quotation->setDate($requestparams["date"]);
    if(isset($requestparams["notes"]))
      $this->quotation->setNotes($requestparams["notes"]);
    if(isset($requestparams["opening"]))
      $this->quotation->setOpening($requestparams["opening"]);
    if(isset($requestparams["closing"]))
      $this->quotation->setClosing($requestparams["closing"]);
    $this->quotation->save();

    $this->redirect($request->getReferer());
  }
  public function executeGenerateInvoice(sfWebRequest $request)
  {
    $this->forward404Unless(
    $quotation=Doctrine_Query::create()
    ->from('Quotation i')
    ->where('i.id = '.$request->getParameter("id"))
    ->fetchOne()
    , sprintf('Quotation with id (%s) not found.', $request->getParameter('id')));

    $conn = Doctrine_Manager::connection();
    $invoice=new Invoice();
    try 
    {
      $conn->beginTransaction();

      $invoice->setDate(MyDate::today());
      $invoice->setCustomerId($quotation->getCustomerId());
      $invoice->setTemplateId(1);
      $invoice->setIsTemporary(2);
      $invoice->setNotes("Autogenerated from Quotation ".$quotation->getCode());
      $invoice->setInvno($this->getUser()->getUsername()." ".date('h:i:s a'));

      //set employee
      $employee=Doctrine_Query::create()
        ->from('Employee e')
      	->where('e.username = "'.$this->getUser()->getUsername().'"')
      	->fetchOne();
    	if($employee)$invoice->setSalesmanId($employee->getId());
    	else $invoice->setSalesmanId(SettingsTable::fetch("default_salesman_id"));

      $invoice->save($conn);

      foreach($quotation->getQuotationdetail() as $qd)
      {
        $detail=new Invoicedetail();
        $detail->setInvoiceId($invoice->getId());
        $detail->setQty($qd->getQty());
        $detail->setProductId($qd->getProductId());
        $detail->setDescription($qd->getDescription());
        $detail->setPrice($qd->getPrice());
        $detail->setWithCommission($qd->getPrice());
        $detail->setIsDiscounted($qd->getIsDiscounted());
        $detail->setTotal($qd->getTotal());
        $detail->setWithCommissionTotal($qd->getTotal());
        $detail->save($conn);
      }

      $invoice->calc();
      $invoice->save($conn);

      $conn->commit();
    } catch (Exception $e) {
      if ($conn)$conn->rollback();
      $this->getUser()->setFlash('error', "Error generating invoice for Quotation ".$quotation->getCode().": ".$e->getMessage());
      return $this->redirect($request->getReferer());
    }

    $this->getUser()->setFlash('notice', "Successfully generated invoice for Quotation ".$quotation->getCode());
    $this->redirect("invoice/view?id=".$invoice->getId());

  }
}
