<?php
$red="#f00";
$yellow="#ff0";
$green="#0f7";
$blue="#ccf";
$plain="#eee";
$black="#000";
?>
<h1>Expense Voucher <?php echo $voucher->getNo()?><?php if($voucher->getIsChecked())echo "<font color=green> (Checked)</font>"?></h1>
<?php if($check->getId()!=null){?>
<?php $passbook=$check->getPassbook();?>
<h2>Amount: <?php echo MyDecimal::format($voucher->getAmount()); ?></h2>
<h2>From Account: <?php echo $passbook." (".$passbook->getAccountNo().")"; ?></h2>
<h2>Pay To: <?php echo $voucher->getPayee(); ?></h2>
<h2>Check Date: <?php echo MyDateTime::frommysql($check->getCheckDate())->toprettydate()?></h2>
<h2>Check No: <?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo(); ?></h2>
<h2>Check Status: <?php 
switch($voucher->getCheckStatus())
{
  case "Pending": $color=$black;break;
  case "Cancelled": $color=$red;break;
  case "Released": $color=$black;break;
  case "Cleared": $color=$black;break;
}
echo "<font color='$color'>".$voucher->getCheckStatus()."</font> ";
?></h2>

<?php }?>

<?php 
if($sf_user->hasCredential(array('admin','banker','manager'), false)){
//-------------RELEASE--------------
if(!$voucher->getIsCancelled() and $check->getId()!=null){
  if($check->getReceiveDate()!=null)
  {
    echo "Released on ".MyDateTime::frommysql($check->getReceiveDate())->toprettydate()." ";
    if($voucher->getCheckStatus()=="Released")echo link_to('Undo','voucher/undoReleaseCheck?id='.$voucher->getId(), array('confirm' => 'Are you sure?'));
  }
  else
  {
    $outcheck=new OutCheck();
    $outcheck->setReceiveDate(MyDate::today());
    $form=new OutCheckForm($outcheck);
    echo form_tag_for($form,'voucher/releaseCheck', array('style'=>'display:inline','onsubmit'=>"return confirm('Are you sure?');"))?>
    <input type=hidden id=id name=id value=<?php echo $voucher->getId()?>>
    <?php 
      echo $form['receive_date'];
    ?>
    <input type=submit name=submit id=submit value="Mark Check as Released">
    </form>
    <?php
  }
}  
echo "<br>";
echo "<br>";
//-------------CLEAR--------------
if(!$voucher->getIsCancelled() and $check->getId()!=null and $check->getReceiveDate()!=null){
  if($check->getClearedDate()!=null)
  {
    echo "Cleared on ".MyDateTime::frommysql($check->getClearedDate())->toprettydate()." ";
    echo link_to('Undo','voucher/undoClearCheck?id='.$voucher->getId(), array('confirm' => 'Are you sure?'));
  }
  else
  {
    $outcheck=new OutCheck();
    $outcheck->setClearedDate(MyDate::today());
    $form=new OutCheckForm($outcheck);
    echo form_tag_for($form,'voucher/clearCheck', array('style'=>'display:inline','onsubmit'=>"return confirm('Are you sure?');"))?>
    <input type=hidden id=id name=id value=<?php echo $voucher->getId()?>>
    <?php 
      echo $form['cleared_date'];
    ?>
    <input type=submit name=submit id=submit value="Mark Check as Cleared">
    </form>
    <?php
  }
}  
echo "<br>";
echo "<br>";

//-------------CANCEL--------------
if($voucher->getIsCancelled())
{
  echo link_to('Undo Cancel','voucher/undoCancel?id='.$voucher->getId(), array('confirm' => 'Are you sure?'));
}
else if($check->getReceiveDate()==null and $check->getClearedDate()==null)
{
  echo form_tag('voucher/cancel', array('style'=>'display:inline','onsubmit'=>"return confirm('CANCEL?');"));
  ?>
  <input type=hidden id=id name=id value=<?php echo $voucher->getId()?>>
  <input type=submit name=submit id=submit value="Cancel">
  </form>
  <?php
  echo form_tag('voucher/cancelAndReplace', array('style'=>'display:inline','onsubmit'=>"return confirm('CANCEL AND REPLACE?');"));
  ?>
  <input type=hidden id=id name=id value=<?php echo $voucher->getId()?>>
  <input type=submit name=submit id=submit value="Cancel and Replace">
  </form>
  <?php
}
echo "<br><br>";
}//end permission admin banker
?>

<?php echo link_to("Edit","voucher/edit?id=".$voucher->getId())?>

<?php if($sf_user->hasCredential(array('admin'), false)){?>
<br><?php echo link_to('Delete','voucher/delete?id='.$voucher->getId(), array('method' => 'delete', 'confirm' => 'Delete this voucher": Are you sure?')) ?>
<?php } ?>

<?php if($sf_user->hasCredential(array('admin'), false)){?>
<br><?php 
if(!$voucher->getIsChecked())
  echo link_to('Mark as Checked','voucher/check?id='.$voucher->getId()); 
else
echo link_to('Mark as Unchecked','voucher/uncheck?id='.$voucher->getId()); 
?>
<?php } ?>

<br><?php 
//print voucher first
if($voucher->getPrintDate()!=null)
{
//print voucher only if not yet printed or user is admin
// if($check->getPrintDate()==null or $sf_user->hasCredential(array('admin'), false))
//   echo link_to("Print Cheque","voucher/printChequeDmf?id=".$voucher->getId())." | ";
if($check->getPrintDate()==null or $sf_user->hasCredential(array('admin'), false))
  echo link_to("Print Cheque PDF","voucher/printChequePdf?id=".$voucher->getId())." | ";
if($check->getPrintDate()!=null)
  echo "Cheque Printed on ".MyDateTime::frommysql($check->getPrintDate())->toshortdate();
}
?>

<br><?php 
  //print voucher only if not yet printed or user is admin
  if($voucher->getPrintDate()==null or $sf_user->hasCredential(array('admin'), false))
  {
    echo link_to("Print Voucher","voucher/viewPdf?id=".$voucher->getId())." | ";
    echo "<br>";
    echo link_to("Print Voucher Old Version","voucher/viewPdfOld?id=".$voucher->getId())." | ";
  }
  if($voucher->getPrintDate()!=null)
  echo "Voucher Printed on ".MyDateTime::frommysql($voucher->getPrintDate())->toshortdate();
?>

<br><?php echo link_to("Back to Voucher Dashboard","voucher/dashboard?date=".$voucher->getDate());?>
<br><?php //echo link_to("View Files","voucher/files?id=".$voucher->getId());?>

<hr>
<b>Images</b>
<?php foreach($voucher->getImages() as $image){?>  
  <br><a href="<?php echo $image->getUrl()?>" target="_blank">View Image <?php echo $image->getId()?></a>
<?php } ?>
<hr>

<table>
  <tr>
    <td>Amount</td>
    <td><?php echo MyDecimal::format($voucher->getAmount());?></td>
  </tr>
  <tr>
    <td>Pay To</td>
    <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
  </tr>
  <tr>
    <td>Pay To Self?</td>
    <td><?php echo $voucher->getIsPayToSelf() ? "YES" : "NO" ?></td>
  </tr>
  <?php if($check->getId()!=null){?>
    <tr>
      <td>
      Check Date 
      </td>
      <td>
        <?php echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?>
      </td>
    </tr>
    <tr>
      <td>
        Check Release Date 
      </td>
      <td>
        <?php if($check->getReceiveDate()!=null)echo MyDateTime::frommysql($check->getReceiveDate())->toshortdate()?>
      </td>
    </tr>
    <tr>
      <td>
      Check Clear Date 
      </td>
      <td>
        <?php if($check->getClearedDate()!=null)echo MyDateTime::frommysql($check->getClearedDate())->toshortdate()?>
      </td>
    </tr>
  <?php }?>
  <tr>
    <td>Ref</td>
    <td>
      <?php 
      if($voucher->getRefType()=="Purchase")
        {
          $purchase=$voucher->getPurchase();
          echo link_to("PO: ".$purchase->getPono(),"purchase/view?id=".$purchase->getId()) ;
        }
      else if($voucher->getRefType()=="CounterReceipt")
        {
          $counter_receipt=$voucher->getCounterReceipt();
          echo link_to("CR: ".$counter_receipt->getCode(),"counter_receipt/view?id=".$counter_receipt->getId()) ;
        }
        else if($voucher->getRefType()=="CommissionPayment")
        {
          $commission_payment=$voucher->getCommissionPayment();
          echo link_to("CP: ".$commission_payment->getName(),"commission_payment/view?id=".$commission_payment->getId()) ;
        }
      else if($voucher->getRefType()=="Invoice")
        {
          $invoice=$voucher->getInvoice();
          echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ;
        }
      ?>      
    </td>
  </tr>
  <tr>
    <td>Particulars</td>
    <td><?php echo $voucher->getParticulars()?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td></td>
  </tr>
  <tr>
    <td>Created on</td>
    <td><?php echo MyDateTime::frommysql($voucher->getDate())->toshortdate()?></td>
  </tr>
  <tr>
    <td>Expense Account</td>
    <td><?php echo $voucher->getVoucherAccount()?></td>
  </tr>
  <tr>
    <td>Expense Sub Account</td>
    <td><?php echo $voucher->getVoucherSubaccount()?></td>
  </tr>
  <tr>
    <td>Payment Type</td>
    <td><?php echo $voucher->getVoucherType()?></td>
  </tr>
  <tr>
    <td>Notes</td>
    <td><?php echo $voucher->getNotes()?></td>
  </tr>
  <tr>
    <td>Invoice Cross Ref</td>
    <td>
      <?php 
        $cross_system=$voucher->getInvoiceCrossRefSystem();
        if($cross_system!=null)
        {
          $label=$cross_system->getName()." Transaction ".$voucher->getInvoiceCrossRefCode();
          if($voucher->getInvoiceCrossRefId()!="")
            echo link_to($label,$cross_system->getUrl()."/web/invoice/".$voucher->getInvoiceCrossRefId()."/view");
          else
            echo $label;
      ?> (<span id="invoice_cross_ref_output" name="invoice_cross_ref_output"></span>)
      <?php } ?>
    </td>
  </tr>
  <tr>
    <td>Purchase Cross Ref</td>
    <td>
      <?php 
        $cross_system=$voucher->getPurchaseCrossRefSystem();
        if($cross_system!=null)
        {
          switch($voucher->getPurchaseCrossRefType())
          {
            case "Purchase":
              $label=$cross_system->getName()." PO ".$voucher->getPurchaseCrossRefCode();
              $module_path="purchase";
              break;
            case "CounterReceipt":
              $label=$cross_system->getName()." CR ".$voucher->getPurchaseCrossRefCode();
              $module_path="counter_receipt";
              break;
          }
          if($voucher->getPurchaseCrossRefId()!="")
            echo link_to($label,$cross_system->getUrl()."/web/".$module_path."/".$voucher->getPurchaseCrossRefId()."/view");
          else
            echo $label;
      ?> (<span id="purchase_cross_ref_output" name="purchase_cross_ref_output"></span>)
      <?php } ?>
    </td>
  </tr>
</table>



<script>
<?php 
  $purchaseCrossRefSystem=$voucher->getPurchaseCrossRefSystem();
  $purchaseCrossRefSystemUrl="";
  if($purchaseCrossRefSystem!=null)$purchaseCrossRefSystemUrl=$purchaseCrossRefSystem->getUrl();
  $invoiceCrossRefSystem=$voucher->getInvoiceCrossRefSystem();
  $invoiceCrossRefSystemUrl="";
  if($invoiceCrossRefSystem!=null)$invoiceCrossRefSystemUrl=$invoiceCrossRefSystem->getUrl();
?>
var cross_ref_system_silent_mode=<?php echo sfConfig::get('custom_cross_ref_system_silent_mode');?>;
if(<?php echo $voucher->getPurchaseCrossRefSystem()!=null?>)
{
  var purchase_cross_ref_system="<?php echo $purchaseCrossRefSystemUrl?>";
  // purchase_cross_ref_system="http://localhost/tmcprogram4";
  var url=purchase_cross_ref_system+"/web/purchase/checkExists?code="+"\"<?php echo $voucher->getPurchaseCrossRefCode()?>\"";
  // alert(url);

  $.ajax({
    url: url, 
    success: function(result)
    {
      //if result is empty, cross reference not found
      if(result=="")
      if(!cross_ref_system_silent_mode)
      alert("Purchase cross reference not found");
      else
      {
        var data=JSON.parse(result);

        //update purchase cross ref id
        var url2="setPurchaseCrossRefId?id="+<?php echo $voucher->getId()?>+"&purchase_cross_ref_id="+data["id"];
        $.ajax({url: url2,});

        //display vendor and total on screen
        $("#purchase_cross_ref_output").html("Supplier: "+data["vendor"]+", Total: "+data["total"]);
        //if total does not match, warning
        if(data["total"]!=<?php echo $voucher->getAmount()?>)alert("Warning: Purchase cross reference total does not match");
      }
    }
  });
}

if(<?php echo $voucher->getInvoiceCrossRefSystem()!=null?>)
{
  var invoice_cross_ref_system="<?php echo $invoiceCrossRefSystemUrl?>";
  // invoice_cross_ref_system="http://localhost/tmcprogram4";
  var url=invoice_cross_ref_system+"/web/invoice/checkExists?code="+"\"<?php echo $voucher->getInvoiceCrossRefCode()?>\"";
  // alert(url);

  $.ajax({
    url: url, 
    success: function(result)
    {
      //if result is empty, cross reference not found
      if(result=="")alert("Invoice cross reference not found");
      else
      {
        var data=JSON.parse(result);

        //update invoice cross ref id
        var url2="setInvoiceCrossRefId?id="+<?php echo $voucher->getId()?>+"&invoice_cross_ref_id="+data["id"];
        $.ajax({url: url2,});

        //display vendor and total on screen
        $("#invoice_cross_ref_output").html("Customer: "+data["customer"]+", Total: "+data["total"]);
        //if total does not match, warning
        if(data["total"]!=<?php echo $voucher->getAmount()?>)
        if(!cross_ref_system_silent_mode)
        alert("Warning: Invoice cross reference total does not match");
      }
    }
  });
}

</script>