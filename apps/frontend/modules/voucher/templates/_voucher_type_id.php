            <div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_voucher_type_id">
        <div>
      <label for="voucher_voucher_type_id">Expense Type</label>
      <div class="content">
<?php echo $form["voucher_type_id"]?>

<?php $vouchertypes= Doctrine_Query::create()
    ->from('VoucherType t')
  	->execute();
foreach($vouchertypes as $type){?>
<input type=button value="<?php echo $type->getName()?>" onClick=choosevouchertype(<?php echo $type->getId()?>);>
<?php } ?>
</div>

          </div>
  </div>
<script>
function choosevouchertype(id) {
  $("#voucher_voucher_type_id").val(id);
  //  $('#voucher_voucher_type_id option:eq('+index+')').prop('selected', true);
  switch(id)
  {
    //2=check - full check form
    //3=bank transfer - account dropdown only
    //all others no check form
    case 2:
      $("#out_check_check_no").prop('disabled', false);
      $("#out_check_passbook_id").prop('disabled', false);
      $("#out_check_check_date_year").prop('disabled', false);
      $("#out_check_check_date_month").prop('disabled', false);
      $("#out_check_check_date_day").prop('disabled', false);
      $("#out_check_receive_date_year").prop('disabled', false);
      $("#out_check_receive_date_month").prop('disabled', false);
      $("#out_check_receive_date_day").prop('disabled', false);
      break;
    case 3:
      $("#out_check_check_no").prop('disabled', true);
      $("#out_check_passbook_id").prop('disabled', false);
      $("#out_check_check_date_year").prop('disabled', true);
      $("#out_check_check_date_month").prop('disabled', true);
      $("#out_check_check_date_day").prop('disabled', true);
      $("#out_check_receive_date_year").prop('disabled', true);
      $("#out_check_receive_date_month").prop('disabled', true);
      $("#out_check_receive_date_day").prop('disabled', true);
      break;
    default:
      $("#out_check_check_no").prop('disabled', true);
      $("#out_check_passbook_id").prop('disabled', true);
      $("#out_check_check_date_year").prop('disabled', true);
      $("#out_check_check_date_month").prop('disabled', true);
      $("#out_check_check_date_day").prop('disabled', true);
      $("#out_check_receive_date_year").prop('disabled', true);
      $("#out_check_receive_date_month").prop('disabled', true);
      $("#out_check_receive_date_day").prop('disabled', true);
      break;
  }
};



</script>

