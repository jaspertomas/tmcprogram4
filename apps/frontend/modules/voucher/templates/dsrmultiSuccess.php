<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new VoucherForm(),"voucher/dsrmulti")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
$startofthisweek=$today->copy()->getthislastsaturday();//can be today
$startofnextweek=$startofthisweek->copy()->addweeks(1);
$startoflastweek=$startofthisweek->copy()->addweeks(-1);
$endofthisweek=$today->copy()->getthisfriday();
$endofnextweek=$endofthisweek->copy()->addweeks(1);
$endoflastweek=$endofthisweek->copy()->addweeks(-1);
?>


<table>
  <tr>
    <td>Account</td>
    <td>
      <?php echo $checkform['passbook_id'];?> 
    </td>
  </tr>
  <tr>
    <td>From Date</td>
    <td><?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $toform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Week","voucher/dsrmulti?startdate=".$startoflastweek->tomysql()."&enddate=".$endoflastweek->tomysql()."&passbook_id=".$passbook_id);?> | 
<?php echo link_to("This Week","voucher/dsrmulti?startdate=".$lastsaturday->tomysql()."&enddate=".$thisfriday->tomysql()."&passbook_id=".$passbook_id);?> | 
<?php echo link_to("Next Week","voucher/dsrmulti?startdate=".$startofnextweek->tomysql()."&enddate=".$endofnextweek->tomysql()."&passbook_id=".$passbook_id);?> | 
<br>
<?php echo link_to("Last Month","voucher/dsrmulti?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql()."&passbook_id=".$passbook_id);?> | 
<?php echo link_to("This Month","voucher/dsrmulti?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql()."&passbook_id=".$passbook_id);?> | 
<?php echo link_to("Next Month","voucher/dsrmulti?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql()."&passbook_id=".$passbook_id);?> | 
<br>
    <?php echo link_to("Yesterday","voucher/dsrmulti?startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql()."&passbook_id=".$passbook_id);?> | 
    <?php echo link_to("Tomorrow","voucher/dsrmulti?startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql()."&passbook_id=".$passbook_id);?> | 
<br>

</form>

<h1>Floating Check Report </h1>
<?php if(!$passbook_id){  ?>
<h2><font color=red>No account selected</font></h2>
<?php }else{ ?>
  <h2>Account: <?php echo $passbook?></h2>
<?php } ?>


<b>Date: <?php echo MyDateTime::frommysql($form->getObject()->getDate())->toshortdate(); ?> to <?php echo MyDateTime::frommysql($toform->getObject()->getDate())->toshortdate(); ?></b>
<br>
<br><b>Total Released Check Amount: <?php echo MyDecimal::format($totalreceived)?></b>
<br>
<br>Total Unreleased Check Amount: <?php echo MyDecimal::format($totalunreceived)?>
<br>Total Cleared Check Amount: <?php echo MyDecimal::format($totalcleared)?>
<br>Total Cancelled Check Amount: <?php echo MyDecimal::format($totalcancelled)?>
<br>
<?php 
$date=$form->getObject()->getDate();
$todate=$toform->getObject()->getDate();
echo link_to("Print","voucher/dsrmultipdf?startdate=".$date."&enddate=".$todate);?> | 

<br>
<br>
<h3>Released but Uncleared Checks</h3>
<table border=1>
  <tr>
    <td>Check Date</td>
    <td>Ref</td>
    <td>Check No</td>
    <td>Account</td>
    <td>Type</td>
    <td>Pay To</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>
  <?php foreach($received as $voucher){?>
    <?php 
    $check=null;$out_payment=$voucher->getOutPayment(); if($out_payment)$check=$out_payment->getOutCheck();
    //$check=$voucher->getOutCheck();
    ?>
    <tr>
        <td><?php if($check)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
        <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
        <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getPassbook()." ".$check->getCheckNo(); ?></td>
        <td><?php echo $voucher->getVoucherAccount()?></td>
        <td><?php echo $voucher->getVoucherType()?></td>
        <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
        <td><?php echo MyDecimal::format($voucher->getAmount());?></td>
        <td><?php echo $voucher->getParticulars()?></td>
        <td><?php echo $voucher->getNotes()?></td>
    </tr>  
<?php }?>
</table>


<br>
<br>
<h3>Unreleased Checks</h3>
<table border=1>
  <tr>
    <td>Check Date</td>
    <td>Ref</td>
    <td>Check No</td>
    <td>Account</td>
    <td>Type</td>
    <td>Pay To</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>
  <?php foreach($unreceived as $voucher){?>
    <?php 
    $check=null;$out_payment=$voucher->getOutPayment(); if($out_payment)$check=$out_payment->getOutCheck();
    //$check=$voucher->getOutCheck();
    ?>
    <tr>
        <td><?php if($check)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
        <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
        <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getPassbook()." ".$check->getCheckNo(); ?></td>
        <td><?php echo $voucher->getVoucherAccount()?></td>
        <td><?php echo $voucher->getVoucherType()?></td>
        <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
        <td><?php echo MyDecimal::format($voucher->getAmount());?></td>
        <td><?php echo $voucher->getParticulars()?></td>
        <td><?php echo $voucher->getNotes()?></td>
    </tr>  
<?php }?>
</table>

<br>
<br>
<h3>Cleared Checks</h3>
<table border=1>
  <tr>
    <td>Check Date</td>
    <td>Ref</td>
    <td>Check No</td>
    <td>Account</td>
    <td>Type</td>
    <td>Pay To</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>
  <?php foreach($cleared as $voucher){?>
    <?php 
    $check=null;$out_payment=$voucher->getOutPayment(); if($out_payment)$check=$out_payment->getOutCheck();
    //$check=$voucher->getOutCheck();
    ?>
    <tr>
        <td><?php if($check)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
        <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
        <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getPassbook()." ".$check->getCheckNo(); ?></td>
        <td><?php echo $voucher->getVoucherAccount()?></td>
        <td><?php echo $voucher->getVoucherType()?></td>
        <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
        <td><?php echo MyDecimal::format($voucher->getAmount());?></td>
        <td><?php echo $voucher->getParticulars()?></td>
        <td><?php echo $voucher->getNotes()?></td>
    </tr>  
<?php }?>
</table>


<br>
<br>
<h3>Cancelled Checks</h3>
<table border=1>
  <tr>
    <td>Check Date</td>
    <td>Ref</td>
    <td>Check No</td>
    <td>Account</td>
    <td>Type</td>
    <td>Pay To</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>
  <?php foreach($cancelled as $voucher){?>
    <?php 
    $check=null;$out_payment=$voucher->getOutPayment(); if($out_payment)$check=$out_payment->getOutCheck();
    //$check=$voucher->getOutCheck();
    ?>
    <tr>
        <td><?php if($check)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
        <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
        <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getPassbook()." ".$check->getCheckNo(); ?></td>
        <td><?php echo $voucher->getVoucherAccount()?></td>
        <td><?php echo $voucher->getVoucherType()?></td>
        <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
        <td><?php echo MyDecimal::format($voucher->getAmount());?></td>
        <td><?php echo $voucher->getParticulars()?></td>
        <td><?php echo $voucher->getNotes()?></td>
    </tr>  
<?php }?>
</table>

