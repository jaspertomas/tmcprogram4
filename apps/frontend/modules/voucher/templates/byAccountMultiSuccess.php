<?php use_helper('I18N', 'Date') ?>
<?php 
//show date form
echo form_tag("voucher/byAccountMulti");
$startDateForm = new sfWidgetFormDate();
$endDateForm = new sfWidgetFormDate();
echo "From ".$startDateForm->render('startdatesplit',$startdate);
echo " to ".$endDateForm->render('enddatesplit',$enddate);
?>
<input type=submit value="View">
</form>
<h1><?php echo 'Expense Detail: '.$account;?></h1>   

Date: <?php echo MyDateTime::frommysql($startdate)->toprettydate(); ?> to <?php echo MyDateTime::frommysql($enddate)->toprettydate(); ?> 


<table border=1>
  <tr>
    <td>Date</td>
    <td>Account</td>
    <td>Type</td>
    <td>Payee</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
    <td>Edit</td>
  </tr>
    <?php foreach($vouchers as $voucher){    ?>
  <tr>
    <td><?php echo $voucher->getDate()?></td>
    <td><?php echo $voucher->getVoucherAccount()?></td>
    <td><?php echo $voucher->getVoucherType()?></td>
    <td><?php echo $voucher->getPayee()?></td>
    <td><?php echo MyDecimal::format($voucher->getAmount())?></td>
    <td><?php echo $voucher->getParticulars()?></td>
    <td><?php echo $voucher->getNotes()?></td>
    <td><?php echo link_to("Edit","voucher/edit?id=".$voucher->getId())?></td>
  </tr>
    <?php }?>
</table>
