<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("L", PDF_UNIT, "GOVERNMENTLEGAL", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Daily Expense Report');

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->startPageGroup();
$pdf->AddPage();

$pettycashvouchers=array();
$nonpettycashvouchers=array();
foreach($vouchers as $voucher)
{
  if($voucher->getVoucherTypeId()==1)//pettycash
    $pettycashvouchers[]=$voucher;
  else
    $nonpettycashvouchers[]=$voucher;
}

//----------HEADER----------------------------
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->write(5,"Expense Report: ".MyDateTime::frommysql($form->getObject()->getDate())->toshortdate(),
'',false,'',true,0,false,false,0,0);

$pdf->SetFont('dejavusans', '', 12, '', true);
$contents=array();
$contents[]=array(
          "Total Petty Cash: ",MyDecimal::format($pettycashtotal),
          );
$contents[]=array(
          "Total Cheque: ",MyDecimal::format($chequetotal),
          );
$contents[]=array(
          "Total Other Expenses: ",MyDecimal::format($othertotal),
          );
$contents[]=array(
          "Total Expenses: ",MyDecimal::format($total),
          );

$widths=array(50,30);
$height=1;
foreach($contents as $content)
{
  $pdf->MultiCell($widths[0], $height, $content[0], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 0, 'R', 0, 1, '', '', true);
}

//-----------PETTY CASH---------------------------
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->write(5,"\nPetty Cash: ",'',false,'',true,0,false,false,0,0);
$pdf->SetFont('dejavusans', '', 12, '', true);

$widths=array(50,35,50,30,110);
$height=5;

$content=array(
    'Account'
    ,'Type'
    ,'Payee'
    ,'Amount'
    ,'Particulars'
          );
$pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[1], $height, $content[1], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[2], $height, $content[2], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[3], $height, $content[3], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[4], $height, $content[4], 1, 'C', 0, 1, '', '', true);

//===============================
foreach($pettycashvouchers as $voucher)
{
      $notesstring="";
      if($voucher->getCheckNo()!="")$notesstring.="; Check No.: ".$voucher->getCheckNo();
      if($voucher->getNotes()!="")$notesstring.="; Notes: ".$voucher->getNotes();
      $content=array(
       $voucher->getVoucherAccount(),
       $voucher->getVoucherType(),
       $voucher->getPayee(),
       MyDecimal::format($voucher->getAmount()),
       $voucher->getParticulars().$notesstring,
      );
      $height=1;
      foreach($content as $index=>$txt) 
      {
        @$numlines=$pdf->getNumLines($txt,$widths[$index],false,true,'','');
        if($height<$numlines)$height=$numlines;
      }
      $height*=6.5;
      $pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[1], $height, $content[1], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[2], $height, $content[2], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[3], $height, $content[3], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[4], $height, $content[4], 1, 'C', 0, 1, '', '', true,0,true);
}

// ---------------------------------------------------------
//-----------NON PETTY CASH---------------------------
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->write(5,"\nOther Expenses: ",'',false,'',true,0,false,false,0,0);
$pdf->SetFont('dejavusans', '', 12, '', true);


$widths=array(50,35,50,30,140);
$height=5;

$content=array(
    'Account'
    ,'Type'
    ,'Payee'
    ,'Amount'
    ,'Particulars'
          );
$pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[1], $height, $content[1], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[2], $height, $content[2], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[3], $height, $content[3], 1, 'C', 0, 0, '', '', true);
$pdf->MultiCell($widths[4], $height, $content[4], 1, 'C', 0, 1, '', '', true);

//===============================
foreach($nonpettycashvouchers as $voucher)
{
      $notesstring="";
      $particularsString="";
      //if($voucher->getCheckNo()!="")$notesstring.="; Check No.: ".$voucher->getCheckNo();
      //if($voucher->getNotes()!="")$notesstring.="; Notes: ".$voucher->getNotes();

      if($voucher->getRefType()=="Purchase")
      {
        $purchase=$voucher->getPurchase();
        if($purchase!=null)
        $particularsString.="PO: ".$purchase->getPono().": ";
      }
      else if($voucher->getRefType()=="CounterReceipt")
      {
        $counter_receipt=$voucher->getCounterReceipt();
        if($counter_receipt!=null)
        $particularsString.="CR: ".$counter_receipt->getCode().": ";
      }
      $particularsString.=$voucher->getParticulars()."; ".$notesstring;

      $content=array(
       $voucher->getVoucherAccount(),
       $voucher->getVoucherType(),
       $voucher->getPayee(),//." ".$voucher->getBiller(),
       MyDecimal::format($voucher->getAmount()),
       $particularsString,
      );
      $height=1;
      foreach($content as $index=>$txt) 
      {
        @$numlines=$pdf->getNumLines($txt,$widths[$index],false,true,'','');
        if($height<$numlines)$height=$numlines;
      }
      $height*=6.5;
      $pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[1], $height, $content[1], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[2], $height, $content[2], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[3], $height, $content[3], 1, 'C', 0, 0, '', '', true,0,true);
      $pdf->MultiCell($widths[4], $height, $content[4], 1, 'C', 0, 1, '', '', true,0,true);

      //extra row for check or bank transfer
      if($voucher->getVoucherType()=="Cheque" or $voucher->getVoucherType()=="Bank Transfer")
      {
        $check=$voucher->getOutPayment()->getOutCheck();
        $statusString=$voucher->getCheckStatus();
        if($statusString=="Cancelled")$statusString="CANCELLED";
        else $statusString="";
        $content=array(
          $statusString,
         "Check Voucher: ".$check->getCode()." | ".
         "Check NO: ".$check->getCheckNo()." | ".
         "Account: ".$check->getPassbook()." | ".
         "Released ".MyDateTime::frommysql($check->getReceiveDate())->toshortdate()." | ".
         "Check Date ".MyDateTime::frommysql($check->getCheckDate())->toshortdate()
        );
        $pdf->MultiCell($widths[0], $height, $content[0], 0, 'C', 0, 0, '', '', true,0,true);
        $pdf->MultiCell($widths[1]+$widths[2]+$widths[3]+$widths[4], $height, $content[1], 1, 'C', 0, 1, '', '', true,0,true);
      
      }

}

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Expenses-'.MyDateTime::frommysql($form->getObject()->getDate())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

