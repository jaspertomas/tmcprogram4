<?php use_helper('I18N', 'Date') ?>
<?php include_partial('voucher/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Replace Voucher', array(), 'messages') ?></h1>

  <?php include_partial('voucher/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('voucher/form_header', array('voucher' => $voucher, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('voucher/form', array('voucher' => $voucher, 'form' => $form, 'checkform' => $checkform, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('voucher/form_footer', array('voucher' => $voucher, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
