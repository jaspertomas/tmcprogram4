<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_purchase_cross_ref_type">
  <div>
    <label for="voucher_purchase_cross_ref_type">PO/CR</label>
    <div class="content">
      <select name="voucher[purchase_cross_ref_type]" id="voucher_purchase_cross_ref_type">
        <option value="Purchase">Purchase</option>
        <option value="CounterReceipt">Counter Receipt</option>
      </select>
    </div>
  </div>
</div>