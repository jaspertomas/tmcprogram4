<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

    <?php echo link_to("Voucher Sorting Wizard","voucher/wizardCategorize?date=".$form->getObject()->getDate()) ?> | 
    <?php echo link_to("Back to Voucher Dashboard","voucher/dashboard?date=".$form->getObject()->getDate()) ?>

<h2>Expense Vouchers: Input Wizard</h2>

<hr>
Write one per line, in the format of "date -- payee -- description -- amount -- account -- subaccount -- allocation -- payment type -- month -- year
"
<br>Example: 2023-12-07 -- wholesale employees -- 2023-12-07 -- 24900 -- Salary & Commission -- Salary - Wholesale Employees -- Wholesale -- Bank Transfer -- 12 -- 2023
<br>Or copy paste from excel sheet with proper format
<br>
<br>
<?php echo form_tag_for($form,"voucher/wizardProcessInput")?>
<input type=hidden id=date name=date value="<?php echo $form->getObject()->getDate()?>">
<input type=submit value=Save>
<br><textarea name=input id=input rows=10 cols=80><?php echo $sf_user->getFlash('meta') ?></textarea>
</form>

