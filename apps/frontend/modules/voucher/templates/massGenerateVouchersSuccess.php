<?php use_helper('I18N', 'Date') ?>
<h1>Mass Generate Vouchers</h1>

<?php echo form_tag("voucher/massGenerateVouchers");?>
<input type=submit name="Submit">
<br>
<textarea rows=25 cols=80 name="content" id="content"><?php echo $content?></textarea>
</form>

<?php foreach($errors as $error){?>
<?php echo $error;?><br>
<?php } ?>

<?php foreach($notices as $notice){?>
<?php echo $notice;?><br>
<?php } ?>

<br>
<br>
Sample
<table border=1>
    <tr>
        <td>Date</td>
        <td>Payee</td>
        <td>Amount</td>
        <td>From Bank</td>
        <td>Particulars</td>
        <td>Notes</td>
        <td>Type</td>
        <td>Allocation</td>
        <td>Account</td>
        <td>Subaccount</td>
        <td>month</td>
        <td>year</td>
    </tr>
    <tr>
        <td>2024-01-16</td>
        <td>Ofel and Arabo</td>
        <td>20,000.00</td>
        <td>MBTC Tradewind Water System</td>
        <td>Loan</td>
        <td></td>
        <td>Bank Transfer</td>
        <td>Bulacan</td>
        <td>Salary & Commission</td>
        <td>Salary - Bulacan</td>
        <td>1</td>
        <td>2024</td>
    </tr>
    <tr>
        <td>2024-03-15</td>
        <td>ofel and arabo</td>
        <td>20,000.00</td>
        <td>MBTC Tradewind Water System</td>
        <td>Loan</td>
        <td></td>
        <td>Bank Transfer</td>
        <td>Bulacan</td>
        <td>Salary & Commission</td>
        <td>Salary - Bulacan</td>
        <td>3</td>
        <td>2024</td>
    </tr>
</table>

<h1>
<?php echo link_to("Voucher Accounts Tree",'voucher_account/tree'); ?>
<br><?php echo link_to("Tradewind Salary",'voucher_subaccount/report?id=8'); ?>
<br><?php echo link_to("Wholesale Salary",'voucher_subaccount/report?id=9'); ?>
<br><?php echo link_to("Voucher Mass Generate",'voucher/massGenerateVouchers'); ?>
<br><?php echo link_to("Salary Segregator",'voucher/segregatePayroll'); ?>
</h1>
