<?php use_helper('I18N', 'Date') ?>
<?php 
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>
<?php 
//show date form
echo form_tag("voucher/dashboardMulti");
$startDateForm = new sfWidgetFormDate();
$endDateForm = new sfWidgetFormDate();
echo "From ".$startDateForm->render('startdatesplit',$startdate);
echo " to ".$endDateForm->render('enddatesplit',$enddate);
?>
<input type=submit value="View">
</form>
<?php echo link_to("Last Month","voucher/dashboardMulti?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","voucher/dashboardMulti?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","voucher/dashboardMulti?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br><?php echo link_to("Yesterday","voucher/dashboardMulti?startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> | 
<?php echo link_to("Tomorrow","voucher/dashboardMulti?startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> | 
<br><?php echo link_to("Back to Voucher Dashboard","voucher/dashboard?date=".$startdate);?>
<br><?php echo link_to("Go to Expense Report for Accounting","voucher/dashboardMultiForAccounting"); ?>

<h1>Expense Report: <?php echo MyDateTime::frommysql($startdate)->toprettydate();?> to <?php echo MyDateTime::frommysql($enddate)->toprettydate();?></h1>

Petty Cash Expenses: <?php echo MyDecimal::format($pettycashtotal)?>
<br>Cheque Expenses: <?php echo MyDecimal::format($chequetotal)?>
<br>Other Expenses: <?php echo MyDecimal::format($othertotal)?>
<br>Total Expenses: <?php echo MyDecimal::format($total)?>
<br><?php //echo link_to("Print","voucher/dashboardPdf?date=".$startdate) ?>

<hr>

<h3>Summary:</h3>
<table border="1">
  <tr>
    <td></td>
    <?php foreach($allocations as $allocation){?>
      <td><?php echo $allocation->getName()?></td>
    <?php } ?>
    <td>Total</td>
  <tr>
<?php foreach($accounts as $account){?>
  <tr>
    <td><?php echo $account?></td>
    <?php foreach($allocations as $allocation){?>
      <td><?php echo MyDecimal::format($accounttotals[$account->getId()][$allocation->getId()])?></td>
    <?php } ?>
    <td><?php echo MyDecimal::format($accounttotals[$account->getId()]["total"])?></td>
  <tr>
<?php } ?>
</table>

<?php foreach($accounts as $account){?>
<hr>
<h3>
<table>
  <tr>
    <td><?php echo $account?><br>Total: <?php echo MyDecimal::format($accounttotals[$account->getId()]["total"])?></td>
  </tr>
</table>
</h3>

<table border=1>
  <tr>
    <td>Date</td>
    <td>Voucher No.</td>
    <td>Type</td>
    <td>Payee</td>
    <td>Amount</td>
    <td>Account</td>
    <td>Allocation</td>
    <td>Particulars</td>
    <td>Notes</td>
    <td>Edit</td>
    <td>Delete</td>
  </tr>
  <?php foreach($accountvouchers[$account->getId()] as $voucher){?>
  <tr>
    <td><?php echo $voucher->getDate()?></td>
    <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
    <td><?php echo $voucher->getVoucherType()?></td>
    <td><?php echo $voucher->getPayee(); 
            // if($voucher->getAmount()>50000)
            //   echo "Questionable: ".MyDecimal::format($voucher->getAmount());
            if($voucher->getIsCancelled())
            echo "; ".MyDecimal::format($voucher->getAmount());
    ?></td>
    <td><?php 
          if($voucher->getIsCancelled())
            echo "<font color=red>Cancelled</font>";
          else
            echo MyDecimal::format($voucher->getAmount());
    ?></td>
    <td><?php echo $voucher->getVoucherAccount()?> <?php if($voucher->getSubaccountId())echo " > ".$voucher->getVoucherSubaccount()?></td>
    <td><?php echo $voucher->getVoucherAllocation()?></td>
    <td><?php echo $voucher->getParticulars()?></td>
    <td><?php echo $voucher->getNotes()?></td>

    <td><?php echo link_to("Edit","voucher/edit?id=".$voucher->getId())?></td>
    <td><?php echo link_to('Delete','voucher/delete?id='.$voucher->getId(), array('method' => 'delete', 'confirm' => 'Delete "'.$voucher->getParticulars().' for '.MyDecimal::format($voucher->getAmount()).'": Are you sure?')) ?></td>
  </tr>
  <?php }?>
</table>
<?php }?>



