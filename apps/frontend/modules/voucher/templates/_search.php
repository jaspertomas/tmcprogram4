<?php use_helper('I18N', 'Date'); ?>
<h2><?php echo link_to("Back","voucher/dashboard")?></h2>
<h1>Voucher Search</h1>
<?php if(count($vouchers)==0){?>
  <tr>
    <td>No vouchers found</td>
  </tr>
<?php }else{?>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Voucher No.</td>
    <td>Account</td>
    <td>Type</td>
    <td>Payee</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
    <td>Edit</td>
    <td>Delete</td>
  </tr>
    <?php foreach($vouchers as $voucher){    ?>
  <tr>
    <td><?php echo MyDateTime::frommysql($voucher->getDate())->toshortdate()?></td>
    <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
    <td><?php echo $voucher->getVoucherAccount()?></td>
    <td><?php echo $voucher->getVoucherType()?></td>
    <td><?php echo $voucher->getPayee()?></td>
    <td><?php echo $voucher->getAmount()?></td>
    <td><?php echo $voucher->getParticulars()?></td>
    <td><?php echo $voucher->getNotes()?></td>
    <td><?php echo link_to("Edit","voucher/edit?id=".$voucher->getId())?></td>
    <td><?php echo link_to('Delete','voucher/delete?id='.$voucher->getId(), array('method' => 'delete', 'confirm' => 'Delete "'.$voucher->getParticulars().' for '.MyDecimal::format($voucher->getAmount()).'": Are you sure?')) ?></td>
  </tr>
    <?php }?>
</table>
<?php } ?>
