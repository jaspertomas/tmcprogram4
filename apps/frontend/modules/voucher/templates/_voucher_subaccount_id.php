<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_voucher_subaccount_id">
  <div>
    <label for="voucher_subaccount_id">Sub Account</label>
    <div class="content" id="voucher_subaccount_div">
      
    </div>
    <input type="hidden" id="voucher_subaccount_id" name="voucher[subaccount_id]" value="<?php echo $form->getObject()->getSubaccountId();?>">
  </div>
</div>

<script>
var subaccount_id=$("#voucher_subaccount_id").val();

//on page load, display its buttons
getSubaccountData(subaccount_id);

//a subaccount button was pressed
function selectSubaccount(id) {
  //set form field voucher[subaccount_id] to selected subaccount's id
  $("#voucher_subaccount_id").val(id);
  //display subaccount buttons
  getSubaccountData(id);
};

//gets name, id, children and ancestors of selected subaccount for display of buttons
//id=selected (button pressed) subaccount_id
function getSubaccountData(id) {

  var url="";

  //if id is blank
  //load data using account_id 
  if(id==null || id=="")
  {
    //get subaccounts from point of view of a voucher account
    var account_id=$("#voucher_account_id").val();
    url="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/voucher_account/getSubaccounts?id="+account_id;
  }
  else
  {
    //get ancestors and children from point of view of a voucher subaccount
    url="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/voucher_subaccount/getAncestorsAndChildren?id="+id;
  }

  $.ajax({url: url, success: function(result){
    var obj = jQuery.parseJSON( result );
    var output="";
    output+="<input type=button value='None' onClick=selectSubaccount(0);><br>";

    for (let i = 0; i < obj["ancestors"].length; i++) 
    {
      output+="<input type=button value='"+obj["ancestors"][i]["name"]+"' onClick=selectSubaccount("+obj["ancestors"][i]["id"]+");> > ";
    }

    output+=obj["name"];
    if(obj["children"].length>0)output+=" > ";

    for (let i = 0; i < obj["children"].length; i++) 
    {
      output+="<br> - <input type=button value='"+obj["children"][i]["name"]+"' onClick=selectSubaccount("+obj["children"][i]["id"]+");> ";
    }

    $("#voucher_subaccount_div").html(output);
  }});
};

//on account id change, update voucher_subaccount buttons
$( "#voucher_account_id" ).change(function() {
  getSubaccountData(null);
});
</script>