<?php
$startdate=MyDateTime::frommysql($startdateform->getObject()->getDate())->tomysql();
$enddate=MyDateTime::frommysql($enddateform->getObject()->getDate())->tomysql();
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
$startofthisyear=$today->getstartofyear();
$startofnextyear=$startofthisyear->addyears(1);
$startoflastyear=$startofthisyear->addyears(-1);
$endofthisyear=$startofthisyear->getendofyear();
$endofnextyear=$startofnextyear->getendofyear();
$endoflastyear=$startoflastyear->getendofyear();
$currentyear=$today->getYear();
$currentmonth=$today->getMonth();
?>

<?php echo form_tag_for(new InvoiceForm(),"voucher/passbookReport")?>

Bank Account: <?php
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'Passbook',
  'add_empty' => true,
));
echo $w->render('passbook_id',(isset($passbook)?$passbook->getId():null));
?><br>


<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $startdateform["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $enddateform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Year","voucher/passbookReport?passbook_id=".$passbook_id."&startdate=".$startoflastyear->tomysql()."&enddate=".$endoflastyear->tomysql());?> | 
<?php echo link_to("This Year","voucher/passbookReport?passbook_id=".$passbook_id."&startdate=".$startofthisyear->tomysql()."&enddate=".$endofthisyear->tomysql());?> | 
<?php echo link_to("Next Year","voucher/passbookReport?passbook_id=".$passbook_id."&startdate=".$startofnextyear->tomysql()."&enddate=".$endofnextyear->tomysql());?> | 
<br>
<?php echo link_to("Last Month","voucher/passbookReport?passbook_id=".$passbook_id."&startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","voucher/passbookReport?passbook_id=".$passbook_id."&startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","voucher/passbookReport?passbook_id=".$passbook_id."&startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br>
    <?php echo link_to("Yesterday","voucher/passbookReport?passbook_id=".$passbook_id."&startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> | 
    <?php echo link_to("Tomorrow","voucher/passbookReport?passbook_id=".$passbook_id."&startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> | 
<br>


<h1>Outgoing Checks Monitoring </h1>
<h2>Account: <?php echo $passbook->getName();?></h2>
<h3>
  From <?php echo MyDateTime::frommysql($startdate)->toprettydate();?>
  To <?php echo MyDateTime::frommysql($enddate)->toprettydate();?>
</h3>
<?php //echo link_to("Print","voucher/dashboardPdf?date=".$form->getObject()->getDate()) ?>

<hr>

<table border=1>
  <tr>
    <td>Voucher Date</td>
    <td>Pay To</td>
    <td>Particulars</td>
    <td>Check No.</td>
    <td>Check Date</td>
    <td>Amount</td>
    <td>Voucher</td>
    <td>Status</td>
    <td>Voucher Account</td>
  </tr>
    <?php foreach($vouchers as $voucher){ ?>
      <?php $out_payment=$voucher->getOutPayment(); if($out_payment){$check=$out_payment->getOutCheck();?>
  <tr>
    <td><?php echo MyDateTime::frommysql($voucher->getDate())->toshortdate()?></td>
    <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
    <td><?php echo $voucher->getParticulars()?></td>
    <td><?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo(); ?></td>
    <td><?php echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
    <td><?php echo MyDecimal::format($voucher->getAmount());?></td>
    <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
    <td><div <?php if($voucher->getStatus()=="Cancelled")echo 'style="color:red"'?>><?php echo $voucher->getStatus() ?></div></td>
    <td><?php echo $voucher->getVoucherAccount()?></td>
  </tr>
      <?php }?>
    <?php }?>
</table>

