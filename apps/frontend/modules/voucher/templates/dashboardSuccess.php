Search Voucher:	<input id=vouchersearchinput autocomplete="off" size=12> 
<div id="vouchersearchresult"></div>
<hr>
<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for($form,"voucher/dashboard")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
?>
<?php echo link_to("Voucher Input Wizard","voucher/wizardInput?date=".$form->getObject()->getDate()) ?> | 
<?php echo link_to("Voucher Sorting Wizard","voucher/wizardCategorize?date=".$form->getObject()->getDate()) ?> | 
    <?php echo link_to("Expense Report","voucher/dashboardMulti?startdate=".$form->getObject()->getDate()."&enddate=".$form->getObject()->getDate()); ?>
<table>
  <tr>
    <td>Date</td>
    <td><?php echo $form["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
</form>
    <?php echo link_to("Yesterday","voucher/dashboard?voucher[date][day]=".$yesterday->getDay()."&voucher[date][month]=".$yesterday->getMonth()."&voucher[date][year]=".$yesterday->getYear()); ?>
    <?php echo link_to("Tomorrow","voucher/dashboard?voucher[date][day]=".$tomorrow->getDay()."&voucher[date][month]=".$tomorrow->getMonth()."&voucher[date][year]=".$tomorrow->getYear()); ?>


<h1>Expense Vouchers: <?php echo MyDateTime::frommysql($form->getObject()->getDate())->toprettydate();?></h1>

Petty Cash Expenses: <?php echo MyDecimal::format($pettycashtotal)?>
<br>Cheque Expenses: <?php echo MyDecimal::format($chequetotal)?>
<br>Other Expenses: <?php echo MyDecimal::format($othertotal)?>
<br>Total Expenses: <?php echo MyDecimal::format($total)?>
<br><?php echo link_to("Print","voucher/dashboardPdf?date=".$form->getObject()->getDate()) ?>

<hr>
<b>Search</b>
  <table>
    <tr>
      <td>Search by Payee: </td>
      <td>
        <?php echo form_tag_for($form,"voucher/searchByPayee")?>
        <input id=searchstring name=searchstring autocomplete="off" size=12> 
        </form>
      </td>
    </tr>
    <tr>
      <td>Search by Amount: </td>
      <td>
        <?php echo form_tag_for($form,"voucher/searchByAmount")?>
        <input id=searchstring name=searchstring autocomplete="off" size=12> 
        </form>
      </td>
    </tr>
    <tr>
      <td>Search by Particulars: </td>
      <td>
        <?php echo form_tag_for($form,"voucher/searchByParticulars")?>
        <input id=searchstring name=searchstring autocomplete="off" size=12> 
        </form>
      </td>
    </tr>
    <tr>
      <td>Search by Check Date: </td>
      <td>
        <?php echo form_tag_for($form,"voucher/searchByCheckDate")?>
        <?php echo $dateform["check_date"]?>
        <input type=submit value=Search >
        </form>
      </td>
    </tr>
    <tr>
      <td>Search by Check No.: </td>
      <td>
        <?php echo form_tag_for($form,"voucher/searchByCheckNo")?>
        <input id=searchstring name=searchstring autocomplete="off" size=12> 
        </form>
      </td>
    </tr>
    <tr>
      <td>Search by Voucher No.: </td>
      <td>
        <?php echo form_tag_for($form,"voucher/searchByVoucherNo")?>
        <input id=searchstring name=searchstring autocomplete="off" size=12> 
        </form>
      </td>
    </tr>
  </table>

<hr>
<b>Create New Voucher: Choose Account</b>

<table>
<?php for($j=0;$j<$columncount;$j++){ ?>
<tr>
  <?php for($i=0;$i<$accountspergroup;$i++){ ?>
  <td>
    <?php $account=$accountgroups[$j][$i];if($account->getId())echo link_to($account,"voucher/new?account_id=".$account->getId()."&date=".$form->getObject()->getDate());?>
  </td>
  <?php }?>
</tr>
<?php }?>
</table>

<hr>

<table border=1>
  <tr>
    <td>Date</td>
    <td>Ref</td>
    <td>Account</td>
    <td>Allocation</td>
    <td>Type</td>
    <td>Pay To</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
    <td>Status</td>
    <td>Checked</td>
  </tr>
    <?php foreach($forms as $form){$voucher=$form->getObject();    ?>
  <tr>
    <td><?php echo MyDateTime::frommysql($voucher->getDate())->toshortdate()?></td>
    <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
    <td><?php echo $voucher->getVoucherAccount()?> <?php if($voucher->getSubaccountId())echo " > ".$voucher->getVoucherSubaccount()?></td>
    <td><?php echo $voucher->getVoucherAllocation()?></td>
    <td><?php echo $voucher->getVoucherType()?></td>
    <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
    <td>
      <?php /*echo form_tag_for($form,"voucher/editAmount")?>
      <?php echo $form['id']?>
      <?php echo $form['amount']?>
      </form>
      */
      echo MyDecimal::format($voucher->getAmount());
      ?>
      
    </td>
    <td><?php echo $voucher->getParticulars()?></td>
    <td><?php echo $voucher->getNotes()?></td>
    <td><div <?php if($voucher->getStatus()=="Cancelled")echo 'style="color:red"'?>><?php echo $voucher->getStatus() ?></div></td>
    <td><font color=green><?php if($voucher->getIsChecked()){?>Checked<?php } ?></font></td>
  </tr>
  
      <?php $out_payment=$voucher->getOutPayment(); if($out_payment){$check=$out_payment->getOutCheck();if($check){?>
        <tr bgcolor="#dd0">
          <td bgcolor="#eee"></td>
          <td bgcolor="#eee"></td>
          <td
          <?php if($voucher->getRefType()!="")echo "bgcolor='#dd0'"?>>
            <?php /*echo form_tag_for($form,"voucher/editNo")?>
            <?php echo $form['id']?>
            <?php echo $form['no']?>
            </form>
            */
            if($voucher->getRefType()=="Purchase")
              {
                $purchase=$voucher->getPurchase();
                echo link_to("PO: ".$purchase->getPono(),"purchase/view?id=".$purchase->getId()) ;
              }
            else if($voucher->getRefType()=="CounterReceipt")
              {
                $counter_receipt=$voucher->getCounterReceipt();
                if($counter_receipt!=null)
                echo link_to("CR: ".$counter_receipt->getCode(),"counter_receipt/view?id=".$counter_receipt->getId()) ;
              }
            ?>      
          </td>
          <td>Check No<br> <?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo(); ?></td>
          <td>Account<br> <?php echo $check->getPassbook()?></td>
          <td>
              <font <?php 
              switch($voucher->getCheckStatus())
              {
                case "Cleared":echo "color=blue";break;
                case "Released":echo "color=green";break;
                case "Cancelled":echo "color=red";break;
                default: echo "color=black";break;
              }
              ?>><?php echo $voucher->getCheckStatus() ?></font>
            <?php //echo MyDecimal::format($check->getAmount())?>
          </td>
          <td colspan=2>
            <?php if($check->getReceiveDate()!=null)echo "Release Date ".MyDateTime::frommysql($check->getReceiveDate())->toshortdate()?>
            <br><?php if($check->getClearedDate()!=null)echo "Clear Date ".MyDateTime::frommysql($check->getClearedDate())->toshortdate()?>
          </td>
          <td></td>
          <td></td>
        </tr>
        <tr><td colspan=10></td></tr>
      <?php }?>
      <?php }?>
    <?php }?>
</table>

<script>
$("#vouchersearchinput").keyup(function(event){
	//if 3 or more letters in search box
    //if($("#vouchersearchinput").val().length>=3){

    //if enter key is pressed
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
	    $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/voucher/search?searchstring="+$("#vouchersearchinput").val()+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	 		  $("#vouchersearchresult").html(result);
	    }});
    }
    //else clear
    //else
 		//  $("#searchresult").html("");
});

</script>
