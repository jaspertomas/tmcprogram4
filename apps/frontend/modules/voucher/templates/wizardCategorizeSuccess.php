<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for($form,"voucher/wizardCategorize")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
?>
<table>
  <tr>
    <td>Date</td>
    <td><?php echo $form["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
</form>
    <?php echo link_to("Yesterday","voucher/wizardCategorize?voucher[date][day]=".$yesterday->getDay()."&voucher[date][month]=".$yesterday->getMonth()."&voucher[date][year]=".$yesterday->getYear()); ?> | 
    <?php echo link_to("Tomorrow","voucher/wizardCategorize?voucher[date][day]=".$tomorrow->getDay()."&voucher[date][month]=".$tomorrow->getMonth()."&voucher[date][year]=".$tomorrow->getYear()); ?> | 
    <?php echo link_to("Voucher Input Wizard","voucher/wizardInput?date=".$form->getObject()->getDate()) ?> | 
    <?php echo link_to("Back to Voucher Dashboard","voucher/dashboard?date=".$form->getObject()->getDate()) ?>


<h2>Expense Vouchers: Sorting Wizard: <?php echo MyDateTime::frommysql($form->getObject()->getDate())->toprettydate();?></h2>

<?php if(!$voucher){//if voucher is null?>

There are no more vouchers that need to be sorted for today
<br><?php echo link_to("Search other dates","voucher/wizardSearchCategorize");?>
<br><?php echo link_to("Go back to Dashboard","voucher/dashboard?date=".$form->getObject()->getDate());?>

<?php }else{ //else voucher is not null?>

Please choose account for: 
<br>
<b>Voucher <?php echo $voucher->getNo()?></b>: 
<b>P<?php echo MyDecimal::format($voucher->getAmount())?></b>
 for <b><?php echo $voucher->getParticulars()?></b>
 by <b><?php echo $voucher->getPayee()?></b>
 on <b><?php echo $voucher->getDate()?></b>
(<?php echo link_to("Edit","voucher/edit?id=".$voucher->getId())?>)
<br>
<br>

<?php echo form_tag_for($form,"voucher/wizardProcessCategorize")?>
<input type=hidden name=id value="<?php echo $voucher->getId()?>" >
<input type=hidden name=date value="<?php echo $form->getObject()->getDate()?>" >
<table>
<?php for($i=0;$i<$accountspergroup;$i++){ ?>
<tr>
  <?php for($j=0;$j<$columncount;$j++){ ?>
  <td>
    <?php $account=$accountgroups[$j][$i];if($account->getId()){?>
    <input type=submit name=submit value="<?php echo $account->getName()?>">    
    <?php }?>
  </td>
  <?php }?>
</tr>
<?php }?>
</table>
</form>

<?php }//end voucher is not null ?>

