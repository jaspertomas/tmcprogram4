<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name').' Billing Statement for '.$voucher->getNo());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================


// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
//$pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 90, '', 'PNG', '', '', false, 300, 'L', false, false, 0, false, false, false);
$pdf->SetFont('helvetica', '', 14, '', true);
//$pdf->write(0,"Tradewind Visayas Corporation",'',false,'R',true,0,false,false,0,0);

$widths=array(500);
$content=array(
  sfConfig::get('custom_company_header_text'),
  sfConfig::get('custom_company_phone'),
  sfConfig::get('custom_company_email'),
);

$tbl="";
$tbl .= <<<EOD
<table border=1>
 <tr>
  <td width="$widths[0]"><font size="18">$content[0]</font></td>
 </tr>
 <tr>
  <td width="$widths[0]">Phone: $content[1]</td>
 </tr>
</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');
 
//$pdf->write(0,"                                                                          ".sfConfig::get('custom_company_tin'),'',false,'L',true,0,false,false,0,0);

// $pdf->write(0,"                                                                                                     ".sfConfig::get('custom_company_phone'),'',false,'L',true,0,false,false,0,0);
// $pdf->write(0,"                                                                                                        ".sfConfig::get('custom_company_email'),'',false,'L',true,0,false,false,0,0);
// $pdf->write(0,"                                                                                                        ".sfConfig::get('custom_company_tin'),'',false,'L',true,0,false,false,0,0);

$pdf->SetFont('helvetica', '', 12, '', true);
$pdf->write(5,"\n");

$tbl = "";

//-------------------

$refString="";
if($voucher->getRefType()=="Purchase")
{
  $refString="PO: ".$voucher->getPurchase()->getPono();
}
else if($voucher->getRefType()=="CounterReceipt")
{
  $refString="CR: ".$voucher->getCounterReceipt()->getCode();
}

$no=  $voucher->getNo();
$passbook=$check->getPassbook();
$content=array(
  MyDateTime::frommysql($voucher->getDate())->toshortdate(),
  "",//$check->getId()==null ? "" : MyDateTime::frommysql($check->getReceiveDate())->toshortdate(),
  $check->getId()==null ? "" : MyDateTime::frommysql($check->getCheckDate())->toshortdate(),
  $passbook." (".$passbook->getAccountNo().")",
  $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo(),
  MyDecimal::format($voucher->getAmount()),
  $refString,
  $voucher->getPayee(),
  $voucher->getParticulars(),
  $voucher->getVoucherAccount(),
  $voucher->getVoucherType(),
  $voucher->getNotes(),
  );


$tbl .= <<<EOD
<table>
  <tr>
    <td width="375"><h2>Check No $content[4]</h2></td>
    <td width="300"><h2>Expense Voucher $no</h2></td>
  </tr>
  <tr><td></td><td></td></tr>
  <tr>
    <td width="150"><h3>Account</h3></td>
    <td width="525"><h3>$content[3]</h3></td>
  </tr>
  <tr><td></td><td></td></tr>
  <tr>
    <td width="150"><h3>Amount</h3></td>
    <td width="225"><h3>P$content[5]</h3></td>
    <td width="150">Ref</td>
    <td width="150">$content[6]</td>
  </tr>
  <tr><td></td><td></td></tr>
  <tr>
    <td width="150"><h3>Pay To</h3></td>
    <td width="600"><h3>$content[7]</h3></td>
  </tr>
  <tr><td></td><td></td></tr>
  <tr>
    <td width="150">Check Clear Date </td>
    <td width="225">$content[2]</td>
    <td width="150">Created on</td>
    <td width="150">$content[0]</td>
  </tr>
  <tr><td></td><td></td></tr>
  <tr>
    <td>Particulars</td>
    <td width="525">$content[8]</td>
  </tr>
  <tr><td></td><td></td></tr>
  <tr>
    <td width="150">Expense Account</td>
    <td width="225">$content[9]</td>
    <td width="150">Payment Type</td>
    <td width="150">$content[10]</td>
  </tr>
  <tr><td></td><td></td></tr>
  <tr>
    <td>Notes</td>
    <td width="525">$content[11]</td>
  </tr>
</table>
EOD;

$tbl .= <<<EOD
<br>
<table>
<tr valign="bottom">
  <td align="center"><br><br>_______________</td>
  <td align="center"><br><br>_______________</td>
  <td align="center"><br><br>_______________</td>
</tr>
<tr>
  <td align="center">Cheque Prepared By</td>
  <td align="center">Cheque Released By:</td>
  <td align="center">Cheque Received By</td>
 </tr>
</table>
EOD;


$pdf->writeHTML($tbl, true, false, false, false, '');

$style = array(
	'border' => false,
	'padding' => false,
	// 'fgcolor' => array(128,0,0),
	// 'bgcolor' => false
);

// QRCODE,Q : QR-CODE Better error correction
$filler=100-($voucher->getId()%100);
$pdf->write2DBarcode('vou;'.$voucher->getId().";".$filler.";".sfConfig::get('custom_server_code'), 'QRCODE,L', 170, 5, 23, 23, $style, 'N');
//--------------------------------------------------

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$templatenamestripped=str_replace(" ","",$templatename);
$pdf->Output($templatenamestripped.$voucher->getNo().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

