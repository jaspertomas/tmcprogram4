<?php

require_once dirname(__FILE__).'/../lib/voucherGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/voucherGeneratorHelper.class.php';

/**
 * voucher actions.
 *
 * @package    sf_sandbox
 * @subpackage voucher
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class voucherActions extends autoVoucherActions
{
  public function executeView(sfWebRequest $request)
  {
    $this->voucher=$this->getRoute()->getObject();
    $this->check=$this->voucher->getOutCheck();
    $this->out_payment=$this->voucher->getOutPayment(); 
    if($this->out_payment){$this->check=$this->out_payment->getOutCheck();}
    $this->cross_ref_system_silent_mode=sfConfig::get('custom_cross_ref_system_silent_mode');
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');

    $this->voucher=Fetcher::fetchOne("Voucher",array("id"=>$this->voucher->getId()));
    $this->voucher->setPrintCount($this->voucher->getPrintCount()+1);
    $this->voucher->setPrintDate(MyDate::today());
    $this->voucher->save();
  }
  public function executeViewPdfOld(sfWebRequest $request)
  {
    $this->executeView($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');

    $this->voucher=Fetcher::fetchOne("Voucher",array("id"=>$this->voucher->getId()));
    $this->voucher->setPrintCount($this->voucher->getPrintCount()+1);
    $this->voucher->setPrintDate(MyDate::today());
    $this->voucher->save();
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->voucher = new Voucher();
    $this->voucher->setDate(MyDate::today());
    $this->voucher->setTime(date('h:i a'));
    //set account if specified
    if($request->getParameter("date"))
    {
      $this->voucher->setDate($request->getParameter("date"));
    }
    if($request->getParameter("account_id"))
    {
      //special conditions: Merchandise Expense vouchers not allowed to be created here
      //go use the link in the PO or counter receipt
      $merchandiseExpenseAccount= Doctrine_Query::create()
        ->from('VoucherAccount a')
      	->where('name = "Merchandise Expense"')
      	->fetchOne();
      if($request->getParameter("account_id")==$merchandiseExpenseAccount->getId())
      {
        $message="To create Merchandise Expense checks, please go to the Purchase Order or Counter Receipt and click the Generate Check Voucher link";
        return $this->redirect("home/error?msg=".$message);
      }

      //check if account has subaccounts
      $has_subaccounts=Fetcher::fetchOne("VoucherSubaccount",array("voucher_account_id"=>$request->getParameter("account_id")));
      if($has_subaccounts)
      {
        //redirect to wizard
        return $this->redirect("voucher_account/newVoucher?account_id=".$request->getParameter("account_id")."&date=".$request->getParameter("date"));
      }
      else
      {
        $this->voucher->setAccountId($request->getParameter("account_id"));
      }
    }
    if($request->getParameter("purchase_id"))
    {
      $purchase=Fetcher::fetchOne("Purchase",array("id"=>$request->getParameter("purchase_id")));

      //validate purchase is not fully paid
      $balance=$purchase->calcBalance();
      if($balance==0)
      {
        $message="PO ".$purchase->getPono()." is already fully paid";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }

      $account=Fetcher::fetchOne("VoucherAccount",array("name"=>"\"Merchandise Expense\""));
      $this->voucher->setAccountId($account->getId());
      $this->voucher->setDate(MyDate::today());
      $this->voucher->setParticulars($purchase->getParticularsString());
      $this->voucher->setAmount($balance);
      $this->voucher->setPayee($purchase->getVendor()->getName());
      $this->voucher->setVoucherTypeId(2);
      $this->voucher->setRefType("Purchase");
      $this->voucher->setRefId($purchase->getId());
      $this->voucher->setClientType("Vendor");
      $this->voucher->setClientId($purchase->getVendorId());
    }
    else if($request->getParameter("counter_receipt_id"))
    {
      $counter_receipt=Fetcher::fetchOne("CounterReceipt",array("id"=>$request->getParameter("counter_receipt_id")));

      //validate counter receipt is not fully paid
      if($counter_receipt->getBalance()==0)
      {
        $message="This counter receipt is already fully paid";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }

      $account=Fetcher::fetchOne("VoucherAccount",array("name"=>"\"Merchandise Expense\""));
      $this->voucher->setAccountId($account->getId());
      $this->voucher->setDate(MyDate::today());
      $this->voucher->setParticulars("POs ".$counter_receipt->getParticularsString());
      $this->voucher->setAmount($counter_receipt->getBalance());
      $this->voucher->setPayee($counter_receipt->getVendor()->getName());
      $this->voucher->setVoucherTypeId(2);
      $this->voucher->setRefType("CounterReceipt");
      $this->voucher->setRefId($counter_receipt->getId());
      $this->voucher->setClientType("Vendor");
      $this->voucher->setClientId($counter_receipt->getVendorId());
    }
    else if($request->getParameter("commission_payment_id"))
    {
      $commission_payment=Fetcher::fetchOne("CommissionPayment",array("id"=>$request->getParameter("commission_payment_id")));

      /*
      //validate counter receipt is not fully paid
      if($commission_payment->getBalance()==0)
      {
        $message="This counter receipt is already fully paid";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
      */

      $account=Fetcher::fetchOne("VoucherAccount",array("name"=>"\"Salary & Commission\""));
      $this->voucher->setAccountId($account->getId());
      $this->voucher->setDate(MyDate::today());
      $startdate=MyDateTime::frommysql($commission_payment->getStartdate());
      $startdatestring=$startdate->getshortmonth()." ".$startdate->getyear();
      $enddate=MyDateTime::frommysql($commission_payment->getEnddate());
      $enddatestring=$enddate->getshortmonth()." ".$enddate->getyear();
      if($startdatestring==$enddatestring)
        $datestring=$startdatestring;
      else
        $datestring=$startdatestring." to ".$enddatestring;
      $this->voucher->setParticulars($commission_payment->getEmployee()->getName()." Sales Commission ".$datestring);
      $this->voucher->setAmount($commission_payment->getBalance());
      $this->voucher->setPayee($commission_payment->getEmployee()->getName());
      $this->voucher->setVoucherTypeId(2);//Cheque
      $this->voucher->setRefType("CommissionPayment");
      $this->voucher->setRefId($commission_payment->getId());
      $this->voucher->setClientType("Employee");
      $this->voucher->setClientId($commission_payment->getEmployeeId());
    }
    else if($request->getParameter("invoice_id"))
    {
      $invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("invoice_id")));

      $account=Fetcher::fetchOne("VoucherAccount",array("name"=>"\"Customer Refund\""));
      $this->voucher->setAccountId($account->getId());
      $this->voucher->setDate(MyDate::today());
      $this->voucher->setParticulars("Refund for ".$invoice);
      $this->voucher->setAmount(-$invoice->getBalance());
      $this->voucher->setPayee($invoice->getCustomer()->getName());
      $this->voucher->setVoucherTypeId(2);//Cheque
      $this->voucher->setRefType("Invoice");
      $this->voucher->setRefId($invoice->getId());
      $this->voucher->setClientType("Customer");
      $this->voucher->setClientId($invoice->getCustomerId());
    }
    /*
    else if($request->getParameter("invoice_return_id"))
    {
      $invoice_return=Fetcher::fetchOne("InvoiceReturn",array("id"=>$request->getParameter("invoice_return_id")));
      $invoice=$invoice_return->getInvoice();

      $account=Fetcher::fetchOne("VoucherAccount",array("name"=>"\"Refund / Fund Transfer\""));
      $this->voucher->setAccountId($account->getId());
      $this->voucher->setDate(MyDate::today());
      $this->voucher->setParticulars("Refund for ".$invoice);
      $this->voucher->setAmount($invoice->getBalance());
      $this->voucher->setPayee($invoice->getCustomer()->getName());
      $this->voucher->setVoucherTypeId(2);//Cheque
      $this->voucher->setRefType("InvoiceReturn");
      $this->voucher->setRefId($invoice_return->getId());
      $this->voucher->setClientType("Customer");
      $this->voucher->setClientId($invoice->getCustomerId());
    }
    */
    
    $this->form = $this->configuration->getForm($this->voucher);

/*
    //restrict choice of billers according to biller type
    $billers= Doctrine_Query::create()
      ->from('Biller b')
      ->where('b.biller_type_id ='.$biller_type_id)
      ->execute();
    $billersarray=array();
    foreach($billers as $t)$billersarray[$t->getId()]=$t->getName();
    $widgetschema=$this->form->getWidgetSchema();
    $widgetschema['biller_id']=new sfWidgetFormChoice(array('choices' => $billersarray));
*/

    $this->check=new OutCheck();
    $this->check->setCheckDate(MyDate::today());
    $this->check->setReceiveDate(MyDate::today());
    $this->checkform=new OutCheckForm($this->check);
  }
  public function executeEdit(sfWebRequest $request)
  {
    $this->voucher = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->voucher);

    //create checkform from new check
    $this->check=new OutCheck();
    $this->check->setCheckDate(MyDate::today());
    $this->check->setReceiveDate(MyDate::today());
    $this->checkform=new OutCheckForm($this->check);
    
    //if check exists, use it in checkform
    $payment=$this->voucher->getOutPayment();
    if($payment!=null)
    {
      $this->check=$payment->getOutCheck();
      if($this->check)
        $this->checkform=new OutCheckForm($this->check);
    }
  }
  public function executeByAccountMulti(sfWebRequest $request)
  {
    //startdate
    if($request->hasParameter("startdate"))
    {
      $startdate=$request->getParameter("startdate");
    }
    elseif($request->hasParameter("startdatesplit"))
    {
      $requestparams=$request->getParameter("startdatesplit");
      $day=$requestparams["day"];
      $month=$requestparams["month"];
      $year=$requestparams["year"];
      $startdate=$year."-".$month."-".$day;
    }
    else
    {
      $startdate=MyDate::today();
    }
    $this->startdate=$startdate;

    //enddate
    if($request->hasParameter("enddate"))
    {
      $enddate=$request->getParameter("enddate");
    }
    elseif($request->hasParameter("enddatesplit"))
    {
      $requestparams=$request->getParameter("enddatesplit");
      $day=$requestparams["day"];
      $month=$requestparams["month"];
      $year=$requestparams["year"];
      $enddate=$year."-".$month."-".$day;
    }
    else
    {
      $enddate=MyDate::today();
    }
    $this->enddate=$enddate;
    
    $this->account= Doctrine_Query::create()
        ->from('Account a')
      	->where('a.id = '.$request->getParameter("account_id"))
      	->orderBy('a.name')
      	->fetchOne();
    $this->vouchers=VoucherTable::fetchByEffectiveDateRange($startdate,$enddate);
  
  }
  public function executeDashboard(sfWebRequest $request)
  {
  	//load accounts
    $this->accounts= Doctrine_Query::create()
        ->from('VoucherAccount a')
      	->orderBy('a.name')
      	->execute();
      	
    //group accounts into columns
    $this->columncount=4;
  	$this->accountgroups=array(array(),array(),array());
  	$this->accountspergroup=ceil(count($this->accounts)/$this->columncount);
  	for($i=0;$i<$this->accountspergroup;$i++)
  	{
  	  for($j=0;$j<$this->columncount;$j++)
  	  {
    	  $this->accountgroups[$j][]=$this->accounts[$j*$this->accountspergroup+$i];
  	  }
  	}
      	
    //auto set date to today, unless date is specified
    $requestparams=$request->getParameter("voucher");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $dateholder=new Voucher();
    if($request->hasParameter('date'))
      $dateholder->setDate($request->getParameter('date'));
    else if(!$day or !$month or !$year)
      $dateholder->setDate(MyDate::today());
    else
      $dateholder->setDate($year."-".$month."-".$day);

    //create new voucher input form
    //this really is just for date input
    $this->form=new VoucherForm($dateholder);

    //load vouchers from database to be displayed
    $this->vouchers =Doctrine_Query::create()
        ->from('Voucher i')
        ->where('date="'.$dateholder->getDate().'"')
        ->orderBy('i.voucher_type_id,i.time')
        ->execute();

    $this->forms=array();
    foreach($this->vouchers as $voucher)
    {
      $this->forms[]=new VoucherForm($voucher);
    }

    //calculate totals
      $this->pettycashtotal=0;
      $this->chequetotal=0;
      $this->othertotal=0;
      $this->total=0;
      foreach($this->vouchers as $voucher)
      {
        $this->total+=$voucher->getAmount();
        if($voucher->getVoucherTypeId()==1)
          $this->pettycashtotal+=$voucher->getAmount();
        else if($voucher->getVoucherTypeId()==2)
          $this->chequetotal+=$voucher->getAmount();
        else
          $this->othertotal+=$voucher->getAmount();
      }

    //for search by check date
    $newOutcheck=new OutCheck();
    $newOutcheck->setCheckDate(MyDate::today());
    $this->dateform=new OutCheckForm($newOutcheck);
  }
  public function executeDashboardPdf(sfWebRequest $request)
  {
    $this->executeDashboard($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());

    //remove comma from amount
    $requestparams["amount"]=str_replace(",","",$requestparams["amount"]);

    //generate code if empty
    if($requestparams["no"]=="")$requestparams["no"]=VoucherTable::genCode();

    $isNew=$requestparams["id"]=="";

    $checkrequestparams=$request->getParameter("out_check");

    if(!$requestparams["voucher_type_id"])
    {
      $message="Voucher Type cannot be empty";
      return $this->redirect("home/error?msg=".$message);
    }
    //subaccount id cannot be blank
    if(!$requestparams["subaccount_id"])
    {
      //enforce this only if subaccounts actually exist
      $voucher_subaccounts=Fetcher::fetch("VoucherSubaccount",array("voucher_account_id"=>$requestparams["account_id"]));
      if(count($voucher_subaccounts)>0)
      {
        $message="Sub Account cannot be empty";
        return $this->redirect("home/error?msg=".$message);
      }
    }

    $voucher_type=Fetcher::fetchOne("VoucherType",array("id"=>$requestparams["voucher_type_id"]));
    if(!$voucher_type)$voucher_type=Fetcher::fetchOne("VoucherType",array("id"=>1));

    $is_check=($voucher_type->getName()=="Cheque");
    $is_bank_transfer=($voucher_type->getName()=="Bank Transfer");

    $requestparams["date_effective"]=$requestparams["date"];
    if($is_check)$requestparams["date_effective"]=$checkrequestparams["check_date"];

    $form->bind($requestparams, $request->getFiles($form->getName()));

    //create checkform
    if($checkrequestparams["id"]!="")
    {
      $this->check=Fetcher::fetchOne("OutCheck",array("id"=>$checkrequestparams["id"]));
      $this->checkform=new OutCheckForm($this->check);
    }
    else
      $this->checkform=new OutCheckForm();
    $this->checkform->bind($checkrequestparams);

    //---VALIDATION----------------------------------
    //validate that biller id is not empty
    //for payment type check or bank transfer
    /*
    if($requestparams["biller_id"]=="" and $is_check)
    {
      $message="Payee is required for Cheque or Bank Transfer";
      $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
    }

    if($requestparams["biller_id"]=="" and $requestparams["payee"]=="")
    {
      $message="Payee or Payee Name is required";
      $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
    }
    */

    if($requestparams["particulars"]=="")
    {
      $message="Particulars cannot be empty";
      return $this->redirect("home/error?msg=".$message);
    }

    if($requestparams["amount"]==0)
    {
      $message="Amount cannot be 0";
      return $this->redirect("home/error?msg=".$message);
    }

    //if payment method is cheque, require check no
    if($voucher_type->getName()=="Cheque" and ($checkrequestparams["check_no"]=="" or strpos(trim($checkrequestparams["check_no"]), ":")))
    {
      $message="Check Number is required for Cheque Payment";
      return $this->redirect("home/error?msg=".$message);
    }

    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $voucher = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $voucher)));

      $this->postprocessform($voucher,$requestparams,$checkrequestparams,$is_check,$is_bank_transfer);

      $this->redirect("voucher/view?id=".$voucher->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  function postprocessform($voucher,$requestparams,$checkrequestparams,$is_check,$is_bank_transfer)
  {
    if($is_check or $is_bank_transfer)
    {
      //if check does not exist
      //create it
      if($checkrequestparams["id"]=="")
      {
        $check=new OutCheck();
        $check->setCode(OutCheckTable::genCode());
        $check->setVoucherAccountId($voucher->getAccountId());
        if($is_bank_transfer)
          $check->setIsBankTransfer(1);
        else
          $check->setIsBankTransfer(0);
        $check->setPassbookId($checkrequestparams["passbook_id"]);
        $check->setPassbookPayToId($checkrequestparams["passbook_pay_to_id"]==""?null:$checkrequestparams["passbook_pay_to_id"]);
        if($check->getCheckNo()==null or $check->getCheckNo()=="")
          $check->setCheckNo($this->getUser()->getUsername()." ".date('h:i:s a'));
        
        //if bank transfer, check_date and receive date will be disabled
        //just use voucher date
        if($is_check)
        {
          $segments=$checkrequestparams["check_date"];
          $check_date=$segments["year"]."-".$segments["month"]."-".$segments["day"];
          $check->setCheckDate($check_date);
        }
        else
        {
          $check->setCheckDate($voucher->getDate());
        }
        $check->setAmount($requestparams["amount"]);
        $check->setRemaining(0);
        $check->setNotes($requestparams["notes"]);
        $check->save();
        
        $check->genVoucherOutPayment($voucher,$requestparams["amount"],MyDate::today(),$requestparams["notes"]);
        
        //go to check edit
        //$this->redirect("out_check/edit?client_class=Voucher&client_id=".$voucher->getId()."&id=".$check->getId());
      }
      //else check exists
      //update it
      else 
      {
        //just update check fields
        $check=Fetcher::fetchOne("OutCheck",array("id"=>$checkrequestparams["id"]));
        $check->setVoucherAccountId($voucher->getAccountId());
        if($is_bank_transfer)
          $check->setIsBankTransfer(1);
        else
          $check->setIsBankTransfer(0);
        $check->setPassbookId($checkrequestparams["passbook_id"]);
        $check->setPassbookPayToId($checkrequestparams["passbook_pay_to_id"]==""?null:$checkrequestparams["passbook_pay_to_id"]);
        $check->setCheckNo($checkrequestparams["check_no"]);
        //if bank transfer, check_date and receive date will be disabled
        //just use voucher date
        if($is_check)
        {
          $segments=$checkrequestparams["check_date"];
          $check_date=$segments["year"]."-".$segments["month"]."-".$segments["day"];
          $check->setCheckDate($check_date);
        }
        else
        {
          $check->setCheckDate($voucher->getDate());
        }
        $check->setAmount($requestparams["amount"]);
        $check->setRemaining(0);
        $check->setNotes($requestparams["notes"]);
        $check->save();

        $out_payment=$voucher->getOutPayment();
        $out_payment->setAmount($requestparams["amount"]);
        $out_payment->save();
      }
      $voucher->setOutCheckId($check->getId());
      $voucher->save();

      //--------------create passbook entry---------
      //check if passbook entry exists
      //if yes, update it
      //if no, create it
      $passbook=$check->getPassbook();
      $date=$check->getCheckDate();
      $qty=$voucher->getAmount()*-1;
      $ref_class="Voucher";
      $ref_id=$voucher->getId();
      // $client_type="Vendor";
      $client_name=$voucher->getPayee();
      $created_by_id=$this->getUser()->getGuardUser()->getId();
      $type="OutCheck";
      $description=$voucher->getParticulars();
      $passbook_entries=$voucher->getPassbookEntries();
      //just delete all the old ones and create a new one
      foreach($passbook_entries as $entry)$entry->delete();
      $passbookEntry=$passbook->addEntry($date, $qty, $ref_class, $ref_id, $client_type, $client_name, $type, $description, $created_by_id);
      }
    //else is not check
    else
    {
      //if check does not exist
      //do nothing
      if($checkrequestparams["id"]=="")
      {
      }
      //if check exists, delete
      else
      {
        $check=Fetcher::fetchOne("OutCheck",array("id"=>$checkrequestparams["id"]));
        $out_payment=$voucher->getOutPayment();
        $passbook_entry=null;
        
        //if passbook entry exists, unmatch
        if($check)$passbook_entry=$check->getPassbookEntry();
        // if($passbook_entry)
        //   $passbook_entry->unMatch();
        if($out_payment)$out_payment->delete();
        if($check)$check->delete();
      }
    }

    $voucher->calcRefStatus();

  }
  public function executeWizardInput(sfWebRequest $request)
  {
    //auto set date to today, unless date is specified
    $requestparams=$request->getParameter("voucher");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $dateholder=new Voucher();
    if($request->hasParameter('date'))
      $dateholder->setDate($request->getParameter('date'));
    else if(!$day or !$month or !$year)
      $dateholder->setDate(MyDate::today());
    else
      $dateholder->setDate($year."-".$month."-".$day);

    //create new voucher input form
    //this really is just for date input
    $this->form=new VoucherForm($dateholder);
  }
  public function executeWizardProcessInput(sfWebRequest $request)
  {
    $input=$request->getParameter('input');
    $date=$request->getParameter('date');

    //parse input
    $lines=explode("\n",$input);
    $data=array();
    foreach($lines as $line)
    {
      //validate line is not empty, ignore
      if(trim($line)=="")
      {
        continue;
      }
    
      //validate format
      //"--" is separator description and amount
      //if separator not found, error
      //if(strpos($line,"--")===false)
      $line=str_replace("\t","--",$line);
      $line=str_replace(",","",$line);
      // var_dump(substr_count($line,"--"));die();
      if(substr_count($line,"--")!=9)
      {
        $message="Incorrect format: ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }
      
      //separate description from value
      $segments=explode("--",$line);
      $date=trim($segments[0]);
      $payee=trim($segments[1]);
      $desc=trim($segments[2]);
      $value=trim($segments[3]);
      $account=trim($segments[4]);
      $subaccount=trim($segments[5]);
      $allocation=trim($segments[6]);
      $type=trim($segments[7]);
      $month=trim($segments[8]);
      $year=trim($segments[9]);
      
      //validate that voucher number exists
      if($date=="")
      {
        $message="Please enter a date for: ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      //validate that payee exists
      if($payee=="")
      {
        $message="Please enter a payee for: ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }
      
      //validate that description exists
      if($desc=="")
      {
        $message="Please enter a description for: ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      //validate that value exists
      if($value=="")
      {
        $message="Please enter a price for: ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      //validate that value exists
      if($account=="")
      {
        $message="Please enter an account for: ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      //validate that value exists
      if($allocation=="")
      {
        $message="Please enter an allocation for: ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      //validate that value exists
      if($type=="")
      {
        $message="Please enter a payment type for: ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      //validate that value is a number
      if(!is_numeric($value))
      {
        $message="Not a number: ".$value." in ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      //validate date
      $datesegments=explode("-",$date);
      if(
        count($datesegments)!=3
        || !is_numeric($datesegments[0])
        || !is_numeric($datesegments[1])
        || !is_numeric($datesegments[2])
        || $datesegments[0] <=2000
      )
      {
        $message="Invalid date: ".$date." in ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      $allocation=Fetcher::fetchOne("VoucherAllocation",array("name"=>"'".$allocation."'"));
      $account=Fetcher::fetchOne("VoucherAccount",array("name"=>"'".$account."'"));
      $subaccount=Fetcher::fetchOne("VoucherSubaccount",array("name"=>"'".$subaccount."'"));
      $type=Fetcher::fetchOne("VoucherType",array("name"=>"'".$type."'"));
      if(!$allocation)
      {
        $message="Invalid allocation: ".$allocation." in ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }
      if(!$account)
      {
        $message="Invalid account: ".$account." in ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }
      if(!$type)
      {
        $message="Invalid payment type: ".$type." in ".$line;
        $this->getUser()->setFlash('error', $message);
        $this->getUser()->setFlash('meta', $input);
        return $this->redirect($request->getReferer());
      }

      $subaccount_id="";
      if($subaccount)$subaccount_id=$subaccount->getId();

      //all checks passed 
      //save data to array
      $data[]=array($date,$payee,$desc,$value,$account->getId(),$subaccount_id,$allocation->getId(),$type->getId(),$month,$year);
    }
    
    //save data as vouchers with no accounts
    $requestparams=$request->getParameter("voucher");
    foreach($data as $item)
    {
      $date=$item[0];
      $no=VoucherTable::genCode();
      $payee=$item[1];
      $desc=$item[2];
      $value=$item[3];
      $voucher = new Voucher();
      $voucher->setDate($date); 
      $voucher->setTime("12:00:00"); 
      $voucher->setNo($no); 
      // $voucher->setVoucherTypeId($requestparams['voucher_type_id']); 
      // $voucher->setVoucherAllocationId($requestparams['voucher_allocation_id']);
      $voucher->setPayee($payee); 
      $voucher->setParticulars($desc); 
      $voucher->setAmount($value); 
      $voucher->setAccountId($item[4]); 
      $voucher->setSubaccountId($item[5]); 
      $voucher->setVoucherAllocationId($item[6]); 
      $voucher->setVoucherTypeId($item[7]); 
      $voucher->setMonth($item[8]); 
      $voucher->setYear($item[9]); 
      $voucher->save();
    }
    
    $this->redirect('voucher/wizardCategorize?date='.$date);
  }
  public function executeWizardCategorize(sfWebRequest $request)
  {
  	//load accounts by category
    $this->accounts= Doctrine_Query::create()
        ->from('VoucherAccount a')
      	->orderBy('a.name')
      	->execute();

    //group accounts into columns
    $this->columncount=4;
  	$this->accountgroups=array(array(),array(),array());
  	$this->accountspergroup=ceil(count($this->accounts)/$this->columncount);
  	for($i=0;$i<$this->accountspergroup;$i++)
  	{
  	  for($j=0;$j<$this->columncount;$j++)
  	  {
    	  $this->accountgroups[$j][]=$this->accounts[$j*$this->accountspergroup+$i];
  	  }
  	}
      	
    //auto set date to today, unless date is specified
    $requestparams=$request->getParameter("voucher");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $dateholder=new Voucher();
    if($request->hasParameter('date'))
      $dateholder->setDate($request->getParameter('date'));
    else if(!$day or !$month or !$year)
      $dateholder->setDate(MyDate::today());
    else
      $dateholder->setDate($year."-".$month."-".$day);

    //create new voucher input form
    //this really is just for date input
    $this->form=new VoucherForm($dateholder);

    $this->voucher =Doctrine_Query::create()
        ->from('Voucher v')
        ->where('v.date=?',$dateholder->getDate())
        ->andWhere('v.account_id is null')
        ->fetchOne();
  }
  public function executeWizardProcessCategorize(sfWebRequest $request)
  {
    //fetch account using submit parameter as account name
    $account =Doctrine_Query::create()
      ->from('VoucherAccount a')
      ->where('a.name=?',$request->getParameter("submit"))
      ->fetchOne();
    if(!$account)
    {
      return $this->redirect("home/error?msg=Sorry, that account does not exist");
    }
    
    //fetch voucher from database
    $voucher =Doctrine_Query::create()
        ->from('Voucher v')
        ->where('v.id=?',$request->getParameter("id"))
        ->fetchOne();
    $voucher->setAccountId($account->getId());
    $voucher->save();
    
    $this->redirect('voucher/wizardCategorize?date='.$request->getParameter("date"));
  }
  //this searches for vouchers that need to be categorized (no account id) across all dates
  public function executeWizardSearchCategorize(sfWebRequest $request)
  {
    //look for a voucher that has no account id
    //get the latest
    $voucher =Doctrine_Query::create()
        ->from('Voucher v')
        ->andWhere('v.account_id is null')
        ->orderBy('v.date desc')
        ->fetchOne();
    //if found
    if($voucher)
    {
      return $this->redirect('voucher/wizardCategorize?date='.$voucher->getDate());
    }
    else
    {
      return $this->redirect('voucher/wizardCategorize');
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    //$this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $voucher=$this->getRoute()->getObject();
    
    $check=null;
    $payment=null;
    $passbook_entry=null;
    
    $ref=$voucher->getRef();
    $payment=$voucher->getOutPayment();
    if($payment)$check=$payment->getOutCheck();

    //only admin can delete
    if ($voucher->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
      $passbook_entries=$voucher->getPassbookEntries();
      foreach($passbook_entries as $entry)
        $entry->delete();
      if($payment!=null)
      {
        $payment->delete();
      }
      if($check!=null)
      {
        $check->delete();
      }
    }
    $voucher->calcRefStatus();

    if($ref!=null)
    {
      if($voucher->getRefType()=="Purchase")
        $this->redirect('purchase/view?id='.$voucher->getRefId());
      else if($voucher->getRefType()=="CounterReceipt")
        $this->redirect('counter_receipt/view?id='.$voucher->getRefId());
      else if($voucher->getRefType()=="CommissionPayment")
        $this->redirect('commission_payment/view?id='.$voucher->getRefId());
    }
    else
      $this->redirect('voucher/dashboard?date='.$voucher->getDate());
  }
  public function executeEditAmount(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('voucher');
    $amount=$requestparams['amount'];
    $this->forward404Unless(
      $voucher =Doctrine_Query::create()
        ->from('Voucher v')
        ->where('v.id = ?',$requestparams['id'])
        ->fetchOne());

    $voucher->setAmount($amount);
    $voucher->save();

    $payment=$voucher->getOutPayment();
    if($payment!=null)
    {
      $payment->setAmount($amount);
      $payment->save();
      $check=$payment->getOutCheck();
      if($check!=null)
      {
        $check->setAmount($amount);
        $check->save();
      }
    }

    $this->redirect("voucher/view?id=".$voucher->getId());
  }
  public function executeEditNo(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('voucher');
    $this->forward404Unless(
      $voucher =Doctrine_Query::create()
        ->from('Voucher v')
        ->where('v.id = ?',$requestparams['id'])
        ->fetchOne());
    $voucher->setNo($requestparams['no']);
    $voucher->save();
    $this->redirect("voucher/view?id=".$voucher->getId());
  }
  public function executeDashboardMulti(sfWebRequest $request)
  {
  	//load accounts
    $this->accounts= Doctrine_Query::create()
        ->from('VoucherAccount a')
      	->orderBy('a.name')
      	->execute();

    //process start and end dates
    //startdate
    if($request->hasParameter("startdate"))
    {
      $startdate=$request->getParameter("startdate");
    }
    elseif($request->hasParameter("startdatesplit"))
    {
      $requestparams=$request->getParameter("startdatesplit");
      $day=str_pad($requestparams["day"], 2, "0", STR_PAD_LEFT);
      $month=str_pad($requestparams["month"], 2, "0", STR_PAD_LEFT);
      $year=$requestparams["year"];
      $startdate=$year."-".$month."-".$day;
    }
    else
    {
      $startdate=MyDate::today();
    }
    $this->startdate=$startdate;

    //enddate
    if($request->hasParameter("enddate"))
    {
      $enddate=$request->getParameter("enddate");
    }
    elseif($request->hasParameter("enddatesplit"))
    {
      $requestparams=$request->getParameter("enddatesplit");
      $day=str_pad($requestparams["day"], 2, "0", STR_PAD_LEFT);
      $month=str_pad($requestparams["month"], 2, "0", STR_PAD_LEFT);
      $year=$requestparams["year"];
      $enddate=$year."-".$month."-".$day;
    }
    else
    {
      $enddate=MyDate::today();
    }
    $this->enddate=$enddate;
 
  	if($this->startdate>$this->enddate)
  	{
  	  return $this->redirect("home/error?msg=Start date cannot be later than end date");
  	}

    //load vouchers from database
    $this->vouchers = VoucherTable::fetchByEffectiveDateRange($startdate,$enddate);

    //calculate totals
      $this->pettycashtotal=0;
      $this->chequetotal=0;
      $this->othertotal=0;
      $this->total=0;
      foreach($this->vouchers as $voucher)
      if(!$voucher->getIsCancelled())
      {
        $this->total+=$voucher->getAmount();
        if($voucher->getVoucherTypeId()==1)
          $this->pettycashtotal+=$voucher->getAmount();
        else if($voucher->getVoucherTypeId()==2)
          $this->chequetotal+=$voucher->getAmount();
        else
          $this->othertotal+=$voucher->getAmount();
      }
      
      
  	//put accounts into an array, sorted by id
  	$this->accountvouchers=array();
  	$this->accounttotals=array();
    $this->allocations=Doctrine_Query::create()
      ->from("VoucherAllocation s")
      ->orderBy("priority")
      ->execute();
    foreach($this->accounts as $account)
    {
  	  $this->accountvouchers[$account->getId()]=array();
    	$this->accounttotals[$account->getId()]=array();
      $this->accounttotals[$account->getId()]["total"]=0;
      foreach($this->allocations as $allocation)
      {
        $this->accounttotals[$account->getId()][$allocation->getId()]=0;
      }
    }
  	foreach($this->vouchers as $voucher)
  	{
  	  $this->accountvouchers[$voucher->getAccountId()][]=$voucher;
      if(!$voucher->getIsCancelled())
      {
        $this->accounttotals[$voucher->getAccountId()]["total"]+=$voucher->getAmount();
        $this->accounttotals[$voucher->getAccountId()][$voucher->getVoucherAllocationId()]+=$voucher->getAmount();
      }
  	}
  }
  public function executeDashboardMultiForAccounting(sfWebRequest $request)
  {
    $this->executeDashboardMulti($request);
    $this->voucherAllocations=Doctrine_Query::create()
      ->from("VoucherAllocation s")
      ->orderBy("priority")
      ->execute();
    $this->voucherAccounts=Doctrine_Query::create()
      ->from("VoucherAccount s")
      ->orderBy("name")
      ->execute();

  }
  public function executeSearch(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()
        ->from('Voucher i')
      	->where('i.no like "%'.trim($request->getParameter("searchstring")).'%"')
      	->orWhere('i.particulars   like "%'.trim($request->getParameter("searchstring")).'%"')
      	->orWhere('i.payee      like "%'.trim($request->getParameter("searchstring")).'%"')
//      	->orWhere('i.chequedata like "%'.trim($request->getParameter("searchstring")).'%"')
//      	->orWhere('i.cheque     like "%'.trim($request->getParameter("searchstring")).'%"')
      ;
    $this->vouchers=$query->execute();
    $this->searchstring=$request->getParameter("searchstring");

  }
  public function executeSearchByPayee(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()->from('Voucher i')
      	->where('i.payee      like "%'.trim($request->getParameter("searchstring")).'%"');
    $this->vouchers=$query->execute();
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeSearchByAmount(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()->from('Voucher i')
      	->where('i.amount      = '.trim($request->getParameter("searchstring")));
    $this->vouchers=$query->execute();
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeSearchByCheckNo(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()->from('Voucher v, v.OutCheck c')
      	->where('c.check_no      like "%'.trim($request->getParameter("searchstring")).'%"');
    $this->vouchers=$query->execute();
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeSearchByVoucherNo(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()->from('Voucher i')
        ->where('i.no like "%'.trim($request->getParameter("searchstring")).'%"');
    $this->vouchers=$query->execute();
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeSearchByCheckDate(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("out_check");
    $date=$requestparams["check_date"];
    $query=Doctrine_Query::create()->from('Voucher v, v.OutCheck c')
      	->where('c.check_date      = "'.$date["year"]."-".$date["month"]."-".$date["day"].'"');
    $this->vouchers=$query->execute();
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeSearchByParticulars(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()->from('Voucher i')
        ->where('i.particulars   like "%'.trim($request->getParameter("searchstring")).'%"');
    $this->vouchers=$query->execute();
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeFiles(sfWebRequest $request)
  {
    $this->voucher=MyModel::fetchOne("Voucher",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('voucher');
    $this->file->setParentId($this->voucher->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="voucher"')
      ->andWhere('f.parent_id='.$this->voucher->getId())
      ->execute();
  }
  public function executeCancel(sfWebRequest $request)
  {
    $voucher=MyModel::fetchOne("Voucher",array('id'=>$request->getParameter("id")));
    $voucher->cancel();
    return $this->redirect($request->getReferer());
  }
  public function executeCancelAndReplace(sfWebRequest $request)
  {
    $oldVoucher=MyModel::fetchOne("Voucher",array('id'=>$request->getParameter("id")));
    $oldVoucher->cancel();
    $oldCheck=$oldVoucher->getCheck();

    $this->voucher=new Voucher();
    $this->voucher->setNo(VoucherTable::genCode());
    $this->voucher->setAccountId($oldVoucher->getAccountId());
    $this->voucher->setSubaccountId($oldVoucher->getSubaccountId());
    $this->voucher->setMonth($oldVoucher->getMonth());
    $this->voucher->setDate(MyDate::today());
    $this->voucher->setTime(date('h:i a'));
    $this->voucher->setParticulars($oldVoucher->getParticulars());
    $this->voucher->setAmount($oldVoucher->getAmount());
    $this->voucher->setPayee($oldVoucher->getPayee());
    $this->voucher->setVoucherTypeId($oldVoucher->getVoucherTypeId());
    $this->voucher->setVoucherAllocationId($oldVoucher->getVoucherAllocationId());
    $this->voucher->setRefType($oldVoucher->getRefType());
    $this->voucher->setRefId($oldVoucher->getRefId());
    $this->voucher->setClientType($oldVoucher->getClientType());
    $this->voucher->setClientId($oldVoucher->getClientId());
    if($oldCheck!=null)
      $notes="Replacement for ".$oldCheck->getPassbook()->getName()." Check No ".$oldCheck->getCheckNo().", amount P".$oldCheck->getAmount().", Voucher No. ".$oldVoucher->getNo();
    else
      $notes="Replacement for Voucher No. ".$oldVoucher->getNo();
    $this->voucher->setNotes($notes);
    $this->form = $this->configuration->getForm($this->voucher);

    $this->check=new OutCheck();
    $this->check->setCheckDate(MyDate::today());
    $this->check->setReceiveDate(MyDate::today());
    $this->checkform=new OutCheckForm($this->check);
  }
  public function executeUndoCancel(sfWebRequest $request)
  {
    $voucher=MyModel::fetchOne("Voucher",array('id'=>$request->getParameter("id")));
    $voucher->undoCancel($this->getUser()->getGuardUser()->getId());
    return $this->redirect($request->getReferer());
  }


  public function executePrintChequePdf(sfWebRequest $request)
  {

    //load voucher from database, error if it doesn't
    $this->forward404Unless(
      $voucher=Doctrine_Query::create()
        ->from('Voucher v')
      	->where('v.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Voucher with id (%s) not found.', $request->getParameter('id')));

    $checkNo="";
    $out_payment=$voucher->getOutPayment();
    if($out_payment)$check=$out_payment->getOutCheck();
    if($check)
    {
      $checkNo=$check->getCheckNo();
      $checkDate=$check->getCheckDate();
    }

    $this->content=MyChequeHelper::printChequePdf($voucher,$checkDate);
    $this->voucher=$voucher;

    if($check)
    {
      $check->setPrintCount($check->getPrintCount()+1);
      $check->setPrintDate(MyDate::today());
      $check->save();
    }

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executePrintChequeDmf(sfWebRequest $request)
  {
    //load voucher from database, error if it doesn't
    $this->forward404Unless(
      $voucher=Doctrine_Query::create()
        ->from('Voucher v')
      	->where('v.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Voucher with id (%s) not found.', $request->getParameter('id')));

    $checkNo="";
    $out_payment=$voucher->getOutPayment();
    if($out_payment)$check=$out_payment->getOutCheck();
    if($check)
    {
      $checkNo=$check->getCheckNo();
      $checkDate=$check->getCheckDate();
    }

    $content=MyChequeHelper::printCheque($voucher,$checkDate);

    $printtoscreen=false;
    if($printtoscreen)
    {
      $content=str_replace("\n","<br>",$content);
      $content=str_replace(" ","&nbsp;",$content);
      echo $content;
      die();
    }

    $content=bin2hex($content);
      
    $check->setPrintCount($check->getPrintCount()+1);
    $check->setPrintDate(MyDate::today());
    $check->save();

    $response = $this->getResponse();
    $response->clearHttpHeaders();
    //$response->setContentType($mimeType);
    $response->setHttpHeader('Content-Disposition', 'attachment; filename="' . basename(str_replace(" ","",$checkNo).".dmf") . '"');
    $response->setHttpHeader('Content-Description', 'File Transfer');
    $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
    $response->setHttpHeader('Content-Length', 80*39);
    $response->setHttpHeader('Cache-Control', 'public, must-revalidate');
    $response->setHttpHeader('Pragma', 'public');
    $response->setContent($content);
    $response->sendHttpHeaders();

    sfConfig::set('sf_web_debug', false);
    return sfView::NONE;
  }

/*
  public function executePrintVoucherDmf(sfWebRequest $request)
  {
    $this->forward404Unless(
      $voucher=Doctrine_Query::create()
        ->from('Voucher v')
      	->where('v.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Voucher with id (%s) not found.', $request->getParameter('id')));

    $checkNo="";
    $out_payment=$voucher->getOutPayment();
    if($out_payment)$check=$out_payment->getOutCheck();
    if($check)$checkNo=$check->getCheckNo();

    $content=MyChequeHelper::printVoucher($voucher,$checkNo);

    $printtoscreen=false;
    if($printtoscreen)
    {
      $content=str_replace("\n","<br>",$content);
      $content=str_replace(" ","&nbsp;",$content);
      echo $content;
      die();
    }

    $content=bin2hex($content);
      
    $response = $this->getResponse();
    $response->clearHttpHeaders();
    //$response->setContentType($mimeType);
    $response->setHttpHeader('Content-Disposition', 'attachment; filename="' . basename(str_replace(" ","",$checkNo).".dmf") . '"');
    $response->setHttpHeader('Content-Description', 'File Transfer');
    $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
    $response->setHttpHeader('Content-Length', 80*39);
    $response->setHttpHeader('Cache-Control', 'public, must-revalidate');
    $response->setHttpHeader('Pragma', 'public');
    $response->setContent($content);
    $response->sendHttpHeaders();

    sfConfig::set('sf_web_debug', false);
    return sfView::NONE;
  }  
  */

  public function executeGenCodeForAll(sfWebRequest $request)
  {
  	die("ACCESS DENIED");

      //default values
    $this->interval=100;
    $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->vouchers=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->vouchers = Doctrine_Query::create()
      ->from('Voucher p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();

    foreach($this->vouchers as $v)
    {
        if($v->getNo()=="")
        {
          $v->setNo(VoucherTable::genCode());
          $v->save();
        }
    }

     $this->start=$this->end+1;
  }
  public function executeResetOutCheckId(sfWebRequest $request)
  {
  	// die("ACCESS DENIED");

      //default values
    $this->interval=100;
    $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->vouchers=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->vouchers = Doctrine_Query::create()
      ->from('Voucher p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();

    foreach($this->vouchers as $v)
    {
      $out_payment=$v->getOutPayment();
      if($out_payment)$check=$out_payment->getOutCheck();
      if($check)
      {
        if($v->getOutCheckId()!=$check->getId())
        {
          // echo $v->getNo()."<br>";
          $v->setOutCheckId($check->getId());
          $v->save();
        }
      }
    }

     $this->start=$this->end+1;
  }
  public function executeClearCheck(sfWebRequest $request)
  {
    $this->forward404Unless(
      $voucher=Doctrine_Query::create()
        ->from('Voucher v')
      	->where('v.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Voucher with id (%s) not found.', $request->getParameter('id')));


    $requestparams=$request->getParameter('out_check');
    $cleared_date=$requestparams["cleared_date"];
    $cleared_date=$cleared_date["year"]."-".$cleared_date["month"]."-".$cleared_date["day"];
    $check=$voucher->getCheck();
    if($check!=null){
      $voucher->setCheckStatus("Cleared");
      $voucher->save();
      $check->setClearedDate($cleared_date);
      $check->save();
      $this->getUser()->setFlash('notice', "Check successfully cleared");
    }
    else
      $this->getUser()->setFlash('error', "Check not found");
    return $this->redirect($request->getReferer());
  }

  public function executeUndoClearCheck(sfWebRequest $request)
  {
    $this->forward404Unless(
      $voucher=Doctrine_Query::create()
        ->from('Voucher v')
      	->where('v.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Voucher with id (%s) not found.', $request->getParameter('id')));

    $check=$voucher->getCheck();
    if($check!=null){
      if($check->getReceiveDate()!=null)
        $voucher->setCheckStatus("Released");
      else
        $voucher->setCheckStatus("Pending");
      $voucher->save();
      $check->setClearedDate(null);
      $check->save();
      $this->getUser()->setFlash('notice', "Undo check clear successful");
    }
    else
      $this->getUser()->setFlash('error', "Check not found");
    return $this->redirect($request->getReferer());
  }
  public function executeReleaseCheck(sfWebRequest $request)
  {
    $this->forward404Unless(
      $voucher=Doctrine_Query::create()
        ->from('Voucher v')
      	->where('v.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Voucher with id (%s) not found.', $request->getParameter('id')));


    $requestparams=$request->getParameter('out_check');
    $receive_date=$requestparams["receive_date"];
    $receive_date=$receive_date["year"]."-".$receive_date["month"]."-".$receive_date["day"];
    $check=$voucher->getCheck();
    if($check!=null){
      $voucher->setCheckStatus("Released");
      $voucher->save();
      $check->setReceiveDate($receive_date);
      $check->save();
      $this->getUser()->setFlash('notice', "Check successfully released");
    }
    else
      $this->getUser()->setFlash('error', "Check not found");
    return $this->redirect($request->getReferer());
  }

  public function executeUndoReleaseCheck(sfWebRequest $request)
  {
    $this->forward404Unless(
      $voucher=Doctrine_Query::create()
        ->from('Voucher v')
      	->where('v.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Voucher with id (%s) not found.', $request->getParameter('id')));

    $check=$voucher->getCheck();
    if($check!=null){
      $voucher->setCheckStatus("Pending");
      $voucher->save();
      $check->setReceiveDate(null);
      $check->save();
      $this->getUser()->setFlash('notice', "Undo check receive successful");
    }
    else
      $this->getUser()->setFlash('error', "Check not found");
    return $this->redirect($request->getReferer());
  }


  public function executeDsrmulti(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");
    $today=MyDateTime::frommysql(MyDate::today());
    $this->lastsaturday=$today->getlastsaturday();
    $this->thisfriday=$today->getthisfriday();

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("startdate"))
      $invoice->setDate($request->getParameter("startdate"));
    elseif(!$day or !$month or !$year)
      $invoice->setDate($this->lastsaturday->tomysql());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $requestparams=$request->getParameter("purchase");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $purchase=new Purchase();
    if($request->hasParameter("enddate"))
      $purchase->setDate($request->getParameter("enddate"));
    elseif(!$day or !$month or !$year)
      $purchase->setDate($this->thisfriday->tomysql());
    else
      $purchase->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->toform=new PurchaseForm($purchase);

    $outcheckparams=$request->getParameter("out_check");
    $passbook_id=$outcheckparams["passbook_id"];
    if($passbook_id==null)$passbook_id=$request->getParameter("passbook_id");
    $outcheck=new OutCheck();
    if($passbook_id)$outcheck->setPassbookId($passbook_id);
    $this->checkform=new OutCheckForm($outcheck);
    $this->passbook_id=$passbook_id;

    $this->totalunreceived=0;
    $this->totalreceived=0;
    $this->totalcleared=0;
    $this->totalcancelled=0;

    if(!$passbook_id)
    {
      $this->unreceived=array();
      $this->received=array();
      $this->cleared=array();
      $this->cancelled=array();
    }
    else
    {
      $this->passbook=Fetcher::fetchOne("Passbook",array("id"=>$passbook_id));
      $this->unreceived = VoucherTable::fetchUnreceivedUnclearedByCheckDateRangeAndAccount($invoice->getDate(),$purchase->getDate(),$passbook_id);
      $this->received = VoucherTable::fetchReceivedUnclearedByCheckDateRangeAndAccount($invoice->getDate(),$purchase->getDate(),$passbook_id);
      $this->cleared = VoucherTable::fetchClearedByCheckDateRangeAndAccount($invoice->getDate(),$purchase->getDate(),$passbook_id);
      $this->cancelled = VoucherTable::fetchCancelledByCheckDateRangeAndAccount($invoice->getDate(),$purchase->getDate(),$passbook_id);
  
      foreach($this->unreceived as $voucher)
        $this->totalunreceived+=$voucher->getAmount();
      foreach($this->received as $voucher)
        $this->totalreceived+=$voucher->getAmount();
      foreach($this->cleared as $voucher)
        $this->totalcleared+=$voucher->getAmount();
      foreach($this->cancelled as $voucher)
        $this->totalcancelled+=$voucher->getAmount();
    }
  }
  public function executeSetAccount(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setAccountId($request->getParameter("voucher_account_id"));
    $voucher->save();
    $startdate=$request->getParameter("startdate");
    $enddate=$request->getParameter("enddate");
    return $this->redirect("voucher/dashboardMultiForAccounting?startdate=".$startdate."&enddate=".$enddate);
  }
  public function executeSetAllocation(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setVoucherAllocationId($request->getParameter("voucher_allocation_id"));
    $voucher->save();
    $startdate=$request->getParameter("startdate");
    $enddate=$request->getParameter("enddate");
    return $this->redirect("voucher/dashboardMultiForAccounting?startdate=".$startdate."&enddate=".$enddate);
  }
  public function executeToggleHelpNeeded(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setIsHelpNeeded(!$voucher->getIsHelpNeeded());
    $voucher->save();
    $startdate=$request->getParameter("startdate");
    $enddate=$request->getParameter("enddate");
    return $this->redirect("voucher/dashboardMultiForAccounting?startdate=".$startdate."&enddate=".$enddate);
  }

  public function executeSetNotes(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setNotes($request->getParameter("notes"));
    $voucher->save();
    $startdate=$request->getParameter("startdate");
    $enddate=$request->getParameter("enddate");
    return $this->redirect("voucher/dashboardMultiForAccounting?startdate=".$startdate."&enddate=".$enddate);
  }
  public function executeSetParticulars(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setParticulars($request->getParameter("particulars"));
    $voucher->save();
    $startdate=$request->getParameter("startdate");
    $enddate=$request->getParameter("enddate");
    return $this->redirect("voucher/dashboardMultiForAccounting?startdate=".$startdate."&enddate=".$enddate);
  }
  public function executeSetInvoiceCrossRefId(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setInvoiceCrossRefId($request->getParameter("invoice_cross_ref_id"));
    $voucher->save();
    die();
  }
  public function executeSetPurchaseCrossRefId(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setPurchaseCrossRefId($request->getParameter("purchase_cross_ref_id"));
    $voucher->save();
    die();
  }
  public function executeSegregatePayroll(sfWebRequest $request)
  {
    $tjl=array(
      "CHESTER",
      "LLOYD",
      "ERIC",
      "JR",
      "NONONG",
      "DANILO",
      "MALBERTO",
      "DAN",
    );
    $wholesale=array(
      "ANN",
      "ANALEE",
      "JOYCE",
      "BENJAMIN",
      "PRINCESS",
      "MARKJOHN",
      "CHRISTIAN",
      "WENDELL",
    );
    $tradewind=array(
      "DILENGER",
      "EFREN",
      "EMILY",
      "IVAN",
      "JENNIFER",
      "JOEBERT",
      "JOSEPH",
      "JUVELYN",
      "LUCIANO",
      "MARINELA",
      "MARVINJ",
      "MEANN",
      "MICHELLE",
      "MILES",
      "RUEL",
      "SONNYBOY",
      "RAMIE",
      "MICHAEL",
      "SONNY",
      "EDILBERTO",
      "ISMAEL",
      "ARJAY",
    );
    $companies=array(
      "tradewind"=>$tradewind,
      "wholesale"=>$wholesale,
      "tjl"=>$tjl,
    );
    $notfound=array();
    $this->companies=$companies;

    $this->originaltext=$request->getParameter("text");
    $text=$request->getParameter("text");
    if($text==null or $text=="")return;
    $text=str_replace("\t"," ",$text);
    $text=str_replace(",","",$text);

    $lines=explode("\n",$text);
    $employeetotals=array();
    $companytotals=array(0,0,0);

    foreach($lines as $line)
    {
      $segments=explode(" ",$line);
      $employeename=$segments[0];
      $employeesalary=$segments[1];

      //go through all companies and employee names and find a match
      $found=false;
      foreach($companies as $companykey => $company)
      foreach($company as $employee)
      {
        if($employeename==$employee)
        {
          $found=true;
          if(!array_key_exists($employee,$employeetotals))
            $employeetotals[$employee]=$employeesalary;
          else
            $employeetotals[$employee]+=$employeesalary;

          $companytotals[$companykey]+=$employeesalary;
          break;//stop loop
        }
      }
      if($found==false and $employeename!="Net" and $employeename!="Deductions" and $employeename!="Total:")
      {
        $notfound[]=$employeename;
      }
    }
    $this->employeetotals=$employeetotals;
    $this->companytotals=$companytotals;
    $this->notfound=$notfound;
  }
  public function executeCheck(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setIsChecked(1);
    $voucher->save();
    return $this->redirect($request->getReferer());
  }
  public function executeUncheck(sfWebRequest $request)
  {
    $voucher=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("id")));
    $voucher->setIsChecked(null);
    $voucher->save();
    return $this->redirect($request->getReferer());
  }
  public function executePassbookReport(sfWebRequest $request)
  {
    $this->passbook_id=$request->getParameter("passbook_id");
    if($this->passbook_id==null)$this->passbook_id=2;
    $this->passbook=Fetcher::fetchOne("Passbook",array("id"=>$this->passbook_id));

    //auto set date to today, unless date is specified
		list($this->startdateform, $this->enddateform) = MyDateRangeHelper::getForms($request);
		$startdate=$this->startdateform->getObject()->getDate();
		$enddate=$this->enddateform->getObject()->getDate();

    //create new voucher input form
    //this really is just for date input
    $this->form=new VoucherForm($dateholder);

    $this->vouchers= Doctrine_Query::create()
      ->from('Voucher v, v.OutCheck c')
      ->where('c.passbook_id ='.$this->passbook_id)
      ->andWhere('c.check_date >="'.$startdate.'"')
      ->andWhere('c.check_date <="'.$enddate.'"')
      ->orderBy('c.check_date')
      ->execute();
  }
  public function executeMassGenPassbookEntry(sfWebRequest $request)
  {
      //default values
    $requestparams=$request->getParameter(invoice);
    // var_dump($requestparams["date"]);die();

    if(isset($requestparams["date"]))
      $date=$requestparams["date"];
    else
      $date="2023-01-01";
    $invoice=new Invoice();
    $invoice->setDate($date);
    $this->invoiceForm=new InvoiceForm($invoice);

    $date=$date["year"]."-".$date["month"]."-".$date["day"];

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      // $this->products=array();  
     return;
    }
     
     $this->vouchers = Doctrine_Query::create()
      ->from('Voucher i')
      ->where('i.date ="'.$date.'"')
      ->andWhere('i.out_check_id is not null')
      ->execute();
      foreach ($this->vouchers as $voucher) {
        $voucher->genPassbookEntry($this->getUser()->getGuardUser()->getId());
      }
      $this->checks = Doctrine_Query::create()
      ->from('InCheck i')
      ->where('i.check_date ="'.$date.'"')
      ->execute();
      foreach ($this->checks as $check) {
        $check->genPassbookEntry($this->getUser()->getGuardUser()->getId());
      }

    $mydate=MyDateTime::frommysql($date);
    $mydate->adddays(1);
    $invoice->setDate($mydate->tomysql());
    $this->invoiceForm=new InvoiceForm($invoice);
  }
  public function executeMassGenerateVouchers(sfWebRequest $request)
  {
    $this->content=$request->getParameter("content");
    $lines=explode("\n",$this->content);
    $data=array();
    foreach($lines as $line)
    {
      if($line=="")continue;
      $data[]=explode("\t",$line);
    }

    //validation
    $this->errors=array();
    $this->notices=array();
    $count=0;
    foreach($data as $item)
    {
      $count+=1;

      if(count($item)<12)
      {
        $this->errors[]="Row ".$count.": wrong number of columns";
        continue;
      }

      $date=MyDateTime::frommysql($item[0]);
      if(!$date->isvalid())
        $this->errors[]="Row ".$count.": invalid date: ".$item[0];

      $payee=$item[1];
      if(empty($payee))
        $this->errors[]="Row ".$count.": payee is required";

      $amount=str_replace(",","",$item[2]);
      if(empty($amount))
        $this->errors[]="Row ".$count.": amount is required";

      $passbook=$item[3];
      if(empty($passbook))
        $this->errors[]="Row ".$count.": bank is required";
      else
      {
        $passbook=Fetcher::fetchOne("Passbook",array("name"=>'"'.$passbook.'"'));
        if($passbook==null)$this->errors[]="Row ".$count.": invalid bank";
      }

      $particulars=$item[4];
      $notes=$item[5];
      $type=$item[6];
      if(empty($type))
        $this->errors[]="Row ".$count.": type is required";
      else
      {
        $type=Fetcher::fetchOne("VoucherType",array("name"=>'"'.$type.'"'));
        if($type==null)$this->errors[]="Row ".$count.": invalid type";
      }

      $allocation=$item[7];
      if(empty($allocation))
        $this->errors[]="Row ".$count.": allocation is required";
      else
      {
        $allocation=Fetcher::fetchOne("VoucherAllocation",array("name"=>'"'.$allocation.'"'));
        if($allocation==null)$this->errors[]="Row ".$count.": invalid allocation";
      }
  
      $account=$item[8];
      if(empty($account))
      {
        $this->errors[]="Row ".$count.": account is required";
      }
      else
      {
        $account=Fetcher::fetchOne("VoucherAccount",array("name"=>'"'.$account.'"'));
        if($account==null)$this->errors[]="Row ".$count.": invalid account";
      }

      $subaccount=$item[9];
      if(!empty($subaccount))
      {
        $subaccount=Fetcher::fetchOne("VoucherSubaccount",array("name"=>'"'.$subaccount.'"'));
        if($subaccount==null)$this->errors[]="Row ".$count.": invalid subaccount";
      }

      $month=$item[10];
      if(intval($month)>12 or intval($month)<1)
        $this->errors[]="Row ".$count.": invalid month: ".$month;

      $year=$item[11];
      if(intval($year)>2033 or intval($year)<2023)
        $this->errors[]="Row ".$count.": invalid year: ".$year;
    }

    //if no errors
    if(count($this->errors)==0)
    {
      foreach($data as $item)
      {
        $count+=1;
  
        $date=MyDateTime::frommysql($item[0])->tomysql();
        $payee=$item[1];
        $amount=str_replace(",","",$item[2]);
        $passbook=$item[3];
        $passbook=Fetcher::fetchOne("Passbook",array("name"=>'"'.$passbook.'"'));
        $particulars=$item[4];
        $notes=$item[5];
        $type=$item[6];
        $type=Fetcher::fetchOne("VoucherType",array("name"=>'"'.$type.'"'));
        $allocation=$item[7];
        $allocation=Fetcher::fetchOne("VoucherAllocation",array("name"=>'"'.$allocation.'"'));
        $account=$item[8];
        $account=Fetcher::fetchOne("VoucherAccount",array("name"=>'"'.$account.'"'));
        $subaccount=$item[9];
        $subaccount=Fetcher::fetchOne("VoucherSubaccount",array("name"=>'"'.$subaccount.'"'));
        $subaccount_id="";
        if($subaccount)$subaccount_id=$subaccount->getId();
        $month=$item[10];
        $year=$item[11];

        $voucher=new Voucher();
        $voucher->setNo(VoucherTable::genCode());
        $voucher->setDate($date);
        $voucher->setPayee($payee);
        $voucher->setAmount($amount);
        $voucher->setParticulars($particulars);
        $voucher->setNotes($notes);
        $voucher->setVoucherType($type);
        $voucher->setVoucherAllocation($allocation);
        $voucher->setVoucherAccount($account);
        if($subaccount)$voucher->setVoucherSubaccount($subaccount);
        $voucher->setMonth($month);
        $voucher->setYear($year);
        $voucher->save();

        $datesegments=explode("-",$date);
        $datesegments=array("year"=>$datesegments[0],"month"=>$datesegments[1],"day"=>$datesegments[2]);

        $requestparams=array(
          "id"=>$voucher->getId(),
          "check_status"=> "Pending",
          "ref_type"=> "",
          "ref_id"=> "",
          "client_type"=> "",
          "client_id"=> "",
          "voucher_allocation_id"=> $allocation->getId(),
          "no"=> $voucher->getNo(),
          "date"=> $datesegments,
          "payee"=>  $payee,
          "particulars"=>  $particulars,
          "amount"=> $amount,
          "account_id"=> $account->getId(),
          "subaccount_id"=> $subaccount_id,
          "month"=> $month,
          "year"=> $year,
          "voucher_type_id"=> $type->getId(),
          "notes"=> $notes,
        );
        $checkrequestparams=array(
          "id"=>"",
          "check_no"=>$voucher->getId(),
          "passbook_id"=>$passbook->getId(),
          "check_date"=>$datesegments,
        );
        $is_check=($type->getName()=="Cheque");
        $is_bank_transfer=($type->getName()=="Bank Transfer");
        $this->postprocessform($voucher,$requestparams,$checkrequestparams,$is_check,$is_bank_transfer);
        $this->notices[]="Voucher ".$voucher->getNo()." successfully generated";
      }
    }
  }
}

