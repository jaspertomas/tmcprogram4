<input type=hidden name="out_check[voucher_account_id]" id="out_check_voucher_account_id" value=<?php echo $form->getObject()->getVoucherAccountId()?>>
<input type=hidden name="out_check[vendor_id]" id="out_check_vendor_id" value=<?php echo $form->getObject()->getVendorId()?>>

<?php if($form->getObject()->getVendorId()){ ?>
<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_vendor_id">
<div>
<label for="out_check_vendor_id">Vendor</label>
<div class="content">
<?php echo link_to($form->getObject()->getVendor(),"vendor/view?id=".$form->getObject()->getVendorId())?> 
</div>
</div>
</div>
<?php }else{?>
<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_voucher_account_id">
<div>
<label for="out_check_voucher_account_id">Voucher Account</label>
<div class="content">
<?php echo $form->getObject()->getVoucherAccount()?> 
</div>
</div>
</div>

<?php } ?>
