<?php use_helper('I18N', 'Date') ?>
<?php include_partial('out_check/assets') ?>

<div id="sf_admin_container">

<?php if(count($existing_checks)>0){?>
  <h1>Choose Existing Check</h1>
  <?php include_partial('out_check/existing_checks', array('select' => true,'client' => $client,'existing_checks' => $existing_checks,'chequeamt' => $chequeamt)) ?>

<?php } ?>

  <h1><?php echo __((count($existing_checks)>0?"or ":"").'Create Outgoing Check', array(), 'messages') ?></h1>
  <?php include_partial('out_check/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('out_check/form_header', array('out_check' => $out_check, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('out_check/form', array('chequeamt' => $chequeamt,'client' => $client,'client_class' => $client_class,'client_id' => $client_id, 'out_check' => $out_check, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('out_check/form_footer', array('out_check' => $out_check, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
