
<h1>Outgoing Check <?php echo $check->getCode()?></h1>
<table border=1>
<tr>
  <th>Check No.</th>
  <th>Amount</th>
  <th>Remaining</th>
  <th>Pay To</th>
</tr>

<tr>
    <td><?php echo $check->getCheckNo()?></td>
    <td><?php echo number_format ( $check->getAmount(),2 )?></td>
    <td><?php echo number_format ( $check->getRemaining(),2 )?></td>
    <td><?php 
      if($check->getVendorId())
        echo link_to($check->getVendor(),"vendor/viewChecks?id=".$check->getVendorId());
      elseif($check->getVoucherAccountId())
        echo $check->getVoucherAccount();
      ?></td>
</tr>
</table>


<h2>Payments:</h2>
<table border=1>
  <tr>
    <td>Counter Receipt / Voucher</td>
    <td>Date</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>

<?php foreach($check->getOutPayment() as $detail){?>


  <?php if($detail->getParentClass()=="CounterReceipt"){$counter_receipt=$detail->getParent();?>
    <tr>
      <td><?php echo link_to($counter_receipt, "counter_receipt/payments?id=".$counter_receipt->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td><?php echo $detail->getAmount() ?></td>
      <td>
        <?php 
          foreach($counter_receipt->getCounterReceiptDetail() as $detail)
            echo link_to($detail->getPurchase(), "purchase/view?id=".$detail->getPurchaseId())." ";
        ?>
      </td>
      <td><?php echo $detail->getNotes() ?></td>
      <td><?php //echo link_to('Edit','out_payment/edit?id='.$detail->getId()) ?></td>
      <td>
        <?php /*echo link_to(
          'Delete',
          'out_payment/delete?id='.$detail->getId(),
          array('method' => 'delete', 'confirm' => 'Are you sure?')
        )*/ ?>
      </td>
    </tr>
  <?php }else if($detail->getParentClass()=="Voucher"){$voucher=$detail->getParent();?>
    <tr>
      <td><?php echo link_to($voucher, "voucher/view?id=".$voucher->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td><?php echo $detail->getAmount() ?></td>
      <td><?php 
      $purchase=$voucher->getPurchase();
      if($purchase!=null) echo link_to($purchase,"purchase/view?id=".$purchase->getId())."; ";
      echo $voucher->getParticulars(); 
      ?></td>
      <td><?php echo $voucher->getNotes() ?></td>
      <td>
        <?php /*echo link_to(
          'Delete',
          'out_payment/delete?id='.$detail->getId(),
          array('method' => 'delete', 'confirm' => 'Are you sure?')
        )*/ ?>
      </td>
    </tr>
  <?php }?>
<?php }?>

</table>


