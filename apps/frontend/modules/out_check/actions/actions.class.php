<?php

require_once dirname(__FILE__).'/../lib/out_checkGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/out_checkGeneratorHelper.class.php';

/**
 * out_check actions.
 *
 * @package    sf_sandbox
 * @subpackage out_check
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class out_checkActions extends autoOut_checkActions
{
  //detach check from counter_receipt; check still exists and can be applied elsewhere
  /*
  public function executeRemoveFromCounterReceipt(sfWebRequest $request)
  {
    $counter_receipt=Fetcher::fetchOne("CounterReceipt",array("id"=>$request->getParameter("client_id")));
    $out_check=$counter_receipt->getOutCheck();
    if($out_check!=null and $out_check->getId()!=null)
    {
      $out_check->removeCounterReceipt($counter_receipt);
      $counter_receipt->calc();
      $counter_receipt->save();
    }
    return $this->redirect($request->getReferer());
  }
  */
  public function executeApplyExistingCheck(sfWebRequest $request)
  {
    //this is the amount to be allocated to the counter_receipt or out_payment
    $chequeamt=$request->getParameter("chequeamt");

    $counter_receipt=Fetcher::fetchOne("CounterReceipt",array("id"=>$request->getParameter("client_id")));

    $out_check=Fetcher::fetchOne("OutCheck",array("id"=>$request->getParameter("id")));

    //if counter_receipt has no balance, error
    if($counter_receipt->getBalance()==0)
    {
      $message="Cannot create cheque: CounterReceipt is fully paid.";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    $out_check->genCounterReceiptOutPayment($counter_receipt,$chequeamt);
    $counter_receipt->calc();
    $counter_receipt->save();
    return $this->redirect("counter_receipt/payments?id=".$counter_receipt->getId());

  }
  public function executeNew(sfWebRequest $request)
    {
        //this is the amount that the user is requesting
        $this->chequeamt=$request->getParameter("chequeamt");
        $this->client_class=$request->getParameter("client_class");
        $this->client_id=$request->getParameter("client_id");

        $this->check=new OutCheck();
        $this->check->setCode(OutCheckTable::genCode());
        $this->check->setCheckDate(MyDate::today());
        $this->check->setReceiveDate(MyDate::today());
        $this->check->setRemaining(0);
        
        if($request->getParameter("client_class")=="CounterReceipt")
        {
            $this->client=Fetcher::fetchOne("CounterReceipt",array("id"=>$request->getParameter("client_id")));
            $this->counter_receipt=$this->client;

            //if counter_receipt has no balance, error
            if($this->counter_receipt->getBalance()==0)
            {
              $message="Cannot create cheque: CounterReceipt is fully paid.";
              $this->getUser()->setFlash('error', $message);
              return $this->redirect($request->getReferer());
            }

            $this->vendor=$this->counter_receipt->getVendor();
            $this->check->setVendorId($this->counter_receipt->getVendorId());
            $this->existing_checks=$this->vendor->getChecksWithRemaining();
        }
        //this will never happen
        //because voucher checks are generated in voucher processform
        //and are simply edited afterwards
        /*
        elseif($request->getParameter("client_class")=="Voucher")
        {
            $this->client=Fetcher::fetchOne("Voucher",array("id"=>$request->getParameter("client_id")));
            $this->voucher=$this->client;

            $this->biller=$this->voucher->getBiller();
            $this->check->setVoucherAccountId($this->biller->getId());
            $this->existing_checks=$this->biller->getChecksWithRemaining();
        }
        */

        $this->form=new OutCheckForm($this->check);
        $this->out_check=$this->check;
    }
    public function executeEdit(sfWebRequest $request)
    {
        $this->out_check = $this->getRoute()->getObject();
        $this->form = $this->configuration->getForm($this->out_check);

        if($request->hasParameter("client_class") and $request->hasParameter("client_id"))
        {
          $this->client_class=$request->getParameter("client_class");
          $this->client_id=$request->getParameter("client_id");
          $this->client=Fetcher::fetchOne($this->client_class,array("id"=>$this->client_id));
        }
        else
        {
          echo "Access Denied"; 
          die();
        }
    }
    //note: voucher outchecks only go through here as edit
    //because they are auto generated in voucher/processform
    protected function processForm(sfWebRequest $request, sfForm $form)
    {
      $chequeamt=$request->getParameter("chequeamt");

      //possible clients
      $counter_receipt=null;
      $voucher=null;
      $client=Fetcher::fetchOne($request->getParameter("client_class"),array("id"=>$request->getParameter("client_id")));
      if(get_class($client)=="CounterReceipt")
        $counter_receipt=$client;
      elseif(get_class($client)=="Voucher")
        $voucher=$client;
      $this->counter_receipt=$counter_receipt;
      
      $requestparams=$request->getParameter($form->getName());
      $isnew=$requestparams["id"]=="";

      //set initial value of remaining as equal to amount
      $requestparams["remaining"]=$requestparams["amount"];
      
      //is_bank_transfer is a checkbox. if false, it will not exist. 
      if(!isset($requestparams["is_bank_transfer"]))
        $requestparams["is_bank_transfer"]=0;
      else
        $requestparams["is_bank_transfer"]=1;

      //validate-----------------------------------
      //if counter_receipt is fully paid, error
      if($counter_receipt and $counter_receipt->getBalance()==0 and $isnew)
      {
        $message="Cannot create cheque: CounterReceipt is fully paid.";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect("counter_receipt/view?id=".$counter_receipt->getId());
      }

      //if check has no check number and is not bank transfer, error
      if(trim($requestparams["check_no"])=="" and $requestparams["is_bank_transfer"]==0)
      {
        $message="Please check 'Is Bank Transfer' or enter a check number";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
  
      //if check has no amount, error
      if($requestparams["amount"]==0)
      {
        $message="Please enter an amount";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
  

      $form->bind($requestparams, $request->getFiles($form->getName()));
      if ($form->isValid())
      {
        $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';
  
        try {
          $out_check = $form->save();
          $out_check->calc();
          $out_check->save();
          
          //do this only if in check is new
          //else it is edit, do not create payment
          if($counter_receipt and $isnew)
          {
            $out_check->genCounterReceiptOutPayment($counter_receipt,$chequeamt);
            $counter_receipt->calc();
            $counter_receipt->save();
          }
        } catch (Doctrine_Validator_Exception $e) {
  
          $errorStack = $form->getObject()->getErrorStack();
  
          $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
          foreach ($errorStack as $field => $errors) {
              $message .= "$field (" . implode(", ", $errors) . "), ";
          }
          $message = trim($message, ', ');
  
          $this->getUser()->setFlash('error', $message);
          return sfView::SUCCESS;
        }
  
        $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $out_check)));
  
        $this->getUser()->setFlash('notice', $notice);

        if($counter_receipt)
          $this->redirect("counter_receipt/payments?id=".$counter_receipt->getId());
        else if($voucher)
          $this->redirect("voucher/view?id=".$voucher->getId());
    }
      else
      {
        $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
      }
    }
    public function executeDelete(sfWebRequest $request)
    {
      $request->checkCSRFProtection();
  
      $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));
  
      $check=$this->getRoute()->getObject();

      $out_payments=$check->getOutPayment();
      if(count($out_payments)>0)
      {
        $message="Cannot delete check: Please delete related payments first";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }

      if ($check->delete())
      {
        $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
      }
  
      $this->redirect($request->getReferer());
    }
    public function executeView(sfWebRequest $request)
    {
      $this->check=$this->getRoute()->getObject();
    }
}
