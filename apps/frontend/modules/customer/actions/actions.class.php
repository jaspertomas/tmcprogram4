<?php

require_once dirname(__FILE__).'/../lib/customerGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/customerGeneratorHelper.class.php';

/**
 * customer actions.
 *
 * @package    sf_sandbox
 * @subpackage customer
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class customerActions extends autoCustomerActions
{
  public function executeView(sfWebRequest $request)
  {
    $this->customer = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->invoice);
    
    $this->returnss = $this->customer->getReturns();
    $this->replacements = $this->customer->getReplacements();
  }
  public function executeViewUnpaid(sfWebRequest $request)
  {
    $this->customer = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->invoice);

    $this->invoices=null;
    if($request->hasParameter("ids"))
    {
      $this->invoices=Doctrine_Query::create()
        ->from('Invoice i')
      	->whereIn('i.id',$request->getParameter("ids"))
        ->orderBy("i.date asc")
        ->execute();
    }
    else
    {
      $this->invoices=$this->customer->getUnpaidInvoices("asc");
    }    
    
    $this->total=0;
    $this->totaldue=0;
    foreach($this->invoices as $invoice)
    {
      $this->total+=$invoice->getBalance();
      if($invoice->isDue())
        $this->totaldue+=$invoice->getBalance();
    }
  }
  public function executeViewUnpaidPdf(sfWebRequest $request)
  {
    $this->executeViewUnpaid($request);
    $this->message=$request->getParameter("message");

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeSearch(sfWebRequest $request)
  {
    $this->invoice_id=null;
    if($request->getParameter("invoice_id"))
      $this->invoice_id=$request->getParameter("invoice_id");
  
    $query=Doctrine_Query::create()
        ->from('Customer i')
      	->where('i.name like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("i.name")
        ;

  	$results=$query->execute();
  	
  	/*
  	if(count($results)==1)
  	{
            $this->customer=$results[0];
            $this->redirect("customer/view?id=".$this->customer->getId());
  	}
  	else
  	*/
    {
        $this->customers=$results;
    }
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->customer = new Customer();
    
    //set customer if param customer_name
    if($request->hasParameter("name"))
    {
      $this->customer->setName($request->getParameter("name"));
    }

    $this->form = $this->configuration->getForm($this->customer);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());
    $form->bind($requestparams, $request->getFiles($form->getName()));

    //if max discount level less than discount level
    $discount_level=Fetcher::fetchOne("DiscountLevel",array("id"=>$requestparams['discount_level_id']));
    $max_discount_level=Fetcher::fetchOne("MaxDiscountLevel",array("id"=>$requestparams['max_discount_level_id']));
    if($max_discount_level->getLess() < $discount_level->getLess())
    {
          $message="Max Discount Level cannot be less than Discount Level";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
    }

    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $customer = $form->save();
        //recalc duedate for all invoices
        Doctrine_Query::create()
          ->update('Invoice i')
          ->set('i.duedate', 'DATE_ADD( date, INTERVAL '.$customer->getTerms().' DAY )')
          ->where('i.customer_id=?',$customer->getId())
          ->execute();
        
        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $customer)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@customer_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect(array('sf_route' => 'customer_edit', 'sf_subject' => $customer));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("customer");
    $customer=Doctrine_Query::create()
        ->from('Customer c')
      	->where('c.id = '.$requestparams["id"])
      	->fetchOne();
    $customer->setAddress($requestparams["address"]);
    $customer->setAddress2($requestparams["address2"]);
    $customer->setPhone1($requestparams["phone1"]);
    $customer->setTinNo($requestparams["tin_no"]);
    $customer->setRep($requestparams["rep"]);
    $customer->setEmail($requestparams["email"]);
    $customer->setNotepad($requestparams["notepad"]);
    $customer->save();
    $this->redirect($request->getReferer());
  }
  public function executeAdjustCollectionNotes(sfWebRequest $request)
  {
    $customer=Doctrine_Query::create()
        ->from('Customer c')
      	->where('c.id = '.$request->getParameter('id'))
      	->fetchOne();
  	$customer->setCollectionNotes($request->getParameter('collection_notes'));
  	$customer->save();
  	die();
  }
  public function executeFiles(sfWebRequest $request)
  {
    $this->customer=MyModel::fetchOne("Customer",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('customer');
    $this->file->setParentId($this->customer->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="customer"')
      ->andWhere('f.parent_id='.$this->customer->getId())
      ->execute();
  }
  public function executeViewStatementDmf(sfWebRequest $request)
  {
    $this->forward404Unless(
      $customer=Doctrine_Query::create()
        ->from('Customer c, c.Invoice i')
      	->where('c.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Customer with id (%s) not found.', $request->getParameter('id')));

    $content=MyCustomerHelper::PrintableStatement($customer);

    $printtoscreen=false;
    if($printtoscreen)
    {
      $content=str_replace("\n","<br>",$content);
      $content=str_replace(" ","&nbsp;",$content);
      echo $content;
      die();
    }

    $content=bin2hex($content);
      
    $response = $this->getResponse();
    $response->clearHttpHeaders();
    //$response->setContentType($mimeType);
    $response->setHttpHeader('Content-Disposition', 'attachment; filename="' . basename(str_replace(" ","",$customer->getName()).".dmf") . '"');
    $response->setHttpHeader('Content-Description', 'File Transfer');
    $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
    $response->setHttpHeader('Content-Length', 80*39);
    $response->setHttpHeader('Cache-Control', 'public, must-revalidate');
    $response->setHttpHeader('Pragma', 'public');
    $response->setContent($content);
    $response->sendHttpHeaders();

    sfConfig::set('sf_web_debug', false);
    return sfView::NONE;
  }
  public function executeViewChecks(sfWebRequest $request)
  {
    $this->customer=$this->getRoute()->getObject();
    $this->checks=$this->customer->getInCheck();
  }
  public function executeMerge1(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
    
  }
  public function executeMerge2(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
 
    $this->merger_id=$request->getParameter("merger_customer_id");
    $this->mergee_id=$request->getParameter("mergee_customer_id");
    $this->merger=Doctrine_Query::create()
        ->from('Customer p')
        ->where("p.id = '".$this->merger_id."'")
        ->fetchOne();
    $this->mergee=Doctrine_Query::create()
        ->from('Customer p')
        ->where("p.id = '".$this->mergee_id."'")
        ->fetchOne();
   
  }
  public function executeMerge3(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
 
    $merger=Doctrine_Query::create()
        ->from('Customer p')
        ->where("p.id = '".$request->getParameter("merger_customer_id")."'")
        ->fetchOne();
    $mergee=Doctrine_Query::create()
        ->from('Customer p')
        ->where("p.id = '".$request->getParameter("mergee_customer_id")."'")
        ->fetchOne();
   
    //merger merges into mergee
    //merger changes customer id
    Doctrine_Query::create()
      ->update('Invoice p')
      ->set('p.customer_id',$mergee->getId())
      ->where('p.customer_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('InCheck p')
      ->set('p.customer_id',$mergee->getId())
      ->where('p.customer_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Quotation p')
      ->set('p.customer_id',$mergee->getId())
      ->where('p.customer_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('CustomerOrder p')
      ->set('p.customer_id',$mergee->getId())
      ->where('p.customer_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('InvoiceDr p')
      ->set('p.customer_id',$mergee->getId())
      ->where('p.customer_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('InvoiceDrDetail p')
      ->set('p.customer_id',$mergee->getId())
      ->where('p.customer_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Returns p')
      ->set('p.client_id',$mergee->getId())
      ->where('p.client_id = ?', $merger->getId())
      ->andWhere('p.client_class = "Customer"')
      ->execute();
    Doctrine_Query::create()
      ->update('Replacement p')
      ->set('p.client_id',$mergee->getId())
      ->where('p.client_id = ?', $merger->getId())
      ->andWhere('p.client_class = "Customer"')
      ->execute();
    
    $merger->delete();

    $this->getUser()->setFlash('msg', $merger." merged into ".$mergee, true);
  }
  public function executeLinkSalesman1(sfWebRequest $request)
  {
    $this->customer=$this->getRoute()->getObject();
    $this->form = new InvoiceForm(new Invoice());
  }  
  public function executeLinkSalesman2(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");
    $customer=Fetcher::fetchOne("Customer",array("id"=>$request->getParameter("id")));
    $salesman=Fetcher::fetchOne("Employee",array("id"=>$requestparams["salesman_id"]));
    if($salesman==null)
    {
      $this->getUser()->setFlash('error', "Salesman not found");
      return $this->redirect($request->getReferer());
    }

    Doctrine_Query::create()
      ->update('Invoice i')
      ->set('i.salesman_id',$salesman->getId())
      ->where('i.customer_id = ?', $customer->getId())
      ->execute();
    
    $this->getUser()->setFlash('notice', "Salesman ".$salesman->getName()." successfully linked to customer ".$customer->getName());
    return $this->redirect($request->getReferer());
  }  
  public function executeAverageSales(sfWebRequest $request)
  {
    $this->customer=$this->getRoute()->getObject();
    // $this->form = new InvoiceForm(new Invoice());
    $this->invoices=Fetcher::fetch("Invoice",array("customer_id"=>$this->customer->getId()));

    $today=MyDateTime::today()->getstartofmonth();
    // $today=$today->addmonths(-6)->getstartofmonth();

    $this->sales=array();
    $this->total=0;
    $this->dates=array();
    for($i=0;$i<12;$i++)
    {
      $startofmonth=$today->tomysql();
      $endofmonth=$today->getendofmonth()->tomysql();

      //get total sales excluding invoices cancelled and not closed
      $result=Doctrine_Query::create()
      ->select("sum(i.total), customer_id")
      ->from('Invoice i')
      ->where('i.date>="'.$startofmonth.'"')
      ->andWhere('i.date<="'.$endofmonth.'"')
      ->andWhere('i.status!="Cancelled"')
      ->andWhere('i.is_temporary=0')
      ->andWhere('i.customer_id='.$request->getParameter("id"))
      ->groupBy('i.customer_id')
      ->execute()->toArray();

      $sum=$result[0]["sum"];
      if(!$sum)$sum=0;
      $this->sales[$i]=$sum;
      $this->total+=$sum;
      $this->dates[$i]=$today->getLongMonth()." ".$today->getYear();
      $today=$today->addmonths(-1);
    }
    $this->average=$this->total/24;

    $this->sales2=array();
    $this->total2=0;
    $this->dates2=array();
    for($i=0;$i<12;$i++)
    {
      $startofmonth=$today->tomysql();
      $endofmonth=$today->getendofmonth()->tomysql();

      //get total sales excluding invoices cancelled and not closed
      $result=Doctrine_Query::create()
      ->select("sum(i.total), customer_id")
      ->from('Invoice i')
      ->where('i.date>="'.$startofmonth.'"')
      ->andWhere('i.date<="'.$endofmonth.'"')
      ->andWhere('i.status!="Cancelled"')
      ->andWhere('i.is_temporary=0')
      ->andWhere('i.customer_id='.$request->getParameter("id"))
      ->groupBy('i.customer_id')
      ->execute()->toArray();

      $sum=$result[0]["sum"];
      if(!$sum)$sum=0;
      $this->sales2[$i]=$sum;
      $this->total2+=$sum;
      $this->dates2[$i]=$today->getLongMonth()." ".$today->getYear();
      $today=$today->addmonths(-1);
    }
    $this->average2=$this->total2/24;
  }  


  public function executeBir1(sfWebRequest $request)
  {
    /*
NS MANGIO 804
sierra mirada 9727
arcos 8871
Aspen Construction and Development Corporation 341
Jupiter Kho De Guzman 10035
Gras Builders Incorporated 2373
Malate Tourist Development Corp. 7388
MMG True Enterprises Inc. 10116
Tanza Doctors Hospital, Inc. 9413
Welcome Builders Corp. 6834
marinduque interisland
     */
    $this->customer_ids=array(804,9727,8871,341,10035,2373,7388,10116,9413,6834,5909);
    $this->customers = Doctrine_Query::create()
    ->from('Customer c')
    ->whereIn('c.id',$this->customer_ids)
    ->execute();      

    
  }  
  public function executeBir2(sfWebRequest $request)
  {

    $this->customer=Fetcher::fetchOne("Customer",array("id"=>$request->getParameter("id")));

    $this->invoices=Doctrine_Query::create()
      ->from('Invoice i')
      ->where('i.date>="2023-01-01"')
      ->andWhere('i.date<="2023-12-31"')
      ->andWhere('i.status!="Cancelled"')
      ->andWhere('i.is_temporary=0')
      ->andWhere('i.template_id=1')
      ->andWhere('i.customer_id='.$request->getParameter("id"))
      ->execute();
  }

  public function executeBir2Pdf(sfWebRequest $request)
  {
    $this->executeBir2($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }

}
