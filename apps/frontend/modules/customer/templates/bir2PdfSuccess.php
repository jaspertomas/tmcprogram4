<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "LETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('BirResponsePdf');

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->startPageGroup();
$pdf->AddPage();

$grandtotal=0;
foreach($invoices as $invoice)
{
  $total=$invoice->getWithCommissionTotal()+$invoice->getWithholdingTax();
  $grandtotal+=$total;
}

$pdf->SetFont('helvetica', '', 10, '', true);
$date=MyDateTime::today();
$pdf->write(0,$date->toprettydate(),'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="Tradewind Water System and Industrial Machinery Inc.";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="711 T. Alonzo St. Santa Cruz, Manila";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="Dear Sir / Madam, ";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="Good Day! ";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="This is to certify that the following are our purchases from Tradewind Water Systems for fiscal year of 2023, with total amount of P".MyDecimal::format($grandtotal);
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);

//body
$widths=array(45,45,45,45);
$scale=3.543;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$content=array(
    'Invoice Date'
    ,'Reference #'
    ,'SI Amount'
//    ,'EWT'
          );
$tbl = <<<EOD
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center">$content[0]</td>
  <td width="$widths[1]" align="center">$content[1]</td>
  <td width="$widths[2]" align="center">$content[2]</td>
 </tr>
</thead>
EOD;
//  <td width="$widths[3]" align="center">$content[3]</td>


foreach($invoices as $invoice)
{
  $total=$invoice->getWithCommissionTotal()+$invoice->getWithholdingTax();
  $content=array(
    MyDateTime::frommysql($invoice->getDate())->toprettydate(),
    $invoice->getInvno(),
    MyDecimal::format($total),
    $invoice->getWithholdingTax(),
  );
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[1]" align="center">$content[1]</td>
<td width="$widths[2]" align="right">$content[2]</td>
</tr>
EOD;
}
//<td width="$widths[3]" align="right">$content[3]</td>

//-----------
$content=array(
  ''
  ,'Total'
  ,MyDecimal::format($grandtotal)
  ,''
);
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[1]" align="right">$content[1]</td>
<td width="$widths[2]" align="right"><b>$content[2]</b></td>
</tr>
</table>
EOD;
//<td width="$widths[3]" align="center">$content[3]</td>


//-----------
$content=array(
  ''
  ,'Net of Vat'
  ,MyDecimal::format($grandtotal/1.12)
  ,''
);
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[1]" align="right">$content[1]</td>
<td width="$widths[2]" align="right"><b>$content[2]</b></td>
</tr>
</table>
EOD;
//<td width="$widths[3]" align="center">$content[3]</td>


//-----------

$pdf->writeHTML($tbl, true, false, false, false, '');
//end addresses and tin numbers table


$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="Thank you.";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="Sincerely Yours, ";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);
$text="_________________";
$pdf->write(0,$text,'',false,'L',true,0,false,false,0,0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('BirResponse-'.$customer->getName().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

