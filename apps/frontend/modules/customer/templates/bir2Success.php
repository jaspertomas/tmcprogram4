<?php
$grandtotal=0;
foreach($invoices as $invoice)
{
  $total=$invoice->getWithCommissionTotal()+$invoice->getWithholdingTax();
  $grandtotal+=$total;
}
?>

<h2>BIR Response: <?php echo $customer->getName();?></h2>

<?php
$date=MyDateTime::today();
echo $date->toprettydate();
?>
<br>
<br>
To: Tradewind Water System and Industrial Machinery Inc<br>
711 T. Alonzo St. Santa Cruz, Manila<br>
<br>
<br>
Dear Sir / Madam,<br>
<br>
Good Day!<br>
<br>
This is to certify that the following are our purchases from Tradewind Water System and Industrial Machinery Inc. for fiscal year of 2023, with total amount of P<?php echo MyDecimal::format($grandtotal)?>


<br>
<br>


<table border="1">
<thead>
 <tr>
  <td align="center">Invoice Date</td>
  <td align="center">Reference #</td>
  <td align="center">SI Amount</td>
  <!-- <td align="center">EWT</td> -->
 </tr>
</thead>
<?php


foreach($invoices as $invoice)
{
  $total=$invoice->getWithCommissionTotal()+$invoice->getWithholdingTax();
  $content=array(
    MyDateTime::frommysql($invoice->getDate())->toprettydate(),
    $invoice->getInvno(),
    MyDecimal::format($total),
    $invoice->getWithholdingTax(),
  );
  ?>
<tr>
<td align="center"><?php echo $content[0]?></td>
<td align="center"><?php echo $content[1]?></td>
<td align="right"><?php echo $content[2]?></td>
<!-- <td align="right"><?php echo $content[3]?></td> -->
</tr>
<?php
}

//-----------
$content=array(
  ''
  ,'Total'
  ,MyDecimal::format($grandtotal)
  ,''
);
?>
<tr>
<td align="center"><?php echo $content[0]?></td>
<td align="right"><?php echo $content[1]?></td>
<td align="right"><b><?php echo $content[2]?></b></td>
<!-- <td align="center"><?php echo $content[3]?></td> -->
</tr>

<?php
$content=array(
  ''
  ,'Net of Vat'
  ,MyDecimal::format($grandtotal/1.12)
  ,''
);
?>
<tr>
<td align="center"><?php echo $content[0]?></td>
<td align="right"><?php echo $content[1]?></td>
<td align="right"><b><?php echo $content[2]?></b></td>
<!-- <td align="center"><?php echo $content[3]?></td> -->
</tr>


</table>
<br>
Thank you.<br>
<br>
Sincerely Yours, <br>
<br>
<br>
Name and Signature: _________________<br>
<br>
Title and Position: _________________<br>
<br>
TIN: _________________<br>
<br>
Date: _________________<br>
<br>
Contact No: _________________<br>
<?php die();?>