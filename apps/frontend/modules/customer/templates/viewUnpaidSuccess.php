<?php use_helper('I18N', 'Date'); ?>
<h1>Customer Unpaid Invoices: <?php echo $customer?> </h1>
<h3><?php echo link_to("Create Invoice","invoice/new?customer_id=".$customer->getId()) ?></h3>
Address: <?php echo $customer->getAddress()?>
<br>Phone: <?php echo $customer->getPhone1()?>
<br>Tin No: <?php echo $customer->getTinNo()?>
<br><?php echo link_to("Edit","customer/edit?id=".$customer->getId()) ?>

<hr>
<table>
  <tr>
    <td>Total Receivable: <?php echo MyDecimal::format($total)?></td>
    <td> | </td>
    <td>Total Due: <?php echo MyDecimal::format($totaldue)?></td>
    <td> | </td>
    <td>Notes: </td>
    <td><?php echo form_tag("customer/adjustCollectionNotes",array("class"=>"collection_notes_form"))?>
      <input type=hidden name=id value=<?php echo $customer->getId()?>><input name=collection_notes value=<?php echo $customer->getCollectionNotes()?>>
      <input type=submit value=Save>
      </form>
</td>
  </tr>
</table>

<hr>
<?php echo form_tag("customer/viewUnpaidPdf?id=".$customer->getId(),array("method"=>"get"))?>
Print Billing Statement
<table>
  <tr>
    <td>Message:</td>
    <td><textarea name=message rows=2 cols=50>This serves as the billing statement for the following purchased items:</textarea></td>
    <td><input type=submit value="Print"></td>
  </tr>
</table>

Please check the invoices to include in billing statement.
<table border=1>
  <tr>
    <td><input id="sf_admin_list_batch_checkbox" onclick="checkAll();" type="checkbox"></td>
    <td>Date</td>
    <td>Invoice</td>
    <td>Due Date</td>
    <td colspan=10>Status</td>
  </tr>
  <tr>
    <td></td>
    <td colspan=3>Product</td>
    <td>Notes</td>
    <td>Qty</td>
    <td>Price</td>
    <td>Total</td>
    <td>Balance</td>
  </tr>
  <?php foreach($invoices as $invoice){?>
  <?php $runningbalance=$invoice->getTotal(); ?>
  <tr>
    <td><input type=checkbox name="ids[]" value="<?php echo $invoice->getId()?>" <?php if($invoice->isDue())echo"checked=checked"?>/></td>
    <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
    <td><?php echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ?></td>
    <td><?php echo MyDateTime::frommysql($invoice->getDuedate())->toshortdate() ?></td>
    <td colspan=10>
      <font color=<?php $isduestring=$invoice->getIsDueString();echo $invoice->getColorForIsDueString($isduestring)?>>
        <?php echo $isduestring;?>
      </font>;
      <span id=collection_status_display_<?php echo $invoice->getId()?> class=collection_status_display>
        <font color=<?php echo $invoice->getColorForCollectionStatusString($invoice->getCollectionStatus())?>>
          <?php echo $invoice->getCollectionStatus();?>
        </font>
      </span>
      <br>
      <input type=button invoice_id="<?php echo $invoice->getId()?>" class=collection_status_button value="Pending">
      <input type=button invoice_id="<?php echo $invoice->getId()?>" class=collection_status_button value="Bill Sent">
      <input type=button invoice_id="<?php echo $invoice->getId()?>" class=collection_status_button value="Cheque Ready">
      </font>
    </td>
  </tr>
  <?php foreach($invoice->getInvoicedetail() as $detail){?>
    <tr>
    	<td></td>
    	<td colspan=3><?php echo $detail->getProduct() ?></td>
      <td><?php echo $detail->getDescription() ?></td>
      <td align=right><?php echo $detail->getQty() ?></td>
      <td align=right><?php echo MyDecimal::format($detail->getPrice()) ?></td>
      <td align=right><?php echo MyDecimal::format($detail->getTotal()) ?></td>
    	<td></td>
    </tr>
  <?php }?>
  
  </tr>
    <td align=right colspan=7>Invoice Total</td>
    <td align=right><b><?php echo MyDecimal::format($invoice->getTotal()) ?></b></td>
    <td align=right><b><?php echo MyDecimal::format($runningbalance) ?></b></td>
  </tr>

  <!--original cash payment-->
  <?php if($invoice->getCash()!=0){ ?>    
    <?php $runningbalance-=$invoice->getCash(); ?>
    <tr>
    	<td align=right colspan=7>Original Cash Payment</td>
    	<td align=right><?php echo MyDecimal::format($invoice->getCash())?></td>
    	<td align=right><?php echo MyDecimal::format($runningbalance)?></td>
    </tr>
  <?php } ?>
  
  <!--original cheque payment-->
  <?php if($invoice->getChequeamt()!=0){ ?>    
    <?php $runningbalance-=$invoice->getChequeamt(); ?>
    <tr>
    	<td align=right colspan=7>Original Cheque Payment: <?php echo MyDateTime::frommysql($invoice->getChequedate())->toshortdate()." ".$invoice->getCheque()?></td>
    	<td align=right><?php echo MyDecimal::format($invoice->getChequeamt())?></td>
    	<td align=right><?php echo MyDecimal::format($runningbalance)?></td>
    </tr>
  <?php } ?>

  <!--events-->
  <?php foreach($invoice->getEvents() as $event){?>
    <?php
          $type="";
          $details="";
          switch($event->getType())
          {
            case "ChequeCollect": 
              $type="Cheque Payment";
              $details=trim(MyDateTime::frommysql($event->getDate())->toshortdate()." ".$event->getDetail1());
              break;
            case "CashCollect": 
              $type="Cash Payment";
              $details=trim(MyDateTime::frommysql($event->getDate())->toshortdate()." ".$event->getDetail1());
              break;
          }
          $runningbalance-=$event->getAmount();
    ?>
    <tr>
    	<td align=right colspan=7><?php echo $type.": ".$details?></td>
    	<td align=right><?php echo MyDecimal::format(0-$event->getAmount())?></td>
    	<td align=right><?php echo MyDecimal::format($runningbalance)?></td>
    </tr>
  <?php }//end foreach($invoice->getEvents() ?>

    </tr>
    	<td align=right colspan=8>Remaining Balance:</td>
      <td align=right><b><?php echo MyDecimal::format($runningbalance) ?></b></td>
    </tr>
    <?php $runningbalance=0?>

    <tr><td colspan=10></td></tr>

  <?php } ?>

</table>
</form>



<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByName('ids[]'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked;
  } 
  return true;
}
/* ]]> */
</script>


<script>
//on clicking Save on notes edit form
$(document).on("submit", ".collection_notes_form", function(e){
e.preventDefault();
    $.ajax({ // create an AJAX call...
        data: $(this).serialize(), // get the form data
        type: $(this).attr('method'), // GET or POST
        url: $(this).attr('action'), // the file to call
        success: function(response) { // on success..
  alert("Note Saved");
        }
    });
return false;
});
//on clicking Due, Bill Sent or Cheque REady
$(document).on("click", ".collection_status_button", function(e){
  var collection_status=$(this).val();
  var invoice_id=$(this).attr("invoice_id");
  $.ajax({ // create an AJAX call...
      data: "id="+invoice_id+"&collection_status="+collection_status, // get the form data
      type: "post", // GET or POST
      url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/invoice/adjustCollectionStatus/action",
      success: function(response) { // on success..
        $("#collection_status_display_"+invoice_id).html(response);
      }
  });
});
</script>
