<?php use_helper('I18N', 'Date'); ?>
<h1>Customer Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  
  <?php 
  $matchfound=false;
  foreach($customers as $customer)
  if(strtolower($customer->getName())==strtolower($searchstring)){$matchfound=true;break;}
  if(!$matchfound){
  ?>
  <tr>
    <td><?php echo $searchstring?> (New Customer) </td>
    <?php if($invoice_id!=null){?>
      <td><?php echo link_to("Use in Invoice","invoice/processChooseCustomer?id=".$invoice_id."&customer_name=".$searchstring) ?></td>
    <?php } ?>
    <td><?php echo link_to("Create New<br>Customer","customer/new?name=".$searchstring) ?></td>
    <td></td>
    <td>
      <?php 
        echo link_to("Create Invoice","invoice/new?customer_name=".$searchstring);
      ?>
    </td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php } ?>
  <?php foreach($customers as $customer){?>
  <tr>
    <td><?php echo $customer->getName() ?></td>
    <?php if($invoice_id!=null){?>
      <td><?php echo link_to("Use in Invoice","invoice/processChooseCustomer?id=".$invoice_id."&customer_id=".$customer->getId()) ?></td>
    <?php } ?>
    <td><?php echo link_to("View","customer/view?id=".$customer->getId()) ?></td>
    <td><?php echo link_to("View Unpaid","customer/viewUnpaid?id=".$customer->getId()) ?></td>
    <td>
      <?php 
        echo link_to("Create Invoice","invoice/new?customer_id=".$customer->getId());
      ?>
    </td>
    <td>
      <?php echo link_to("Create Quotation","quotation/new?customer_id=".$customer->getId());?>
    </td>
    <td><?php if(sfConfig::get('custom_enable_customer_return'))echo link_to("Create Return","returns/new?client_id=".$customer->getId()."&client_class=Customer");?></td>
    <td>
      <?php echo $customer->getNotepad();?>
    </td>
  </tr>
  <?php } ?>
</table>
