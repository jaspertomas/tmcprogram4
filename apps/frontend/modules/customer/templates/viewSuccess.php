<?php use_helper('I18N', 'Date'); ?>
<h1>Customer: <?php echo $customer?> </h1>
<h3><?php echo link_to("Create Invoice","invoice/new?customer_id=".$customer->getId()) ?></h3>

Terms: <?php echo $customer->getTerms()?> 
<br>Address: <?php echo $customer->getAddress()?>
<br>Address2: <?php echo $customer->getAddress2()?>
<br>Phone: <?php echo $customer->getPhone1()?>
<br>Fax: <?php echo $customer->getFaxnum()?>
<br>Email: <?php echo $customer->getEmail()?>
<br>Tin No: <?php echo $customer->getTinNo()?>
<br>Notes: <?php echo $customer->getNote()?>
<br>Representative: <?php echo $customer->getRep()?>; <?php echo $customer->getRepNo()?>
<br>Representative 2: <?php echo $customer->getRep2()?>; <?php echo $customer->getRep2No()?>

<br><?php echo link_to("Edit","customer/edit?id=".$customer->getId()) ?>
 | <?php echo link_to('Delete','customer/delete?id='.$customer->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
<br><?php echo link_to("View Unpaid Invoices","customer/viewUnpaid?id=".$customer->getId()) ?>
 | <?php echo link_to("Print Statement of Account","customer/viewStatementDmf?id=".$customer->getId()) ?>
<br><?php echo link_to("View Files","customer/files?id=".$customer->getId()) ?>
 | <?php if($sf_user->hasCredential(array('admin'), false)){echo link_to("Link Salesman","customer/linkSalesman1?id=".$customer->getId());}?>
<br><?php echo link_to("View Checks","customer/viewChecks?id=".$customer->getId()) ?>
<br><?php echo link_to("View Average Sales","customer/averageSales?id=".$customer->getId()) ?>


<br>
<br>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Invoice</td>
    <td>Status</td>
    <td>Product</td>
    <td>Qty</td>
    <td>Price</td>
    <td>Discrate</td>
    <td>Discamt</td>
    <td>Item Total</td>
    <td>Total</td>
    <?php if(sfConfig::get('custom_visible_commission_payment_in_customer_view')){?>
    <td>Commission Paid</td>
    <?php } ?>
  </tr>
  <?php foreach($customer->getInvoices("desc") as $invoice){?>
  <tr>
    <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
    <td>
      <?php echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ?>
      <button onClick={copy("<?php echo $invoice->getInvno()?>")}>C</button>
    </td>
    <td>
      <?php 
        if($invoice->getIsTemporary()==0)echo $invoice->getStatus(); 
        else echo $invoice->getIsTemporaryString();
      ?>
    </td>
    <td>
      <?php if($invoice->getIsTemporary()==0 and $invoice->getStatus()=="Pending"){?>
        Due Date: <?php echo MyDateTime::frommysql($invoice->getDuedate())->toshortdate() ?>
      <?php } ?>
    </td>
  	<td></td>
  	<td></td>
  	<td></td>
  	<td></td>
  	<td></td>
  	<td><b><?php echo MyDecimal::format($invoice->getTotal())?></b></td>
    <?php if(sfConfig::get('custom_visible_commission_payment_in_customer_view')){?>
  	<td>
      <?php echo $invoice->getSalesman()?>
      | 
      <?php if($invoice->getCommissionPaymentId())echo link_to("=".$invoice->getCommissionPaymentId()."=","commission_payment/view?id=".$invoice->getCommissionPaymentId())?>
    </td>
    <?php } ?>
  </tr>
  <?php foreach($invoice->getInvoicedetail() as $detail){?>
  <tr>
  	<td></td>
  	<td></td>
  	<td></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
    <td><?php echo $detail->getQty() ?></td>
    <td><?php echo  MyDecimal::format($detail->getPrice()) ?></td>
    <td><?php echo $detail->getDiscrate() ?></td>
    <td><?php echo $detail->getDiscamt() ?></td>
    <td><?php echo  MyDecimal::format($detail->getTotal()) ?></td>
    <td></td>
  </tr>
  <?php }?>
  <?php } ?>
</table>

<!-- -------------------- -->

<hr>

<?php if(count($returnss)==0)echo "No returns found";else{ ?>
<h1>Return Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Date</td>
    <td>Reference</td>
    <td>Particulars</td>
  </tr>
  
  <?php 
  foreach($returnss as $returns){?>
  <tr>
    <td><?php echo link_to($returns->getCode(),"returns/view?id=".$returns->getId()) ?></td>
    <td><?php echo MyDateTime::frommysql($returns->getDate())->toshortdate() ?></td>
    <td><?php $ref=$returns->getRef();if($ref!=null)echo link_to($ref,$returns->getRefClass()."/view?id=".$returns->getRefId()) ?></td>
    <td><?php echo $returns->getParticularsString() ?></td>
    <td><?php echo $returns->getNotes() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

<hr>

<?php if(count($replacements)==0)echo "No replacements found";else{ ?>
<h1>Replacement Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Date</td>
    <td>Reference</td>
    <td>Particulars</td>
  </tr>
  
  <?php 
  foreach($replacements as $replacement){?>
  <tr>
    <td><?php echo link_to($replacement->getName(),"replacement/view?id=".$replacement->getId()) ?></td>
    <td><?php echo MyDateTime::frommysql($replacement->getDate())->toshortdate() ?></td>
    <td><?php $ref=$replacement->getRef();if($ref!=null)echo link_to($ref,$replacement->getRefClass()."/view?id=".$replacement->getRefId()) ?></td>
    <td><?php echo $replacement->getParticularsString() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>


<input id="copy_text">
<script>
function copy(mystring)
{
  var copyText = document.getElementById("copy_text");
  copyText.value=mystring;
  copyText.select();
  document.execCommand("copy");
  //alert(copyText.value+"copied to clipboard");
}
</script>
