<?php use_helper('I18N', 'Date'); ?>

<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h2><?php echo link_to("Back to Customer View","customer/view?id=".$customer->getId())?></h2>
<h1>Customer: <?php echo $customer?> </h1>

Assign a salesman to all invoices belonging to this customer
<?php echo form_tag_for($form,"customer/linkSalesman2")?>
<input type=hidden name=id id=id value=<?php echo $customer->getId()?>>
<br>Salesman: <?php echo $form["salesman_id"]?>
<br><input type=submit >
</form>
