<?php use_helper('I18N', 'Date'); ?>
<h1>Average Sales of <?php echo $customer?> </h1>
<?php echo link_to("View all invoices","customer/view?id=".$customer->getId())?>
<h3>From <?php echo $dates[0]?> to <?php echo $dates[11]?> (This Year)</h3>
<hr>
<table>
  <tr>
    <td></td>
  </tr>
</table>

<table border=1>
  <tr>
    <td>Month of</td>
    <td>Total Sales</td>
  </tr>
  <?php for($i=0;$i<12;$i++){?>
  <tr>
    <td><?php echo $dates[$i]?></td>
    <td align=right><?php echo MyDecimal::format($sales[$i])?></td>
  </tr>
  <?php } ?>
  <tr>
  <td><b>Average</b></td>
    <td align=right><b><?php echo MyDecimal::format($average)?></b></td>
  </tr>
</table>

<h3>From <?php echo $dates2[0]?> to <?php echo $dates2[11]?> (Last Year) </h3>
<hr>
<table>
  <tr>
    <td></td>
  </tr>
</table>

<table border=1>
  <tr>
    <td>Month of</td>
    <td>Total Sales</td>
  </tr>
  <?php for($i=0;$i<12;$i++){?>
  <tr>
    <td><?php echo $dates2[$i]?></td>
    <td align=right><?php echo MyDecimal::format($sales2[$i])?></td>
  </tr>
  <?php } ?>
  <tr>
  <td><b>Average</b></td>
    <td align=right><b><?php echo MyDecimal::format($average2)?></b></td>
  </tr>
</table>
