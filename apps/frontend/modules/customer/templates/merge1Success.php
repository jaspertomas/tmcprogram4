<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Merge Customers</h1>
<br>
Remember to backup your database first!
<br>
<?php echo form_tag("customer/merge2");?>
Customer ID to merge: <input name=merger_customer_id id=merger_customer_id>
<br>Customer ID to merge into <input name=mergee_customer_id id=mergee_customer_id>
<br><input type=submit value=Test>
</form>
