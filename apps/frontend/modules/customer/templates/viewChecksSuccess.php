<?php use_helper('I18N', 'Date') ?>
<?php include_partial('in_check/assets') ?>

<div id="sf_admin_container">

<br><?php echo link_to("Back to Customer View","customer/view?id=".$customer->getId())?>

<h1>Checks for <?php echo $customer?></h1>
<table>
<tr>
  <th>Check Voucher</th>
  <th>Receive Date</th>
  <th>Check Date</th>
  <th>Check No.</th>
  <th>Amount</th>
  <th>Remaining</th>
</tr>

<?php foreach($checks as $check){ ?>
  <tr>
    <td><?php echo link_to($check->getCode(),"in_check/view?id=".$check->getId())?></td>
    <td><?php echo MyDateTime::frommysql($check->getReceiveDate())->toshortdate()?></td>
    <td><?php echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
    <td><?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
    <td><?php echo number_format ( $check->getAmount(),2 )?></td>
    <td><?php echo number_format ( $check->getRemaining(),2 )?></td>
  </tr>

  <?php foreach($check->getInvoice() as $invoice){ ?>
    <tr>
      <td></td>
      <td><?php echo "Inv ".link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())?></td>
      <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate()?></td>
      <td><?php echo "P".$invoice->getChequeamt()?></td>
      <td colspan=10><?php echo $invoice->getParticularsString()?></td>
    </tr>
  <?php } ?>
  <?php foreach($check->getEvent() as $event){$invoice=$event->getParent() ?>
    <tr>
      <td></td>
      <td><?php echo link_to("Inv ".$invoice->getInvno(),"invoice/view?id=".$invoice->getId())?></td>
      <td><?php echo MyDateTime::frommysql($event->getDate())->toshortdate()?></td>
      <td>P<?php echo $event->getAmount()?></td>
      <td colspan=10><?php echo $invoice->getParticularsString()?></td>
    </tr>
  <?php } ?>

<?php } ?>
</table>
