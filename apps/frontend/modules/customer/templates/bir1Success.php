<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>BIR Response 1</h1>

<?php foreach($customers as $c){?>
  <?php echo link_to($c->getName(), "customer/bir2?id=".$c->getId()); ?>   <?php echo link_to("PDF", "customer/bir2Pdf?id=".$c->getId()); ?>   <br/>
<?php } ?>
