<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');
$date=MyDate::today();

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name').' Billing Statement for '.$customer->getName());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(20, 10, 20, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================


// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
// $pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 100, '', 'PNG', '', '', false, 300, 'C', false, false, 0, false, false, false);
// $pdf->write(5,"\n\n\n\n");
$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->write(0,sfConfig::get('custom_company_header_text'),'',false,'C',true,0,false,false,0,0);
//$pdf->write(0,sfConfig::get('custom_company_address'),'',false,'C',true,0,false,false,0,0);
$pdf->write(0,sfConfig::get('custom_company_email'),'',false,'C',true,0,false,false,0,0);
$pdf->write(5,"\n");
$pdf->write(0,"Customer: ".$customer->getName(),'',false,'L',true,0,false,false,0,0);
$pdf->write(0,"Phone: ".$customer->getPhone1(),'',false,'L',true,0,false,false,0,0);
$pdf->write(0,"Date: ".MyDateTime::frommysql($date)->toshortdate(),'',false,'L',true,0,false,false,0,0);
$pdf->write(0,"Address: ".$customer->getAddress()." / ".$customer->getAddress2(),'',false,'L',true,0,false,false,0,0);

$pdf->write(5,"\n");
$pdf->write(0,"\t".$message,'',false,'L',true,0,false,false,0,0);

//====TABLE HEADER===========================

$widths=array(16,16,40,16,17,17,16,17);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl = <<<EOD
<h2 align="center">Billing Statement</h2>
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Date</b></td>
  <td width="$widths[6]" align="center"><b>Due<br>Date</b></td>
  <td width="$widths[1]" align="center"><b>Invoice No.</b></td>
  <td width="$widths[2]" align="center"><b>Item</b></td>
  <td width="$widths[3]" align="center"><b>Qty</b></td>
  <td width="$widths[4]" align="center"><b>Unit Price</b></td>
  <td width="$widths[5]" align="center"><b>Amount</b></td>
  <td width="$widths[7]" align="center"><b>Balance</b></td>
 </tr>
</thead>
EOD;

//===TABLE BODY============================
$height=1;
$grandtotal=0;
  foreach($invoices as $invoice)
  {
    $runningbalance=0;
    
    //if invoice total is 0, ignore
    if($invoice->getTotal()==0)continue;
  
    //add invoice total to grand total
    $runningbalance+=$invoice->getTotal();

    //special treatment for first invoice detail
    $first=true;
    foreach($invoice->getInvoicedetail() as $detail)
    {
      //if is discount based
      if(sfConfig::get('custom_is_discount_based'))
      {
        //WithCommission is price with commission
        //WithCommissionTotal is total with commission
        $price=MyDecimal::format($detail->getPrice());
        //if discount rate is present, say "Price<br> less 10 5 5 <br> Discounted Price"
        if($detail->getDiscrate()!="" and $detail->getQty()!=0)
          $price=$price."<br>Less ".$detail->getDiscrate()."<br>".MyDecimal::format($detail->getTotal()/$detail->getQty());
        $total=MyDecimal::format($detail->getTotal());
      }
      else
      {
        //WithCommission is price with commission
        //WithCommissionTotal is total with commission
        $price=MyDecimal::format($detail->getWithCommission());
        //if discount rate is present, say "Price<br> less 10 5 5 <br> Discounted Price"
        if($detail->getDiscrate()!="" and $detail->getQty()!=0)
          $price=$price."<br>Less ".$detail->getDiscrate()."<br>".MyDecimal::format($detail->getWithCommissionTotal()/$detail->getQty());
        $total=MyDecimal::format($detail->getWithCommissionTotal());
      }
        
      $content=array(
        $first?MyDateTime::frommysql($invoice->getDate())->toshortdate():"",
        $first?$invoice->getInvno():"",
        $detail->getProduct(),
        $detail->getQty(),
        $price,
        $total,
        $first?MyDateTime::frommysql($invoice->getDuedate())->toshortdate():"",
        "",
        );
    
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[6]" align="center">$content[6]</td>
<td width="$widths[1]" align="center">$content[1]</td>
<td width="$widths[2]" align="center">$content[2]</td>
<td width="$widths[3]" align="right">$content[3]</td>
<td width="$widths[4]" align="right">$content[4]</td>
<td width="$widths[5]" align="right">$content[5]</td>
<td width="$widths[7]" align="right">$content[7]</td>
</tr>
EOD;
    
      //no longer first
      $first=false;
    }

    //totals line    
    $content=array(
      "",
      "",
      "",
      "",
      "",
      MyDecimal::format($invoice->getTotal()),
      "Subtotal",
      MyDecimal::format($runningbalance),
      );
$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="center">$content[0]</td>
  <td width="$widths[6]" align="center"><b>$content[6]</b></td>
  <td width="$widths[1]" align="center">$content[1]</td>
  <td width="$widths[2]" align="center">$content[2]</td>
  <td width="$widths[3]" align="right">$content[3]</td>
  <td width="$widths[4]" align="right"><b>$content[4]</b></td>
  <td width="$widths[5]" align="right"><b>$content[5]</b></td>
  <td width="$widths[7]" align="right"><b>$content[7]</b></td>
 </tr>
EOD;

$content=array(
  "",
  "",
  "",
  "",
  "",
  "",
  "Payments",
  "",
  );
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[6]" align="center"><b>$content[6]</b></td>
<td width="$widths[1]" align="center">$content[1]</td>
<td width="$widths[2]" align="center">$content[2]</td>
<td width="$widths[3]" align="right">$content[3]</td>
<td width="$widths[4]" align="right"><b>$content[4]</b></td>
<td width="$widths[5]" align="right"><b>$content[5]</b></td>
<td width="$widths[7]" align="right"><b>$content[7]</b></td>
</tr>
EOD;



//original cash payment
if($invoice->getCash()!=0)
{
  $runningbalance-=$invoice->getCash();
  
    //cash payment line    
    $content=array(
      "",
      "",
      "",
      "",
      "",
      MyDecimal::format($invoice->getCash()),
      "Cash ",
      MyDecimal::format($runningbalance),
      "",
      );
$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="center">$content[0]</td>
  <td width="$widths[6]" align="center">$content[6]</td>
  <td width="$widths[1]" align="center">$content[1]</td>
  <td width="$widths[2]" align="center">$content[2]</td>
  <td width="$widths[3]" align="right">$content[3]</td>
  <td width="$widths[4]" align="right">$content[4]</td>
  <td width="$widths[5]" align="right">$content[5]</td>
  <td width="$widths[7]" align="right">$content[7]</td>
 </tr>
EOD;
}

//original cheque payment
if($invoice->getChequeamt()!=0)
{
  $runningbalance-=$invoice->getChequeamt();
  
    //cash payment line    
    $content=array(
      "",
      "",
      $invoice->getCheque()."<br>".MyDateTime::frommysql($invoice->getChequedate())->toshortdate(),
      "",
      "",
      MyDecimal::format($invoice->getChequeamt()),
      "Cheque ",
      MyDecimal::format($runningbalance),
      );
$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="center">$content[0]</td>
  <td width="$widths[6]" align="center">$content[6]</td>
  <td width="$widths[1]" align="center">$content[1]</td>
  <td width="$widths[2]" align="center">$content[2]</td>
  <td width="$widths[3]" align="right">$content[3]</td>
  <td width="$widths[4]" align="right">$content[4]</td>
  <td width="$widths[5]" align="right">$content[5]</td>
  <td width="$widths[7]" align="right">$content[7]</td>
 </tr>
EOD;
}


//events
foreach($invoice->getEvents() as $event)
{
  $type="";
  $details="";
  switch($event->getType())
  {
    case "ChequeCollect": 
      $type="Cheque";
      $chequedate="";
      if($invoice->getChequedate()!=null)
        $chequedate="<br>".MyDateTime::frommysql($invoice->getChequedate())->toshortdate();
      $details=trim($event->getDetail1().$chequedate);
      break;
    case "CashCollect": 
      $type="Cash";
      $details="";
      break;
  }
  $runningbalance-=$event->getAmount();

  $content=array(
    MyDateTime::frommysql($event->getDate())->toshortdate(),
    "",
    $details,
    "",
    "",
    MyDecimal::format($event->getAmount()),
    $type,
    MyDecimal::format($runningbalance),
    );

$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="center">$content[0]</td>
  <td width="$widths[6]" align="center">$content[6]</td>
  <td width="$widths[1]" align="center">$content[1]</td>
  <td width="$widths[2]" align="center">$content[2]</td>
  <td width="$widths[3]" align="right">$content[3]</td>
  <td width="$widths[4]" align="right">$content[4]</td>
  <td width="$widths[5]" align="right">$content[5]</td>
  <td width="$widths[7]" align="right">$content[7]</td>
 </tr>
EOD;
}


/*

  <!--events-->
  <?php ?>
    <tr>
    	<td align=right><?php echo MyDecimal::format(0-$event->getAmount())?></td>
    	<td align=right><?php echo MyDecimal::format($runningbalance)?></td>
    </tr>
  <?php }//end foreach($invoice->getEvents() ?>

*/


  //Invoice Total
  $content=array(
    "",
    "",
    "",
    "",
    "",
    "",
    "Balance",
    MyDecimal::format($runningbalance),
    );
$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="center">$content[0]</td>
  <td width="$widths[6]" align="center"><b>$content[6]</b></td>
  <td width="$widths[1]" align="center">$content[1]</td>
  <td width="$widths[2]" align="center">$content[2]</td>
  <td width="$widths[3]" align="right">$content[3]</td>
  <td width="$widths[4]" align="right"><b>$content[4]</b></td>
  <td width="$widths[5]" align="right"><b>$content[5]</b></td>
  <td width="$widths[7]" align="right"><b>$content[7]</b></td>
 </tr>
EOD;


    //spacer    
    $content=array(
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      );
$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="center">$content[0]</td>
  <td width="$widths[6]" align="center">$content[1]</td>
  <td width="$widths[1]" align="center">$content[1]</td>
  <td width="$widths[2]" align="center">$content[2]</td>
  <td width="$widths[3]" align="right">$content[3]</td>
  <td width="$widths[4]" align="right">$content[4]</td>
  <td width="$widths[5]" align="right">$content[5]</td>
 </tr>
EOD;
    
    
    $grandtotal+=$runningbalance;
    $runningbalance=0;
  }

  //Grand Total    
  $content=array(
    "",
    "PAYABLE",
    "",
    "",
    "",
    "",
    "TOTAL",
    MyDecimal::format($grandtotal),
    );
$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="center">$content[0]</td>
  <td width="$widths[6]" align="center"><b>$content[6]</b></td>
  <td width="$widths[1]" align="center"><b>$content[1]</b></td>
  <td width="$widths[2]" align="center">$content[2]</td>
  <td width="$widths[3]" align="right">$content[3]</td>
  <td width="$widths[4]" align="right"><b>$content[4]</b></td>
  <td width="$widths[5]" align="right"><b>$content[5]</b></td>
  <td width="$widths[7]" align="right"><b>$content[7]</b></td>
 </tr>
EOD;


//===TABLE END============================
$tbl .= <<<EOD
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

//===AFTER TABLE=====================
$pdf->write(0,"\n\n\tPlease prepare a cheque payable to ",'',false,'L',false,0,false,true,0,0);
$pdf->SetFont('helveticaB', '', 10, '', true);//bold font
$pdf->write(0,sfConfig::get('custom_cheques_payable_to').".",'',false,'L',true,0,false,false,0,0);
$pdf->SetFont('helvetica', '', 10, '', true);//normal font

//===DEDICATION=====================
$content=array(
sfConfig::get('custom_billing_statement_signatory_name'),
sfConfig::get('custom_billing_statement_signatory_position'),
);
$footer = <<<EOD
<br>
<br>
<br>
<table>
<tr>
<td>
<br>Respectfully yours, 
<br>
<br>___________________
<br>$content[0]
<br>$content[1]
</td>
<td>
<br>Received By:
<br>
<br>___________________
</td>
</tr>
</table>
EOD;
$pdf->writeHTML($footer, true, false, false, false, '');

//===FOOTER============================
$content=array(
sfConfig::get('custom_company_phone'),
sfConfig::get('custom_company_email'),
sfConfig::get('custom_company_address'),
);
$footer = <<<EOD
<br>
<br>
<br>Tel: $content[0]
<br>Email: $content[1]
<br>$content[2]
EOD;
$pdf->writeHTML($footer, true, false, false, false, '');

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('UnpaidSales-'.$customer->getName()."-".MyDateTime::frommysql(MyDate::today())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

