<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_notice"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<?php echo form_tag('folder/search') ?>
Search for folders: <input id="searchstring" name="searchstring" size=12><input value="Search" type="submit">
</form>

<hr>
<?php echo form_tag('file/search') ?>
Search for files: <input id="searchstring" name="searchstring" size=12><input value="Search" type="submit">
</form>

<hr>
<?php echo html_entity_decode($folder->getBreadcrumbs()); ?>
<h1>Folder: <?php echo $folder->getTitle() ?></h1>
<?php if(!$folder->isRoot())echo link_to("Edit","folder/edit?id=".$folder->getId()) ?>
| <?php echo link_to("New Subfolder","folder/new?parent_id=".$folder->getId())?>


<hr>
<h2>Subfolders</h2>
<table border=1>
<tr>
	<td>View</td>
	<td>Title</td>
	<td>Description</td>
	<td>Edit</td>
	<td>Delete</td>
</tr>
<?php foreach($folders as $folder){?>
<tr>
	<td><?php echo link_to("View","folder/view?id=".$folder->getId()) ?></td>
	<td><?php echo link_to($folder->getTitle(),"folder/view?id=".$folder->getId()) ?></td>
	<td><?php echo $folder->getDescription()?></td>
	<td><?php echo link_to("Edit","folder/edit?id=".$folder->getId()) ?></td>
	<td><?php echo link_to("Delete","folder/delete?id=".$folder->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?></td>
</tr>
<?php	} ?>
</table>

<hr>
<h2>New File</h2>
  <?php echo form_tag_for($form, '@file') ?>
    <?php echo $form->renderHiddenFields(false) ?>
<table>
<tr>
<td>Title*</td><td><?php echo $form['title'] ?></td>
</tr>
<tr>
<td valign=top>Description</td><td><?php echo $form['description'] ?></td>
</tr>
<tr>
<td valign=top>Keywords</td><td><?php echo $form['keywords'] ?></td>
</tr>
<tr>
<td>File</td><td><?php echo $form['file'] ?></td>
</tr>
<tr>
<td></td><td><input type=submit value=Save></td>
</tr>
</table>
</form>

<hr>
<h2>Files</h2>
<table border=1>
<tr>
	<td>View</td>
	<td>Download</td>
	<td>Title</td>
	<td>Description</td>
	<td>File Name</td>
	<td>File Type</td>
	<td>Delete</td>
</tr>
<?php foreach($files as $file){?>
<tr>
	<td><?php echo link_to("View","file/view?id=".$file->getId()) ?></td>
	<td><a download="<?php echo $file->getFilename()?>" href="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace(array("/index.php","/frontend_dev.php"),"",$_SERVER['SCRIPT_NAME'])?>/uploads/files/<?php echo $file->getUploadlocation()."/".$file->getFile() ?>"">Download</a></td>
	<td><?php echo link_to($file->getTitle(),"file/view?id=".$file->getId()) ?></td>
	<td><?php echo $file->getDescription()?></td>
	<td><?php echo $file->getFilename()?></td>
	<td><?php echo $file->getFiletype()?></td>
	<td><?php echo link_to("Delete","file/delete?id=".$file->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?></td>
</tr>
<?php	} ?>
</table>

