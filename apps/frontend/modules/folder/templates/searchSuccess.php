<h1>Folder Search</h1>
<?php echo link_to("New Folder","folder/new?parent_id=1")?>
<br>
<br>
<?php echo form_tag('folder/search') ?>
	<input id="searchstring" name="searchstring" size=12><input value="Search" type="submit">
</form>
<br>
<table border=1>
<tr>
	<td>Parent</td>
	<td>Title</td>
	<td>Description</td>
</tr>
<?php foreach($folders as $folder){?>
<tr>
	<td><?php if(!$folder->isRoot())echo link_to($folder->getParent(),"folder/view?id=".$folder->getParentId())?></td>
	<td><?php echo link_to($folder->getTitle(),"folder/view?id=".$folder->getId()) ?></td>
	<td><?php echo $folder->getDescription()?></td>
</tr>
<?php	} ?>
</table>

