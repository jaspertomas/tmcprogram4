<?php

require_once dirname(__FILE__).'/../lib/folderGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/folderGeneratorHelper.class.php';

/**
 * folder actions.
 *
 * @package    sf_sandbox
 * @subpackage folder
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class folderActions extends autoFolderActions
{
  public function executeNew(sfWebRequest $request)
  {
    $this->folder = new Folder();

    $this->folder->setParentId($request->getParameter("parent_id"));

    //validation: if parent folder doesn't exist, make parent folder root folder
    if($this->folder->getParent()==null)
    {
      $this->getUser()->setFlash('error', 'Folder not found');
      return $this->redirect($request->getReferer());
    }

    $this->form = new FolderForm($this->folder);
  }
  public function executeFiles(sfWebRequest $request)
  {
//    $this->folder = $this->getRoute()->getObject();
//    $this->form = $this->configuration->getForm($this->folder);
    $this->redirect('folder/view?id='.$request->getParameter('id'));
  }
  public function executeView(sfWebRequest $request)
  {
    $this->folder=MyModel::fetchOne("Folder",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('folder');
    $this->file->setParentId($this->folder->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="folder"')
      ->andWhere('f.parent_id='.$this->folder->getId())
      ->execute();
      
    $this->child_folder=new Folder();
    $this->child_folder->setParentId($this->folder->getId());
    $this->folder_form=new FolderForm($this->child_folder);

    $this->folders=$this->folder->getChildren();
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter("folder");
    if($requestparams['id']==1)
    {
      $this->getUser()->setFlash('error', 'Cannot edit root folder');
      return $this->redirect($request->getReferer());
    }
  
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $folder = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $folder)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@folder_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect('folder/files?id='.$folder->getId());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeSearch(sfWebRequest $request)
  {
    if(trim($request->getParameter("searchstring"))=="")
    {
      $this->folders=array();
      return;
    }
  
    $keywords=explode(" ",$request->getParameter("searchstring"));

    $query=Doctrine_Query::create()
        ->from('Folder f')
        ->orderBy("f.title")
      	;

    foreach($keywords as $keyword)
    {
    	$query->orWhere("f.title LIKE '%".$keyword."%'");
    	$query->orWhere("f.description LIKE '%".$keyword."%'");
    	$query->orWhere("f.keywords LIKE '%".$keyword."%'");
    }
  	$this->folders=$query->execute();

    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $folder=$this->getRoute()->getObject();

    if($folder->getParentId()==null)
    {
      $this->getUser()->setFlash('error', 'Cannot delete root folder');
      return $this->redirect($request->getReferer());
    }
  
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($folder->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('folder/view?id='.$folder->getParentId());
  }
}
