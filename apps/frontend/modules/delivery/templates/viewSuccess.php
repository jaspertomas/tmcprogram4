<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_notice"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<?php slot('transaction_id', $delivery->getId()) ?>
<?php slot('transaction_type', "Delivery") ?>
<?php use_helper('I18N', 'Date') ?>
<h1><?php 
switch($delivery->getType())
{
case "Outgoing": echo "Sales";break;
case "Incoming": echo "Purchase";break;
default: break;
}
?> Product Conversion: <?php echo $delivery->getCode()?></h1>

<table width=100%>
  <tr>
    <td>

      <table>
<?php if($delivery->getRefClass()=="purchase" or $delivery->getRefClass()=="invoice"){?>
        <tr>
          <td>Code:</td>
    <td>
      <?php echo form_tag("delivery/setCode");?>
      <?php echo $delivery->getCode() ?>
      <input type=hidden id=id name=id value=<?php echo $delivery->getId()?>>
      <input id=value size=10 name=value>
      </form>
    </td>
        </tr>
<?php } ?>
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($delivery->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo form_tag_for($form,'delivery/adjust')?> <?php echo $form['id'] ?>
            <?php echo $form['date'] ?>
            <input type=submit value=Save>
            <?php }?>	
          </td>
        </tr>
        <!--tr>
          <td>Type:</td>
          <td>
            <?php //echo $delivery->getType()?>
          </td>
        </tr-->

      <?php $ref=$delivery->getRef();if($ref){?>
        <tr>
          <td><?php echo ucwords($delivery->getRefClass())?>:</td>
          <td><?php echo link_to($ref,$delivery->getRefClass()."/view?id=".$delivery->getRefId())?></td>
        </tr>
      <?php } ?>

      <?php $client=$delivery->getClient();if($client){?>
        <tr>
          <td><?php echo ucwords($delivery->getClientClass())?>:</td>
          <td><?php echo link_to($client,$delivery->getClientClass()."/view?id=".$delivery->getClientId())?> <?php echo link_to("Edit",$delivery->getClientClass()."/edit?id=".$delivery->getClientId())?></td>
        </tr>
      <?php } ?>

        <tr>
          <td>Total:</td>
          <td><?php echo MyDecimal::format($delivery->getTotal()) ?></td>
        </tr>
      </table>
    
    </td>
    <td>

      <table>
<?php if($delivery->getRefClass()=="purchase" or $delivery->getRefClass()=="invoice"){?>
        <tr>
          <td>Total:</td>
          <td>
            <?php echo MyDecimal::format($delivery->getTotal())?>
          </td>
        </tr>
<?php } ?>
        <tr>
          <td>Warehouse:</td>
          <td><?php echo $delivery->getWarehouse()?></td>
        </tr>
        <!--tr>
          <td>Related DRs:</td>
          <td>
            <?php //foreach($delivery->getRelatedDeliverys() as $ref){?>
              <?php //echo link_to($ref,"delivery/view?id=".$ref->getId())?>&nbsp;
            <?php //} ?>
          </td>
        </tr-->
<!--
<?php // if($delivery->getRefClass()=="purchase" or $delivery->getRefClass()=="invoice"){?>
        <tr>
          <td>Payment Status:</td>
          <td>
            <font color=<?php // echo $delivery->getPaymentStatusColor()?>><?php // echo $delivery->getPaymentStatus()?></font>
            <?php // foreach(array("Pending","Paid","Cancelled") as $value)if($delivery->getPaymentStatus()!=$value)echo link_to("$value","delivery/setPaymentStatus?id=".$delivery->getId()."&submit=$value", array("class"=>"button","confirm" => "$value\n\nAre you sure?")) ?> 
          </td>
        </tr>
<?php // }else{ ?>
        <tr>
          <td>Status:</td>
          <td>
            <font color=<?php // echo $delivery->getStatusColor()?>><?php // echo $delivery->getStatus()?></font>
            <?php // foreach(array("Pending","Complete","Cancelled") as $value)if($delivery->getStatus()!=$value)echo link_to("$value","delivery/setStatus?id=".$delivery->getId()."&submit=$value", array("class"=>"button","confirm" => "$value\n\nAre you sure?")) ?> 
          </td>
        </tr>
<?php // } ?>
-->
<?php if($delivery->getType()!="Conversion"){?>
        <tr>
          <td>Method:</td>
          <td>
            <?php echo form_tag("delivery/setDeliveryMethod");?>
            <?php echo $delivery->getDeliveryMethod()?>
            <input type=hidden id=id name=id value=<?php echo $delivery->getId()?>>
            <input type=submit id=submit name=submit value="<?php echo $delivery->getDeliveryMethod()=="Pickup"?"Delivery":"Pickup"?>" class=button>
            </form>
          </td>
        </tr>
<?php } ?>

      </table>
    
    </td>
    <td>
      <table>
        <td colspan=3>
          Notes: <br><?php echo $delivery->getNotes() ?>
          <?php echo form_tag("delivery/setNotes");?>

          <input type=hidden id=id name=id value=<?php echo $delivery->getId()?>>
          <textarea id=value rows=2 cols=20 name=value><?php echo $delivery->getNotes() ?>
          </textarea>
          <input type=submit value=Save>
          </form>
        </td>
      </table>
    </td>
  </tr>
</table>



<?php echo link_to("Edit","delivery/edit?id=".$delivery->getId()) ?> |
<?php echo link_to('Delete','delivery/delete?id='.$delivery->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
<?php echo link_to("Multi Product Input Wizard","delivery/multiInputWizard?id=".$delivery->getId()) ?> |
<?php echo link_to("Print","delivery/viewPdf?id=".$delivery->getId()) ?> |
<hr>
<?php if($delivery->getType()=="Conversion"){ //-------------------------------------------?>
<b>Search conversion type:</b> 
<input id=deliveryconversionsearchinput autocomplete="off" value="<?php echo $searchstring ?>">  
<input type=button value="Clear" id=deliveryclearsearch> <span id=pleasewait></span>
<?php echo link_to("(View list of conversion types)","delivery_conversion/index")?><br>

<?php echo form_tag_for($deliveryconversionform,"@deliveryconversion",array("id"=>"new_delivery_detail_form")); ?>
<input type=hidden name=deliveryconversion[parent_class] value="Delivery">
<input type=hidden name=deliveryconversion[parent_id] value=<?php echo $delivery->getId()?>>
    <?php echo $deliveryconversionform->renderHiddenFields(false) ?>

<br>
<table>
	<tr>
		<td>Conversion</td>
		<td>Qty</td>
		<td>Notes</td>
	</tr>

	<tr>
		<td>
      <input hidden=true name=deliveryconversion[conversion_id] id=deliveryconversion_conversion_id value=<?php echo $deliveryconversionform->getObject()->getConversionId()?>>
		  <div id=conversionname>
			<?php if($deliveryconversionform->getObject()->getConversionId())echo $deliveryconversionform->getObject()->getConversion(); else echo "No item selected";?>
			</div>
		</td>
		<td><input size="5" name="deliveryconversion[qty]" value="1" id="deliveryconversion_qty" type="text" <?php if(!$conversion_is_set)echo "disabled=true"?>></td>
		<td><input name="deliveryconversion[description]" id="deliveryconversion_description" type="text" <?php if(!$conversion_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=delivery_detail_submit value=Save  <?php if(!$conversion_is_set)echo "disabled=true"?> ></form>
</td>
	</tr>

	<input type=hidden id="conversion_name" value="<?php echo $deliveryconversionform->getObject()->getConversion()->getName();?>">
</table>

<?php }else{ //-------------------------------------------------------------------?>
<b>Search product:</b> <input id=deliveryproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=deliveryclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@deliverydetail",array("id"=>"new_delivery_detail_form")); ?>
<input type=hidden name=deliverydetail[parent_id] value=<?php echo $delivery->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Product</td>
		<td>Qty</td>
		<td>Price</td>
		<td>Discount Rate</td>
		<td>Notes</td>
	</tr>

	<tr>
		<td>
      <input hidden=true name=deliverydetail[product_id] id=deliverydetail_product_id value=<?php echo $detailform->getObject()->getProductId()?>>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<td><input size="5" name="deliverydetail[qty]" value="1" id="deliverydetail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td>
		  <?php if($product){?>
		    <?php echo $product->getMaxsellprice()?>
  		  <input name="deliverydetail[price]" value="<?php echo $product->getMaxsellprice()?>" id="deliverydetail_price" type="hidden" >
		  <?php }else{?>
		    0.00
		  <?php }?>
		
		</td>
		<td><input size="5" name="deliverydetail[discrate]" value="<?php echo $client->getDiscrate()?>" id="deliverydetail_discrate" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input name="deliverydetail[description]" id="deliverydetail_description" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=delivery_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> ></form>
</td>
	</tr>

	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>
<?php }  //-------------------------------------------------------------------?>
<div id="deliverysearchresult"></div>

<?php if($delivery->getType()=="Conversion"){?>

<hr>
<h2>Conversions</h2>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Product</td>
    <td>Notes</td>
    <td></td>
    <td></td>
  </tr>
  <?php 
  	foreach($delivery->getDeliveryconversion() as $detail){?>
  <tr>
    <td>
      <?php echo form_tag("deliveryconversion/setQty");?>
      <?php echo $detail->getQty() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value>
      </form>
    </td>
    <td>
      <?php echo link_to($detail->getConversion(),"conversion/view?id=".$detail->getConversionId()) ?>
    </td>
    <td>
      <?php echo form_tag("deliveryconversion/setDescription");?>
      <?php echo $detail->getDescription() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=10 name=value>
      </form>
    </td>
    <?php //if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
      <td>
        <?php echo link_to('Delete','deliveryconversion/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
      </td>
    <?php //}?>
  </tr>
  <?php }?>
</table>
<?php } ?>


<hr>
<h2>Products</h2>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Product</td>
    <td>Price</td>
    <td>Disc Rate</td>
    <td>Disc Price</td>
    <td>Total</td>
    <td>Notes</td>
    <td></td>
    <td></td>
  </tr>
  <?php 
  	foreach($delivery->getDeliverydetail() as $detail){?>
  <tr>
    <td>
      <?php if($delivery->getType()!="Conversion")echo form_tag("deliverydetail/setQty");?>
      <?php echo $detail->getQty() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value value="<?php echo $detail->getQty() ?>">
      </form>
    </td>
    <td>
      <?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?>
    </td>
    <td align=right>
      <?php echo $detail->getPrice() ?>
    </td>
    <td>
      <?php //if($delivery->getType()!="Conversion")echo form_tag("deliverydetail/setDiscrate");?>
      <?php echo $detail->getDiscrate() ?>
      <!--
      <input type=hidden id=id name=id value=<?php //echo $detail->getId()?>>
      <input id=value size=3 name=value value="<?php //echo $detail->getDiscrate() ?>">
      </form>
      -->
    </td>
    <td align=right>
      <?php echo MyDecimal::format($detail->getDiscprice()) ?>
    </td>
    <td align=right>
      <?php echo MyDecimal::format($detail->getTotal()) ?>
    </td>
    <td>
      <?php echo form_tag("deliverydetail/setDescription");?>
      <?php echo $detail->getDescription() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=10 name=value>
      </form>
    </td>
    <?php if($delivery->getType()!="Conversion"){?>
      <td>
        <?php echo link_to('Delete','deliverydetail/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
      </td>
    <?php }?>
  </tr>
  <?php }?>
</table>

<hr>
<?php if($delivery->getType()!="Conversion"){?>
<h2>Possible Conversions</h2>
<?php echo link_to("Recalculate","delivery/recalcConversions?id=".$delivery->getId())?>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Conversion</td>
    <td>Convert</td>
    <td></td>
  </tr>
  <?php 
  	foreach($delivery->getDeliveryconversion() as $dc){?>
  <tr>
    <td><?php echo $dc->getPossibleConversionQty($delivery->getDeliverydetail())?></td>
    <td><?php echo $dc->getConversion()?></td>
    <td><?php echo link_to("Convert","delivery/genConversionDr?deliveryconversion_id=".$dc->getId(), array('confirm' => 'This will generate a conversion DR. Are you sure?')) ?></td>
  </tr>
  <?php }?>
</table>

<?php } ?>





<script type="text/javascript">
function changeText(id)
{
var x=document.getElementById("mySelect");
x.value=id;
}

//on qty get focus, select its contents
$("#deliverydetail_qty").focus(function() { $(this).select(); } );
$("#deliveryconversion_qty").focus(function() { $(this).select(); } );
//on product search get focus, select its contents
$("#deliveryproductsearchinput").focus(function() { $(this).select(); } );
//on conversion search get focus, select its contents
$("#deliveryconversionsearchinput").focus(function() { $(this).select(); } );

//give focus to these if they exist and are enabled
$("#deliveryproductsearchinput").focus();
$("#deliveryconversionsearchinput").focus();
$("#deliverydetail_qty").focus();
$("#deliveryconversion_qty").focus();

//if no product id set, disable save button
if($("#deliverydetail_product_id").val()=='')	 		  
  $("#delivery_detail_submit").prop("disabled",true);
//------Delivery Discount-------------------
//on page ready, var discounted is false
var discounted=false;
//set checkbox to false as default
$("#chk_is_discounted").prop('checked',false);
//hide all password entry boxes
$(".password_tr").attr('hidden',true);

//------Delivery (not header) product search-----------
$("#deliveryproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#delivery_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#deliveryproductsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#deliveryproductsearchinput").val();
      //if searchstring is all numbers except last letter, 
      //and length is 8
      //this is a barcode. 
      if(
        $.isNumeric(searchstring.slice(0, -1))
        &&
        searchstring.length>=8
        )
      {
        processBarcode(searchstring);
      }
      //else do ajax to product search, display result in deliverysearchresult
      else
      {
	      $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+searchstring+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	   		  $("#deliverysearchresult").html(result);
	      }});
      }
    }
    //else clear
    //else
 		//  $("#deliverysearchresult").html("");
});
$("#deliveryclearsearch").click(function(){
 		  $("#deliverysearchresult").html("");
});
//---------New Delivery Detail Submit-----------------------------
//on submit new delivery detail form
$("#new_delivery_detail_form").submit(function(event){

  //this is when the cursor is on qty and the barcode reader is used
  //determine if qty is actually a barcode
  if(
    //if qty is too long to be a quantity, this is probably a barcode
    $("#deliverydetail_qty").val().length>=8 && 
    //but only if qty is enabled
    $("#deliverydetail_qty").attr("disabled")!="disabled")
  {
    processBarcode($("#deliverydetail_qty").val());
    return false;
  } 

  //if discount checkbox is checked, go ahead with submit
  if($("#chk_is_discounted").prop('checked'))return true;

  //determine minimum product selling price
  var price=$("#deliverydetail_price").prop('value');
  var minprice=$("#product_min_price").val();
  if(minprice==0)minprice=$("#product_price").val();
  //if price is less than min price, complain and don't submit
  if(parseFloat(price)<parseFloat(minprice))
  {
    alert("Minimum selling price is "+minprice+".\n\nPlease check Discounted check box to request for discount, and notify manager. ");
    return false;
  }
});
//------ CONVERSION --------------------------
$("#deliveryconversionsearchinput").on('keyup', function(event) {
    //search contents have been edited. disable save button
    $("#delivery_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#deliveryconversionsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#deliveryconversionsearchinput").val();
      var url="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/conversion/search?searchstring="+searchstring+"&transaction_id=<?php include_slot('transaction_id') ?>";
      $.ajax({url: url, success: function(result){
   		  $("#deliverysearchresult").html(result);
      }});
    }
});
$("#deliveryclearsearch").click(function(){
 		  $("#deliverysearchresult").html("");
});

//------ BARCODE -----------------------------
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
function processBarcode(barcode)
{
        //
        if(barcode.length<8)
        {
          barcode=pad(barcode, 8);
        }

		$("#pleasewait").html("<font color=red><b>PLEASE WAIT</b></font>");
	      $.ajax
	      ({
	        url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/delivery/barcodeEntry?barcode="+barcode, 
	        dataType: "json",
	        success: function(result)
	        {
			$("#pleasewait").html("");
	          //if product found
	          if(result[0]!=0)
	          {
              //invoidetail input controls disabled=false
              $("#chk_is_discounted").removeAttr('disabled');
              $("#deliverydetail_description").removeAttr('disabled');
              $("#delivery_detail_submit").removeAttr('disabled');
              $("#deliverydetail_qty").removeAttr('disabled');
              $("#deliverydetail_price").removeAttr('disabled');
              $("#deliverydetail_with_commission").removeAttr('disabled');

	            //populate values
	            $("#deliveryproductsearchinput").val("");
              $("#deliverydetail_qty").val(1);
              $("#deliverydetail_product_id").val(result[0]);//product_id
              $("#productname").html(result[1]);//product name
              $("#deliverydetail_price").val(result[2]);//maxsellprice
              $("#deliverydetail_with_commission").val(0);
              $("#chk_is_discounted").attr("checked",false);
              $("#deliverydetail_description").val("");
              
              $("#product_min_price").val(result[3]);//minsellprice
              $("#product_price").val(result[2]);//maxsellprice
              $("#product_name").val(result[1]);//product name
              $("#deliverydetail_barcode").val(barcode);
              
              //set focus to qty
              $("#deliverydetail_qty").focus();
	          }
	          else
	          {
	            alert("Product not found");
              $("#deliverydetail_qty").val("");
	          }
	        }
	      });
}
</script>
