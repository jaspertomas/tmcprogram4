<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
  <?php echo form_tag_for($form, '@delivery') ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

<div class="sf_admin_form_row sf_admin_text">
  <div>
    <label>Warehouse</label>
    <div class="content"><?php echo $form->getObject()->getWarehouse()?>&nbsp;</div>
  </div>
</div>
<div class="sf_admin_form_row sf_admin_text">
  <div>
    <label>Type</label>
    <div class="content"><?php echo $form->getObject()->getType()?>&nbsp;</div>
  </div>
</div>
<?php if($form->getObject()->getType()!="Conversion"){?>
<div class="sf_admin_form_row sf_admin_text">
  <div>
    <label><?php echo $form->getObject()->getType()=="Incoming"?"From":"To"?></label>
    <div class="content"><?php echo $form->getObject()->getClient()?>&nbsp;</div>
  </div>
</div>
<?php } ?>
<div class="sf_admin_form_row sf_admin_text">
  <div>
    <label>Status</label>
    <div class="content"><?php echo $form->getObject()->getStatus()?>&nbsp;</div>
  </div>
</div>


    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
      <?php include_partial('delivery/form_fieldset', array('delivery' => $delivery, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
    <?php endforeach; ?>

    <?php include_partial('delivery/form_actions', array('delivery' => $delivery, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </form>
</div>
