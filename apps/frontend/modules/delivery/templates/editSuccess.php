<?php use_helper('I18N', 'Date') ?>
<?php include_partial('delivery/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit Delivery Receipt', array(), 'messages') ?></h1>

  <?php include_partial('delivery/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('delivery/form_header', array('delivery' => $delivery, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('delivery/form', array('delivery' => $delivery, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('delivery/form_footer', array('delivery' => $delivery, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
