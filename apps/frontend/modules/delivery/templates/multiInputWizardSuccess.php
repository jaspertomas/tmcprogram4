<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_notice"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<?php slot('transaction_id', $delivery->getId()) ?>
<?php slot('transaction_type', "DeliveryMultiInput") ?>
<?php use_helper('I18N', 'Date') ?>
<h1>Multi Input Wizard: Delivery Receipt <?php echo $delivery->getCode()?> (<?php echo $delivery->getType()?>)</h1>

<?php echo link_to("Back to DR","delivery/view?id=".$delivery->getId()) ?> |
<hr>
<b>Search product set:</b> 
<input id=deliveryconversionsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				
<input type=button value="Clear" id=deliveryclearsearch> <span id=pleasewait></span>



<div id="deliverysearchresult"></div>

<?php if($delivery->getType()=="Conversion"){?>

<hr>
<h2>Conversions</h2>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Product</td>
    <td>Notes</td>
    <td></td>
    <td></td>
  </tr>
  <?php 
  	foreach($delivery->getDeliveryconversion() as $detail){?>
  <tr>
    <td>
      <?php echo form_tag("deliveryconversion/setQty");?>
      <?php echo $detail->getQty() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value>
      </form>
    </td>
    <td>
      <?php echo link_to($detail->getConversion(),"conversion/view?id=".$detail->getConversionId()) ?>
    </td>
    <td>
      <?php echo form_tag("deliveryconversion/setDescription");?>
      <?php echo $detail->getDescription() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=10 name=value>
      </form>
    </td>
    <?php //if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
      <td>
        <?php echo link_to('Delete','deliveryconversion/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
      </td>
    <?php //}?>
  </tr>
  <?php }?>
</table>
<?php } ?>



<?php if(isset($conversion)){?>
<hr>
<?php echo form_tag("delivery/processMultiInputWizard")?>
<input type=hidden id=id name=id value=<?php echo $delivery->getId()?>>
<input type=hidden id=conversion_id name=conversion_id value=<?php echo $conversion->getId()?>>
<h2>Products</h2>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Product</td>
    <td></td>
    <td></td>
  </tr>
  <?php 
  	foreach($conversiondetails as $detail){?>
  <tr>
    <td>
      <input id=qty size=3 name=qty[<?php echo $detail->getProductId()?>]>
    </td>
    <td>
      <?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?>
    </td>
  </tr>
  <?php }?>
</table>
<input type=submit value=Save>
</form>
<?php } ?>





<script type="text/javascript">
//on conversion search get focus, select its contents
$("#deliveryconversionsearchinput").focus(function() { $(this).select(); } );

//give focus to these if they exist and are enabled
$("#deliveryconversionsearchinput").focus();

//------ CONVERSION --------------------------
$("#deliveryconversionsearchinput").on('keyup', function(event) {
    //search contents have been edited. disable save button
    $("#delivery_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#deliveryconversionsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#deliveryconversionsearchinput").val();
      var url="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/conversion/search?searchstring="+searchstring+"&transaction_id=<?php include_slot('transaction_id') ?>"+"&transaction_type=<?php include_slot('transaction_type') ?>";
      $.ajax({url: url, success: function(result){
   		  $("#deliverysearchresult").html(result);
      }});
    }
});
$("#deliveryclearsearch").click(function(){
 		  $("#deliverysearchresult").html("");
});

</script>
