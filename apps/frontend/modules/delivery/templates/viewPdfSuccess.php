<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');
$date=MyDate::today();

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name').' Billing Statement for '.$delivery->getName());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(20, 10, 20, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================


// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
// $pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 100, '', 'PNG', '', '', false, 300, 'C', false, false, 0, false, false, false);
// $pdf->write(5,"\n\n\n\n");
$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->write(0,sfConfig::get('custom_company_header_text'),'',false,'C',true,0,false,false,0,0);
//$pdf->write(0,sfConfig::get('custom_company_address'),'',false,'C',true,0,false,false,0,0);
$pdf->write(0,sfConfig::get('custom_company_email'),'',false,'C',true,0,false,false,0,0);
$pdf->write(5,"\n");

$tbl = "";

//-------------------
$client=$delivery->getClient();
$ref=$delivery->getRef();

$content=array(
  $delivery->getName(),
  MyDateTime::frommysql($date)->toshortdate(),
  $client->getName(),
  $delivery->getPono(),
  $client->getPhone1(),
  $client->getEmail(),
  
  
  );
  
  

$tbl .= <<<EOD
<table>
 <tr>
  <td align="left">Delivery No.: $content[0]</td>
  <td align="right">Date: $content[1]</td>
 </tr>
 <tr>
  <td align="left">Client: $content[2]</td>
  <td align="right">Client PO No.: $content[3]</td>
 </tr>
 <tr>
  <td align="left">Contact No.: $content[4]</td>
 </tr>
 <tr>
  <td align="left">Email: $content[5]</td>
 </tr>
</table>
EOD;




//$pdf->write(5,"\n");
//$pdf->write(0,"\t".$message,'',false,'L',true,0,false,false,0,0);

//====TABLE HEADER===========================

$widths=array(10,50,20,20,20,20);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl .= <<<EOD
<hr>
<h2>Delivery Receipt</h2>
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Qty</b></td>
  <td width="$widths[1]" align="center"><b>Product</b></td>
  <td width="$widths[2]" align="center"><b>List Price</b></td>
  <td width="$widths[3]" align="center"><b>Disc</b></td>
  <td width="$widths[4]" align="center"><b>Discounted\nPrice</b></td>
  <td width="$widths[5]" align="center"><b>Total</b></td>
 </tr>
</thead>
EOD;

//===TABLE BODY============================
$height=1;
  $grandtotal=0;
	foreach($delivery->getDeliverydetail() as $detail)
  {
    $content=array(
      $detail->getQty(),
      $detail->getProduct(),
      MyDecimal::format($detail->getPrice()),
      $detail->getDiscrate(),
      MyDecimal::format($detail->getDiscprice()),
      MyDecimal::format($detail->getTotal()),
      );
  
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center">$content[0]</td>
    <td width="$widths[1]" >$content[1]</td>
    <td width="$widths[2]" align="right">$content[2]</td>
    <td width="$widths[3]" align="right">$content[3]</td>
    <td width="$widths[4]" align="right">$content[4]</td>
    <td width="$widths[5]" align="right">$content[5]</td>
  </tr>
EOD;
  }    
$tbl .= <<<EOD
</table>

<h4>Warranty</h4>
<ul>
<li>3 Months warranty against workmanship and weld
<li>Parts are not included in the warranty
<li>There is no warranty against Pressure above the recommended (40psi)
<li>There is no warranty against Vacuum
</ul>

<h4>Payment Scheme:</h4>
<ul>
<li>Payable within 90 days upon delivery of product
<li>Payee: TRADEWIND VISAYAS CORPORATION
</ul>
EOD;


$tbl .= <<<EOD
<table>
 <tr>
  <td align="center">Prepared By:</td>
  <td align="center">Warehouse:</td>
  <td align="center">Customer Confirmation:</td>
 </tr>
 <tr>
  <td align="center"><img src="images/atansign.png" width="40"><br>Jonathan Lester Tomas<br>Sales Manager</td>
  <td align="center"><br><br><br><br><br>Warehouse Man</td>
  <td align="center"></td>
 </tr>
</table>
<br>
<br><b>Tel: 09328504712</b>
<br>Email: tmc.visayas@gmail.com
<br>Door # 2 RTC Bldg, Maharlika Highway, Brgy 110 Utap, Tacloban City



EOD;


$pdf->writeHTML($tbl, true, false, false, false, '');

//-----------------


//--------------------------------------------------

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output($delivery->getCode().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

