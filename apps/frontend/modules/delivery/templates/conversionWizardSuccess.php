<h1>Product Conversion Wizard</h1>

<table>
  <tr>
    <td>
      <?php echo form_tag("delivery/conversionWizard");?>
      Conversion Type: <?php
      $w=new sfWidgetFormDoctrineChoice(array(
        'model'     => 'Conversion',
        'add_empty' => true,
      ));
      echo $w->render('conversion_id',($conversion!=null?$conversion->getId():null));
      ?>
    </td>
    <td><input type=submit value="View"></td>
  </tr>
</table>

</form>

<?php echo link_to("New Product Conversion Type","conversion/new")?>

<?php
if($conversion==null)return;
?>

<hr>

<h2>Conversion Type: <?php echo $conversion?></h2>
<?php echo link_to("Edit","conversion/view?id=".$conversion->getId())?>

<h2>Possible Conversions: <?php echo $minqty?></h2>

<?php echo form_tag("delivery/processConversionWizard", array('onsubmit'=>"return confirm('Are you sure?');"));?>
Convert Qty: <input name=qty id=qty size=2>
<input type=hidden value=<?php echo $conversion->getId()?> name=conversion_id id=conversion_id >
<input type=hidden value=<?php echo $minqty?> name=minqty id=minqty >
Ignore Qty Limit: <input type=checkbox name=ignorelimit id=ignorelimit >
<input type=submit value="Convert">
</form>

<?php foreach(array("From"=>"from","To"=>"to") as $key=>$value){?>
<h2><?php echo $key?> Products:</h2>
<table border=1>
  <tr>
    <td>Stock on Hand</td>
    <td>Product</td>
  </tr>
  <?php foreach($conversion->getConversiondetail() as $detail)if($detail->getType()=="$value"){?>
  <tr>
    <td>
      <?php if(isset($stockarray[$detail->getProductId()])){?>
        <?php $stock=$stockarray[$detail->getProductId()]; if($stock)echo link_to($stock->getCurrentQty(),"product/inventory?id=".$detail->getProductId());?>
      <?php }else{ ?>
        0.00
      <?php } ?>
    </td>
    <td>
      <?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?>
    </td>
  </tr>
  <?php }?>
  
</table>
<?php }?>


