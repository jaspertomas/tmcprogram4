<?php

require_once dirname(__FILE__).'/../lib/deliveryGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/deliveryGeneratorHelper.class.php';

/**
 * delivery actions.
 *
 * @package    sf_sandbox
 * @subpackage delivery
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class deliveryActions extends autoDeliveryActions
{
  public function executeDashboard(sfWebRequest $request)
  {
  }
  public function executeView(sfWebRequest $request)
  {
    $this->delivery= $this->getRoute()->getObject();
    $this->product=null;

    if($this->delivery->getType()=="Conversion")
    {
      //allow set conversion id by url
      $deliveryconversion=new Deliveryconversion();
      $deliveryconversion->setQty(1);
      $this->conversion_is_set=false;
      if($request->hasParameter("conversion_id"))
      {
        $deliveryconversion->setConversionId($request->getParameter("conversion_id"));
        $this->conversion_is_set=true;
      }
      $this->deliveryconversionform = new DeliveryconversionForm($deliveryconversion);
    }
    else
    {
      //allow set product id by url
      $detail=new Deliverydetail();
      $detail->setQty(1);
      $this->product_is_set=false;
      if($request->hasParameter("product_id"))
      {
        $detail->setProductId($request->getParameter("product_id"));
        $this->product=$detail->getProduct();
        $this->product_is_set=true;
      }
      $this->detailform = new DeliverydetailForm($detail);
    }

    $this->form=new DeliveryForm($this->delivery);
    $this->searchstring=$this->request->getParameter('searchstring');
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);
    $this->message=$request->getParameter("message");

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeNew(sfWebRequest $request)
  {
    /*
    $this->delivery->setClientClass($request->getParameter("client_class"));
    $this->delivery->setClientId($request->getParameter("client_id"));
    $this->delivery->setRefClass($request->getParameter("ref_class"));
    $this->delivery->setRefId($request->getParameter("ref_id"));
    $this->delivery->setType($request->getParameter("type"));
    */
    
    /*
    parameters (choose one):
    customer_name
    vendor_name
    customer_id
    vendor_id
    invoice_id
    purchase_id
    //warehouse_id, type (warehouse, Incoming/Outgoing)
    no parameters (Conversion)
    */
    
    //if customer_name is provided, generate new customer and use that as client
    if($request->hasParameter("customer_name"))
    {
      $customer= Doctrine_Query::create()
        ->from('Customer c')
        ->where('c.name = "'.$request->getParameter("customer_name").'"')
        ->fetchOne();
      if($customer==null)
      {
        $customer=new Customer();
        $customer->setName($request->getParameter("customer_name"));
        $customer->save();
      }
      $this->delivery = DeliveryTable::createDefault("Outgoing");
      $this->delivery->setClientClass("customer");
      $this->delivery->setClientId($customer->getId());
    }
    else
    //if vendor_name is provided, generate new vendor and use that as client
    if($request->hasParameter("vendor_name"))
    {
      $vendor= Doctrine_Query::create()
        ->from('Vendor c')
        ->where('c.name = "'.$request->getParameter("vendor_name").'"')
        ->fetchOne();
      if($vendor==null)
      {
        $vendor=new Vendor();
        $vendor->setName($request->getParameter("vendor_name"));
        $vendor->save();
      }
      $this->delivery = DeliveryTable::createDefault("Incoming");
      $this->delivery->setClientClass("vendor");
      $this->delivery->setClientId($vendor->getId());
    }
    else    
    //if customer_id is provided
    if($request->hasParameter("customer_id"))
    {
      $customer= Doctrine_Query::create()
        ->from('Customer c')
        ->where('c.id = '.$request->getParameter("customer_id"))
        ->fetchOne();
      $this->delivery = DeliveryTable::createDefault("Outgoing");
      $this->delivery->setClientClass("customer");
      $this->delivery->setClientId($customer->getId());
    }
    else
    //if vendor_id is provided
    if($request->hasParameter("vendor_id"))
    {
      $vendor= Doctrine_Query::create()
        ->from('Vendor c')
        ->where('c.id = "'.$request->getParameter("vendor_id").'"')
        ->fetchOne();
      $this->delivery = DeliveryTable::createDefault("Incoming");
      $this->delivery->setClientClass("vendor");
      $this->delivery->setClientId($vendor->getId());
    }
    else
    //if invoice_id is provided
    if($request->hasParameter("invoice_id"))
    {
      $invoice= Doctrine_Query::create()
        ->from('Invoice c')
        ->where('c.id = "'.$request->getParameter("invoice_id").'"')
        ->fetchOne();
      $this->delivery = DeliveryTable::createDefault("Outgoing");
      $this->delivery->setRefClass("invoice");
      $this->delivery->setRefId($invoice->getId());
      $this->delivery->setClientClass("customer");
      $this->delivery->setClientId($invoice->getCustomerId());
      $this->delivery->setInvno($invoice->getInvno());

      //if invoice, 
      //never create stock entries for these
      $this->delivery->setStatus("Pending");
    }
    else
    //if purchase_id is provided
    if($request->hasParameter("purchase_id"))
    {
      $purchase= Doctrine_Query::create()
        ->from('Purchase c')
        ->where('c.id = "'.$request->getParameter("purchase_id").'"')
        ->fetchOne();
      $this->delivery = DeliveryTable::createDefault("Incoming");
      $this->delivery->setRefClass("purchase");
      $this->delivery->setRefId($purchase->getId());
      $this->delivery->setClientClass("vendor");
      $this->delivery->setClientId($purchase->getVendorId());
      $this->delivery->setPono($purchase->getPono());

      //if purchase, 
      //this is just for remembering contents and totals of interoffice requisition slips 
      //never create stock entries for these
      $this->delivery->setStatus("Pending");
    }
    else
    /*
    //if warehouse_id is provided
    if($request->hasParameter("warehouse_id"))
    {
      $warehouse= Doctrine_Query::create()
        ->from('Warehouse c')
        ->where('c.id = "'.$request->getParameter("warehouse_id").'"')
        ->fetchOne();
      $this->delivery = DeliveryTable::createDefault("Transfer");
      $this->delivery->setClientClass("warehouse");
      $this->delivery->setClientId($warehouse->getId());
    }
    else
    */
    //default: conversion
    {
      $this->delivery = DeliveryTable::createDefault("Conversion");
    }
    $this->delivery->save();

    $this->form = new DeliveryForm($this->delivery);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        //set date and adjust stock entry date if necessary
        //only if delivery is not new (action is edit)
        if(!$isnew)$form->getObject()->setDateAndUpdateStockEntry($requestparams["date"]["year"]."-".$requestparams["date"]["month"]."-".$requestparams["date"]["day"]);

        $delivery = $form->save();
        
        //update purchase receive date
        if($delivery->getRefClass()=='purchase')
        {
          $purchase=$delivery->getRef();
          $purchase->setDate($delivery->getDate());
          $purchase->save();
        }
        //??how about invoice?
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $delivery)));

      $this->getUser()->setFlash('notice', $notice);
      $this->redirect('delivery/view?id='.$this->delivery->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeSetStatus(sfWebRequest $request)
  {
      $this->delivery=Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.id = '.$request->getParameter('id'))
      ->fetchOne();

      $oldstatus=$this->delivery->getStatus();
      $this->delivery->setStatus($request->getParameter('submit'));
      $this->delivery->save();

      //if delivered, generate stock entries      
      if($this->delivery->getIsDelivered())
      {
        foreach($this->delivery->getDeliverydetail() as $detail)
            $detail->updateStockentry();
      }
      else
      {
        foreach($this->delivery->getDeliverydetail() as $detail)
            $detail->deleteStockentry();
      }
      
      $this->getUser()->setFlash('notice', 'Successfully updated DR '.$this->delivery->getCode().' from '.$oldstatus.' to '.$this->delivery->getStatus().'.', true);
      $this->redirect($request->getReferer());
  }
  public function executeSetPaymentStatus(sfWebRequest $request)
  {
      $this->delivery=Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.id = '.$request->getParameter('id'))
      ->fetchOne();

      $oldstatus=$this->delivery->getPaymentStatus();
      $this->delivery->setPaymentStatus($request->getParameter('submit'));
      $this->delivery->save();

      $this->getUser()->setFlash('notice', 'Successfully updated DR '.$this->delivery->getCode().' from '.$oldstatus.' to '.$this->delivery->getPaymentStatus().'.', true);
      $this->redirect($request->getReferer());
  }
  public function executeSetDeliveryMethod(sfWebRequest $request)
  {
      $this->delivery=Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.id = '.$request->getParameter('id'))
      ->fetchOne();

      $this->delivery->setDeliveryMethod($request->getParameter('submit'));
      $this->delivery->save();

      $this->getUser()->setFlash('notice', 'Successfully updated delivery method.', true);
      $this->redirect($request->getReferer());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $delivery=$this->getRoute()->getObject();
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $delivery)));

    if ($delivery->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    //if referer is delivery/view, do not go back to it, it will throw an error, go to index instead
    //else redirect to referer
    //if "/delivery/" and "/view" are in referer url, 
    if(
      strpos($request->getReferer(),"/delivery/")!==false 
      and 
      strpos($request->getReferer(),"/view")!==false
      )
    {
      if($delivery->getRefClass())
        $this->redirect($delivery->getRefClass().'/view?id='.$delivery->getRefId());
      else
        $this->redirect('delivery/index');
    }
    else
      $this->redirect($request->getReferer());
  }
  public function executeSetNotes(sfWebRequest $request)
  {
      $this->delivery=Doctrine_Query::create()
      ->from('Delivery id')
      ->where('id.id = '.$request->getParameter('id'))
      ->fetchOne();

      $this->delivery->setNotes($request->getParameter('value'));
      $this->delivery->save();
      
      $this->redirect($request->getReferer());
  }
  public function executeSetCode(sfWebRequest $request)
  {
      $this->delivery=Doctrine_Query::create()
      ->from('Delivery id')
      ->where('id.id = '.$request->getParameter('id'))
      ->fetchOne();
      
      $code=$request->getParameter('value');

      $duplicate=Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.code = "'.$code.'"')
      ->fetchOne();
      if($duplicate!=null)
      {
        $this->getUser()->setFlash('error', "Delivery ".$code." already exists");
        $this->redirect($request->getReferer());
      }

      $this->delivery->setCode($code);
      $this->delivery->save();
      
      $this->redirect($request->getReferer());
  }
  /*
  public function executeProcessConversion(sfWebRequest $request)
  {
    $delivery=Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.id = '.$request->getParameter('parent_id'))
      ->fetchOne();
    $this->forward404Unless($delivery, sprintf('Delivery with id (%s) not found.', $request->getParameter('parent_id')));
    
    $conversion=Doctrine_Query::create()
      ->from('Conversion c')
      ->where('c.id = '.$request->getParameter('conversion_id'))
      ->fetchOne();
    $this->forward404Unless($conversion, sprintf('Conversion with id (%s) not found.', $request->getParameter('conversion_id')));
      
    //for each conversion detail, generate delivery details
    foreach($conversion->getConversiondetail() as $detail)
    {
      //??todo
    }
      
    $this->redirect('delivery/view?id='.$delivery->getId());
  }
  */
  public function executeMultiInputWizard(sfWebRequest $request)
  {
    $this->delivery= $this->getRoute()->getObject();

    /*
    //allow set conversion id by url
    $deliveryconversion=new Deliveryconversion();
    $deliveryconversion->setQty(1);
    $this->conversion_is_set=false;
    if($request->hasParameter("conversion_id"))
    {
      $deliveryconversion->setConversionId($request->getParameter("conversion_id"));
      $this->conversion_is_set=true;
    }
    $this->deliveryconversionform = new DeliveryconversionForm($deliveryconversion);
    */
    if($request->hasParameter("conversion_id"))
    {
      $this->conversion=Doctrine_Query::create()
        ->from('Conversion c, c.Conversiondetail cd')
        ->where('c.id = '.$request->getParameter('conversion_id'))
        ->fetchOne();
      $this->conversiondetails=$this->conversion->getConversiondetail();
    }
    
    $this->searchstring=$this->request->getParameter('searchstring');
  }
  public function executeProcessMultiInputWizard(sfWebRequest $request)
  {
    //just for validation
    $delivery=Doctrine_Query::create()
      ->from('Delivery id')
      ->where('id.id = '.$request->getParameter('id'))
      ->fetchOne();
    $this->forward404Unless($delivery, sprintf('Delivery with id (%s) not found.', $request->getParameter('parent_id')));
    
    //just for validation
    $conversion=Doctrine_Query::create()
      ->from('Conversion c')
      ->where('c.id = '.$request->getParameter('conversion_id'))
      ->fetchOne();   
    $this->forward404Unless($conversion, sprintf('Conversion with id (%s) not found.', $request->getParameter('conversion_id')));
    
    $detailscreated=false;
    $qtys=$request->getParameter('qty');//in the form of array(product_id=>qty)
    foreach($qtys as $product_id=>$qty)
    {
      //create detail only if qty is not 0
      if($qty!=0)
      {
        $detail=new Deliverydetail();
        $detail->setParentId($delivery->getId());
        $detail->setProductId($product_id);
        $detail->setQty($qty);
        $detail->save();
        $detailscreated=true;
        if($delivery->isDelivered())
          $detail->updateStockentry();
      }
    }
    
    //create deliveryconversion only if deliverydetails were created //this feature is disabled
    //if($detailscreated)
    {
      $existingdc=Doctrine_Query::create()
        ->from('Deliveryconversion dc')
        ->where('dc.parent_id = '.$delivery->getId())
        ->andWhere('dc.conversion_id = '.$request->getParameter('conversion_id'))
        ->fetchOne();   
      if($existingdc==null)
      {
        $dc=new Deliveryconversion();
        $dc->setParentId($delivery->getId());
        $dc->setConversionId($request->getParameter('conversion_id'));
        $dc->setQty(0);
        $dc->save();
      }
    }
    
    $this->getUser()->setFlash('notice', "Successfully added delivery receipt details");
    $this->redirect('delivery/view?id='.$delivery->getId());
  }
  public function executeGenConversionDr(sfWebRequest $request)
  {
    $dc=Doctrine_Query::create()
      ->from('Deliveryconversion dc')
      ->where('dc.id = '.$request->getParameter('deliveryconversion_id'))
      ->fetchOne();   
    $this->forward404Unless($dc, sprintf('DeliveryConversion with id (%s) not found.', $request->getParameter('deliveryconversion_id')));
    
    $delivery=$dc->getDelivery();
    
    //validation: cannot create a conversion dr for another conversion dr
    if($delivery->getType()=="Conversion")
  	{
        $this->getUser()->setFlash('error', "Cannot generate conversion DR for another conversion DR");
        return $this->redirect($request->getReferer());
  	}
    
    $conversiondelivery=$dc->genConversionDr();    
    
    $this->getUser()->setFlash('notice', "Successfully generated conversion delivery");
    $this->redirect('delivery/view?id='.$conversiondelivery->getId());
  }
  public function executeConversionWizard(sfWebRequest $request)
  {
    $conversion_id=$request->getParameter("conversion_id");
    if($conversion_id=="")$conversion_id=0;
    $this->conversion=Doctrine_Query::create()
      ->from('Conversion c')
      ->where('c.id = '.$conversion_id)
      ->fetchOne();  
    if($this->conversion==null)return;
    
    $this->conversiondetails=$this->conversion->getConversiondetail();
      
    $productids=array();
    foreach($this->conversiondetails as $cd)
      $productids[]=$cd->getProductId();
      
    //fetch stock quantities      
    $stocks=Doctrine_Query::create()
        ->from('Stock s')
      	->whereIn('s.product_id', $productids)
      	->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	->execute();

    //put stocks and stock quantities into an array      	
    $this->stockarray=array();
    $this->qtysbyproductid=array();
    foreach($stocks as $stock)
    {
      if(!isset($this->qtysbyproductid[$stock->getProductId()]))$this->qtysbyproductid[$stock->getProductId()]=0;
      $this->qtysbyproductid[$stock->getProductId()]+=$stock->getCurrentQty();
      $this->stockarray[$stock->getProductId()]=$stock;
    }

    //calculate max conversion qty    
    $first=true;
    $qty=0;
    $this->minqty=0;
    //for each part of the conversion
    foreach($this->conversiondetails as $convdetail){
    //use only 'from' parts
    if($convdetail->getType()=="from")
    {
      //if some parts missing, conversion is not possible
      if(!isset($this->qtysbyproductid[$convdetail->getProductId()]))
      {
        $this->minqty=0;
        break;
      }

      //apply get minimum value algorithm for $qty
      //$qty is the qty convertible if taking only this part into account
      $qty=$this->qtysbyproductid[$convdetail->getProductId()]/$convdetail->getQty();
      //if first loop, just save the qty to minqty
      if($first)
        $this->minqty=$qty;
      //compare possibleqty
      else
        if($this->minqty>$qty)$this->minqty=$qty;
    
      $first=false;
    }
    }
    
    
    
  }
  public function executeProcessConversionWizard(sfWebRequest $request)
  {
    //load parameters
    $minqty=$request->getParameter("minqty");
    $ignorelimit=$request->getParameter("ignorelimit");
    $qty=$request->getParameter("qty");

    //fetch conversion from database
    $conversion_id=$request->getParameter("conversion_id");
    if($conversion_id=="")$conversion_id=0;
    $conversion=Doctrine_Query::create()
      ->from('Conversion c')
      ->where('c.id = '.$conversion_id)
      ->fetchOne();  
      
    //validate conversion exists
	  if($conversion==null)
	    return $this->redirect("home/error?msg=Invalid conversion type");

    //validate qty is valid, unless ignore checkbox is checked
    if($qty>$minqty and $ignorelimit==null)
      return $this->redirect("home/error?msg=Insufficient parts. Please check the Ignore Qty Limit checkbox");
  
    //generate delivery
    $gendelivery=DeliveryTable::createDefault("Conversion");
//    $gendelivery->setRefClass('delivery');
//    $gendelivery->setRefId($delivery->getId());
//    if($delivery->getType()=="Incoming")
//      $gendelivery->setPono($delivery->getCode());
//    else      
//      $gendelivery->setInvno($delivery->getCode());
    $gendelivery->setNotes("Generated by Conversion Wizard: ".$conversion->getName()." x ".$qty);
    $gendelivery->save();
    
    $dc=new Deliveryconversion();
    $dc->setParentClass("Delivery");
    $dc->setParentId($gendelivery->getId());
    $dc->setConversionId($conversion->getId());
    $dc->setQty($qty);
    $dc->save();
    $dc->genDeliverydetails();

	  return $this->redirect("delivery/view?id=".$gendelivery->getId());
  }
  public function executeRecalcConversions(sfWebRequest $request)
  {
    $delivery=Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.id = '.$request->getParameter("id"))
      ->fetchOne();  

    //get the conversions included in this dr
    //put their ids in an array
    $conversion_ids=array();
    foreach($delivery->getDeliveryconversion() as $conversion)
      $conversion_ids[]=$conversion->getId();
    
    foreach($delivery->getDeliverydetail() as $detail)
    {
      //detect conversions this detail's product might be a part of
      $conversiondetails=$detail->getProduct()->getConversiondetail();
      $conversion=null;
      foreach($conversiondetails as $cd)
      {
        //if conversion id is not in list of dr's conversions
        if(!in_array($cd->getConversionId(),$conversion_ids))
        {
          //create it
          $deliveryconversion=new Deliveryconversion();
          $deliveryconversion->setParentId($delivery->getId());
          $deliveryconversion->setConversionId($cd->getConversionId());
          $deliveryconversion->save();
          $conversion_ids[]=$cd->getConversionId();
        }
      }
    }

	  return $this->redirect("delivery/view?id=".$delivery->getId());
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('delivery');

    $this->forward404Unless($delivery = Doctrine::getTable('Delivery')->find(array($requestparams['id'])), sprintf('Delivery with id (%s) not found.', $request->getParameter('id')));

    //set date and adjust stock entry date if necessary
    $delivery->setDateAndUpdateStockEntry($requestparams["date"]["year"]."-".$requestparams["date"]["month"]."-".$requestparams["date"]["day"]);
    $delivery->save();

    $this->redirect($request->getReferer());
  }
}
