<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
  <?php echo form_tag_for($form, '@in_check') ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <input type=hidden name="invoice_id" id="invoice_id" value=<?php echo $invoice->getId()?>>
    <!--if invoice_based is true, work directly on invoice and not on event-->
    <input type=hidden name="invoice_based" id="invoice_based" value=<?php echo $invoice_based?>>
    <input type=hidden name="chequeamt" id="chequeamt" value=<?php echo $chequeamt?>>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
      <?php include_partial('in_check/form_fieldset', array('in_check' => $in_check, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
    <?php endforeach; ?>

    <?php include_partial('in_check/form_actions', array('invoice' => $invoice, 'in_check' => $in_check, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </form>
</div>
