<table>
<tr>
<?php if($select){?>
  <th></th>
<?php } ?>
  <th>Voucher</th>
  <th>Check No.</th>
  <th>Amount</th>
  <th>Remaining</th>
  <th>Customer</th>
  <th>Pay To</th>
  <th>Receive Date</th>
  <th>Check Date</th>
<?php /*
<?php if($edit){?>
  <th>Edit</th>
<?php } ?>
<?php if($delete){?>
  <th>Delete</th>
<?php } ?>
*/ ?>
</tr>

<?php foreach($existing_checks as $check){ ?>
<tr>
  <?php if($select){?>
    <td>
      <?php echo form_tag('in_check/applyExistingCheck') ?>
        <input type="hidden" id=id name=id value=<?php echo $check->getId()?>>
        <input type="hidden" id=invoice_id name=invoice_id value=<?php echo $invoice->getId()?>>
        <input type="hidden" id=invoice_based name=invoice_based value=<?php echo $invoice_based?>>
        <input type="hidden" id=chequeamt name=chequeamt value=<?php echo $chequeamt?>>
        <input type=submit value="Select">
      </form>
    </td>
  <?php } ?>
    <td><?php echo $check->getCode()?></td>
    <td><?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
    <td><?php echo number_format ( $check->getAmount(),2 )?></td>
    <td><?php echo number_format ( $check->getRemaining(),2 )?></td>
    <td><?php echo $check->getCustomer()?></td>
    <td><?php echo $check->getPassbook()?></td>
    <td><?php echo MyDateTime::frommysql($check->getReceiveDate())->toshortdate()?></td>
    <td><?php echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
  <?php } ?>
</tr>
</table>
