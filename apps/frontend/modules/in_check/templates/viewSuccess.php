
<h1>Incoming Check <?php echo $check->getCode()?>
<?php
if($check->isDeposited())echo " <font color=blue>(Deposited)</font>";
if($check->isBounced())echo " <font color=red>(Bounced)</font>";
?>

</h1>
<table border=1>
<tr>
  <th>Voucher</th>
  <th>Check No.</th>
  <th>Amount</th>
  <th>Remaining</th>
  <th>Customer</th>
</tr>

<tr>
    <td><?php echo $check->getCode()?></td>
    <td><?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
    <td><?php echo number_format ( $check->getAmount(),2 )?></td>
    <td><?php echo number_format ( $check->getRemaining(),2 )?></td>
    <td><?php echo link_to($check->getCustomer(),"customer/viewChecks?id=".$check->getCustomerId());?></td>
</tr>
</table>
<br>
<?php 
if($sf_user->hasCredential(array('admin'),false))
{
  if($check->isDeposited())
    echo link_to('Undo Deposit','in_check/undoDeposit?id='.$check->getId(),array('confirm' => 'UNDO DEPOSIT?'));
  else if($check->isBounced())
    echo link_to('Undo Bounce','in_check/undoBounce?id='.$check->getId(),array('confirm' => 'UNDO BOUNCE?'));
  else
  {
    echo link_to('Deposit','in_check/deposit?id='.$check->getId(),array('confirm' => 'DEPOSIT?'));
    echo "<br>";
    echo link_to('Bounce','in_check/bounce?id='.$check->getId(),array('confirm' => 'BOUNCE?'));
  }
}
else if(!$check->isDeposited())
  echo link_to('Deposit','in_check/deposit?id='.$check->getId(),array('confirm' => 'DEPOSIT?'));

if($check->isDeposited())
  echo "<br>Check Deposited on ".MyDateTime::frommysql($check->getDepositDate())->toshortdate()."<br>";
if($check->isBounced())
  echo "<br>Check Bounced on ".MyDateTime::frommysql($check->getBounceDate())->toshortdate()."<br>";

?>


<h2>Paid for:</h2>
<table border=1>
  <tr>
    <td>Invoice</td>
    <td>Date</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>

<?php foreach($check->getInvoice() as $invoice){ ?>
  <?php if($invoice->getChequeAmt()>0){?>
    <tr>
      <td><?php echo link_to($invoice, "invoice/events?id=".$invoice->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
      <td><?php echo $invoice->getChequeAmt() ?></td>
      <td><?php echo $invoice->getParticularsString() ?></td>
      <td><?php echo $invoice->getInCheck()->getNotes() ?></td>
      <td></td>
      <td><?php //echo link_to("Delete","in_check/removeFromInvoice?invoice_id=".$invoice->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?'));?></td>
    </tr>
  <?php } ?>
<?php } ?>

<?php foreach($check->getEvent() as $detail){$invoice=$detail->getParent(); ?>
    <tr>
      <td><?php echo link_to($invoice, "invoice/events?id=".$invoice->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td><?php echo $detail->getAmount() ?></td>
      <td><?php echo $invoice->getParticularsString() ?></td>
      <td><?php echo $detail->getNotes() ?></td>
      <td><?php //echo link_to('Edit','event/edit?id='.$detail->getId()) ?></td>
      <td>
        <?php /*echo link_to(
          'Delete',
          'event/delete?id='.$detail->getId(),
          array('method' => 'delete', 'confirm' => 'Are you sure?')
        )*/ ?>
      </td>
    </tr>
  <?php }?>



</table>


