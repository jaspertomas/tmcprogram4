<?php use_helper('I18N', 'Date') ?>
<?php include_partial('in_check/assets') ?>

<div id="sf_admin_container">

<?php if(count($existing_checks)>0){?>
  <h1>Choose Existing Check</h1>
  <?php include_partial('in_check/existing_checks', array('select' => true,'invoice_based' => $invoice_based,'invoice' => $invoice,'existing_checks' => $existing_checks,'chequeamt' => $chequeamt)) ?>

<?php } ?>

  <h1><?php echo __((count($existing_checks)>0?"or ":"").'Create Incoming Check', array(), 'messages') ?></h1>
  <?php include_partial('in_check/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('in_check/form_header', array('in_check' => $in_check, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('in_check/form', array('chequeamt' => $chequeamt,'invoice_based' => $invoice_based,'invoice' => $invoice, 'in_check' => $in_check, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('in_check/form_footer', array('in_check' => $in_check, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
