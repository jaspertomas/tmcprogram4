<?php

require_once dirname(__FILE__).'/../lib/in_checkGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/in_checkGeneratorHelper.class.php';

/**
 * in_check actions.
 *
 * @package    sf_sandbox
 * @subpackage in_check
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class in_checkActions extends autoIn_checkActions
{
  //detach check from invoice; check still exists and can be applied elsewhere
  public function executeRemoveFromInvoice(sfWebRequest $request)
  {
    $invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("invoice_id")));
    $in_check=$invoice->getInCheck();
    if($in_check!=null and $in_check->getId()!=null)
      $in_check->removeInvoice($invoice);
    return $this->redirect($request->getReferer());
  }
  public function executeApplyExistingCheck(sfWebRequest $request)
  {
    //this is the amount to be allocated to the invoice or event
    $chequeamt=$request->getParameter("chequeamt");

    $invoice_based=$request->getParameter("invoice_based");
    if($invoice_based=="false")$invoice_based=false;
    $invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("invoice_id")));
    $in_check=Fetcher::fetchOne("InCheck",array("id"=>$request->getParameter("id")));

    //if invoice has no balance, error
    if($invoice->getBalance()==0)
    {
      $message="Cannot create cheque: Invoice is fully paid.";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    if($invoice_based)
    {
      $in_check->applyToInvoice($invoice,$chequeamt);
      return $this->redirect("invoice/view?id=".$invoice->getId());
    }
    else
    {
      $in_check->genCheckCollectionEvent($invoice,$chequeamt);
      return $this->redirect("invoice/events?id=".$invoice->getId());
    }

  }
  public function executeNew(sfWebRequest $request)
    {
        //this is the amount that the user is requesting
        $this->chequeamt=$request->getParameter("chequeamt");

        $this->check=new InCheck();
        $this->check->setCode(InCheckTable::genCode());
        $this->check->setCheckDate(MyDate::today());
        $this->check->setReceiveDate(MyDate::today());
        $this->check->setRemaining(0);
        
        if($request->hasParameter("invoice_id"))
        {
            $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("invoice_id")));

            //if invoice has no balance, error
            if($this->invoice->getBalance()==0)
            {
              $message="Cannot create cheque: Invoice is fully paid.";
              $this->getUser()->setFlash('error', $message);
              return $this->redirect($request->getReferer());
            }

            //apply check payment directly on invoice, not on event
            $this->invoice_based;
            if($request->hasParameter("invoice_based"))
              $this->invoice_based=$request->getParameter("invoice_based");
            else
              $this->invoice_based=false;
            
            $this->customer=$this->invoice->getCustomer();
            $this->check->setCustomerId($this->invoice->getCustomerId());
            $this->existing_checks=$this->customer->getChecksWithRemaining();
        }

        $this->form=new InCheckForm($this->check);
        $this->in_check=$this->check;
    }
    public function executeEdit(sfWebRequest $request)
    {
        $this->in_check = $this->getRoute()->getObject();
        $this->form = $this->configuration->getForm($this->in_check);

        if($request->hasParameter("invoice_id"))
        {
            $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("invoice_id")));
        }
        else
        {
          echo "Access Denied"; 
          die();
        }
    }
    protected function processForm(sfWebRequest $request, sfForm $form)
    {
      $chequeamt=$request->getParameter("chequeamt");

      $invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("invoice_id")));
      //if invoice_based is true, work directly on invoice and not on event
      $invoice_based=$request->getParameter("invoice_based");
      if($invoice_based=="false")$invoice_based=false;
      
      $requestparams=$request->getParameter($form->getName());
      $isnew=$requestparams["id"]=="";
      
      //if bank to bank, automatic deposit
      if($requestparams["is_bank_transfer"])
        $requestparams["deposit_date"]=MyDate::today();

      //set initial value of remaining as equal to amount
      $requestparams["remaining"]=$requestparams["amount"];
      
      //is_bank_transfer is a checkbox. if false, it will not exist. 
      if(!isset($requestparams["is_bank_transfer"]))
        $requestparams["is_bank_transfer"]=0;
      else
        $requestparams["is_bank_transfer"]=1;

      //validate-----------------------------------
      //if invoice is fully paid, error
      if($invoice->getBalance()==0 and $isnew)
      {
        $message="Cannot create cheque: Invoice is fully paid.";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect("invoice/view?id=".$invoice->getId());
      }

      //if check has no check number and is not bank transfer, error
      if(trim($requestparams["check_no"])=="" and $requestparams["is_bank_transfer"]==0)
      {
        $message="Please check 'Is Bank Transfer' or enter a check number";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
  
      //if check has no amount, error
      if($requestparams["amount"]==0)
      {
        $message="Please enter an amount";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
  

      $form->bind($requestparams, $request->getFiles($form->getName()));
      if ($form->isValid())
      {
        $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';
  
        try {
          $in_check = $form->save();
          $in_check->calc();
          $in_check->save();
          
          //do this only if in check is new
          //else it is edit, do not create payment
          if($isnew)
          {
            if($invoice_based)
            {
              $in_check->applyToInvoice($invoice,$chequeamt);
            }
            else
            {
              $in_check->genCheckCollectionEvent($invoice,$chequeamt);
            }
          }
        } catch (Doctrine_Validator_Exception $e) {
  
          $errorStack = $form->getObject()->getErrorStack();
  
          $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
          foreach ($errorStack as $field => $errors) {
              $message .= "$field (" . implode(", ", $errors) . "), ";
          }
          $message = trim($message, ', ');
  
          $this->getUser()->setFlash('error', $message);
          return sfView::SUCCESS;
        }
  
        $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $in_check)));
  
        $this->getUser()->setFlash('notice', $notice);

        //--------------create passbook entry---------
        //check if passbook entry exists
        //if yes, update it
        //if no, create it
        $passbook=$in_check->getPassbook();
        $date=$in_check->getCheckDate();
        $qty=$in_check->getAmount();
        $ref_class="InCheck";
        $ref_id=$in_check->getId();
        $client_type="Customer";
        $client_name=$in_check->getCustomer()->getName();
        $created_by_id=$this->getUser()->getGuardUser()->getId();
        $type="InCheck";
        $description=$in_check->getNotes();

        $passbook_entries=$in_check->getPassbookEntries();
        //just delete all the old ones and create a new one
        foreach($passbook_entries as $entry)$entry->delete();
        $passbookEntry=$passbook->addEntry($date, $qty, $ref_class, $ref_id, $client_type, $client_name, $type, $description, $created_by_id);

        if($invoice_based)
        $this->redirect("invoice/view?id=".$invoice->getId());
        else
        $this->redirect("invoice/events?id=".$invoice->getId());
      }
      else
      {
        $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
      }
    }
    public function executeDelete(sfWebRequest $request)
    {
      $request->checkCSRFProtection();
  
      $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));
  
      $check=$this->getRoute()->getObject();

      //there should only be one invoice
      $invoices=$check->getInvoice();
      if(count($invoices)>0)
      {
        //$check->removeInvoice($invoices[0]);
        $message="Cannot delete check: Please delete related payments first";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }

      $events=$check->getEvent();
      if(count($events)>0)
      {
        $message="Cannot delete check: Please delete related payments first";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }

      if ($check->delete())
      {
        $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
        $passbook_entries=$check->getPassbookEntries();
        foreach($passbook_entries as $entry)
          $entry->delete();
      }
  
      $this->redirect($request->getReferer());
    }
    public function executeView(sfWebRequest $request)
    {
      $this->check=$this->getRoute()->getObject();
      /*
      if($check->getCustomerId()!=null)
      {
        return $this->redirect("customer/viewChecks?id=".$check->getCustomerId());
      }
      else if($check->getBilleeId()!=null)
      {
        return $this->redirect("billee/viewChecks?id=".$check->getBilleeId());
      }
      echo "No valid customer or billee for this check. Cannot continue";
      die();
      */
    }
    public function executeDeposit(sfWebRequest $request)
    {
      $check=$this->getRoute()->getObject();
      $check->toggleDeposit(true);
      return $this->redirect($request->getReferer());
    }
    public function executeUndoDeposit(sfWebRequest $request)
    {
      $check=$this->getRoute()->getObject();
      $check->toggleDeposit(false);
      //recalculate all invoices
      return $this->redirect($request->getReferer());
    }
    public function executeBounce(sfWebRequest $request)
    {
      $check=$this->getRoute()->getObject();
      $check->toggleBounce(true);
      return $this->redirect($request->getReferer());
    }
    public function executeUndoBounce(sfWebRequest $request)
    {
      $check=$this->getRoute()->getObject();
      $check->toggleBounce(false);
      //recalculate all invoices
      return $this->redirect($request->getReferer());
    }
    /*
    public function executeMassGenPassbookEntry(sfWebRequest $request)
    {
        //default values
      $this->interval=100;
      $this->start=1;
  
      //if method = get
      if(!isset($_REQUEST["submit"]))
      {
        $this->products=array();  
       return;
      }
       
      if(isset($_REQUEST["interval"]))
          $this->interval=$request->getParameter("interval");
      if(isset($_REQUEST["start"]))
          $this->start=$request->getParameter("start");
       $this->end=$this->start+$this->interval-1;
      
       $this->checks = Doctrine_Query::create()
        ->from('InCheck i')
        ->where('i.id <='.$this->end)
        ->andWhere('i.id >='.$this->start)
        ->execute();
        foreach ($this->checks as $check) {
          $check->genPassbookEntry($this->getUser()->getGuardUser()->getId());
        }
       $this->start=$this->end+1;
    }
    */
  }
