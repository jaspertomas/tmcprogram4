<?php

require_once dirname(__FILE__).'/../lib/invoice_dr_detailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/invoice_dr_detailGeneratorHelper.class.php';

/**
 * invoiceDr_detail actions.
 *
 * @package    sf_sandbox
 * @subpackage invoiceDr_detail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoice_dr_detailActions extends autoInvoice_dr_detailActions
{
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());
    
    $qty=intval($requestparams["qty"]);
    if($qty<1)
    {
      $this->getUser()->setFlash('error', 'Invalid Quantity', true);
      return $this->redirect("invoiceDr/view?id=".$requestparams['invoiceDr_id']);
    }
  
    //detect if return already has an rdetail of same product id
    $rdetail=Doctrine_Query::create()
    ->from('InvoiceDrDetail rd')
    ->where('rd.invoiceDr_id = '.$requestparams['invoiceDr_id'])
    ->andWhere('rd.product_id = '.$requestparams['product_id'])
    ->fetchOne();

    //if rdetail with same product id exists, adjust qty for that
    if($rdetail!=null)
    {
      $ref=$rdetail->getRef();
      
      if($ref!=null)
      {
        //adjust ref qty returned
        $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
        $ref->save();
      }
    }
    //else create new rdetail
    else
    {
      $rdetail=new InvoiceDrDetail();
      $rdetail->setProductId($requestparams['product_id']);
      $rdetail->setInvoiceDrId($requestparams['invoiceDr_id']);

      $invoiceDr=$rdetail->getInvoiceDr();
      $rdetail->setRefClass($invoiceDr->getIsSales()?"Invoicedetail":"Purchasedetail");
      
      //try to find an Invoicedetail or Purchasedetail that matches product_id
      if($invoiceDr->getIsSales())
      {
        $ref=Doctrine_Query::create()
          ->from('Invoicedetail id')
          ->where('id.invoice_id = '.$rdetail->getInvoiceDr()->getRefId())
          ->andWhere('id.product_id = '.$requestparams['product_id'])
          ->fetchOne();
      }else{
        $ref=Doctrine_Query::create()
          ->from('Purchasedetail id')
          ->where('id.purchase_id = '.$rdetail->getInvoiceDr()->getRefId())
          ->andWhere('id.product_id = '.$requestparams['product_id'])
          ->fetchOne();
      }
      //if invoicedetail or purchasedetail is found, use it as ref
      if($ref!=null)
      {
        $rdetail->setRefId($ref->getId());
        //adjust ref qty returned
        $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
        $ref->save();
      }

    }

    //adjust rdetail qty and calc prices
    $rdetail->setPrice($requestparams['price']);
    $rdetail->setTotal($requestparams['price']*$qty);
    $rdetail->setQty($qty);
    $rdetail->save();

    //adjust stock
    $rdetail->updateStockEntry();

    $this->getUser()->setFlash('notice', 'Successfully returned '.$rdetail->getProduct(), false);


    $this->redirect("invoiceDr/view?id=".$rdetail->getInvoiceDrId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $invoiceDr_detail=$this->getRoute()->getObject();

    if(count($invoiceDr_detail->getReplacementDetail())>0)
    {
      $this->getUser()->setFlash('error', "Cannot delete this returned item - replacements exist.");
      $this->redirect($request->getReferer());
    }
    
    if ($invoiceDr_detail->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("invoiceDr/view?id=".$invoiceDr_detail->getInvoiceDrId());
  }
}
