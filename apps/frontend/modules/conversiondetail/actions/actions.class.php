<?php

require_once dirname(__FILE__).'/../lib/conversiondetailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/conversiondetailGeneratorHelper.class.php';

/**
 * conversiondetail actions.
 *
 * @package    sf_sandbox
 * @subpackage conversiondetail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class conversiondetailActions extends autoConversiondetailActions
{
  public function executeEdit(sfWebRequest $request)
  {
    $this->redirect("home/error?msg=conversion details cannot be edited, only deleted");
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $conversiondetail = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $conversiondetail)));

      $this->getUser()->setFlash('notice', $notice);
      $this->redirect('conversion/view?id='.$conversiondetail->getConversionId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    
    $detail=$this->getRoute()->getObject();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $detail)));

    if ($detail->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('conversion/view?id='.$detail->getConversionId());
  }
  public function executeSetQty(sfWebRequest $request)
  {
    if($request->getParameter('value')==0)
    {
      $message="Invalid qty";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    $this->detail=Doctrine_Query::create()
    ->from('Conversiondetail id')
    ->where('id.id = '.$request->getParameter('id'))
    ->fetchOne();

    $this->detail->setQty($request->getParameter('value'));
    $this->detail->save();

    $this->redirect($request->getReferer());
  }
  public function executeSetType(sfWebRequest $request)
  {
    if($request->getParameter('value')!='from' and $request->getParameter('value')!='to')
    {
      $message="Invalid value ".$request->getParameter('value');
      $this->getUser()->setFlash('notice', $message,true);
      return $this->redirect($request->getReferer());
    }

    $this->detail=Doctrine_Query::create()
    ->from('Conversiondetail id')
    ->where('id.id = '.$request->getParameter('id'))
    ->fetchOne();

    $this->detail->setType($request->getParameter('value'));
    $this->detail->save();

    $this->getUser()->setFlash('notice', "Successfully moved item",true);
    $this->redirect($request->getReferer());
  }
}
