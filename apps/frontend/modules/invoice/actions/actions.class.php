<?php

require_once dirname(__FILE__).'/../lib/invoiceGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/invoiceGeneratorHelper.class.php';

/**
 * invoice actions.
 *
 * @package    sf_sandbox
 * @subpackage invoice
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $

to show status pending if checks not yet cleared:
on invoice update:
	invoice checkcleardate default = date


 */
class invoiceActions extends autoInvoiceActions
{
  public function executeIncstate(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");

    //startdate
    if($request->hasParameter("startdate"))
    {
      $startdate=$request->getParameter("startdate");
    }
    elseif($request->hasParameter("startdatesplit"))
    {
      $requestparams=$request->getParameter("startdatesplit");
      $day=str_pad($requestparams["day"], 2, "0", STR_PAD_LEFT);
      $month=str_pad($requestparams["month"], 2, "0", STR_PAD_LEFT);
      $year=$requestparams["year"];
      
      if($day=="" or $month=="" or $year=="")
    	  return $this->redirect("home/error?msg=Incorrect start date");
      
      $startdate=$year."-".$month."-".$day;
    }
    else
    {
      $startdate=MyDate::today();
    }
    $this->startdate=$startdate;

    //enddate
    if($request->hasParameter("enddate"))
    {
      $enddate=$request->getParameter("enddate");
    }
    elseif($request->hasParameter("enddatesplit"))
    {
      $requestparams=$request->getParameter("enddatesplit");
      $day=str_pad($requestparams["day"], 2, "0", STR_PAD_LEFT);
      $month=str_pad($requestparams["month"], 2, "0", STR_PAD_LEFT);
      $year=$requestparams["year"];
      
      if($day=="" or $month=="" or $year=="")
    	  return $this->redirect("home/error?msg=Incorrect end date");
      
      $enddate=$year."-".$month."-".$day;
    }
    else
    {
      $enddate=MyDate::today();
    }
    $this->enddate=$enddate;
 
  	if($this->startdate>$this->enddate)
  	{
  	  return $this->redirect("home/error?msg=Start date cannot be later than end date");
  	}

    //load records from database
    $this->invoices = InvoiceTable::fetchByDateRange($startdate,$enddate);
    $this->events = EventTable::fetchByDatenParentclass($startdate,"Invoice");
    $this->vouchers = VoucherTable::fetchByDateRange($startdate,$enddate);

      $this->cashsales=0;
      $this->chequesales=0;
      $this->creditsales=0;
      $this->cashother=0;
      $this->chequeother=0;
      $this->creditother=0;
      $this->cashtotal=0;
      $this->chequetotal=0;
      $this->credittotal=0;
      $this->deducttotal=0;

      $this->templates=Doctrine_Query::create()
        ->from('InvoiceTemplate t')
        //->where('t.name="Invoice"')//invoice only
        ->orderBy('id')
      	->execute();

      //calculations start

      foreach($this->invoices as $invoice)
        if($invoice->getIsTemporary()==0)
        if($invoice->getStatus()!="Cancelled")
        if($invoice->getHidden()==0)//if not hidden
      {
        $this->cashsales+=$invoice->getCash();

        $this->chequesales+=$invoice->getChequeamt();
        $this->creditsales+=$invoice->getCredit();
        $this->cashtotal+=$invoice->getCash();
        $this->chequetotal+=$invoice->getChequeamt();
        $this->credittotal+=$invoice->getCredit();
        $this->deducttotal+=$invoice->getDsrdeduction();
      }
    $this->salestotal=$this->cashtotal+$this->chequetotal+$this->credittotal;

      //-------------VOUCHERS------------------------
    //calculate totals
    $this->accounts= Doctrine_Query::create()
        ->from('VoucherAccount a')
      	->orderBy('a.name')
      	->execute();
    	//account totals
    	$this->totals_by_account=array();
    	$this->expensestotal=0;
      foreach($this->accounts as $account)
      {
        $this->totals_by_account[$account->getId()]=0;
      }
      foreach($this->vouchers as $voucher)
      {
        if($voucher->getAccountId())
          $this->totals_by_account[$voucher->getAccountId()]+=$voucher->getAmount();
      }
      foreach($this->accounts as $account)
      {
        $this->expensestotal+=$this->totals_by_account[$account->getId()];
      }
    //-------------PROFITDETAIL--------------------------
    //fetch profit records from database
    $this->profitdetails = ProfitdetailTable::fetchByDateRange($startdate,$enddate);
    $this->invoicedetails = InvoicedetailTable::fetchByDateRangeAndHasRemaining($startdate,$enddate);

    $this->profittotal=0;
    $this->unaccountedsaletotal=0;

    //calculations start

    foreach($this->profitdetails as $detail)
      $this->profittotal+=$detail->getProfit();

    foreach($this->invoicedetails as $detail)
    {
      if($detail->getIsVat())continue;//ignore vat entries
    
      //detect buy price; check max buy price first
      $buyprice=$detail->getProduct()->getMaxbuyprice();
      //if max buy price is 0, check min buy price
      if($buyprice==0)$buyprice=$detail->getProduct()->getMinbuyprice();
      
      //if it's a service, 
      //is_service=1 means labor - repairs, etc - cost is employee salary
      //is_service=2 means production (control panels, etc) - 
          //cost of materials will be deducted via expenses
      //is_service=3 means delivery fee, not counted
      if($detail->getProduct()->getIsService()==1 or $detail->getProduct()->getIsService()==2)
      {
        //add to profit total
        $this->profittotal+=$detail->getRemaining()*$detail->getPrice();
      }
      //if still no buy price, add to unaccounted sale
      elseif($buyprice==0)
      {
        //add to unaccounted sales
        $this->unaccountedsaletotal+=$detail->getRemaining()*$detail->getPrice();
      }
      else
      {
        //use buy price to calculate profit
        $profit=$detail->getPrice()-$buyprice;
        //add to profit total
        $this->profittotal+=$detail->getRemaining()*$profit;
      }
    }

    //------------TOTALS----------------------------------
    $this->incometotal=$this->salestotal-$this->expensestotal;
  }
  /*
  public function executeIncstate(sfWebRequest $request)
  {
    $this->invoices=Doctrine_Query::create()
        ->from('Invoice i')
      	->where('date >="2011-02-01"')
      	->andwhere('date <="2011-02-28"')
      	->execute();

    $this->bymonth=array();
    $this->cashtotal=0;
    $this->chequetotal=0;
    $this->credittotal=0;
    foreach($this->invoices as $invoice)
    {
      if(!array_key_exists($invoice->getDate(),$this->bymonth))
      {
        $this->bymonth[$invoice->getDate()]["cash"]=0;
        $this->bymonth[$invoice->getDate()]["cheque"]=0;
        $this->bymonth[$invoice->getDate()]["credit"]=0;
      }
      $this->bymonth[$invoice->getDate()]["cash"]+=$invoice->getCash();
      $this->bymonth[$invoice->getDate()]["cheque"]+=$invoice->getChequeamt();
      $this->bymonth[$invoice->getDate()]["credit"]+=$invoice->getCredit();
      $this->cashtotal+=$invoice->getCash();
      $this->chequetotal+=$invoice->getChequeamt();
      $this->credittotal+=$invoice->getCredit();
    }
  }
    */
  public function executeDsr(sfWebRequest $request)
  {


    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);

    //------------INVOICES--------------------
    $this->invoices = InvoiceTable::fetchByDate($invoice->getDate());
    $this->events = EventTable::fetchByDatenParentclass($invoice->getDate(),"Invoice");
    $this->vouchers = VoucherTable::fetchPettyCashVouchersByDate($invoice->getDate());

      $this->cashsales=0;
      $this->chequesales=0;
      $this->creditsales=0;
      $this->cashother=0;
      $this->chequeother=0;
      $this->creditother=0;
      $this->cashtotal=0;
      $this->chequetotal=0;
      $this->credittotal=0;
      $this->deducttotal=0;
      $this->totalForProductCategory=0;
      $this->totalForNoProductCategory=0;
      foreach($this->invoices as $invoice)
      //if closed and not cancelled, do the usual process
      if($invoice->getIsTemporary()==0 and $invoice->getStatus()!="Cancelled")
      // if($invoice->getHidden()==0)//if not hidden
      {
        $this->cashsales+=$invoice->getCash();
        $this->chequesales+=$invoice->getChequeamt();
        $this->creditsales+=$invoice->getCredit();
        $this->cashtotal+=$invoice->getCash();
        $this->chequetotal+=$invoice->getChequeamt();
        $this->credittotal+=$invoice->getCredit();
        $this->deducttotal+=$invoice->getDsrdeduction();
        $this->totalForProductCategory+=$invoice->getTotalForProductCategory();
        $this->totalForNoProductCategory+=$invoice->getTotalForNoProductCategory();
      }
      //else if temporary or cancelled but paid, payment must still be reported
      else
      {
        $this->cashsales+=$invoice->getCash();
        $this->chequesales+=$invoice->getChequeamt();
        $this->cashtotal+=$invoice->getCash();
        $this->chequetotal+=$invoice->getChequeamt();
      }

      foreach($this->events as $event)
      {
        $invoice=$event->getParent();
        if($invoice and $invoice->getStatus()!="Cancelled")
        {
          $this->cashother+=$event->getDetail("cashamt");
          $this->chequeother+=$event->getDetail("chequeamt");
          $this->creditother+=$event->getDetail("creditamt");
          $this->cashtotal+=$event->getDetail("cashamt");
          $this->chequetotal+=$event->getDetail("chequeamt");
          $this->credittotal+=$event->getDetail("creditamt");
          $this->deducttotal+=$event->getDetail3();
        }
      }
      $this->total=$this->cashtotal+$this->chequetotal+$this->credittotal;
      //-------------VOUCHERS------------------------

    //calculate totals
      $this->pettycashtotal=0;
      foreach($this->vouchers as $voucher)
      {
        if($voucher->getVoucherTypeId()==1)
          $this->pettycashtotal+=$voucher->getAmount();
      }
      $this->remainingcash=$this->cashtotal-$this->pettycashtotal;
  }

  public function executeDsrmulti(sfWebRequest $request)
  {
    $this->template_id=$request->getParameter("template_id");

/*
    //default template is invoice
    if($this->template_id==null)
    {
      $default_template_name=sfConfig::get('custom_dsrmulti_default_template_name');
      if($default_template_name!="")
      $template=Doctrine_Query::create()
        ->from('InvoiceTemplate t')
        ->where('t.name="'.$default_template_name.'"')
        ->fetchOne();
      //if template exists
      if($template)
        $this->template_id=$template->getId();
    }
*/

    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("startdate"))
      $invoice->setDate($request->getParameter("startdate"));
    elseif(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $requestparams=$request->getParameter("purchase");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $purchase=new Purchase();
    if($request->hasParameter("enddate"))
      $purchase->setDate($request->getParameter("enddate"));
    elseif(!$day or !$month or !$year)
      $purchase->setDate(MyDate::today());
    else
      $purchase->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->toform=new PurchaseForm($purchase);

    $this->invoices = InvoiceTable::fetchByDateRange($invoice->getDate(),$purchase->getDate());
    $this->events = EventTable::fetchByDatenParentclass($invoice->getDate(),"Invoice");
    $this->vouchers = VoucherTable::fetchByDateRange($invoice->getDate(),$purchase->getDate());

      $this->cashsales=0;
      $this->chequesales=0;
      $this->creditsales=0;
      $this->cashother=0;
      $this->chequeother=0;
      $this->creditother=0;
      $this->cashtotal=0;
      $this->chequetotal=0;
      $this->credittotal=0;
      $this->deducttotal=0;
      $this->totalForProductCategory=0;
      $this->totalForNoProductCategory=0;

      $query=Doctrine_Query::create()
        ->from('InvoiceTemplate t')
        ->orderBy('id');
      //hide interoffice and other templates if invoiceonly=true
      if($this->template_id!=null)
        $query->where('t.id='.$this->template_id);//specific template
      $this->templates=$query->execute();

      //calculations start

      //calculate product categories
      $this->productCategoryTotals=array();
      $this->totalPumps=0;
      $this->totalTanks=0;
      $this->totalSheets=0;
      $this->totalSupplies=0;

      //no tmc so, no lesley, no interoffice
      foreach($this->templates as $template)
      {
        //end no tmc so, no lesley, no interoffice
        foreach($this->invoices as $invoice)
        if($invoice->getIsTemporary()==0)
        if($invoice->getTemplateId()==$template->getId())
        if($invoice->getStatus()!="Cancelled")
        if($invoice->getHidden()==0)//if not hidden
      {
        $this->cashsales+=$invoice->getCash();
        $this->chequesales+=$invoice->getChequeamt();
        $this->creditsales+=$invoice->getCredit();
        $this->cashtotal+=$invoice->getCash();
        $this->chequetotal+=$invoice->getChequeamt();
        $this->credittotal+=$invoice->getCredit();
        $this->deducttotal+=$invoice->getDsrdeduction();
        $this->totalForProductCategory+=$invoice->getTotalForProductCategory();
        $this->totalForNoProductCategory+=$invoice->getTotalForNoProductCategory();

        //calculate product categories
        $totals=$invoice->getTotalForProductCategories();
        $this->productCategoryTotals[$invoice->getId()]=$totals;
        $this->totalPumps+=$totals[""];
        $this->totalTanks+=$totals["TJL"];
        $this->totalSheets+=$totals["Sheet"];
        $this->totalSupplies+=$totals["Supplies"];
      }
//no tmc so, no lesley, no interoffice
}
//end no tmc so, no lesley, no interoffice
/*
//hide events
      foreach($this->events as $event)
      {
        $invoice=$event->getParent();
        if($invoice->getStatus()!="Cancelled")
        {
          $this->cashother+=$event->getDetail("cashamt");
          $this->chequeother+=$event->getDetail("chequeamt");
          $this->creditother+=$event->getDetail("creditamt");
          $this->cashtotal+=$event->getDetail("cashamt");
          $this->chequetotal+=$event->getDetail("chequeamt");
          $this->credittotal+=$event->getDetail("creditamt");
          $this->deducttotal+=$event->getDetail3();
        }
      }
*/
      $this->total=$this->cashtotal+$this->chequetotal+$this->credittotal;

      //-------------VOUCHERS------------------------
    //calculate totals
      $this->pettycashtotal=0;
      foreach($this->vouchers as $voucher)
      {
        if($voucher->getVoucherTypeId()==1)
          $this->pettycashtotal+=$voucher->getAmount();
      }
      $this->remainingcash=$this->cashtotal-$this->pettycashtotal;
  }


  public function executeDsrmultiForProductCategory(sfWebRequest $request)
  {
    $this->template_id=$request->getParameter("template_id");

    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("startdate"))
      $invoice->setDate($request->getParameter("startdate"));
    elseif(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $requestparams=$request->getParameter("purchase");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $purchase=new Purchase();
    if($request->hasParameter("enddate"))
      $purchase->setDate($request->getParameter("enddate"));
    elseif(!$day or !$month or !$year)
      $purchase->setDate(MyDate::today());
    else
      $purchase->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->toform=new PurchaseForm($purchase);

    $this->invoices = InvoiceTable::fetchByDateRange($invoice->getDate(),$purchase->getDate());
    // $this->events = EventTable::fetchByDatenParentclass($invoice->getDate(),"Invoice");
    // $this->vouchers = VoucherTable::fetchByDateRange($invoice->getDate(),$purchase->getDate());

    $this->productcategorytotal=0;
    $this->noproductcategorytotal=0;

      $query=Doctrine_Query::create()
        ->from('InvoiceTemplate t')
        ->orderBy('id');
      //hide interoffice and other templates if invoiceonly=true
      if($this->template_id!=null)
        $query->where('t.id='.$this->template_id);//specific template
      $this->templates=$query->execute();

      //calculations start

    //no tmc so, no lesley, no interoffice
    foreach($this->templates as $template)
    {
    //end no tmc so, no lesley, no interoffice
      foreach($this->invoices as $invoice)
        if($invoice->getIsTemporary()==0)
        if($invoice->getTemplateId()==$template->getId())
        if($invoice->getStatus()!="Cancelled")
        if($invoice->getHidden()==0)//if not hidden
      {
        $this->productcategorytotal+=$invoice->getTotalForProductCategory();
        $this->noproductcategorytotal+=$invoice->getTotalForNoProductCategory();
      }
    }
    $this->total=$this->productcategorytotal+$this->noproductcategorytotal;

  }
/*
 public function executeDsrmulti(sfWebRequest $request)
 {
 $requestparams=$request->getParameter("invoice");
 $day=$requestparams["date"]["day"];
 $month=$requestparams["date"]["month"];
 $year=$requestparams["date"]["year"];

 $invoice=new Invoice();
 if(!$day or !$month or !$year)
 $invoice->setDate(MyDate::today());
 else
 $invoice->setDate($year."-".$month."-".$day);

 $requestparams=$request->getParameter("purchase");
 $day=$requestparams["date"]["day"];
 $month=$requestparams["date"]["month"];
 $year=$requestparams["date"]["year"];
 $purchase=new Purchase();
 if(!$day or !$month or !$year)
 $purchase->setDate(MyDate::today());
 else
 $purchase->setDate($year."-".$month."-".$day);

 $this->form=new InvoiceForm($invoice);
 $this->toform=new PurchaseForm($purchase);

 $this->invoices = InvoiceTable::fetchByDateRange($invoice->getDate(),$purchase->getDate());
 $this->events = EventTable::fetchByDatenParentclass($invoice->getDate(),"Invoice");

 $this->cashsales=0;
 $this->chequesales=0;
 $this->creditsales=0;
 $this->cashother=0;
 $this->chequeother=0;
 $this->creditother=0;
 $this->cashtotal=0;
 $this->chequetotal=0;
 $this->credittotal=0;
 $this->deducttotal=0;
 foreach($this->invoices as $invoice)if($invoice->getStatus()!="Cancelled")
 {
 $this->cashsales+=$invoice->getCash();
 $this->chequesales+=$invoice->getChequeamt();
 $this->creditsales+=$invoice->getCredit();
 $this->cashtotal+=$invoice->getCash();
 $this->chequetotal+=$invoice->getChequeamt();
 $this->credittotal+=$invoice->getCredit();
 $this->deducttotal+=$invoice->getDsrdeduction();
 }
 foreach($this->events as $event)
 {
 $invoice=$event->getParent();
 if($invoice->getStatus()!="Cancelled")
 {
 $this->cashother+=$event->getDetail("cashamt");
 $this->chequeother+=$event->getDetail("chequeamt");
 $this->creditother+=$event->getDetail("creditamt");
 $this->cashtotal+=$event->getDetail("cashamt");
 $this->chequetotal+=$event->getDetail("chequeamt");
 $this->credittotal+=$event->getDetail("creditamt");
 $this->deducttotal+=$event->getDetail3();
 }
 }
 $this->total=$this->cashtotal+$this->chequetotal+$this->credittotal;
 }
 */


  public function executeCommission(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    //if no date, date is today. else date is date given
    if(!$year or !$month or !$day)$date=MyDateTime::today();
    else $date=new MyDateTime($year,$month,$day,0,0,0);

    $invoice=new Invoice();
    $invoice->setDate($date->getstartofmonth()->tomysql());
    $purchase=new Purchase();
    $purchase->setDate($date->getendofmonth()->tomysql());

    $this->date=$date;
    $this->form=new InvoiceForm($invoice);
    $this->toform=new PurchaseForm($purchase);

    //set up an array of employees indexed by employee id
    $this->rawemployees=Doctrine_Query::create()
        ->from('Employee e')
      	->where('e.commission > 0 ')
      	->execute();
    $this->employees=array();
    foreach($this->rawemployees as $employee)
    {
      $this->employees[$employee->getId()]=$employee;
    }

    $this->empinvoices = array();
    foreach($this->employees as $employee)
      $this->empinvoices[$employee->getId()]=Doctrine_Query::create()
        ->from('Invoice i, i.Employee e')
        ->where('i.salesman_id='.$employee->getId())
      	->andwhere('i.date >= "'.$invoice->getDate().'"')
      	->andwhere('i.date <=  "'.$purchase->getDate().'"')
        ->orWhere('i.technician_id='.$employee->getId())
      	->andwhere('i.date >= "'.$invoice->getDate().'"')
      	->andwhere('i.date <=  "'.$purchase->getDate().'"')
      	->orderBy('i.invno')
      	->execute();

    $commissiontotals=array();
    foreach($this->empinvoices as $empid=>$employeedata)
    {
      $totalcommission=0;
      foreach($employeedata as $invoice)
      {
        if($invoice->getStatus()=="Paid")
        if($invoice->getIsTemporary()==0)
        	$totalcommission+=$invoice->getCommissionTotal($this->employees[$empid]);
      }
      $commissiontotals[$empid]=$totalcommission;
    }
    $this->commissiontotals=$commissiontotals;
    $this->grandtotalcommission=0;
    foreach($commissiontotals as $total)
    {
      $this->grandtotalcommission+=$total;
    }

    $this->templates=Doctrine_Query::create()
      ->from('InvoiceTemplate t')
      ->orderBy('id')
    	->execute();
  }
  public function executeCommissionGrossSaleSave(sfWebRequest $request)
  {
    $employee_id=$request->getParameter('employee_id');
    // $start_date=$request->getParameter('start_date');
    $end_date=$request->getParameter('end_date');
    $inv_ids=$request->getParameter('ids');
    $commission_payment_name=$request->getParameter('commission_payment_name');
    $notes=$request->getParameter('notes');
    
    //check if commission payment name exists
    if($commission_payment_name==null or $commission_payment_name=="")
      return $this->redirect("home/error?msg=Commission payment name is required");

    //check if commission payment name is duplicate
    $duplicate=Doctrine_Query::create()->from('CommissionPayment cp')
    ->where('cp.name = "'.$commission_payment_name.'"')->fetchOne();
    if($duplicate!=null)
    return $this->redirect("home/error?msg=Commission payment name is taken");

    $employee=Doctrine_Query::create()
    ->from('Employee e')
    ->where('e.id = '.$employee_id)
    ->fetchOne();
    if($employee==null)
    {
      $this->redirect("home/error?msg=Salesman not found");
    }


    $this->commission_payment = new CommissionPayment();
    $this->commission_payment->setName($commission_payment_name);
    $this->commission_payment->setEmployeeId($employee_id);
    // $this->commission_payment->setStartdate($start_date);
    $this->commission_payment->setEnddate($end_date);
    $this->commission_payment->setDateCreated(MyDateTime::today()->tomysql());
    $this->commission_payment->setDatePaid(MyDateTime::today()->tomysql());
    $this->commission_payment->setNotes($notes);
    $this->commission_payment->setType("Gross");
    $this->commission_payment->setRate1($this->commission_payment->getEmployee()->getCommission()/1000);
    $this->commission_payment->save();
    
    $this->cp=$this->commission_payment;

    // $this->invnos=explode("\n",$request->getParameter('invnos'));
    $this->notfound=array();
    $this->taken=array();
    $this->belongs=array();
    $this->added=array();
    $this->mismatch=array();

    //for each invno, load invoice and run tests, then add to commission payment
    foreach($inv_ids as $inv_id)
    {
      $invoice = Fetcher::fetchOne("Invoice",array("id"=>$inv_id));
      $invoice_name=$invoice->getInvoiceTemplate()." ".$invoice->getInvno();

      //if invoice not found
      if($invoice==null)
      {
        $this->notfound[]="Invoice with id $inv_id not found";
        continue;
      }

      //invoice already belongs to a commission payment 
      if($invoice->getCommissionPaymentId()!=null)
      {
        if($invoice->getCommissionPaymentId()==$this->cp->getId())
          $this->belongs[]="$invoice_name already belongs to this commission payment";
        else
          $this->taken[]="$invoice_name already belongs to commission payment ".$invoice->getCommissionPaymentId();
        continue;
      }
      $invoice->setCommissionPaymentId($this->cp->getId());
      $invoice->save();
      $this->added[]="$invoice_name added";
    }
    $this->commission_payment->recalculate();
  }
  public function executeCommissionGrossSale(sfWebRequest $request)
  {
    //process date
    /*
    $requestparams=$request->getParameter("invoice");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $invoice=new Invoice();
    if($request->hasParameter("startdate"))
      $invoice->setDate($request->getParameter("startdate"));
    elseif($day and $month and $year)
      $invoice->setDate($year."-".$month."-".$day);
    else
      $invoice->setDate(MyDateTime::today()->getStartOfMonth()->tomysql());
    */

    $requestparams=$request->getParameter("purchase");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $purchase=new Purchase();
    if($request->hasParameter("enddate"))
      $purchase->setDate($request->getParameter("enddate"));
    elseif($day and $month and $year)
      $purchase->setDate($year."-".$month."-".$day);
    else
      $purchase->setDate(MyDateTime::today()->getEndOfMonth()->tomysql());

    //$this->form=new InvoiceForm($invoice);
    $this->toform=new PurchaseForm($purchase);
    //$this->invoice=$invoice;
    $this->purchase=$purchase;
    //end process date

    //set up an array of employees indexed by employee id
    $this->employee_id=$request->getParameter("employee_id");
    $this->employees=array();//this will contain only 1 employee
    if($this->employee_id)
      $this->employees[$this->employee_id]=Fetcher::fetchOne("Employee",array("id"=>$this->employee_id));

    $this->empinvoices = array();
    foreach($this->employees as $employee)
      $this->empinvoices[$employee->getId()]=Doctrine_Query::create()
        ->from('Invoice i, i.Customer c')
        ->where('i.salesman_id='.$employee->getId())
      	//->andwhere('i.date >= "'.$invoice->getDate().'"')
      	->andwhere('i.date <=  "'.$purchase->getDate().'"')
        ->orWhere('i.technician_id='.$employee->getId())
      	//->andwhere('i.date >= "'.$invoice->getDate().'"')
      	->andwhere('i.date <=  "'.$purchase->getDate().'"')
      	->orderBy('c.name, i.date')
      	->execute();

    $commissiontotals=array();
    foreach($this->empinvoices as $empid=>$employeedata)
    {
      $totalcommission=0;
      foreach($employeedata as $invoice)
      {
        if($invoice->getStatus()=="Paid")
        if($invoice->getIsTemporary()==0)
        	$totalcommission+=$invoice->getCommissionTotal($this->employees[$empid]);
      }
      $commissiontotals[$empid]=$totalcommission;
    }
    $this->commissiontotals=$commissiontotals;
    $this->grandtotalcommission=0;
    foreach($commissiontotals as $total)
    {
      $this->grandtotalcommission+=$total;
    }
  }
  public function executeCommissionpdf(sfWebRequest $request)
  {
    $this->executeCommission($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeCancel(sfWebRequest $request)
  {
    $this->invoice = $this->getRoute()->getObject();

    //if drs exist for this invoice
    //retained after dr_less_sales for backwards compatibility of foreign keys
    $deliverys=Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.ref_class = "invoice"')
      ->andWhere('d.ref_id = '.$this->invoice->getId())
      ->andWhere('d.status != "Cancelled"')
    	->execute()
      ;
    foreach($deliverys as $delivery)
      $delivery->cascadeDelete();
    /*
    if(count($deliverys)>0)
    {
      $this->getUser()->setFlash('error', "Invoice not cancelled: Please cancel delivery receipts first");
      return $this->redirect('invoice/delivery?id='.$this->invoice->getId());
    }
    */

    $this->invoice->cascadeCancel();
    $this->invoice->calc();

    $this->redirect("invoice/view?id=".$this->invoice->getId());

    /*
			Docu for cancel
				on cancel
					call family of data objects, tell them to cancel in their own way


				after cancel:
					invoice still shows cancelled events and details
					invoice must not be able to create events.
					invoicedetails must not create quotes or stockentries.

    */
  }
  public function executeUndocancel(sfWebRequest $request)
  {
    $invoice = $this->getRoute()->getObject();
    $invoice->setStatus("");
    $invoice->calc();
    $invoice->save();
    
    //update generate conversion stock entries
    foreach($invoice->getDeliveryconversion() as $detail)
    {
      $detail->setQty($detail->getRef()->getQty());
      $detail->save();
      $detail->cascadeUpdateDeliverydetailQtys();
    }

    //only closed invoices have stock entries. closed means is_temporary=0
    if($invoice->getIsTemporary()==0)
        foreach($invoice->getInvoicedetail() as $detail)
    {
			$detail->updateStockentry();
    }
    //I forgot how to regenerate the quotes

    $this->redirect("invoice/view?id=".$invoice->getId());
  }
  public function executeView(sfWebRequest $request)
  {
    $this->forward404Unless(
      $this->invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Employee e, i.Customer c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));


    $this->form = $this->configuration->getForm($this->invoice);
    $this->customerform = new CustomerForm($this->invoice->getCustomer());

    //allow set product id by url
    $detail=new Invoicedetail();
    $detail->setQty(1);
    $this->product_is_set=false;
    $this->product=null;
    if($request->getParameter("product_id"))
    {
      $detail->setProductId($request->getParameter("product_id"));
      $this->product=$detail->getProduct();
      $this->product_is_set=true;
    }
    $this->detailform = new InvoicedetailForm($detail);
    $this->detail = $detail;
    $this->searchstring=$this->request->getParameter('searchstring');
    
    //force commission column to appear,
    //but 1 time only
    $this->commissionOn=false;
    if($request->getParameter("commission")=="on")
      $this->commissionOn=true;
      
    $this->check=$this->invoice->getInCheck();
    if($this->check != null and $this->check->getId()==null)
      $this->check=null;

    $this->checks=$this->invoice->getCustomer()->getInCheck();
    $this->checksRemainingTotal=0;
    foreach($this->checks as $check)$this->checksRemainingTotal+=$check->getRemaining();
  }
  public function executeViewPrintable(sfWebRequest $request)
  {
    $this->forward404Unless(
      $invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Employee e, i.Customer c, i.Invoicedetail id')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

    $content=MyInvoiceHelper::PrintableInvoiceBig($invoice);
      
    $response = $this->getResponse();
    $response->clearHttpHeaders();
    //$response->setContentType($mimeType);
    $response->setHttpHeader('Content-Disposition', 'attachment; filename="' . basename(str_replace(" ","",$invoice->getInvoiceTemplate()).$invoice->getInvno().".txt") . '"');
    $response->setHttpHeader('Content-Description', 'File Transfer');
    $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
    $response->setHttpHeader('Content-Length', 80*39);
    $response->setHttpHeader('Cache-Control', 'public, must-revalidate');
    $response->setHttpHeader('Pragma', 'public');
    $response->setContent($content);
    $response->sendHttpHeaders();

    sfConfig::set('sf_web_debug', false);
    return sfView::NONE;
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);
    //$this->message=$request->getParameter("message");

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeRecalc(sfWebRequest $request)
  {
    $invoice=$this->getRoute()->getObject();
    $invoice->calc();
    $invoice->save();
    foreach($invoice->getInvoiceDr() as $dr)
      $dr->calc();
    foreach($invoice->getInvoicedetail() as $detail)
      $detail->calcDr();
    $this->redirect($request->getReferer());
  }
  public function executeAccounting(sfWebRequest $request)
  {
    $this->forward404Unless(
    $this->invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Employee e, i.Customer c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));
    $this->form = $this->configuration->getForm($this->invoice);

    $this->accountentries=$this->invoice->getAccountentries(true);
    $this->totalsbyaccount=array();

    foreach($this->invoice->getAccountids() as $id)
    {
      $this->totalsbyaccount[$id]=0;
    }
    foreach($this->accountentries as $entry)
    {
      $this->totalsbyaccount[$entry->getAccountId()]+=$entry->getQty();
    }
  }
  public function executeEvents(sfWebRequest $request)
  {
    $this->forward404Unless(
    $this->invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Employee e, i.Customer c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));
    $this->form = $this->configuration->getForm($this->invoice);
  }
  public function executeDiscount(sfWebRequest $request)
  {
    $this->forward404Unless(
    $this->invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Employee e, i.Customer c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));
    $this->form = $this->configuration->getForm($this->invoice);
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->invoice = new Invoice();
    $this->invoice->setTransactionCode(InvoiceTable::genTransactionCode());
    $this->invoice->setDate(MyDate::today());
    $this->invoice->setDatereleased(MyDate::today());
    $this->invoice->setTemplateId(10);
    
    //auto set employee
    $employee=Doctrine_Query::create()
    ->from('Employee e')
  	->where('e.username = "'.$this->getUser()->getUsername().'"')
  	->fetchOne();
  	if($employee)
	    $this->invoice->setSalesmanId($employee->getId());
  	else
    		$this->invoice->setSalesmanId(SettingsTable::fetch("default_salesman_id"));
    		
    $this->invoice->setIsTemporary(2);

    //set invoice no if param invno is present
    if($request->getParameter("invno"))
    {
      $this->invoice->setInvno($request->getParameter("invno"));
    }

    //set customer if param customer_id
    if($request->getParameter("customer_id"))
    {
      $this->invoice->setCustomerId($request->getParameter("customer_id"));
      $this->invoice->setDiscountLevelId($this->invoice->getCustomer()->getDiscountLevelId());
      $this->invoice->setMaxDiscountLevelId($this->invoice->getCustomer()->getMaxDiscountLevelId());
    }
    else
    {
      $this->invoice->setCustomerId(SettingsTable::fetch('default_customer_id'));
      $this->invoice->setDiscountLevelId(1);
      $this->invoice->setMaxDiscountLevelId(2);
    }
    
    //set customer name if param customer_name
    if($request->getParameter("customer_name"))
    {
      $this->invoice->setCustomerId(1);
      $this->invoice->setCustomerName($request->getParameter("customer_name"));
    }
    
    $this->invoice->setTerms($this->invoice->getCustomer()->getTerms());
    $this->invoice->calcDueDate();
    
    $this->form = $this->configuration->getForm($this->invoice);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $isnew=$form->getObject()->isNew();
    $requestparams=$request->getParameter($form->getName());
    $requestparams['invno']=trim($requestparams['invno']);

    //if invno is empty, generate from username and time    
    if($requestparams['invno']=="")
    {
      $requestparams['invno']=$this->getUser()->getUsername()." ".date('h:i:s a');
    }
    else if($isnew)
    {
      $duplicate=Fetcher::fetchOne("Invoice",array("invno"=>"\"".$requestparams["invno"]."\""));
      if($duplicate)
      {
        return $this->redirect('invoice/duplicateError?id='.$duplicate->getId());
      }
    }

    //if discount level is wholesale and salesman is not wholesale, error
    $employee=Fetcher::fetchOne("Employee",array("id"=>$requestparams['salesman_id']));
    $discount_level=Fetcher::fetchOne("DiscountLevel",array("id"=>$requestparams['discount_level_id']));
    if($discount_level->getIsWholesale() && !$employee->getIsWholesale())
    {
          $message="Cannot assign Wholesale Discount Level, please select Wholesale Salesman";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
    }
    $max_discount_level=Fetcher::fetchOne("MaxDiscountLevel",array("id"=>$requestparams['max_discount_level_id']));
    if($max_discount_level->getIsWholesale() && !$employee->getIsWholesale())
    {
          $message="Cannot assign Wholesale Max Discount Level, please select Wholesale Salesman";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
    }
    if($max_discount_level->getTotalDiscount() < $discount_level->getTotalDiscount())
    {
          $message="Max Discount Level cannot be less than Discount Level";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
    }
    
    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        //set date and adjust stock entry date if necessary
        //only if invoice is not new (action is edit)
        // if(!$isnew)$form->getObject()->setDateAndUpdateStockEntry($requestparams["date"]["year"]."-".$requestparams["date"]["month"]."-".$requestparams["date"]["day"]);

        //save this for later use
        $oldistemporary=$form->getObject()->getIsTemporary();
        
        $invoice = $form->save();
        
        //save this for later use
        $newistemporary=$invoice->getIsTemporary();

        if(!$invoice->getCheckcleardate())$invoice->setCheckcleardate($invoice->getDate());

        $invoice->calc();
        $invoice->calcDueDate();
        $invoice->getUpdateChequedata();
        $invoice->save();
        $invoice->genCustomer();
        
        /*
        //update stock entries if is_temporary changed from not closed to closed or vice versa
        //determine if is_temporary changed 
        //closed = 0
        //narrow down values (remove 1) to make it easier to determine
        if($oldistemporary==1)$oldistemporary=2;
        if($newistemporary==1)$newistemporary=2;
        //if it changed
        if($oldistemporary!=$newistemporary)
        {
            //if closed
            if($newistemporary==0)
            {
                foreach($invoice->getInvoicedetail() as $detail)
                {
			$detail->updateStockentry();
                }
            }
            //else if not closed
            else
            {
                foreach($invoice->getInvoicedetail() as $detail)
                {
                    $detail->getStockentry()->delete();
                }
            }
        }
        */
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $invoice)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect('invoice/view?id='.$invoice->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeDsrpdf(sfWebRequest $request)
  {
    $this->executeDsr($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeDsrmultipdf(sfWebRequest $request)
  {
    $this->executeDsrmulti($request);
    //$this->document = $this->getRoute()->getObject();
    //$this->form = $this->configuration->getForm($this->incoming);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeSearch(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()
        ->from('Invoice i')
      	->where('i.invno = "'.trim($request->getParameter("searchstring")).'"')
      	->orWhere('i.transaction_code   like "%'.trim($request->getParameter("searchstring")).'%"')
      	->orWhere('i.ponumber   like "%'.trim($request->getParameter("searchstring")).'%"')
      	->orWhere('i.notes      like "%'.trim($request->getParameter("searchstring")).'%"')
//      	->orWhere('i.chequedata like "%'.trim($request->getParameter("searchstring")).'%"')
//      	->orWhere('i.cheque     like "%'.trim($request->getParameter("searchstring")).'%"')
      ;
    $this->invoices=$query->execute();
    $this->searchstring=$request->getParameter("searchstring");

    /*
    //if there is only 1 result, show page
  	if(count($this->invoices)==1)
  	{
			$this->invoice=$this->invoices[0];
  		$this->redirect("invoice/view?id=".$this->invoice->getId());
  	}
  	elseif(count($this->invoices)>1)
    */

  	//if there are any results, even just 1, display as list
  	//so that if it's not the one you want, you can create new 
  	if(count($this->invoices)>0)
  	{
  	  //nothing left to do, just let the page be displayed
  	}
  	//no search result found
  	else
  		$this->redirect("invoice/new?invno=".$request->getParameter("searchstring"));


  }
    public function executeHide(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Employee e, i.Customer c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->setHidden(1);
        $this->invoice->save();

        $this->redirect($request->getReferer());
    }

    public function executeUnhide(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Employee e, i.Customer c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->setHidden(0);
        $this->invoice->save();

        $this->redirect($request->getReferer());
    }
    public function executeInspect(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->setIsInspected(1);
        $this->invoice->save();

        $this->redirect($request->getReferer());
    }
    public function executeUninspect(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->setIsInspected(0);
        $this->invoice->save();

        $this->redirect($request->getReferer());
    }
    //this is where stock entries for each invoice detail get created
    public function executeFinalize(sfWebRequest $request)
    {
	    $requestparams=$request->getParameter('invoice');
	    $id=$requestparams['id'];
	    $invno=trim($requestparams['invno']);
	    
	    //validation
	    //if template is invoice
	    $template=Fetcher::fetchById("InvoiceTemplate",$requestparams['template_id']);
	    //if($template->getIsInvoice() or $template->getIsDr())
	    if($template->getId()!=10)
	    {
	      //enforce proper invoice number
	      if(strpos($invno," ")!=false or strpos($invno,":")!=false)
	      {
              $message="Invalid invoice number";
              $this->getUser()->setFlash('error', $message);
              return $this->redirect($request->getReferer());
	      }
	      
	      //validate invno unique
          $duplicateinvoice=Doctrine_Query::create()
          ->from('Invoice i')
        	->where('i.invno = "'.$invno.'"')
        	->andWhere('i.id != "'.$id.'"')
        	->fetchOne();
        	if($duplicateinvoice!=false)
        	{
        		//new invno not unique: error msg and quit
              $message="Invoice ".$invno." already exists";
              $this->getUser()->setFlash('error', $message);
              return $this->redirect($request->getReferer());
        	}
	    }
	    
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
      	->where('i.id = '.$id)
      	->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

      	//validate invoice is not cancelled
      	if($this->invoice->getStatus()=="Cancelled")
      	{
            $message="Invoice ".$invno." cannot be finalized because it is already cancelled.";
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
      	}

	    //if template is 'transaction'
	    //use invoice id as transaction no.
	    if($template->getId()==10 and strpos($this->invoice->getInvno(), " "))
	    {
	      //$invno=str_pad($this->invoice->getId(), 6, "0", STR_PAD_LEFT);
	      $invno=$this->invoice->getTransactionCode();
	    }

        $this->invoice->setIsTemporary(0);
        $this->invoice->setTemplateId($requestparams['template_id']);
        $this->invoice->setInvno($invno);
        $this->invoice->setWasClosed(1);
        $this->invoice->calc();
        $this->invoice->calcForProductCategories();
        $this->invoice->save();
        
        //-----------dr_less_sales: disable dr generation-----------
        
        //update generate conversion stock entries
        foreach($this->invoice->getDeliveryconversion() as $detail)
        {
          $detail->setQty($detail->getRef()->getQty());
          $detail->save();
          $detail->cascadeUpdateDeliverydetailQtys();
        }

        if($this->invoice->isDrBased())
        {
          //todo
          /*
on invoice close 
    if no unreleased exists, stay in invoice page
    if unreleased exists
        if no dr, generate
            all qtys = unreleased
        if unreleased dr exist, update
        if no unreleased dr, generate
if returns exist, show as negative
 
           */
          //DR is required if remaining != 0
          //remaining = qty != qty_released
          //at first finalize, qty_released==0
          //and remaining = qty
          if($this->invoice->checkDrRequired())
          {
            //generate a dr and show it for releasing
            $invoice_dr = InvoiceDrTable::genDrForInvoiceId($this->invoice->getId(),$this->getUser()->getGuardUser());
            return $this->redirect("invoice_dr/view?id=".$invoice_dr->getId());
          }
          //else if dr not required
          else
          {
            return $this->redirect($request->getReferer());
          }
        }
        //else invoice is not dr based
        else
        {
          //generate stock entries
          foreach($this->invoice->getInvoicedetail() as $detail)
            $detail->updateStockentry();

          return $this->redirect($request->getReferer());
        }
        
        
        //$delivery=$this->invoice->genDelivery();    
        //$this->getUser()->setFlash('notice', "Successfully generated delivery receipt");
        //$this->redirect('delivery/view?id='.$delivery->getId());

        //---------end dr_less_sales: disable dr generation------------
    }
  public function executeList(sfWebRequest $request)
  {
    //process date parameter
    //and store it in $invoice->date
    $requestparams=$request->getParameter("invoice");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $this->invoice=new Invoice();
    if(!$day or !$month or !$year)
      $this->invoice->setDate(MyDate::today());
    else
      $this->invoice->setDate($year."-".$month."-".$day);
    $this->form=new InvoiceForm($this->invoice);

    //if parameter "all" (meaning all dates) is set
    //get all temporary invoices regardless of date
    if($request->getParameter("all"))
      $this->invoices=Doctrine_Query::create()
        ->from('Invoice i')
      	->where('is_temporary!=0')
        ->execute();
    //else fetch all invoices 
    //for specified date
    else
      $this->invoices=Doctrine_Query::create()
        ->from('Invoice i')
        ->andWhere('date="'.$this->invoice->getDate().'"')
        ->execute();
        
    $this->purchases=Doctrine_Query::create()
      ->from('Purchase p')
    	->where('p.invno = ""')
      ->andWhere('p.date="'.$this->invoice->getDate().'"')
    	->execute();
  
    //this is for unreceived or not yet arrived purchases
    $this->unreceivedpurchases=Doctrine_Query::create()
        ->from('Purchase p')
      	->where('received_status!="Fully Received"')
      	->andWhere('status!="Cancelled"')
      	->orderBy('p.vendor_id desc, p.date desc')
      	->limit(20)
      	->execute();

  	//put vendors into an array, sorted by id
  	$this->vendors=array();
  	$this->vendorpurchases=array();
  	foreach($this->purchases as $purchase)
  	{
      if(!isset($this->vendors[$purchase->getVendorId()]))
    	  $this->vendors[$purchase->getVendorId()]=$purchase->getVendor();
    	//put purchases into a 2d array under the same id as the vendor
  	  $this->vendorpurchases[$purchase->getVendorId()][]=$purchase;
  	}

    $this->today=$this->invoice->getDate();
    $this->oneyearago=MyDateTime::frommysql($this->today)->adddays(-365)->tomysql();
    $this->unpaidinvoicestotaloneyear = InvoiceTable::totalUnpaidByDateRange($this->oneyearago, $this->today);

    if($this->getUser()->hasCredential(array('admin','encoder'),false))
      $this->setTemplate("listforapproval");
    else if($this->getUser()->hasCredential(array('sales'),false))
      $this->setTemplate("listnew");
    else if($this->getUser()->hasCredential(array('cashier'),false))
      $this->setTemplate("listcheckedout");
    else
      $this->redirect('home/error?msg=You do not have the necessary credentials to view this page');
  }
  public function executeListunpaid(sfWebRequest $request)
  {
    $this->invoices=Doctrine_Query::create()
        ->from('Invoice i, i.Customer c')
      	->where('status!="Paid"')
      	->andWhere('status!="Cancelled"')
      	->andWhere('hidden=0')
      	->andWhere('is_temporary=0')
      	->orderBy('i.customer_id, i.date')
      	//->limit(10)
      	->execute();
  	
  	//put customers into an array, sorted by id
  	$this->customers=array();
  	$this->customerinvoices=array();
  	$this->customertotals=array();
  	$this->customerpumptotals=array();
  	$this->customertanktotals=array();
  	$this->customerduetotals=array();
  	foreach($this->invoices as $invoice)
  	{
  	  $this->customers[$invoice->getCustomerId()]=$invoice->getCustomer();
    	//put invoices into a 2d array under the same id as the customer
      if(!isset($this->customerinvoices[$invoice->getCustomerId()]))
      {
    	  $this->customerinvoices[$invoice->getCustomerId()]=array();
      	$this->customertotals[$invoice->getCustomerId()]=0;
      	$this->customerpumptotals[$invoice->getCustomerId()]=0;
      	$this->customertanktotals[$invoice->getCustomerId()]=0;
      	$this->customerduetotals[$invoice->getCustomerId()]=0;
      }
  	  $this->customerinvoices[$invoice->getCustomerId()][]=$invoice;
    	$this->customertotals[$invoice->getCustomerId()]+=$invoice->getBalance();
      $this->customerpumptotals[$invoice->getCustomerId()]+=$invoice->getTotalForPumps();
      $this->customertanktotals[$invoice->getCustomerId()]+=$invoice->getTotalForTanks();
    if($invoice->isDue())
      	$this->customerduetotals[$invoice->getCustomerId()]+=$invoice->getBalance();
  	}
  	
  }

  function cmpByProductName($a, $b)
  {
      return strcmp($a["name"], $b["name"]);
  }

  public function executeListunpaidByProduct(sfWebRequest $request)
  {
    $this->invoices=Doctrine_Query::create()
        ->from('Invoice i, i.Customer c')
      	->where('status!="Paid"')
      	->andWhere('status!="Cancelled"')
      	->andWhere('hidden=0')
      	->andWhere('is_temporary=0')
      	->orderBy('i.customer_id, i.date')
      	//->limit(10)
      	->execute();
  	
  	//put customers into an array, sorted by id
  	$this->products=array();
    $this->total=0;
  	// $this->customerinvoices=array();
  	// $this->customertotals=array();
  	// $this->customerduetotals=array();
  	foreach($this->invoices as $invoice)
  	{
      $customername=$invoice->getCustomer()->getName();
      if($customername=="TMC CDO")continue;
      if($customername=="CDO Warehouse")continue;
      
      foreach($invoice->getInvoicedetail() as $detail)
      {
        if(!isset($this->products[$detail->getProductId()]))
        {
          $product=$detail->getProduct();
          $array=array();
          $array["id"]=$product->getId();
          $array["name"]=$product->getName();
          $array["unpaidtotal"]=0;
          $this->products[$detail->getProductId()]=$array;
        }
        $this->products[$detail->getProductId()]["unpaidtotal"]+=$detail->getTotal();
        $this->total+=$detail->getTotal();
      }

    }
    usort($this->products, "self::cmpByProductName");
  }

    public function executeSetinvno(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->setTemplateId($request->getParameter('template_id'));
        $this->invoice->setInvno($request->getParameter('invno'));
        $this->invoice->save();

        $this->redirect($request->getReferer());
    }
    public function executePartialSaleType(sfWebRequest $request)
    {
        $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter('id')));    
        $this->form=new InvoiceForm($this->invoice);
    }
    public function executeProcessPartialSaleType(sfWebRequest $request)
    {
        $requestparams=$request->getParameter('invoice');
        $cash=$requestparams["cash"];
        $chequeamt=$requestparams["chequeamt"];

        $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$requestparams["id"]));    

        //validate---------------------
        if(!is_numeric($cash))
        {
            $message="Invalid Cash value";
            $this->getUser()->setFlash('error', $message);
        		return $this->redirect($request->getReferer());
        }
        if(!is_numeric($chequeamt))
        {
            $message="Invalid Check Amount value";
            $this->getUser()->setFlash('error', $message);
        		return $this->redirect($request->getReferer());
        }
        if($cash+$chequeamt<=0)
        {
            $message="Invalid Input";
            $this->getUser()->setFlash('error', $message);
        		return $this->redirect($request->getReferer());
        }
    
        //if check exists, remove
        $check=$this->invoice->getInCheck();
        if($check!=null)
          $check->removeInvoice($this->invoice);

        //pretend check creation failed, set cash value
        if($cash==0)
          $this->invoice->setSaleType("Account");
        else
          $this->invoice->setSaleType("Partial");
          
        $this->invoice->setCash($cash);

        $this->invoice->calc();
        $this->invoice->save();

        if($chequeamt>0)
          $this->redirect("in_check/new?invoice_based=true&chequeamt=".$chequeamt."&invoice_id=".$this->invoice->getId());
        else        
          $this->redirect("invoice/view?id=".$this->invoice->getId());
    }
    public function executeAdjustSaleTypeAccount(sfWebRequest $request)
    {
        $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter('id')));    
    
        $this->invoice->setSaleType("Account");

        //if check exists, remove
        $check=$this->invoice->getInCheck();
        if($check!=null)
          $check->removeInvoice($this->invoice);

        $this->invoice->calc();
        $this->invoice->save();

        $this->redirect($request->getReferer());
    }
    public function executeAdjustSaleTypeCheck(sfWebRequest $request)
    {
        $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter('id')));    
    
        $this->invoice->setSaleType("Account");

        //if check exists, remove
        $check=$this->invoice->getInCheck();
        if($check!=null)
          $check->removeInvoice($this->invoice);

        $this->invoice->calc();
        $this->invoice->save();

        $this->redirect("in_check/new?invoice_based=true&invoice_id=".$this->invoice->getId());
    }
    public function executeAdjustSaleTypeCash(sfWebRequest $request)
    {
        $requestparams=$request->getParameter('invoice');

        $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter('id')));

        $this->invoice->setSaleType("Cash");

        //if check exists, remove
        $check=$this->invoice->getInCheck();
        if($check!=null)
          $check->removeInvoice($this->invoice);
          
        $this->invoice->calc();
        $this->invoice->save();

        $this->redirect($request->getReferer());
    }
    /*
    public function executeAdjustSaleType(sfWebRequest $request)
    {
        $requestparams=$request->getParameter('invoice');
        $id=$requestparams['id'];
    
        $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$id));    
    
      //$this->invoice->setSaleType($requestparams['saletype']);
      $this->invoice->setSaleType("Partial");
      $this->invoice->setCash($requestparams["cash"]);
      //$this->invoice->setChequeamt($requestparams["chequeamt"]);
      //$this->invoice->setCredit($requestparams["credit"]);

        $this->invoice->calc();
        $this->invoice->save();

        $this->redirect($request->getReferer());
    }
    */
    public function executeAdjust(sfWebRequest $request)
    {
        $requestparams=$request->getParameter('invoice');
        $id=$requestparams['id'];
    
        $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$id));    

        if(isset($requestparams["collection_status"]))
          $this->invoice->setCollectionStatus($requestparams["collection_status"]);
        if(isset($requestparams["ponumber"]))
          $this->invoice->setPonumber($requestparams["ponumber"]);
        if(isset($requestparams["notes"]))
          $this->invoice->setNotes($requestparams["notes"]);
        if(isset($requestparams["technician_id"]))
          $this->invoice->setTechnicianId($requestparams["technician_id"]);
        if(isset($requestparams["salesman_id"]))
          $this->invoice->setSalesmanId($requestparams["salesman_id"]);

        if(isset($requestparams["date"]["year"]))
        {
          $date=
            str_pad($requestparams["date"]["year"], 2, "0", STR_PAD_LEFT)
            ."-".
            str_pad($requestparams["date"]["month"], 2, "0", STR_PAD_LEFT)
            ."-".
            str_pad($requestparams["date"]["day"], 2, "0", STR_PAD_LEFT)
            ;

          $this->invoice->setDate($date);
        
          if(isset($requestparams["datereleased"]["year"]))
          {
            $datereleased=
              str_pad($requestparams["datereleased"]["year"], 2, "0", STR_PAD_LEFT)
              ."-".
              str_pad($requestparams["datereleased"]["month"], 2, "0", STR_PAD_LEFT)
              ."-".
              str_pad($requestparams["datereleased"]["day"], 2, "0", STR_PAD_LEFT)
              ;
            
/*
            //THIS FEATURE DISABLED
            //because datereceived was removed (just hidden really)
            //this is meant to maintain relationship of date to datereceived
            //if date and datereceived are the same
            //and date is changed
            
            //if date changed but date received not changed, 
            //if date and date received are previously the same,
            //make date received equal to date
            if($this->invoice->getDate()!=$date)
            if($this->invoice->getDatereleased()==$datereleased)
            if($this->invoice->getDatereleased()==$this->invoice->getDate())
            {
*/        
              $datereleased=$date;
/*          
            }
*/

            //set date and adjust stock entry date if necessary
            $this->invoice->setDateAndUpdateStockEntry($datereleased);
          }
          
        }

        if(isset($requestparams["terms"]) 
          and $this->invoice->getTerms()!=$requestparams["terms"]
          )
        {
          $this->invoice->setTerms($requestparams["terms"]);
          $this->invoice->calcDueDate();
        }

        $this->invoice->calc();
        $this->invoice->save();

        if($this->request->getParameter("source")=="discount")
          $this->redirect("invoice/view?id=".$this->invoice->getId());
        else
          $this->redirect($request->getReferer());
    }
    public function executeAdjustDiscountLevel(sfWebRequest $request)
    {
      $requestparams=$request->getParameter('invoice');
      $id=$requestparams['id'];
  
      $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$id));    

      $discount_recalc_needed=false;
      // $old_discount_level_id=$this->invoice->getDiscountLevelId();
      // $old_discrate=$this->invoice->getDiscrate();
      if(isset($requestparams["discount_level_id"])
      // and $old_discount_level_id!=$requestparams["discount_level_id"]
      )
      {
        $this->invoice->setDiscountLevelId($requestparams["discount_level_id"]);
        $discount_recalc_needed=true;
      }
      if(isset($requestparams["max_discount_level_id"])
      // and $old_discount_level_id!=$requestparams["discount_level_id"]
      )
      {
        $this->invoice->setMaxDiscountLevelId($requestparams["max_discount_level_id"]);
        $discount_recalc_needed=true;
      }
      if(isset($requestparams["discrate"])
      // and $old_discrate!=$requestparams["discrate"]
      )
      {
        $this->invoice->setDiscrate($requestparams["discrate"]);
        $discount_recalc_needed=true;
      }

      //validation
      //if discount level is wholesale and salesman is not wholesale, error
      $employee=$this->invoice->getEmployee();
      $discount_level=$this->invoice->getDiscountLevel();
      if($discount_level->getIsWholesale() && !$employee->getIsWholesale())
      {
        $message="Cannot assign Wholesale Discount Level, please select Wholesale Salesman";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
      $max_discount_level=$this->invoice->getMaxDiscountLevel();
      if($max_discount_level->getIsWholesale() && !$employee->getIsWholesale())
      {
        $message="Cannot assign Wholesale Max Discount Level, please select Wholesale Salesman";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
      if($max_discount_level->getTotalDiscount() < $discount_level->getTotalDiscount())
      {
        $message="Max Discount Level cannot be less than Discount Level";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }



      //if discount_recalc_needed
      if($discount_recalc_needed)
      {
        foreach($this->invoice->getInvoicedetail() as $detail)
        {
          $detail->setDiscrate($this->invoice->getDiscountingRate());
          $detail->calc();
          $detail->save();
        }
      }
      $this->invoice->calc();
      $this->invoice->save();

      if($this->request->getParameter("source")=="discount")
        $this->redirect("invoice/view?id=".$this->invoice->getId());
      else
        $this->redirect($request->getReferer());
  }
    public function executeApprove(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->setIsTemporary(1);
        //$this->invoice->calc();
        $this->invoice->save();

        //$this->redirect($request->getReferer());
        if($this->getUser()->hasCredential(array('manager','sales','admin'),false))
        $this->redirect($request->getReferer());
        else
        $this->redirect("invoice/list");
    }
    public function executeDeny(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->setIsTemporary(2);
        //$this->invoice->calc();
        $this->invoice->save();

        //$this->redirect($request->getReferer());
        $this->redirect("invoice/list");
    }
    public function executeUndocheckout(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->setIsTemporary(2);
        //$this->invoice->calc();
        $this->invoice->save();

        //if it's a user requesting for undo checkout, stay in invoice page
        if($this->getUser()->hasCredential(array('sales'),false))
        $this->redirect($request->getReferer());
        else
        $this->redirect("invoice/listcheckedout");
    }
    public function executeUndoclose(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        //for non dr based invoices, stock entries are attached to invoicedetail
        //delete them whether dr based or not
        foreach($this->invoice->getInvoicedetail() as $detail)
        {
          $detail->deleteStockentry();
          if(!$this->invoice->isDrBased())
          {
            $detail->setQtyReturned(0);
            $detail->save();
          }
        }

        $this->invoice->setIsDrBased(1);
        $this->invoice->setIsTemporary(1);
        //$this->invoice->calc();
        $this->invoice->save();
        
        //update generate conversion stock entries
        foreach($this->invoice->getDeliveryconversion() as $detail)
        {
          $detail->setQty(0);
          $detail->save();
          $detail->cascadeUpdateDeliverydetailQtys();
        }

        $this->redirect($request->getReferer());
    }
    //this is Settemporarystate with validation and redirection
    public function executeCheckout(sfWebRequest $request)
    {
        $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id = '.$request->getParameter('id'))
        ->fetchOne()
        , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

        $this->invoice->calc();

        if(!sfConfig::get('custom_allow_empty_invoice'))
        //validation: must have invoicedetails and positive total
        if($this->invoice->getTotal()==0)
        {
            $message="Empty invoice";
            $this->getUser()->setFlash('error', $message);
        		return $this->redirect($request->getReferer());
        }

        $is_discounted=false;
        foreach($this->invoice->getInvoicedetail() as $detail)
        {
          if($detail->getIsDiscounted()){$is_discounted=true;break;}
        }

        if($is_discounted)
          $this->invoice->setIsTemporary(3);//for approval
        else
          $this->invoice->setIsTemporary(1);//checked out
        $this->invoice->save();

//        if($this->getUser()->hasCredential(array('sales','admin'),false))
        $this->redirect($request->getReferer());
//        else
//        $this->redirect("home/index");
    }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    $invoice=$this->getRoute()->getObject();

    if($invoice->getIsTemporary()==0)
    {
      $message="Cannot delete closed invoice";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    $returncount=count($invoice->getReturns());
    if($returncount>0)
    {
      $message="Cannot delete this Invoice because Returns exist, please delete Returns first.";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    if ($invoice->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('home/index');
  }
  public function executeBarcodeEntry(sfWebRequest $request)
  {
    $barcode=$request->getParameter("barcode");
    list($suffix,$body,$purchdetail,$product)=MyBarcode::evaluateBarcode($barcode);
    $arr = array();
    $arr[0] = $product->getId();//product_id
    $arr[1] = $product->getName();//product_name
    $arr[2] = $product->getMaxsellprice();//maxsellprice
    $arr[3] = $product->getMinsellprice();//minsellprice
    $arr[4] = $product->getIsAllowZeroprice()?"true":"false";//is_allow_zeroprice
    echo json_encode($arr);
    exit();  
  }
  public function executeChooseCustomer(sfWebRequest $request)
  {
    $this->forward404Unless(
    $this->invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Customer c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));
  	
	}
  public function executeProcessChooseCustomer(sfWebRequest $request)
  {
    $this->forward404Unless(
    $invoice=Doctrine_Query::create()
        ->from('Invoice i, i.Customer c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

    //fetch customer name or id
  	$customer_name=null;
  	$customer_id=null;
    if($request->getParameter("customer_name"))
      $customer_name=$request->getParameter("customer_name");
    elseif($request->getParameter("customer_id"))
      $customer_id=$request->getParameter("customer_id");
    else
      return $this->redirect("home/error?msg="."No customer specified");

    //customer name supplied, potential new customer
    //search for customer,
    //if non-existent, create
    $customer=null;
    if($customer_name)
    {
      $customer=Doctrine_Query::create()
        ->from('Customer c')
      	->where('c.name = "'.$customer_name.'"')
      	->fetchOne();
    	if(!$customer)
    	{
    	  $customer=new Customer();
    	  $customer->setName($customer_name);
    	  $customer->save();
    	}
    }
    else if($customer_id)
    {
      $customer=Doctrine_Query::create()
        ->from('Customer c')
      	->where('c.id = '.$customer_id)
      	->fetchOne();
    }

    $invoice->setCustomerId($customer->getId());
    $invoice->save();
    $this->redirect("invoice/view?id=".$request->getParameter('id'));
	}
  public function executeAdjustCollectionStatus(sfWebRequest $request)
  {
    $this->forward404Unless(
    $invoice=Doctrine_Query::create()
        ->from('Invoice i')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));
  	$invoice->setCollectionStatus($request->getParameter('collection_status'));
  	$invoice->save();
  	echo "<font color=".$invoice->getColorForCollectionStatusString($request->getParameter('collection_status')).">".$request->getParameter('collection_status')."</font>";
  	die();
  }
  public function executeFiles(sfWebRequest $request)
  {
    $this->invoice=MyModel::fetchOne("Invoice",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('invoice');
    $this->file->setParentId($this->invoice->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="invoice"')
      ->andWhere('f.parent_id='.$this->invoice->getId())
      ->execute();
  }
  public function executeDelivery(sfWebRequest $request)
  {
    $this->invoice=MyModel::fetchOne("Invoice",array('id'=>$request->getParameter("id")));
    
    $this->deliverys=$this->invoice->getDeliverys();
  }
  /*
  purpose: create a quick way to generate purchase orders connected to invoices
  in an attempt to make it easy to trace which invoice/s each purchase order was sold to
  */
  public function executeGenPO(sfWebRequest $request)
  {
    $this->invoice=MyModel::fetchOne("Invoice",array('id'=>$request->getParameter("id")));

    //if not yet finalized, error
    if($this->invoice->getIsTemporary()!=0)
    {
	    //new invno not unique: error msg and quit
        $message="Please finalize invoice before generating PO";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
    }
  
    $this->form=new InvoiceForm($this->invoice);
    $this->customerform = new CustomerForm($this->invoice->getCustomer());
    $this->vendor_id = $request->getParameter("vendor_id");
    if($this->vendor_id)
    $this->vendor=MyModel::fetchOne("Vendor",array('id'=>$request->getParameter("vendor_id")));
  }
  public function executeProcessGenPO(sfWebRequest $request)
  {
    $invoicedetail_ids=$request->getParameter("invoicedetail_ids");

    //if no invoice detail ids, error
    if(count($invoicedetail_ids)==0)
    {
      $message="Please choose at least one product";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }
    
    $qtys=$request->getParameter("qty");
    $prices=$request->getParameter("price");
    $vendor=MyModel::fetchOne("Vendor",array('id'=>$request->getParameter("vendor_id")));
    $invoice=MyModel::fetchOne("Invoice",array('id'=>$request->getParameter("id")));
    
    $po=PurchaseTable::genFromInvoice($vendor,$invoice,$invoicedetail_ids,$qtys,$prices);
  	$this->redirect("purchase/view?id=".$po->getId());
  }
  //this is used by executeGenPO()
  public function executeVendorSearch(sfWebRequest $request)
  {
    $this->invoice=MyModel::fetchOne("Invoice",array('id'=>$request->getParameter("id")));
    $this->searchstring=$request->getParameter("searchstring");
    $this->vendors=Doctrine_Query::create()
        ->from('Vendor i')
      	->where('i.name like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("i.name")
        ->execute();
  }
 public function executeViewStatementDmf(sfWebRequest $request)
  {
    $this->forward404Unless(
      $invoice=Doctrine_Query::create()
        ->from('Invoice i')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

    $inv_statement_code_setting=Fetcher::fetchOne("Settings",array("name"=>"\"last_invoice_statement_code\""));
    $inv_statement_code_setting->setValue(str_pad($inv_statement_code_setting->getValue()+1, 8, "0", STR_PAD_LEFT));
    $inv_statement_code_setting->save();
    $content=MyInvoiceHelper::PrintableStatement($invoice,$inv_statement_code_setting->getValue());

    $printtoscreen=false;
    if($printtoscreen)
    {
      $content=str_replace("\n","<br>",$content);
      $content=str_replace(" ","&nbsp;",$content);
      echo $content;
      die();
    }

    $content=bin2hex($content);
      
    $response = $this->getResponse();
    $response->clearHttpHeaders();
    //$response->setContentType($mimeType);
    $response->setHttpHeader('Content-Disposition', 'attachment; filename="' . basename(str_replace(" ","",$invoice->getInvno()).".dmf") . '"');
    $response->setHttpHeader('Content-Description', 'File Transfer');
    $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
    $response->setHttpHeader('Content-Length', 80*39);
    $response->setHttpHeader('Cache-Control', 'public, must-revalidate');
    $response->setHttpHeader('Pragma', 'public');
    $response->setContent($content);
    $response->sendHttpHeaders();

    sfConfig::set('sf_web_debug', false);
    return sfView::NONE;
  }
  //Daily Inventory Report
  public function executeDir(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    //warehouse id
    $this->warehouse_id=$request->getParameter("warehouse_id");
    if(!$this->warehouse_id)
      $this->warehouse_id=SettingsTable::fetch('default_warehouse_id');
      
    $this->form=new InvoiceForm($invoice);

    $this->stockentries = StockEntryTable::fetchByDate($invoice->getDate(),$this->warehouse_id);
    
    //create array of product ids and qtys
    $array=array();
    foreach($this->stockentries as $entry)
    {
      //if qty == 0 ignore
      if($entry->getQty()==0)continue;
  
      $stock_id=$entry->getStockId();
      
      //initialize slot if necessary
      if(!isset($array[$stock_id]))
      {
        $array[$stock_id]["in"]=0;
        $array[$stock_id]["out"]=0;
        $product=$entry->getStock()->getProduct();
        $array[$stock_id]["product_id"]=$product->getId();
        $array[$stock_id]["product_name"]=$product->getName();
        $array[$stock_id]["product_type_name"]=$product->getProducttype()->getName();
        $array[$stock_id]["endbalance"]=$entry->getBalance();
        $array[$stock_id]["qty_reported"]=null;
        $array[$stock_id]["stock_id"]=$stock_id;
        $array[$stock_id]["notes"]="";
        $array[$stock_id]["comment"]="";
      }

      //if this is a report stock entry,
      //special behavior
      if($entry->isReport())
      {
        $array[$stock_id]["qty_reported"]=$entry->getQtyReported();
        $array[$stock_id]["notes"]=$entry->getDescription();
        $array[$stock_id]["comment"]=$entry->getReportComment();
        continue;
      }

      //add qty to array
      if($entry->getQty()>0)
        $array[$stock_id]["in"]+=$entry->getQty();
      else
        $array[$stock_id]["out"]-=$entry->getQty();
    }
    
    //sort by product name, then by product type name
    usort($array, function($a, $b) {
      return strnatcmp($a['product_name'], $b['product_name']);
    });
    usort($array, function($a, $b) {
      return strnatcmp($a['product_type_name'], $b['product_type_name']);
    });

    $this->array=$array;
  }
  public function executeDirpdf(sfWebRequest $request)
  {
    $this->executeDir($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeDirmulti(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("startdate"))
      $invoice->setDate($request->getParameter("startdate"));
    elseif(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());

    else
      $invoice->setDate($year."-".$month."-".$day);

    $requestparams=$request->getParameter("purchase");
    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    $purchase=new Purchase();
    if($request->hasParameter("enddate"))
      $purchase->setDate($request->getParameter("enddate"));
    elseif(!$day or !$month or !$year)
      $purchase->setDate(MyDate::today());
    else
      $purchase->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->toform=new PurchaseForm($purchase);

    //warehouse id
    $this->warehouse_id=$request->getParameter("warehouse_id");
    if(!$this->warehouse_id)
      $this->warehouse_id=SettingsTable::fetch('default_warehouse_id');
      
    $this->stockentries = StockEntryTable::fetchByDateRange($invoice->getDate(),$purchase->getDate(),$this->warehouse_id);
    
    //create array of product ids and qtys
    $array=array();
    foreach($this->stockentries as $entry)
    {
      $stock_id=$entry->getStockId();
      
      //initialize slot if necessary
      if(!isset($array[$stock_id]))
      {
        $array[$stock_id]["in"]=0;
        $array[$stock_id]["out"]=0;
        $product=$entry->getStock()->getProduct();
        $array[$stock_id]["product_id"]=$product->getId();
        $array[$stock_id]["product_name"]=$product->getName();
        $array[$stock_id]["product_type_name"]=$product->getProducttype()->getName();
        $array[$stock_id]["endbalance"]=$entry->getBalance();
      }

      //add qty to array
      if($entry->getQty()>0)
        $array[$stock_id]["in"]+=$entry->getQty();
      else
        $array[$stock_id]["out"]-=$entry->getQty();
    }
    
    //sort by product name, then by product type name
    usort($array, function($a, $b) {
      return strnatcmp($a['product_name'], $b['product_name']);
    });
    //sort by product name, then by product type name
    usort($array, function($a, $b) {
      return strnatcmp($a['product_type_name'], $b['product_type_name']);
    });

    $this->array=$array;
  }
  public function executeDirmultipdf(sfWebRequest $request)
  {
    $this->executeDirmulti($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
    public function executeProcessDir(sfWebRequest $request)
    {
      $input_qtys=$request->getParameter("qty_reported");
      
      //use parameter "balance" instead of "qty_reported"
      //to save unreported items
      if($request->getParameter("submit")=="Save Unreported")
      {
        $input_qtys=$request->getParameter("balance");
      }
      
      $input_notes=$request->getParameter("notes");
      $warehouse_id=$request->getParameter("warehouse_id");
      $date=$request->getParameter("date");
      
      //create array of stock ids from $input_qtys
      $input_stock_ids=array_keys($input_qtys);
      
      //load existing report stock entries 
      //from database
      $reports_stock_entries=Doctrine_Query::create()
        ->from('Stockentry se')
      	->whereIn('se.stock_id',$input_stock_ids)
      	->andWhere('se.type = "Report"')
      	->andWhere('se.datetime >= "'.$date.' 00:00:00"')
      	->andWhere('se.datetime <= "'.$date.' 23:59:59"')
      	->execute();

      //put these into an array by index
      $reports_se_by_stock_id=array();
      foreach($reports_stock_entries as $entry)
      {
        $reports_se_by_stock_id[$entry->getStockId()]=$entry;
      }

      //save qtys reported by identifying which stockentry, and if not found, create
      foreach($input_qtys as $stock_id=>$qty)
      {
        //if index exists
        if(isset($reports_se_by_stock_id[$stock_id]))
        {
          $se=$reports_se_by_stock_id[$stock_id];
          $notes=$input_notes[$stock_id];

          //if qty not specified
          //delete report
          if($qty==="")
          {
            $se->delete();
            continue;
          }
          //else if qty is specified
          else
          {

            //if qty is different
            if($se->getQtyReported()!=$qty)
            {
              $se->setTimeToEndOfDay();
              $se->setQtyReported($qty);
              $se->setDescription($notes);
              $se->calcReport();
              $se->save();
              $se->getStock()->calcFromStockEntry($se);
            }
            //else if only notes is different
            //just update
            //do not calc
            else if($se->getDescription()!=$notes)
            {
              $se->setDescription($notes);
              $se->save();
            }
            //else nothing changed, do nothing
          }
        }
        //else $se does not exist
        else
        {
          //if qty not specified
          //do nothing
          if($qty==="")
          {
            continue;
          }
          //else create se
          else
          {
            $description=$input_notes[$stock_id];
            $stock=Fetcher::fetchById("Stock",$stock_id);

            $se=$stock->addEntry($date, 0, null, null ,"Report",$description,$this->getUser()->getGuardUser()->getId(),$qty);
          }
        }
      }
      
      $today=MyDateTime::frommysql($date); 
      return $this->redirect("invoice/dir?invoice[date][day]=".$today->getDay()."&invoice[date][month]=".$today->getMonth()."&invoice[date][year]=".$today->getYear());
    }
    public function executeConvertTransactionToInvoice(sfWebRequest $request)
    {
      //settings do not allow this function - do nothing
      if(!sfConfig::get('custom_transaction_can_become_invoice'))
      {
        return $this->redirect($request->getReferer());
      }
    
	    $requestparams=$request->getParameter('invoice');
	    $id=$requestparams['id'];
	    $invno=trim($requestparams['invno']);
	    
      $this->forward404Unless(
      $this->invoice=Doctrine_Query::create()
      ->from('Invoice i')
    	->where('i.id = '.$id)
    	->fetchOne()
      , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

	    //validation
	    //if template is not a transaction, error
	    if($this->invoice->getInvoiceTemplate()->getIsInvoice())
	    {
        $message="This is already an invoice";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
	    }
	    
    	//validate invoice is not cancelled
    	if($this->invoice->getStatus()=="Cancelled")
    	{
        $message="Transaction ".$invno." cannot be converted because it is already cancelled.";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
    	}

    	//validate invoice is not cancelled
    	if($this->invoice->getIsTemporary()!=0)
    	{
        $message="Only closed transactions can be converted to invoice";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
    	}

      //if specified template is "transaction",
      //use Invoice as template
	    if($requestparams['template_id']==10)
  	    $invoicetemplate=Fetcher::fetchOne("InvoiceTemplate",array("name"=>"'Invoice'"));
      //else use specified template
	    else
  	    $invoicetemplate=Fetcher::fetchById("InvoiceTemplate",$requestparams['template_id']);
	    
      //if invoicetemplate not found
      if($invoicetemplate==null)$invoicetemplate=Fetcher::fetchOne("InvoiceTemplate");
	    
      $this->invoice->setTemplateId($invoicetemplate->getId());
      $this->invoice->setInvno($invno);
      $this->invoice->setNotes($this->invoice->getNotes()."; Invoiced ".MyDateTime::today()->toshortdate());
      $this->invoice->save();
        
      $this->redirect("invoice/view?id=".$this->invoice->getId());
    }
    public function executeDrDiagnostics(sfWebRequest $request)
    {
      // die("ACCESS DENIED");
  
        //default values
      $this->interval=100;
      $this->start=12900;
  
      //if method = get
      if(!isset($_REQUEST["submit"]))
      {
        $this->invoices=array();  
       return;
      }
       
      if(isset($_REQUEST["interval"]))
          $this->interval=$request->getParameter("interval");
      if(isset($_REQUEST["start"]))
          $this->start=$request->getParameter("start");
       $this->end=$this->start+$this->interval-1;
      
       $this->invoices = Doctrine_Query::create()
        ->from('Invoice p')
        ->where('p.id <='.$this->end)
        ->andWhere('p.id >='.$this->start)
        ->execute();
  
      foreach($this->invoices as $p)
      {
        foreach($p->getInvoicedetail() as $detail)
        {
          $released=0;
          foreach($detail->getInvoiceDrDetail() as $drdetail)
          {
            if($drdetail->isReleased())
              $released+=$drdetail->getQty();
          }
          if($released>$detail->getQty() and $detail->getProductId()!=2392 and $detail->getProductId()!=1609 )
          {
            echo "DR Calculation Error detected: "
              ."<br>".$p
              ."<br>Invoice ID ".$p->getId()
              ."<br>Released Qty: ".$released
              ."<br>Invoice Qty: ".$detail->getQty()
              ."<br>Product: ".$detail->getProduct()->getName()
              ;
            die();
          }
        }
      }
  
       $this->start=$this->end+1;
    }
    public function executeToggleIsDrBased(sfWebRequest $request)
    {
      $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
          ->from('Invoice i')
          ->where('i.id = '.$request->getParameter('id'))
          ->fetchOne()
      , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

      if($this->invoice->isDrBased())
        $this->invoice->setIsDrBased(0);
      else
        $this->invoice->setIsDrBased(1);
      $this->invoice->save();

      $this->redirect("invoice/view?id=".$this->invoice->getId());
    }
    public function executeSetCommissionPayment(sfWebRequest $request)
    {
      $this->forward404Unless(
        $this->commission_payment=Doctrine_Query::create()
          ->from('CommissionPayment i')
          ->where('i.id = '.$request->getParameter('commission_payment_id'))
          ->fetchOne()
      , sprintf('Commission Payment with id (%s) not found.', $request->getParameter('commission_payment_id')));

      $this->forward404Unless(
        $this->invoice=Doctrine_Query::create()
          ->from('Invoice i')
          ->where('i.id = '.$request->getParameter('id'))
          ->fetchOne()
      , sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

      $this->invoice->setCommissionPaymentId($request->getParameter('commission_payment_id'));
      $this->invoice->save();
    }

    public function executeMassRecalc(sfWebRequest $request)
    {
        //default values
      $this->interval=100;
      $this->start=1;
  
      //if method = get
      if(!isset($_REQUEST["submit"]))
      {
        $this->products=array();  
       return;
      }
       
      if(isset($_REQUEST["interval"]))
          $this->interval=$request->getParameter("interval");
      if(isset($_REQUEST["start"]))
          $this->start=$request->getParameter("start");
       $this->end=$this->start+$this->interval-1;
      
       $this->invoices = Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id <='.$this->end)
        ->andWhere('i.id >='.$this->start)
        ->execute();
        foreach ($this->invoices as $invoice) {
          $invoice->calc();
          $invoice->save();
        }
       $this->start=$this->end+1;
    }
  
    public function executeMassRecalcForProductCategory(sfWebRequest $request)
    {
        //default values
      $this->interval=100;
      $this->start=1;
  
      //if method = get
      if(!isset($_REQUEST["submit"]))
      {
        $this->products=array();  
       return;
      }
       
      if(isset($_REQUEST["interval"]))
          $this->interval=$request->getParameter("interval");
      if(isset($_REQUEST["start"]))
          $this->start=$request->getParameter("start");
       $this->end=$this->start+$this->interval-1;
      
       $this->invoices = Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.id <='.$this->end)
        ->andWhere('i.id >='.$this->start)
        ->execute();
        foreach ($this->invoices as $invoice) {
          $invoice->calcForProductCategories();
          $invoice->save();
        }
       $this->start=$this->end+1;
    }
    public function executeCheckExists(sfWebRequest $request)
    {
      $invoice=Fetcher::fetchOne("Invoice",array("transaction_code"=>$request->getParameter("code")));
      if($invoice!=null)
        echo json_encode(array("id"=>$invoice->getId(), "total"=>$invoice->getTotal(), "customer"=>$invoice->getCustomer()->getName()));
      else 
        echo "";
      die();
    }
    //this is to calculate sales reports by salesman 
    //calculate invoicedetail totals per date range
    public function executeSalesTotalBySalesmanIdApi(sfWebRequest $request)
    {
      $startdate=$request->getParameter("startdate");
      $enddate=$request->getParameter("enddate");
      $salesman_id=$request->getParameter("salesman_id");
      // $startdate="2022-02-02";
      // $enddate="2022-02-02";
      // $salesman_id=30;
      $invoices = InvoicedetailTable::fetchByDateRangeAndSalesmanId($startdate,$enddate,$salesman_id);
      $output=array();
      $total=0;
      foreach($invoices as $i)
      {
        $total+=$i->getTotal();
      }
      echo json_encode(array("total"=>$total));
      die();
    }
    public function executeSalesReportBySalesman(sfWebRequest $request)
    {
      $this->form=new InvoiceForm();
      $requestparams=$request->getParameter("invoice");
      $this->salesman_id=$requestparams["salesman_id"];
      $this->salesman=Fetcher::fetchOne("Employee",array("id"=>$this->salesman_id));
    }
    public function executeDuplicateError(sfWebRequest $request)
    {
      $this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("id")));
    }
  }

