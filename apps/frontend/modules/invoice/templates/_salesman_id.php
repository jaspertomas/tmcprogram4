

<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_salesman_id">
<div>
<label for="invoice_salesman_id">Salesman</label>
<div class="content">
<?php
echo '<select name="invoice[salesman_id]" id="invoice_salesman_id">';
foreach(EmployeeTable::getSalesmen() as $salesman)
{
echo '<option value="'.$salesman->getId().'" '.($salesman->getId()==$form->getObject()->getSalesmanId()?'selected=selected':'').'>'.$salesman->getName().'</option>';
}
echo '</select>';
?>
<?php 
?> &ensp;(<?php //echo link_to("Pick another salesman","salesman/search")?>)
</div>
</div>
</div>
