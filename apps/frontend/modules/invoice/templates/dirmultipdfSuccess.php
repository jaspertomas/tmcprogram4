<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "LETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Tradewind Mdsg Corp Daily Sales Report');

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->startPageGroup();
$pdf->AddPage();



//-----------------------------------
//addresses and tin numbers table
//header
$pdf->SetFont('dejavusans', '', 12, '', true);
$pdf->write(0,MyDateTime::frommysql($form->getObject()->getDate())->toshortdate(),
'',false,'',true,0,false,false,0,0);
$pdf->write(5,"Multi Date Inventory Report\n");
$pdf->SetFont('dejavusans', '', 10, '', true);
$widths=array(120,18,18,18,18);
$scale=3.543;
$producttypename="";

foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl = <<<EOD
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Product</b></td>
  <td width="$widths[1]" align="center"><b>Start</b></td>
  <td width="$widths[2]" align="center"><b>In</b></td>
  <td width="$widths[3]" align="center"><b>Out</b></td>
  <td width="$widths[4]" align="center"><b>End</b></td>
 </tr>
</thead>
EOD;
//body
foreach($array as $item){ 
  if($item["in"]==0 and $item["out"]==0 )continue;
  if($item["product_type_name"]!=$producttypename){
    $producttypename=$item["product_type_name"];

$output=array($item["product_type_name"]);
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="left"><b>$output[0]</b></td>
<td width="$widths[1]" align="center"></td>
<td width="$widths[2]" align="center"></td>
<td width="$widths[3]" align="center"></td>
<td width="$widths[4]" align="center"></td>
</tr>
EOD;
}


$output=array(
  $item["product_name"],
  $item["endbalance"]+$item["out"]-$item["in"],  
  $item["in"],
  $item["out"],
  $item["endbalance"]+0,
  );

$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="left">$output[0]</td>
<td width="$widths[1]" align="right">$output[1]</td>
<td width="$widths[2]" align="right">$output[2]</td>
<td width="$widths[3]" align="right">$output[3]</td>
<td width="$widths[4]" align="right">$output[4]</td>
</tr>
EOD;
}
$tbl .= <<<EOD
</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');
//end addresses and tin numbers table

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('DIR-'.MyDateTime::frommysql($form->getObject()->getDate())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

