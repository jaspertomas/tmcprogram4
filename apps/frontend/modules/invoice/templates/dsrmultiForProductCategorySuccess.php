<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new InvoiceForm(),"invoice/dsrmultiForProductCategory")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>

Template: <?php
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'InvoiceTemplate',
  'add_empty' => true,
));
echo $w->render('template_id',intval($template_id));
?>

<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $toform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Month","invoice/dsrmultiForProductCategory?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql()."&template_id=".$template_id);?> | 
<?php echo link_to("This Month","invoice/dsrmultiForProductCategory?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql()."&template_id=".$template_id);?> | 
<?php echo link_to("Next Month","invoice/dsrmultiForProductCategory?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql()."&template_id=".$template_id);?> | 
<br>
    <?php echo link_to("Yesterday","invoice/dsrmultiForProductCategory?invoice[date][day]=".$yesterday->getDay()."&invoice[date][month]=".$yesterday->getMonth()."&invoice[date][year]=".$yesterday->getYear()."&template_id=".$template_id); ?>
    <?php echo link_to("Tomorrow","invoice/dsrmultiForProductCategory?invoice[date][day]=".$tomorrow->getDay()."&invoice[date][month]=".$tomorrow->getMonth()."&invoice[date][year]=".$tomorrow->getYear()."&template_id=".$template_id); ?>
<br>

</form>

<h1>Period Sales Report Split Type</h1>
<h3>(Separate TJL)</h3>
Date: <?php echo $form->getObject()->getDate(); ?> to <?php echo $toform->getObject()->getDate(); ?>

<br>TJL Sales: <?php echo MyDecimal::format($productcategorytotal)?>
<br>Other Sales: <?php echo MyDecimal::format($noproductcategorytotal)?>
<br>Total Sales: <?php echo MyDecimal::format($total)?>
<br>
<br>Petty Cash Expenses: <?php echo MyDecimal::format($pettycashtotal)?>
<br>Remaining Cash: <?php echo MyDecimal::format($remainingcash)?>
<br>
<?php 
$datearray=explode("-",$form->getObject()->getDate());
$todatearray=explode("-",$toform->getObject()->getDate());
echo link_to("Print","invoice/dsrmultiForProductCategorypdf?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]."&template_id=".$template_id);?>


<br>
<br>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Customer</td>
    <td>Form</td>
    <td>Reference</td>
    <td>Transaction Code</td>
    <td>Item Description</td>
    <td>Total TJL</td>
    <td>Total Other</td>
    <td>Salesman</td>
    <td>Remarks</td>
  </tr>
  <?php 
foreach($templates as $template){
//foreach(array(2,4,1,3,5) as $template_id){
?>
    <?php foreach($invoices as $invoice)
        if($invoice->getIsTemporary()==0)
        if($invoice->getTemplateId()==$template->getId())
        if($invoice->getStatus()!="Cancelled")
        if($invoice->getHidden()==0)//if not hidden
        {
        $tin_no=$invoice->getCustomer()->getTinNo();
        ?>
    <tr>
      <td><?php echo $invoice->getDate() ?></td>
      <td><?php echo $invoice->getCustomer()." (".($tin_no?$tin_no:"No Tin").")" ?></td>
      <td><?php echo $invoice->getInvoiceTemplate() ?></td>
      <td><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php echo $invoice->getTransactionCode() ?></td>
      <td><?php echo $invoice->getParticularsString().($invoice->getCheque()?("; Check No.:".$invoice->getCheque().": ".$invoice->getChequedate()):"") ?></td>
      <td><?php echo $invoice->getTotalForProductCategory() ?></td>
      <td><?php echo $invoice->getTotalForNoProductCategory() ?></td>
      <td><?php echo $invoice->getEmployee() ?></td>
      <td><?php echo $invoice->getStatus() ?></td>
      <td><?php echo $invoice->getNotes() ?></td>
      <td><?php echo link_to("Edit","invoice/edit?id=".$invoice->getId()) ?></td>
      <td><?php echo $invoice->getDate() ?></td>
    </tr>
    <?php }?>
    <tr>
      <td></td>
    </tr>
  <?php } ?>
</table>

