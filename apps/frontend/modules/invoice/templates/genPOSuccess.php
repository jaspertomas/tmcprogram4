<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
Generate PO from Invoice
</h1>
<?php slot('transaction_id', $invoice->getId()) ?>
<?php slot('transaction_type', "Invoice") ?>

<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Inv no.</td>
          <td>
            <?php echo $invoice->getInvoiceTemplate()." ".$invoice->getInvno(); ?>
          </td>
        </tr>
        <tr>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
        </tr>
        <!--tr>
          <td>PO No.</td>
          <td><?php //echo $invoice->getPonumber() ?></td>
        </tr-->
        <tr>
          <td>Customer</td>
          <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId(),array("target"=>"edit_customer")); ?> </td>
        </tr>
		</table>
    <td>
      <table>
        <tr>
          <td>Sales Rep</td>
          <td><?php echo $invoice->getEmployee() ?></td>
        </tr>
        <tr>
          <td>Total</td>
          <td><?php echo $invoice->getTotal() ?></td>
        </tr>
		    <tr>
	        <td><b>Status</b></td>
	        <td>
            <font <?php 
            
            
            switch($invoice->getStatus())
            {
              case "Paid":echo "color=blue";break;
              case "Pending":echo "color=red";break;
              case "Cancelled":echo "color=red";break;
              case "Overpaid":echo "color=red";break;
            }
            ?>>
	        <?php 
            /*
              if status=paid,
                if checkcleardate > today, 
                  status = pending. 
                else 
                  status = paid
            */
            if($invoice->getStatus()=="Paid"){
            $today=MyDateTime::today();
            $checkcleardate=MyDateTime::frommysql($invoice->getCheckcleardate());
            $status="Paid";
            if($checkcleardate->islaterthan($today))$status="Check to clear on ".$checkcleardate->toshortdate();
            echo $status;

            }
            else echo $invoice->getStatus();

            ?>
            </font>
          </td>
		    </tr>
      </table>
    </td>
  </tr>
</table>

<?php echo link_to("Back to Invoice","invoice/view?id=".$invoice->getId())?>
<hr>

<?php if(isset($vendor)){?><font size=5 face=arial>Supplier: <b><?php echo $vendor->getName()?></b></font><br><?php } ?>
<font size=5 face=arial><b>Choose Supplier: </b></font>

<input id=invoicesuppliersearchinput autocomplete="off">
<div id="invoicesearchresult"></div>

<hr>

<?php if($vendor_id){// hide unless vendor id is specified?>

<!-----invoice detail table------>
<font size=5 face=arial><b>Choose products to include in PO</b></font>

<?php echo form_tag('invoice/processGenPO')?> 
<input type=hidden name="vendor_id" value=<?php echo $vendor->getId()?>>
<input type=hidden name="id" value="<?php echo $invoice->getId()?>">
<table border=1>
  <tr>
    <td width=1><input type="checkbox" class="checkall" /></td>
    <td>Qty</td>
    <td>Price</td>
    <td>Description</td>
  </tr>
  <?php 
  	$counter=0;
  	foreach($invoice->getInvoicedetails() as $detail)
  	{
  		$counter++;?>
  <tr>
    <td><input type="checkbox" class="check" name="invoicedetail_ids[<?php echo $detail->getId()?>]" value="<?php echo $detail->getId()?>"/></td>
    <td><input name="qty[<?php echo $detail->getId()?>]" value="<?php echo $detail->getQty()?>"/></td>
    <td><input name="price[<?php echo $detail->getId()?>]" value="<?php echo $detail->getProduct()->getMaxbuyprice()?>"/></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
  </tr>
  <?php }?>
</table>
<input type=submit class=submit value="Generate Purchase Order">
</form>


<hr>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo "<br>".form_tag_for($customerform,'customer/adjust')?> 
          <?php echo $customerform['id'] ?>
<?php } ?>    
<table>
  <tr>
    <td colspan=2><font size=5 face=arial><b>Edit customer details</b></font></td>
  </tr>
  <tr>
    <td colspan=2>Sold to: <u><?php echo $invoice->getCustomer()?></u></td>
  </tr>
  <tr>
    <td colspan=2>Address: <u><?php echo $invoice->getCustomer()->getAddress()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['address'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Address2: <u><?php echo $invoice->getCustomer()->getAddress2()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['address2'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Phone: <u><?php echo $invoice->getCustomer()->getPhone1()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['phone1'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Email: <u><?php echo $invoice->getCustomer()->getEmail()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['email'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Representative: <u><?php echo $invoice->getCustomer()->getRep()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['rep'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>TIN/SC-TIN: <u><?php echo $invoice->getCustomer()->getTinNo()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['tin_no'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr valign=top>
    <td colspan=2>Notes: <u><?php echo $invoice->getCustomer()->getNotepad()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <br><textarea cols=100 rows=2 name="customer[notepad]" id="customer_notepad" type="text"><?php echo $invoice->getCustomer()->getNotepad()?></textarea>
  <?php } ?>    
    </td>
  </tr>
</table>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
  <input type="submit" value="Save">
  </form>
<?php } ?>    

<?php } //end hide unless vendor id is specified?>


<script>
$(document).ready(function(){ 
    $("#invoicesuppliersearchinput").focus(function() { $(this).select(); } );
    $('.checkall').click(function() {
        $(".check").prop('checked', $(this).is(':checked'));
    });     
    $("#invoicesuppliersearchinput").on('keyup', function(event) {
        //product has been edited. disable save button
        //$("#invoice_detail_submit").prop("disabled",true);
	      
	      //if less than 1 letter in search box, do nothing
        if($("#invoicesuppliersearchinput").val().length==0)return;
        
        //on enter
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13')
        {
          $("#chosensupplier").html("");
          var searchstring=$("#invoicesuppliersearchinput").val();
          $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/invoice/vendorSearch?id=<?php echo $invoice->getId()?>&searchstring="+searchstring, success: function(result){
       		  $("#invoicesearchresult").html(result);
          }});
        }
        //else clear
        else
     		  $("#invoicesearchresult").html("");
    });
    $('.submit').click(function(e) {
        //e.preventDefault();
        if (window.confirm("Are you sure?")) {
            location.href = this.href;
        }
    });
});  


</script>

