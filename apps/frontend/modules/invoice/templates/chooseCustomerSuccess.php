<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
	Invoice (<?php echo $invoice->getIsTemporaryString()?>): Choose Customer 
</h1>
<?php slot('transaction_id', $invoice->getId()) ?>
<?php slot('transaction_type', "Invoice") ?>

<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Inv no.</td>
          <td>
            <?php echo $invoice->getInvoiceTemplate()." ".$invoice->getInvno(); ?>
          </td>
        </tr>
        <tr>
          <td>Date</td>
          <td><?php echo $invoice->getDate() ?></td>
        </tr>
        <!--tr>
          <td>PO No.</td>
          <td><?php //echo $invoice->getPonumber() ?></td>
        </tr-->
        <tr>
          <td>Customer</td>
          <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId(),array("target"=>"edit_customer")); ?></td>
        </tr>
				<!--tr>
					<td>Cheque Data</td>
        <?php //$cheques=explode(", ",$invoice->getChequedata());foreach($cheques as $cheque)  {?>
					<td><?php //echo $cheque?></td>				
			  </tr>
				<tr>
					<td></td>				
			  <?php //} ?>
				</tr-->
        <tr>
          <td></td>
          <td colspan=2><?php echo link_to("Cancel","invoice/view?id=".$invoice->getId()) ?> </td>
        </tr>
		</table>
    <td>
      <table>
        <!--tr>
          <td>Due date</td>
          <td><?php //echo $invoice->getDuedate() ?></td>
        </tr-->
        <tr>
          <td>Sales Rep</td>
          <td><?php echo $invoice->getEmployee() ?></td>
        </tr>
        <!--tr>
          <td>Discount Rate</td>
          <td><?php //echo $invoice->getDiscrate() ?></td>
        </tr>
        <tr>
          <td>Discount Amount</td>
          <td><?php //echo $invoice->getDiscamt() ?></td>
        </tr-->
        <tr>
          <td>Total</td>
          <td><?php echo $invoice->getTotal() ?></td>
        </tr>
		    <tr>
	        <td><b>Status</b></td>
	        <td>
            <font <?php 
            
            
            switch($invoice->getStatus())
            {
              case "Paid":echo "color=blue";break;
              case "Pending":echo "color=red";break;
              //case "Cancelled":echo "color=red";break;
            }
            ?>>
	        <?php 
            /*
              if status=paid,
                if checkcleardate > today, 
                  status = pending. 
                else 
                  status = paid
            */
            if($invoice->getStatus()=="Paid"){
            $today=MyDateTime::today();
            $checkcleardate=MyDateTime::frommysql($invoice->getCheckcleardate());
            $status="Paid";
            if($checkcleardate->islaterthan($today))$status="Check to clear on ".$checkcleardate->toshortdate();
            echo $status;

            }
            else echo $invoice->getStatus();

            ?>
            </font>
          </td>
		    </tr>
      </table>
    </td>
    <td>
      <table>
        <tr valign=top>
          <td>Sale Type</td>
          <td><?php echo $invoice->getSaleType() ?></td>
          <td>
        </td>
        </tr>
        <tr>
          <td>Cash</td>
          <td><?php echo $invoice->getCash() ?></td>
          <td>
          </td>
        </tr>
        <tr>
          <td>Cheque</td>
          <td><?php echo $invoice->getChequeamt() ?></td>
          <td>
          </td>
        </tr>
        <tr>
          <td>Balance</td>
          <td><?php echo $invoice->getCredit() ?></td>
          <td>
          </td>
        </tr>
      </table>
    </td>
    <td>
			<table>
        <tr valign=top>
          <td>Po No.</td>
          <td><?php echo $invoice->getPonumber() ?></td>
          <td>
          </td>
        </tr>
        <tr>
          <td>Notes</td>
          <td><?php echo $invoice->getNotes() ?></td>
          <td>
          </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td>
          </td>
        </tr>
		  </table>
    </td>
  </tr>
  <tr hidden=true id=invoice_edit_password_tr class=password_tr >
    <td align=center colspan=10>Enter manager password:<input id=invoice_edit_password_input type=password><input type=button value="Submit Password" id=invoice_edit_password_button></td>
  </tr> 
</table>
<hr>
<?php
//if user is salesman or encoder
//if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>

Search Customer: <input id=invoicecustomersearchinput autocomplete="off" size=12> 
<div id="invoicesearchresult"></div>

<?php echo form_tag('invoice/processChooseCustomer')?> 



<?php //} ?>
<script>
$("#invoicecustomersearchinput").focus();
$("#invoicecustomersearchinput").keyup(function(event){
	//if 3 or more letters in search box
    //if($("#customersearchinput").val().length>=3){
    
    //if enter key is pressed
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
	    $.ajax(
	      {
	        url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/customer/search?searchstring="+$("#invoicecustomersearchinput").val()+"&invoice_id="+<?php echo $invoice->getId()?>, 
	        success: function(result){
	 		  $("#invoicesearchresult").html(result);
	    }});
    }
});

</script>
