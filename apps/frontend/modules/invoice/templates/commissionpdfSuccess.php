<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "LETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Tradewind Mdsg Corp Period Sales Report');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 30, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.

  foreach($empinvoices as $empid=>$employeedata)
  {
    $pdf->AddPage();

    $pdf->write(0,"Commission Report",'',false,'',true,0,false,false,0,0);
    $pdf->write(0,$date->getshortmonth()." ".$date->getYear(),'',false,'',true,0,false,false,0,0);
    $pdf->write(0,$employees[$empid]->getName().": ".MyDecimal::format($commissiontotals[$employees[$empid]->getId()]),'',false,'',true,0,false,false,0,0);
    $pdf->write(0,"",'',false,'',true,0,false,false,0,0);
    $pdf->write(0,"",'',false,'',true,0,false,false,0,0);

    $count=0;
    $contents=array();
    foreach($templates as $template)
    foreach($employeedata as $invoice){
	  if($invoice->getStatus()!="Pending")
	  if($invoice->getIsTemporary()==0)
	  if($invoice->getTemplateId()==$template->getId())
            {
      $count++;
      $contents[]=array(
                $count,
                MyDateTime::frommysql($invoice->getDate())->toshortdate(),
                $invoice,
                MyDecimal::format($invoice->getTotal()),
                MyDecimal::format($invoice->getCommissionTotal($employees[$empid])),
                MyDecimal::format($invoice->getCommission($employees[$empid])),
                $invoice->getStatus(),
                );
    }}
    $widths=array(10,30,35,30,30,20,20);
    $height=1;

    //cancelled is yellow
		$pdf->SetFillColor(255, 255, 0);

    //dislay paid and cancelled
    foreach($contents as $content)
    {
      $pdf->MultiCell($widths[0], $height, $content[0], 0, 'R', ($content[6]=="Cancelled"?1:0), 0, '', '', true);
      $pdf->MultiCell($widths[1], $height, $content[1], 0, 'R', ($content[6]=="Cancelled"?1:0), 0, '', '', true);
      $pdf->MultiCell($widths[2], $height, $content[2], 0, 'R', ($content[6]=="Cancelled"?1:0), 0, '', '', true);
      $pdf->MultiCell($widths[3], $height, $content[3], 0, 'R', ($content[6]=="Cancelled"?1:0), 0, '', '', true);
      $pdf->MultiCell($widths[4], $height, $content[4], 0, 'R', ($content[6]=="Cancelled"?1:0), 0, '', '', true);
      $pdf->MultiCell($widths[5], $height, $content[5], 0, 'R', ($content[6]=="Cancelled"?1:0), 0, '', '', true);
      $pdf->MultiCell($widths[6], $height, $content[6], 0, 'R', ($content[6]=="Cancelled"?1:0), 1, '', '', true);
    }

    //pending is lightgreen
		$pdf->SetFillColor(127, 255, 127);
//		$pdf->SetTextColor(128,0,0);
//		$pdf->SetDrawColor(128, 0, 0);

    //display pending
    $contents=array();
    foreach($templates as $template)
    foreach($employeedata as $invoice){
  	if($invoice->getStatus()=="Pending")
	  if($invoice->getIsTemporary()==0)
	  if($invoice->getTemplateId()==$template->getId())
    {
      $count++;
      $contents[]=array(
                $count,     
                MyDateTime::frommysql($invoice->getDate())->toshortdate(),
                $invoice,
                MyDecimal::format($invoice->getTotal()),
                MyDecimal::format($invoice->getCommissionTotal($employees[$empid])),
                MyDecimal::format($invoice->getCommission($employees[$empid])),
                $invoice->getStatus(),
                );
    }}
    $widths=array(10,30,35,30,30,20,20);
    $height=1;
    foreach($contents as $content)
    {
      $pdf->MultiCell($widths[0], $height, $content[0], 0, 'R', 1, 0, '', '', true);
      $pdf->MultiCell($widths[1], $height, $content[1], 0, 'R', 1, 0, '', '', true);
      $pdf->MultiCell($widths[2], $height, $content[2], 0, 'R', 1, 0, '', '', true);
      $pdf->MultiCell($widths[3], $height, $content[3], 0, 'R', 1, 0, '', '', true);
      $pdf->MultiCell($widths[4], $height, $content[4], 0, 'R', 1, 0, '', '', true);
      $pdf->MultiCell($widths[5], $height, $content[5], 0, 'R', 1, 0, '', '', true);
      $pdf->MultiCell($widths[6], $height, $content[6], 0, 'R', 1, 1, '', '', true);
    }
  }
// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

