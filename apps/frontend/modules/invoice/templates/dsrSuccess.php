<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new InvoiceForm(),"invoice/dsr")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
$red="#faa";
$yellow="#ffc";
$green="#6d6";
$blue="#ccf";
$plain="#eee";
?>
<table>
  <tr>
    <td>Date</td>
    <td><?php echo $form["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
    <?php echo link_to("Yesterday","invoice/dsr?invoice[date][day]=".$yesterday->getDay()."&invoice[date][month]=".$yesterday->getMonth()."&invoice[date][year]=".$yesterday->getYear()); ?>
    <?php echo link_to("Tomorrow","invoice/dsr?invoice[date][day]=".$tomorrow->getDay()."&invoice[date][month]=".$tomorrow->getMonth()."&invoice[date][year]=".$tomorrow->getYear()); ?>
    <?php echo link_to("Go to DSR Multi Date","invoice/dsrmulti?invoice[date][day]=".$today->getDay()."&invoice[date][month]=".$today->getMonth()."&invoice[date][year]=".$today->getYear()."&purchase[date][day]=".$today->getDay()."&purchase[date][month]=".$today->getMonth()."&purchase[date][year]=".$today->getYear()); ?>


</form><h1>Daily Sales Report </h1>
Date: <?php echo $form->getObject()->getDate(); $datearray=explode("-",$form->getObject()->getDate());?>

<br>Cash Sales: <?php echo MyDecimal::format($cashtotal)?>
<br>Cheque Sales: <?php echo MyDecimal::format($chequetotal)?>
<br>Credit Sales: <?php echo MyDecimal::format($credittotal)?>
<br>Total Sales: <?php echo MyDecimal::format($total)?>
<br>
<?php if(sfConfig::get('custom_show_product_category_totals_in_dsr')){?>
<br>Tank Sales: <?php echo MyDecimal::format($totalForProductCategory)?>
<br>Pump Sales: <?php echo MyDecimal::format($totalForNoProductCategory)?>
<br>
<?php }?>
<br>Petty Cash Expenses: <?php echo MyDecimal::format($pettycashtotal)?>
<br>Remaining Cash: <?php echo MyDecimal::format($remainingcash)?>
<br><?php echo link_to("Print","invoice/dsrpdf?invoice[date][year]=".$datearray[0]."&invoice[date][month]=".$datearray[1]."&invoice[date][day]=".$datearray[2]);?>


<br>
<br>
<table border=1>
  <tr>
    <td>Customer</td>
    <td>Form</td>
    <!--td>Tin No</td-->
    <td>Reference</td>
    <td>Transaction Code</td>
    <td>Item Description</td>
    <td>Terms</td>
    <td>Cash</td>
    <td>Cheque</td>
    <td>Acct Sales</td>
    <?php if(sfConfig::get('custom_show_product_category_totals_in_dsr')){?>
    <td>Tanks</td>
    <td>Pumps</td>
    <?php }?>
    <td>Salesman</td>
    <td>Status</td>
    <td>Sold From</td>
    <td>Notes</td>
  </tr>
  <?php $addresses=array();?>
  <?php foreach(array(2,4,1,3,5,10) as $template_id){?>
    <?php foreach($events as $event){
      $invoice=$event->getParent();
      if($invoice and $invoice->getTemplateId()==$template_id){
        $customer=$invoice->getCustomer();
        //catch addresses if present
        if($customer->getAddress()!="" or $customer->getTinNo()!="")
        {
          $addresses[]=array(
            $customer,
            $customer->getTinNo(),
            $invoice->getInvno(),
            $customer->getAddress(),
          );
        }
      
      ?>
    <tr>
      <td><?php echo $customer ?></td>
      <td><?php echo $invoice->getInvoiceTemplate() ?></td>
      <!--td><?php //echo $customer->getTinNo() ?></td-->
      <td><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php echo $invoice->getTransactionCode() ?></td>
      <td><?php echo $event->getType().": ".$event->getDetail1().": ".$event->getDetail2() ?></td>
      <td></td>
      <td align=right><?php echo $event->getDetail("cashamt");?></td>
      <td align=right><?php echo $event->getDetail("chequeamt");?></td>
      <td align=right><?php if($invoice->getStatus()!="Cancelled")echo $event->getDetail("creditamt");?></td>
      <?php if(sfConfig::get('custom_show_product_category_totals_in_dsr')){?>
      <td></td>
      <td></td>
      <?php }?>
      <td><?php echo $invoice->getEmployee() ?></td>
      <td><?php echo $invoice->getStatus() ?></td>
      <td><?php echo $event->getNotes() ?></td>
      <td><?php echo link_to("Edit","event/edit?id=".$event->getId()) ?></td>
    </tr>
    <?php }?>
    <?php }?>
    <?php foreach($invoices as $invoice)
    //show only closed uncancelled invoices
    // if($invoice->getIsTemporary()==0)
    //show closed, uncancelled invoices
    //show open or cancelled invoices if they have payment
    if(($invoice->getIsTemporary()==0 and $invoice->getStatus()!="Cancelled") or $invoice->getCash()!=0 or $invoice->getChequeamt()!=0)
    {
    if($invoice->getTemplateId()==$template_id){
      $customer=$invoice->getCustomer();
      //catch addresses if present
      if($customer->getAddress()!="" or $customer->getTinNo()!="")
      {
        $addresses[]=array(
          $customer,
          $customer->getTinNo(),
          $invoice->getInvno(),
          $customer->getAddress(),
        );
      }
      $invoice_details=$invoice->getInvoicedetail();
      $profit_details=$invoice->getProfitDetails();
      $item_count=0;
      $sold_count=0;
      $item_count_per_product=array();
      $sold_count_per_product=array();

      foreach($invoice_details as $pd)
      {
        //initialize array values to 0
        //it must be done here, coz not all products have a profit detail
        if(!isset($item_count_per_product[$pd->getProductId()]))
        {
          $item_count_per_product[$pd->getProductId()]=0;
          $sold_count_per_product[$pd->getProductId()]=0;
        }
        $item_count_per_product[$pd->getProductId()]+=$pd->getQty();
        $item_count+=$pd->getQty();
      }

      //----------------sold count pre process----------------
      foreach($profit_details as $pd)
      {
        $sold_count_per_product[$pd->getProductId()]+=$pd->getQty();
        $sold_count+=$pd->getQty();
      }
      ?>
    <tr>
      <td><?php echo $customer ?></td>
      <td><?php echo $invoice->getInvoiceTemplate() ?></td>
      <!--td><?php //echo $customer->getTinNo() ?></td-->
      <td><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php echo $invoice->getTransactionCode() ?></td>
      <td><?php 
        foreach($invoice_details as $id)
        {
          $productsoldcount=$sold_count_per_product[$id->getProductId()];
          $productitemcount=$item_count_per_product[$id->getProductId()];
          if($productsoldcount==$productitemcount)
            echo $id->getProduct()->getName()." <br> ";
          else
            echo link_to($id->getProduct()->getName(),"product/soldTo?id=".$id->getProductId())." <br> ";
        }
      ?></td>
      <td><?php //echo $invoice->getTotal() ?></td>
      <td align=right><?php if($invoice->getCash()!=0)echo MyDecimal::format($invoice->getCash()) ?></td>
      <td align=right><?php if($invoice->getChequeamt()!=0)echo MyDecimal::format($invoice->getChequeamt()) ?></td>
      <td align=right><?php if($invoice->getCredit()!=0 and $invoice->getStatus()!="Cancelled")echo MyDecimal::format($invoice->getCredit()) ?></td>
      <?php if(sfConfig::get('custom_show_product_category_totals_in_dsr')){?>
      <td align=right><?php if($invoice->getTotalForProductCategory()!=0 and $invoice->getStatus()!="Cancelled")echo MyDecimal::format($invoice->getTotalForProductCategory()) ?></td>
      <td align=right><?php if($invoice->getTotalForNoProductCategory()!=0 and $invoice->getStatus()!="Cancelled")echo MyDecimal::format($invoice->getTotalForNoProductCategory()) ?></td>
      <?php }?>
      <td><?php echo $invoice->getEmployee() ?></td>
      <td><?php 
/*
  if status=paid,
    if checkcleardate > today, 
      status = pending. 
    else 
      status = paid
*/
if($invoice->getStatus()=="Paid"){
$today=MyDateTime::today();
$checkcleardate=MyDateTime::frommysql($invoice->getCheckcleardate());
$status="Paid";
if($checkcleardate->islaterthan($today))$status="Check to clear on ".$checkcleardate->toshortdate();
echo $status;

}
else echo $invoice->getStatus();

?></td>
      <!-----------SOLD TO--------->
      <?php 
        switch ($sold_count) {
          case 0: $bgcolor=$red;break;
          case $item_count: $bgcolor=$green;break;
          default: $bgcolor=$yellow;break;
        }
        //if inspected, color is blue unless it's green
        if($bgcolor!=$green and $invoice->getIsInspected()==1)$bgcolor=$blue;
      ?>
      <td bgcolor="<?php echo $bgcolor;?>">
        <?php
          echo $sold_count."/".$item_count.": ";
          echo $invoice->getPonumber().": "; 
        ?>
        <?php 
          if($bgcolor!=$green)
          if($invoice->getIsInspected())
            echo link_to(" (Not Done)","invoice/uninspect?id=".$invoice->getId());
          else
            echo link_to(" (Done)","invoice/inspect?id=".$invoice->getId());
        ?>
      </td>
      <td><?php echo $invoice->getNotes() ?></td>
      <td><?php echo link_to("Edit","invoice/edit?id=".$invoice->getId()) ?></td>
      <td>
        <?php if($invoice->getHidden()==0)echo link_to("Hide ","invoice/hide?id=".$invoice->getId()) ?>
	  <?php if($invoice->getHidden()==1)echo link_to("Unhide ","invoice/unhide?id=".$invoice->getId()) ?>
      </td>
    </tr>
    <?php }?>
    <?php }?>
    <tr>
      <td></td>
    </tr>
  <?php }?>
</table>
<br>

<table>
  <tr>
    <td>Customer</td>
    <td>Tin No.</td>
    <td>Invoice No.</td>
    <td>Address</td>
  </tr>
<?php foreach($addresses as $address){ ?>
  <tr>
    <td><?php echo $address[0]?></td>
    <td><?php echo $address[1]?></td>
    <td><?php echo $address[2]?></td>
    <td><?php echo $address[3]?></td>
  </tr>
  <?php } ?>
</table>



