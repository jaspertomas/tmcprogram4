<h1>Sales Report by Salesman</h1>
<?php echo form_tag_for($form,"invoice/salesReportBySalesman")?>
<table>
  <tr>
    <td>Salesman</td>
    <td><?php echo $form["salesman_id"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
</form>

<br><br>

<h2><?php if($salesman!=null)echo $salesman->getName()?></h2>
<?php
$today=MyDateTime::frommysql("2018-01-01"); 
$dates=array();
$startofthismonth=$today->getstartofyear();
if(isset($salesman_id))
for($i=1;$i<=(12*6);$i++)
{
  $endofthismonth=$startofthismonth->getendofmonth();
  $dates[$i]=array("start"=>$startofthismonth->tomysql(),"end"=>$endofthismonth->tomysql());
  $startofthismonth=$startofthismonth->addmonths(1);
}
?>

<script>
  //this adds commas to format decimals
  $.fn.digits = function(){ 
      return this.each(function(){ 
          $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
      })
  }

  var datesstring="<?php echo str_replace('"','\"',json_encode($dates));?>";
  var dates=JSON.parse(datesstring);
  var totals=[];
  var salesman_id="<?php echo $salesman_id?>";

  $.each(dates, function(key,value) {
    var startdate=value["start"];
    var enddate=value["end"];
    $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/invoice/salesTotalBySalesmanIdApi?startdate="+startdate+"&enddate="+enddate+"&salesman_id="+salesman_id, success: function(result){
      var json = JSON.parse(result);
      var total = parseFloat(json["total"]).toFixed(2);
      total = Number(total).toLocaleString('en');
      $('#myTable').append('<tr><td>'+startdate+'</td><td>'+total+'</td></tr>');
    }});
  }); 
</script>

<div id="ajaxresult"></div>
<table id="myTable" border="1">
  <tbody>
      <tr><td>Date</td><td>Sales Total</td></tr>
  </tbody>
  <tfoot>
      <tr><td></td></tr>
  </tfoot>
</table>





