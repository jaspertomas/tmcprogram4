<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
  <?php echo $invoice; ?>  (<?php if($invoice->isDrBased())echo $invoice->releaseStatusString().", "?><?php echo $invoice->getIsTemporaryString()?>)
	<?php 
		if($invoice->getIsTemporary()==0 && $sf_user->hasCredential(array('admin','manager'),false))
			echo link_to("Undo Close","invoice/undoclose?id=".$invoice->getId()); 
		else if($invoice->getIsTemporary()==2 && $sf_user->hasCredential(array('admin', 'encoder', 'sales'),false))
			echo link_to("Check Out","invoice/checkout?id=".$invoice->getId()); 
		else if($invoice->getIsTemporary()==1 && $sf_user->hasCredential(array('admin', 'encoder', 'cashier'),false))
			echo link_to("Undo Check Out","invoice/undocheckout?id=".$invoice->getId()); 
		else if($invoice->getIsTemporary()==3)
		{
		  if($sf_user->hasCredential(array('manager','admin'),false))
		  {
			  echo link_to("Approve","invoice/approve?id=".$invoice->getId())." "; 
			  echo link_to("Deny","invoice/deny?id=".$invoice->getId())." "; 
		  }
		  if($sf_user->hasCredential(array('sales'),false))
		  {
  			echo link_to("Undo Check Out","invoice/undocheckout?id=".$invoice->getId()); 
		  }
		}
?>
</h1>
<?php slot('transaction_id', $invoice->getId()) ?>
<?php slot('transaction_type', "Invoice") ?>

<table>
  <tr valign=top>
    <td>
      <table>
        <tr valign=top>
          <td>Inv no.</td>
          <td>
            <?php 
              $invoice_template=$invoice->getInvoiceTemplate();
              if($sf_user->hasCredential(array('admin', 'encoder', 'cashier'), false))
              if($invoice->getIsTemporary()==1){
            		echo form_tag_for($form,'invoice/finalize'); 
            		echo $form['template_id'];
            		echo $form['invno'];
            		echo $form['id'];
            		?>
		            <br><input type="submit" value="Finalize">
	            </form>
            <?php 
              }else if(!$invoice_template->getIsInvoice() and $invoice->getIsTemporary()==0 and sfConfig::get('custom_transaction_can_become_invoice')){ //convert transaction to invoice
            		echo form_tag_for($form,'invoice/convertTransactionToInvoice'); 
            		echo $form['template_id'];
            		echo $form['invno'];
            		echo $form['id'];
            		?>
		            <br><input type="submit" value="Convert">
	            </form>
            <?php }else{echo $invoice_template." ".$invoice->getInvno();} ?>
          </td>
        </tr>
        <tr valign=top>
          <td>Transaction</td>
          <td><?php echo $invoice->getTransactionCode() ?></td>
        </tr>
          <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
          <?php echo form_tag_for($form,'invoice/adjust')?> <?php echo $form['id'] ?>
          <?php }?>	
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['date'] ?>

            <?php }?>	
          </td>
        </tr>
        <?php /*
        <tr valign=top>
          <td>Release Date</td>
          <td><?php echo MyDateTime::frommysql($invoice->getDatereleased())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['datereleased'] ?>
            <?php }?>	
          </td>
        </tr>
        */?>
        <!--tr>
          <td>PO No.</td>
          <td><?php //echo $invoice->getPonumber() ?></td>
        </tr-->
        <tr valign=top>
          <td>Customer</td>
          <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId(),array("target"=>"edit_customer")); ?> <?php echo link_to("(Choose)","invoice/chooseCustomer?id=".$invoice->getId())?> <?php if(sfConfig::get('custom_is_discount_based'))echo link_to("(Edit)","customer/edit?id=".$invoice->getCustomerId())?></td>
        </tr>
				<!--tr>
					<td>Cheque Data</td>
        <?php //$cheques=explode(", ",$invoice->getChequedata());foreach($cheques as $cheque)  {?>
					<td><?php //echo $cheque?></td>				
			  </tr>
				<tr>
					<td></td>				
			  <?php //} ?>
				</tr-->
        <tr valign=top>
          <td>Po No.</td>
          <td><?php echo $invoice->getPonumber() ?>
            <?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <?php echo $form['ponumber'] ?>
            <?php }?>	
          </td>
        </tr>
        <tr valign=top>
          <td>Technician</td>
          <td>
            <?php echo $invoice->getTechnician()?>
            <?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <?php 
                // echo $form['technician_id'] 
                //select from employees who are actually technicians
                echo '<select name="invoice[technician_id]" id="invoice_technician_id">';
                echo '<option value=""></option>';
                foreach(EmployeeTable::getTechnicians() as $technician)
                {
                  echo '<option value="'.$technician->getId().'" '.($technician->getId()==$form->getObject()->getTechnicianId()?'selected=selected':'').'>'.$technician->getName().'</option>';
                }
                echo '</select>';
              ?>
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td>Discount Level:</td>
          <td>
          <?php echo $invoice->getDiscountingString()?> 
          (<?php 
            if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin'), false)){
              echo link_to("Edit","invoice/discount?id=".$invoice->getId());
            }
          ?>)
          </td>
        </tr>
		</table>
		</td>
    <td>
      <table>
        <!--tr>
          <td>Due date</td>
          <td><?php //echo $invoice->getDuedate() ?></td>
        </tr-->
        <tr valign=top>
          <td>Sales Rep</td>
          <td><?php 
          // echo $invoice->getEmployee() 
          echo '<select name="invoice[salesman_id]" id="invoice_salesman_id">';
          foreach(EmployeeTable::getSalesmen() as $salesman)
          {
            echo '<option value="'.$salesman->getId().'" '.($salesman->getId()==$form->getObject()->getSalesmanId()?'selected=selected':'').'>'.$salesman->getName().'</option>';
          }
          echo '</select>';
          ?></td>
        </tr>
        <!--tr>
          <td>Discount Rate</td>
          <td><?php //echo $invoice->getDiscrate() ?></td>
        </tr>
        <tr>
          <td>Discount Amount</td>
          <td><?php //echo $invoice->getDiscamt() ?></td>
        </tr-->
        <tr>
          <td>Total</td>
          <td><?php echo $invoice->getTotal() ?></td>
        </tr>
		    <tr>
	        <td><b>Status</b></td>
	        <td>
            <font <?php echo $invoice->getFancyStatusColor();?>>
	        <?php echo $invoice->getFancyStatusString();?>
            </font>
          </td>
		    </tr>
        <?php if($invoice->getBalance()<0){?>
          <tr>
            <td></td>
            <td><?php echo link_to("Generate Refund Voucher","voucher/new?invoice_id=".$invoice->getId()); ?></td>
          </tr>
        <?php } ?>
        <tr>
          <td>Terms</td>
          <td><?php echo $invoice->getTerms() ?>
            <?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <?php echo $form['terms'] ?>
            <?php }?>	
          </td>
        </tr>
        <tr valign=top>
          <td>Due Date: </td>
          <td>
            <?php if($invoice->getStatus()=="Pending")echo MyDateTime::frommysql($invoice->getDuedate())->toshortdate() ?>
          </td>
        </tr>
        <tr valign=top>
          <td>Collection: </td>
          <td>
            <?php if($invoice->getStatus()=="Pending"){ ?>
              <?php if(!$invoice->isDue()){ ?>
                Not yet due
              <?php }else{ ?>
                <?php echo $invoice->getCollectionStatus() ?>
                <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
                  <?php echo $form['collection_status'] ?>
                <?php }?>	
              <?php } ?>
            <?php } ?>
        </td>
        </tr>
        <tr>
          <td>Notes</td>
          <td><?php echo $invoice->getNotes() ?>
            <?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <textarea rows="2" cols="20" name="invoice[notes]" id="invoice_notes"><?php echo $invoice->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <input type="submit" value="Save">
            <?php }?>	
          </td>
        </tr>
      </table>
</form>
    </td>
    <td>
      <table>
      <?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
        <tr>
          <td colspan=2>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
                  <?php if($invoice->getSaleType()!="Cash")echo link_to("Cash",'invoice/adjustSaleTypeCash?id='.$invoice->getId());else echo "Cash"; ?>
                  <?php echo link_to("Cheque",'invoice/adjustSaleTypeCheck?id='.$invoice->getId()); ?>
                  <?php if($invoice->getSaleType()!="Account")echo link_to("Account",'invoice/adjustSaleTypeAccount?id='.$invoice->getId());else echo "Account"; ?>
                  <?php echo link_to("Partial",'invoice/partialSaleType?id='.$invoice->getId()); ?>
      <?php }?>	
          </td>
        </tr>
      <?php }?>	
        <tr valign=top>
          <td>Sale Type</td>
          <td><?php echo $invoice->getSaleType() ?>
        </td>
        </tr>
        <tr valign=top>
          <td>Cash</td>
          <td><?php echo $invoice->getCash() ?></td>
        </tr>
        <tr valign=top>
          <td>Cheque/BT</td>
          <td><?php echo $invoice->getChequeamt() ?></td>
        </tr>
        <tr>
          <td>Current Balance</td>
          <td><?php echo $invoice->getBalance() ?></td>
        </tr>
        <?php if($check!=null){ ?>
        <tr>
          <td>Check Voucher</td>
          <td><?php echo $check->getCode() ?></td>
        </tr>
        <tr>
          <td>Pay To</td>
          <td><?php echo $check->getPassbook() ?></td>
        </tr>
        <?php } ?>
        <?php if($invoice->getChequeamt()>0){ ?>
        <tr>
          <td>Check No.</td>

          <td><?php echo $invoice->getCheque() ?> </td>
        </tr>
        <tr>
          <td>Check Date</td>
          <td><?php echo $invoice->getChequeDate()?MyDateTime::frommysql($invoice->getChequeDate())->toshortdate():"" ?></td>
        </tr>
        <?php } ?>
        <tr>
          <td>Unused Check Balance</td>
          <td><font <?php if($checksRemainingTotal>0)echo "color=red"?>><?php echo number_format($checksRemainingTotal,2) ?></font></td>
        </tr>
        <tr>
          <td colspan=2><?php echo link_to("View Payments","invoice/events?id=".$invoice->getId());
 ?></td>
        </tr>

		  </table>
    </td>
  </tr>
  <tr hidden=true id=invoice_edit_password_tr class=password_tr >
    <td align=center colspan=10>Enter manager password:<input id=invoice_edit_password_input type=password><input type=button value="Submit Password" id=invoice_edit_password_button></td>
  </tr> 
</table>

<?php if($invoice->getCommissionPaymentId()==null){ ?>
  Commission not yet paid
<?php }else{ ?>
  <?php echo link_to("Commission paid","commission_payment/view?id=".$invoice->getCommissionPaymentId()) ?>
<?php } ?>
<br>

Related DRs:
<?php foreach($invoice->getInvoiceDr() as $ref){?>
  <?php if($ref->getInvoiceReturnId())continue; ?>
  <?php echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".link_to($ref,"invoice_dr/view?id=".$ref->getId())." ".MyDateTime::fromdatetime($ref->getDatetime())->toshortdate();?> 
  <?php if($ref->isCancelled()){echo "<font color=red>(Cancelled)</font>";?>
  <?php } else { ?>
    <?php if($ref->getQtyForRelease()==0 and $ref->getQtyForReturn()==0)echo "(Empty)"?>
    <?php if($ref->getQtyForRelease()>0)echo "(".$ref->getQtyForRelease()." ".($ref->isReleased()?"Released":"Pending Release").")"?>
    <?php if($ref->getQtyForReturn()>0)echo "(".$ref->getQtyForReturn()." ".($ref->isReleased()?"Returned":"Pending Return").")"?>
  <?php } ?>
<?php } ?>

<br>
Related POs:
<?php foreach($invoice->getPurchases() as $ref){?>
  <?php echo link_to($ref,"purchase/view?id=".$ref->getId())?>&nbsp;
<?php } ?>

<br>
Related Returns:
<?php foreach($invoice->getReturns() as $ref){?>
  <?php echo link_to($ref,"returns/view?id=".$ref->getId())?>&nbsp;
<?php } ?>
<br>
Related Replacements:
<?php foreach($invoice->getReplacement() as $ref){?>
  <?php echo link_to($ref,"replacement/view?id=".$ref->getId())?>&nbsp;
<?php } ?>

<?php foreach($invoice->getInvoiceReturn() as $ir){?>
  <?php echo "<br>&emsp;".link_to($ir,"invoice_return/view?id=".$ir->getId())." ".MyDateTime::frommysql($ir->getDate())->toshortdate()?>
  <?php foreach($ir->getInvoiceDr() as $dr){?>
    <?php echo "<br>&emsp;&emsp;".link_to($dr,"invoice_dr/view?id=".$dr->getId())." ".MyDateTime::fromdatetime($dr->getDatetime())->toshortdate();?> 
    <?php if($dr->isCancelled()){echo "<font color=red>(Cancelled)</font>";?>
    <?php } else { ?>
      <?php if($dr->getQtyForRelease()==0 and $dr->getQtyForReturn()==0)echo "(Empty)"?>
      <?php if($dr->getQtyForRelease()>0)echo "(".$dr->getQtyForRelease()." ".($dr->isReleased()?"Released":"Pending Release").")"?>
      <?php if($dr->getQtyForReturn()>0)echo "(".$dr->getQtyForReturn()." ".($dr->isReleased()?"Returned":"Pending Return").")"?>
    <?php } ?>
  <?php } ?>
<?php } ?>

<br>Related Refund Vouchers:
<?php foreach($invoice->getVouchers() as $ref){?>
  <?php echo "<br>&emsp;".link_to($ref,"voucher/view?id=".$ref->getId())." P".MyDecimal::format($ref->getAmount())?>&nbsp;
<?php } ?>

<br>
<br>
            <?php if($invoice->getIsTemporary()!=0)echo link_to("Edit","invoice/edit?id=".$invoice->getId(),array("id"=>"invoice_edit")) ?> |
            <?php if($invoice->getIsTemporary()>0)echo link_to('Delete','invoice/delete?id='.$invoice->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
            <?php echo link_to("View Files","invoice/files?id=".$invoice->getId());?> |

          <?php 
            //if invoice is closed, is "new version" (DRs enabled) and is not cancelled
            if($invoice->getIsTemporary()==0 and $invoice->getIsDrBased()==1 and $invoice->getStatus()!="Cancelled"){
          ?>
            <?php //echo link_to("Generate PO","invoice/genPO?id=".$invoice->getId());?> |
            <?php //if($invoice->getIsTemporary()==0)echo link_to("Generate Return","returns/new?ref_id=".$invoice->getId()."&ref_class=Invoice");?> |
            <?php echo link_to("Generate DR","invoice_dr/new?invoice_id=".$invoice->getId());?> |
            <br>
            <?php echo link_to("Print","invoice/viewPdf?id=".$invoice->getId());?> |
            <?php echo link_to("Print Invoice","invoice/viewPrintable?id=".$invoice->getId());?> |
            <?php echo link_to("Print Statement of Account","customer/viewStatementDmf?id=".$invoice->getCustomer()->getId()) ?> | 
            <br><?php echo link_to("Print Transaction Statement","invoice/viewStatementDmf?id=".$invoice->getId()) ?>

            <br><?php echo link_to("Return and Replace","invoice_return/new?return_type=Replace&invoice_id=".$invoice->getId()) ?> |
            | <?php //if($sf_user->hasCredential(array('admin'), false))echo link_to("Return and Refund","invoice_return/new?return_type=Refund&invoice_id=".$invoice->getId()) ?> |
            | <?php if($invoice->getBalance()<0)echo link_to("Generate Refund Voucher","voucher/new?invoice_id=".$invoice->getId());?>
          <?php } ?>
<?php
//if user is salesman or encoder
if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){
?>
<?php //echo link_to("Add Detail","invoicedetail/new?invoice_id=".$invoice->getId()) ?>

<hr>

<?php if($invoice->getIsTemporary()!=0 and !sfConfig::get('custom_is_discount_based')){
//--------------------------start is not discount based-----------------------
?>
<b>Search product:</b> <input id=invoiceproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=invoiceclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@invoicedetail",array("id"=>"new_invoice_detail_form")); ?>
<input type=hidden name=invoicedetail[invoice_id] value=<?php echo $invoice->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Product</td>
		<td>Qty</td>
		<td>Price</td>
		<td>+Commission</td>
		<td><?php if(sfConfig::get('custom_non_admins_can_discount') or $sf_user->hasCredential(array('admin'),false)){?>Discounted<?php } ?></td>
		<!--td>Discrate</td-->
		<td>Notes</td>
	</tr>

	<tr>
		<td>
      <input hidden=true name=invoicedetail[barcode] id=invoicedetail_barcode>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<td><input size="5" name="invoicedetail[qty]" value="1" id="invoicedetail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="8" name="invoicedetail[price]" value="0.00" id="invoicedetail_price" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="8" name="invoicedetail[with_commission]" value="0.00" id="invoicedetail_with_commission" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td align=center><?php if(sfConfig::get('custom_non_admins_can_discount') or $sf_user->hasCredential(array('admin'),false)){?><input type=checkbox name=invoicedetail[is_discounted] id=chk_is_discounted <?php if(!$product_is_set)echo "disabled=true"?>><?php } ?></td>
		<td><input name="invoicedetail[description]" id="invoicedetail_description" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=invoice_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> >
</td>
	</tr>

	<!--tr>
	  <td>Barcode: <?php //echo $detailform["barcode"]; ?></td>
	</tr-->
	<input type=hidden id="product_allow_zeroprice" value="<?php echo $detailform->getObject()->getProduct()->getIsAllowZeroprice()?"true":"false";?>">
	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>
</form>


<?php 
//--------------------------end is not discount based-----------------------
}else if($invoice->getIsTemporary()!=0){
//--------------------------start is_discount_based-------------------------
?>

<b>Search product:</b> <input id=invoiceproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=invoiceclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@invoicedetail",array("id"=>"new_invoice_detail_form")); ?>
<input type=hidden name=invoicedetail[invoice_id] value=<?php echo $invoice->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Product</td>
		<td>Qty</td>
		<td>Price</td>
		<td>Is Discounted</td>
		<td>Discount Rate</td>
		<td>Notes</td>
	</tr>

	<tr>
		<td>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<td><input size="5" name="invoicedetail[qty]" value="1" id="invoicedetail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="8" name="invoicedetail[price]" value="0.00" id="invoicedetail_price" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td align=center><?php if(sfConfig::get('custom_non_admins_can_discount') or $sf_user->hasCredential(array('admin'),false)){?><input type=checkbox name=invoicedetail[is_discounted] id=chk_is_discounted <?php if(!$product_is_set)echo "disabled=true"?>><?php } ?></td>
		<td><input size="5" name="invoicedetail[discrate]" value="<?php echo $invoice->getCustomer()->getDiscrate()?>" id="invoicedetail_discrate" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input name="invoicedetail[description]" id="invoicedetail_description" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=invoice_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> ></form>
</td>
	</tr>

	<input type=hidden id="product_allow_zeroprice" value="<?php echo $detailform->getObject()->getProduct()->getIsAllowZeroprice()?"true":"false";?>">
	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>

<?php }//-------------end is_discount_based-------------------- ?>

<?php } ?>

<div id="invoicesearchresult"></div>

<hr>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo "<br>".form_tag_for($customerform,'customer/adjust')?> 
          <?php echo $customerform['id'] ?>
<?php } ?>    
<table>
  <tr>
    <td colspan=2><font size=5 face=arial><b>TRADEWIND WATER SYSTEM AND INDUSTRIAL MACHINERY INC.</b></font></td>
  </tr>
  <tr>
    <td>CASH INVOICE</td>
    <td align=right>Date: <u><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate()?></u></td>
  </tr>
  <tr>
    <td colspan=2>Sold to: <u><?php echo $invoice->getCustomer()?></u></td>
  </tr>
  <tr>
    <td colspan=2>Address: <u><?php echo $invoice->getCustomer()->getAddress()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['address'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Address2: <u><?php echo $invoice->getCustomer()->getAddress2()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['address2'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Phone: <u><?php echo $invoice->getCustomer()->getPhone1()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['phone1'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Email: <u><?php echo $invoice->getCustomer()->getEmail()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['email'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>Representative: <u><?php echo $invoice->getCustomer()->getRep()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['rep'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr>
    <td colspan=2>TIN/SC-TIN: <u><?php echo $invoice->getCustomer()->getTinNo()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <?php echo $customerform['tin_no'] ?>
<?php } ?>    
    </td>
  </tr>
  <tr valign=top>
    <td colspan=2>Notes: <u><?php echo $invoice->getCustomer()->getNotepad()?></u>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
          <br><textarea cols=100 rows=2 name="customer[notepad]" id="customer_notepad" type="text"><?php echo $invoice->getCustomer()->getNotepad()?></textarea>
  <?php } ?>    
    </td>
  </tr>
</table>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
  <input type="submit" value="Save">
  </form>
<?php } ?>    

<!-----invoice detail table------>

<?php if(!sfConfig::get('custom_is_discount_based')){
//--------------------------start is not discount based-----------------------
?>

<?php 
  $totalcommission=0; 
  $totalsalewithcommission=0; 
  $totalsale=0; 
  $vatentryexists=false; 
  $commissionexists=false; 
  
  //check if commission is forced on
  if($commissionOn)
	    $commissionexists=true;
  //check if commission exists in any invoice detail
  else
	foreach($invoice->getInvoicedetails() as $detail){
	  if($detail->getIsVat()==1);
	  else if($detail->getWithCommission()!=$detail->getPrice())
	    $commissionexists=true;
  }  
?>
<table border=1>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <?php if($commissionexists){?>
    <td colspan=2 align=center>With Commission</td>
    <?php } ?>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <!--td>Barcode</td-->
    <td>Qty</td>
    <td>Unit</td>
    <td width=50%>Description</td>
    <td>Discount Rate</td>
    <?php if($commissionexists){?>
    <td>Unit Price</td>
    <td>Subtotal</td>
    <?php } ?>
    <td <?php if($commissionexists){?>bgcolor=pink <?php } ?>>Unit Price</td>
    <td <?php if($commissionexists){?>bgcolor=pink <?php } ?>>Subtotal</td>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td></td>
    <td></td>
    <td>Notes</td>
    <?php if($invoice->isDrBased()){?>
    <td>Unreleased Qty</td>
    <?php } ?>
    <?php if($sf_user->hasCredential(array('admin'), false)){?>
    <td>Max Sell Price</td>
    <td>Min Sell Price</td>
    <td>Max Buy Price</td>
    <td>Min Buy Price</td>
    <?php } ?>
<?php } ?>
  </tr>
  <?php 
  	$counter=0;
  	foreach($invoice->getInvoicedetails() as $detail){
  		$counter++;?>
  <tr>
    <!--td><?php echo $detail->getBarcode() ?></td-->
    <td>
      <?php echo $detail->getQty() ?>

      <?php if($invoice->getIsTemporary()!=0){ ?>
      <?php echo form_tag("invoicedetail/setQty");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value>
      </form>
      <?php } ?>

    </td>
    <td></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?> <?php if($sf_user->hasCredential(array('admin'), false))echo link_to("(Edit)","product/edit?id=".$detail->getProductId())?></td>

    <td align=center>
      <?php echo "Less <br>".$detail->getDiscrate() ?>
      <?php if($invoice->getIsTemporary()!=0){ ?>
      <?php echo form_tag("invoicedetail/setDiscrate");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value>
      </form>
      <?php } ?>
    </td>

    <?php if($commissionexists){?>
    <td align=right>
      <?php echo $detail->getWithCommission(); if(!$detail->getIsVat())$totalcommission+=$detail->getWithCommissionTotal()-$detail->getTotal();?>

        <?php if($invoice->getIsTemporary()!=0){ ?>
        <?php echo form_tag("invoicedetail/setWithCommission");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=3 name=value>
        </form>
        <?php } ?>
      
    </td>
    <td align=right><?php echo $detail->getWithCommissionTotal(); if(!$detail->getIsVat())$totalsalewithcommission+=$detail->getWithCommissionTotal();else $vatentryexists=true; ?></td>
    <?php } ?>

    <td align=right <?php if($commissionexists){?>bgcolor=pink <?php } ?>>
      <?php echo $detail->getPrice() ?>
      <?php if($invoice->getIsTemporary()!=0){ ?>
      <?php echo form_tag("invoicedetail/setPrice");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input type=hidden id=commissionexists name=commissionexists value=<?php echo $commissionexists ?>>
      <input id=value size=3 name=value>
      </form>
      <?php } ?>
    </td>
    <td align=right <?php if($commissionexists){?>bgcolor=pink <?php } ?>><?php echo $detail->getTotal(); $totalsale+=$detail->getTotal(); ?></td>

    <!--td><?php //echo link_to("Price List","producttype/view?id=".$detail->getProduct()->getProducttypeId()) ?></td-->
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td>
      <?php if($invoice->getIsTemporary()!=0)echo link_to("Edit","invoicedetail/edit?id=".$detail->getId(),array("class"=>"invoice_detail_edit","detail_id"=>$detail->getId())) ?>
      <br><br><?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'encoder'),false))echo link_to("Change Product","invoicedetail/changeProduct?id=".$detail->getId()) ?>
    </td>
    <td>
<?php if($invoice->getIsTemporary()!=0)echo link_to(
  'Delete',
  'invoicedetail/delete?id='.$detail->getId(),
  array('method' => 'delete', 'confirm' => 'Are you sure?')
) ?>

    </td>
    <!--td><?php //echo link_to("Edit Product","product/edit?id=".$detail->getProductId()) ?></td-->
    <td><?php echo $detail->getDescription() ?></td>
    <?php if($invoice->isDrBased()){?>
    <td align=center>
      <?php 
        $remaining=$detail->getQty()-$detail->getQtyReleased();
        if($remaining!=0)echo "<font color=red>".$remaining."</font>";
      ?>
    </td>
    <?php } ?>
    <?php if($sf_user->hasCredential(array('admin'), false)){?>
    <td><font color=<?php echo $detail->getIsDiscounted()?"green":"black"?>><?php echo $detail->getProduct()->getMaxsellprice()?></font></td>
    <td><font color=<?php echo $detail->getIsDiscounted()?"green":"black"?>><?php echo $detail->getProduct()->getMinsellprice()?></font></td>
    <td><font color=<?php echo $detail->getIsDiscounted()?"green":"black"?>><?php echo $detail->getProduct()->getMaxbuyprice()?></font></td>
    <td><font color=<?php echo $detail->getIsDiscounted()?"green":"black"?>><?php echo $detail->getProduct()->getMinbuyprice()?></font></td>
    <?php } ?>
  </tr>
  <tr hidden=true class="password_tr invoice_detail_edit_password_tr" id=invoice_detail_edit_password_tr_<?php echo $detail->getId()?>>
    <td align=center colspan=10>Enter manager password:<input class=invoice_detail_edit_password_input id=invoice_detail_edit_password_input_<?php echo $detail->getId()?> type=password><input type=button value="Submit Password" class=invoice_detail_edit_password_button id=invoice_detail_edit_password_button_<?php echo $detail->getId()?>></td>
  </tr> 
  <?php }?>
  <?php }?>
  
  <?php if(!$vatentryexists and $totalcommission!=0 and $invoice->getIsTemporary()!=0){//do this if no vat entry is detected?>
  <tr>
  <td colspan=6 align=center>Total Vat On Commission: P<?php echo $totalcommission*.12?> (<?php echo link_to("Auto Create Vat entry","invoicedetail/createVatEntry?amount=".$totalcommission*.12."&invoice_id=".$invoice->getId());?>)</td>
  </tr>
  <?php } ?>
  
  <?php
  $pinkcolumns="";
  if($commissionexists)
    $pinkcolumns="<td bgcolor=pink></td><td bgcolor=pink>";
  while($counter<10){$counter++;echo "<tr><td>&nbsp;</td><td></td><td></td><td></td><td></td></td><td>".$pinkcolumns."</td></tr>";}
  ?>
  <tr align=right>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td>Total Sales (net of VAT)</td>

    <?php if($commissionexists){?>
	  <td></td>
	  <td><?php $withcommisionvat=$totalsalewithcommission/1.12;echo number_format($withcommisionvat,2,".",",")?></td>
    <?php } ?>
	  <td <?php if($commissionexists){?> bgcolor=pink <?php } ?>></td>
	  <td <?php if($commissionexists){?> bgcolor=pink <?php } ?>><?php $vat=$totalsale/1.12;echo number_format($vat,2,".",",")?></td>
  </tr>
  <tr align=right>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td>Add 12% VAT</td>
    <?php if($commissionexists){?>
	  <td></td>
	  <td><?php echo number_format($totalsalewithcommission-$withcommisionvat,2,".",",")?></td>
    <?php } ?>
	  <td <?php if($commissionexists){?> bgcolor=pink <?php } ?>></td>
	  <td <?php if($commissionexists){?> bgcolor=pink <?php } ?>><?php echo number_format($totalsale-$vat,2,".",",")?></td>
  </tr>
  <tr align=right>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td>Amount Received</td>
    <?php if($commissionexists){?>
	  <td></td>
	  <td><?php echo number_format($totalsalewithcommission,2,".",",")?></td>
    <?php } ?>
	  <td <?php if($commissionexists){?> bgcolor=pink <?php } ?>></td>
	  <td <?php if($commissionexists){?> bgcolor=pink <?php } ?>><?php echo number_format($totalsale,2,".",",")?></td>
  </tr>
</table>

<hr>

Total Sale With Commission: <?php echo $totalsalewithcommission?>
<br>Total Commission: <?php echo $totalcommission?>
<br>Total Vat On Commission: <?php echo $totalcommission*.12?>
<br>Commission Less Vat: <?php echo $totalcommission*.88?>
<?php 
  if(!$commissionexists)echo "<br>".link_to("Enable Commission","invoice/view?commission=on&id=".$invoice->getId());
  else echo "<br>".link_to("Disable Commission","invoice/view?id=".$invoice->getId());
?>

<?php 
//--------------------------end is not discount based-----------------------
}else{
//--------------------------start is_discount_based-------------------------
?>

<table border=1>
  <tr>
    <td>Qty</td>
    <td>Product</td>
    <td>Price</td>
    <td>Disc Rate</td>
    <td>Disc Price</td>
    <td>Total</td>
    <td>Notes</td>
    <td></td>
    <td></td>
  </tr>
  <?php 
  	foreach($invoice->getinvoicedetails() as $detail){?>
  <tr>
    <td>
      <?php echo $detail->getQty() ?>
      <?php if($invoice->getIsTemporary()!=0){ ?>
        <?php echo form_tag("invoicedetail/setQty");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=3 name=value value="<?php echo $detail->getQty() ?>">
        </form>
      <?php } ?>
    </td>
    <td>
      <?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?>
    </td>
    <td align=right>
      <?php echo $detail->getPrice() ?>
      <?php if($invoice->getIsTemporary()!=0){ ?>
        <?php echo form_tag("invoicedetail/setPrice");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=3 name=value value=<?php echo $detail->getPrice()?>>
        </form>
      <?php } ?>
    </td>
    <td>
      <?php echo $detail->getDiscrate() ?>
      <?php if($invoice->getIsTemporary()!=0){ ?>
        <?php echo form_tag("invoicedetail/setDiscrate");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=3 name=value value="<?php echo $detail->getDiscrate() ?>">
        </form>
      <?php } ?>
    </td>
    <td align=right>
      <?php echo MyDecimal::format($detail->getDiscprice()) ?>
    </td>
    <td align=right>
      <?php echo MyDecimal::format($detail->getTotal()) ?>
    </td>
    <td>
      <?php echo $detail->getDescription() ?>
      <?php if($invoice->getIsTemporary()!=0){ ?>
        <?php echo form_tag("invoicedetail/setDescription");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input id=value size=10 name=value>
        </form>
      <?php } ?>
    </td>
    <?php {?>
      <td>
        <?php if($invoice->getIsTemporary()!=0)echo link_to('Delete','invoicedetail/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
      </td>
    <?php }?>
  </tr>
  <?php }?>
</table>


<?php }//-------------end is_discount_based-------------------- ?>

<hr>

<h2>Conversions</h2>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Product</td>
    <td>Notes</td>
    <td>Reset</td>
    <td></td>
  </tr>
  <?php 
  	foreach($invoice->getDeliveryconversion() as $detail){?>
  <tr>
    <td>
      <?php //echo form_tag("deliveryconversion/setQty");?>
      <?php echo $detail->getQty() ?>
      <!--
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value>
      </form>
      -->
    </td>
    <td>
      <?php echo link_to($detail->getConversion(),"conversion/view?id=".$detail->getConversionId()) ?>
    </td>
    <td>
      <?php echo form_tag("deliveryconversion/setDescription");?>
      <?php echo $detail->getDescription() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=10 name=value>
      </form>
    </td>
    <td>
    <?php if($invoice->getIsTemporary()!=0 && $sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
        <?php echo form_tag("deliveryconversion/reset");?>
        <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
        <input type=submit value="Reset">
        </form>
        <?php //echo link_to('Delete','deliveryconversion/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
    <?php }?>
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td colspan=2>
      <?php 
        foreach($detail->getDeliverydetails() as $dd)
          if($dd->getQty()<0)
            echo $dd->getQty()." x ".$dd->getProduct()->getName()."<br>";
        foreach($detail->getDeliverydetails() as $dd)
          if($dd->getQty()>0)
            echo $dd->getQty()." x ".$dd->getProduct()->getName()."<br>";
      ?>
    </td>
  </tr>
  <?php }?>
</table>


<hr>

<table>
	<tr>
          <td><?php echo link_to("View Payments","invoice/events?id=".$invoice->getId()) ?></td>
      <td><?php echo link_to("Cash Collection","event/new?parent_class=Invoice&parent_id=".$invoice->getId()."&type=CashCollect") ?></td>
      <td>
        Cheque Collection P
      </td>
      <td>
        <?php echo form_tag("in_check/new?invoice_id=".$invoice->getId());?>
        <input name=chequeamt id=chequeamt size=5>
        <input type=submit value=Submit>
        </form>
      </td>
      <td><?php //echo link_to("Bank Expense","event/new?parent_class=Invoice&parent_id=".$invoice->getId()."&type=BankExp") ?></td>
  </tr> 
</table>
<table>
	<tr>
	      <td><?php if($invoice->getStatus()!="Cancelled")echo link_to("Cancel","invoice/cancel?id=".$invoice->getId(),array("id"=>"invoice_cancel", 'confirm' => 'Are you sure?')) ?></td>
    <td hidden=true class=password_tr id=invoice_cancel_password_tr align=center colspan=10>Enter manager password:<input id=invoice_cancel_password_input type=password><input type=button value="Submit Password" id=invoice_cancel_password_button></td>
  </tr> 
</table>
<table>
	<tr>
	      <td><?php if($invoice->getStatus()=="Cancelled")echo link_to("Undo Cancel","invoice/undocancel?id=".$invoice->getId(),array("id"=>"invoice_undocancel", 'confirm' => 'Are you sure?')) ?></td>
    <td hidden=true class=password_tr id=invoice_undocancel_password_tr align=center colspan=10>Enter manager password:<input id=invoice_undocancel_password_input type=password><input type=button value="Submit Password" id=invoice_undocancel_password_button></td>
  </tr> 
</table>
<table>
	<tr>
	      <td><?php if($invoice->getStatus()!="Cancelled" and $invoice->getIsTemporary()==0 and $sf_user->hasCredential(array('admin'), false))echo link_to("Undo Finalize","invoice/undoclose?id=".$invoice->getId(),array("id"=>"invoice_undoclose")) ?></td>
    <td hidden=true class=password_tr id=invoice_undoclose_password_tr align=center colspan=10>Enter manager password:<input id=invoice_undoclose_password_input type=password><input type=button value="Submit Password" id=invoice_undoclose_password_button></td>
  </tr> 
</table>
<?php if($sf_user->hasCredential(array('admin'), false))echo link_to("Recalculate","invoice/recalc?id=".$invoice->getId()) ?>

<script type="text/javascript">
function changeText(id)
{
var x=document.getElementById("mySelect");
x.value=id;
}
var manager_password="<?php 
	    $setting=Doctrine_Query::create()
	        ->from('Settings s')
	      	->where('name="manager_password"')
	      	->fetchOne();
      	if($setting!=null)echo $setting->getValue();
?>";
//on qty get focus, select its contents
$("#invoicedetail_qty").focus(function() { $(this).select(); } );
$("#invoiceproductsearchinput").focus(function() { $(this).select(); } );
//set price textbox to read only
//$("#invoicedetail_price").prop('readonly', true);
//set product name
//$("#invoiceproductsearchinput").prop('value', $("#product_name").val());
//set price to default price
$("#invoicedetail_price").prop('value', $("#product_price").val());
//select invno if cashier
//else set focus on product search input
var is_cashier=<?php echo $sf_user->hasCredential(array('cashier'),false)?"true":"false"?>;
var is_admin=<?php echo $sf_user->hasCredential(array('admin'),false)?"true":"false"?>;
if(is_admin)
{
$("#invoiceproductsearchinput").focus();
$("#invoiceproductsearchinput").select(); 
}
else if(is_cashier)
{
$("#invoice_invno").focus();
$("#invoice_invno").select(); 
}
else
{
$("#invoiceproductsearchinput").focus();
$("#invoiceproductsearchinput").select(); 
}
//if no product id set, disable save button
if($("#invoicedetail_product_id").val()=='')	 		  
  $("#invoice_detail_submit").prop("disabled",true);
//------Invoice Discount-------------------
//on page ready, var discounted is false
var discounted=false;
//set checkbox to false as default
$("#chk_is_discounted").prop('checked',false);
//hide all password entry boxes
$(".password_tr").attr('hidden',true);

//------Invoice (not header) product search-----------
//$("#invoiceproductsearchinput").keyup(function(){
//$("#invoiceproductsearchinput").on('input propertychange paste', function() {
$("#invoiceproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#invoice_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#invoiceproductsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#invoiceproductsearchinput").val();
      //if searchstring is all numbers except last letter, 
      //and length is 8
      //this is a barcode. 
      if(
        $.isNumeric(searchstring.slice(0, -1))
        &&
        searchstring.length>=8
        )
      {
        processBarcode(searchstring);
      }
      //else do ajax to product search, display result in invoicesearchresult
      else
      {
	      $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+searchstring+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	   		  $("#invoicesearchresult").html(result);
	      }});
      }
    }
    //else clear
    //else
 		//  $("#invoicesearchresult").html("");
});
$("#invoiceclearsearch").click(function(){
 		  $("#invoicesearchresult").html("");
});
//------Invoice Edit-------------------
$('#invoice_edit').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#invoice_edit_password_tr").attr('hidden',false);
});
$('#invoice_edit_password_button').click(function(event) {
	//get entered password value
    var pass=$("#invoice_edit_password_input").val();
    if (pass==manager_password){
	    window.location = $('#invoice_edit').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//----Invoice Cancel-------------------------------
/*
$('#invoice_cancel').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#invoice_cancel_password_tr").attr('hidden',false);
});
*/
$('#invoice_cancel_password_button').click(function(event) {
	//get entered password value
    var pass=$("#invoice_cancel_password_input").val();
    if (pass==manager_password){
	    window.location = $('#invoice_cancel').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//----Invoice Undo Cancel-------------------------------
/*
$('#invoice_undocancel').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#invoice_undocancel_password_tr").attr('hidden',false);
});
$('#invoice_undocancel_password_button').click(function(event) {
	//get entered password value
    var pass=$("#invoice_undocancel_password_input").val();
    if (pass==manager_password){
	    window.location = $('#invoice_undocancel').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
*/
//----Invoice Undo Close-------------------------------
$('#invoice_undoclose').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#invoice_undoclose_password_tr").attr('hidden',false);
});
$('#invoice_undoclose_password_button').click(function(event) {
	//get entered password value
    var pass=$("#invoice_undoclose_password_input").val();
    if (pass==manager_password){
	    window.location = $('#invoice_undoclose').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//--------Invoice Detail Edit---------------------------
var invoice_detail_edit_href="";
var detail_id="";
$('.invoice_detail_edit').click(function(event) {
	//prevent form from sending
    event.preventDefault();
    //extract detail->getId() value
    detail_id=$(this).attr('detail_id');
    //save href value for later use
    invoice_detail_edit_href=$(this).attr('href');
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide password entry textbox
    $("#invoice_detail_edit_password_tr_"+detail_id).attr('hidden',false);
});
$('.invoice_detail_edit_password_button').click(function(event) {
	//get entered password value
    var pass=$("#invoice_detail_edit_password_input_"+detail_id).val();
    if (pass==manager_password){
	    window.location = invoice_detail_edit_href;
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//---------New Invoice Detail Submit-----------------------------
//on submit new invoice detail form
$("#new_invoice_detail_form").submit(function(event){

  //this is when the cursor is on qty and the barcode reader is used
  //determine if qty is actually a barcode
  if(
    //if qty is too long to be a quantity, this is probably a barcode
    $("#invoicedetail_qty").val().length>=8 && 
    //but only if qty is enabled
    $("#invoicedetail_qty").attr("disabled")!="disabled")
  {
    processBarcode($("#invoicedetail_qty").val());
    return false;
  } 

  //if discount checkbox is checked, go ahead with submit
  if($("#chk_is_discounted").prop('checked'))return true;

  //determine minimum product selling price
  var price=$("#invoicedetail_price").prop('value');
  var maxprice=$("#product_price").val();
  var minprice=$("#product_min_price").val();
  var allow_zeroprice=$("#product_allow_zeroprice").val()=='';
  //if(minprice==0)minprice=$("#product_price").val();

  //if custom setting fixed_price_or_free, meaning valid values are >=maxsellprice or 0
  //this policy first used in tacloban
  var discountmessage="<?php if(sfConfig::get('custom_non_admins_can_discount'))echo "\\n\\nPlease check Discounted check box to request for discount, and notify manager." ?>";
  if(<?php echo sfConfig::get('custom_fixed_price_or_free')?"true":"false"?> || allow_zeroprice){
    if(parseFloat(price)<parseFloat(maxprice) && parseFloat(price)!=0)
    {
      alert("Selling price is "+maxprice+". "+discountmessage+" \n\nOr set price to 0 for free item.");
      return false;
    }
  }
  //else if custom setting fixed_price=true
  else if(<?php echo sfConfig::get('custom_fixed_price')?"true":"false"?>){
    if(parseFloat(price)<parseFloat(maxprice))
    {
      alert("Selling price is "+maxprice+". "+discountmessage);
      return false;
    }
  }
  //else not fixed price, allow price >= minsellprice
  else{
    //validate by min price
    //if price is less than min price, complain and don't submit
    if(parseFloat(price)<parseFloat(minprice))
    {
      alert("Minimum selling price is "+minprice+". "+discountmessage);
      return false;
    }
  }

});
//------ BARCODE -----------------------------
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
function processBarcode(barcode)
{
        //
        if(barcode.length<8)
        {
          barcode=pad(barcode, 8);
        }

		$("#pleasewait").html("<font color=red><b>PLEASE WAIT</b></font>");
	      $.ajax
	      ({
	        url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/invoice/barcodeEntry?barcode="+barcode, 
	        dataType: "json",
	        success: function(result)
	        {
			$("#pleasewait").html("");
	          //if product found
	          if(result[0]!=0)
	          {
              //invoidetail input controls disabled=false
              $("#chk_is_discounted").removeAttr('disabled');
              $("#invoicedetail_description").removeAttr('disabled');
              $("#invoice_detail_submit").removeAttr('disabled');
              $("#invoicedetail_qty").removeAttr('disabled');
              $("#invoicedetail_price").removeAttr('disabled');
              $("#invoicedetail_with_commission").removeAttr('disabled');
              $("#invoicedetail_discrate").removeAttr('disabled');

	            //populate values
	            $("#invoiceproductsearchinput").val("");
              $("#invoicedetail_qty").val(1);
              $("#invoicedetail_product_id").val(result[0]);//product_id
              $("#productname").html(result[1]);//product name
              $("#invoicedetail_price").val(result[2]);//maxsellprice
              $("#invoicedetail_with_commission").val(0);
              $("#chk_is_discounted").attr("checked",false);
              $("#invoicedetail_description").val("");
              
              $("#product_allow_zeroprice").val(result[4]);//is_allow_zeroprice
              $("#product_min_price").val(result[3]);//minsellprice
              $("#product_price").val(result[2]);//maxsellprice
              $("#product_name").val(result[1]);//product name
              $("#invoicedetail_barcode").val(barcode);
              
              //set focus to qty
              $("#invoicedetail_qty").focus();
	          }
	          else
	          {
	            alert("Product not found");
              $("#invoicedetail_qty").val("");
	          }
	        }
	      });

}

//this is called when user clicks "Choose" button in product search form
function populateSubform(product_id,product_name,price,min_price,allow_zeroprice) {
  $("#product_allow_zeroprice").val(allow_zeroprice);
  $("#product_min_price").val(min_price);
  $("#product_price").val(price);
  $("#product_name").html(product_name);

  $("#invoicedetail_price").val(price);
  $("#invoicedetail_with_commission").val(0);
  $("#productname").html(product_name);
  $("#invoicedetail_product_id").val(product_id);
  $("#invoicesearchresult").html("");

  $("#chk_is_discounted").removeAttr("disabled");
  $("#invoicedetail_description").removeAttr("disabled");
  $("#invoice_detail_submit").removeAttr("disabled");
  $("#invoicedetail_qty").removeAttr("disabled");
  $("#invoicedetail_price").removeAttr("disabled");
  $("#invoicedetail_with_commission").removeAttr("disabled");
  $("#invoicedetail_discrate").removeAttr('disabled');

  $("#invoicedetail_qty").focus();
}
</script>
<!--
          <td><?php //echo link_to("Cheque Collection","event/new?parent_class=Invoice&parent_id=".$invoice->getId()."&type=ChequeCollect") ?></td>
          <td><?php //echo link_to("Cash Collection","event/new?parent_class=Invoice&parent_id=".$invoice->getId()."&type=CashCollect") ?></td>
          <td><?php //echo link_to("Bank Expense","event/new?parent_class=Invoice&parent_id=".$invoice->getId()."&type=BankExp") ?></td>
          <td><?php //echo link_to("Cancel","invoice/cancel?id=".$invoice->getId()) ?></td>
          <td><?php //if($invoice->getSaletype()!="Cash")echo link_to("Cash sale","invoice/adjustsaletype?id=".$invoice->getId()."&type=Cash") ?></td>
          <td><?php //if($invoice->getSaletype()!="Cheque")echo link_to("Cheque sale","invoice/adjustsaletype?id=".$invoice->getId()."&type=Cheque") ?></td>
          <td><?php //if($invoice->getSaletype()!="Credit")echo link_to("Account sale","invoice/adjustsaletype?id=".$invoice->getId()."&type=Account") ?></td>
<br>
          <td><?php //echo link_to("View Details","invoice/view?id=".$invoice->getId()) ?></td>
          <td><?php //echo link_to("View Events","invoice/events?id=".$invoice->getId()) ?></td>
          <td><?php //echo link_to("View Accounting","invoice/accounting?id=".$invoice->getId()) ?></td>
-->

<?php //echo link_to("Generate PO","invoice/generatePurchase?id=".$invoice->getId()) ?>
