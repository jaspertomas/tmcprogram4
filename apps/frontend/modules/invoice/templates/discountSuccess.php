\<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<font size=5><?php echo link_to("(Back)","invoice/view?id=".$invoice->getId())?></font>
<h1>
	Invoice Discounting for <?php echo $invoice?> 
</h1>
<?php slot('transaction_id', $invoice->getId()) ?>
<?php slot('transaction_type', "Invoice") ?>

<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Inv no.</td>
          <td>
            <?php echo $invoice->getInvoiceTemplate()." ".$invoice->getInvno(); ?>
          </td>
        </tr>
        <tr>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
        </tr>
        <!--tr>
          <td>PO No.</td>
          <td><?php //echo $invoice->getPonumber() ?></td>
        </tr-->
        <tr>
          <td>Customer</td>
          <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId(),array("target"=>"edit_customer"))." (".$invoice->getCustomer()->getTinNo().")"; ?></td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Sale Type</td>
          <td><?php echo $invoice->getSaleType() ?></td>
        </tr>
        <!--tr>
          <td>Due date</td>
          <td><?php //echo $invoice->getDuedate() ?></td>
        </tr-->
        <!--tr>
          <td>Discount Rate</td>
          <td><?php //echo $invoice->getDiscrate() ?></td>
        </tr>
        <tr>
          <td>Discount Amount</td>
          <td><?php //echo $invoice->getDiscamt() ?></td>
        </tr-->
        <tr>
          <td>Total</td>
          <td><b><?php echo $invoice->getTotal() ?></b></td>
        </tr>
        <tr>
          <td>Balance</td>
          <td><b><?php echo $invoice->getBalance() ?></b></td>
        </tr>
      </table>
    </td>
    <td>
    </td>
  </tr>
</table>

<hr>

<?php echo form_tag("invoice/adjustDiscountLevel");?>
<input type=hidden id=source name=source value=discount>
<input type=hidden id=invoice[id] name=invoice[id] value="<?php echo $invoice->getId()?>">
<table>
  <tr>
    <td><font size=4><b>Discount Rate: </b></font></td>
    <td><font size=4><b><?php echo $invoice->getDiscountingString() ?></font></b></td>
  </tr>

  <?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>

  <tr>
    <td>Discount Level</td>
    <td>
      <?php echo $form["discount_level_id"]?>
    </td>
  </tr>
  <tr>
    <td>Max Discount Level</td>
    <td>
      <?php echo $form["max_discount_level_id"]?>
    </td>
  </tr>
  <!--
  <tr>
    <td>Custom Discount Rate</td>
    <td>
      <?php echo $form["discrate"]?>
    </td>
    <td>(Ignore discount level if present)</td>
  </tr>
  <tr>
    <td>Discount Amount</td>
    <td>
      <?php //echo $form["discamt"]?>
    </td>
  </tr>
  -->
  <tr>
    <td></td>
    <td>
      <input type=submit value=Submit>
    </td>
  </tr>
  <?php }?>	
</table>
</form>

<?php if($invoice->getIsTemporary()!=0 and $sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
<br>Note: This will overwrite custom discount rates of invoice details
<?php }?>	
