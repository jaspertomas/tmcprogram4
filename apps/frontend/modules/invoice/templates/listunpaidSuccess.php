<?php use_helper('I18N', 'Date') ?>
<h1>Unpaid Invoices <?php echo link_to("Refresh","invoice/listunpaid")?></h1>

<?php foreach($customers as $customer){?>
<hr>
<table>
  <tr>
    <td>Customer</td>
    <td>Total Pumps</td>
    <td>Total Tanks</td>
    <td>Total Receivable</td>
    <td><b>Total Due</b></td>
    <td>Phone</td>
    <td>Notes</td>
  </tr>
<h3>
  <tr>
    <td><?php echo link_to($customer,"customer/viewUnpaid?id=".$customer->getId()) ?></td>
    <td align=right><?php echo MyDecimal::format($customerpumptotals[$customer->getId()])?></td>
    <td align=right><?php echo MyDecimal::format($customertanktotals[$customer->getId()])?></td>
    <td align=right><?php echo MyDecimal::format($customertotals[$customer->getId()])?></td>
    <td align=right><b><?php echo MyDecimal::format($customerduetotals[$customer->getId()])?></b></td>
    <td><?php echo $customer->getPhone1()?></td>
    <td>
      <?php echo form_tag("customer/adjustCollectionNotes",array("class"=>"collection_notes_form"))?>
      <input type=hidden name=id value=<?php echo $customer->getId()?>><input name=collection_notes value=<?php echo $customer->getCollectionNotes()?>>
      <input type=submit value=Save>
      </form>
    </td>
  </tr>
</table border=1>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Due Date</td>
    <td>Type</td>
    <td>Invoice</td>
    <td>Total Price</td>
    <td>Pumps</td>
    <td>Tanks</td>
    <td>Balance</td>
    <td>Notes</td>
    <td>Status</td>
  </tr>
  <?php foreach($customerinvoices[$customer->getId()] as $invoice){?>
  <tr>
      <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
      <td><?php echo MyDateTime::frommysql($invoice->getDueDate())->toshortdate() ?></td>
      <td><?php echo $invoice->getInvoiceTemplate() ?></td>
      <td><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php echo MyDecimal::format($invoice->getTotal()) ?></td>
      <td><?php if($invoice->getTotalForPumps()>0)echo MyDecimal::format($invoice->getTotalForPumps()) ?></td>
      <td><?php if($invoice->getTotalForTanks()>0)echo MyDecimal::format($invoice->getTotalForTanks()) ?></td>
      <td><b><?php echo $invoice->getBalance() ?></b></td>
      <td><?php echo $invoice->getNotes() ?></td>
      <td>
        <span id=collection_status_display_<?php echo $invoice->getId()?> class=collection_status_display>
          <font color=<?php $isduestring=$invoice->getIsDueString();echo $invoice->getColorForIsDueString($isduestring)?>>
            <?php echo $isduestring;?>
          </font>
        </span>
<input type=button invoice_id="<?php echo $invoice->getId()?>" class=collection_status_button value="Due">
<input type=button invoice_id="<?php echo $invoice->getId()?>" class=collection_status_button value="Bill Sent">
<input type=button invoice_id="<?php echo $invoice->getId()?>" class=collection_status_button value="Cheque Ready">
      </td>
  </tr>
  <?php }?>
</table>
<?php }?>

<script>
//on clicking Save on notes edit form
$(document).on("submit", ".collection_notes_form", function(e){
e.preventDefault();
    $.ajax({ // create an AJAX call...
        data: $(this).serialize(), // get the form data
        type: $(this).attr('method'), // GET or POST
        url: $(this).attr('action'), // the file to call
        success: function(response) { // on success..
  alert("Note Saved");
        }
    });
return false;
});
//on clicking Due, Bill Sent or Cheque REady
$(document).on("click", ".collection_status_button", function(e){
  var collection_status=$(this).val();
  var invoice_id=$(this).attr("invoice_id");
  $.ajax({ // create an AJAX call...
      data: "id="+invoice_id+"&collection_status="+collection_status, // get the form data
      type: "post", // GET or POST
      url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/invoice/adjustCollectionStatus/action",
      success: function(response) { // on success..
        $("#collection_status_display_"+invoice_id).html(response);
      }
  });
});
</script>

<?php echo link_to("recalc","invoice/massRecalcForProductCategory")?>