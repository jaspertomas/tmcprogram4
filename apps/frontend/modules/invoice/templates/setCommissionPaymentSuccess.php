<?php use_helper('I18N', 'Date') ?>
<h1>Invoice set Commission Payment</h1>
<br>Commission Payment Name: <?php echo link_to($commission_payment->getName(),"commission_payment/view?id=".$commission_payment->getId());?>
<br>Salesman: <b><?php echo $commission_payment->getEmployee();?></b>
<br>Start Date: <?php echo MyDateTime::frommysql($commission_payment->getStartDate())->toshortdate();?>
<br>End Date: <?php echo MyDateTime::frommysql($commission_payment->getEndDate())->toshortdate();?>
<br>
<br>Invoice: <?php echo link_to($invoice->getInvoiceTemplate()." ".$invoice->getInvno(),"invoice/view?id=".$invoice->getId());?>
<br>Date: <?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate();?>
<br>Customer: <?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId());?>
<br>Salesman: <b><?php echo $invoice->getSalesman();?></b>
