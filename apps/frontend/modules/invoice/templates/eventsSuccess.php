\<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
	Invoice Payments <?php echo link_to("(Back)","invoice/view?id=".$invoice->getId())?>
</h1>
<?php slot('transaction_id', $invoice->getId()) ?>
<?php slot('transaction_type', "Invoice") ?>

<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Inv no.</td>
          <td>
            <?php echo $invoice->getInvoiceTemplate()." ".$invoice->getInvno(); ?>
          </td>
        </tr>
        <tr>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
        </tr>
        <!--tr>
          <td>PO No.</td>
          <td><?php //echo $invoice->getPonumber() ?></td>
        </tr-->
        <tr>
          <td>Customer</td>
          <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId(),array("target"=>"edit_customer"))." (".$invoice->getCustomer()->getTinNo().")"; ?></td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Sale Type</td>
          <td><?php echo $invoice->getSaleType() ?></td>
        </tr>
        <!--tr>
          <td>Due date</td>
          <td><?php //echo $invoice->getDuedate() ?></td>
        </tr-->
        <!--tr>
          <td>Discount Rate</td>
          <td><?php //echo $invoice->getDiscrate() ?></td>
        </tr>
        <tr>
          <td>Discount Amount</td>
          <td><?php //echo $invoice->getDiscamt() ?></td>
        </tr-->
        <tr>
          <td>Total</td>
          <td><b><?php echo $invoice->getTotal() ?></b></td>
        </tr>
        <tr>
          <td>Balance</td>
          <td><b><?php echo $invoice->getBalance() ?></b></td>
        </tr>
      </table>
    </td>
    <td>
    </td>
    <td>
			<table>
      <tr>
	        <td><b>Status</b></td>
	        <td>
            <font <?php echo $invoice->getFancyStatusColor();?>>
	        <?php echo $invoice->getFancyStatusString();?>
            </font>
          </td>
		    </tr>
        <tr>
          <td>Notes</td>
          <td><?php echo $invoice->getNotes() ?></td>
        </tr>
				<!--tr>
					<td>Cheque Data</td>
        <?php //$cheques=explode(", ",$invoice->getChequedata());foreach($cheques as $cheque)  {?>
					<td><?php //echo $cheque?></td>				
			  </tr>
				<tr>
					<td></td>				
			  <?php //} ?>
				</tr-->
        <tr>
          <td><?php //echo link_to("Edit","invoice/edit?id=".$invoice->getId(),array("id"=>"invoice_edit")) ?></td>
        </tr>
		</table>
    </td>
  </tr>
</table>

<hr><h2>Refunds:</h2>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Type</td>
    <td>Amount</td>
    <td>Check Voucher</td>
    <td>Check No</td>
    <td>Check Date</td>
    <td>Notes</td>
  </tr>

  <?php foreach($invoice->getVouchers() as $voucher){$check=$voucher->getOutCheck()?>
    <tr>
      <td><?php echo MyDateTime::frommysql($voucher->getDate())->toshortdate() ?></td>
      <td><?php echo $voucher->getVoucherType() ?></td>
      <td align=right><?php echo $voucher->getAmount() ?></td>
      <td><?php if($check)echo link_to($check->getCode(),"in_check/view?id=".$check->getId())?></td>
      <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
      <td><?php if($check)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
      <td><?php echo $voucher->getNotes() ?></td>
    </tr>
  <?php }?>
</table>


<hr><h2>Payments:</h2>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Type</td>
    <td>Amount</td>
    <td>Check Voucher</td>
    <td>Check No</td>
    <td>Check Date</td>
    <td>Notes</td>
  </tr>

<?php if($invoice->getCash()>0){?>
  <tr>
      <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
      <td><?php echo "Cash" ?></td>
      <td align=right><?php echo $invoice->getCash() ?></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>
    </tr>
  <?php }?>

  <?php if($invoice->getChequeAmt()>0){$check=$invoice->getInCheck();?>
    <tr>
      <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
      <td><?php echo "Check" ?></td>
      <td align=right><?php echo $invoice->getChequeAmt() ?></td>
      <td><?php if($check)echo link_to($check->getCode(),"in_check/view?id=".$check->getId())?></td>
      <td><?php echo $invoice->getCheque() ?></td>
      <td><?php echo MyDateTime::frommysql($invoice->getCheckcleardate())->toshortdate()  ?></td>
      <td><?php if($check)echo $check->getNotes() ?></td>
      <td></td>
      <td><?php echo link_to("Delete","in_check/removeFromInvoice?invoice_id=".$invoice->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?'));?></td>
  </tr>
<?php } ?>


  <?php foreach($invoice->getEventsByDate() as $detail){?>
  <?php if($detail->getType()=="CashCollect"){?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td><?php echo $detail->getType() ?></td>
      <td align=right><?php echo $detail->getAmount() ?></td>
      <td></td>
      <td></td>
      <td></td>
      <td><?php echo $detail->getNotes() ?></td>
      <td><?php echo link_to('Edit','event/edit?id='.$detail->getId()) ?></td>
      <td>
  <?php echo link_to(
    'Delete',
    'event/delete?id='.$detail->getId(),
    array('method' => 'delete', 'confirm' => 'Are you sure?')
  ) ?>

      </td>
    </tr>
  <?php }elseif($detail->getType()=="ChequeCollect"){ $check=$detail->getInCheck();?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td><?php echo $detail->getType() ?></td>
      <td align=right><?php echo $detail->getAmount() ?></td>
      <td><?php if($check)echo link_to($check->getCode(),"in_check/view?id=".$check->getId())?></td>
      <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
      <td><?php if($check)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
      <td><?php echo $detail->getNotes() ?></td>
      <td><?php echo link_to('Edit','event/edit?id='.$detail->getId()) ?></td>
      <td>
        <?php echo link_to(
          'Delete',
          'event/delete?id='.$detail->getId(),
          array('method' => 'delete', 'confirm' => 'Are you sure?')
        ) ?>
      </td>
    </tr>
  <?php } ?>
  <?php }?>
</table>


<br>
<table>
	<tr>
      <td><?php echo link_to("Cash Collection","event/new?parent_class=Invoice&parent_id=".$invoice->getId()."&type=CashCollect") ?></td>
      <td>
        Cheque Collection P
      </td>
      <td>
        <?php echo form_tag("in_check/new?invoice_id=".$invoice->getId());?>
        <input name=chequeamt id=chequeamt size=5>
        <input name=invoice_based id=invoice_based type=hidden value=false>
        <input type=submit value=Submit>
        </form>
      </td>
      <td><?php //echo link_to("Bank Expense","event/new?parent_class=Invoice&parent_id=".$invoice->getId()."&type=BankExp") ?></td>
	</tr>
</table>

<hr>
<h2>Check History for <?php echo $invoice->getCustomer()?></h1>
<?php include_partial('customer_checks', array('select' => false,'invoice' => $invoice,'customer' => $invoice->getCustomer())) ?>

<script>
var manager_password="<?php 
	    $setting=Doctrine_Query::create()
	        ->from('Settings s')
	      	->where('name="manager_password"')
	      	->fetchOne();
      	if($setting!=null)echo $setting->getValue();
?>";
$('#invoice_edit').click(function(event) {
    event.preventDefault();
    var pass=prompt("ENTER MANAGER PASSWORD","");
    //if password is correct
    if (pass==manager_password){
       window.location = $(this).attr('href');
    }
    //if cancelled
    else if(pass==null){}
    else
    {
        	alert("WRONG PASSWORD");
    }
    
});
</script>
