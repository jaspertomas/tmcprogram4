<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new InvoiceForm(),"invoice/dsrmulti")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>

Template: <?php
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'InvoiceTemplate',
  'add_empty' => true,
));
echo $w->render('template_id',intval($template_id));
?>

<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $toform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Month","invoice/dsrmulti?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql()."&template_id=".$template_id);?> | 
<?php echo link_to("This Month","invoice/dsrmulti?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql()."&template_id=".$template_id);?> | 
<?php echo link_to("Next Month","invoice/dsrmulti?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql()."&template_id=".$template_id);?> | 
<br>
    <?php echo link_to("Yesterday","invoice/dsrmulti?startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql()."&template_id=".$template_id);?> | 
    <?php echo link_to("Tomorrow","invoice/dsrmulti?startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql()."&template_id=".$template_id);?> | 
<br>

</form>

<h1>Period Sales Report </h1>
Date: <?php echo $form->getObject()->getDate(); ?> to <?php echo $toform->getObject()->getDate(); ?>

<br>Cash Sales: <?php echo MyDecimal::format($cashtotal)?>
<br>Cheque Sales: <?php echo MyDecimal::format($chequetotal)?>
<br>Credit Sales: <?php echo MyDecimal::format($credittotal)?>
<br>Total Sales: <?php echo MyDecimal::format($total)?>
<br>
<?php 
$datearray=explode("-",$form->getObject()->getDate());
$todatearray=explode("-",$toform->getObject()->getDate());
echo link_to("Print","invoice/dsrmultipdf?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]."&template_id=".$template_id);?>


<br>
Total Pumps: <?php echo MyDecimal::format($totalPumps)?><br>
Total Tanks: <?php echo MyDecimal::format($totalTanks)?><br>
Total Sheets: <?php echo MyDecimal::format($totalSheets)?><br>
Total Supplies: <?php echo MyDecimal::format($totalSupplies)?><br>
<br>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Customer</td>
    <td>Form</td>
    <td>Reference</td>
    <td>Transaction Code</td>
    <td>Item Description</td>
    <td>Terms</td>
    <td>Cash</td>
    <td>Cheque</td>
    <td>Acct Sales</td>
    
    <td>Pumps</td>
    <td>Tanks</td>
    <!-- <td>Sheets</td>
    <td>Supplies</td> -->

    <td>Salesman</td>
    <td>Remarks</td>
  </tr>
  <?php 
foreach($templates as $template){
//foreach(array(2,4,1,3,5) as $template_id){
?>
    <?php foreach($invoices as $invoice)
        if($invoice->getIsTemporary()==0)
        if($invoice->getTemplateId()==$template->getId())
        if($invoice->getStatus()!="Cancelled")
        if($invoice->getHidden()==0)//if not hidden
        {
        $tin_no=$invoice->getCustomer()->getTinNo();
        ?>
    <tr>
      <td><?php echo $invoice->getDate() ?></td>
      <td><?php echo $invoice->getCustomer()." (".($tin_no?$tin_no:"No Tin").")" ?></td>
      <td><?php echo $invoice->getInvoiceTemplate() ?></td>
      <td><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php echo $invoice->getTransactionCode() ?></td>
      <td><?php echo $invoice->getParticularsString().($invoice->getCheque()?("; Check No.:".$invoice->getCheque().": ".$invoice->getChequedate()):"") ?></td>
      <td><?php //echo $invoice->getTotal() ?></td>
      <td align=right><?php if($invoice->getCash()>0 and $invoice->getStatus()!="Cancelled")echo MyDecimal::format($invoice->getCash()) ?></td>
      <td align=right><?php if($invoice->getChequeamt()>0 and $invoice->getStatus()!="Cancelled")echo MyDecimal::format($invoice->getChequeamt()) ?></td>
      <td align=right><?php if($invoice->getCredit()>0 and $invoice->getStatus()!="Cancelled")echo MyDecimal::format($invoice->getCredit()) ?></td>

      <td align=right><?php if($invoice->getStatus()!="Cancelled" and $productCategoryTotals[$invoice->getId()][""]!=0)echo MyDecimal::format($productCategoryTotals[$invoice->getId()][""]) ?></td>
      <td align=right><?php if($invoice->getStatus()!="Cancelled" and $productCategoryTotals[$invoice->getId()]["TJL"]!=0)echo MyDecimal::format($productCategoryTotals[$invoice->getId()]["TJL"]) ?></td>
      <!-- 
      <td align=right><?php //if($invoice->getStatus()!="Cancelled" and $productCategoryTotals[$invoice->getId()]["Sheet"]!=0)echo MyDecimal::format($productCategoryTotals[$invoice->getId()]["Sheet"]) ?></td>
      <td align=right><?php //if($invoice->getStatus()!="Cancelled" and $productCategoryTotals[$invoice->getId()]["Supplies"]!=0)echo MyDecimal::format($productCategoryTotals[$invoice->getId()]["Supplies"]) ?></td> 
      -->

      <td><?php echo $invoice->getEmployee() ?></td>
      <td><?php echo $invoice->getStatus() ?></td>
      <td><?php echo $invoice->getNotes() ?></td>
      <td><?php echo link_to("Edit","invoice/edit?id=".$invoice->getId()) ?></td>
      <td><?php echo $invoice->getDate() ?></td>
    </tr>
    <?php }?>
    <tr>
      <td></td>
    </tr>
  <?php } ?>
</table>

<br>
<br>
<h2>Cancelled Invoices</h2>
<table border=1>
  <tr>
    <td>Form</td>
    <td>Customer</td>
    <td>Reference</td>
    <td>Code</td>
    <td>Item Description</td>
    <td>Terms</td>
    <td>Cash</td>
    <td>Cheque</td>
    <td>Acct Sales</td>
    <td>Salesman</td>
    <td>Remarks</td>
  </tr>
  <?php foreach($templates as $template){?>
    <?php foreach($invoices as $invoice)
        //if($invoice->getIsTemporary()==0 or $invoice->getIsTemporary()==1)
        if($invoice->getTemplateId()==$template->getId())
        if($invoice->getStatus()=="Cancelled")
        if($invoice->getHidden()==0)//if not hidden
        {?>
    <tr>
      <td><?php echo $invoice->getInvoiceTemplate() ?></td>
      <td><?php echo $invoice->getCustomer() ?></td>
      <td><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php //echo $invoice->getTotal() ?></td>
      <td><?php echo $invoice->getParticularsString().($invoice->getCheque()?("; Check No.:".$invoice->getCheque().": ".$invoice->getChequedate()):"") ?></td>
      <td><?php //echo $invoice->getTotal() ?></td>
      <td align=right><?php if($invoice->getCash()>0)echo MyDecimal::format($invoice->getCash()) ?></td>
      <td align=right><?php if($invoice->getChequeamt()>0)echo MyDecimal::format($invoice->getChequeamt()) ?></td>
      <td align=right><?php if($invoice->getCredit()>0)echo MyDecimal::format($invoice->getCredit()) ?></td>
      <?php if(sfConfig::get('custom_show_product_category_totals_in_dsr')){?>
      <td align=right><?php if($invoice->getTotalForProductCategory()!=0)echo MyDecimal::format($invoice->getTotalForProductCategory()) ?></td>
      <td align=right><?php if($invoice->getTotalForNoProductCategory()!=0)echo MyDecimal::format($invoice->getTotalForNoProductCategory()) ?></td>
      <?php }?>
      <td><?php echo $invoice->getEmployee() ?></td>
      <td><?php echo $invoice->getStatus() ?></td>
      <td><?php echo $invoice->getNotes() ?></td>
      <td><?php echo link_to("Edit","invoice/edit?id=".$invoice->getId()) ?></td>
      <td><?php echo $invoice->getDate() ?></td>
    </tr>
    <?php }?>
    <tr>
      <td></td>
    </tr>
  <?php }?>
</table>

