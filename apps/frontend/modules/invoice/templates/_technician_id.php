

<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_technician_id">
<div>
<label for="invoice_technician_id">Technician</label>
<div class="content">
<?php
echo '<select name="invoice[technician_id]" id="invoice_technician_id">';
echo '<option value=""></option>';
foreach(EmployeeTable::getTechnicians() as $technician)
{
echo '<option value="'.$technician->getId().'" '.($technician->getId()==$form->getObject()->getTechnicianId()?'selected=selected':'').'>'.$technician->getName().'</option>';
}
echo '</select>';
?>
<?php 
?> &ensp;(<?php //echo link_to("Pick another technician","technician/search")?>)
</div>
</div>
</div>
