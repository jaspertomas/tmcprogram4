<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_notice"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<?php use_helper('I18N', 'Date'); ?>
<h1>Invoice: <?php echo $invoice->getInvno()?>: Delivery Receipts </h1>
<table border="1">
  <tr>
    <td>View</td>
    <td>Name</td>
    <td>Type</td>
    <td>Customer/Vendor</td>
    <td>Reference</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php foreach($deliverys as $delivery){?>
  <tr>
    <td><?php echo link_to("View","delivery/view?id=".$delivery->getId()) ?></td>
    <td><?php echo link_to($delivery->getName(),"delivery/view?id=".$delivery->getId()) ?></td>
    <td><?php echo $delivery->getType() ?></td>
    <td><?php $client=$delivery->getClient();if($client!=null)echo link_to($client,$delivery->getClientClass()."/view?id=".$delivery->getClientId()) ?></td>
    <td><?php $ref=$delivery->getRef();if($ref!=null)echo link_to($ref,$delivery->getRefClass()."/view?id=".$delivery->getRefId()) ?></td>
    <td>
      <font color=<?php echo $delivery->getStatusColor()?>><?php echo $delivery->getStatus()?></font>
      <?php foreach(array("Pending","Complete","Cancelled") as $value)if($delivery->getStatus()!=$value)echo link_to("$value","delivery/setStatus?id=".$delivery->getId()."&submit=$value", array("class"=>"button","confirm" => "$value\n\nAre you sure?")) ?> 
    </td>
    <td>
      <?php echo link_to('Delete','delivery/delete?id='.$delivery->getId(), array('method' => 'delete', 'confirm' => 'Really delete this delivery receipt?')) ?> |
    </td>

  </tr>
  <?php } ?>
</table>
