<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new InvoiceForm(),"invoice/dirmulti")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>

<table>
  <tr>
    <td>From Date: <?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>To Date: <?php echo $toform["date"] ?></td>
  </tr>
  <tr>
    <td>Warehouse: <?php
      $w=new sfWidgetFormDoctrineChoice(array(
        'model'     => 'Warehouse',
        'add_empty' => false,
      ));
      echo $w->render('warehouse_id',$warehouse_id);
      ?>
    </td>
  </tr>
  <tr>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Month","invoice/dirmulti?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","invoice/dirmulti?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","invoice/dirmulti?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br>
    <?php echo link_to("Yesterday","invoice/dirmulti?invoice[date][day]=".$yesterday->getDay()."&invoice[date][month]=".$yesterday->getMonth()."&invoice[date][year]=".$yesterday->getYear()); ?>
    <?php echo link_to("Tomorrow","invoice/dirmulti?invoice[date][day]=".$tomorrow->getDay()."&invoice[date][month]=".$tomorrow->getMonth()."&invoice[date][year]=".$tomorrow->getYear()); ?>
<br>

</form>

<h1>Multi Date Inventory Report </h1>
Date: <?php echo MyDateTime::frommysql($form->getObject()->getDate())->toshortdate(); ?> to <?php echo MyDateTime::frommysql($toform->getObject()->getDate())->toshortdate(); ?>

<br>
<?php 
$datearray=explode("-",$form->getObject()->getDate());
$todatearray=explode("-",$toform->getObject()->getDate());
echo link_to("Print","invoice/dirmultipdf?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);?>

<?php $producttypename="" ?>

<br>
<br>
<table border=1>
  <tr>
    <td>Product</td>
    <td>Start</td>
    <td>In</td>
    <td>Out</td>
    <td>End</td>
  </tr>
<?php foreach($array as $item){ if($item["in"]==0 and $item["out"]==0 )continue;?>
  <?php if($item["product_type_name"]!=$producttypename){
    $producttypename=$item["product_type_name"];
  ?>
  <tr>
    <td><b><?php echo $item["product_type_name"]?></b></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php } ?>
  <tr>
    <td><?php echo $item["product_name"]?></td>
    <td align=right><?php echo $item["endbalance"]+$item["out"]-$item["in"]?></td>
    <td align=right><?php echo $item["in"]?></td>
    <td align=right><?php echo $item["out"]?></td>
    <td align=right><?php echo $item["endbalance"]-0?></td>
  </tr>
  <?php } ?>
</table>


