<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("L", PDF_UNIT, "GOVERNMENTLEGAL", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Tradewind Mdsg Corp Daily Sales Report');

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->startPageGroup();
$pdf->AddPage();

$pdf->write(0,MyDateTime::frommysql($form->getObject()->getDate())->toshortdatewithweek(),
'',false,'',true,0,false,false,0,0);

$contents=array();
$contents[]=array(
          "Cash Sales: ",
          MyDecimal::format($cashsales),
          " ",
          "Cheque Sales: ",
          MyDecimal::format($chequesales),
          " ",
          "Credit Sales: ",
          MyDecimal::format($creditsales),
          " ",
          "Total Sales: ",
          MyDecimal::format($cashsales+$chequesales+$creditsales),
          );
$contents[]=array(
          "Other Cash: ",
          MyDecimal::format($cashother),
          " ",
          "Other Cheque: ",
          MyDecimal::format($chequeother),
          " ",
          "Other Credit: ",
          MyDecimal::format($creditother),
          " ",
          "Total Other: ",
          MyDecimal::format($cashother+$chequeother+$creditother),
          );
$contents[]=array(
          "Total Cash: ",
          MyDecimal::format($cashtotal),
          " ",
          "Total Cheque: ",
          MyDecimal::format($chequetotal),
          " ",
          "Total Credit: ",
          MyDecimal::format($credittotal),
          " ",
          "Total: ",
          MyDecimal::format($total),
          );
$contents[]=array(
          "Less Petty Cash: ",
          MyDecimal::format($pettycashtotal),
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          );
$contents[]=array(
          "Remaining Cash: ",
          MyDecimal::format($remainingcash),
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          );
          /*
$contents[]=array(
          "Less Deductions: ",
          MyDecimal::format($deducttotal*-1),
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          );
$contents[]=array(
          "Total Cash: ",
          MyDecimal::format($cashtotal-$deducttotal),
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          " ",
          );
*/
$widths=array(30,30,20,30,30,20,30,30,20,30,30,);
$height=1;
foreach($contents as $content)
{
  $pdf->MultiCell($widths[0], $height, $content[0], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[8], $height, $content[8], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[9], $height, $content[9], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[10], $height, $content[10], 0, 'R', 0, 1, '', '', true);
}

//$pdf->write(5,"\n\n");



/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/

// Set some content to print
$txt = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
// Multicell test
$widths=array(40,18,20,100,20,20,20,40,20);
$scale=3.543;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$content=array(
    'Particulars'
    ,'Tin No'
    ,'Ref'
    ,'Item Description'
    ,'Cash'
    ,'Cheque'
    ,'Acct Sales'
    ,'Salesman'
    ,'Remarks'
          );
$tbl = <<<EOD
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>$content[0]</b></td>
  <td width="$widths[1]" align="center"><b>$content[1]</b></td>
  <td width="$widths[2]" align="center"><b>$content[2]</b></td>
  <td width="$widths[3]" align="center"><b>$content[3]</b></td>
  <td width="$widths[4]" align="center"><b>$content[4]</b></td>
  <td width="$widths[5]" align="center"><b>$content[5]</b></td>
  <td width="$widths[6]" align="center"><b>$content[6]</b></td>
  <td width="$widths[7]" align="center"><b>$content[7]</b></td>
  <td width="$widths[8]" align="center"><b>$content[8]</b></td>
 </tr>
</thead>
EOD;

//===============================
$addresses=array();
foreach(array(2,1,3,5,10) as $template_id)
{
  $pdf->SetFont('dejavusans', '', 14, '', true);

  $width=298*$scale;
  $content=InvoiceTemplateTable::fetch($template_id);
  $tbl .= <<<EOD
 <tr>
  <td width="$width" align="left"><b><font size=10>$content</font></b></td>
 </tr>
EOD;
  //$pdf->MultiCell(298, 0, InvoiceTemplateTable::fetch($template_id), 1, 'L', 0, 1, '', '', true);

  $pdf->SetFont('dejavusans', '', 8, '', true);
  foreach($events as $event)
  {

    $invoice=$event->getParent();
    // if($invoice and $invoice->getStatus()!="Cancelled")
    if($invoice)//show payment even if cancelled
    if($invoice->getTemplateId()==$template_id)
    {
    
      //catch addresses if present
      if($invoice->getCustomer()->getAddress()!="")
      {
        $addresses[]=array(
          $invoice->getCustomer(),
          $invoice->getCustomer()->getTinNo(),
          $invoice->getInvno(),
          $invoice->getCustomer()->getAddress(),
        );
      }
    
      $content=array(
        $invoice->getCustomer(),
        $invoice->getCustomer()->getTinNo(),
        $invoice->getInvno()." (".MyDateTime::frommysql($invoice->getDate())->toshortdate().")",
        $event->getType().": ".$event->getDetail1().": ".$event->getDetail2(),
        ($event->getDetail("cashamt")!=0)?$event->getDetail("cashamt"):" ",
        ($event->getDetail("chequeamt")!=0)?$event->getDetail("chequeamt"):" ",
        ($event->getDetail("creditamt")!=0 and $invoice->getStatus()!="Cancelled")?$event->getDetail("creditamt"):" ",
        $invoice->getEmployee()?$invoice->getEmployee():" ",
        $invoice->getStatus()?$invoice->getStatus():($event->getDetail("status")?$event->getDetail("status"):" "),
        );
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[1]" align="center">$content[1]</td>
<td width="$widths[2]" align="center">$content[2]</td>
<td width="$widths[3]" align="center">$content[3]</td>
<td width="$widths[4]" align="right">$content[4]</td>
<td width="$widths[5]" align="right">$content[5]</td>
<td width="$widths[6]" align="right">$content[6]</td>
<td width="$widths[7]" align="center">$content[7]</td>
<td width="$widths[8]" align="center">$content[8]</td>
</tr>
EOD;
    }
  }
  foreach($invoices as $invoice)
  if($invoice->getTemplateId()==$template_id)
  //show only closed uncancelled invoices
  // if($invoice->getIsTemporary()==0)
  // if($invoice->getStatus()!="Cancelled")

  //show closed, uncancelled invoices
  //show open or cancelled invoices if they have payment
  if(($invoice->getIsTemporary()==0 and $invoice->getStatus()!="Cancelled") or $invoice->getCash()!=0 or $invoice->getChequeamt()!=0)
  {
			$particularsstring=$invoice->getParticularsString()?$invoice->getParticularsString():" ";
			if($invoice->getCheque())$particularsstring=implode("; ",array($particularsstring,"Cheque no.: ".$invoice->getCheque().", ".MyDateTime::frommysql($invoice->getChequeDate())->toshortdate()));

			$chequestring=$invoice->getChequeamt()>0?$invoice->getChequeamt():" ";
			//if($invoice->getCheque())$chequestring=implode("; ",array($chequestring,"Cheque no.: ".$invoice->getCheque().", ".MyDateTime::frommysql($invoice->getChequeDate())->toshortdate()));
  
/*
DISPLAY "CHECK TO CLEAR" IF CHECKCLEARDATE NOT REACHED
  if status=paid,
    if checkcleardate > today, 
      status = pending. 
    else 
      status = paid
*/
      if($invoice->getStatus()=="Paid")
      {
        $today=MyDateTime::today();
        $checkcleardate=MyDateTime::frommysql($invoice->getCheckcleardate());
        $status="Paid";
        if($checkcleardate->islaterthan($today))$status="Check to clear on ".$checkcleardate->toshortdate();
      }
      else 
      {
        $status=$invoice->getStatus();
      }

      //catch addresses if present
      if($invoice->getCustomer()->getAddress()!="")
      {
        $addresses[]=array(
          $invoice->getCustomer(),
          $invoice->getCustomer()->getTinNo(),
          $invoice->getInvno(),
          $invoice->getCustomer()->getAddress(),
        );
      }

      $content=array(
       $invoice->getCustomer(),
        $invoice->getCustomer()->getTinNo(),
      $invoice->getInvno()?$invoice->getInvno():" ",
       $particularsstring,
      ($invoice->getCash()!=0)?$invoice->getCash():" ",
      $invoice->getStatus()!="Cancelled"?$chequestring:" ",
      ($invoice->getCredit()!=0)?$invoice->getCredit():" ",
       $invoice->getEmployee()?$invoice->getEmployee():" ",
       //$invoice->getStatus()=="Paid Check"?$invoice->getCheque():($invoice->getStatus()?$invoice->getStatus():" "),
       $status,
      );
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[1]" align="center">$content[1]</td>
<td width="$widths[2]" align="center">$content[2]</td>
<td width="$widths[3]" align="center">$content[3]</td>
<td width="$widths[4]" align="right">$content[4]</td>
<td width="$widths[5]" align="right">$content[5]</td>
<td width="$widths[6]" align="right">$content[6]</td>
<td width="$widths[7]" align="center">$content[7]</td>
<td width="$widths[8]" align="center">$content[8]</td>
</tr>
EOD;
  }
}
$tbl .= <<<EOD
</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');
//end table

//------Cancelled Invoices--------------------------
//table header
$content=array(
    'Particulars'
    ,'Tin No'
    ,'Ref'
    ,'Item Description'
    ,'Cash'
    ,'Cheque'
    ,'Acct Sales'
    ,'Salesman'
    ,'Remarks'
          );
$tbl = <<<EOD
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>$content[0]</b></td>
  <td width="$widths[1]" align="center"><b>$content[1]</b></td>
  <td width="$widths[2]" align="center"><b>$content[2]</b></td>
  <td width="$widths[3]" align="center"><b>$content[3]</b></td>
  <td width="$widths[4]" align="center"><b>$content[4]</b></td>
  <td width="$widths[5]" align="center"><b>$content[5]</b></td>
  <td width="$widths[6]" align="center"><b>$content[6]</b></td>
  <td width="$widths[7]" align="center"><b>$content[7]</b></td>
  <td width="$widths[8]" align="center"><b>$content[8]</b></td>
 </tr>
</thead>
EOD;
//table body
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->write(5,"Cancelled Invoices\n");
foreach(array(2,1,3,5,10) as $template_id)
{
  $pdf->SetFont('dejavusans', '', 14, '', true);
  
  $width=298*$scale;
  $content=InvoiceTemplateTable::fetch($template_id);
  $tbl .= <<<EOD
 <tr>
  <td width="$width" align="left"><b><font size=10>$content</font></b></td>
 </tr>
EOD;
  //$pdf->MultiCell(, 0, InvoiceTemplateTable::fetch($template_id), 1, 'L', 0, 1, '', '', true);

  $pdf->SetFont('dejavusans', '', 8, '', true);

  foreach($invoices as $invoice)
  if($invoice->getStatus()=="Cancelled")
  if($invoice->getTemplateId()==$template_id)
  {
			$particularsstring=$invoice->getParticularsString()?$invoice->getParticularsString():" ";
			if($invoice->getCheque())$particularsstring=implode("; ",array($particularsstring,"Cheque no.: ".$invoice->getCheque().", ".MyDateTime::frommysql($invoice->getChequeDate())->toshortdate()));

			$chequestring=$invoice->getChequeamt()>0?$invoice->getChequeamt():" ";
  

      //catch addresses if present
      if($invoice->getCustomer()->getAddress()!="")
      {
        $addresses[]=array(
          $invoice->getCustomer(),
          $invoice->getCustomer()->getTinNo(),
          $invoice->getInvno(),
          $invoice->getCustomer()->getAddress(),
        );
      }

      $content=array(
       $invoice->getCustomer(),
        $invoice->getCustomer()->getTinNo(),
      $invoice->getInvno()?$invoice->getInvno():" ",
       $particularsstring,
      ($invoice->getCash()!=0)?$invoice->getCash():" ",
      $invoice->getStatus()!="Cancelled"?$chequestring:" ",
      ($invoice->getCredit()!=0)?$invoice->getCredit():" ",
       $invoice->getEmployee()?$invoice->getEmployee():" ",
       $invoice->getStatus(),
      );
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[1]" align="center">$content[1]</td>
<td width="$widths[2]" align="center">$content[2]</td>
<td width="$widths[3]" align="center">$content[3]</td>
<td width="$widths[4]" align="right">$content[4]</td>
<td width="$widths[5]" align="right">$content[5]</td>
<td width="$widths[6]" align="right">$content[6]</td>
<td width="$widths[7]" align="center">$content[7]</td>
<td width="$widths[8]" align="center">$content[8]</td>
</tr>
EOD;
  }
}
$tbl .= <<<EOD
</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');
//end table

//-----------------------------------
//addresses and tin numbers table
//header
$pdf->SetFont('dejavusans', '', 12, '', true);
$pdf->write(5,"Addresses and Tin Numbers\n");
$pdf->SetFont('dejavusans', '', 8, '', true);
$widths=array(70,30,30,168);
$scale=3.543;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl = <<<EOD
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Customer</b></td>
  <td width="$widths[1]" align="center"><b>Tin No.</b></td>
  <td width="$widths[2]" align="center"><b>Invoice No.</b></td>
  <td width="$widths[3]" align="center"><b>Address</b></td>
 </tr>
</thead>
EOD;
//body
foreach($addresses as $address)
{
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$address[0]</td>
<td width="$widths[1]" align="center">$address[1]</td>
<td width="$widths[2]" align="center">$address[2]</td>
<td width="$widths[3]" align="center">$address[3]</td>
</tr>
EOD;
}
$tbl .= <<<EOD
</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');
//end addresses and tin numbers table

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('DSR-'.MyDateTime::frommysql($form->getObject()->getDate())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

