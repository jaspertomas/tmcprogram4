<?php use_helper('I18N', 'Date') ?>
<?php 
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>
<?php 
//show date form
echo form_tag("invoice/incstate");
$startDateForm = new sfWidgetFormDate();
$endDateForm = new sfWidgetFormDate();
echo "From ".$startDateForm->render('startdatesplit',$startdate);
echo " to ".$endDateForm->render('enddatesplit',$enddate);
?>
<input type=submit value="View">
</form>
<?php echo link_to("Last Month","invoice/incstate?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","invoice/incstate?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","invoice/incstate?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br><?php echo link_to("Yesterday","invoice/incstate?startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> | 
<?php echo link_to("Tomorrow","invoice/incstate?startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> | 

<h1>Income Statement</h1>
Date: <?php echo MyDateTime::frommysql($startdate)->toprettydate();?> to <?php echo MyDateTime::frommysql($enddate)->toprettydate();?> 

<table>
  <tr>
    <td>Cash Sales: </td>
    <td><?php echo MyDecimal::format($cashtotal)?></td>
  </tr>
  <tr>
    <td>Cheque Sales: </td>
    <td><?php echo MyDecimal::format($chequetotal)?></td>
  </tr>
  <tr>
    <td>Credit Sales: </td>
    <td><?php echo MyDecimal::format($credittotal)?></td>
  </tr>
  <tr>
    <td>Total Sales: </td>
    <td><?php echo MyDecimal::format($salestotal)?></td>
  </tr>
  <tr>
    <td>Total Gross Profit: </td>
    <td><?php echo MyDecimal::format($profittotal)//this is sales minus purchases?></td>
  </tr>
  <tr>
    <td>Total Expenses: </td>
    <td><?php echo MyDecimal::format($expensestotal)?></td>
  </tr>
  <tr>
    <td>Total Net Profit: </td>
    <td><?php echo MyDecimal::format($profittotal-$expensestotal)//this is gross income minus expenses?></td>
  </tr>
  <tr>
    <td>Percentage Net Profit: </td>
    <td><?php echo round(($profittotal-$expensestotal)/$salestotal*100,2)//net income / total sales?>%</td>
  </tr>
  <tr>
    <td>Total Sale Uncomputable: </td>
    <td><?php echo MyDecimal::format($unaccountedsaletotal)?></td>
  </tr>
</table>

<br>
<?php 
$datearray=explode("-",$startdate);
$todatearray=explode("-",$enddate);
/*
echo link_to("Print","invoice/incstatepdf?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);
*/                
?>

<h2>Expenses</h2>
<?php echo link_to("View","voucher/dashboardMulti?startdate=".$startdate."&enddate=".$enddate);?>
<table>
  <tr>
    <td>Account</td>
    <td>Amount</td>
  </tr>
<?php foreach($accounts as $account){?>
  <tr>
    <td><?php echo $account->getName()?></td>
    <td align=right><?php echo MyDecimal::format($totals_by_account[$account->getId()]) ?></td>
  </tr>
<?php } ?>
</table>

