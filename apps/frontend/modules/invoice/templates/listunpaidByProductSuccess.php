<?php use_helper('I18N', 'Date') ?>
<h1>Unpaid Products <?php echo link_to("Refresh","invoice/listunpaid")?></h1>
Total Amount Unpaid: <?php echo MyDecimal::format($total) ?>
<hr>
<table border=1>
  <tr>
    <td>ID</td>
    <td>Product</td>
    <td>Unpaid Total</td>
  </tr>
  <?php foreach($products as $product){?>
  <tr>
      <td><?php echo $product["id"] ?></td>
      <td><?php echo link_to($product["name"],"product/transactionsUnpaid?id=".$product["id"]) ?></td>
      <td align=right><?php echo MyDecimal::format($product["unpaidtotal"]) ?></td>
  </tr>
  <?php }?>
</table>
