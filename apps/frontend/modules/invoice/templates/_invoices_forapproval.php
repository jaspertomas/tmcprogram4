<h2>Invoices for Approval</h2>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Type</td>
    <td>Ref</td>
    <td>Customer</td>
    <td>Salesman</td>
    <td>Particulars</td>
    <td>Total Price</td>
    <td>Notes</td>
  </tr>
  <?php foreach($invoices as $invoice)if($invoice->getIsTemporary()==3 and $invoice->getStatus()!="Cancelled"){?>
  <tr>
      <td><?php echo $invoice->getDate() ?></td>
      <td><?php echo $invoice->getInvoiceTemplate() ?></td>
      <td><?php echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId()) ?></td>
      <td><?php echo $invoice->getEmployee() ?></td>
      <td><?php echo $invoice->getParticularsString().($invoice->getCheque()?("; Check No.:".$invoice->getCheque().": ".$invoice->getChequedate()):"") ?></td>
      <td><?php echo $invoice->getTotal() ?></td>
      <td><?php echo $invoice->getNotes() ?></td>
  </tr>
  <?php }?>
</table>
<hr>
