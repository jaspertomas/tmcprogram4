<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new InvoiceForm(),"invoice/commissionGrossSale")?>

Salesman: <?php
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'Employee',
  'add_empty' => true,
));
echo $w->render('employee_id',(isset($employee_id)?$employee_id:null));
?>

<table>
  <!--
  <tr>
    <td>From Date</td>
    <td><?php //echo $form["date"] ?></td>
  </tr>
  -->
  <tr>
    <td>Cutoff Date</td>
    <td><?php echo $toform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<br>
    <?php //echo link_to("PDF","invoice/commissionGrossSalePdf?invoice[date][day]=".$thismonth->getDay()    ."&invoice[date][month]=".$thismonth->getMonth()."&invoice[date][year]=".$thismonth->getYear()); ?>
<br>

</form>


</form>

<h1>Commission Report on Gross Sale: As of <?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate(); ?></h1>

<?php

foreach($empinvoices as $empid=>$employeedata)
{
  echo "<hr>";
  echo "<h3>".$employees[$empid]."</h3>";
  echo "<table>";

  //paid and cancelled records at the top
  $count=0;
  $statusesbydate=array();

  echo "<tr >";
  echo '<td><input id="sf_admin_list_batch_checkbox" onclick="checkAll();" type="checkbox"></td>';
  echo "<td ></td>";
  echo "<td >Invoice</td>";
  echo "<td ></td>";
  echo "<td >Invoice</td>";
  echo "<td align=right>Invoice</td>";
  echo "<td align=right>Commissionable</td>";
  echo "<td align=right>Commission</td>";
  echo "<td >Invoice</td>";
  echo "<td >Customer</td>";
  echo "<td >Commission</td>";
  echo "</tr>";

  echo "<tr >";
  echo "<td ></td>";
  echo "<td ></td>";
  echo "<td >Date</td>";
  echo "<td ></td>";
  echo "<td align=right></td>";
  echo "<td align=right>Total</td>";
  echo "<td align=right></td>";
  echo "<td align=right></td>";
  echo "<td >Status</td>";
  echo "<td ></td>";
  echo "<td >Paid</td>";
  echo "</tr>";

  echo form_tag("invoice/commissionGrossSaleSave");
  $employee=$employees[$empid];
  $commission_payment_name=$employee->getName()." released ".MyDateTime::today()->toshortdate();
  echo "Save checked items as <br>Commission Payment Name: <input size=50 name='commission_payment_name' id='commission_payment_name' value='".$commission_payment_name."'>";
  echo "<br>Notes: <textarea cols=25 name='notes' id='notes' ></textarea>";
  //echo "<input type=hidden name='start_date' id='start_date' value='".$invoice->getDate()."'>";
  echo "<input type=hidden name='end_date' id='end_date' value='".$purchase->getDate()."'>";
  echo "<input type=hidden name='employee_id' id='employee_id' value=".$employee_id.">";
  echo "<br><input type=submit><br><br>";

  foreach($employeedata as $invoiceindex=>$invoice)
	// if($invoice->getIsTemporary()==0)
  {
    if($invoice->getStatus()=="Paid")
    {
      /*
      DISPLAY "CHECK TO CLEAR" IF CHECKCLEARDATE NOT REACHED
        if status=paid,
          if checkcleardate > today, 
            status = pending. 
          else 
            status = paid
      */
      $today=MyDateTime::today();
      $checkcleardate=MyDateTime::frommysql($invoice->getCheckcleardate());
      $status="Paid";
      if($checkcleardate->islaterthan($today))$status="Check to clear on ".$checkcleardate->toshortdate();

      $statusesbydate[$invoiceindex]=$status;
      $statuscolor="";
    }
    else
    {
      $status=isset($statusesbydate[$invoiceindex])?$statusesbydate[$invoiceindex]:$invoice->getStatus();
      if($status=="Pending")$statuscolor="lightgreen";
      elseif($status=="Cancelled")$statuscolor="pink";
      else $statuscolor="";
    }

    if($invoice->getCommissionPaymentId())
      $statuscolor="lightgray";
    if($invoice->getIsTemporary()!=0)
      $statuscolor="lightblue";

    $count++;

    echo "<tr bgcolor=".$statuscolor.">";
    //if commission already paid, do not show check box
    if($invoice->getCommissionPaymentId())
      echo "<td></td>";
    //else show checkbox
    //if invoice is closed and customer paid, check checkbox
    else
      echo '<td><input type=checkbox name="ids[]" '.(($invoice->getIsTemporary()==0 and $invoice->isFullyPaid())?"checked=checked":"").' value='.$invoice->getId().' /></td>';
    echo "<td >".$count."</td>";
    echo "<td >".MyDateTime::frommysql($invoice->getDate())->toshortdate()."</td>";
    echo "<td>".link_to($invoice->getInvoiceTemplate(),"invoice/view?id=".$invoice->getId())."</td>";
    echo "<td align=right>".link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())."</td>";
    echo "<td align=right>".MyDecimal::format($invoice->getTotal())."</td>";
    echo "<td align=right>".($invoice->getStatus()!="Cancelled"?MyDecimal::format($invoice->getCommission($employees[$empid])):0)."</td>";
    echo "<td align=right>".($invoice->getStatus()!="Cancelled"?MyDecimal::format($invoice->getCommissionTotal($employees[$empid])):0)."</td>";
    //if not closed, show isTemporaryString, else show status
    echo "<td >".($invoice->getIsTemporary()==0?$status:$invoice->getIsTemporaryString())."</td>";
    echo "<td>".$invoice->getCustomer()."</td>";
    if($invoice->getCommissionPaymentId())
    {
      $cp=$invoice->getCommissionPayment();
      echo "<td >".link_to($cp->getName(),"commission_payment/view?id=".$invoice->getCommissionPaymentId())."</td>";
    }
    else echo "<td></td>";
    echo "</tr>";
  }
  echo "</table>";
}

echo "</form>";
?>


<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked;
  } 
  return true;
}
/* ]]> */
</script>