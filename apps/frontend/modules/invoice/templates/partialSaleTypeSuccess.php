<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
	Partial Payment <?php echo link_to("(Back)","invoice/view?id=".$invoice->getId())?>
</h1>
<?php slot('transaction_id', $invoice->getId()) ?>
<?php slot('transaction_type', "Invoice") ?>

<table>
  <tr>
    <td>Inv no.</td>
    <td>
      <?php echo $invoice->getInvoiceTemplate()." ".$invoice->getInvno(); ?>
    </td>
  </tr>
  <tr>
    <td>Date</td>
    <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
  </tr>
  <!--tr>
    <td>PO No.</td>
    <td><?php //echo $invoice->getPonumber() ?></td>
  </tr-->
  <tr>
    <td>Customer</td>
    <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId(),array("target"=>"edit_customer"))." (".$invoice->getCustomer()->getTinNo().")"; ?></td>
  </tr>
</table>

<hr/>
<?php echo form_tag("invoice/processPartialSaleType");?>
<input type=hidden id=invoice_id name=invoice[id] value=<?php echo $invoice->getId()?>>
<table>
  <tr>
    <td>Cash</td>
    <td><?php echo $form["cash"] ?></td>
  </tr>
  <tr>
    <td>Check</td>
    <td><?php echo $form["chequeamt"] ?></td>
  </tr>
  <tr>
    <td></td>
    <td><input type="submit" value="Save"></td>
  </tr>
</table>
</form>

