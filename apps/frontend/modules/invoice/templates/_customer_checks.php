<table border=1>
<tr valign=bottom>
  <th>Receive<br>Date</th>
  <th>Amount</th>
  <th>Remaining</th>
  <th>Check<br>Voucher</th>
  <th>Check No.</th>
  <th>Check<br>Date</th>
  <th>Pay To</th>
  <th>Pay For</th>
  <th>Notes</th>
  <th>Deposit</th>
</tr>

<?php foreach($customer->getChecks() as $check){ 
  $invoices=$check->getInvoice();
  $events=$check->getEvent();
  $particulars=array();
  foreach($invoices as $i)
  {
    if(!isset($particulars[$i->getInvno()]))
    $particulars[$i->getInvno()]=link_to($i,"invoice/events?id=".$i->getId());
  }
  foreach($events as $e)
  {
    if(!isset($particulars[$e->getParentName()]))
    $particulars[$e->getParentName()]=link_to($e->getParent(),"invoice/events?id=".$e->getParentId());
  }
?>
<tr>
    <td><?php echo MyDateTime::frommysql($check->getReceiveDate())->toShortDate()?></td>
    <td align=right><?php echo number_format ( $check->getAmount(),2 )?></td>
    <td align=right><?php echo number_format ( $check->getRemaining(),2 )?></td>
    <td><?php echo link_to($check->getCode(),"in_check/view?id=".$check->getId())?></td>
    <td><?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
    <td><?php echo MyDateTime::frommysql($check->getCheckDate())->toShortDate()?></td>
    <td><?php echo $check->getPassbook()?></td>
    <td><?php echo implode(", ",$particulars)?></td>
    <td><?php echo $check->getNotes()?></td>
    <td>
      <?php 
        if($sf_user->hasCredential(array('admin'),false))
        {
          if($check->isDeposited())
            echo link_to('Undo Deposit','in_check/undoDeposit?id='.$check->getId(),array('confirm' => 'UNDO DEPOSIT?'));
          else if($check->isBounced())
            echo link_to('Undo Bounce','in_check/undoBounce?id='.$check->getId(),array('confirm' => 'UNDO BOUNCE?'));
          else
          {
            echo link_to('Deposit','in_check/deposit?id='.$check->getId(),array('confirm' => 'DEPOSIT?'));
            echo " ";
            echo link_to('(Bounce)','in_check/bounce?id='.$check->getId(),array('confirm' => 'BOUNCE?'));
          }
        }
        else if(!$check->isDeposited())
          echo link_to('Deposit','in_check/deposit?id='.$check->getId(),array('confirm' => 'DEPOSIT?'));
      ?>
    </td>
    <td><?php echo link_to('Edit','in_check/edit?invoice_id='.$invoice->getId().'&id='.$check->getId()) ?></td>
    <td>
      <?php echo link_to(
        'Delete',
        'in_check/delete?id='.$check->getId(),
        array('method' => 'delete', 'confirm' => 'Are you sure?')
      ) ?>
    </td>
  <?php } ?>
</tr>
</table>
