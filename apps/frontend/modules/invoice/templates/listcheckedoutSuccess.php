<?php use_helper('I18N', 'Date') ?>

<?php include_partial('invoice/listdatepicker',array("form"=>$form)) ?>

<?php include_partial('invoice/invoices_checkedout',array("invoices"=>$invoices)) ?>

<?php include_partial('invoice/invoices_new',array("invoices"=>$invoices)) ?>

<?php include_partial('invoice/invoices_closed',array("invoices"=>$invoices)) ?>

<?php include_partial('invoice/invoices_forapproval',array("invoices"=>$invoices)) ?>

<?php include_partial('invoice/invoices_cancelled',array("invoices"=>$invoices)) ?>

<?php include_partial('purchase/listIncomplete',array("purchases"=>$purchases)) ?>

<?php include_partial('purchase/listunreceived',array("unreceivedpurchases"=>$unreceivedpurchases)) ?>

