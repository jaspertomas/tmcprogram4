<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name').' Billing Statement for '.$invoice->getName());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(20, 10, 20, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================

//hide headers by config
$template=$invoice->getInvoiceTemplate();
$showheader=true;
if($template->getId()==10 and sfConfig::get('custom_no_headers_on_transaction'))$showheader=false;
if($template->getId()==2 and sfConfig::get('custom_no_headers_on_interoffice'))$showheader=false;
if($showheader)
{
// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
// $pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 100, '', 'PNG', '', '', false, 300, 'C', false, false, 0, false, false, false);
// $pdf->write(5,"\n\n\n\n");
$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->write(0,sfConfig::get('custom_company_header_text'),'',false,'C',true,0,false,false,0,0);
//$pdf->write(0,sfConfig::get('custom_company_address'),'',false,'C',true,0,false,false,0,0);
$pdf->write(0,sfConfig::get('custom_company_email'),'',false,'C',true,0,false,false,0,0);
$pdf->write(5,"\n");
}
else
{
  $pdf->write(0,sfConfig::get('custom_header_for_no_header')."\n");
}

$tbl = "";

//-------------------
$client=$invoice->getCustomer();

$templatename=$invoice->getInvoiceTemplate();

$content=array(
  $invoice->getName(),
  MyDateTime::frommysql($invoice->getDate())->toshortdatewithweek(),
  $client->getName(),
  $invoice->getPonumber(),
  $client->getPhone1(),
  $client->getEmail(),
  sfConfig::get('custom_invoice_prefix')." ".$templatename,  
  $invoice->getTerms(),
  MyDateTime::frommysql($invoice->getDuedate())->toshortdate(),
  $client->getAddress()." / ".$client->getAddress2(),
  );
  
  

$tbl .= <<<EOD
<table>
 <tr>
  <td align="left">$content[6] No.: $content[0]</td>
  <td align="right">Date: $content[1]</td>
 </tr>
 <tr>
  <td align="left">Customer: $content[2]</td>
  <td align="right">Customer PO No.: $content[3]</td>
 </tr>
 <tr>
  <td align="left">Contact No.: $content[4]</td>
  <td align="right">Terms: $content[7] days</td>
 </tr>
 <tr>
  <td align="left">Email: $content[5]</td>
  <td align="right">Due Date: $content[8]</td>
 </tr>
 <tr>
  <td align="left" colspan="2">Address: $content[9]</td>
 </tr>
</table>
EOD;




//$pdf->write(5,"\n");
//$pdf->write(0,"\t".$message,'',false,'L',true,0,false,false,0,0);

//====TABLE HEADER===========================

$widths=array(10,50,20,20,20,20);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl .= <<<EOD
<hr>
<h2 align="center">$content[6] $content[0]</h2>
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Qty</b></td>
  <td width="$widths[1]" align="center"><b>Product</b></td>
  <td width="$widths[2]" align="center"><b>List Price</b></td>
  <td width="$widths[3]" align="center"><b>Discout<br>Rate</b></td>
  <td width="$widths[4]" align="center"><b>Discounted<br>Price</b></td>
  <td width="$widths[5]" align="center"><b>Total</b></td>
 </tr>
</thead>
EOD;

//===TABLE BODY============================
$height=1;
  $grandtotal=0;
	foreach($invoice->getInvoicedetails() as $detail)
  {
    $content=array(
      $detail->getQty(),
      $detail->getProduct(),
      MyDecimal::format($detail->getPrice()),
      $detail->getDiscrate(),
      MyDecimal::format($detail->getDiscprice()),
      MyDecimal::format($detail->getTotal()),
      );
  
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center">$content[0]</td>
    <td width="$widths[1]" >$content[1]</td>
    <td width="$widths[2]" align="right">$content[2]</td>
    <td width="$widths[3]" align="right">$content[3]</td>
    <td width="$widths[4]" align="right">$content[4]</td>
    <td width="$widths[5]" align="right">$content[5]</td>
  </tr>
EOD;
  }    

//table footer
$content=array(
  "",
  "",
  "",
  "",
  "Total:",
  MyDecimal::format($invoice->getTotal()),
  );
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center">$content[0]</td>
    <td width="$widths[1]" >$content[1]</td>
    <td width="$widths[2]" align="right">$content[2]</td>
    <td width="$widths[3]" align="right">$content[3]</td>
    <td width="$widths[4]" align="right"><b>$content[4]</b></td>
    <td width="$widths[5]" align="right"><b>$content[5]</b></td>
  </tr>
EOD;

$content=array(
sfConfig::get('custom_cheques_payable_to'),
);
$tbl .= <<<EOD
</table>
<br>
<br>
<b>Warranty on Pumps</b>
<br> - 1 year warranty (labor only) on factory defect
<br> - No warranty on burnt coil
<br><b>Warranty on Tanks</b>
<br> - 3 Months warranty on workmanship and weld (parts not included)
<br> - Warranty only against leaks, not against dents and other damage due to mishandling
<br> - No warranty against operating above the recommended pressure (40psi) or against Vacuum
<br> - No warranty against corrosion due to chemical or salty water
<br>

<b>Payment Scheme:</b>

<br> - Payable within 90 days upon invoice of product
<br> - Payee: $content[0]
<br>
<br>
EOD;

$content=array(
sfConfig::get('custom_billing_statement_signatory_name'),
sfConfig::get('custom_billing_statement_signatory_position'),
sfConfig::get('custom_billing_statement_signatory_image'),
sfConfig::get('custom_company_phone'),
sfConfig::get('custom_company_email'),
sfConfig::get('custom_company_address'),
);
$tbl .= <<<EOD
<table border="1">
 <tr>
  <td align="center">Prepared By:</td>
  <td align="center">Warehouse:</td>
  <td align="center">Customer Confirmation:</td>
 </tr>
 <tr valign="bottom">
  <td align="center"><img src="$content[2]" width="50"><br>$content[0]<br>$content[1]</td>
  <td align="center"><br><br><br><br><br></td>
  <td align="center"></td>
 </tr>
</table>
<br>
<br><b>Tel: $content[3]</b>
<br>Email: $content[4]
<br>$content[5]



EOD;


$pdf->writeHTML($tbl, true, false, false, false, '');

//-----------------


//--------------------------------------------------

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$templatenamestripped=str_replace(" ","",$templatename);
$pdf->Output($templatenamestripped.$invoice->getInvno().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

