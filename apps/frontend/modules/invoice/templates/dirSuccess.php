<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new InvoiceForm(),"invoice/dir")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
?>
<table>
  <tr>
    <td>Date<?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>Warehouse: <?php
      $w=new sfWidgetFormDoctrineChoice(array(
        'model'     => 'Warehouse',
        'add_empty' => false,
      ));
      echo $w->render('warehouse_id',$warehouse_id);
      ?>
    </td>
  </tr>
  <tr>
    <td><input type=submit value=View ></td>
  </tr>
</table>
    <?php echo link_to("Yesterday","invoice/dir?invoice[date][day]=".$yesterday->getDay()."&invoice[date][month]=".$yesterday->getMonth()."&invoice[date][year]=".$yesterday->getYear()); ?>
    <?php echo link_to("Tomorrow","invoice/dir?invoice[date][day]=".$tomorrow->getDay()."&invoice[date][month]=".$tomorrow->getMonth()."&invoice[date][year]=".$tomorrow->getYear()); ?>
    <?php echo link_to("Go to DIR Multi Date","invoice/dirmulti?invoice[date][day]=".$today->getDay()."&invoice[date][month]=".$today->getMonth()."&invoice[date][year]=".$today->getYear()."&purchase[date][day]=".$today->getDay()."&purchase[date][month]=".$today->getMonth()."&purchase[date][year]=".$today->getYear()); ?>


</form>

<h1>Daily Inventory Report </h1>
Date: <?php echo MyDateTime::frommysql($form->getObject()->getDate())->toshortdate();$datearray=explode("-",$form->getObject()->getDate());?>

<br><?php echo link_to("Print","invoice/dirpdf?invoice[date][year]=".$datearray[0]."&invoice[date][month]=".$datearray[1]."&invoice[date][day]=".$datearray[2]);?>

<?php $producttypename="" ?>

<br>
<br>
*Unreleased / unreceived delivery receipts will not be shown here
<br>
<br>
<?php echo form_tag("invoice/processDir");?>
<input type=hidden name=date id=date value="<?php echo $form->getObject()->getDate()?>">
<input type=hidden name=warehouse_id id=warehouse_id value="<?php echo $warehouse_id?>">
<table border=1>
  <td>
    <?php if(count($array)!=0){?>
      <input name=submit type=submit value=Save >
      <!--
      <input name=submit type=submit value="Save Unreported" >
      -->
    <?php } ?>
  </td>
  <tr>
    <td>Product</td>
    <td>Start</td>
    <td>In</td>
    <td>Out</td>
    <td>End</td>
    <td>Rpt</td>
    <td>Comment</td>
    <td>Update</td>
    <td>Notes</td>
  </tr>
  <?php foreach($array as $stock_id=>$item){?>
  <?php if($item["product_type_name"]!=$producttypename){
    $producttypename=$item["product_type_name"];
  ?>
  <tr>
    <td><b><?php echo $item["product_type_name"]?></b></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php } ?>
  <tr>
    <td><?php echo link_to($item["product_name"],"stock/view?id=".$item['stock_id'])?></td>
    <td align=right><?php echo $item["endbalance"]+$item["out"]-$item["in"]?></td>
    <td align=right><?php echo $item["in"]?></td>
    <td align=right><?php echo $item["out"]?></td>
    <td align=right><?php echo $item["endbalance"]-0?></td>
    <td align=right><?php echo $item["qty_reported"]?></td>
    <td align=center><?php echo $item["comment"]?></td>
    <td align=right>
      <?php if($sf_user->hasCredential(array('admin','encoder'),false)){?>
      
        <!--If Save Unreported, This is saved instead-->
        <input type=hidden name=balance[<?php echo $item['stock_id']?>] id=balance[<?php echo $item['stock_id']?>] value=<?php if($item["qty_reported"]===null)echo $item["endbalance"];else echo $item["qty_reported"];?>>

        <input size=1 name=qty_reported[<?php echo $item['stock_id']?>] id=qty_reported[<?php echo $item['stock_id']?>] value=<?php echo $item["qty_reported"]?>>
      <?php } ?>
    </td>
    <td align=right>
      <?php echo $item["notes"]?>    
      <?php if($sf_user->hasCredential(array('admin','encoder'),false)){?>
        <input size=1 name=notes[<?php echo $item['stock_id']?>] id=notes[<?php echo $item['stock_id']?>] value=<?php echo $item["notes"]?>>
      <?php } ?>
    </td>
    <td>Print Barcode</td>
    <td><?php echo link_to("Small","product/barcodethermalpdf?product_id=".$item["product_id"]) ?></td>
    <td><?php echo link_to("Large","product/barcodethermallargepdf?product_id=".$item["product_id"]) ?></td>
  </tr>
  <?php } ?>
  <td>
    <?php if(count($array)!=0){?>
      <input name=submit type=submit value=Save >
      <!--
      <input name=submit type=submit value="Save Unreported" >
      -->
    <?php } ?>
  </td>
</table>
</form>

