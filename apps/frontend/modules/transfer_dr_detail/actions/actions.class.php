<?php

require_once dirname(__FILE__).'/../lib/transfer_dr_detailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/transfer_dr_detailGeneratorHelper.class.php';

/**
 * transferDr_detail actions.
 *
 * @package    sf_sandbox
 * @subpackage transferDr_detail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class transfer_dr_detailActions extends autoTransfer_dr_detailActions
{
/*
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());
    
    $qty=intval($requestparams["qty"]);
    if($qty<1)
    {
      $this->getUser()->setFlash('error', 'Invalid Quantity', true);
      return $this->redirect("transferDr/view?id=".$requestparams['transferDr_id']);
    }
  
    //detect if return already has an rdetail of same product id
    $rdetail=Doctrine_Query::create()
    ->from('TransferDrDetail rd')
    ->where('rd.transferDr_id = '.$requestparams['transferDr_id'])
    ->andWhere('rd.product_id = '.$requestparams['product_id'])
    ->fetchOne();

    //if rdetail with same product id exists, adjust qty for that
    if($rdetail!=null)
    {
      $ref=$rdetail->getRef();
      
      if($ref!=null)
      {
        //adjust ref qty returned
        $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
        $ref->save();
      }
    }
    //else create new rdetail
    else
    {
      $rdetail=new TransferDrDetail();
      $rdetail->setProductId($requestparams['product_id']);
      $rdetail->setTransferDrId($requestparams['transferDr_id']);

      $transferDr=$rdetail->getTransferDr();
      $rdetail->setRefClass("TransferDrDetail");//?????????
      
      //try to find an Transferdetail or Transferdetail that matches product_id
      if($transferDr->getIsSales())
      {
        $ref=Doctrine_Query::create()
          ->from('Transferdetail id')
          ->where('id.purchase_id = '.$rdetail->getTransferDr()->getRefId())
          ->andWhere('id.product_id = '.$requestparams['product_id'])
          ->fetchOne();
      }else{
        $ref=Doctrine_Query::create()
          ->from('Transferdetail id')
          ->where('id.purchase_id = '.$rdetail->getTransferDr()->getRefId())
          ->andWhere('id.product_id = '.$requestparams['product_id'])
          ->fetchOne();
      }
      //if transferdetail or transferdetail is found, use it as ref
      if($ref!=null)
      {
        $rdetail->setRefId($ref->getId());
        //adjust ref qty returned
        $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
        $ref->save();
      }

    }

    //adjust rdetail qty and calc prices
    $rdetail->setPrice($requestparams['price']);
    $rdetail->setTotal($requestparams['price']*$qty);
    $rdetail->setQty($qty);
    $rdetail->save();

    //adjust stock
    $rdetail->updateStockEntry();

    $this->getUser()->setFlash('notice', 'Successfully returned '.$rdetail->getProduct(), false);


    $this->redirect("transferDr/view?id=".$rdetail->getTransferDrId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $transferDr_detail=$this->getRoute()->getObject();

    if(count($transferDr_detail->getReplacementDetail())>0)
    {
      $this->getUser()->setFlash('error', "Cannot delete this returned item - replacements exist.");
      $this->redirect($request->getReferer());
    }
    
    if ($transferDr_detail->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("transferDr/view?id=".$transferDr_detail->getTransferDrId());
  }
*/
}
