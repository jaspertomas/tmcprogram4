<?php

require_once dirname(__FILE__).'/../lib/fileGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/fileGeneratorHelper.class.php';

/**
 * file actions.
 *
 * @package    sf_sandbox
 * @subpackage file
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class fileActions extends autoFileActions
{
  public function executeView(sfWebRequest $request)
  {
    $this->file = $this->getRoute()->getObject();
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    /*structure of requestfiles: array(1) { ["file"]=> array(5) { ["error"]=> int(0) ["name"]=> string(39) "Screenshot from 2016-10-07 11:13:08.png" ["type"]=> string(9) "image/png" ["tmp_name"]=> string(14) "/tmp/php0kIFzk" ["size"]=> int(14663) } } 
    */
    //transfer file size and mime type to request params
    $requestparams=$request->getParameter($form->getName());  
    $requestfiles=$request->getFiles($form->getName());  
    //if file is specified 
    if($requestfiles['file']['name']!="")
    {
      $uploadlocation=FileTable::genUploadLocation();//yyyymm of upload date
      $requestparams['uploadlocation']=$uploadlocation;
      $requestparams['filename']=$requestfiles['file']['name'];
      $requestparams['filetype']=$requestfiles['file']['type'];
      $requestparams['filesize']=$requestfiles['file']['size'];
    }
    //else if file not specified and record is new
    else
    {
      if($requestparams['id']=="")
      {
        $this->getUser()->setFlash('error', 'Please specify a file for upload', true);
        $this->redirect($request->getReferer());
      }
    }

    if(trim($requestparams['title'])=="")
    {
      $this->getUser()->setFlash('error', 'Please enter a title', true);
      $this->redirect($request->getReferer());
    }
 
    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $file = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $file)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        //$this->redirect('@file_new');
        $this->redirect($request->getReferer());
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        //$this->redirect(array('sf_route' => 'file_edit', 'sf_subject' => $file));
        $this->redirect($request->getReferer());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', true);
      $this->redirect($request->getReferer());
    }
  }
  public function executeSearch(sfWebRequest $request)
  {
    if(trim($request->getParameter("searchstring"))=="")
    {
      $this->files=array();
      return;
    }
  
    $keywords=explode(" ",$request->getParameter("searchstring"));

		//search in name
    $query=Doctrine_Query::create()
        ->from('File f')
        ->orderBy("f.title")
      	//->where('f.id == 0')
      	//->andWhere('s.file_id = p.id')
      	//->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	;

    foreach($keywords as $keyword)
    {
    	$query->orWhere("f.title LIKE '%".$keyword."%'");
    	$query->orWhere("f.description LIKE '%".$keyword."%'");
    	$query->orWhere("f.keywords LIKE '%".$keyword."%'");
    }
  	$this->files=$query->execute();

    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $file=$this->getRoute()->getObject();
    if ($file->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    if($file->getParentClass()!="")
      $this->redirect(strtolower($file->getParentClass())."/files?id=".$file->getParentId());
    else
      $this->redirect('@file');
  }
  public function executeFiles(sfWebRequest $request)
  {
    $this->file=MyModel::fetchOne("File",array('id'=>$request->getParameter("id")));
    $this->filee=new File();
    $this->filee->setParentClass('file');
    $this->filee->setParentId($this->file->getId());
    $this->form=new FileForm($this->filee);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="file"')
      ->andWhere('f.parent_id='.$this->file->getId())
      ->execute();
  }
}
