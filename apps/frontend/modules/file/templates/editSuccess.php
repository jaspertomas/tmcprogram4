<?php use_helper('I18N', 'Date') ?>
<?php include_partial('file/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit File', array(), 'messages') ?></h1>

  <?php include_partial('file/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('file/form_header', array('file' => $file, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('file/form', array('file' => $file, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('file/form_footer', array('file' => $file, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>

<hr>
<?php if($file->getFilename()==""){?>

NO FILE UPLOADED

<?php }else{ ?> 
    <a download="<?php echo $file->getFilename()?>" href="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace(array("/index.php","/frontend_dev.php"),"",$_SERVER['SCRIPT_NAME'])?>/uploads/files/<?php echo $file->getUploadlocation()."/".$file->getFile() ?>"">(Download)</a><br>
  <?php if(strpos($file->getFiletype(), 'image') !== false){ ?>
    <img src="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace(array("/index.php","/frontend_dev.php"),"",$_SERVER['SCRIPT_NAME'])?>/uploads/files/<?php echo $file->getUploadlocation()."/".$file->getFile() ?>" />
  <?php }else{ ?>
    <?php echo $file->getFilename()." (".$file->getFiletype().")"?>
  <?php } ?>
<?php } ?>

