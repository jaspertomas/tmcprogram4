<?php

require_once dirname(__FILE__).'/../lib/employeeGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/employeeGeneratorHelper.class.php';

/**
 * employee actions.
 *
 * @package    sf_sandbox
 * @subpackage employee
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class employeeActions extends autoEmployeeActions
{
  public function executeProcessmgrpasswd(sfWebRequest $request)
  {
  	$oldpass=$request->getParameter("oldpass");
  	$newpass1=$request->getParameter("newpass1");
  	$newpass2=$request->getParameter("newpass2");
  	
  	if($oldpass=='')
  	{
        $message="Please enter old password";
        $this->getUser()->setFlash('error', $message,true);
        return $this->redirect($request->getReferer());
  	}
  	else if($newpass1=='')
  	{
        $message="Please enter new password";
        $this->getUser()->setFlash('error', $message,true);
        return $this->redirect($request->getReferer());
  	}
  	else if($newpass2=='')
  	{
        $message="Please enter new password twice";
        $this->getUser()->setFlash('error', $message,true);
        return $this->redirect($request->getReferer());
  	}
  	else if($newpass2!=$newpass1)
  	{
        $message="New passwords do not match";
        $this->getUser()->setFlash('error', $message,true);
        return $this->redirect($request->getReferer());
  	}
  	
    $setting=Doctrine_Query::create()
        ->from('Settings s')
      	->where('s.name = "manager_password"')
      	->fetchOne();
  	
  	if($setting->getValue()!=$oldpass)
  	{
        $message="Wrong password";
        $this->getUser()->setFlash('error', $message,true);
        return $this->redirect($request->getReferer());
  	}
  	else
  	{
  		$setting->setValue($newpass1);
  		$setting->save();
        $message="Manager password successfully changed";
        $this->getUser()->setFlash('msg', $message,true);
        return $this->redirect($request->getReferer());
  	}
  	
  	$this->redirect($request->getReferer());
  }
  public function executeMgrpasswd(sfWebRequest $request)
  {
    $this->setting=Doctrine_Query::create()
        ->from('Settings s')
      	->where('s.name = "manager_password"')
      	->fetchOne();
  }


  protected function processForm(sfWebRequest $request, sfForm $form)
  {
      $requestparams=$request->getParameter($form->getName());

      //validate: 
      //hr cannot edit commission rate
      //if commission rate changed 
      //and user is not admin
      //error
      $oldEmployee=Fetcher::fetchOne("Employee",array("id"=>$requestparams["id"]));
      if($oldEmployee!=null)
      if(
            $oldEmployee->getCommission()!=$requestparams["commission"]
            and
            !$this->getUser()->hasCredential(array('admin'),false)
      )
      {
            return $this->redirect('home/error?msg=You do not have the permissions to edit commission rate');
      }

    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $employee = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $employee)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@employee_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect(array('sf_route' => 'employee_edit', 'sf_subject' => $employee));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

}
