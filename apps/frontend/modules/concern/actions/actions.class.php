<?php

require_once dirname(__FILE__).'/../lib/concernGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/concernGeneratorHelper.class.php';

/**
 * concern actions.
 *
 * @package    sf_sandbox
 * @subpackage concern
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class concernActions extends autoConcernActions
{
  public function executeNew(sfWebRequest $request)
  {
    $this->concern = new Concern();
    $this->concern->setDate(MyDate::today());
       		
    $this->form = $this->configuration->getForm($this->concern);
  }
}
