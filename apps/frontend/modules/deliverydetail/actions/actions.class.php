<?php

require_once dirname(__FILE__).'/../lib/deliverydetailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/deliverydetailGeneratorHelper.class.php';

/**
 * deliverydetail actions.
 *
 * @package    sf_sandbox
 * @subpackage deliverydetail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class deliverydetailActions extends autoDeliverydetailActions
{
    public function executeSetQty(sfWebRequest $request)
    {
        if($request->getParameter('value')==0)
        {
          $message="Invalid qty";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
    
        $this->detail=Doctrine_Query::create()
        ->from('Deliverydetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setQty($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $delivery=$this->detail->getDelivery();
        $delivery->calc();
        $delivery->save();

        if($this->detail->getDelivery()->getIsDelivered())
        {
	        $this->detail->updateStockentry();
        }
        
        $this->redirect($request->getReferer());
    }
    public function executeSetDiscrate(sfWebRequest $request)
    {
        //reject anything except numbers and spaces
        $pattern = '/^[0-9 ]+$/';
        if ( !preg_match ($pattern, $request->getParameter('value')) and $request->getParameter('value')!="")
        {
          $message="Invalid discount rate";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }    
    
        $this->detail=Doctrine_Query::create()
        ->from('Deliverydetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setDiscrate($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $delivery=$this->detail->getDelivery();
        $delivery->calc();
        $delivery->save();

        if($this->detail->getDelivery()->getIsDelivered())
        {
	        $this->detail->updateStockentry();
        }
        
        $this->redirect($request->getReferer());
    }
    public function executeSetPrice(sfWebRequest $request)
    {
        if($request->getParameter('value')==0)
        {
          $message="Invalid price";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
    
        $this->detail=Doctrine_Query::create()
        ->from('Deliverydetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setPrice($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $delivery=$this->detail->getDelivery();
        $delivery->calc();
        $delivery->save();

        if($this->detail->getDelivery()->getIsDelivered())
        {
	        $this->detail->updateStockentry();
        }
        
        $this->redirect($request->getReferer());
    }
    public function executeSetDescription(sfWebRequest $request)
    {
        $this->detail=Doctrine_Query::create()
        ->from('Deliverydetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setDescription($request->getParameter('value'));
        $this->detail->save();
        
        $this->redirect($request->getReferer());
    }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $deliverydetail = $form->save();
        $deliverydetail->calc();
        $deliverydetail->save();

        $delivery=$deliverydetail->getDelivery();
        $delivery->calc();
        $delivery->save();
        //if deliverydetail is created / edited while delivery is already delivered
        //note: stockentry must only be created when delivery is delivered
        //note: if delivery is cancelled, stock entry will not exist
        if($delivery->getIsDelivered())
	        $deliverydetail->updateStockentry();
        else
	        $deliverydetail->deleteStockentry();
        
        ////create deliveryconversion if necessary, 
        ////allowing conversion from parts to finished product in dr
        //look for the conversion that this product belongs to, if any
        $conversiondetails=$deliverydetail->getProduct()->getConversiondetail();
        $conversion=null;
        foreach($conversiondetails as $cd)
        {
          $conversion=$cd->getConversion();

          //if conversion exists (if product belongs to a conversion)
          //and the delivery does not have a deliveryconversion for it yet
          //create
          if($conversion)
          {
            $delivery=$deliverydetail->getDelivery();
            $deliveryconversion=Doctrine_Query::create()
            ->from('Deliveryconversion dc')
            ->where('dc.parent_id = '.$delivery->getId())
            ->andWhere('dc.conversion_id = '.$conversion->getId())
            ->fetchOne();
            //if $deliveryconversion does not exist
            if($deliveryconversion==null)
            {
              //create
              $deliveryconversion=new Deliveryconversion();
              $deliveryconversion->setParentId($delivery->getId());
              $deliveryconversion->setConversionId($conversion->getId());
              $deliveryconversion->save();
            }
          }
        }

      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $deliverydetail)));

      $this->getUser()->setFlash('notice', $notice);
      $this->redirect(strtolower($detail->getParentClass).'/view?id='.$deliverydetail->getParentId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $detail=$this->getRoute()->getObject();
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $detail)));

    if ($detail->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
      $delivery=$detail->getDelivery();
      $delivery->calc();
      $delivery->save();
   }

    $this->redirect(strtolower($detail->getParentClass).'/view?id='.$detail->getParentId());
  }
}
