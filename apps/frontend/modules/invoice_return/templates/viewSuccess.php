<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Invoice Return <?php echo $invoice_return->getCode();?></h1>
<h3>For <?php echo link_to($invoice_return->getInvoice(),"invoice/view?id=".$invoice_return->getInvoiceId())?></h3>
<h3></h3>
<h3><?php echo link_to("Edit","invoice_return/edit?id=".$invoice_return->getId())?></h3>

<?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
  <?php //echo form_tag_for($form,'invoice_return/adjust')?> <?php echo $form['id'] ?>
<?php }?>	
<table>
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($invoice_return->getDate())->toprettydate() ?></td>
        </tr>
        
        <tr valign=top>
          <td>Customer</td>
          <td><?php echo link_to($invoice_return->getCustomer(),"customer/view?id=".$invoice_return->getCustomerId(),array("target"=>"edit_customer")); ?></td>
        </tr>


      <table>
      <tr>
          <td>Reason</td>
          <td><?php echo $invoice_return->getReason() ?>
          </td>
        </tr>
        <tr>
          <td>Notes</td>
          <td><?php echo $invoice_return->getNotes() ?>
          </td>
        </tr>
      </table>

      <?php 
      if($sf_user->hasCredential(array('admin','encoder'), false))// and $cp->getBalance()>0
        echo link_to('Delete','invoice_return/delete?id='.$invoice_return->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?'));
      ?> |

<br>
<br>
      <?php 
        //if invoice is closed, is "new version" (DRs enabled) and is not cancelled
        if($invoice->getIsTemporary()==0 and $invoice->getIsDrBased()==1 and $invoice->getStatus()!="Cancelled")
        //if user is admin or encoder
        if($sf_user->hasCredential(array('admin','encoder'), false))// and $cp->getBalance()>0
        {
          //there should only be 1 return dr unless admin allows
          // allow creation of return dr 
          // only if there is no return dr yet
          // or if user is admin
          $return_drs=$invoice_return->getReturnDrs();
          if(count($return_drs)==0 or $sf_user->hasCredential(array('admin'), false))
          echo link_to("Generate Return DR","invoice_dr/new?is_return=1&invoice_return_id=".$invoice_return->getId())."<br>";

          if($invoice_return->getReturnType()=="Replace")
          {
            //there should only be 1 replacement dr unless admin allows
            // allow creation of replacement dr 
            // only if there is no replacement dr yet
            // or if user is admin
            $replacement_drs=$invoice_return->getReplacementDrs();
            if(count($replacement_drs)==0 or $sf_user->hasCredential(array('admin'), false))
            echo link_to("Generate Replacement DR","invoice_dr/new?is_return=0&invoice_return_id=".$invoice_return->getId())."<br>";
          }
          /*
          else if($invoice_return->getReturnType()=="Refund")
          {
            echo link_to("Generate Check Voucher","voucher/new?invoice_return_id=".$invoice_return->getId());
          }
          */
        }
      ?>

<br>
<br>
Related DRs:
<?php foreach($invoice_return->getInvoiceDr() as $dr){?>
  <?php echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".link_to($dr,"invoice_dr/view?id=".$dr->getId())." ".MyDateTime::fromdatetime($dr->getDatetime())->toshortdate();?> 
  <?php if($dr->isCancelled()){echo "<font color=red>(Cancelled)</font>";?>
  <?php } else { ?>
    <?php if($dr->getQtyForRelease()==0 and $dr->getQtyForReturn()==0)echo "(Empty)"?>
    <?php if($dr->getQtyForRelease()>0)echo "(".$dr->getQtyForRelease()." ".($dr->isReleased()?"Released":"Pending Release").")"?>
    <?php if($dr->getQtyForReturn()>0)echo "(".$dr->getQtyForReturn()." ".($dr->isReleased()?"Returned":"Pending Return").")"?>
  <?php } ?>
<?php } ?>

<br>
<br>
Related Vouchers:
<?php foreach($invoice_return->getVouchers() as $voucher){
    $date=$voucher->getDate();
    $out_check=$voucher->getOutCheck();
    if($out_check)$date=$out_check->getCheckDate();
  ?>
  <?php echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".link_to($voucher,"voucher/view?id=".$voucher->getId())." P".MyDecimal::format($voucher->getAmount())." ".MyDateTime::fromdatetime($date)->toshortdate();?> 
  <?php if($voucher->isCancelled())echo "<font color=red>(Cancelled)</font>";?>
<?php } ?>
