<?php

require_once dirname(__FILE__).'/../lib/invoice_returnGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/invoice_returnGeneratorHelper.class.php';

/**
 * invoice_return actions.
 *
 * @package    sf_sandbox
 * @subpackage invoice_return
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoice_returnActions extends autoInvoice_returnActions
{

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
	  //created at, updated at
	  $requestparams=$request->getParameter($form->getName());
	  $requestparams["updated_by_id"]=$this->getUser()->getGuardUser()->getId();
	  $requestparams["updated_at"]=MyDateTime::now()->todatetime();
	  if($form->getObject()->isNew())
	  {
		$requestparams["created_by_id"]=$this->getUser()->getGuardUser()->getId();
		$requestparams["created_at"]=MyDateTime::now()->todatetime();
	  }

	  //validate reason is required
	  if($requestparams["reason"]=="")
	  {
		return $this->redirect("home/error?msg=Please enter a reason");
	  }

	  $form->bind($requestparams, $request->getFiles($form->getName()));
	  if ($form->isValid())
	  {
		$notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';
  
		try {
		  $invoice_return = $form->save();
		} catch (Doctrine_Validator_Exception $e) {
  
		  $errorStack = $form->getObject()->getErrorStack();
  
		  $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
		  foreach ($errorStack as $field => $errors) {
			  $message .= "$field (" . implode(", ", $errors) . "), ";
		  }
		  $message = trim($message, ', ');
  
		  $this->getUser()->setFlash('error', $message);
		  return sfView::SUCCESS;
		}
  
		$this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $invoice_return)));
  
		$this->getUser()->setFlash('notice', $notice);
		$this->redirect("invoice_return/view?id=".$invoice_return->getId());
	  }
	  else
	  {
		$invoice_return = new InvoiceReturn();
		$invoice_return->setCustomerId($requestparams["customer_id"]);
		$invoice_return->setInvoiceId($requestparams["invoice_id"]);
		$date=$requestparams["date"];
		$invoice_return->setDate($date["year"]."-".$date["month"]."-".$date["day"]);
		$invoice_return->setReturnType($requestparams["return_type"]);
		$invoice_return->setCode($requestparams["code"]);
		$invoice_return->setReason($requestparams["reason"]);
		$invoice_return->setNotes($requestparams["notes"]);
		$invoice_return->setStatus($requestparams["status"]);
		$invoice_return->setCreatedAt($requestparams["created_at"]);
		$invoice_return->setCreatedById($requestparams["created_by_id"]);
		$invoice_return->setUpdatedAt($requestparams["updated_at"]);
		$invoice_return->setUpdatedById($requestparams["updated_by_id"]);
		$this->form = new InvoiceReturnForm($invoice_return);
		$this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
	  }
	}
    public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

	$item=$this->getRoute()->getObject();

	$drs=$item->getInvoiceDr();
	if(count($drs)>0)
		return $this->redirect('home/error?msg=Cannot delete because DRs exist. Please delete them first.');

    if ($item->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('invoice/view?id='.$item->getInvoiceId());
  }
  public function executeView(sfWebRequest $request)
  {
	$this->invoice_return=Fetcher::fetchOne("InvoiceReturn",array("id"=>$request->getParameter("id")));
	$this->invoice=$this->invoice_return->getInvoice();
    $this->form = new InvoiceReturnForm($this->invoice_return);
  }
  public function executeNew(sfWebRequest $request)
  {
	$this->return_type=$request->getParameter("return_type");
	$this->invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("invoice_id")));
    $this->invoice_return = new InvoiceReturn();
	$this->invoice_return->setCustomerId($this->invoice->getCustomerId());
	$this->invoice_return->setInvoiceId($this->invoice->getId());
	$this->invoice_return->setDate(MyDate::today());
	$this->invoice_return->setReturnType($this->return_type);
	$this->invoice_return->genCode();
    $this->form = new InvoiceReturnForm($this->invoice_return);
  }
}
