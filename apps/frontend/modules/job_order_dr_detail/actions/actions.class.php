<?php

require_once dirname(__FILE__).'/../lib/job_order_dr_detailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/job_order_dr_detailGeneratorHelper.class.php';

/**
 * job_order_dr_detail actions.
 *
 * @package    sf_sandbox
 * @subpackage job_order_dr_detail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class job_order_dr_detailActions extends autoJob_order_dr_detailActions
{
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    protected function processForm(sfWebRequest $request, sfForm $form)
    {
      $requestparams=$request->getParameter($form->getName());
      
      $qty=intval($requestparams["qty"]);
      if($qty<1)
      {
        $this->getUser()->setFlash('error', 'Invalid Quantity', true);
        return $this->redirect("job_order_dr/view?id=".$requestparams['job_order_dr_id']);
      }
    
      //detect if return already has an rdetail of same product id
      $rdetail=Doctrine_Query::create()
      ->from('JobOrderDrDetail rd')
      ->where('rd.job_order_dr_id = '.$requestparams['job_order_dr_id'])
      ->andWhere('rd.product_id = '.$requestparams['product_id'])
      ->fetchOne();
  
      //if rdetail with same product id exists, adjust qty for that
      if($rdetail!=null)
      {
        $ref=$rdetail->getRef();
        
        if($ref!=null)
        {
          //adjust ref qty returned
          $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
          $ref->save();
        }
      }
      //else create new rdetail
      else
      {
        $rdetail=new JobOrderDrDetail();
        $rdetail->setProductId($requestparams['product_id']);
        $rdetail->setJobOrderDrId($requestparams['job_order_dr_id']);
  
        $job_order_dr=$rdetail->getJobOrderDr();
        $rdetail->setRefClass($job_order_dr->getIsSales()?"JobOrderDetail":"JobOrderDetail");
        
        //try to find an JobOrderDetail or JobOrderDetail that matches product_id
        if($job_order_dr->getIsSales())
        {
          $ref=Doctrine_Query::create()
            ->from('JobOrderDetail id')
            ->where('id.job_order_id = '.$rdetail->getJobOrderDr()->getRefId())
            ->andWhere('id.product_id = '.$requestparams['product_id'])
            ->fetchOne();
        }else{
          $ref=Doctrine_Query::create()
            ->from('JobOrderDetail id')
            ->where('id.job_order_id = '.$rdetail->getJobOrderDr()->getRefId())
            ->andWhere('id.product_id = '.$requestparams['product_id'])
            ->fetchOne();
        }
        //if job_orderdetail or job_orderdetail is found, use it as ref
        if($ref!=null)
        {
          $rdetail->setRefId($ref->getId());
          //adjust ref qty returned
          $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
          $ref->save();
        }
  
      }
  
      //adjust rdetail qty and calc prices
      $rdetail->setPrice($requestparams['price']);
      $rdetail->setTotal($requestparams['price']*$qty);
      $rdetail->setQty($qty);
      $rdetail->save();
  
      //adjust stock
      $rdetail->updateStockEntry();
  
      $this->getUser()->setFlash('notice', 'Successfully returned '.$rdetail->getProduct(), false);
  
  
      $this->redirect("job_order_dr/view?id=".$rdetail->getJobOrderDrId());
    }
    public function executeDelete(sfWebRequest $request)
    {
      $request->checkCSRFProtection();
  
      $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));
  
      $job_order_dr_detail=$this->getRoute()->getObject();
  
      if(count($job_order_dr_detail->getReplacementDetail())>0)
      {
        $this->getUser()->setFlash('error', "Cannot delete this returned item - replacements exist.");
        $this->redirect($request->getReferer());
      }
      
      if ($job_order_dr_detail->delete())
      {
        $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
      }
  
      $this->redirect("job_order_dr/view?id=".$job_order_dr_detail->getJobOrderDrId());
    }
  }
