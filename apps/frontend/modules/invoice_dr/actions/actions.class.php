<?php

require_once dirname(__FILE__).'/../lib/invoice_drGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/invoice_drGeneratorHelper.class.php';

/**
 * invoice_dr actions.
 *
 * @package    sf_sandbox
 * @subpackage invoice_dr
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoice_drActions extends autoInvoice_drActions
{
  public function executeFilter(sfWebRequest $request)
  {
    $this->setPage(1);

    $this->filters = $this->configuration->getFilterForm($this->getFilters());

    $requestparams=$request->getParameter($this->filters->getName());
    if($request->hasParameter("customer_id"))
      $requestparams["customer_id"]=$request->getParameter("customer_id");

    $this->filters->bind($requestparams);
    if ($this->filters->isValid())
    {
      $this->setFilters($this->filters->getValues());

      $this->redirect('@invoice_dr');
    }

    $this->pager = $this->getPager();
    $this->sort = $this->getSort();

    $this->setTemplate('index');
  }

  public function executeNew(sfWebRequest $request)
  {
    if($request->hasParameter("invoice_id"))
    {
      $invoice=Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter("invoice_id")));
      if(!$invoice)return $this->redirect("home/error?msg=Invoice not found");
      $invoice_dr = InvoiceDrTable::genDrForInvoiceId($invoice->getId(),$this->getUser()->getGuardUser());
    }
    else if($request->hasParameter("invoice_return_id"))
    {
      $invoice_return=Fetcher::fetchOne("InvoiceReturn",array("id"=>$request->getParameter("invoice_return_id")));
      if(!$invoice_return)return $this->redirect("home/error?msg=Invoice Return not found");
      $invoice_dr = InvoiceDrTable::genDrForInvoiceReturnId($invoice_return->getId(),$this->getUser()->getGuardUser(),$request->getParameter("is_return"));
    }

    $this->redirect("invoice_dr/view?id=".$invoice_dr->getId());
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $invoice_dr = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $invoice_dr)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect('invoice_dr/view?id='.$invoice_dr->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

  public function executeView(sfWebRequest $request)
  {
    //fetch return by id
    $this->invoice_dr=Fetcher::fetchOne("InvoiceDr",array("id"=>$request->getParameter('id')));

    $this->form = $this->configuration->getForm($this->invoice_dr);

    $this->invoice=$this->invoice_dr->getInvoice();

    //arrange returndetails into an array with invoicedetail as key
    $this->dr_details=$this->invoice_dr->getInvoiceDrDetailsIndexedByInvoicedetailId();
    
    $this->warehouses=Fetcher::fetch("Warehouse",array());
    /*
    $detail=new InvoiceDrDetail();
    $detail->setQty(1);
    $this->product_is_set=false;
    $this->product=null;
    $this->detailform = new InvoiceDrDetailForm($detail);
    $this->detail = $detail;
    $this->searchstring=$this->request->getParameter('searchstring');
    */
  }
  public function executeSetQtys(sfWebRequest $request)
  {
    //fetch return by id
    $dr=Fetcher::fetchOne("InvoiceDr",array("id"=>$request->getParameter('id')));

    if($dr->isReleased())
    {
      $error="Cannot set qty, DR is already released";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('invoice_dr/view?id='.$dr->getId());
    }
    if($dr->isCancelled())
    {
      $error="Cannot set qty, DR is cancelled";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('invoice_dr/view?id='.$dr->getId());
    }

    //arrange returndetails into an array with invoicedetail as key
    //Usage:
    //$dr_detail = $dr_details[$invoicedetail_id] 
    $dr_details=$dr->getInvoiceDrDetailsIndexedByInvoicedetailId();


    // var_dump($request->getParameter('qtys'));die();
    foreach($request->getParameter('qtys') as $invoicedetail_id => $qty)
    {
      $invoicedetail=Fetcher::fetchOne("Invoicedetail",array("id"=>$invoicedetail_id));
      $qty=intval($qty);

      //for returns, 
      //for cosmetic purposes only
      //invert quantities
      if($dr->isReturn())$qty*=-1;

      //validate: qty must not be greater than remaining
      $qty_remaining=$invoicedetail->getQty()-$invoicedetail->getQtyReleased();

      $action_string="Cannot release";
      if($dr->isReturn())$action_string="Cannot return";
      if($qty>$qty_remaining)
      {
        $error=$action_string." ".$qty." units, only ".$qty_remaining." units remaining";
        $this->getUser()->setFlash('error', $error);
        return $this->redirect('invoice_dr/view?id='.$dr->getId());
      }

      //if deliverydetail exists, just save the qty
      if(array_key_exists($invoicedetail_id, $dr_details))
      {
        $dr_detail=$dr_details[$invoicedetail_id];
        $dr_detail->setQty($qty);
        $dr_detail->save();

        //set as return if qty is negative
        $dr_detail->setIsReturn(($qty<0)?1:0);
      }
      //else if deliverydetail does not exist, create it
      else 
      {
        //if qty is 0, don't do anything
        if($qty==0)continue;
      
        $dr_detail=new InvoiceDrDetail();
        $dr_detail->setInvoiceDrId($dr->getId());
        $dr_detail->setInvoiceId($dr->getInvoiceId());
        $dr_detail->setCustomerId($dr->getCustomerId());
        $dr_detail->setWarehouseId($dr->getWarehouseId());

        $dr_detail->setInvoicedetailId($invoicedetail_id);
        $id=$invoicedetail;
        $dr_detail->setDatetime($dr->getDatetime());
        $dr_detail->setProductId($id->getProductId());
        $dr_detail->setQty($qty);
        //set as return if qty is negative
        $dr_detail->setIsReturn(($qty<0)?1:0);

        $user=$this->getUser()->getGuardUser();
        $dr_detail->setCreatedById($user->getId());
        $dr_detail->setUpdatedById($user->getId());
        $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
        $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr_detail->save();
      }
    }

    $dr->calc();

    return $this->redirect("invoice_dr/view?id=".$dr->getId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $invoice_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $invoice_dr)));

    if ($invoice_dr->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("invoice/view?id=".$invoice_dr->getInvoiceId());
  }
  public function executeCancel(sfWebRequest $request)
  {
    $invoice_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $invoice_dr)));

    if ($invoice_dr->cascadeCancel())
    {
      $this->getUser()->setFlash('notice', 'The item was cancelled successfully.');
    }

    $this->redirect("invoice_dr/view?id=".$invoice_dr->getId());
  }
  public function executeUndoCancel(sfWebRequest $request)
  {
    $invoice_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $invoice_dr)));

    if ($invoice_dr->cascadeUndoCancel())
    {
      $this->getUser()->setFlash('notice', 'The item was uncancelled successfully.');
    }

    $this->redirect("invoice_dr/view?id=".$invoice_dr->getId());
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('invoice_dr');
    $id=$requestparams['id'];

    $this->forward404Unless(
    $this->invoice_dr=Doctrine_Query::create()
    ->from('InvoiceDr i')
    ->where('i.id = '.$id)
    ->fetchOne()
    , sprintf('InvoiceDr with id (%s) not found.', $request->getParameter('id')));

    $this->invoice_dr->setNotes($requestparams["notes"]);
    $this->invoice_dr->setWarehouseId($requestparams["warehouse_id"]);
    Doctrine_Query::create()
      ->update('InvoiceDrDetail idd')
      ->set('idd.warehouse_id=?',$requestparams["warehouse_id"])
      ->where('idd.invoice_dr_id=?', $this->invoice_dr->getId())
      ->execute();

    //set date and adjust stock entry date if necessary
    if(isset($requestparams["datetime"]))
    {
      $newdatetime=
        $requestparams["datetime"]["year"]."-".
        $requestparams["datetime"]["month"]."-".
        $requestparams["datetime"]["day"]."-".
        $requestparams["datetime"]["hour"]."-".
        $requestparams["datetime"]["minute"];
      $this->invoice_dr->setDatetime($newdatetime);
      foreach($this->invoice_dr->getInvoiceDrDetail() as $detail)
      {
        $detail->setDatetime($newdatetime);
        $detail->save();

			  //update stockentry
			  $stockentry=$detail->getStockentry();
			  $stockentry->setDatetime($newdatetime);
			  $stockentry->save();
			  $stockentry->getStock()->calcFromStockEntry($stockentry);
      }
      
      //no need for this, change date allowed only
      //when dr is not released
      //stock entries will be generated when it is released
      // $this->invoice_dr->setDateAndUpdateStockEntry($newdatetime);
    }
    
    $this->invoice_dr->save();

    $this->redirect($request->getReferer());
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeRelease(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('InvoiceDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
    
    if($request->getParameter('today')=="true")
    {
      $dr->setDatetime(MyDateTime::now()->todatetime());
      $dr->save();
    }
    $error=$dr->release();
    if($error!=false)
    {
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('invoice_dr/view?id='.$dr->getId());
    }

    //todo:create history

    $this->redirect($request->getReferer());
  }
  public function executeUndoRelease(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('InvoiceDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    $dr->undoRelease();

    //todo:create history

    $this->redirect($request->getReferer());
  }
  //this can only be called if DR is not yet released
  public function executeSetInvoiceDateAsReleaseDate(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('InvoiceDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    $invoice=$dr->getInvoice();
    $dr->setDatetime($invoice->getDate());
    $dr->save();

    $this->redirect($request->getReferer());
  }
  public function executeRecalc(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('InvoiceDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
    $dr->regenMissingDetails($this->getUser()->getGuardUser());
    $dr->calc();
    $this->redirect($request->getReferer());
  }
  public function executeRecalcAll(sfWebRequest $request)
  {
    echo "ACCESS DENIED";die();
    $drs=Doctrine_Query::create()
      ->from('InvoiceDr dr')
      ->execute();
    foreach($drs as $dr)
    {
      //$dr->calc();
      $invoice=$dr->getInvoice();
      $invoice->setIsDrBased(1);
      $invoice->save();
    }
    $this->redirect("home/index");
  }
}
