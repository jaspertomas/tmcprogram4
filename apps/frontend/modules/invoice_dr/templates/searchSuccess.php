<?php use_helper('I18N', 'Date'); ?>
<h1>Invoice Search</h1>
<table border="1">
  <tr>
    <td>Inv Number</td>
    <td>Customer</td>
    <td>Particulars</td>
    <td>Total</td>
    <td>Status</td>
  </tr>
  
  <?php 
  $matchfound=false;
  foreach($invoices as $invoice)
  if(strtolower($invoice->getName())==strtolower($searchstring)){$matchfound=true;break;}
  if(!$matchfound){
  ?>
  <tr>
    <td><?php echo link_to($searchstring." (Create New)","invoice/new?name=".$searchstring) ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php } ?>
  <?php foreach($invoices as $invoice){?>
  <tr>
    <td><?php echo link_to($invoice->getName(),"invoice/view?id=".$invoice->getId()) ?></td>
    <td><?php echo $invoice->getCustomer() ?></td>
    <td><?php echo $invoice->getParticularsString() ?></td>
    <td><?php echo $invoice->getTotal() ?></td>
    <td><?php echo $invoice->getStatus() ?></td>
  </tr>
  <?php } ?>
</table>
