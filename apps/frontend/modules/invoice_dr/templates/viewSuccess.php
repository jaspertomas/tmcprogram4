<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h2><?php echo link_to("> Back to ".$invoice,"invoice/view?id=".$invoice->getId())?></h2>
<h2><?php if($invoice_dr->getInvoiceReturnId())echo link_to("> Back to ".$invoice_dr->getInvoiceReturn(),"invoice_return/view?id=".$invoice_dr->getInvoiceReturnId())?></h2>
<h1>
  <?php echo $invoice_dr->getTypeString(); ?> <?php echo $invoice_dr->getCode(); ?>
  <?php if($invoice_dr->isCancelled()){?>
    (<font color=red>Cancelled</font>)
  <?php }else{?>
    (<?php echo $invoice_dr->releaseStatusString(); ?>)
    <?php 
      if(!$invoice_dr->isReleased())
      {
        $olddatereleased=MyDateTime::fromdatetime($invoice_dr->getDatetime());
        $today=MyDateTime::now();
        echo "<br>".link_to($invoice_dr->getTypeActionString()." Today (".$today->toshortdate().")","invoice_dr/release?today=true&id=".$invoice_dr->getId());
        if($olddatereleased->tomysql()!=MyDate::today())echo "<br>".link_to($invoice_dr->getTypeActionString()." ".
        $olddatereleased->toshortdate(),"invoice_dr/release?id=".$invoice_dr->getId());
      }
      elseif($sf_user->hasCredential(array('admin'), false))
        echo link_to("Undo ".$invoice_dr->getTypeActionString(),"invoice_dr/undoRelease?id=".$invoice_dr->getId());
    ?>
  <?php }?>
</h1>
<?php slot('transaction_id', $invoice_dr->getId()) ?>
<?php slot('transaction_type', "InvoiceDr") ?>

<table>
  <tr valign=top>
    <td>
      <table>
          <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
          <?php echo form_tag_for($form,'invoice_dr/adjust')?> <?php echo $form['id'] ?>
          <?php }?>	
        <tr valign=top>
          <td>Release from Warehouse:</td>
          <td>
            <select name="invoice_dr[warehouse_id]">
              <?php foreach($warehouses as $warehouse){ ?>
                <option value=<?php echo $warehouse->getId()?> <?php if($warehouse->getId()==$invoice_dr->getWarehouseId())echo "selected=selected"?>><?php echo $warehouse->getName()?></option>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::fromdatetime($invoice_dr->getDatetime()) ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($invoice_dr->isPending()){?>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
            <?php echo $form['datetime'] ?>
            <?php } ?>
            <?php }?>	
          </td>
        </tr>
        <tr valign=top>
          <td>Customer</td>
          <td><?php echo link_to($invoice_dr->getCustomer(),"customer/view?id=".$invoice_dr->getCustomerId()); ?> </td>
        </tr>
        <tr valign=top>
          <td >Invoice:</td>
          <td>
            <?php if($invoice_dr->getInvoiceId())echo link_to($invoice,"invoice/view?id=".$invoice_dr->getInvoiceId())?>&nbsp;
          </td>
        </tr>
		</table>
    <td>
      <table>
        <tr>
          <td>Notes</td>
          <td><?php echo $invoice_dr->getNotes() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <textarea rows="2" cols="10" name="invoice_dr[notes]" id="invoice_dr_notes"><?php echo $invoice_dr->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <input type="submit" value="Save"></form>
            <?php }?>	
          </td>
        </tr>
		  </table>
    </td>
  </tr>
</table>

            <!--allow cancel only if not released and not cancelled-->
            <?php if($invoice_dr->isPending())echo link_to('Cancel','invoice_dr/cancel?id='.$invoice_dr->getId(), array('confirm' => 'Cancel: Are you sure?')) ?> |
            <!--only admin can delete and undo cancel-->
            <?php if($sf_user->hasCredential(array('admin'), false) and !$invoice_dr->isReleased()){?>
              <!--allow undo cancel only if cancelled-->
              <?php if($invoice_dr->isCancelled())echo link_to('Undo Cancel','invoice_dr/undoCancel?id='.$invoice_dr->getId(), array('confirm' => 'Undo Cancel: Are you sure?')) ?> |
              <?php echo link_to('Delete','invoice_dr/delete?id='.$invoice_dr->getId(), array('method' => 'delete', 'confirm' => 'Delete: Are you sure?')) ?> |
            <?php } ?>
            <?php echo link_to("Print","invoice_dr/viewPdf?id=".$invoice_dr->getId());?> |
            <!--allow set date to invoice date only if not released and not cancelled-->
            <?php if($invoice_dr->isPending())echo "<br>".link_to("Set invoice date as release date","invoice_dr/setInvoiceDateAsReleaseDate?id=".$invoice_dr->getId());?> |
            <br><?php echo link_to("Recalculate","invoice_dr/recalc?id=".$invoice_dr->getId())?>
<hr>

<?php echo form_tag("invoice_dr/setQtys");?>
<input type=hidden id=id name=id value=<?php echo $invoice_dr->getId()?>>
<table border=1>
  <tr>
    <td>Invoice</td>
    <td>DR</td>
    <td><?php echo $invoice_dr->isReturn()?"Returned":"Released"?></td>
    <td>Remaining</td>
    <td width=50%>Description</td>
    <td>Release</td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>Qty</td>
    <td>Qty</td>
    <td>Qty</td>
    <td></td>
    <td>Date</td>
  </tr>
  <?php 
  	foreach($invoice->getDetails() as $invoicedetail){
      $dr_detail=$dr_details[$invoicedetail->getId()];
      if($dr_detail==null)echo "<font color=red>SOMETHING IS WRONG. PLEASE PRESS RECALCULATE";
      else $deliveryqty=$dr_detail->getQty();

      //for returns, 
      //for cosmetic purposes only
      //invert quantities
      if($invoice_dr->isReturn())$deliveryqty*=-1;
  ?>
  <tr>
    <td><?php echo $invoicedetail->getQty() ?></td>
    <td>
      <?php if($invoice_dr->isPending()){?>
      <input id=qtys[<?php echo $invoicedetail->getId()?>] size=3 name=qtys[<?php echo $invoicedetail->getId()?>] value=<?php echo $deliveryqty?>>
      <br>
      <?php } ?>
      <?php echo $deliveryqty;?>
    </td>
    <td align=right ><?php if($dr_detail!=null)if($dr_detail->isReleased())echo $deliveryqty;else echo 0; ?></td>
    <td align=center>
      <?php 
      //disable: ignore prodict if is_service
      //if product is service, ignore
      // if($invoicedetail->getProduct()->isService())$remaining=0;
      // else
        $remaining=$invoicedetail->getQty()-$invoicedetail->getQtyReleased();
      //for returns, 
      //for cosmetic purposes only
      //invert quantities
      if($invoice_dr->isReturn())$remaining*=-1;
        if($remaining!=0)echo "<font color=red>".$remaining."</font>";
      ?>
    </td>
    <td><?php echo link_to($dr_detail->getProduct(),"stock/view?id=".$dr_detail->getStock()->getId()) ?></td>
    <td align=right ><?php if($dr_detail!=null)if($dr_detail->isReleased())echo MyDateTime::fromdatetime($dr_detail->getDatetime()) ?></td>
  <?php }?>
  
</table>
<?php if($invoice_dr->isPending()){?>
  <input type=submit value="Save">
<?php } ?>
</form>

<?php /*
<h2>Choose Product to Return</h2>

<b>Search product:</b> <input id=invoiceDrproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=invoiceDrclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@invoice_dr_detail",array("id"=>"new_invoice_dr_detail_form")); ?>
<input type=hidden name=invoice_dr_detail[invoice_dr_id] value=<?php echo $invoice_dr->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Product</td>
		<td>Qty</td>
		<td>Price</td>
		<td>Notes</td>
	</tr>

	<tr>
		<td>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<input name="invoiceDr_detail[product_id]" id="invoiceDr_detail_product_id" type="hidden" ?>
		<td><input size="5" name="invoiceDr_detail[qty]" value="1" id="invoiceDr_detail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="8" name="invoiceDr_detail[price]" value="0.00" id="invoiceDr_detail_price" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input name="invoiceDr_detail[description]" id="invoiceDr_detail_description" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=invoiceDr_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> >
</td>
	</tr>

	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>
</form>

<div id="invoiceDrsearchresult"></div>

<hr>
*/?>


<hr>
<br>
<br>
<br>
<br>


