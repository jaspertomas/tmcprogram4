<?php

require_once dirname(__FILE__).'/../lib/replacement_detailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/replacement_detailGeneratorHelper.class.php';

/**
 * replacement_detail actions.
 *
 * @package    sf_sandbox
 * @subpackage replacement_detail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class replacement_detailActions extends autoReplacement_detailActions
{
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());
    
    $qty=intval($requestparams["qty"]);
    if($qty<1)
    {
      $this->getUser()->setFlash('error', 'Invalid Quantity', true);
      return $this->redirect("replacement/view?id=".$requestparams['replacement_id']);
    }
  
    //detect if return already has an repdetail of same product id
    $repdetail=Doctrine_Query::create()
    ->from('ReplacementDetail rd')
    ->where('rd.replacement_id = '.$requestparams['replacement_id'])
    ->andWhere('rd.product_id = '.$requestparams['product_id'])
    ->fetchOne();

    //if repdetail with same product id exists, adjust qty for that
    if($repdetail!=null)
    {
      $ref=$repdetail->getReturnsDetail();
      
      if($ref!=null)
      {
        //adjust ref qty returned
        $ref->setQtyReplaced($ref->getQtyReplaced()-$repdetail->getQty()+$qty);
        $ref->save();
      }
    }
    //else create new repdetail
    else
    {
      $repdetail=new ReplacementDetail();
      $repdetail->setProductId($requestparams['product_id']);
      $repdetail->setReplacementId($requestparams['replacement_id']);

      $replacement=$repdetail->getReplacement();
      $repdetail->setRefClass($replacement->getIsSales()?"Invoicedetail":"Purchasedetail");
      
      //try to find an ReturnsDetail that matches product_id
      $ref=Doctrine_Query::create()
        ->from('ReturnsDetail id')
        ->where('id.returns_id = '.$repdetail->getReplacement()->getReturnsId())
        ->andWhere('id.product_id = '.$requestparams['product_id'])
        ->fetchOne();
      //if invoicedetail or purchasedetail is found, use it as ref
      if($ref!=null)
      {
        $repdetail->setReturnsDetailId($ref->getId());
        $repdetail->setRefId($ref->getRefId());
        //adjust ref qty returned
        $ref->setQtyReplaced($ref->getQtyReplaced()-$repdetail->getQty()+$qty);
        $ref->save();
      }

    }

    //adjust repdetail qty and calc prices
    $repdetail->setPrice($requestparams['price']);
    $repdetail->setTotal($requestparams['price']*$qty);
    $repdetail->setQty($qty);
    $repdetail->save();

    //adjust stock
    $repdetail->updateStockEntry();

    $this->getUser()->setFlash('notice', 'Successfully returned '.$repdetail->getProduct(), false);


    $this->redirect("replacement/view?id=".$repdetail->getReplacementId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $replacement_detail=$this->getRoute()->getObject();
    if ($replacement_detail->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("replacement/view?id=".$replacement_detail->getReplacementId());
  }
}
