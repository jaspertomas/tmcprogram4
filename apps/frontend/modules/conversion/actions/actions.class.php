<?php

require_once dirname(__FILE__).'/../lib/conversionGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/conversionGeneratorHelper.class.php';

/**
 * conversion actions.
 *
 * @package    sf_sandbox
 * @subpackage conversion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class conversionActions extends autoConversionActions
{
  public function executeView(sfWebRequest $request)
  {
    $this->conversion=Doctrine_Query::create()
      ->from('Conversion i')
    	->where('i.id = '.$request->getParameter('id'))
    	->fetchOne();
    $this->forward404Unless($this->conversion, sprintf('Conversion with id (%s) not found.', $request->getParameter('id')));
    
    //allow set product id by url
    $detail=new Conversiondetail();
    $detail->setQty(1);
    $this->product_is_set=false;
    if($request->getParameter("product_id"))
    {
      $detail->setProductId($request->getParameter("product_id"));
      $this->product_is_set=true;
    }
    $this->detailform = new ConversiondetailForm($detail);
    $this->detail = $detail;
    $this->searchstring=$this->request->getParameter('searchstring');
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());

    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $conversion = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $conversion)));

      $this->getUser()->setFlash('notice', $notice);
      $this->redirect('conversion/view?id='.$conversion->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    $conversion=$this->getRoute()->getObject();
    if ($conversion->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('conversion/index');
  }
  public function executeSearch(sfWebRequest $request)
  {
    $this->transaction_type=$request->getParameter("transaction_type");
    $this->transaction_id=$request->getParameter("transaction_id");

    if(trim($request->getParameter("searchstring"))=="")
    {
    	$this->conversions=array();
      return;
    }
  
    $keywords=explode(" ",$request->getParameter("searchstring"));

		//search in name
    $query=Doctrine_Query::create()
        ->from('Conversion p')
        ->orderBy("p.name")
      	;

    foreach($keywords as $keyword)
    	$query->andWhere("p.name LIKE '%".$keyword."%' or p.name LIKE '%".$keyword."%'");

  	$this->conversions=$query->execute();

    $this->searchstring=$request->getParameter("searchstring");
  }
  /*
  //create conversions for coupled pumps and tanks
  public function executeGenCoupledConversions(sfWebRequest $request)
  {
ConversionTable::genCoupledConversions();
$this->getUser()->setFlash('notice', "Conversions generated successfully");
$this->redirect($request->getReferer());
  }
  */
}
