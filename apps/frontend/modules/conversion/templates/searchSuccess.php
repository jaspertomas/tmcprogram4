<?php slot('transaction_id', $transaction_id) ?>
<?php slot('transaction_type', $transaction_type) ?>
<h1>Conversion Search</h1>
<table border=1>
<tr>
	<td>Name</td>
	<td>Description</td>
	<td></td>
</tr>
<?php foreach($conversions as $conversion){?>
<tr>
	<td>
    <?php if($transaction_type=="DeliveryMultiInput"){?>
	    <?php echo link_to("Use in DR","delivery/multiInputWizard?id=".$transaction_id."&conversion_id=".$conversion->getId()."&searchstring=".$searchstring);?>
	  <?php }else{?>
	    <?php echo link_to("Use in Conversion","delivery/view?id=".$transaction_id."&conversion_id=".$conversion->getId()."&searchstring=".$searchstring);?>
	  <?php }?>
	</td>
	<td><?php echo link_to($conversion->getName(),"conversion/view?id=".$conversion->getId()) ?></td>
	<td><?php echo $conversion->getDescription()?></td>
</tr>
<?php } ?>
</table>

