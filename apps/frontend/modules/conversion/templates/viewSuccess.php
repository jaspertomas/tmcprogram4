<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
	Product Conversion: <?php echo $conversion->getName()?>
</h1>
<?php slot('transaction_id', $conversion->getId()) ?>
<?php slot('transaction_type', "Conversion") ?>

<table>
  <tr>
    <td>Description:</td>
    <td><?php echo $conversion->getDescription() ?></td>
  </tr>
</table>

<?php echo link_to("Edit","conversion/edit?id=".$conversion->getId(),array("id"=>"conversion_edit")) ?> |
<?php echo link_to('Delete','conversion/delete?id='.$conversion->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
<?php //echo link_to("View Files","conversion/files?id=".$conversion->getId());?>

<?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>

<hr>
<b>Search product:</b> <input id=conversionproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=conversionclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@conversiondetail",array("id"=>"new_conversion_detail_form")); ?>
<input type=hidden name=conversiondetail[conversion_id] value=<?php echo $conversion->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td colspan=2><center>Type</center></td>
		<td>&nbsp;</td>
		<td>Product</td>
		<td>Qty</td>
	</tr>
	<tr>
		<td>
      <input type="radio" name=conversiondetail[type] id=conversiondetail_type value="from" checked=checked>From<br>
		</td>
		<td>
      <input type="radio" name=conversiondetail[type] id=conversiondetail_type value="to">To<br>
		</td>
		<td>&nbsp;</td>
		<td>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<td><input size="5" name="conversiondetail[qty]" value="1" id="conversiondetail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=conversion_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> ></form>
</td>
	</tr>

	<!--tr>
	  <td>Barcode: <?php //echo $detailform["barcode"]; ?></td>
	</tr-->
	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>

<?php  } ?>

<div id="conversionsearchresult"></div>

<hr>

<?php foreach(array("From"=>"from","To"=>"to") as $key=>$value){?>
<h2><?php echo $key?> Products:</h2>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Product</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php foreach($conversion->getConversiondetail() as $detail)if($detail->getType()=="$value"){?>
  <tr>
    <td>
      <?php echo form_tag("conversiondetail/setQty");?>
      <?php echo $detail->getQty() ?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=value size=3 name=value>
      </form>
    </td>
    <td>
      <?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?>
    </td>
    <?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
      <td>
        <?php //echo link_to("Edit","conversiondetail/edit?id=".$detail->getId()) ?>
      </td>
      <td>
        <?php echo link_to('Delete','conversiondetail/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
      </td>
      <td>
        <?php $alt=($detail->getType()=='from'?'To':'From');echo link_to("Set as ".$alt,"conversiondetail/setType?id=".$detail->getId()."&value=".strtolower($alt)) ?>
      </td>
    <?php }?>
  </tr>
  <?php }?>
  
</table>
<?php }?>



<script type="text/javascript">
function changeText(id)
{
var x=document.getElementById("mySelect");
x.value=id;
}
var manager_password="<?php 
	    $setting=Doctrine_Query::create()
	        ->from('Settings s')
	      	->where('name="manager_password"')
	      	->fetchOne();
      	if($setting!=null)echo $setting->getValue();
?>";
//on qty get focus, select its contents
$("#conversiondetail_qty").focus(function() { $(this).select(); } );
$("#conversionproductsearchinput").focus(function() { $(this).select(); } );
//set price textbox to read only
//$("#conversiondetail_price").prop('readonly', true);
//set product name
//$("#conversionproductsearchinput").prop('value', $("#product_name").val());
//set price to default price
$("#conversiondetail_price").prop('value', $("#product_price").val());
//select code if cashier
//else set focus on product search input
var is_cashier=<?php echo $sf_user->hasCredential(array('cashier'),false)?"true":"false"?>;
var is_admin=<?php echo $sf_user->hasCredential(array('admin'),false)?"true":"false"?>;
if(is_admin)
{
$("#conversionproductsearchinput").focus();
$("#conversionproductsearchinput").select(); 
}
else if(is_cashier)
{
$("#conversion_code").focus();
$("#conversion_code").select(); 
}
else
{
$("#conversionproductsearchinput").focus();
$("#conversionproductsearchinput").select(); 
}
//if no product id set, disable save button
if($("#conversiondetail_product_id").val()=='')	 		  
  $("#conversion_detail_submit").prop("disabled",true);

//------Conversion (not header) product search-----------
//$("#conversionproductsearchinput").keyup(function(){
//$("#conversionproductsearchinput").on('input propertychange paste', function() {
$("#conversionproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#conversion_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#conversionproductsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#conversionproductsearchinput").val();
      //if searchstring is all numbers except last letter, 
      //and length is 8
      //this is a barcode. 
      if(
        $.isNumeric(searchstring.slice(0, -1))
        &&
        searchstring.length>=8
        )
      {
        processBarcode(searchstring);
      }
      //else do ajax to product search, display result in conversionsearchresult
      else
      {
	      $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+searchstring+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	   		  $("#conversionsearchresult").html(result);
	      }});
      }
    }
    //else clear
    //else
 		//  $("#conversionsearchresult").html("");
});
$("#conversionclearsearch").click(function(){
 		  $("#conversionsearchresult").html("");
});
//---------New Conversion Detail Submit-----------------------------
//on submit new conversion detail form
$("#new_conversion_detail_form").submit(function(event){

  //this is when the cursor is on qty and the barcode reader is used
  //determine if qty is actually a barcode
  if(
    //if qty is too long to be a quantity, this is probably a barcode
    $("#conversiondetail_qty").val().length>=8 && 
    //but only if qty is enabled
    $("#conversiondetail_qty").attr("disabled")!="disabled")
  {
    processBarcode($("#conversiondetail_qty").val());
    return false;
  } 

  //if discount checkbox is checked, go ahead with submit
  if($("#chk_is_discounted").prop('checked'))return true;

  //determine minimum product selling price
  var price=$("#conversiondetail_price").prop('value');
  var minprice=$("#product_min_price").val();
  if(minprice==0)minprice=$("#product_price").val();
  //if price is less than min price, complain and don't submit
  if(parseFloat(price)<parseFloat(minprice))
  {
    alert("Minimum selling price is "+minprice+".\n\nPlease check Discounted check box to request for discount, and notify manager. ");
    return false;
  }
});
//------ BARCODE -----------------------------
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
function processBarcode(barcode)
{
        //
        if(barcode.length<8)
        {
          barcode=pad(barcode, 8);
        }

		$("#pleasewait").html("<font color=red><b>PLEASE WAIT</b></font>");
	      $.ajax
	      ({
	        url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/conversion/barcodeEntry?barcode="+barcode, 
	        dataType: "json",
	        success: function(result)
	        {
			$("#pleasewait").html("");
	          //if product found
	          if(result[0]!=0)
	          {
              //invoidetail input controls disabled=false
              $("#chk_is_discounted").removeAttr('disabled');
              $("#conversiondetail_description").removeAttr('disabled');
              $("#conversion_detail_submit").removeAttr('disabled');
              $("#conversiondetail_qty").removeAttr('disabled');
              $("#conversiondetail_price").removeAttr('disabled');
              $("#conversiondetail_with_commission").removeAttr('disabled');

	            //populate values
	            $("#conversionproductsearchinput").val("");
              $("#conversiondetail_qty").val(1);
              $("#conversiondetail_product_id").val(result[0]);//product_id
              $("#productname").html(result[1]);//product name
              $("#conversiondetail_price").val(result[2]);//maxsellprice
              $("#conversiondetail_with_commission").val(0);
              $("#chk_is_discounted").attr("checked",false);
              $("#conversiondetail_description").val("");
              
              $("#product_min_price").val(result[3]);//minsellprice
              $("#product_price").val(result[2]);//maxsellprice
              $("#product_name").val(result[1]);//product name
              $("#conversiondetail_barcode").val(barcode);
              
              //set focus to qty
              $("#conversiondetail_qty").focus();
	          }
	          else
	          {
	            alert("Product not found");
              $("#conversiondetail_qty").val("");
	          }
	        }
	      });

}
</script>

