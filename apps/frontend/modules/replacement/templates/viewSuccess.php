<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1><?php echo $replacement->getPrettyType()?> Replacement <?php echo $replacement->getCode(); ?></h1>
<?php slot('transaction_id', $replacement->getId()) ?>
<?php slot('transaction_type', "Replacement") ?>

<table>
  <tr valign=top>
    <td>
      <table>
          <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
          <?php echo form_tag_for($form,'replacement/adjust')?> <?php echo $form['id'] ?>
          <?php }?>	
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($replacement->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
            <?php echo $form['date'] ?>
            <?php }?>	
          </td>
        </tr>
<?php if($replacement->getType()==0){?>
        <tr valign=top>
          <td><?php echo ($replacement->getIsSales()?"Customer: ":"Supplier: ")?></td>
          <td><?php echo link_to($replacement->getClient(),($replacement->getIsSales()?"customer":"vendor")."/view?id=".$replacement->getClientId()); ?> </td>
        </tr>
<?php }else{ ?>
        <tr valign=top>
          <td>Supplier</td>
          <td><?php echo link_to($replacement->getClient(),"vendor/view?id=".$replacement->getClientId()); ?> </td>
        </tr>
<?php } ?>
        <tr valign=top>
          <td >Ref:</td>
          <td>
            <?php echo link_to($replacement->getReturns(),"returns/view?id=".$replacement->getReturnsId())?>
            <br>
            <?php if($replacement->getRefId())echo link_to($ref,strtolower($replacement->getType())."/view?id=".$replacement->getRefId())?>
          </td>
        </tr>
		</table>
    <td>
      <table>
        <tr>
          <td>Total</td>
          <td><?php echo $replacement->getTotal() ?></td>
        </tr>
		    <tr>
	        <td>Status</td>
	        <td>
          <font <?php 
            switch($replacement->getStatus())
            {
              case "Cancelled":echo "color=red";break;
              default: echo "color=black";break;
            }
            ?>>
            <?php 
            switch($replacement->getStatus())
            {
              case "Cancelled":echo $replacement->getStatus();break;
              default: break;
            }
            ?>
            </font>
          </td>
		    </tr>
        <tr>
          <td>Notes</td>
          <td><?php echo $replacement->getNotes() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <textarea rows="2" cols="10" name="replacement[notes]" id="replacement_notes"><?php echo $replacement->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>
              <input type="submit" value="Save"></form>
            <?php }?>	
          </td>
        </tr>
		  </table>
    </td>
  </tr>
</table>

            <?php echo link_to('Delete','replacement/delete?id='.$replacement->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
            <?php echo link_to("Print","replacement/viewPdf?id=".$replacement->getId());?> |
            <?php echo link_to("View Files","replacement/files?id=".$replacement->getId());?> |


<hr>

<h2>Items Replaced</h2>

<!-----replacement detail table------>

<?php if(!sfConfig::get('custom_is_discount_based')){
//--------------------------start is not discount based-----------------------
?>

<table border=1>
  <tr>
    <td>Qty</td>
    <td>Unit</td>
    <td width=50%>Description</td>
    <td>Unit Price</td>
    <td>Total</td>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td>Notes</td>
<?php } ?>
  </tr>
  <?php 
  	foreach($replacement->getReplacementDetail() as $detail){?>
  <tr>
    <td><?php echo $detail->getQty() ?></td>
    <td></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>

    <td align=right >
      <?php echo MyDecimal::format($detail->getPrice()) ?>
    </td>
    <td align=right ><?php echo MyDecimal::format($detail->getTotal()); ?></td>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td><?php echo $detail->getDescription() ?></td>
    <td align=right >
<?php echo link_to('Delete','replacement_detail/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
    </td>
  </tr>
  <?php }?>
  <?php }?>
  
  <tr align=right>
	  <td colspan=4>Total Return Value</td>
	  <td <?php echo MyDecimal::format($replacement->getTotal())?></td>
	  <td></td>
  </tr>
</table>



<?php 
//--------------------------end is not discount based-----------------------
}else{
//--------------------------start is_discount_based-------------------------
?>

<table border=1>
  <tr>
    <td>Qty</td>
    <td>Unit</td>
    <td width=50%>Description</td>
    <td>Unit Price</td>
    <td>Total</td>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td></td>
    <td></td>
    <td>Notes</td>
<?php } ?>
  </tr>
  <?php 
  	foreach($replacement->getReplacementDetail() as $detail){?>
  <tr>
    <td><?php echo $detail->getQty() ?></td>
    <td></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>

    <td align=right >
      <?php echo MyDecimal::format($detail->getPrice()) ?>
    </td>
    <td align=right ><?php echo MyDecimal::format($detail->getTotal()); ?></td>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'sales'), false)){?>
    <td><?php echo $detail->getDescription() ?></td>
  </tr>
  <?php }?>
  <?php }?>
  
  <tr align=right>
	  <td colspan=4>Total Return Value</td>
	  <td <?php echo MyDecimal::format($replacement->getTotal())?></td>
	  <td></td>
  </tr>
</table>


<?php }//-------------end is_discount_based-------------------- ?>

<hr>


<!-----invoice or PO details, choose which ones to return------>

<h2>Replace by Quantity</h2>

<?php echo form_tag("replacement/setQtys");?>
<input type=hidden id=id name=id value=<?php echo $replacement->getId()?>>
<table border=1>
  <tr>
    <td>Returned</td>
    <td>Replaced</td>
    <td>Remaining</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>Qty</td>
    <td>Qty</td>
    <td width=50%>Description</td>
    <td>Unit Price</td>
  </tr>
  <?php 
  	foreach($replacement->getReturns()->getReturnsDetail() as $rdetail){?>
  <tr>
    <td>
      <?php echo $rdetail->getQty() ?>
    </td>
    <td>
      <?php 
        $repdetail=null;
        $rqty="0.00";
        if(array_key_exists($rdetail->getId(), $repdetails->getRawValue()))
        {
          $repdetail=$repdetails[$rdetail->getId()];
          $rqty=$repdetail->getQty();
        }
        echo $rqty;
      ?>
      <input id=qtys[<?php echo $rdetail->getId()?>] size=3 name=qtys[<?php echo $rdetail->getId()?>] value=<?php echo $rqty?>>

    </td>
    <td align=right ><?php echo $rdetail->getQty()-$rdetail->getQtyReplaced(); ?></td>
    <td><?php echo link_to($rdetail->getProduct(),"product/view?id=".$rdetail->getProductId()) ?></td>
    <td align=right ><?php echo MyDecimal::format($rdetail->getPrice()) ?></td>
  <?php }?>
  
</table>
<input type=submit value="Save">
</form>
<hr>

<h2>Choose Product to Replace</h2>

<b>Search product:</b> <input id=replacementproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=replacementclearsearch> <span id=pleasewait></span>

<?php echo form_tag_for($detailform,"@replacement_detail",array("id"=>"new_replacement_detail_form")); ?>
<input type=hidden name=replacement_detail[replacement_id] value=<?php echo $replacement->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Product</td>
		<td>Qty</td>
		<td>Price</td>
		<td>Notes</td>
	</tr>

	<tr>
		<td>
		  <div id=productname>
			<?php if($detailform->getObject()->getProductId())echo $detailform->getObject()->getProduct(); else echo "No item selected";?>
			</div>
		</td>
		<input name="replacement_detail[product_id]" id="replacement_detail_product_id" type="hidden" ?>
		<td><input size="5" name="replacement_detail[qty]" value="1" id="replacement_detail_qty" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input size="8" name="replacement_detail[price]" value="0.00" id="replacement_detail_price" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input name="replacement_detail[description]" id="replacement_detail_description" type="text" <?php if(!$product_is_set)echo "disabled=true"?>></td>
		<td><input type=submit name=submit id=replacement_detail_submit value=Save  <?php if(!$product_is_set)echo "disabled=true"?> >
</td>
	</tr>

	<input type=hidden id="product_min_price" value="<?php echo $detailform->getObject()->getProduct()->getMinsellprice();?>">
	<input type=hidden id="product_price" value="<?php echo $detailform->getObject()->getProduct()->getMaxsellprice();?>">
	<input type=hidden id="product_name" value="<?php echo $detailform->getObject()->getProduct()->getName();?>">
</table>
</form>

<div id="replacementsearchresult"></div>

<hr>


<hr>
<br>
<br>
<br>
<br>

<?php 
//--------------------------end is invoice or purchase type-----------------------
?>
<hr>



<script type="text/javascript">

function changeText(id)
{
var x=document.getElementById("mySelect");
x.value=id;
}
var manager_password="<?php 
	    $setting=Doctrine_Query::create()
	        ->from('Settings s')
	      	->where('name="manager_password"')
	      	->fetchOne();
      	if($setting!=null)echo $setting->getValue();
?>";
//on qty get focus, select its contents
$("#replacement_detail_qty").focus(function() { $(this).select(); } );
$("#replacementproductsearchinput").focus(function() { $(this).select(); } );
//set price textbox to read only
//$("#replacement_detail_price").prop('readonly', true);
//set product name
//$("#replacementproductsearchinput").prop('value', $("#product_name").val());
//set price to default price
$("#replacement_detail_price").prop('value', $("#product_price").val());
//select invno if cashier
//else set focus on product search input
var is_cashier=<?php echo $sf_user->hasCredential(array('cashier'),false)?"true":"false"?>;
var is_admin=<?php echo $sf_user->hasCredential(array('admin'),false)?"true":"false"?>;
if(is_admin)
{
$("#replacementproductsearchinput").focus();
$("#replacementproductsearchinput").select(); 
}
else if(is_cashier)
{
$("#replacement_invno").focus();
$("#replacement_invno").select(); 
}
else
{
$("#replacementproductsearchinput").focus();
$("#replacementproductsearchinput").select(); 
}
//if no product id set, disable save button
if($("#replacement_detail_product_id").val()=='')	 		  
  $("#replacement_detail_submit").prop("disabled",true);
//------replacement Discount-------------------
//on page ready, var discounted is false
//var discounted=false;
//set checkbox to false as default
//$("#chk_is_discounted").prop('checked',false);
//hide all password entry boxes
$(".password_tr").attr('hidden',true);

//------replacement (not header) product search-----------
//$("#replacementproductsearchinput").keyup(function(){
//$("#replacementproductsearchinput").on('input propertychange paste', function() {
$("#replacementproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#replacement_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#replacementproductsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#replacementproductsearchinput").val();
      //if searchstring is all numbers except last letter, 
      //and length is 8
      //this is a barcode. 
      if(
        $.isNumeric(searchstring.slice(0, -1))
        &&
        searchstring.length>=8
        )
      {
        processBarcode(searchstring);
      }
      //else do ajax to product search, display result in replacementsearchresult
      else
      {
	      $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/index?searchstring="+searchstring+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	   		  $("#replacementsearchresult").html(result);
	      }});
      }
    }
    //else clear
    //else
 		//  $("#replacementsearchresult").html("");
});
$("#replacementclearsearch").click(function(){
 		  $("#replacementsearchresult").html("");
});
//------replacement Edit-------------------
$('#replacement_edit').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#replacement_edit_password_tr").attr('hidden',false);
});
$('#replacement_edit_password_button').click(function(event) {
	//get entered password value
    var pass=$("#replacement_edit_password_input").val();
    if (pass==manager_password){
	    window.location = $('#replacement_edit').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//----replacement Cancel-------------------------------
/*
$('#replacement_cancel').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#replacement_cancel_password_tr").attr('hidden',false);
});
*/
$('#replacement_cancel_password_button').click(function(event) {
	//get entered password value
    var pass=$("#replacement_cancel_password_input").val();
    if (pass==manager_password){
	    window.location = $('#replacement_cancel').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//----replacement Undo Cancel-------------------------------
/*
$('#replacement_undocancel').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#replacement_undocancel_password_tr").attr('hidden',false);
});
$('#replacement_undocancel_password_button').click(function(event) {
	//get entered password value
    var pass=$("#replacement_undocancel_password_input").val();
    if (pass==manager_password){
	    window.location = $('#replacement_undocancel').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
*/
//----replacement Undo Close-------------------------------
$('#replacement_undoclose').click(function(event) {
    event.preventDefault();
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide this password entry box
    $("#replacement_undoclose_password_tr").attr('hidden',false);
});
$('#replacement_undoclose_password_button').click(function(event) {
	//get entered password value
    var pass=$("#replacement_undoclose_password_input").val();
    if (pass==manager_password){
	    window.location = $('#replacement_undoclose').attr('href');
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//--------replacement Detail Edit---------------------------
var replacement_detail_edit_href="";
var detail_id="";
$('.replacement_detail_edit').click(function(event) {
	//prevent form from sending
    event.preventDefault();
    //extract detail->getId() value
    detail_id=$(this).attr('detail_id');
    //save href value for later use
    replacement_detail_edit_href=$(this).attr('href');
    //hide all password entry boxes
    $(".password_tr").attr('hidden',true);
    //unhide password entry textbox
    $("#replacement_detail_edit_password_tr_"+detail_id).attr('hidden',false);
});
$('.replacement_detail_edit_password_button').click(function(event) {
	//get entered password value
    var pass=$("#replacement_detail_edit_password_input_"+detail_id).val();
    if (pass==manager_password){
	    window.location = replacement_detail_edit_href;
    }
    else
    {
        	alert("WRONG PASSWORD");
    }
});
//---------New replacement Detail Submit-----------------------------
//on submit new replacement detail form
$("#new_replacement_detail_form").submit(function(event){

  //this is when the cursor is on qty and the barcode reader is used
  //determine if qty is actually a barcode
  if(
    //if qty is too long to be a quantity, this is probably a barcode
    $("#replacement_detail_qty").val().length>=8 && 
    //but only if qty is enabled
    $("#replacement_detail_qty").attr("disabled")!="disabled")
  {
    processBarcode($("#replacement_detail_qty").val());
    return false;
  } 

  //if discount checkbox is checked, go ahead with submit
  if($("#chk_is_discounted").prop('checked'))return true;

  //determine minimum product selling price
  var price=$("#replacement_detail_price").prop('value');
  var minprice=$("#product_min_price").val();
  //if(minprice==0)minprice=$("#product_price").val();
  //if price is less than min price, complain and don't submit
  if(parseFloat(price)<parseFloat(minprice))
  {
    alert("Minimum selling price is "+minprice+".\n\nPlease check Discounted check box to request for discount, and notify manager. ");
    return false;
  }
});
//------ BARCODE -----------------------------
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
function processBarcode(barcode)
{
        //
        if(barcode.length<8)
        {
          barcode=pad(barcode, 8);
        }

		$("#pleasewait").html("<font color=red><b>PLEASE WAIT</b></font>");
	      $.ajax
	      ({
	        url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/replacement/barcodeEntry?barcode="+barcode, 
	        dataType: "json",
	        success: function(result)
	        {
			$("#pleasewait").html("");
	          //if product found
	          if(result[0]!=0)
	          {
              //invoidetail input controls disabled=false
              //$("#chk_is_discounted").removeAttr('disabled');
              $("#replacement_detail_description").removeAttr('disabled');
              $("#replacement_detail_submit").removeAttr('disabled');
              $("#replacement_detail_qty").removeAttr('disabled');
              $("#replacement_detail_price").removeAttr('disabled');
              //$("#replacement_detail_with_commission").removeAttr('disabled');
              $("#replacement_detail_discrate").removeAttr('disabled');

	            //populate values
	            $("#replacementproductsearchinput").val("");
              $("#replacement_detail_qty").val(1);
              $("#replacement_detail_product_id").val(result[0]);//product_id
              $("#productname").html(result[1]);//product name
              $("#replacement_detail_price").val(result[2]);//maxsellprice
              //$("#replacement_detail_with_commission").val(0);
              //$("#chk_is_discounted").attr("checked",false);
              $("#replacement_detail_description").val("");
              
              $("#product_min_price").val(result[3]);//minsellprice
              $("#product_price").val(result[2]);//maxsellprice
              $("#product_name").val(result[1]);//product name
              $("#replacement_detail_barcode").val(barcode);
              
              //set focus to qty
              $("#replacement_detail_qty").focus();
	          }
	          else
	          {
	            alert("Product not found");
              $("#replacement_detail_qty").val("");
	          }
	        }
	      });

}

//this is called when user clicks "Choose" button in product search form
function populateSubform(product_id,product_name,price,min_price) {
  $("#product_min_price").val(min_price);
  $("#product_price").val(price);
  $("#product_name").html(product_name);
  
  $("#replacement_detail_price").val(price);
  //$("#replacement_detail_with_commission").val(0);
  $("#productname").html(product_name);
  $("#replacement_detail_product_id").val(product_id);
  $("#replacementsearchresult").html("");

  $("#chk_is_discounted").removeAttr("disabled");
  $("#replacement_detail_description").removeAttr("disabled");
  $("#replacement_detail_submit").removeAttr("disabled");
  $("#replacement_detail_qty").removeAttr("disabled");
  $("#replacement_detail_price").removeAttr("disabled");
  //$("#replacement_detail_with_commission").removeAttr("disabled");
  $("#replacement_detail_discrate").removeAttr('disabled');

  $("#replacement_detail_qty").focus();
}</script>
