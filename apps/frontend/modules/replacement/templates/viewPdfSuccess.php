<?php

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "LETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Replacement_'.$replacement->getCode());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->startPageGroup();
$pdf->AddPage();




// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
// $pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 100, '', 'PNG', '', '', false, 300, 'C', false, false, 0, false, false, false);
// $pdf->write(5,"\n\n\n\n");
$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->write(0,sfConfig::get('custom_company_header_text'),'',false,'C',true,0,false,false,0,0);
//$pdf->write(0,sfConfig::get('custom_company_address'),'',false,'C',true,0,false,false,0,0);
$pdf->write(0,sfConfig::get('custom_company_email'),'',false,'C',true,0,false,false,0,0);
$pdf->write(5,"\n");

$pdf->SetFont('helvetica', '', 15, '', true);
$pdf->write(0,$replacement->getType()." Replacement ".$replacement->getCode(),'',false,'C',true,0,false,false,0,0);
$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->write(5,"\n");



$tbl = "";

//-------------------
$client=$replacement->getClient();
$content=array(
  ($replacement->getIsSales()?"Customer: ":"Supplier: ").$client->getName(),
  $client->getPhone1(),
  $client->getEmail(),
  MyDateTime::frommysql($replacement->getDate())->toshortdate(),
  $replacement->getRef(),
  $replacement->getNotes(),
  );
  
  

$tbl .= <<<EOD
<table>
 <tr>
  <td align="left">$content[0]</td>
  <td align="right">Date: $content[3]</td>
 </tr>
 <tr>
  <td align="left">Contact Details: $content[1]; $content[2]</td>
  <td align="right"></td>
 </tr>
 <tr>
  <td align="left">References: $content[4]</td>
  <td align="right">Notes: $content[5]</td>
 </tr>
</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');




$pdf->SetFont('dejavusans', '', 10, '', true);
$widths=array(10,150);
$scale=3.543;

foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl = <<<EOD
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>Qty</b></td>
  <td width="$widths[1]" align="center"><b>Unit</b></td>
 </tr>
</thead>
EOD;
//body
foreach($replacement->getReplacementDetail() as $detail){
  $output=array(
    $detail->getQty(),
    $detail->getProduct(),
  );

$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$output[0]</td>
<td width="$widths[1]" align="center">$output[1]</td>
</tr>
EOD;
}
$tbl .= <<<EOD
</table>
EOD;

// ---------------------------------------------------------


//signature line
$content=array(
  $replacement->getIsSales()?"Customer:":"Supplier Representative:"
  );
$tbl .= <<<EOD
<br>
<br>This serves as your Delivery Receipt
<br>
<table border="1">
 <tr>
  <td align="center">Prepared By:</td>
  <td align="center">Warehouse Man:</td>
  <td align="center">$content[0]</td>
 </tr>
 <tr valign="bottom">
  <td align="center"><br><br><br><br></td>
  <td align="center"></td>
  <td align="center"></td>
 </tr>
</table>
EOD;

// ---------------------------------------------------------

$pdf->writeHTML($tbl, true, false, false, false, '');

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('DIR-'.MyDateTime::frommysql($form->getObject()->getDate())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

