<?php

require_once dirname(__FILE__).'/../lib/replacementGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/replacementGeneratorHelper.class.php';

/**
 * replacement actions.
 *
 * @package    sf_sandbox
 * @subpackage replacement
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class replacementActions extends autoReplacementActions
{
  public function executeFilter(sfWebRequest $request)
  {
    $this->setPage(1);

    $this->filters = $this->configuration->getFilterForm($this->getFilters());

    $requestparams=$request->getParameter($this->filters->getName());
    if($request->hasParameter("client_class"))
      $requestparams["client_class"]=$request->getParameter("client_class");

    $this->filters->bind($requestparams);
    if ($this->filters->isValid())
    {
      $this->setFilters($this->filters->getValues());

      $this->redirect('@replacement');
    }

    $this->pager = $this->getPager();
    $this->sort = $this->getSort();

    $this->setTemplate('index');
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->replacement = new Replacement();
    
    $this->replacement->setReturnsId($request->getParameter("returns_id"));
    $returns=$this->replacement->getReturns();

    if($request->hasParameter("ref_id"))
    {
      $this->replacement->setRefId($request->getParameter("ref_id"));
      $this->replacement->setRefClass($request->getParameter("ref_class"));
      $this->replacement->setType($request->getParameter("ref_class"));
      
      if($request->getParameter("ref_class")=="Invoice")
      {
        $this->replacement->setCode(ReplacementTable::genCode($returns));
        $this->ref=$this->replacement->getRef();
        $this->replacement->setClientId($this->ref->getCustomerId());
        $this->replacement->setClientClass("Customer");
      }
      elseif($request->getParameter("ref_class")=="Purchase")
      {
        $this->replacement->setCode(ReplacementTable::genCode($returns));
        $this->ref=$this->replacement->getRef();
        $this->replacement->setClientId($this->ref->getVendorId());
        $this->replacement->setClientClass("Vendor");
      }
    }
    elseif($request->hasParameter("client_id"))
    {
      if($request->getParameter("client_class")=="Customer")
        $this->replacement->setCode(ReplacementTable::genCode($returns));
      elseif($request->getParameter("client_class")=="Vendor")
        $this->replacement->setCode(ReplacementTable::genCode($returns));
      $this->replacement->setClientId($request->getParameter("client_id"));
      $this->replacement->setClientClass($request->getParameter("client_class"));
      $this->replacement->setType($request->getParameter("client_class"));
    }
    
    $this->replacement->setDate(MyDate::today());

    $this->form=new ReplacementForm($this->replacement);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $replacement = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $replacement)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect('replacement/view?id='.$replacement->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

  public function executeView(sfWebRequest $request)
  {
    //fetch return by id
    $this->forward404Unless(
      $this->replacement=Doctrine_Query::create()
        ->from('Replacement r, r.ReplacementDetail d')
      	->where('r.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    $this->form = $this->configuration->getForm($this->replacement);

    $this->ref=$this->replacement->getRef();

    //arrange repdetails into an array with invoicedetail as key
    //to be used as:
    //if $repdetails[invoicedetail_id] exists, do this
    //else do that
    $this->repdetails=array();
    foreach($this->replacement->getReplacementDetail() as $repdetail)
    {
      $this->repdetails[$repdetail->getReturnsDetailId()]=$repdetail;
    }
    
    $detail=new ReplacementDetail();
    $detail->setQty(1);
    $this->product_is_set=false;
    $this->product=null;
    $this->detailform = new ReplacementDetailForm($detail);
    $this->detail = $detail;
    $this->searchstring=$this->request->getParameter('searchstring');
  }
  public function executeSetQtys(sfWebRequest $request)
  {
    //fetch return by id
    $this->forward404Unless(
      $this->replacement=Doctrine_Query::create()
        ->from('Replacement r, r.ReplacementDetail d')
      	->where('r.id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    //arrange repdetails into an array with returns_detail as key
    //to be used as:
    //if $repdetails[returns_detail_id] exists, do this
    //else do that
    $this->repdetails=array();
    foreach($this->replacement->getReplacementDetail() as $repdetail)
    {
      $this->repdetails[$repdetail->getReturnsDetailId()]=$repdetail;
    }

    foreach($request->getParameter('qtys') as $returns_detail_id => $qty)
    {
      $qty=intval($qty);

      //if repdetail exists
      if(array_key_exists($returns_detail_id, $this->repdetails))
      {
        $repdetail=$this->repdetails[$returns_detail_id];
        $ref=$repdetail->getReturnsDetail();

        //if qty is 0, adjust ref qty and delete rdetail
        if($qty==0)
        {
          //postdelete already takes care of 
          //adjusting qty returned
          //and deleting stock entry
          //$ref->setQtyReplaced($ref->getQtyReplaced()-$repdetail->getQty());
          //$ref->save();
          //$repdetail->deleteStockEntry();
          $repdetail->delete(); 
          continue;
        }
      }
      //else if repdetail does not exist, create it
      else 
      {
        //if qty is 0, don't do anything
        if($qty==0)continue;
      
        $repdetail=new ReplacementDetail();
        $repdetail->setReturnsDetailId($returns_detail_id);
        $ref=$repdetail->getReturnsDetail();
        $repdetail->setRefClass($ref->getRefClass());
        $repdetail->setRefId($ref->getRefId());
        $repdetail->setProductId($ref->getProductId());
        $repdetail->setReplacementId($this->replacement->getId());
        $repdetail->setPrice($ref->getPrice());
      }
        
      //validate qty will not push ref qtyreplaced over ref qty
      if($ref->getQtyReplaced()-$repdetail->getQty()+$qty>$ref->getQty())
      {
        $error="Cannot replace ".$qty." units of ".$ref->getProduct().", only ".($ref->getQty()-$ref->getQtyReplaced())." units remaining";
        $this->getUser()->setFlash('error', $error);
        return $this->redirect('replacement/view?id='.$this->replacement->getId());
      }

      //adjust ref qty
      $ref->setQtyReplaced($ref->getQtyReplaced()-$repdetail->getQty()+$qty);
      $ref->save();

      $repdetail->setQty($qty);
      $repdetail->setTotal($ref->getPrice()*$repdetail->getQty());
      $repdetail->save();

      $repdetail->updateStockEntry();
    }
    return $this->redirect("replacement/view?id=".$this->replacement->getId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $replacement=$this->getRoute()->getObject();
    if ($replacement->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    //$this->redirect(strtolower($replacement->getRefClass())."/view?id=".$replacement->getRefId());
    $this->redirect("returns/view?id=".$replacement->getReturnsId());
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('replacement');
    $id=$requestparams['id'];

    $this->forward404Unless(
    $this->replacement=Doctrine_Query::create()
    ->from('Replacement i')
    ->where('i.id = '.$id)
    ->fetchOne()
    , sprintf('Replacement with id (%s) not found.', $request->getParameter('id')));

    $this->replacement->setNotes($requestparams["notes"]);

    //set date and adjust stock entry date if necessary
    $this->replacement->setDateAndUpdateStockEntry($requestparams["date"]["year"]."-".$requestparams["date"]["month"]."-".$requestparams["date"]["day"]);

    $this->replacement->save();

    $this->redirect($request->getReferer());
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);

    //cannot print empty dr
    if(count($this->replacement->getReplacementDetail())==0)
    {
        $message="Cannot print empty DR";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
    }

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
}
