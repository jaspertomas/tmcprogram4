<?php

require_once dirname(__FILE__).'/../lib/out_paymentGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/out_paymentGeneratorHelper.class.php';

/**
 * out_payment actions.
 *
 * @package    sf_sandbox
 * @subpackage out_payment
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class out_paymentActions extends autoOut_paymentActions
{
/*
  public function executeNew(sfWebRequest $request)
  {
    $this->out_payment = new OutPayment();

    //set Default Data
    $this->out_payment->setParentClass($request->getParameter("parent_class"));
    $this->out_payment->setParentId($request->getParameter("parent_id"));

    $parent = $this->out_payment->getParent();

    //validate parent is not cancelled, do nothing
    if($parent->isCancelled())
    {
      $this->redirect("home/error?msg='Cannot create out_payment. This transaction has been cancelled.'");
    }
    
    //validate parent is not fully paid
    if($this->out_payment->getParentClass()=="CounterReceipt")
    {
    		$counter_receipt=$this->out_payment->getParent();
    		if($counter_receipt->getStatus()=="Paid")
    		{
      		//new invno not unique: error msg and quit
            $message="CounterReceipt is fully paid";
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
    		}
    }

    $this->out_payment->setParentName($parent->getName());
    $this->out_payment->setType($request->getParameter("type"));
    $this->out_payment->setDate(MyDate::today());

    if(
      $request->getParameter("type")=="stockin" or
      $request->getParameter("type")=="stockout" or
      $request->getParameter("type")=="stocktrans"
    )
    {
      OutPaymentTable::generate($this->out_payment);
      $this->redirect($request->getReferer());
    }

    
    $this->form = $this->configuration->getForm($this->out_payment);
  }
  */
  //modify processForm such that it posts / updates account entries
  //Note: this implementation focuses on counter receipt
  //because voucher outpayments are auto generated and auto deleted
  //in voucher/processform and never go through here
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $isnew=$form->getObject()->isNew();
    $requestparams=$request->getParameter($form->getName());
    
    //this should not be called for cheque collect new
    //that is the job for out_check`/new
    if($isnew and $requestparams["type"]=="ChequeCollect"){echo "Access Denied"; die();}
    
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        //for chequecollect edit
        //try to delete old out_payment and create a new one using new amount
        $out_payment=null;
        //if($requestparams["type"]=="ChequeCollect")
        {
          $oldout_payment=Fetcher::fetchOne("OutPayment",array("id"=>$requestparams["id"]));
          $check=$oldout_payment->getOutCheck();
          $check->removeOutPayment($oldout_payment);

          $counter_receipt=$oldout_payment->getParent();
          $amount=$requestparams["amount"];
          $date=$requestparams["date"]["year"]."/".$requestparams["date"]["month"]."/".$requestparams["date"]["day"];
          $notes=$requestparams["notes"];

          $oldout_payment->delete();
          
          //create a new one
          $out_payment=$check->genCounterReceiptOutPayment($counter_receipt,$amount,$date,$notes);
        }
/*
        else
        {
          $out_payment = $form->save();
          $out_payment->updateAccountentries();
          $out_payment->updateParent();
        }
*/        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $out_payment)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@out_payment_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        if($out_payment->getParentClass()=="CounterReceipt")
          $this->redirect('counter_receipt/payments?id='.$out_payment->getParentId());
        else if($out_payment->getParentClass()=="Voucher")
          $this->redirect('voucher/payments?id='.$out_payment->getParentId());
        //
        else
          $this->redirect(array('sf_route' => 'out_payment_edit', 'sf_subject' => $out_payment));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $out_payment=$this->getRoute()->getObject();
    $parent=$out_payment->getParent();
    
    $check=$out_payment->getOutCheck();
    if($check!=null)
    {
      //unapply this out_payment from the incoming check
      $check->removeOutPayment($out_payment);
    }
    
    if ($out_payment->delete())
    {
      $parent->calc();
      $parent->save();
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect($request->getReferer());
  }
  public function executeEdit(sfWebRequest $request)
  {
    $this->out_payment = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->out_payment);
  }
}
