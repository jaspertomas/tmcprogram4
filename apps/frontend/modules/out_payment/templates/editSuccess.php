<?php use_helper('I18N', 'Date') ?>
<?php include_partial('out_payment/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit Ougoing Payment', array(), 'messages') ?></h1>

  <?php include_partial('out_payment/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('out_payment/form_header', array('out_payment' => $out_payment, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('out_payment/form', array('check' => $check, 'out_payment' => $out_payment, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('out_payment/form_footer', array('out_payment' => $out_payment, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
