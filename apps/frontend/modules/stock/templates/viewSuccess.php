<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>Inventory: <?php echo '"'.link_to($product,"product/view?id=".$stock->getProductId()).'"'." in warehouse ".$stock->getWarehouse()?></h1>

<table border=1>
  <tr>
    <td>Warehouse</td>
    <td>Qty</td>
    <td>View</td>
  </tr>
  <?php foreach(WarehouseTable::fetchAll() as $warehouse)
    {
    $astock=StockTable::fetch($warehouse->getId(), $product->getId());
  ?>
  <tr>
    <td><?php echo $warehouse->getName() ?></td>
    <td><?php echo $astock->getCurrentQty() ?></td>
    <td><?php echo link_to("View","stock/view?id=".$astock->getId()) ?></td>
  </tr>
  <?php }?>
</table>

<br>
<br>

<table>
  <tr>
    <td>Product ID: </td>
    <td><?php echo $stock->getProductId();?></td>
  </tr>
  <tr>
    <td>Current Quantity: </td>
    <td><?php echo $stock->getCurrentqty();?></td>
  </tr>
  <tr>
    <td><?php echo link_to("Go to Invoice Purchase Matcher","stock/soldTo?id=".$stock->getId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("Go to Product Type Inventory Report","producttype/dir?id=".$product->getProducttypeId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("Edit Product","product/edit?id=".$stock->getProductId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("Print Barcodes","product/barcode?id=".$stock->getProductId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("Print Barcodes with Thermal Printer Small","product/barcodethermalpdf?product_id=".$stock->getProductId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("Print Barcodes with Thermal Printer Large","product/barcodethermallargepdf?product_id=".$stock->getProductId()) ?></td>
  </tr>
  <tr>
  <!--
  <tr>
    <td><?php echo link_to("View all warehouses","product/inventory?id=".$stock->getProductId()) ?></td>
  </tr>
  -->
<?php if($sf_user->hasCredential(array('admin'), false)){?>
  <tr>
    <td><?php echo link_to("Receive All Purchases","stock/receiveAllPurchases?id=".$stock->getId()) ?></td>
  </tr>
<?php } ?>
  <tr>
    <td><?php echo link_to("Recalculate","stock/calc?id=".$stock->getId()) ?></td>
  </tr>
  <tr>
    <td>
      Is Updated: <?php echo $stock->getIsUpdated()?"Yes":"No" ?> (<?php echo link_to("Set to ". ($stock->getIsUpdated()?"No":"Yes"),"stock/toggleIsUpdated?id=".$stock->getId())?>)

      <?php //echo link_to("Calculate","stock/calc?id=".$stock->getId()) ?>
    </td>
  </tr>
  <tr>
    <td><?php echo link_to("View Transactions By Page","product/transactionsByPage?id=".$stock->getProductId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("View Transactions By Date","product/transactions?id=".$stock->getProductId()) ?></td>
  </tr>
</table>

<?php //echo link_to("Add Detail","stockentry/new?stock_id=".$stock->getId()) ?>


<?php if($sf_user->hasCredential(array('admin','encoder'), false)){?>

<!--adjustments form-->
<?php /*
<b>New Adjustment</b>
<?php $stockentry=new Stockentry();$stockentry->setDate(MyDate::today());$form=new StockentryForm($stockentry); ?>
<?php echo form_tag_for($form,"@stockentry"); ?>
<input type=hidden name=stockentry[stock_id] value=<?php echo $stock->getId()?>  >
    <?php echo $form->renderHiddenFields(false) ?>
<table>
	<tr>
		<td>Date</td>
		<td>In / -Out Qty</td>
		<td>Notes</td>
	</tr>
	<tr>
		<td><?php echo $form["date"]; ?></td>
		<td><input name="stockentry[qty]" id="stockentry_qty" type="text" size=10></td>
		<td><?php echo $form["description"]; ?></td>
		<td><input type=submit name=submit value=Save  ></td>
	</tr>
</table>
</form>
*/?>

<hr>
<b>Images</b>
<?php foreach($product->getImages() as $image){?>  
  <br><a href="<?php echo $image->getUrl()?>" target="_blank">View Image <?php echo $image->getId()?></a>
<?php } ?>
<hr>

<b>New Inventory Record</b>
<!--inventory records form-->
<?php 
  $stockentry=new Stockentry();
  $stockentry->setDatetime(MyDateTime::now()->todatetime());
  //uncomment this to fix the date to something specific
  // $stockentry->setDate("2020-05-20");
  $form=new StockentryForm($stockentry); 
?>
<?php echo form_tag_for($form,"@stockentry"); ?>
<input type=hidden name=redirect_to value="view"  >
<input type=hidden name=stockentry[stock_id] value=<?php echo $stock->getId()?>  >
    <?php echo $form->renderHiddenFields(false) ?>
		<input name="stockentry[qty]" id="stockentry_qty" type="hidden" value="0">
		<input name="stockentry[type]" id="stockentry_type" type="hidden" value="Report">
<table>
	<tr>
		<td>Date/Time</td>
		<td>Actual Qty</td>
		<td>Notes</td>
	</tr>
	<tr>
		<td><?php echo $form["datetime"]; ?></td>
		<td><input name="stockentry[qty_reported]" id="stockentry_qty_reported" type="text" size=10></td>
		<td><?php echo $form["description"]; ?></td>
		<td><input type=submit name=submit value=Save  ></td>
	</tr>
</table>
</form>


<?php } ?>    

<br>

<b>Page <?php echo $page;?></b>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."&page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."&page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo "<b>".$apage."</b> ";
  else echo link_to("$apage",$pagepath."&page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."&page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."&page=".$pagecount))." ";

?>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Date Received</td>
    <td>Stock In</td>
    <td>Stock Out</td>
    <td>Balance</td>
    <td>Ref / Actual Qty</td>
    <td>Price</td>
    <td>Type</td>
    <td>Customer / Supplier ; Notes</td>
    <td>Salesman</td>
    <td>Created By</td>
    <td></td>
  </tr>
  <?php 
    foreach($stockentries as $se){
        $is_conversion=false;
        $ref=$se->getRef();
        $parent=null;
        $parentref=null;
        $profitdetails=array();
      //if no in and no out, do not show
      //if($se->getQty()==0)continue;

      //ignore unreleased / unreceived drs
      if($se->getRefClass()=="InvoiceDrDetail")
      {
        //dont show if qty is 0
        //unless  first
        $refref=$ref->getInvoiceDr();
        if($se->getQty()==0 and !$refref->isFirstDr())continue;
      }
      if($se->getRefClass()=="PurchaseDrDetail")
      {
        $refref=$ref->getPurchaseDr();
        if($se->getQty()==0 and !$refref->isFirstDr())continue;
      }
  ?>
  <tr>

  <td>
      <?php 
        switch($se->getRefClass())
        {
          case "Invoicedetail":
            if($ref)
            {
              $invoice=$ref->getInvoice();
              echo MyDateTime::frommysql($invoice->getDate())->toshortdate();
            }
            break;
          case "Purchasedetail":
            if($ref)
            {
              $purchase=$ref->getPurchase();
              echo MyDateTime::frommysql($purchase->getDate())->toshortdate();
            }
            break;
          default: 
          echo MyDateTime::fromdatetime($se->getDatetime())->toshortdate();
        }
      ?>
    </td>
    <td>
      <?php 
        switch($se->getRefClass())
        {
          case "Purchasedetail":
            $purchase=$ref->getPurchase();
            if($purchase->isCancelled())echo "<font color=red>Cancelled</font>";
            break;
          case "PurchaseDrDetail":
            $purchase=$ref->getPurchase();
            if($purchase->isCancelled())echo "<font color=red>Cancelled</font>";
            elseif(!$ref->getPurchaseDr()->getIsReceived())echo "Pending";
            else
            echo MyDateTime::fromdatetime($se->getDatetime())->toshortdate();
            break;
          case "Invoicedetail":
            $invoice=$ref->getInvoice();
            if($invoice->isCancelled())echo "<font color=red>Cancelled</font>";
            break;
          case "InvoiceDrDetail":
            $invoice=$ref->getInvoice();
            if($invoice->isCancelled())echo "<font color=red>Cancelled</font>";
            elseif(!$ref->getInvoiceDr()->getIsReleased())echo "Pending";
            else
            echo MyDateTime::fromdatetime($se->getDatetime())->toshortdate();
            break;
          case "TransferDrDetail":
            if(!$ref->getTransferDr()->getIsTransferred())echo "Pending";
            else
            echo MyDateTime::frommysql($ref->getTransferDr()->getDateTime())->toshortdate();
            break;
          case "JobOrderDrDetail":
            $job_order=$ref->getJobOrder();
            if($job_order->isCancelled())echo "<font color=red>Cancelled</font>";
            elseif(!$ref->getJobOrderDr()->getIsProduced())echo "Pending";
            else
            echo MyDateTime::fromdatetime($se->getDatetime())->toshortdate();
            break;
        }
      ?>
    </td>
    <td align=right>
      <?php 
      if($se->getQty()>0)echo MyDecimal::format($se->getQty()); 
      
      if($se->getRefClass()=="Purchasedetail")
      {
        if($purchase->getReceivedStatus()!="Fully Received"){?>
          <?php echo form_tag('purchase/receiveAll', array('style'=>'display:inline','onsubmit'=>"return confirm('Receive all items: Are you sure?');"))?>
          <input type=hidden id=id name=id value=<?php echo $purchase->getId()?>>
          <?php if($sf_user->hasCredential(array('admin'), false)){?><input type=submit name=submit id=submit value="QR"><?php } ?>
          </form>
          <?php } ?>
      <?php } ?>
    </td>
    <td align=right><?php if($se->getQty()<0)echo MyDecimal::format($se->getQty()*-1) ?></td>
    <td align=right>
      <?php 
        if($se->getType()=="Report")
          echo $se->getQtyReported();
        else
          if($se->getQty()!=0) echo $se->getBalance();
      ?>
    </td>
    <td>
      <?php 
        switch($se->getRefClass())
        {
          case "Invoicedetail":
            if($ref)
            {
              $invoice=$ref->getInvoice();
              echo link_to($invoice,"invoice/view?id=".$ref->getInvoiceId());
              //echo '<button onClick={copy("'.$invoice->getInvno().'")}>C</button>';
            }
            break;
          case "Purchasedetail":
            if($ref)
            {
              $purchase=$ref->getPurchase();
              echo link_to($purchase,"purchase/view?id=".$ref->getPurchaseId());
              // echo '<button onClick={copy("'.$purchase->getPono().'")}>C</button>';
            }
            break;
          case "Deliverydetail":
            $parent=$ref->getParent(); 
            switch($parent->getClass())
            {
              case "Delivery": 
                echo link_to($parent,strtolower($parent->getClass())."/view?id=".$parent->getId());
                echo "<br>";
                $parentref= $parent->getRef(); 
                if($parentref)echo link_to($parentref,strtolower($parent->getRefClass())."/view?id=".$parentref->getId());
                break;
              case "Invoice": 
                $is_conversion=true;
                echo link_to($parent,"invoice/view?id=".$parent->getId());
                break;
              case "Purchase": 
                $is_conversion=true;
                echo link_to($parent,"purchase/view?id=".$parent->getId());
                break;
            }
            break;
          case "ReturnsDetail":
            echo link_to($ref->getReturns(),"returns/view?id=".$ref->getReturnsId());
            break;
          case "ReplacementDetail":
            echo link_to($ref->getReplacement(),"replacement/view?id=".$ref->getReplacementId());
            break;
          case "InvoiceDrDetail":
            echo link_to($ref->getInvoice(),"invoice/view?id=".$ref->getInvoiceId());
            break;
          case "PurchaseDrDetail":
            echo link_to($ref->getPurchase(),"purchase/view?id=".$ref->getPurchaseId());
            break;
          case "TransferDrDetail":
            echo link_to($ref->getTransfer(),"transfer/view?id=".$ref->getTransferId());
            break;
          case "JobOrderDrDetail":
            echo link_to($ref->getJobOrder(),"job_order/view?id=".$ref->getJobOrderId());
            break;
          default:
            if($se->getType()=="Report")
              echo "Prev Balance ".$se->getBalance();
            break;
        }
      ?>
    </td>
    <td>
      <?php 
        switch($se->getType())
        {
          case "Invoice":
          case "Purchase":
            if(!$is_conversion)echo $ref->getUnittotal();
            break;
          case "Report":
            echo $se->getReportComment();
            break;
          default: 
            switch($se->getRefClass())
            {
              case "InvoiceDrDetail":
                echo $ref->getInvoicedetail()->getPrice();
                break;
              case "PurchaseDrDetail":
                $detail=$ref->getPurchasedetail();
                if($detail->getQty()==0)break;
                echo $detail->getTotal()/$detail->getQty();
                break;
            }
        }
      ?>
    </td>
    <td><?php 
        switch($se->getRefClass())
        {
          case "InvoiceDrDetail":
            echo link_to($ref->getInvoiceDr(),"invoice_dr/view?id=".$ref->getInvoiceDrId());
            if($ref->getQty()<0)echo " <font color=red>RETURN</font>";
            break;
          case "PurchaseDrDetail":
            echo link_to($ref->getPurchaseDr(),"purchase_dr/view?id=".$ref->getPurchaseDrId());
            if($ref->getQty()<0)echo " <font color=red>RETURN</font>";
            break;
          case "TransferDrDetail":
            echo link_to($ref->getTransferDr(),"transfer_dr/view?id=".$ref->getTransferDrId());
            break;
          case "JobOrderDrDetail":
            echo link_to($ref->getJobOrderDr(),"job_order_dr/view?id=".$ref->getJobOrderDrId());
            if($ref->getQty()<0)echo " <font color=red>Consumed</font>";
            break;
          default:
            echo $se->getType(); if($is_conversion)echo " Conversion";
        }
    
    ?></td>
    <td>
      <!--customer/supplier/notes-->
      <?php 
        switch($se->getRefClass())
        {
          case "ReplacementDetail":
            $r=$ref->getReplacement();
            if($r->getClientClass()=="Customer")
            {
              $customer=$r->getClient();
              echo link_to($customer,"customer/view?id=".$customer->getId());
            }
            elseif($r->getClientClass()=="Vendor")
            {
              $vendor=$r->getClient();
              echo link_to($vendor,"vendor/view?id=".$vendor->getId());
            }
            break;
          case "ReturnsDetail":
            $r=$ref->getReturns();
            if($r->getClientClass()=="Customer")
            {
              $customer=$r->getClient();
              echo link_to($customer,"customer/view?id=".$customer->getId());
            }
            elseif($r->getClientClass()=="Vendor")
            {
              $vendor=$r->getClient();
              echo link_to($vendor,"vendor/view?id=".$vendor->getId());
            }
            break;
          case "Deliverydetail":
            break;
          case "Purchasedetail":
          case "PurchaseDrDetail":
            $purchase=$ref->getPurchase();
            $vendor=$purchase->getVendor();
            echo "Supplier: ".link_to($vendor,"vendor/view?id=".$purchase->getVendorId())."; Sold to ".$purchase->getInvno().": "; 
            if($purchase->getMemo()!="")echo "; ".$purchase->getMemo();
            if($se->getRefClass()=="Purchasedetail")
            {
              $profitdetails= $ref->getProfitdetail();
            }
            if($se->getRefClass()=="PurchaseDrDetail")
            {
              $purchaseDr=$ref->getPurchaseDr();
              if($purchaseDr->getNotes()!="")echo "; ".$purchaseDr->getNotes();

              $profitdetails= $ref->getPurchasedetail()->getProfitdetail();
            }
            //display links to invoices sold to
            foreach($profitdetails as $pdetail)
            {
              $invoice=$pdetail->getInvoice();
              echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())." ";
            }
            break;
          case "Invoicedetail":
          case "InvoiceDrDetail":
            $invoice=$ref->getInvoice();
            $customer=$invoice->getCustomer();
            echo "Customer: ".link_to($customer,"customer/view?id=".$customer->getId());

            if($invoice->getNotes()!="")echo "; ".$invoice->getNotes();
            if($se->getRefClass()=="InvoiceDrDetail")
            {
              $invoiceDr=$ref->getInvoiceDr();
              if($invoiceDr->getNotes()!="")echo "; ".$invoiceDr->getNotes();
            }
            break;
          case "TransferDrDetail":
            //if stock entry is for source warehouse
            if($stock->getId()==$ref->getFromStock()->getId()){
              echo "To Warehouse ".$ref->getTransferDr()->getToWarehouse()->getName();
            }
            //else stock entry is for destination warehouse
            else{
              echo "From Warehouse ".$ref->getTransferDr()->getFromWarehouse()->getName();
            }
            break;
          case "JobOrderDrDetail"://notes
            $job_order=$ref->getJobOrder();
            //echo "Sold to ".$job_order->getInvno().": "; 
            if($job_order->getNotes()!="")echo "; ".$job_order->getNotes();
            if($se->getRefClass()=="JobOrderdetail")
            {
              // $profitdetails= $ref->getProfitdetail();
            }
            if($se->getRefClass()=="JobOrderDrDetail")
            {
              $job_order_dr=$ref->getJobOrderDr();
              if($job_order_dr->getNotes()!="")echo "; ".$job_order_dr->getNotes();

              // $profitdetails= $ref->getJobOrderdetail()->getProfitdetail();
            }
            // //display links to invoices sold to
            // foreach($profitdetails as $pdetail)
            // {
            //   $invoice=$pdetail->getInvoice();
            //   echo link_to($invoice->getInvno(),"invoice/view?id=".$invoice->getId())." ";
            // }
            break;
          default:
            if(isset($ref))
            {
              $client=$ref->getRef()->getClient();
              echo link_to($client,strtolower(get_class($client->getRawValue()))."/view?id=".$client->getId());
            }
            echo $se->getDescription();
        }
      ?>
    </td>
    <td>
      <?php 
        switch($se->getRefClass())
        {
          case "Invoicedetail":
          case "InvoiceDrDetail":
            if($ref)echo $ref->getInvoice()->getEmployee();
            break;
          case "Purchasedetail":
          case "PurchaseDrDetail":
              if($ref)echo $ref->getPurchase()->getEmployee();
            break;
          case "Deliverydetail":
            $parent=$ref->getParent(); 
            switch($parent->getClass())
            {
              case "Invoice": 
                $is_conversion=true;
                echo $parent->getEmployee();
                break;
            }
            break;
        }
      ?>
    </td>
    <td>
      <?php
        $created_by=false; 
        if($se->getCreatedById())
          $created_by = Doctrine_Query::create()
          ->from('SfGuardUser s')
          ->where("s.id=".$se->getCreatedById())
          ->fetchOne();
        if($created_by)echo $created_by->getUsername(); 
      ?></td>
    <td>
    <?php 
      if($sf_user->hasCredential(array('admin'), false))if($se->getType()=="Adjustment" or $se->getType()=="Report")echo link_to('Delete','stockentry/delete?id='.$se->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>


    <?php 
      switch($se->getRefClass())
      {
        case "Purchasedetail":
          $purchase=$ref->getPurchase();
          $files=$purchase->getFiles();
          if(count($files)>0)echo link_to(count($files)." File".(count($files)>1?"s":""),"purchase/files?id=".$ref->getPurchaseId());
          break;
      }
    ?>



      
    </td>
    
    <?php 
      $qtyarray=array(null,null,null,null,null,null,null,null,null);
      $colorarray=array(null,null,null,null,null,null,null,null,null);
      $fontcolorarray=array(null,null,null,null,null,null,null,null,null);
      $color="";
      $bold=false;
      $openbold="";
      $closebold="";
      switch($se->getRefClass())
      {
        case "Purchasedetail":
          $bold=true;
          $openbold="<b>";
          $closebold="</b>";
          $qtyarray[$ref->getSlot()]=str_replace(".00","",$ref->getQty());
          $colorarray[$ref->getSlot()]=$ref->getColor();
          $fontcolorarray[$ref->getSlot()]=($ref->getColorIndex()>3)?"white":"black";
          break;
        case "Invoicedetail":
          //there's really only one, but who cares
          $profitdetails= $ref->getProfitdetail();
          foreach($profitdetails as $pdetail)
          {
            $qtyarray[$pdetail->getSlot()]=link_to(str_replace(".00","",$pdetail->getQty()),"stock/disconnectInvoiceAndPurchase?profit_detail_id=".$pdetail->getId(), array('confirm' => 'Are you sure?'));
            $colorarray[$pdetail->getSlot()]=$pdetail->getColor();
            $fontcolorarray[$pdetail->getSlot()]=($pdetail->getColorIndex()>3)?"white":"black";
          }
          break;
      }
    ?>
    <?php if($qtyarray[1]!=null)echo "<td align=center bgcolor=".$colorarray[1]."><font color=".$fontcolorarray[1].">".$openbold.$qtyarray[1].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[2]!=null)echo "<td align=center bgcolor=".$colorarray[2]."><font color=".$fontcolorarray[2].">".$openbold.$qtyarray[2].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[3]!=null)echo "<td align=center bgcolor=".$colorarray[3]."><font color=".$fontcolorarray[3].">".$openbold.$qtyarray[3].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[4]!=null)echo "<td align=center bgcolor=".$colorarray[4]."><font color=".$fontcolorarray[4].">".$openbold.$qtyarray[4].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[5]!=null)echo "<td align=center bgcolor=".$colorarray[5]."><font color=".$fontcolorarray[5].">".$openbold.$qtyarray[5].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[6]!=null)echo "<td align=center bgcolor=".$colorarray[6]."><font color=".$fontcolorarray[6].">".$openbold.$qtyarray[6].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[7]!=null)echo "<td align=center bgcolor=".$colorarray[7]."><font color=".$fontcolorarray[7].">".$openbold.$qtyarray[7].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[8]!=null)echo "<td align=center bgcolor=".$colorarray[8]."><font color=".$fontcolorarray[8].">".$openbold.$qtyarray[8].$closebold."</font></td>";else echo "<td></td>";?>

    <td><?php //echo $se->getId(); ?></td>
  </tr>
  <?php }//end foreach $stockentries as $se?>      
</table>

<b>Page <?php echo $page;?></b>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."&page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."&page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo "<b>".$apage."</b> ";
  else echo link_to("$apage",$pagepath."&page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."&page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."&page=".$pagecount))." ";

?>

<br>
<br>

<input id="copy_text">
<script>
function copy(mystring)
{
  var copyText = document.getElementById("copy_text");
  copyText.value=mystring;
  copyText.select();
  document.execCommand("copy");
  //alert(copyText.value+"copied to clipboard");
}
</script>
