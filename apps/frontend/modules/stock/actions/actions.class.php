<?php

require_once dirname(__FILE__).'/../lib/stockGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/stockGeneratorHelper.class.php';

/**
 * stock actions.
 *
 * @package    sf_sandbox
 * @subpackage stock
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class stockActions extends autoStockActions
{
  public function executeReceiveAllPurchases(sfWebRequest $request)
  {
    //set all purchases to fully received unless cancelled or partially received
    $stock = $this->getRoute()->getObject();
    $stockentries=$stock->getStockEntriesDesc();    

    foreach($stockentries as $se)
    if($se->getRefClass()=="Purchasedetail")
    {
      $ref=$se->getRef();
      if($ref)
      {
        $purchase=$ref->getPurchase();
        if($purchase!=null and ($purchase->getDatereceived()==null or $purchase->getReceivedStatus()=="Not Received") and $purchase->getStatus()!="Cancelled")
        {
          $purchase->receiveAll($purchase->getDate());
        }
      }
    }
    $this->redirect($request->getReferer());
  }
  public function executeSoldTo(sfWebRequest $request)
  {
    $this->itemsperpage=50;
    $this->page=$request->getParameter("page");
    if($this->page==null)$this->page=1;
    $this->stock = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->stock);
    $this->stockentries=$this->stock->getStockEntriesDescByPage($this->page,$this->itemsperpage);
    $this->pagecount=$this->stock->getStockentriesPageCount($this->itemsperpage);
    $this->pagepath="stock/soldTo?id=".$this->stock->getId();
  }
  public function executeView(sfWebRequest $request)
  {
    $this->itemsperpage=50;
    $this->page=$request->getParameter("page");
    if($this->page==null)$this->page=1;
    $this->stock = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->stock);
    $this->stockentries=$this->stock->getStockEntriesDescByPage($this->page,$this->itemsperpage);
    $this->pagecount=$this->stock->getStockentriesPageCount($this->itemsperpage);
    $this->pagepath="stock/view?id=".$this->stock->getId();
    $this->product = $this->stock->getProduct();
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->stock = new stock();
    $this->stock->setDatetime(MyDateTime::now()->todatetime());
    $this->form = $this->configuration->getForm($this->stock);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $stock = $form->save();
        $stock->calc($stock->getDatetime());
        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $stock)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@stock_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect("stock/view?id=".$stock->getId());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeCalc(sfWebRequest $request)
  {
    $this->stock = $this->getRoute()->getObject();
    $this->stock->calc($this->stock->getDatetime());
    $this->redirect($request->getReferer());
    
  }
  public function executeStats(sfWebRequest $request)
  {
    /*
      SELECT *
      FROM `stock` , `product`
      WHERE product.monitored = true
      AND stock.currentqty <= stock.quota
    */
    $this->stocks=Doctrine_Query::create()
        ->from('Stock s, s.Product p, s.Warehouse w')
        ->where('p.monitored = true')
        ->andWhere('s.currentqty <= s.quota')
        ->execute();
  }
  public function executeToggleIsUpdated(sfWebRequest $request)
  {
    $stock=MyModel::fetchOne("Stock",array('id'=>$request->getParameter("id")));
    if($stock->getIsUpdated())
      $stock->setIsUpdated(null);
    else
      $stock->setIsUpdated(1);
    $stock->save();
     $this->getUser()->setFlash('notice', $notice.' Successfully set this stock to '.($stock->getIsUpdated()?"Updated":"Not Updated").'.',true);
   $this->redirect($request->getReferer());
  }

  public function executeZeroOut(sfWebRequest $request)
  {
  	die("ACCESS DENIED");

      //default values
    $this->interval=100;
    $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->products=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->products = Doctrine_Query::create()
      ->from('Product p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();

    $setting=Fetcher::fetchOne("Settings",array("name"=>"'default_warehouse_id'"));
    $warehouse_id=$setting->getValue();
    $qty=0;
    $type="Report";
    $qty_reported=0;
    // $datetime=$request->getParameter("datetime");
    $datetime=MyDateTime::now()->todatetime();
    $description="zero-out";

    foreach($this->products as $p)
    {
      $stock=StockTable::fetch($warehouse_id, $p->getId());
      $stockentry=$stock->addEntry($datetime, $qty, null, null ,$type,$description,$this->getUser()->getGuardUser()->getId(),$qty_reported);
    }

     $this->start=$this->end+1;
  }

  //used to connect invoice and purchase
  public function executeConnectInvoiceAndPurchase(sfWebRequest $request)
  {
    $purchase_id=$request->getParameter("purchase_id");
    $invoice_id=$request->getParameter("invoice_id");
    $invoice=Fetcher::fetchOne("Invoice",array("id"=>$invoice_id));
    $purchase=Fetcher::fetchOne("Purchase",array("id"=>$purchase_id));

    //create data structure: array[product_id][0...x]=invoice_detail.id
    $invoice_detail_ids=array();
    $invoice_details=$invoice->getInvoicedetails();
    foreach($invoice_details as $id)
    {
      if(!isset($invoice_detail_ids[$id->getProductId()]))
        $invoice_detail_ids[$id->getProductId()]=array();
      $invoice_detail_ids[$id->getProductId()][]=$id->getId();
    }

    //create data structure: array[product_id][0...x]=purchase_detail.id
    $purchase_detail_ids=array();
    $purchase_details=$purchase->getPurchasedetails();
    foreach($purchase_details as $pd)
    {
      if(!isset($purchase_detail_ids[$pd->getProductId()]))
        $purchase_detail_ids[$pd->getProductId()]=array();
      $purchase_detail_ids[$pd->getProductId()][]=$pd->getId();
    }

    foreach($invoice_detail_ids as $product_id=>$detail_ids)
    {
      #if the same product id is present in $purchase_detail_ids
      if(isset($purchase_detail_ids[$product_id]))
      {
        #go through every combination of invoice_detail_id and purchase_detail_id 
        #and connect them
        $idids=$invoice_detail_ids[$product_id];
        $pdids=$purchase_detail_ids[$product_id];
        foreach($idids as $idid)
          foreach($pdids as $pdid)
            ProfitdetailTable::connectDetails($product_id, $invoice, $idid, $purchase, $pdid);
      }
    }
    $this->redirect($request->getReferer());
  }

  //used to connect invoice and purchase
  public function executeDisconnectInvoiceAndPurchase(sfWebRequest $request)
  {
    $profit_detail_id=$request->getParameter("profit_detail_id");
    $profit_detail=Fetcher::fetchOne("ProfitDetail",array("id"=>$profit_detail_id));
    $invoicedetail=$profit_detail->getInvoicedetail();
    $purchasedetail=$profit_detail->getPurchasedetail();
    $profit_detail->delete();

    $invoicedetail->calcRemaining();
    $invoicedetail->save();
    $purchasedetail->calcRemaining();
    $purchasedetail->save();
    
    $this->redirect($request->getReferer());
  }
}
