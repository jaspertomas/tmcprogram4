<?php

require_once dirname(__FILE__).'/../lib/passbook_entryGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/passbook_entryGeneratorHelper.class.php';

/**
 * passbook_entry actions.
 *
 * @package    sf_sandbox
 * @subpackage passbook_entry
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class passbook_entryActions extends autoPassbook_entryActions
{

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
      $requestparams=$request->getParameter($form->getName());
      $requestparams["created_at"]=MyDateTime::now()->todatetime();
      $requestparams["created_by_id"]=$this->getUser()->getGuardUser()->getId();
      $form->bind($requestparams, $request->getFiles($form->getName()));
      if ($form->isValid())
      {
        $passbook=Fetcher::fetchOne("Passbook",array("id"=>$requestparams['passbook_id']));
        $qty=$requestparams['qty'];
        $qty_reported=$requestparams['qty_reported'];
        $datetime=
          $requestparams['datetime']['year']."-".
          $requestparams['datetime']['month']."-".
          $requestparams['datetime']['day']." ".
          $requestparams['datetime']['hour'].":".
          $requestparams['datetime']['minute'].":00";
        $type=$requestparams['type']!=""?$requestparams['type']:'Report';
        $description=$requestparams['description'];
        $created_by_id=$this->getUser()->getGuardUser()->getId();
        $passbook_entry=$passbook->addEntry($datetime, $qty, "", "", "", "", $type, $description, $created_by_id, $qty_reported);

        $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

        $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $passbook_entry)));
  
        $this->getUser()->setFlash('notice', $notice);
  
        $this->redirect('passbook/view?id='.$passbook_entry->getPassbookId());
      }
      else
      {
        if($form['qty']->getError())
          $this->redirect('home/error?msg="Invalid Qty: '.$qty.'"');
        if($form['datetime']->getError())
          $this->redirect('home/error?msg="Invalid Date/Time: '.$datetime.'"');
  
        //$this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
      }
    }
    public function executeDelete(sfWebRequest $request)
    {
      $request->checkCSRFProtection();
  
      $passbookentry=$this->getRoute()->getObject();
      $passbook=$passbookentry->getPassbook();
      $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));
  
      if ($passbookentry->delete())
      {
        $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
      }
  
      //$passbook->calc($passbookentrydate);
      $this->redirect($request->getReferer());
    }
}
