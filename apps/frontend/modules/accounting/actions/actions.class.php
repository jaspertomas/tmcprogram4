<?php

class accountingActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        echo json_encode(array("success"=>true,"message"=>"hi"));
        die();
    }
    //usage:
    //http://localhost/tmcprogram4/web/accounting/totalSalesForMonthAndYear?year=2022&month=12
    public function executeTotalSalesForMonthAndYear(sfWebRequest $request)
    {
        $year=$request->getParameter("year");
        $month=$request->getParameter("month");
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();

        $result= Doctrine_Query::create()
            ->select("sum(i.total)")
            ->from('Invoice i, i.Customer c')
            ->where('i.date>="'.$startofmonth->tomysql().'"')
            ->andWhere('i.date<="'.$endofmonth->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ->andWhere('c.is_owned!=1')
            ->execute();

        echo json_encode(array("success"=>true,"sum"=>$result[0]->getSum()));
        die();
    }
    //usage:
    //http://localhost/tmcprogram4/web/accounting/totalPurchasesForMonthAndYear?year=2022&month=12
    public function executeTotalPurchasesForMonthAndYear(sfWebRequest $request)
    {
        $year=$request->getParameter("year");
        $month=$request->getParameter("month");
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();

        $result= Doctrine_Query::create()
            ->select("sum(p.total)")
            ->from('Purchase p')
            ->where('p.date>="'.$startofmonth->tomysql().'"')
            ->andWhere('p.date<="'.$endofmonth->tomysql().'"')
            ->andWhere('p.status!="Cancelled"')
            ->execute();

        echo json_encode(array("success"=>true,"sum"=>$result[0]->getSum()));
        die();
    }
    //account means voucher account
    //http://localhost/tmcprogram4/web/accounting/totalExpensesForMonthAndYearAndAccountIdAndAllocationId?year=2022&month=12&accountId=35&allocationId=1
    public function executeTotalExpensesForMonthAndYearAndAccountIdAndAllocationId(sfWebRequest $request)
    {
        $year=$request->getParameter("year");
        $month=$request->getParameter("month");
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary
        $accountId=$request->getParameter("accountId");
        $allocationId=$request->getParameter("allocationId");

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();

        $result= Doctrine_Query::create()
            ->select("sum(v.amount)")
            ->from('Voucher v')
            ->where('v.date>="'.$startofmonth->tomysql().'"')
            ->andWhere('v.date<="'.$endofmonth->tomysql().'"')
            ->andWhere('v.is_cancelled is null')
            ->andWhere('v.account_id='.$accountId)
            ->andWhere('v.voucher_allocation_id='.$allocationId)
            ->execute();

        echo json_encode(array("success"=>true,"sum"=>$result[0]->getSum()));
        die();
    }
    public function executeGetAccountData(sfWebRequest $request)
    {
        $items= Doctrine_Query::create()
            ->from('VoucherAccount va')
            ->execute();

        $array=array();
        foreach($items as $item)
        {
            $array[$item->getId()]=$item->getName();
        }
        echo json_encode(array("success"=>true,"data"=>$array));
        die();
    }
    public function executeGetAllocationData(sfWebRequest $request)
    {
        $items= Doctrine_Query::create()
            ->from('VoucherAllocation va')
            ->execute();

        $array=array();
        foreach($items as $item)
        {
            $array[$item->getId()]=$item->getName();
        }
        echo json_encode(array("success"=>true,"data"=>$array));
        die();
    }

    //usage:
    //http://localhost/tmcprogram4/web/accounting/totalSalesForMonthAndYear?year=2022&month=12
    public function executeTotalSalesForDate(sfWebRequest $request)
    {
        // $date=MyDateTime::frommysql($request->getParameter("date"));

        $result= Doctrine_Query::create()
            ->select("sum(i.total)")
            ->from('Invoice i')
            ->where('i.date="'.$request->getParameter("date").'"')
            ->andWhere('i.status!="Cancelled"')
            ->execute();

        $sum=$result[0]->getSum();
        if($sum==null)$sum=0;

        echo json_encode(array("success"=>true,"sum"=>$sum));
        die();
    }

    //-----------separates sales of Tanks and Pumps------------
    public function executeTotalSalesForMonthAndYearByProductCategory(sfWebRequest $request)
    {
        $year=$request->getParameter("year");
        $month=$request->getParameter("month");
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();

        $query=Doctrine_Query::create()
            ->select("sum(id.total)")
            ->from('Invoice i, i.Invoicedetail id, id.Product p')
            ->where('i.date>="'.$startofmonth->tomysql().'"')
            ->andWhere('i.date<="'.$endofmonth->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ;

        //if product category is ALL, return all products
        if($request->getParameter("productcategory")=="ALL");
        //if TJL, return tanks
        else if($request->getParameter("productcategory")=="TJL")
            $query->andWhere('p.productcategory="TJL"');
        //if Sheets, return sheets
        else if($request->getParameter("productcategory")=="Sheet")
            $query->andWhere('p.productcategory="Sheet"');
        //if Supplies, return welding rods, etc
        else if($request->getParameter("productcategory")=="Supplies")
            $query->andWhere('p.productcategory="Supplies"');
        //else, return pumps
        else if($request->getParameter("productcategory")=="Pumps")
            $query->andWhere('p.productcategory=""');
            
        $result=$query->execute();
        echo json_encode(array("success"=>true,"sum"=>$result[0]->getSum()));
        die();
    }
    public function executeTotalSalesForDateByProductCategory(sfWebRequest $request)
    {
        // $date=MyDateTime::frommysql($request->getParameter("date"));
        $query=Doctrine_Query::create()
            ->select("sum(id.total)")
            ->from('Invoice i, i.Invoicedetail id, id.Product p')
            ->where('i.date="'.$request->getParameter("date").'"')
            ->andWhere('i.status!="Cancelled"')
            ;

            //if product category is tjl, return productcategory !=""
        if($request->getParameter("productcategory")=="TJL")
            $query->andWhere('p.productcategory!=""');
        //else, return  productcategory ==""
        else
            $query->andWhere('p.productcategory=""');

        $result=$query->execute();

        $sum=$result[0]->getSum();
        if($sum==null)$sum=0;

        echo json_encode(array("success"=>true,"sum"=>$sum));
        die();
    }
    public function executeTotalPurchasesForMonthAndYearByProductCategory(sfWebRequest $request)
    {
        $year=$request->getParameter("year");
        $month=$request->getParameter("month");
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();

        $query=Doctrine_Query::create()
            ->select("sum(id.total)")
            ->from('Purchase i, i.Purchasedetail id, id.Product p')
            ->where('i.date>="'.$startofmonth->tomysql().'"')
            ->andWhere('i.date<="'.$endofmonth->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ;

        //if product category is ALL, return all products
        if($request->getParameter("productcategory")=="ALL");
        //if TJL, return tanks
        else if($request->getParameter("productcategory")=="TJL")
            $query->andWhere('p.productcategory="TJL"');
        //if Sheets, return sheets
        else if($request->getParameter("productcategory")=="Sheet")
            $query->andWhere('p.productcategory="Sheet"');
        //if Supplies, return welding rods, etc
        else if($request->getParameter("productcategory")=="Supplies")
            $query->andWhere('p.productcategory="Supplies"');
        //else, return pumps
        else if($request->getParameter("productcategory")=="Pumps")
            $query->andWhere('p.productcategory=""');

        $result=$query->execute();
        echo json_encode(array("success"=>true,"sum"=>$result[0]->getSum()));
        die();
    }
    public function executeTotalPurchasesForDateByProductCategory(sfWebRequest $request)
    {
        // $date=MyDateTime::frommysql($request->getParameter("date"));
        $query=Doctrine_Query::create()
            ->select("sum(id.total)")
            ->from('Purchase i, i.Purchasedetail id, id.Product p')
            ->where('i.date="'.$request->getParameter("date").'"')
            ->andWhere('i.status!="Cancelled"')
            ;

            //if product category is tjl, return productcategory !=""
        if($request->getParameter("productcategory")=="TJL")
            $query->andWhere('p.productcategory!=""');
        //else, return  productcategory ==""
        else
            $query->andWhere('p.productcategory=""');

        $result=$query->execute();

        $sum=$result[0]->getSum();
        if($sum==null)$sum=0;

        echo json_encode(array("success"=>true,"sum"=>$sum));
        die();
    }
    public function executeTotalSalesForMonthAndYearByCustomerIdAndProductCategory(sfWebRequest $request)
    {
        $customer_id=$request->getParameter("customer_id");
        $year=$request->getParameter("year");
        $month=$request->getParameter("month");
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();

        $query=Doctrine_Query::create()
            ->select("sum(i.total), customer_id")
            ->from('Invoice i')
            ->where('i.date>="'.$startofmonth->tomysql().'"')
            ->andWhere('i.date<="'.$endofmonth->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ->groupBy('i.customer_id')
            ;

            // //if product category is tjl, return productcategory !=""
        // if($request->getParameter("productcategory")=="TJL")
        //     $query->andWhere('p.productcategory!=""');
        // //else, return  productcategory ==""
        // else
        //     $query->andWhere('p.productcategory=""');

        $result=$query->execute();
        var_dump($result[0]->getSum());
        echo "<br>";
        var_dump($result[1]->getSum());
        echo "<br>";
        var_dump($result[2]->getSum());
        echo "<br>";
        foreach($result as $key=>$value)
        {
            echo json_encode(array("success"=>true,"key"=>$key,"sum"=>$result[$key]->getSum(),"customer_id"=>$result[$key]->getCustomerId()));
            echo "<br>";
        }
        die();
    }
    public function executeTopCustomersForYear(sfWebRequest $request)
    {
        $customer_id=$request->getParameter("customer_id");
        $year=$request->getParameter("year");
        $month=$request->getParameter("month");
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();
        $startofyear=MyDateTime::frommysql($year.'-01-01');
        $endofyear=$startofyear->getendofyear();

        $query=Doctrine_Query::create()
            ->select("sum(i.total), customer_id")
            ->from('Invoice i')
            ->where('i.date>="'.$startofyear->tomysql().'"')
            ->andWhere('i.date<="'.$endofyear->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ->groupBy('i.customer_id')
            ->orderBy('sum desc')
            ;

            // //if product category is tjl, return productcategory !=""
        // if($request->getParameter("productcategory")=="TJL")
        //     $query->andWhere('p.productcategory!=""');
        // //else, return  productcategory ==""
        // else
        //     $query->andWhere('p.productcategory=""');

        $result=$query->execute();
        $rank=0;
        foreach($result as $key=>$value)
        {
            $rank+=1;
            echo $result[$key]->getCustomerId().";".$rank.";".$result[$key]->getSum().";".$result[$key]->getCustomer()->getName();
            echo "<br>";
        }
        die();
    }

    //------forecast tools------
    public function executeTotalSalesTanksThisYear(sfWebRequest $request)
    {
        $year=$request->getParameter("year");
        if($year==null)$year=2023;
        $productcategory=$request->getParameter("productcategory");
        if($productcategory==null)$productcategory="TJL";

        // SELECT invoicedetail.qty
        // FROM invoice
        // LEFT JOIN invoicedetail
        // ON invoicedetail.invoice_id = invoice.id
        // where invoice.date >="2023-01-01" and invoicedetail.product_id=691	
        // echo "<table border=1>";
        $results=$this->totalSalesPerCategoryPerYear($productcategory,$year);

        $this->months=array("1","2","3","4","5","6","7","8","9","10","11","12");
        foreach($this->months as $month)
        {
            $data=$this->totalSalesPerCategoryPerMonth($productcategory,$month,$year);
            foreach($data as $key=>$value)
            {
                $results[$key]["months"][$month]=$value;
            }
        }
        $this->results=$results;
        $this->year=$year;
    }
    function totalSalesPerCategoryPerMonth($productcategory,$month,$year)
    {
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();

        $query=Doctrine_Query::create()
            ->select("id.product_id, sum(id.qty) as qty, sum(id.total) as total")
            ->from('Invoicedetail id, id.Invoice i, id.Product p')
            ->where('i.date>="'.$startofmonth->tomysql().'"')
            ->andWhere('i.date<="'.$endofmonth->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ->andWhere('p.productcategory="'.$productcategory.'"')
            ->groupBy('id.product_id')
            ;
        $data=$query->execute()->toArray();
        $result=array();
        foreach($data as $productdata)
        {
            $result[$productdata["product_id"]]=array(
                "product_id"=>$productdata["product_id"],
                "total"=>$productdata["total"],
                "qty"=>$productdata["qty"],
            );
        }
        // echo "<pre>";
        // var_dump($result);
        return $result;
    
    }
    function totalSalesPerCategoryPerYear($productcategory,$year)
    {
        $startofyear=MyDateTime::frommysql($year.'-01-01');
        $endofyear=$startofyear->getendofyear();

        $query=Doctrine_Query::create()
            ->select("id.product_id, sum(id.qty) as qty, sum(id.total) as total")
            ->from('Invoicedetail id, id.Invoice i, id.Product p')
            ->where('i.date>="'.$startofyear->tomysql().'"')
            ->andWhere('i.date<="'.$endofyear->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ->andWhere('p.productcategory="'.$productcategory.'"')
            ->groupBy('id.product_id')
            ;
        $data=$query->execute()->toArray();
        $result=array();
        foreach($data as $productdata)
        {
            $result[$productdata["product_id"]]=array(
                "product_id"=>$productdata["product_id"],
                "total"=>$productdata["total"],
                "qty"=>$productdata["qty"],
                "months"=>$array,
            );
        }
        return $result;
    }

    public function executeTotalPurchasesTanksThisYear(sfWebRequest $request)
    {
        $year=$request->getParameter("year");
        if($year==null)$year=2023;
        $productcategory=$request->getParameter("productcategory");
        if($productcategory==null)$productcategory="Sheet";

        $results=$this->totalPurchasesPerCategoryPerYear($productcategory,$year);

        $this->months=array("1","2","3","4","5","6","7","8","9","10","11","12");
        foreach($this->months as $month)
        {
            $data=$this->totalPurchasesPerCategoryPerMonth($productcategory,$month,$year);
            foreach($data as $key=>$value)
            {
                $results[$key]["months"][$month]=$value;
            }
        }
        $this->results=$results;
        $this->year=$year;
    }
    function totalPurchasesPerCategoryPerMonth($productcategory,$month,$year)
    {
        $month=str_pad($month, 2, '0', STR_PAD_LEFT); //left pad with zeros if necessary

        //calculate end of month
        $startofmonth=MyDateTime::frommysql($year.'-'.$month.'-01');
        $endofmonth=$startofmonth->getendofmonth();

        $query=Doctrine_Query::create()
            ->select("id.product_id, sum(id.qty) as qty, sum(id.total) as total")
            ->from('Purchasedetail id, id.Purchase i, id.Product p')
            ->where('i.date>="'.$startofmonth->tomysql().'"')
            ->andWhere('i.date<="'.$endofmonth->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ->andWhere('p.productcategory="'.$productcategory.'"')
            ->groupBy('id.product_id')
            ;
        $data=$query->execute()->toArray();
        $result=array();
        foreach($data as $productdata)
        {
            $result[$productdata["product_id"]]=array(
                "product_id"=>$productdata["product_id"],
                "total"=>$productdata["total"],
                "qty"=>$productdata["qty"],
            );
        }
        // echo "<pre>";
        // var_dump($result);
        return $result;
    
    }
    function totalPurchasesPerCategoryPerYear($productcategory,$year)
    {
        $startofyear=MyDateTime::frommysql($year.'-01-01');
        $endofyear=$startofyear->getendofyear();

        $query=Doctrine_Query::create()
            ->select("id.product_id, sum(id.qty) as qty, sum(id.total) as total")
            ->from('Purchasedetail id, id.Purchase i, id.Product p')
            ->where('i.date>="'.$startofyear->tomysql().'"')
            ->andWhere('i.date<="'.$endofyear->tomysql().'"')
            ->andWhere('i.status!="Cancelled"')
            ->andWhere('p.productcategory="'.$productcategory.'"')
            ->groupBy('id.product_id')
            ;
        $data=$query->execute()->toArray();
        $result=array();
        foreach($data as $productdata)
        {
            $result[$productdata["product_id"]]=array(
                "product_id"=>$productdata["product_id"],
                "total"=>$productdata["total"],
                "qty"=>$productdata["qty"],
                "months"=>$array,
            );
        }
        return $result;
    }
}
