<h3>For Year <?php echo $year?></h3>
<h3>Purchases Quantity</h3>
<table border=1>
    <tr>
        <td>Product Id</td>
        <td>Product</td>
        <td><?php echo $year?></td>
        <td>Jan</td>
        <td>Feb</td>
        <td>Mar</td>
        <td>Apr</td>
        <td>May</td>
        <td>Jun</td>
        <td>Jul</td>
        <td>Aug</td>
        <td>Sep</td>
        <td>Oct</td>
        <td>Nov</td>
        <td>Dec</td>
    </tr>
    <?php
        foreach($results as $item)
        {
            $product=Fetcher::fetchOne("Product",array("id"=>$item["product_id"]));
            echo "<tr><td>".$product->getId()."</td><td>".$product->getName()."</td>";
            echo "<td align=right>".$item["qty"]."</td>";
            foreach($months as $month)
            {
                $qty=$item["months"][$month]["qty"];
                $total=$item["months"][$month]["total"];
                if($qty!=0)echo "<td align=right>".$qty."</td>";
                else echo "<td align=right></td>";
            }
            echo "</tr>";
        }
    ?>
</table>

<h3>Purchases Total</h3>
<table border=1>
    <tr>
        <td>Product Id</td>
        <td>Product</td>
        <td><?php echo $year?></td>
        <td>Jan</td>
        <td>Feb</td>
        <td>Mar</td>
        <td>Apr</td>
        <td>May</td>
        <td>Jun</td>
        <td>Jul</td>
        <td>Aug</td>
        <td>Sep</td>
        <td>Oct</td>
        <td>Nov</td>
        <td>Dec</td>
    </tr>
    <?php
        foreach($results as $item)
        {
            $product=Fetcher::fetchOne("Product",array("id"=>$item["product_id"]));
            echo "<tr><td>".$product->getId()."</td><td>".$product->getName()."</td>";
            echo "<td align=right>".MyDecimal::format($item["total"])."</td>";
            foreach($months as $month)
            {
                $qty=$item["months"][$month]["qty"];
                $total=$item["months"][$month]["total"];
                if($qty!=0)echo "<td align=right>".MyDecimal::format($item["months"][$month]["total"])."</td>";
                else echo "<td align=right></td>";
            }
            echo "</tr>";
        }
    ?>
</table>

