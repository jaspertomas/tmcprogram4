<?php use_helper('I18N', 'Date'); ?>
<h1>Commission Payment: <?php echo $cp->getName()?> </h1>
<h3>Commission Type: Profit Based </h3>
<br>Salesman: <?php echo $cp->getEmployee()?>
<br>Date Created: <?php echo MyDateTime::frommysql($cp->getDateCreated())->toshortdate()?>
<br>Date Paid: <?php echo $cp->getDatePaid()?>
<br><?php echo link_to('Print','commission_payment/viewPdf?id='.$cp->getId()) ?>
<br><?php echo link_to('Delete','commission_payment/delete?id='.$cp->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>

<?php
$grandtotalpurchase=0;
$grandtotalprofit=0;
$unaccountedsaletotal=0;
?>
<br>

<?php if($sf_user->hasCredential(array('admin','encoder'),false)){?>
<hr>
<h2>Quick Input</h2>
<?php echo form_tag("commission_payment/quickInput1")?>
<input type=hidden name=id value=<?php echo $cp->getId()?>>
<textarea name=invnos cols=40 rows=5>
</textarea>
<br><input type=submit>
</form>
<?php } ?>

<hr>
<h2>Invoices</h2>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Invoice</td>
    <td>Product</td>
    <td>Qty</td>
    <td>Sell Price</td>
    <td>Buy Price</td>
    <td>Profit</td>
    <td>Profit<br>Rate</td>
    <td>Total<br>Profit</td>
    <td>Unaccounted<br>Sale</td>
    <td>Total<br>Sale</td>
  </tr>
  <?php foreach($invoices as $invoice){?>
  <tr>
    <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
    <td><?php echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td><b><?php $saletotal+=$invoice->getTotal(); echo MyDecimal::format($invoice->getTotal()); ?></b></td>
  </tr>
  <?php foreach($invoice->getInvoicedetail() as $detail)if(!$detail->getIsVat()){
    if($detail->getProduct()->getIsService()==0)//if product is service, ignore
    {
      $profitdetails=$detail->getProfitdetail(); //echo count($pd);
      //-------------PROCESS INVOICEDETAILS----------------
      //use only remaining, not qty
      if($detail->getRemaining()!=0)
      {
      ?>
  <tr>
  	<td></td>
  	<td></td>
    <td><?php echo $detail->getProduct() ?></td>
    <td><?php echo intval($detail->getRemaining()) ?></td>
    <td align=right><?php $sellprice=$detail->getPrice(); echo $sellprice; ?></td>
    <td align=right><?php $buyprice=$detail->getProduct()->getMaxbuyprice();if($buyprice==0)$buyprice=$detail->getProduct()->getMinbuyprice(); echo $buyprice; $grandtotalpurchase+=$buyprice*$detail->getRemaining();?></td>
    <td align=right><?php if($buyprice==0)$profit=0; else $profit=$sellprice-$buyprice; echo MyDecimal::format($profit,",","."); ?></td>
    <td align=right>
<?php if($buyprice==0)$profitrate=0;else $profitrate=$profit/$buyprice*100; ?>
<font <?php if($profitrate>50)echo "color=red" ?>>
<?php echo MyDecimal::format($profitrate); ?>%
</font>
</td>

<?php if($buyprice!=0){ //Total Profit?>
    <td align=right><?php 
      // ---------special min profit rates-------------
      //see if product's type is in array of product type ids with special minimum profit rates
      $index=array_search($detail->getProduct()->getProducttypeId(), $sf_data->getRaw('producttype_ids'));
      //if minprofitrate contains a producttype id
      if ($index)
      {
        $minprofitrate=$minprofitrates[$index];
      }
      //else use default
      else
      {
        $minprofitrate=$defaultminimumprofitrate;
      }
      
      //if profit rate is equal or above minimum or if it is below 0 (negative profit)
      //include it to commission
      if($profitrate>=$minprofitrate or $profitrate<0){$totalprofit=$profit*$detail->getRemaining(); echo MyDecimal::format($totalprofit,",","."); $grandtotalprofit+=$totalprofit;}else{echo "0.00";} 
      // ---------end special min profit rates-------------
      ?>
    </td>
  	<td></td>
<?php }else{ ?>
  	<td></td>
    <td align=right><?php echo MyDecimal::format($saletotal,",","."); $unaccountedsaletotal+=$saletotal; //Unaccounted Sale?></td>
<?php } ?>

<td></td>
  </tr>
      <?php
      }// end if($detail->getRemaining()!=0)
      //------------PROCESS PROFITDETAILS-----------------
      foreach($profitdetails as $pd)
      {
      ?>
  <tr>
  	<td></td>
  	<td></td>
    <td><?php echo $detail->getProduct() ?></td>
    <td><?php echo intval($pd->getQty()) ?></td>
    <td align=right><?php $sellprice=$detail->getPrice(); echo $sellprice; ?></td>
    <td align=right><?php $buyprice=$pd->getBuyprice(); echo $buyprice; $grandtotalpurchase+=$buyprice*$pd->getQty();?></td>
    <td align=right><?php $profit=$sellprice-$buyprice; echo MyDecimal::format($profit,",","."); ?></td>
<?php if($buyprice!=0){?>
    <td align=right>
<?php if($buyprice==0)$profitrate=100;else $profitrate=intval($pd->getProfitrate()*100); ?>
<font <?php if($profitrate>50)echo "color=red" ?>>
<?php echo MyDecimal::format($profitrate); ?>%
</font>
</td>
    <td align=right><?php 
      // ---------special min profit rates-------------
      //see if product's type is in array of product type ids with special minimum profit rates
      $index=array_search($detail->getProduct()->getProducttypeId(), $sf_data->getRaw('producttype_ids'));
      //if minprofitrate contains a producttype id
      if ($index)
      {
        $minprofitrate=$minprofitrates[$index];
      }
      //else use default
      else
      {
        $minprofitrate=$defaultminimumprofitrate;
      }
      
      //if profit rate is equal or above minimum or if it is below 0 (negative profit)
      //include it to commission
      if($profitrate>=$minprofitrate or $profitrate<0){$totalprofit=$profit*$pd->getQty(); echo MyDecimal::format($totalprofit,",","."); $grandtotalprofit+=$totalprofit;}else{echo "0.00";} 
      // ---------end special min profit rates-------------
      ?>
    </td>
  	<td></td>
      <?php }else{ ?>
  	<td></td>
    <td align=right><?php $rowtotal=$sellprice*$pd->getQty(); echo MyDecimal::format($rowtotal,",","."); $unaccountedsaletotal+=$rowtotal;?></td>
      <?php } ?>
      <td></td>
  </tr>
      <?php
      }//end foreach($profitdetails as $pd)
      //----------END PROCESS INVOICEDETAILS AND PROFITDETAILS-----------
    }//end if($detail->getProduct()->getIsService()==0)
  ?>
  <?php } //end foreach($invoice->getInvoicedetail() as $detail)?>
  <?php } //end foreach($invoices as $invoice)?>

  <?php //commission gross based calculation?>
  <?php if(sfConfig::get('custom_commission_type')=="gross_sale"){?>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=3>Total Sale:</td>
    <td align=right><?php echo MyDecimal::format($saletotal)?></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=3>Commission Rate:</td>
    <td align=right><?php echo MyDecimal::format($cp->getEmployee()->getCommission(),",",".");?>%</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=3>Total Commission:</td>
    <td align=right><?php echo MyDecimal::format($saletotal/100*$cp->getEmployee()->getCommission(),",",".");?></td>
    <td></td>
  </tr>
  <?php }else{ ?>

  <?php //commission profit based calculation?>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=2>Total Sale:</td>
    <td></td>
    <td></td>
    <td><?php echo MyDecimal::format($saletotal)?></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=2>Total Profit:</td>
    <td align=right><?php echo MyDecimal::format($grandtotalprofit,",",".");?></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=2>Total Purchase:</td>
    <td align=right><?php echo MyDecimal::format($grandtotalpurchase,",",".");?></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=2>Profit Rate:</td>
    <td align=right><?php $profitrate=$grandtotalprofit/$grandtotalpurchase*100; echo MyDecimal::format($profitrate,",",".");?>%</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=3>Total Unaccounted Sale:</td>
    <td align=right><?php echo MyDecimal::format($unaccountedsaletotal,",",".");?></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=2>Profit Commission:</td>
    <td align=right><?php $profitcommission=$grandtotalprofit*.75*$cp->getEmployee()->getCommission()/100; echo MyDecimal::format($profitcommission,",",".");?></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=3>Unaccounted Sale Commission:</td>
    <td align=right><?php $unaccountedcommission=$unaccountedsaletotal*$profitrate/100*.75*$cp->getEmployee()->getCommission()/100; echo MyDecimal::format($unaccountedcommission,",",".");?></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=3>Total Commission:</td>
    <td align=right><b><?php echo MyDecimal::format($profitcommission+$unaccountedcommission,",",".");?></b></td>
    <td></td>
  </tr>
<?php } ?>
</table>


