<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("L", PDF_UNIT, "FOLIO", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Tradewind Mdsg Corp Commission Report - '.$cp->getName());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 20, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->startPageGroup();
$pdf->AddPage();

$pdf->write(0,"Commission Report: ".$cp->getName(),'',false,'',true,0,false,false,0,0);

$pdf->SetFont('dejavusans', '', 10, '', true);

//header
$contents=array();
$contents[]=array(
  "Date",
  "Invoice",
  "Invoice",
  "",
  "",
  "",
  "",
  "",
);
$contents[]=array(
  "",
  "Product",
  "Total",
  "Qty",
  "Item Total",
  "Pumps",
  "Tanks",
  "Not Included",
  "Customer",
  );
$widths=array(30,80,25,10,25,25,25,25,50);
$height=1;
foreach($contents as $content)
{
  $height=max(
    $pdf->getStringHeight($widths[0],$content[0])
    ,$pdf->getStringHeight($widths[1],$content[1])
    ,$pdf->getStringHeight($widths[2],$content[2])
    ,$pdf->getStringHeight($widths[3],$content[3])
    ,$pdf->getStringHeight($widths[4],$content[4])
    ,$pdf->getStringHeight($widths[5],$content[5])
    ,$pdf->getStringHeight($widths[6],$content[6])
    ,$pdf->getStringHeight($widths[7],$content[7])
    ,$pdf->getStringHeight($widths[8],$content[8])
  );      
  $pdf->MultiCell($widths[0], $height, $content[0], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[8], $height, $content[8], 0, 'C', 0, 1, '', '', true);
}

//body
//-------------------
$tanks_total=0;
$pumps_total=0;
$other_total=0;

$contents=array();
foreach($invoices as $invoice){
  $contents[]=array(
    MyDateTime::frommysql($invoice->getDate())->toshortdate(),
    $invoice,
    MyDecimal::format($invoice->getTotal()),
    "",
    "",
    "",
    "",
    "",
    $invoice->getCustomer()->getName(),
  );
  foreach($invoice->getInvoicedetail() as $detail)if(!$detail->getIsVat()){
    $profitdetails=$detail->getProfitdetail(); //echo count($pd);
    $product=$detail->getProduct();
    $skip=false;
    if($product->getIsTax())$skip=true;//do not include withholding tax and other "services"
    $employee=$cp->getEmployee();
    //if employee is technician, show only services
    if($employee->getIsTechnician()){if(!$product->getIsService())$skip=true;}
    //if employee is salesman, show only products
    else{if($product->getIsService())$skip=true;}
  
    if($skip)
    {
      $other=$detail->getTotal();
      $other_total+=$other;
      $contents[]=array(
        "",
        $product->getName(),
        "",
        intval($detail->getQty()),
        MyDecimal::format($detail->getTotal()/$detail->getQty()),
        "",
        "",
        MyDecimal::format($other),
        "",
      );
    }
    else
    {
      $pumps=0;
      $tanks=0;
      if($product->getProductcategory()=='TJL')
      {
        $tanks=$detail->getTotal();
        $tanks_total+=$tanks;
      }else{
        $pumps=$detail->getTotal();
        $pumps_total+=$pumps;
      }
    }
    $contents[]=array(
      "",
      $product->getName(),
      "",
      intval($detail->getQty()),
      MyDecimal::format($detail->getTotal()/$detail->getQty()),
      MyDecimal::format($pumps),
      MyDecimal::format($tanks),
      MyDecimal::format($other),
      "",
    );
  } //end foreach($invoice->getInvoicedetail() as $detail)
  } //end foreach($invoices as $invoice)
    
foreach($contents as $content)
{
  $height=max(
    $pdf->getStringHeight($widths[0],$content[0])
    ,$pdf->getStringHeight($widths[1],$content[1])
    ,$pdf->getStringHeight($widths[2],$content[2])
    ,$pdf->getStringHeight($widths[3],$content[3])
    ,$pdf->getStringHeight($widths[4],$content[4])
    ,$pdf->getStringHeight($widths[5],$content[5])
    ,$pdf->getStringHeight($widths[6],$content[6])
    ,$pdf->getStringHeight($widths[7],$content[7])
    ,$pdf->getStringHeight($widths[8],$content[8])
  );      
  $pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 1, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[8], $height, $content[8], 1, 'R', 0, 1, '', '', true);
}

//footer    
$grand_total=$tanks_total+$pumps_total+$other_total;
$pdf->SetFont('dejavusans','B',10);
$contents=array();
$contents[]=array(
  "",
  "Total Commissionable Amount:",
  "",
  "",
  MyDecimal::format($grand_total,",","."),
  MyDecimal::format($pumps_total,",","."),
  MyDecimal::format($tanks_total,",","."),
  MyDecimal::format($other_total,",","."),
  "",
);
$contents[]=array(
  "",
  "Rate:",
  "",
  "",
  MyDecimal::format(100*$cp->getRate1(),",",".")."%",
  MyDecimal::format(100*$cp->getRate1(),",",".")."%",
  MyDecimal::format(100*$cp->getRate1(),",",".")."%",
  MyDecimal::format(100*$cp->getRate1(),",",".")."%",
  "",
);
$contents[]=array(
  "",
  "Commission:",
  "",
  "",
  MyDecimal::format($grand_total*$cp->getRate1(),",","."),
  MyDecimal::format($pumps_total*$cp->getRate1(),",","."),
  MyDecimal::format($tanks_total*$cp->getRate1(),",","."),
  MyDecimal::format($other_total*$cp->getRate1(),",","."),
  "",
);

//---------------------------
foreach($contents as $content)
{
  $height=max(
    $pdf->getStringHeight($widths[0],$content[0])
    ,$pdf->getStringHeight($widths[1],$content[1])
    ,$pdf->getStringHeight($widths[2],$content[2])
    ,$pdf->getStringHeight($widths[3],$content[3])
    ,$pdf->getStringHeight($widths[4],$content[4])
    ,$pdf->getStringHeight($widths[5],$content[5])
    ,$pdf->getStringHeight($widths[6],$content[6])
    ,$pdf->getStringHeight($widths[7],$content[7])
    ,$pdf->getStringHeight($widths[7],$content[7])
  );      
  $pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 1, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[8], $height, $content[8], 1, 'R', 0, 1, '', '', true);
}

$contents=array();
$contents[]=array(
  "",
  "",
  "",
  "",
  "Total",
  "Pumps",
  "Tanks",
  "Not Included",
);

      
foreach($contents as $content)
{
  $height=max(
    $pdf->getStringHeight($widths[0],$content[0])
    ,$pdf->getStringHeight($widths[1],$content[1])
    ,$pdf->getStringHeight($widths[2],$content[2])
    ,$pdf->getStringHeight($widths[3],$content[3])
    ,$pdf->getStringHeight($widths[4],$content[4])
    ,$pdf->getStringHeight($widths[5],$content[5])
    ,$pdf->getStringHeight($widths[6],$content[6])
    ,$pdf->getStringHeight($widths[7],$content[7])
  );      
  $pdf->MultiCell($widths[0], $height, $content[0], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 0, 'R', 0, 1, '', '', true);
}



// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('CommissionPayment_'.$cp->getName().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

