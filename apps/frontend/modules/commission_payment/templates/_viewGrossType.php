<?php use_helper('I18N', 'Date'); ?>
<h1>Commission Payment: <?php echo $cp->getName()?> </h1>
<h3>Commission Type: Gross Based </h3>
Salesman: <?php echo $cp->getEmployee()?>
<br>Commission: <?php echo MyDecimal::format($cp->getCommission())?>
<br>Paid: <?php echo MyDecimal::format($cp->getCommission()-$cp->getBalance())?>
<br>Balance: <?php echo MyDecimal::format($cp->getBalance())?>
<br>
<br>Date Created: <?php echo MyDateTime::frommysql($cp->getDateCreated())->toshortdate()?>
<!--br>Date Paid: <?php //echo $cp->getDatePaid()?>-->
<br>Notes: <?php echo $cp->getNotes();?>
<br>
<br><?php echo link_to('Print','commission_payment/viewPdf?id='.$cp->getId()) ?>
<?php if($sf_user->hasCredential(array('admin','encoder'),false)){?>
<br><?php echo link_to('Delete','commission_payment/delete?id='.$cp->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
<?php } ?>

<?php
$grandtotalpurchase=0;
$grandtotalprofit=0;
$unaccountedsaletotal=0;
?>
<br>
<?php echo link_to("Recalculate","commission_payment/recalculate?id=".$cp->getId());?>

<hr>
<h3>Vouchers</h3>
<table>
  <tr>
    <td>
      <?php 
        $vouchers=$cp->getVouchers();
        foreach($vouchers as $voucher)
          echo link_to($voucher->getNo(),"voucher/view?id="
            .$voucher->getId())." P".MyDecimal::format($voucher->getAmount())
            .($voucher->isCancelled()?" <font color=red>(Cancelled)</font>":"")
            ." ".$voucher->getParticulars()
            ."<br>";
        if($sf_user->hasCredential(array('admin','encoder'), false) and $cp->getBalance()>0) echo link_to("Generate Check Voucher","voucher/new?commission_payment_id=".$cp->getId());
        ?>
    </td>
  </tr>
</table>


<?php if($sf_user->hasCredential(array('admin','encoder'),false)){?>
<hr>
<h2>Quick Input</h2>
<?php echo form_tag("commission_payment/quickInput1")?>
<input type=hidden name=id value=<?php echo $cp->getId()?>>
<textarea name=invnos cols=40 rows=5>
</textarea>
<br><input type=submit>
</form>
<?php } ?>

<hr>
<h2>Invoices</h2>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Invoice</td>
    <td>Product</td>
    <td>Qty</td>
    <td>Sell Price</td>
    <td>Total<br>Sale</td>
  	<td>Invoice<br>Total</td>
  	<td>Tanks</td>
  	<td>Pumps</td>
  	<td>Not<br>Included</td>
  	<td>Customer</td>
    <td></td>
  </tr>
  <?php foreach($invoices as $invoice){?>
  <tr>
    <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
    <td><?php echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td align=right><b><?php $saletotal+=$invoice->getTotal(); echo MyDecimal::format($invoice->getTotal()); ?></b></td>
    <td></td>
    <td></td>
    <td></td>
    <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomer())?></td>
    <td><?php if($sf_user->hasCredential(array('admin'),false))echo link_to("Remove","commission_payment/removeInvoice?invoice_id=".$invoice->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?'))?></td>
  </tr>
  <?php foreach($invoice->getInvoicedetail() as $detail)if(!$detail->getIsVat()){
      $profitdetails=$detail->getProfitdetail(); //echo count($pd);
      $product=$detail->getProduct();
      $skip=false;
      if($product->getIsTax())$skip=true;//do not include withholding tax and other "services"
      $employee=$cp->getEmployee();
      //if employee is technician, show only services
      if($employee->getIsTechnician()){if(!$product->getIsService())$skip=true;}
      //if employee is salesman, show only products
      else{if($product->getIsService())$skip=true;}
      //-------------PROCESS INVOICEDETAILS----------------
      //use only remaining, not qty
      //why???
      // if($detail->getRemaining()!=0)
      {
        //if skip, show only product, no prices, and do not include in totals calculation
        if($skip)
        {
?>
  <tr>
  	<td></td>
  	<td></td>
    <td><?php echo $product ?></td>
    <td><?php $qty=intval($detail->getQty());echo $qty; ?></td>
    <td align=right><?php $sellprice=$detail->getTotal()/$qty; echo MyDecimal::format($sellprice); ?></td>
    <td align=right><?php $totalsellprice=$detail->getTotal(); echo MyDecimal::format($totalsellprice); ?></td>
  	<td></td>
  	<td></td>
  	<td></td>
  	<td align=right><?php $totalsellprice=$detail->getTotal();echo MyDecimal::format($totalsellprice);?></td>
  	<td></td>
    <td></td>
  </tr>
<?php } else { ?>
  <tr>
  	<td></td>
  	<td></td>
    <td><?php echo $product ?></td>
    <td><?php $qty=intval($detail->getQty());echo $qty; ?></td>
    <td align=right><?php $sellprice=$detail->getTotal()/$qty; echo MyDecimal::format($sellprice); ?></td>
    <td align=right><?php $totalsellprice=$detail->getTotal(); echo MyDecimal::format($totalsellprice); ?></td>
  	<td></td>
  	<td align=right><?php if($product->getProductcategory()=='TJL'){echo MyDecimal::format($totalsellprice);}?></td>
  	<td align=right><?php if($product->getProductcategory()!='TJL'){echo MyDecimal::format($totalsellprice);}?></td>
  	<td></td>
  	<td></td>
    <td></td>
  </tr>
      <?php }
      }// end if($detail->getRemaining()!=0)
      //------------PROCESS PROFITDETAILS-----------------
      /*
      foreach($profitdetails as $pd)
      {
      ?>
  <tr>
  	<td></td>
  	<td></td>
    <td><?php echo $product ?></td>
    <td><?php $qty=intval($pd->getQty());echo $qty; ?></td>
    <td align=right><?php $sellprice=$detail->getTotal()/$qty; echo MyDecimal::format($sellprice); ?></td>
    <td align=right><?php $totalsellprice=$detail->getTotal(); echo MyDecimal::format($totalsellprice); ?></td>
  	<td></td>
  	<td align=right><?php if($product->getProductcategory()=='TJL'){echo MyDecimal::format($totalsellprice);$tjl_total+=$totalsellprice;}?></td>
  	<td align=right><?php if($product->getProductcategory()!='TJL'){echo MyDecimal::format($totalsellprice);$importation_total+=$totalsellprice;}?></td>

  </tr>
      <?php
      }//end foreach($profitdetails as $pd)
      */
      //----------END PROCESS INVOICEDETAILS AND PROFITDETAILS-----------
  ?>
  <?php } //end foreach($invoice->getInvoicedetail() as $detail)?>
  <?php } //end foreach($invoices as $invoice)?>

  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  	<td></td>
  	<td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=2>Total Sale:</td>
    <td align=right><?php echo MyDecimal::format($cp->getBase1()+$cp->getBase2())?></td>
    <td align=right><?php echo MyDecimal::format($cp->getBase1())?></td>
    <td align=right><?php echo MyDecimal::format($cp->getBase2())?></td>
    <td align=right><?php //echo MyDecimal::format($other_total)?></td>
  	<td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=2>Commission Rate:</td>
    <td align=right><?php echo MyDecimal::format($cp->getRate1()*100)?>%</td>
    <td></td>
    <td></td>
  	<td></td>
  	<td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan=2>Total Commission:</td>
    <td align=right><b><?php echo MyDecimal::format($cp->getCommission(),",",".");?></b></td>
    <td align=right><?php echo MyDecimal::format($cp->getRate1()*$cp->getBase1())?></td>
    <td align=right><?php echo MyDecimal::format($cp->getRate1()*$cp->getBase2())?></td>
  	<td></td>
  	<td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  	<td>Tanks</td>
  	<td>Pumps</td>
  	<td>Not<br>Included</td>
  	<td></td>
    <td></td>
  </tr>
</table>


