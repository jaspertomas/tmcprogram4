<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("L", PDF_UNIT, "FOLIO", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Tradewind Mdsg Corp Commission Report - '.$cp->getName());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 20, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->startPageGroup();
$pdf->AddPage();

$pdf->write(0,"Commission Report: ".$cp->getEmployee().": ".MyDateTime::frommysql($cp->getStartdate())->toshortdate()." to ".MyDateTime::frommysql($cp->getStartdate())->toshortdate(),'',false,'',true,0,false,false,0,0);

$pdf->SetFont('dejavusans', '', 10, '', true);

//header
$contents=array();
$contents[]=array(
  "Date",
  "Invoice",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
);
$contents[]=array(
  "",
  "Product",
  "Qty",
  "Sell Price",
  "Buy Price",
  "Profit",
  "Profit Rate",
  "Total Profit",
  "Unaccounted Sale"
  );
$widths=array(30,80,20,30,30,20,30,30,30);
$height=1;
foreach($contents as $content)
{
  $height=max(
    $pdf->getStringHeight($widths[0],$content[0])
    ,$pdf->getStringHeight($widths[1],$content[1])
    ,$pdf->getStringHeight($widths[2],$content[2])
    ,$pdf->getStringHeight($widths[3],$content[3])
    ,$pdf->getStringHeight($widths[4],$content[4])
    ,$pdf->getStringHeight($widths[5],$content[5])
    ,$pdf->getStringHeight($widths[6],$content[6])
    ,$pdf->getStringHeight($widths[7],$content[7])
    ,$pdf->getStringHeight($widths[8],$content[8])
  );      
  $pdf->MultiCell($widths[0], $height, $content[0], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[8], $height, $content[8], 0, 'C', 0, 1, '', '', true);
}

//body
//-------------------
$grandtotalpurchase=0;
$grandtotalprofit=0;
$unaccountedsaletotal=0;

$contents=array();
foreach($invoices as $invoice){
  $contents[]=array(
    $invoice->getDate(),
    $invoice,
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  );
  foreach($invoice->getInvoicedetail() as $detail)if(!$detail->getIsVat()){
    if($detail->getProduct()->getIsService()==0)//if product is service, ignore
    {
      $profitdetails=$detail->getProfitdetail(); //echo count($pd);
      //-------------PROCESS INVOICEDETAILS----------------
      //use only remaining, not qty
      if($detail->getRemaining()!=0)
      {
        //calc sellprice
        $sellprice=$detail->getPrice();
        //calc buyprice
        $buyprice=$detail->getProduct()->getMaxbuyprice();if($buyprice==0)$buyprice=$detail->getProduct()->getMinbuyprice(); 
          $grandtotalpurchase+=$buyprice*$detail->getRemaining();
        //calc profit
        if($buyprice==0)$profit=0; else $profit=$sellprice-$buyprice; 
        //calc profitrate
        if($buyprice==0)$profitrate=0;else $profitrate=$profit/$buyprice*100; 

        //special profit rate for specific product types
        $totalprofit="";
        $saletotal="";
        if($buyprice!=0){
          //see if product's type is in array of product type ids with special minimum profit rates
          $index=array_search($detail->getProduct()->getProducttypeId(), $sf_data->getRaw('producttype_ids'));
          //if minprofitrate contains a producttype id
          if ($index)
          {
            $minprofitrate=$minprofitrates[$index];
          }
          //else use default
          else
          {
            $minprofitrate=$defaultminimumprofitrate;
          }
          
          //if profit rate is equal or above minimum or if it is below 0 (negative profit)
          //include it to commission
          if($profitrate>=$minprofitrate or $profitrate<0)
          {
            $totalprofit=$profit*$detail->getRemaining(); 
            $grandtotalprofit+=$totalprofit;
            $totalprofit=MyDecimal::format($totalprofit,",","."); //print
          }
          else
          {
            $totalprofit="0.00"; //print
          } 
          // ---------end special min profit rates-------------
        
        }else{ 
          $saletotal=$sellprice*$detail->getRemaining(); 
          $unaccountedsaletotal+=$saletotal;
          $saletotal=MyDecimal::format($saletotal,",",".");  //print
        } 

        $contents[]=array(
          "",
          $detail->getProduct(),
          intval($detail->getRemaining()),
          $sellprice,
          $buyprice,
          MyDecimal::format($profit,",","."),
          MyDecimal::format($profitrate)."%",
          $totalprofit,
          $saletotal,
        );

      }// end if($detail->getRemaining()!=0)

      //------------PROCESS PROFITDETAILS-----------------
      foreach($profitdetails as $pd)
      {
        //calc sellprice
        $sellprice=$detail->getPrice();
        //calc buyprice
        $buyprice=$pd->getBuyprice(); $grandtotalpurchase+=$buyprice*$pd->getQty();
        //calc profit
        $profit=$sellprice-$buyprice; 
        //calc profit rate
        if($buyprice!=0){
          $profitrate=intval($pd->getProfitrate()*100); 

          $totalprofit="";
          $saletotal="";
          // ---------special min profit rates-------------
          //see if product's type is in array of product type ids with special minimum profit rates
          $index=array_search($detail->getProduct()->getProducttypeId(), $sf_data->getRaw('producttype_ids'));
          //if minprofitrate contains a producttype id
          if ($index)
          {
            $minprofitrate=$minprofitrates[$index];
          }
          //else use default
          else
          {
            $minprofitrate=$defaultminimumprofitrate;
          }
          
          //if profit rate is equal or above minimum or if it is below 0 (negative profit)
          //include it to commission
          if($profitrate>=$minprofitrate or $profitrate<0)
          {
            $totalprofit=$profit*$pd->getQty(); 
            $grandtotalprofit+=$totalprofit;
            $totalprofit=MyDecimal::format($totalprofit,",","."); //print 
          }
          else
          {
            $totalprofit="0.00"; //print
          } 
          // ---------end special min profit rates-------------
        }else{ 
          $saletotal=$sellprice*$pd->getQty(); 
          $unaccountedsaletotal+=$saletotal;
          $saletotal=MyDecimal::format($saletotal,",",".");  //print
        } 
      
        $contents[]=array(
          "",
          $detail->getProduct(),
          intval($pd->getQty()),
          $sellprice,
          $buyprice,
          MyDecimal::format($profit,",","."),
          MyDecimal::format($profitrate)."%",
          $totalprofit,
          $saletotal,
        );
  	
  	
      }//end foreach($profitdetails as $pd)
      //----------END PROCESS INVOICEDETAILS AND PROFITDETAILS-----------
    }//end if($detail->getProduct()->getIsService()==0)
  
  } //end foreach($invoice->getInvoicedetail() as $detail)
  } //end foreach($invoices as $invoice)
    
foreach($contents as $content)
{
  $height=max(
    $pdf->getStringHeight($widths[0],$content[0])
    ,$pdf->getStringHeight($widths[1],$content[1])
    ,$pdf->getStringHeight($widths[2],$content[2])
    ,$pdf->getStringHeight($widths[3],$content[3])
    ,$pdf->getStringHeight($widths[4],$content[4])
    ,$pdf->getStringHeight($widths[5],$content[5])
    ,$pdf->getStringHeight($widths[6],$content[6])
    ,$pdf->getStringHeight($widths[7],$content[7])
    ,$pdf->getStringHeight($widths[8],$content[8])
  );      
  $pdf->MultiCell($widths[0], $height, $content[0], 1, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 1, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[8], $height, $content[8], 1, 'R', 0, 1, '', '', true);
}

$pdf->write(0,"",'',false,'',true,0,false,false,0,0);
    
//footer    
$widths=array(30,80,20,20,20,10,10,80,30);
  $contents=array();
  $contents[]=array(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Total Profit:",
    MyDecimal::format($grandtotalprofit,",","."),
  );
  $contents[]=array(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Total Purchase:",
    MyDecimal::format($grandtotalpurchase,",","."),
  );

  $profitrate=$grandtotalprofit/$grandtotalpurchase*100; 
  $contents[]=array(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Profit Rate:",
    MyDecimal::format($profitrate,",",".")."%",
  );
  $contents[]=array(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Total Unaccounted Sale:",
    MyDecimal::format($unaccountedsaletotal,",","."),
  );
  $profitcommission=$grandtotalprofit*.75*$cp->getEmployee()->getCommission()/100; 
  $contents[]=array(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Profit Commission:",
    MyDecimal::format($profitcommission,",","."),
  );
  $unaccountedcommission=$unaccountedsaletotal*$profitrate/100*.75*$cp->getEmployee()->getCommission()/100;
  $contents[]=array(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Unaccounted Sale Commission:",
    MyDecimal::format($unaccountedcommission,",","."),
  );
  $contents[]=array(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Total Commission:",
    MyDecimal::format($profitcommission+$unaccountedcommission,",","."),
  );

//---------------------------
foreach($contents as $content)
{
  $height=max(
    $pdf->getStringHeight($widths[0],$content[0])
    ,$pdf->getStringHeight($widths[1],$content[1])
    ,$pdf->getStringHeight($widths[2],$content[2])
    ,$pdf->getStringHeight($widths[3],$content[3])
    ,$pdf->getStringHeight($widths[4],$content[4])
    ,$pdf->getStringHeight($widths[5],$content[5])
    ,$pdf->getStringHeight($widths[6],$content[6])
    ,$pdf->getStringHeight($widths[7],$content[7])
    ,$pdf->getStringHeight($widths[8],$content[8])
  );      
  $pdf->MultiCell($widths[0], $height, $content[0], 0, 'C', 0, 0, '', '', true);
  $pdf->MultiCell($widths[1], $height, $content[1], 0, 'L', 0, 0, '', '', true);
  $pdf->MultiCell($widths[2], $height, $content[2], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[3], $height, $content[3], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[4], $height, $content[4], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[5], $height, $content[5], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[6], $height, $content[6], 0, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[7], $height, $content[7], 1, 'R', 0, 0, '', '', true);
  $pdf->MultiCell($widths[8], $height, $content[8], 1, 'R', 0, 1, '', '', true);
}



// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('DSR-'.MyDateTime::frommysql($cp->getStartdate())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

