<?php use_helper('I18N', 'Date'); ?>

<h2><?php echo link_to("Back","commission_payment/view?id=".$cp->getId())?></h2>
<h1>Commission Payment Quick Input Result</h1>
Salesman: <?php echo $cp->getEmployee()?>
<br>Date Created: <?php echo MyDateTime::frommysql($cp->getDateCreated())->toshortdate()?>
<br>Date Paid: <?php echo $cp->getDatePaid()?>

<hr>
<b>Succesfully Added: <?php echo count($added)?></b>
<?php foreach($added as $result){echo "<br>".$result;} ?>

<hr>
<b>Already Included: <?php echo count($belongs)?></b>
<?php foreach($belongs as $result){echo "<br>".$result;} ?>

<hr>
<b>Belongs to Another Commission Payment: <?php echo count($taken)?></b>
<?php foreach($taken as $result){echo "<br>".$result;} ?>

<hr>
<b>Invoice Not Found: <?php echo count($notfound)?></b>
<?php foreach($notfound as $result){echo "<br>".$result;} ?>

<hr>
<b>Salesman Mismatch: <?php echo count($mismatch)?></b>
<?php foreach($mismatch as $result){echo "<br>".$result;} ?>
