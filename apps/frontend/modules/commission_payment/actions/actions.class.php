<?php

require_once dirname(__FILE__).'/../lib/commission_paymentGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/commission_paymentGeneratorHelper.class.php';

/**
 * commission_payment actions.
 *
 * @package    sf_sandbox
 * @subpackage commission_payment
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class commission_paymentActions extends autoCommission_paymentActions
{
  public function executeView(sfWebRequest $request)
  {
    $this->commission_payment = $this->getRoute()->getObject();
    $this->cp=$this->commission_payment;
    
    $this->invoices=Doctrine_Query::create()
      // ->from('Invoice i, i.Invoicedetail id, id.Product p, id.Profitdetail pd, pd.Purchasedetail purd')
      ->from('Invoice i, i.Customer c')
    	->where('i.commission_payment_id = '.$this->cp->getId())
      ->orderBy('c.name')
    	->execute();

    	
    //special minimum profit rates for certain product types
    //tanks: 12% minimum profit rate (if profit rate is equal to or above 12%, give commission)
    $this->producttype_ids=explode(",",SettingsTable::fetch('commission_min_profit_product_type_ids'));
    $this->minprofitrates=explode(",",SettingsTable::fetch('commission_min_profit_rates'));
    $this->defaultminimumprofitrate=SettingsTable::fetch('commission_default_min_profit_rate');

    if($this->cp->getType()=="Gross")
    {
      $tjl_total=0;
      $importation_total=0;
      $other_total=0;
      foreach($this->invoices as $invoice)
      {
        foreach($invoice->getInvoicedetail() as $detail)if(!$detail->getIsVat())
        {
          // $profitdetails=$detail->getProfitdetail(); //echo count($pd);
          $product=$detail->getProduct();
          $skip=false;
          if($product->getIsTax())$skip=true;//do not include withholding tax and other "services"
          $employee=$this->cp->getEmployee();
          //if employee is technician, show only services
          if($employee->getIsTechnician()){if(!$product->getIsService())$skip=true;}
          //if employee is salesman, show only products
          elseif($product->getIsService())$skip=true;
            //if skip, show only product, no prices, and do not include in totals calculation
          if($skip)
          {
            $totalsellprice=$detail->getTotal();
            $other_total+=$totalsellprice;
          } else { 
            $totalsellprice=$detail->getTotal(); 
            if($product->getProductcategory()=='TJL')$tjl_total+=$totalsellprice;
            if($product->getProductcategory()!='TJL')$importation_total+=$totalsellprice;
          }
        }
      }
      $vouchers=$this->cp->getVouchers();
      $totalpayment=0;
      foreach($vouchers as $voucher)
        $totalpayment+=$voucher->getAmount();
      $this->tjl_total=$tjl_total;
      $this->importation_total=$importation_total;
      $this->other_total=$other_total;
      $this->total_commission=($tjl_total+$importation_total)*$this->cp->getRate1();
      $this->totalpayment=$totalpayment;
      $this->balance=$this->total_commission-$totalpayment;
    }
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $this->commission_payment=$this->getRoute()->getObject();
    if ($this->commission_payment->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('@commission_payment');
  }
  public function executeQuickInput1(sfWebRequest $request)
  {
    $this->commission_payment = Fetcher::fetchOne("CommissionPayment",array("id"=>$request->getParameter('id')));
    $this->cp=$this->commission_payment;

    $this->invnos=explode("\n",$request->getParameter('invnos'));
    $this->notfound=array();
    $this->taken=array();
    $this->belongs=array();
    $this->added=array();
    $this->mismatch=array();

    //for each invno, load invoice and run tests, then add to commission payment
    foreach($this->invnos as $invno)
    {
      $invno=trim($invno);
      if($invno=="")continue;
      if(strpos($invno," ")!=false)continue;

      $invoice = Fetcher::fetchOne("Invoice",array("invno"=>'"'.$invno.'"'));

      //if invoice not found
      if($invoice==null)
      {
        $this->notfound[]="$invno not found";
        continue;
      }

      //invoice already belongs to a commission payment 
      if($invoice->getCommissionPaymentId()!=null)
      {
        if($invoice->getCommissionPaymentId()==$this->cp->getId())
          $this->belongs[]="$invno already belongs to this commission payment";
        else
          $this->taken[]="$invno already belongs to commission payment ".$invoice->getCommissionPaymentId();
        continue;
      }

      //if invoice salesman does not match cp salesman, error
      if($invoice->getSalesmanId()!=$this->cp->getEmployeeId())
      {
        $salesman = Fetcher::fetchOne("Employee",array("id"=>$invoice->getSalesmanId()));
        $this->mismatch[]="$invno belongs to ".$salesman->getName();
        continue;
      }

      $invoice->setCommissionPaymentId($this->cp->getId());
      $invoice->save();
      $this->added[]="$invno added";
    }
    $this->cp->recalculate();
  }
  public function executeRecalculate(sfWebRequest $request)
  {
    $this->commission_payment = Fetcher::fetchOne("CommissionPayment",array("id"=>$request->getParameter('id')));
    $this->commission_payment->recalculate();
    $this->redirect($request->getReferer());
  }
  public function executeRemoveInvoice(sfWebRequest $request)
  {
    $this->invoice = Fetcher::fetchOne("Invoice",array("id"=>$request->getParameter('invoice_id')));
    $this->commission_payment=$this->invoice->getCommissionPayment();
    $this->invoice->setCommissionPaymentId(null);
    $this->invoice->save();
    $this->commission_payment->recalculate();
    $this->redirect($request->getReferer());
  }
  public function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $commission_payment = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $commission_payment)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@commission_payment_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect("commission_payment/view?id=".$commission_payment->getId());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
}
