<h1>Quota Dashboard</h1>
<table border=1>
<tr>
	<td>Id</td>
	<td>Name</td>
	<td>Description</td>
	<td>TMC Stock</td>
	<td>Quota</td>
	<td>Reorder Qty</td>
	<td>Edit</td>
	<td>Price List</td>
	<td>Transactions</td>
</tr>
<?php foreach($stocks as $stock){$product=$stock->getProduct();?>
<tr>
	<td><?php echo $product->getId()?></td>
	<td><?php echo link_to($product->getName(),"product/view?id=".$product->getId()) ?></td>
	<td><?php echo $product->getDescription()?></td>
	<td align=right><?php if($stock)echo link_to($stock->getCurrentQty(),"product/inventory?id=".$product->getId());?></td>
	<td align=right <?php if($product->getQuota()>0 and $stock->getCurrentQty()<$product->getQuota()){if($stock->getCurrentQty()<=0)echo "bgcolor=red";else echo "bgcolor=pink";}?>><?php echo $product->getQuota();?></td>
	<td align=right <?php $reorder=$product->getQuota()-$stock->getCurrentQty()?>><?php if($reorder>0)echo $reorder;?></td>
	<td><?php echo link_to("Edit","product/edit?id=".$product->getId());?></td>
	<td><?php echo link_to("Price List","producttype/view?id=".$product->getProducttypeId());?></td>
	<td><?php echo link_to("Transactions","product/view?id=".$product->getId());?></td>
</tr>
<?php	} ?>
</table>

