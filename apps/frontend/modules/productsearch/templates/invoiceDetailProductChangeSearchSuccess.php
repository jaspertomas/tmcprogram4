<?php slot('invoice_detail_id', $invoice_detail_id) ?>
<h3>Choose new product</h3>
<table border=1>
<tr>
	<td>Id</td>
	<td>Name</td>
	<td>Description</td>
	<?php /*
	<td>Max Sale</td>
	<td>Min Sale</td>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'cashier'), false)){//for admin only?>
	<td>Max Vendor</td>
	<td>Min Vendor</td>
<?php } ?>

	<td>TMC Stock</td>
	<td>Quota</td>
	<td>Edit</td>
	<td>Price List</td>
	<td>Transactions</td>
	*/ ?>
</tr>
<?php foreach($products as $product){@$stock=$stockarray[$product->getId()];?>
<tr>
	<td><?php echo $product->getId()?></td>
	<td><?php echo link_to($product->getName(),"invoicedetail/changeProduct?id=".$invoice_detail_id."&product_id=".$product->getId()."&searchstring=".$searchstring);?></td>
	<td><?php echo $product->getDescription()?></td>
	<?php /*
	<td align=right><?php echo $product->getMaxsellprice()?></td>
	<td align=right><?php echo $product->getMinsellprice()?></td>
<?php if($sf_user->hasCredential(array('admin', 'encoder', 'cashier'), false)){//for admin only?>
	<td align=right><font color=gray><?php echo $product->getMaxbuyprice()?></font></td>
	<td align=right><font color=gray><?php echo $product->getMinbuyprice()?></font></td>
<?php } ?>
	<td align=right><?php if($stock)echo link_to($stock->getCurrentQty(),"product/inventory?id=".$product->getId());?></td>
	<td align=right <?php if($product->getQuota()>0 and $stock and $stock->getCurrentQty()<$product->getQuota()){if($stock->getCurrentQty()<=0)echo "bgcolor=red";else echo "bgcolor=pink";}?>><?php echo $product->getQuota();?></td>
	<td><?php echo link_to("Edit","product/edit?id=".$product->getId());?></td>
	<td><?php echo link_to("Price List","producttype/view?id=".$product->getProducttypeId());?></td>
	<td><?php echo link_to("Transactions","product/view?id=".$product->getId());?></td>
	*/ ?>
</tr>
<?php } ?>
</table>

