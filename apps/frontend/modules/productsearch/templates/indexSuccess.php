<?php slot('transaction_id', $transaction_id) ?>
<?php slot('transaction_type', $transaction_type) ?>
<h1>Product Search</h1>
<table border=1>
<tr>
  <?php if($transaction_type=="Invoice"
		or $transaction_type=="Purchase"
		or $transaction_type=="Quotation"
		or $transaction_type=="Returns"
		or $transaction_type=="Replacement"
		or $transaction_type=="JobOrder"
		or $transaction_type=="Transfer"
		or $transaction_type=="Delivery"){?>
	<td></td>
	<?php }?>
	<td>Id</td>
	<td>Name</td>
	<td>Description</td>
	<td>Stock</td>
	<td>Warehouse</td>
  <?php if($transaction_type=="Invoice" or $transaction_type=="Purchase" or $transaction_type=="Quotation" or $transaction_type=="Returns" or $transaction_type=="Replacement" or $transaction_type=="Delivery" or $transaction_type==""){?>
	<td>Max Sell Price</td>
	<td>Min Sell Price</td>

<?php if($sf_user->hasCredential(array('admin', 'encoder', 'cashier'), false)){//for admin only?>
	<td>Max Buy Price</td>
	<td>Min Buy Price</td>
<?php } ?>
	<td>Quota</td>
	<td>Edit</td>
	<td>Price List</td>
	<td>Transactions</td>
	<td>Unpaid</td>
<?php } ?>
</tr>
<?php 
	foreach($products as $product)
	{
		foreach($stockarray[$product->getId()] as $stock)
		{
			//if not main warehouse, and if qty = 0, do not display
			if(
				$stock->getWarehouseId()!=$default_warehouse_id && 
				$stock->getCurrentQty()==0)continue;
?>
<tr>
  <?php if($transaction_type=="Quotation"){?>
	<td><?php echo link_to("Use in Quotation","quotation/view?id=".$transaction_id."&product_id=".$product->getId()."&searchstring=".$searchstring);?></td>
  <?php }elseif($transaction_type=="Delivery"){?>
	<td><?php echo link_to("Use in DR","delivery/view?id=".$transaction_id."&product_id=".$product->getId()."&searchstring=".$searchstring);?></td>
  <?php }elseif($transaction_type=="Conversion"){?>
	<td><?php echo link_to("Use in Conversion","conversion/view?id=".$transaction_id."&product_id=".$product->getId()."&searchstring=".$searchstring);?></td>

  <?php }elseif($transaction_type=="Invoice"){?>
	<?php if($product->getIsHidden()==1 || $stock->getWarehouseId()!=$default_warehouse_id){?>
		<td></td>
	<?php }else{?>
		<td><button class="selectbutton" product_name="<?php echo $product->getName()?>" product_id="<?php echo $product->getId()?>" price="<?php echo $product->getMaxsellprice()?>" min_price="<?php echo $product->getMinsellprice()?>" allow_zeroprice="<?php echo $product->getIsAllowZeroprice()?"true":"false"?>">Choose</button></td>
	<?php }?>

  <?php }elseif($transaction_type=="Purchase"){?>
	<?php if($product->getIsHidden()==1 || $stock->getWarehouseId()!=$default_warehouse_id){?>
		<td></td>
	<?php }else{?>
		<td><button class="selectbutton" product_name="<?php echo $product->getName()?>" product_id="<?php echo $product->getId()?>" price="<?php echo $product->getMaxbuyprice()?>" min_price="<?php echo $product->getMinbuyprice()?>" curr_qty="<?php echo $stock->getCurrentQty()?>" allow_zeroprice="<?php echo $product->getIsAllowZeroprice()?"true":"false"?>">Choose</button></td>
	<?php }?>

  <?php }elseif($transaction_type=="Returns" or $transaction_type=="Replacement"){?>
	<td><button class="selectbutton" product_name="<?php echo $product->getName()?>" product_id="<?php echo $product->getId()?>" price="<?php echo $product->getMaxsellprice()?>" min_price="<?php echo $product->getMinsellprice()?>">Choose</button></td>

	<?php }elseif($transaction_type=="JobOrder"){?>
	<td><button class="selectbutton" product_name="<?php echo $product->getName()?>" product_id="<?php echo $product->getId()?>" >Choose</button></td>

  <?php }elseif($transaction_type=="Transfer"){?>
	<td><button class="selectbutton" product_name="<?php echo $product->getName()?>" product_id="<?php echo $product->getId()?>" >Choose</button></td>

  <?php }else{?>
	<!--no choose button-->
  <?php }?>

<?php if($stock->getWarehouseId()!=$default_warehouse_id){?>
	<!--not main warehouse-->
	<td></td>
	<td><?php echo link_to($product->getName().($product->getIsHidden()==1?" (Discontinued)":""),"stock/view?id=".$stock->getId()) ?></td>
	<td></td>
	<td align=right><?php if($stock)echo link_to($stock->getCurrentQty(),"stock/view?id=".$stock->getId());?></td>
	<td align=right><?php if($stock)echo $stock->getWarehouse();?></td>
	<?php if($transaction_type=="Invoice" or $transaction_type=="Purchase" or $transaction_type=="Quotation" or $transaction_type=="Returns" or $transaction_type=="Replacement" or $transaction_type=="Delivery" or $transaction_type=="" ){?>
	<td></td>
	<td></td>
	<?php if($sf_user->hasCredential(array('admin', 'encoder', 'cashier'), false)){//for admin only?>
	<td></td>
	<td></td>
	<?php } ?>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<?php } ?>
	<!--end not main warehouse-->
<?php }else{?>
	<!--main warehouse-->
	<td><?php echo $product->getId()?></td>
	<td><?php echo link_to($product->getName().($product->getIsHidden()==1?" (Discontinued)":""),"stock/view?id=".$stock->getId()) ?></td>
	<td><?php echo $product->getDescription()?></td>
	<td align=right><?php if($stock)echo link_to($stock->getCurrentQty(),"stock/view?id=".$stock->getId());?></td>
	<td align=right><?php if($stock)echo $stock->getWarehouse();?></td>
  <?php if($transaction_type=="Invoice" or $transaction_type=="Purchase" or $transaction_type=="Quotation" or $transaction_type=="Returns" or $transaction_type=="Replacement" or $transaction_type=="Delivery" or $transaction_type=="" ){?>
	<td align=right><?php echo $product->getMaxsellprice()?></td>
	<td align=right><?php echo $product->getMinsellprice()?></td>
  <?php if($sf_user->hasCredential(array('admin', 'encoder', 'cashier'), false)){//for admin only?>
	<td align=right><font color=gray><?php echo $product->getMaxbuyprice()?></font></td>
	<td align=right><font color=gray><?php echo $product->getMinbuyprice()?></font></td>
  <?php } ?>
	<td align=right <?php if($product->getQuota()>0 and $stock and $stock->getCurrentQty()<$product->getQuota()){if($stock->getCurrentQty()<=0)echo "bgcolor=red";else echo "bgcolor=pink";}?>><?php echo $product->getQuota();?></td>
	<td><?php echo link_to("Edit","product/edit?id=".$product->getId());?></td>
	<td><?php echo link_to("Price List","producttype/view?id=".$product->getProducttypeId());?></td>
	<td><?php echo link_to("Transactions","product/transactionsByPage?id=".$product->getId());?></td>
	<td><?php echo link_to("Unpaid","product/transactionsUnpaid?id=".$product->getId());?></td>
  <?php } ?>
	<!--end main warehouse-->
	
<?php	} ?>
</tr>
<?php	} ?>
<?php } ?>
</table>

<script>
$(".selectbutton").click(function(event){
  var allow_zeroprice=$(this).attr("allow_zeroprice");
  var min_price=$(this).attr("min_price");
  var price=$(this).attr("price");
  var product_name=$(this).attr("product_name");
  var product_id=$(this).attr("product_id");
  var curr_qty=$(this).attr("curr_qty");
  //this is defined in parent form
  populateSubform(product_id,product_name,price,min_price,allow_zeroprice,curr_qty);
});

</script>

