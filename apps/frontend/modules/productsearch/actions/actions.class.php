<?php

/**
 * search actions.
 *
 * @package    sf_sandbox
 * @subpackage search
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class productsearchActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeInvoiceDetailProductChangeSearch(sfWebRequest $request)
  {
    $this->invoice_detail_id=$request->getParameter("invoice_detail_id");

    $this->search($request);
  }
  public function executeIndex(sfWebRequest $request)
  {
    $this->transaction_type=$request->getParameter("transaction_type");
    $this->transaction_id=$request->getParameter("transaction_id");

    $this->search($request);
    $this->default_warehouse_id=SettingsTable::fetch('default_warehouse_id');
  }
  private function search($request)
  {
    $keywords=explode(" ",$request->getParameter("searchstring"));
		//search in name
    $query=Doctrine_Query::create()
        ->from('Product p')
        ->orderBy("p.is_hidden, p.name")
      	->where('p.id != 0')
      	//->andWhere('s.product_id = p.id')
      	//->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	;

    foreach($keywords as $keyword)
    	$query->andWhere("p.name LIKE '%".$keyword."%'");

		//search in description
  	$query->orWhere('p.id != 0')
      	//->andWhere('s.product_id = p.id')
      	//->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	;

    foreach($keywords as $keyword)
    	$query->andWhere("p.description LIKE '%".$keyword."%'");

		//search in code
  	$query->orWhere('p.id != 0');
    foreach($keywords as $keyword)
    	$query->andWhere("p.code LIKE '%".$keyword."%'");

  	$this->products=$query->execute();

    $productids=array();
  	foreach($this->products as $product)
      $productids[]=$product->getId();
  	
    $stocks=Doctrine_Query::create()
        ->from('Stock s')
      	->whereIn('s.product_id', $productids)
      	// ->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	->execute();
    
    //convert into 2D array: stockarray[product_id][warehouse_id]
    $this->stockarray=array();
    foreach($stocks as $stock)
    {
      if($this->stockarray[$stock->getProductId()]==null)
        $this->stockarray[$stock->getProductId()]=array();
      
      $this->stockarray[$stock->getProductId()][$stock->getWarehouseId()]=$stock;
    }
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeUpdateProductSearch(sfWebRequest $request)
  {
    $this->purchasedetail_id=$request->getParameter("purchasedetail_id");

    $keywords=explode(" ",$request->getParameter("searchstring"));

		//search in name
    $query=Doctrine_Query::create()
        ->from('Product p')
        ->orderBy("p.name")
      	->where('p.id != 0')
      	//->andWhere('s.product_id = p.id')
      	//->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	;

    foreach($keywords as $keyword)
    	$query->andWhere("p.name LIKE '%".$keyword."%'");

		//search in description
  	$query->orWhere('p.id != 0')
      	//->andWhere('s.product_id = p.id')
      	//->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	;

    foreach($keywords as $keyword)
    	$query->andWhere("p.description LIKE '%".$keyword."%'");

  	$this->products=$query->execute();

    $productids=array();
  	foreach($this->products as $product)
      $productids[]=$product->getId();
  	
    $stocks=Doctrine_Query::create()
        ->from('Stock s')
      	->whereIn('s.product_id', $productids)
      	->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	->execute();
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeQuotaDashboard(sfWebRequest $request)
  {
    /*
    SELECT product.name, product.quota, stock.currentqty
    FROM product
    LEFT JOIN stock ON product.id = stock.product_id
    where product.quota>stock.currentqty
    */
		//search in name
    $this->stocks=Doctrine_Query::create()
        ->from('Stock s, s.Product p')
      	->where('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
      	->andWhere('p.quota > s.currentqty')
      	->andWhere('p.quota != 0')
      	->execute();

  }
}
