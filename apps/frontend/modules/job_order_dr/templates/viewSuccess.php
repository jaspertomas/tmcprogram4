<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h2><?php echo link_to("> Back to ".$job_order,"job_order/view?id=".$job_order->getId())?></h2>
<h1>
  <?php echo $job_order_dr->getCode(); ?>
  <?php if($job_order_dr->isCancelled()){?>
    (<font color=red>Cancelled</font>)
  <?php }else{?>
    (<?php echo $job_order_dr->getProductionStatusString(); ?>)
    <?php 
      if(!$job_order_dr->isProduced())
      {
        $olddateproduced=MyDateTime::fromdatetime($job_order_dr->getDatetime());
        $today=MyDateTime::now();
        echo "<br>".link_to("Produce Today (".$today->toshortdate().")","job_order_dr/produce?today=true&id=".$job_order_dr->getId());
        if($olddateproduced->tomysql()!=MyDate::today())echo "<br>".link_to("Produce ".
        $olddateproduced->toshortdate(),"job_order_dr/produce?id=".$job_order_dr->getId());
      }
      elseif($sf_user->hasCredential(array('admin'), false))
        echo link_to("Undo Produce","job_order_dr/undoProduce?id=".$job_order_dr->getId());
    ?>
  <?php }?>
</h1>
<?php slot('transaction_id', $job_order_dr->getId()) ?>
<?php slot('transaction_type', "JobOrderDr") ?>

<table>
  <tr valign=top>
    <td>
      <table>
          <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
          <?php echo form_tag_for($form,'job_order_dr/adjust')?> <?php echo $form['id'] ?>
          <?php }?>	
        <tr valign=top>
          <td>Location:</td>
          <td><?php 
            if($job_order_dr->isPending() && $sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false))
              echo $form['warehouse_id'];
            else
              echo $job_order_dr->getWarehouse()->getName();
            ?>
          </td>
        </tr>
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::fromdatetime($job_order_dr->getDatetime()) ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($job_order_dr->isPending()){?>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
            <?php echo $form['datetime'] ?>
            <?php } ?>
            <?php }?>	
          </td>
        </tr>
        <tr valign=top>
          <td >JobOrder:</td>
          <td>
            <?php if($job_order_dr->getJobOrderId())echo link_to($job_order,"job_order/view?id=".$job_order_dr->getJobOrderId())?>&nbsp;
          </td>
        </tr>
		</table>
    <td>
      <table>
        <tr>
          <td>Notes</td>
          <td><?php echo $job_order_dr->getNotes() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <textarea rows="2" cols="10" name="job_order_dr[notes]" id="job_order_dr_notes"><?php echo $job_order_dr->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <input type="submit" value="Save"></form>
            <?php }?>	
          </td>
        </tr>
		  </table>
    </td>
  </tr>
</table>

            <!--allow cancel only if not produced and not cancelled-->
            <?php if($job_order_dr->isPending())echo link_to('Cancel','job_order_dr/cancel?id='.$job_order_dr->getId(), array('confirm' => 'Cancel: Are you sure?')) ?> |
            <!--only admin can delete and undo cancel-->
            <?php if($sf_user->hasCredential(array('admin'), false) and !$job_order_dr->getIsProduced()){?>
              <!--allow undo cancel only if cancelled-->
              <?php if($job_order_dr->isCancelled())echo link_to('Undo Cancel','job_order_dr/undoCancel?id='.$job_order_dr->getId(), array('confirm' => 'Undo Cancel: Are you sure?')) ?> |
              <?php echo link_to('Delete','job_order_dr/delete?id='.$job_order_dr->getId(), array('method' => 'delete', 'confirm' => 'Delete: Are you sure?')) ?> |
            <?php } ?>
            <?php if(!$job_order_dr->isPending() && !$job_order_dr->isCancelled())echo link_to("Print","job_order_dr/viewPdf?id=".$job_order_dr->getId());?> |
            <!--allow set date to job_order date only if not produced and not cancelled-->
            <?php if($job_order_dr->isPending())echo "<br>".link_to("Set job_order date as produce date","job_order_dr/setJobOrderDateAsProduceDate?id=".$job_order_dr->getId());?> |
            <br><?php echo link_to("Recalculate","job_order_dr/recalc?id=".$job_order_dr->getId())?>
<hr>

<?php echo form_tag("job_order_dr/setQtys");?>
<input type=hidden id=id name=id value=<?php echo $job_order_dr->getId()?>>
<table border=1>
  <tr>
    <td>Qty in</td>
    <td>Qty in</td>
    <td>Qty</td>
    <td>Qty</td>
    <td>Product</td>
    <td>Qty Pending</td>
    <td>Qty Pending</td>
  </tr>
  <tr>
    <td>Job Order</td>
    <td>DR</td>
    <td>Produced</td>
    <td>Consumed</td>
    <td></td>
    <td>Production</td>
    <td>Consumption</td>
  </tr>
  <?php 
  	foreach($job_order->getDetails() as $job_orderdetail){
      $dr_detail=$dr_details[$job_orderdetail->getId()];
      if($dr_detail==null)echo "<font color=red>SOMETHING IS WRONG. PLEASE PRESS RECALCULATE";
      else $deliveryqty=$dr_detail->getQty();
  ?>
  <tr>
    <td><?php echo $job_orderdetail->getQty() ?></td>
    <td>
      <?php if($job_order_dr->isPending()){?>
      <input id=qtys[<?php echo $job_orderdetail->getId()?>] size=3 name=qtys[<?php echo $job_orderdetail->getId()?>] value=<?php echo $deliveryqty?>>
      <br>
      <?php } ?>
      <?php echo $deliveryqty;?>
    </td>
    <td align=right >
      <?php 
        if($dr_detail!=null)if($dr_detail->getIsProduced() && $deliveryqty>0)echo $deliveryqty;
      ?>
    </td>
    <td align=right >
      <?php 
        if($dr_detail!=null)if($dr_detail->getIsProduced() && $deliveryqty<0)echo $deliveryqty*-1;
      ?>
    </td>
    <td><?php echo link_to($dr_detail->getProduct(),"stock/view?id=".$dr_detail->getStock()->getId()) ?></td>
    <td align=center>
      <?php 
        //disable: ignore prodict if is_service
        //if product is service, ignore
        // if($job_orderdetail->getProduct()->isService())$remaining=0;
        // else
        $remaining=$job_orderdetail->getQty()-$job_orderdetail->getQtyProduced();
        if($remaining>0)echo "<font color=red>".$remaining."</font>";
      ?>
    </td>
    <td align=center>
      <?php 
        if($remaining<0)echo "<font color=red>".($remaining*-1)."</font>";
      ?>
    </td>
  <?php }?>
  
</table>
<?php if($job_order_dr->isPending()){?>
  <input type=submit value="Save">
<?php } ?>
</form>

<hr>
<br>
<br>
<br>
<br>


