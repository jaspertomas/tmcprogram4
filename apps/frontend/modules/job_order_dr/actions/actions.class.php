<?php

require_once dirname(__FILE__).'/../lib/job_order_drGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/job_order_drGeneratorHelper.class.php';

/**
 * job_order_dr actions.
 *
 * @package    sf_sandbox
 * @subpackage job_order_dr
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class job_order_drActions extends autoJob_order_drActions
{
  public function executeNew(sfWebRequest $request)
  {
    $job_order=Fetcher::fetchOne("JobOrder",array("id"=>$request->getParameter("job_order_id")));

    $job_order_dr = JobOrderDrTable::genDrForJobOrderId($job_order->getId(),$this->getUser()->getGuardUser());
    $this->redirect("job_order_dr/view?id=".$job_order_dr->getId());
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $job_order_dr = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $job_order_dr)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect('job_order_dr/view?id='.$job_order_dr->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

  public function executeView(sfWebRequest $request)
  {
    //fetch return by id
    $this->job_order_dr=Fetcher::fetchOne("JobOrderDr",array("id"=>$request->getParameter('id')));

    $this->form = $this->configuration->getForm($this->job_order_dr);

    $this->job_order=$this->job_order_dr->getJobOrder();

    //arrange returndetails into an array with job_order_detail as key
    $this->dr_details=$this->job_order_dr->getJobOrderDrDetailsIndexedByJobOrderDetailId();
    
    $this->warehouses=Fetcher::fetch("Warehouse",array());
    // $this->default_warehouse=Fetcher::fetch("Warehouse",array("id"=>SettingsTable::fetch("default_warehouse_id")));
    // $detail=new JobOrderDrDetail();
    // $detail->setQty(1);
    // $this->product_is_set=false;
    // $this->product=null;
    // $this->detailform = new JobOrderDrDetailForm($detail);
    // $this->detail = $detail;
    // $this->searchstring=$this->request->getParameter('searchstring');
  }
  public function executeSetQtys(sfWebRequest $request)
  {
    //fetch return by id
    $dr=Fetcher::fetchOne("JobOrderDr",array("id"=>$request->getParameter('id')));
  
    if($dr->isProduced())
    {
      $error="Cannot set qty, DR is already produced";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('job_order_dr/view?id='.$dr->getId());
    }
    if($dr->isCancelled())
    {
      $error="Cannot set qty, DR is cancelled";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('job_order_dr/view?id='.$dr->getId());
    }
  
    //arrange returndetails into an array with job_order_detail as key
    //Usage:
    //$dr_detail = $dr_details[$job_order_detail_id] 
    $dr_details=$dr->getJobOrderDrDetailsIndexedByJobOrderDetailId();
  
  
    // var_dump($request->getParameter('qtys'));die();
    foreach($request->getParameter('qtys') as $job_order_detail_id => $qty)
    {
      $job_order_detail=Fetcher::fetchOne("JobOrderDetail",array("id"=>$job_order_detail_id));
      $qty=intval($qty);
  
      //validate: qty must not be greater than remaining
      $qty_remaining=$job_order_detail->getQty()-$job_order_detail->getQtyProduced();
  
      if($job_order_detail->getQty()>0)
      {
        if($qty>$qty_remaining)
        {
          $error="Cannot produce ".$qty." units, only ".$qty_remaining." units remaining";
          $this->getUser()->setFlash('error', $error);
          return $this->redirect('job_order_dr/view?id='.$dr->getId());
        }
        else if($qty<0)
        {
          $error="Invalid quantity, pls enter positive number for final products (".$job_order_detail->getProduct()->getName().")";
          $this->getUser()->setFlash('error', $error);
          return $this->redirect('job_order_dr/view?id='.$dr->getId());
        }
      }
      else if($job_order_detail->getQty()<0)
      {
        if($qty<$qty_remaining)
        {
          $error="Cannot consume ".$qty." units, only ".$qty_remaining." units allocated";
          $this->getUser()->setFlash('error', $error);
          return $this->redirect('job_order_dr/view?id='.$dr->getId());
        }
        else if($qty>0)
        {
          $error="Invalid quantity, pls enter negative number for raw materials (".$job_order_detail->getProduct()->getName().")";
          $this->getUser()->setFlash('error', $error);
          return $this->redirect('job_order_dr/view?id='.$dr->getId());
        }
      }

  
      //if deliverydetail exists, just save the qty
      if(array_key_exists($job_order_detail_id, $dr_details))
      {
        $dr_detail=$dr_details[$job_order_detail_id];
        $dr_detail->setQty($qty);
        $dr_detail->save();
      }
      //else if deliverydetail does not exist, create it
      else 
      {
        //if qty is 0, don't do anything
        if($qty==0)continue;
      
        $dr_detail=new JobOrderDrDetail();
        $dr_detail->setJobOrderDrId($dr->getId());
        $dr_detail->setJobOrderId($dr->getJobOrderId());
        $dr_detail->setWarehouseId($dr->getWarehouseId());
  
        $dr_detail->setJobOrderDetailId($job_order_detail_id);
        $id=$job_order_detail;
        $dr_detail->setDatetime($dr->getDatetime());
        $dr_detail->setProductId($id->getProductId());
        $dr_detail->setQty($qty);
  
        $user=$this->getUser()->getGuardUser();
        $dr_detail->setCreatedById($user->getId());
        $dr_detail->setUpdatedById($user->getId());
        $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
        $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr_detail->save();
      }
    }
  
    $dr->calc();
  
    return $this->redirect("job_order_dr/view?id=".$dr->getId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
  
    $job_order_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $job_order_dr)));
  
    if ($job_order_dr->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }
  
    $this->redirect("job_order/view?id=".$job_order_dr->getJobOrderId());
  }
  public function executeCancel(sfWebRequest $request)
  {
    $job_order_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $job_order_dr)));
  
    if ($job_order_dr->cascadeCancel())
    {
      $this->getUser()->setFlash('notice', 'The item was cancelled successfully.');
    }
  
    $this->redirect("job_order_dr/view?id=".$job_order_dr->getId());
  }
  public function executeUndoCancel(sfWebRequest $request)
  {
    $job_order_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $job_order_dr)));
  
    if ($job_order_dr->cascadeUndoCancel())
    {
      $this->getUser()->setFlash('notice', 'The item was uncancelled successfully.');
    }
  
    $this->redirect("job_order_dr/view?id=".$job_order_dr->getId());
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('job_order_dr');
    $id=$requestparams['id'];
  
    $this->forward404Unless(
    $this->job_order_dr=Doctrine_Query::create()
    ->from('JobOrderDr i')
    ->where('i.id = '.$id)
    ->fetchOne()
    , sprintf('JobOrderDr with id (%s) not found.', $request->getParameter('id')));
  
    $this->job_order_dr->setNotes($requestparams["notes"]);

    if(isset($requestparams["warehouse_id"])){
      $this->job_order_dr->setWarehouseId($requestparams["warehouse_id"]);
      Doctrine_Query::create()
        ->update('JobOrderDrDetail pdd')
        ->set('pdd.warehouse_id=?',$requestparams["warehouse_id"])
        ->where('pdd.job_order_dr_id=?', $this->job_order_dr->getId())
        ->execute();
    }
    
    //set date and adjust stock entry date if necessary
    if(isset($requestparams["datetime"]))
    {
      $newdatetime=
        $requestparams["datetime"]["year"]."-".
        $requestparams["datetime"]["month"]."-".
        $requestparams["datetime"]["day"]."-".
        $requestparams["datetime"]["hour"]."-".
        $requestparams["datetime"]["minute"];
      $this->job_order_dr->setDatetime($newdatetime);
      foreach($this->job_order_dr->getJobOrderDrDetail() as $detail)
      {
        $detail->setDatetime($newdatetime);
        $detail->save();
  
              //update stockentry
              $stockentry=$detail->getStockentry();
              $stockentry->setDatetime($newdatetime);
              $stockentry->save();
              $stockentry->getStock()->calcFromStockEntry($stockentry);
      }
      
      //no need for this, change date allowed only
      //when dr is not produced
      //stock entries will be generated when it is produced
      // $this->job_order_dr->setDateAndUpdateStockEntry($newdatetime);
    }
    
    $this->job_order_dr->save();
  
    $this->redirect($request->getReferer());
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);
  
    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeProduce(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('JobOrderDr dr')
          ->where('dr.id = '.$request->getParameter('id'))
          ->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
    
    if($request->getParameter('today')=="true")
    {
      $dr->setDatetime(MyDateTime::now()->todatetime());
      $dr->save();
    }
    $error=$dr->produce();
    if($error!=false)
    {
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('job_order_dr/view?id='.$dr->getId());
    }
  
    //todo:create history
  
    $this->redirect($request->getReferer());
  }
  public function executeUndoProduce(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('JobOrderDr dr')
          ->where('dr.id = '.$request->getParameter('id'))
          ->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
  
    $dr->undoProduce();
  
    //todo:create history
  
    $this->redirect($request->getReferer());
  }
  //this can only be called if DR is not yet produced
  public function executeSetJobOrderDateAsProduceDate(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('JobOrderDr dr')
          ->where('dr.id = '.$request->getParameter('id'))
          ->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
  
    $job_order=$dr->getJobOrder();
    $dr->setDatetime($job_order->getDate());
    $dr->save();
  
    $this->redirect($request->getReferer());
  }
  public function executeRecalc(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('JobOrderDr dr')
          ->where('dr.id = '.$request->getParameter('id'))
          ->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
    $dr->regenMissingDetails($this->getUser()->getGuardUser());
    $dr->calc();
    $this->redirect($request->getReferer());
  }
  public function executeRecalcAll(sfWebRequest $request)
  {
    echo "ACCESS DENIED";die();
    $drs=Doctrine_Query::create()
      ->from('JobOrderDr dr')
      ->execute();
    foreach($drs as $dr)
    {
      //$dr->calc();
      $job_order=$dr->getJobOrder();
      $dr->setCode(str_replace("IDR","PDR",$dr->getCode()));
      $dr->save();
      $job_order->setIsDrBased(1);
      $job_order->save();
    }
    $this->redirect("home/index");
}
}
