<h1>Regenerate Stock Entries for Invoice Details</h1>
Some invoice details are missing their stock entry. This recreates it if it is missing.
<?php 
echo form_tag("invoicedetail/regenStockentry"); 
?>
<br>If an error occurs, decrease the interval.
<br>
<br>Interval: <input name=interval value="<?php echo $interval?>">
<br>Start: <input name=start value="<?php echo $start?>" >
<br>
<br><button name=submit onclick="myFunction(); ">START</button>
</form>
<br>
<br>
<?php
foreach($invoicedetails as $invoicedetail)
{
    echo "<br>".$invoicedetail->getId();
}
?>
<script>
var count=<?php echo count($invoicedetails)?>;
//run automatically if there are invoicedetails to process
//stop automatically if there are no more invoicedetails to process
if(count!=0)document.getElementsByName("submit")[0].click();
function myFunction() {
    document.getElementsByName("submit")[0].innerHTML ="Please Wait";
}
    </script>
