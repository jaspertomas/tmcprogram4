<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<?php if(!$sf_user->hasCredential(array('admin', 'encoder'), false)){//for admin only?>
<h1>Sorry, you are not allowed to access this page</H1>
<?php die(); ?>
<?php } ?>


<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>Invoice Detail Change Product</h1>
<?php slot('invoice_detail_id', $invoicedetail->getId()) ?>

<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Inv no.</td>
          <td>
            <?php echo $invoice->getInvoiceTemplate()." ".$invoice->getInvno(); ?>
          </td>
        </tr>
        <tr>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td>Customer</td>
          <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId(),array("target"=>"edit_customer")); ?> </td>
        </tr>
        <tr>
          <td>Related DRs:</td>
          <td>
            <?php foreach($invoice->getDeliverys() as $ref){?>
              <?php echo link_to($ref,"delivery/view?id=".$ref->getId())?>&nbsp;
            <?php } ?>
          </td>
        </tr>
		</table>
    <td>
      <table>
        <tr>
          <td>Sales Rep</td>
          <td><?php echo $invoice->getEmployee() ?></td>
        </tr>
        <tr>
          <td>Total</td>
          <td><?php echo $invoice->getTotal() ?></td>
        </tr>
		    <tr>
	        <td><b>Status</b></td>
	        <td>
            <font <?php 
            
            
            switch($invoice->getStatus())
            {
              case "Paid":echo "color=blue";break;
              case "Pending":echo "color=red";break;
              case "Cancelled":echo "color=red";break;
              case "Overpaid":echo "color=red";break;
            }
            ?>>
	        <?php 
            /*
              if status=paid,
                if checkcleardate > today, 
                  status = pending. 
                else 
                  status = paid
            */
            if($invoice->getStatus()=="Paid"){
            $today=MyDateTime::today();
            $checkcleardate=MyDateTime::frommysql($invoice->getCheckcleardate());
            $status="Paid";
            if($checkcleardate->islaterthan($today))$status="Check to clear on ".$checkcleardate->toshortdate();
            echo $status;

            }
            else echo $invoice->getStatus();

            ?>
            </font>
          </td>
		    </tr>
        <tr valign=top>
          <td>Due Date: </td>
          <td>
            <?php if($invoice->getStatus()=="Pending")echo MyDateTime::frommysql($invoice->getDuedate())->toshortdate() ?>
          </td>
        </tr>
        <tr valign=top>
          <td>Collection: </td>
          <td>
            <?php if($invoice->getStatus()=="Pending"){ ?>
              <?php if(!$invoice->isDue()){ ?>
                Not yet due
              <?php }else{ ?>
                <?php echo $invoice->getCollectionStatus() ?>
              <?php } ?>
            <?php } ?>
        </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr valign=top>
          <td>Sale Type</td>
          <td><?php echo $invoice->getSaleType() ?></td>
          <td>
        </td>
        </tr>
        <tr>
          <td>Cash</td>
          <td><?php echo $invoice->getCash() ?></td>
          <td>
          </td>
        </tr>
        <tr>
          <td>Cheque/BT</td>
          <td><?php echo $invoice->getChequeamt() ?></td>
          <td>
          </td>
        </tr>
        <tr>
          <td>Balance</td>
          <td><?php echo $invoice->getCredit() ?></td>
          <td>
          </td>
        </tr>
      </table>
    </td>
    <td>
			<table>
        <tr valign=top>
          <td>Po No.</td>
          <td><?php echo $invoice->getPonumber() ?><br>
          </td>
        </tr>
        <tr>
          <td>Notes</td>
          <td><?php echo $invoice->getNotes() ?><br>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
          </td>
        </tr>
		  </table>
    </td>
  </tr>
</table>

<hr>
<h3>Invoice Detail to change</h3>
<table border=1>
  <tr>
    <td>Qty</td>
    <td>Price</td>
    <td>Product</td>
    <td>Description</td>
  </tr>
  <tr>
    <td><?php echo $invoicedetail->getQty() ?></td>
    <td><?php echo $invoicedetail->getPrice() ?></td>
    <td><?php echo link_to($invoicedetail->getProduct(),"product/view?id=".$invoicedetail->getProductId()) ?></td>
    <td><?php echo $invoicedetail->getDescription() ?></td>
</table>

<?php if(isset($product)){?>
<h3>Change product to: </h3>

<?php echo form_tag("invoicedetail/processChangeProduct");?>
<input type=hidden id=id name=id value=<?php echo $invoicedetail->getId()?>>
<input type=hidden id=product_id name=product_id value=<?php echo $product->getId()?>>
<table border=1>
  <tr>
    <td>Product</td>
    <td>Description</td>
    <td></td>
  </tr>
  <tr>
    <td><?php echo $product->getName() ?></td>
    <td><?php echo $product->getDescription() ?></td>
  </tr>
  <tr>
  </tr>
</table>
    <td><input type=submit value="Change Product"></td>
</form>

<?php } ?>
<hr>

<b>Search for new product:</b> <input id=invoiceproductsearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=invoiceclearsearch> <span id=pleasewait></span>

<hr>

<div id="invoicesearchresult"></div>

<br>
<br>
<br>
<br>
<br>
<br>

<script>
//------Invoice (not header) product search-----------
//$("#invoiceproductsearchinput").keyup(function(){
//$("#invoiceproductsearchinput").on('input propertychange paste', function() {
$("#invoiceproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#invoice_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
    if($("#invoiceproductsearchinput").val().length==0)return;
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
      var searchstring=$("#invoiceproductsearchinput").val();
      var url="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/invoiceDetailProductChangeSearch?searchstring="+searchstring+"&invoice_detail_id=<?php include_slot('invoice_detail_id') ?>";
      $.ajax({url: url, success: function(result){
   		  $("#invoicesearchresult").html(result);
      }});
    }
    //else clear
    //else
 		//  $("#invoicesearchresult").html("");
});
$("#invoiceclearsearch").click(function(){
 		  $("#invoicesearchresult").html("");
});

</script>


