<?php

require_once dirname(__FILE__).'/../lib/invoicedetailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/invoicedetailGeneratorHelper.class.php';

/**
 * invoicedetail actions.
 *
 * @package    sf_sandbox
 * @subpackage invoicedetail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoicedetailActions extends autoInvoicedetailActions
{
  //change product page - product search
  public function executeChangeProduct(sfWebRequest $request)
  {
    $this->invoicedetail=Doctrine_Query::create()
    ->from('Invoicedetail i')
  	->where('i.id = '.$request->getParameter("id"))
  	->fetchOne();
  	$this->invoice=$this->invoicedetail->getInvoice();
    $this->searchstring=$this->request->getParameter('searchstring');
    
    if($request->hasParameter('product_id'))
    {
      $this->product=Doctrine_Query::create()
      ->from('Product p')
    	->where('p.id = '.$request->getParameter("product_id"))
    	->fetchOne();
    }
  }
  //process product page
  public function executeProcessChangeProduct(sfWebRequest $request)
  {
    //fetch invoice detail
    $invoicedetail=Doctrine_Query::create()
    ->from('Invoicedetail i')
  	->where('i.id = '.$request->getParameter("id"))
    ->fetchOne();
    $drdetails=Doctrine_Query::create()
    ->from('InvoiceDrDetail i')
  	->where('i.invoicedetail_id = '.$invoicedetail->getId())
    ->execute();
    //fetch product
    $product=Doctrine_Query::create()
    ->from('Product p')
  	->where('p.id = '.$request->getParameter("product_id"))
  	->fetchOne();
    
    if($product==null)
    {
      $message="Invalid product id";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect("invoice/view?id=".$invoicedetail->getInvoiceId());
    }
    
    if($invoicedetail==null)
    {
      $message="Invalid invoice detail id";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect("invoice/view?id=".$invoicedetail->getInvoiceId());
    }
    
    //get stock of existing product
    $warehouse_id=SettingsTable::fetch("default_warehouse_id");
    //delete stock entry
    $oldstock=StockTable::fetch($warehouse_id, $invoicedetail->getProductId());
    $oldstockentry=$invoicedetail->getStockEntryDoNotCreate();
    if($oldstockentry)$oldstock->deleteEntry($oldstockentry);
    $oldproduct=$invoicedetail->getProduct();

    foreach($drdetails as $drdetail)
    {
      $oldstockentry=$drdetail->getStockEntryDoNotCreate();
      if($oldstockentry)$oldstock->deleteEntry($oldstockentry);
    }

    $oldproduct=$invoicedetail->getProduct();
    
    //get stock of new product
    //create stock entry
    $newstock=StockTable::fetch($warehouse_id, $product->getId());
    // $newstock->addEntry($oldstockentry->getDatetime(), $oldstockentry->getQty(), $oldstockentry->getRefClass(), $oldstockentry->getRefId(), $oldstockentry->getType(), $oldstockentry->getDescription());
    $invoicedetail->setProductId($product->getId());
    $invoicedetail->setDescription($product->getDescription());
    $invoicedetail->save();
    foreach($drdetails as $drdetail)
    {
      $drdetail->setProductId($product->getId());
      $drdetail->save();
      $drdetail->updateStockEntry();
    }
    
    $message="Successfully changed product from ".$oldproduct->getName()." to ".$product->getName();
    $this->getUser()->setFlash('notice', $message);
    return $this->redirect("invoice/view?id=".$invoicedetail->getInvoiceId());
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->invoicedetail = new Invoicedetail();
    $invoice_id=$request->getParameter("invoice_id");
    $this->invoicedetail->setInvoiceId($invoice_id);
    $invoice=$this->invoicedetail->getInvoice();
    $this->invoicedetail->setQty(1);
    $this->invoicedetail->setDiscrate($invoice->getDiscrate());
    $this->form = $this->configuration->getForm($this->invoicedetail);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter("invoicedetail");

    $isnew=$form->getObject()->isNew();
    
    //if edit, determine if qty or price has been edited
    //if so, processbarcode is necessary
    $qtyorpriceedited=false;
    if(!$isnew)
    {
      if($requestparams['price']!=$form->getObject()->getPrice())$qtyorpriceedited=true;
      elseif($requestparams['qty']!=$form->getObject()->getQty())$qtyorpriceedited=true;
      elseif($requestparams['discrate']!=$form->getObject()->getDiscrate())$qtyorpriceedited=true;
      elseif($requestparams['discamt']!=$form->getObject()->getDiscamt())$qtyorpriceedited=true;
    }
    
    if($requestparams["qty"]==0)
    {
      $message="Invalid Quantity";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    if(!isset($requestparams['is_discounted']))$requestparams['is_discounted']=false;
    
    //if with_commission is less than price, error    
    if($requestparams["with_commission"] < $requestparams["price"] and $requestparams["with_commission"]!=0)
    {
      $message="Price with commission cannot be lower than Price";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    //is_discounted is checkbox. if it is false, it needs to be created explicitly    
    if(!$requestparams["is_discounted"])$requestparams["is_discounted"]=0;
    else 
    {
      //validate price is lower than minprice; if not, discounted=0
      //fetch product
      $product=Doctrine_Query::create()
        ->from('Product p')
        ->where('p.id = '.$requestparams["product_id"])
        ->fetchOne();
      //validate product exists
      if(!$product)return $this->redirect("home/error?msg=Product with id ".$requestparams["product_id"]." not found");
      //apply is_discounted only if price is lower than product minprice
      if($requestparams["price"]>=$product->getMinbuyprice())$requestparams["is_discounted"]=0;
      else $requestparams["is_discounted"]=1;
    }

    $form->bind($requestparams);
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
      
        $invoicedetail = $form->save();
        $invoice=$invoicedetail->getInvoice();
        $invoicedetail->setDiscrate($invoice->getDiscountLevel()->getLess());
        //if invoice is not cancelled
        if(!$invoice->isCancelled())
        {
		      $invoicedetail->updateProduct();
  
          //if invoicedetail is created / edited while invoice is already closed
          //note: stockentry must only be created when invoice is closed
          //note: if invoice is cancelled, stock entry will not exist
          //stock is now responsibility of deliverydetail
          //dr_less_sales: stock is again responsibility of deliverydetail
          if($invoice->getIsTemporary()==0)
          {
		        $invoicedetail->updateStockentry();
		        $invoicedetail->updateProfitdetail();
          }

          //invoice conversion
          //if action is new (not edit)
          //and product->conversion_id is not null
          //create invoiceconversion
		      $product=$invoicedetail->getProduct();
          if($isnew and $product->getConversionId()!=null)
          {
            /*

            $convert=new Deliveryconversion();
            $convert->setParentClass("Invoice");
            $convert->setParentId($invoice->getId());
            //$convert->setRefId($invoicedetail->getId());
            //$convert->setRefClass($invoicedetail->getId());
            $convert->setConversionId($product->getConversionId());
            //if($invoice->getIsTemporary()==0)
              //$convert->activate();//includes save
            //else
              $convert->save();
            */
            $dc=new Deliveryconversion();
            $dc->setParentClass("Invoice");
            $dc->setParentId($invoice->getId());
            $dc->setRefClass("Invoicedetail");
            $dc->setRefId($invoicedetail->getId());
            $dc->setConversionId($product->getConversionId());
            $dc->setQty($qty);
            $dc->save();
            $dc->genDeliverydetails();
          }

        }

        //custom calculation
        if($invoicedetail->getDescription()=="")
          $invoicedetail->setDescription($invoicedetail->getProduct()->getDescription());
        $invoicedetail->calc();
        //process profitdetails
        $invoicedetail->calcRemaining();//counts profitdetails
        if($isnew or $qtyorpriceedited)
          $this->processBarcode($invoicedetail,$isnew);
        $invoicedetail->save();
        $invoice->calc();
        $invoice->save();
        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $invoicedetail)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');
        $this->redirect('@invoicedetail_new?invoice_id='.$invoice->getId());
     }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect('invoice/view?id='.$invoicedetail->getInvoiceId());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  private function processBarcode($invdetail,$isnew)
  {
    //if barcode exists
    //and it has a purchasedetail
    $barcode=$invdetail->getBarcode();
    if($barcode and substr($barcode,-1)=="D")
    {
      //extract product id and purchase no.
      list($suffix,$body,$purchdetail,$product)=MyBarcode::evaluateBarcode($barcode);
      
      //----CREATE PROFITDETAIL-------------------
      if($isnew)
      {
       $pd=new ProfitDetail();
       $pds=array($pd);
      }
      else
      {
       $pds=Doctrine_Query::create()
          ->from('Profitdetail pd')
        	->where('pd.invoicedetail_id = '.$invdetail->getId())
        	->andWhere('pd.purchasedetail_id = '.$purchdetail->getId())
        	->execute();
      }

      foreach($pds as $pd)
      {
         //give back qty in profitdetail to invdetail and purchdetail
	       $purchdetail->setRemaining($purchdetail->getRemaining()+$pd->getQty());
	       $invdetail->setRemaining($invdetail->getRemaining()+$pd->getQty());

         //process barcode / create new profitdetail
         //calculate qty to process
         //it's purchdetail->remaining or invdetail->remaining, whichever is lower
	       $processed=$purchdetail->getRemaining();
	       if($processed>$invdetail->getRemaining())$processed=$invdetail->getRemaining();
	       if($processed<0)$processed=0;

         //give qty to process from invdetail and purchdetail to profitdetail 
         $pd->setQty($processed);
		     $purchdetail->setRemaining($purchdetail->getRemaining()-$pd->getQty());
	       $invdetail->setRemaining($invdetail->getRemaining()-$pd->getQty());
         //if remaining=0, profitcalcluated=1
		     $invdetail->setIsProfitcalculated($invdetail->getRemaining()==0?1:0);
	       $purchdetail->save();

		     $pd->setProductId($invdetail->getProductId());
		     $pd->setInvoiceId($invdetail->getInvoiceId());
		     $pd->setPurchaseId($purchdetail->getPurchaseId());
		     $pd->setInvoicedetailId($invdetail->getId());
		     $pd->setPurchasedetailId($purchdetail->getId());
		     $pd->setInvoicedate($invdetail->getInvoice()->getDate());
		     $pd->setPurchasedate($purchdetail->getPurchase()->getDate());
		     $buyprice=$purchdetail->getTotal()/$purchdetail->getQty();//buyprice per unit
		     $sellprice=$invdetail->getTotal()/$invdetail->getQty();//sellprice per unit
		     $pd->setSellprice($sellprice);
		     $pd->setBuyprice($buyprice);
		     $pd->setProfitperunit($sellprice-$buyprice);
		     if($buyprice!=0)
			     $pd->setProfitrate(($sellprice/$buyprice)-1);
		     //else buyprice per unit is 0 - I got it for free
		     else
			     $pd->setProfitrate("100");//avoid divide by 0
		     $pd->setProfit($pd->getProfitperunit()*$processed);
		     $pd->save();
      }
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $invoicedetail=$this->getRoute()->getObject();
    $invoice=$invoicedetail->getInvoice();
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($invoicedetail->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');

      //detect orphan invoicedetail (detached from invoice)
      if($invoice==null or $invoice->getId()==null)
        $this->redirect($request->getReferer());
      else
      {
        $invoice->calc();
        $invoice->save();
        $this->redirect('invoice/view?id='.$invoice->getId());
      }
    }

  }
  public function executeCreateVatEntry(sfWebRequest $request)
  {
    if($request->getParameter("amount")==0)$this->redirect($request->getReferer());
  
    $detail=new Invoicedetail();
    $detail->setInvoiceId($request->getParameter("invoice_id"));
    $detail->setPrice($request->getParameter("amount"));
    $detail->setTotal($request->getParameter("amount"));
    $detail->setProductId(SettingsTable::fetch('vat_product_id'));
    $detail->setQty(1);
    $detail->setIsVat(1);
    $detail->save();
    
    $this->redirect($request->getReferer());
  }
    public function executeSetPrice(sfWebRequest $request)
    {
        if($request->getParameter('value')<=0)
        {
          $message="Invalid price";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
    
        $this->detail=Doctrine_Query::create()
        ->from('Invoicedetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $product=$this->detail->getProduct();

        //if value is below minimum selling price
        //and is_discounted is false
        //show error
        /*
        if($request->getParameter('value')<$product->getMinSellPrice() and $this->detail->getIsDiscounted()==0)
        {
            $message="Sorry, price too low. Minimum price for ".$product->getName()." is ".$product->getMinSellPrice();
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
        }
        */

        //if commission exists
        //value of with_commission must not be lower than price
        //if value (for commission) is less than price
        if($request->getParameter('commissionexists'))
        if($request->getParameter('value')>$this->detail->getWithCommission())
        {
            $message="Price with commission cannot be lower than Price";
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
        }

        $this->detail->setPrice($request->getParameter('value'));
        //if no commission exists, withcommission gets the same value as price
        if(!$request->getParameter('commissionexists'))
          $this->detail->setWithCommission($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $this->detail->getInvoice()->calc();
        $this->detail->getInvoice()->save();
	      $this->detail->updateProfitdetail();
        
        //todo: auto calc vat entry here

        $this->redirect($request->getReferer());
    }
    public function executeSetQty(sfWebRequest $request)
    {
      /*
        if($request->getParameter('value')==0)
        {
          $message="Invalid qty";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
        */
        if($request->getParameter('value')==0)
        {
          $message="Warning: Zero Quantity";
          $this->getUser()->setFlash('error', $message);
        }
    
        $this->detail=Doctrine_Query::create()
        ->from('Invoicedetail id, id.Invoice i')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setQty($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->calcRemaining();
        $this->detail->save();
        $this->detail->getInvoice()->calc();
        $this->detail->getInvoice()->save();
        //if closed, update stock entry
        if($this->detail->getInvoice()->getIsTemporary()==0)
          $this->detail->updateStockentry();
        
        
        //todo: auto calc vat entry here

        $this->redirect($request->getReferer());
    }
    public function executeSetDiscrate(sfWebRequest $request)
    {
        //reject anything except numbers and spaces
        $pattern = '/^[0-9. ]+$/';
        if ( !preg_match ($pattern, $request->getParameter('value')) and $request->getParameter('value')!="")
        {
          $message="Invalid discount rate";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }    
    
        $this->detail=Doctrine_Query::create()
        ->from('Invoicedetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        //cannot set discount rate greater than max discount rate
        $max_discount_level=$this->detail->getInvoice()->getMaxDiscountLevel();
        if (DiscountLevelTable::getTotalDiscount($request->getParameter('value')) > $max_discount_level->getTotalDiscount())
        {
          $message="Invalid discount rate";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }

        $this->detail->setDiscrate($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $this->detail->getInvoice()->calc();
        $this->detail->getInvoice()->save();

        $this->redirect($request->getReferer());
    }
    public function executeSetDescription(sfWebRequest $request)
    {
        $this->detail=Doctrine_Query::create()
        ->from('Invoicedetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setDescription($request->getParameter('value'));
        $this->detail->save();

        $this->redirect($request->getReferer());
    }
    public function executeSetWithCommission(sfWebRequest $request)
    {
        //value is new value for with_commission
        
        //if value is empty or 0
        //show error
        if($request->getParameter('value')<=0)
        {
          $message="Invalid price";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
    
        //read record from database
        $this->detail=Doctrine_Query::create()
        ->from('Invoicedetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        //value of with_commission must not be lower than price
        //if value (for commission) is less than price
        if($request->getParameter('value')<$this->detail->getPrice())
        {
            $message="Price with commission cannot be lower than Price";
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
        }

        $this->detail->setWithCommission($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $this->detail->getInvoice()->calc();
        $this->detail->getInvoice()->save();
	      $this->detail->updateProfitdetail();
        
        //todo: auto calc vat entry here

        $this->redirect($request->getReferer());
    }
    public function executeRegenStockentry(sfWebRequest $request)
    {
      // die("ACCESS DENIED");
  
        //default values
      $this->interval=100;
      $this->start=1;
  
      //if method = get
      if(!isset($_REQUEST["submit"]))
      {
        $this->invoicedetails=array();  
       return;
      }
       
      if(isset($_REQUEST["interval"]))
          $this->interval=$request->getParameter("interval");
      if(isset($_REQUEST["start"]))
          $this->start=$request->getParameter("start");
       $this->end=$this->start+$this->interval-1;
      
       $this->invoicedetails = Doctrine_Query::create()
        ->from('Invoicedetail p')
        ->where('p.id <='.$this->end)
        ->andWhere('p.id >='.$this->start)
        ->execute();
      $se_raw = Doctrine_Query::create()
        ->from('Stockentry se')
        ->where('se.ref_class="Invoicedetail"')
        ->andWhere('se.ref_id <='.$this->end)
        ->andWhere('se.ref_id >='.$this->start)
        ->execute();
      $this->stockentries=array();
      foreach($se_raw as $se)
        $this->stockentries[$se->getRefId()]=$se;
  
      foreach($this->invoicedetails as $pd)
      {
        if(!isset($this->stockentries[$pd->getId()]))
        {
          $invoice=$pd->getInvoice();
          if($invoice!=null and $invoice->getIsTemporary()==0 and $invoice->getStatus()!="Cancelled")
          {
            $pd->updateStockentry();
            echo "regenerated: ".$pd->getId()."<br>";
          }
        }
      }
  
       $this->start=$this->end+1;
    }  
  }
