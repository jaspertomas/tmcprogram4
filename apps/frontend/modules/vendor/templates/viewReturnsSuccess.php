<?php use_helper('I18N', 'Date') ?>
<h1>Vendor History: <?php echo $vendor->getName() ?></h1>

<?php echo link_to("New Purchase Order","purchase/new?vendor_id=".$vendor->getId()) ?>
<br><?php echo link_to("Edit","vendor/edit?id=".$vendor->getId()) ?>
<br>
<br>Address: <?php echo $vendor->getAddr1()?>
<br>Address 2: <?php echo $vendor->getAddr2()?>
<br>Address 3: <?php echo $vendor->getAddr3()?>
<br>Phone: <?php echo $vendor->getPhone1()?>
<br>Tin No: <?php echo $vendor->getTinNo()?>
<br><?php //echo link_to("Add Quote","quote/new?vendor_id=".$vendor->getId()) ?>
<br>
<br>

<table border=1 width="1200">
<tr>
  <td width="16%"><h2><center><?php echo link_to("Purchases","vendor/view?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Unpaid Purchase Orders","vendor/viewUnpaid?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center>Returns and Replacements</center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Consignment Payments","consignment_payment/index?vendor_id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Counter Receipts","vendor/viewCounterReceipts?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Vouchers","vendor/viewVouchers?id=".$vendor->getId()) ?></center></h2></td>

</tr>
</table>

<br>

<?php if(count($returnss)==0)echo "No returns found";else{ ?>
<h1>Return Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Date</td>
    <td>Reference</td>
    <td>Particulars</td>
  </tr>
  
  <?php 
  foreach($returnss as $returns){?>
  <tr>
    <td><?php echo link_to($returns->getCode(),"returns/view?id=".$returns->getId()) ?></td>
    <td><?php echo MyDateTime::frommysql($returns->getDate())->toshortdate() ?></td>
    <td><?php $ref=$returns->getRef();if($ref!=null)echo link_to($ref,$returns->getRefClass()."/view?id=".$returns->getRefId()) ?></td>
    <td><?php echo $returns->getParticularsString() ?></td>
    <td><?php echo $returns->getNotes() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

<hr>

<?php if(count($replacements)==0)echo "No replacements found";else{ ?>
<h1>Replacement Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Date</td>
    <td>Reference</td>
    <td>Particulars</td>
  </tr>
  
  <?php 
  foreach($replacements as $replacement){?>
  <tr>
    <td><?php echo link_to($replacement->getName(),"replacement/view?id=".$replacement->getId()) ?></td>
    <td><?php echo MyDateTime::frommysql($replacement->getDate())->toshortdate() ?></td>
    <td><?php $ref=$replacement->getRef();if($ref!=null)echo link_to($ref,$replacement->getRefClass()."/view?id=".$replacement->getRefId()) ?></td>
    <td><?php echo $replacement->getParticularsString() ?></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>

