<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Merge Vendors</h1>
<br>
Remember to backup your database first!
<br>
<?php echo form_tag("vendor/merge2");?>
Vendor ID to merge: <input name=merger_vendor_id id=merger_vendor_id>
<br>Vendor ID to merge into <input name=mergee_vendor_id id=mergee_vendor_id>
<br><input type=submit value=Test>
</form>
