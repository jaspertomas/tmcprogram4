<?php use_helper('I18N', 'Date') ?>
<h1>Vendor History: <?php echo $vendor->getName() ?></h1>

<?php echo link_to("New Voucher Order","voucher/new?vendor_id=".$vendor->getId()) ?>
<br><?php echo link_to("Edit","vendor/edit?id=".$vendor->getId()) ?>
<br>
<br>Address: <?php echo $vendor->getAddr1()?>
<br>Address 2: <?php echo $vendor->getAddr2()?>
<br>Address 3: <?php echo $vendor->getAddr3()?>
<br>Phone: <?php echo $vendor->getPhone1()?>
<br>Tin No: <?php echo $vendor->getTinNo()?>
<br><?php //echo link_to("Add Quote","quote/new?vendor_id=".$vendor->getId()) ?>
<br>
<br>

<table border=1 width="1200">
<tr>
  <td width="16%"><h2><center><?php echo link_to("Purchases","vendor/view?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Unpaid Purchase Orders","vendor/viewUnpaid?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Returns and Replacements","vendor/viewReturns?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Consignment Payments","consignment_payment/index?vendor_id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Counter Receipts","vendor/viewCounterReceipts?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center>Vouchers</center></h2></td>
</tr>
</table>

<?php if(count($vouchers)==0){?>
<br>No check vouchers found
<?php }else{ ?>

<br>
<b>Page <?php echo $page;?></b>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."&page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."&page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo "<b>".$apage."</b> ";
  else echo link_to("$apage",$pagepath."&page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."&page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."&page=".$pagecount))." ";

?>

<br>
<br>

<table border=1>
  <tr>
    <td>Check Date</td>
    <td>Create Date</td>
    <td>Release Date</td>
    <td>Clear Date</td>
    <td>Ref</td>
    <td>Check No</td>
    <td>Account</td>
    <td>Type</td>
    <td>Pay To</td>
    <td>Amount</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>
  <?php foreach($vouchers as $voucher){?>
    <?php $check=null;$out_payment=$voucher->getOutPayment(); if($out_payment)$check=$out_payment->getOutCheck();?>
    <tr>
        <td><?php if($check!=null)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
        <td><?php echo MyDateTime::frommysql($voucher->getDate())->toshortdate()?></td>
        <td><?php if($check!=null and $check->getReceiveDate()!=null)echo MyDateTime::frommysql($check->getReceiveDate())->toshortdate()?></td>
        <td><?php if($check!=null and $check->getClearedDate()!=null)echo MyDateTime::frommysql($check->getClearedDate())->toshortdate()?></td>
        <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())?></td>
        <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo(); ?></td>
        <td><?php echo $voucher->getVoucherAccount()?></td>
        <td><?php echo $voucher->getVoucherType()?></td>
        <td><?php echo $voucher->getPayee(); //echo $voucher->getBiller()?></td>
        <td><?php echo MyDecimal::format($voucher->getAmount());?></td>
        <td><?php echo $voucher->getParticulars()?></td>
        <td><?php echo $voucher->getNotes()?></td>
    </tr>  
  <?php } ?>
</table>

<b>Page <?php echo $page;?></b>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."&page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."&page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo "<b>".$apage."</b> ";
  else echo link_to("$apage",$pagepath."&page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."&page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."&page=".$pagecount))." ";

?>

<?php } ?>

<br>
<br>
