<?php use_helper('I18N', 'Date'); ?>
<h1>Unpaid Purchase Orders: <?php echo $vendor?> </h1>
<?php echo link_to("New Purchase Order","purchase/new?vendor_id=".$vendor->getId()) ?>
<br><?php echo link_to("Edit","vendor/edit?id=".$vendor->getId()) ?>
<br>
<br>Address: <?php echo $vendor->getAddr1()?>
<br>Address 2: <?php echo $vendor->getAddr2()?>
<br>Address 3: <?php echo $vendor->getAddr3()?>
<br>Phone: <?php echo $vendor->getPhone1()?>
<br>Tin No: <?php echo $vendor->getTinNo()?>
<br><?php //echo link_to("Add Quote","quote/new?vendor_id=".$vendor->getId()) ?>
<br>
<br>
<!--br--><?php //echo link_to("Print Payment Report","vendor/viewUnpaidPdf?id=".$vendor->getId()) ?>

<table border=1 width="1200">
<tr>
  <td width="16%"><h2><center><?php echo link_to("Purchases","vendor/view?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center>Unpaid Purchases</center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Returns and Replacements","vendor/viewReturns?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Consignment Payments","consignment_payment/index?vendor_id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Counter Receipts","vendor/viewCounterReceipts?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Vouchers","vendor/viewVouchers?id=".$vendor->getId()) ?></center></h2></td>

</tr>
</table>

<hr>
<table>
  <tr>
    <td>Total Receivable: <?php echo MyDecimal::format($total)?></td>
    <td> | </td>
    <td>Total Due: <?php echo MyDecimal::format($totaldue)?></td>
    <td> | </td>
    <td>Notes: </td>
    <td><?php echo form_tag("vendor/adjustCollectionNotes",array("class"=>"collection_notes_form"))?>
      <input type=hidden name=id value=<?php echo $vendor->getId()?>><input name=collection_notes value=<?php echo $vendor->getCollectionNotes()?>>
      <input type=submit value=Save>
      </form>
</td>
  </tr>
</table>

<hr>

<table border=1>
  <tr>
    <td>Date</td>
    <td>Purchase</td>
    <td>Status</td>
    <td>Product</td>
    <td>Qty</td>
    <td>Price</td>
    <td>Total</td>
    <td>Balance</td>
  </tr>
  <?php foreach($purchases as $purchase){?>
  <tr>
    <td><?php echo $purchase->getDate() ?></td>
    <td><?php echo link_to($purchase,"purchase/view?id=".$purchase->getId()) ?></td>
    <td colspan=2>
      <span id=collection_status_display_<?php echo $purchase->getId()?> class=collection_status_display>
        <font color=<?php $isduestring=$purchase->getIsDueString();echo $purchase->getColorForIsDueString($isduestring)?>>
          <?php echo $isduestring;?>
        </font>
      </span>
      <input type=button purchase_id="<?php echo $purchase->getId()?>" class=collection_status_button value="Due">
      <input type=button purchase_id="<?php echo $purchase->getId()?>" class=collection_status_button value="Counter Received">
      <input type=button purchase_id="<?php echo $purchase->getId()?>" class=collection_status_button value="Cheque Ready">
  	<td></td>
  	<td></td>
  	<td></td>
  	<td></td>
  </tr>
  <?php foreach($purchase->getPurchasedetail() as $detail){?>
  <tr>
  	<td></td>
  	<td></td>
  	<td></td>
    <td><?php echo $detail->getDescription() ?></td>
    <td align=right><?php echo $detail->getQty() ?></td>
    <td align=right><?php echo MyDecimal::format($detail->getPrice()) ?></td>
    <td align=right><?php echo MyDecimal::format($detail->getTotal()) ?></td>
  	<td></td>
  </tr>
  <?php }?>
  </tr>
  	<td></td>
  	<td></td>
  	<td></td>
  	<td></td>
  	<td></td>
  	<td></td>
    <td align=right><b><?php echo MyDecimal::format($purchase->getTotal()) ?></b></td>
    <td align=right><b><?php echo MyDecimal::format($purchase->getBalance()) ?></b></td>
  </tr>
  <?php } ?>
</table>


<script>
//on clicking Save on notes edit form
$(document).on("submit", ".collection_notes_form", function(e){
e.preventDefault();
    $.ajax({ // create an AJAX call...
        data: $(this).serialize(), // get the form data
        type: $(this).attr('method'), // GET or POST
        url: $(this).attr('action'), // the file to call
        success: function(response) { // on success..
  alert("Note Saved");
        }
    });
return false;
});
//on clicking Due, Bill Sent or Cheque REady
$(document).on("click", ".collection_status_button", function(e){
  var collection_status=$(this).val();
  var purchase_id=$(this).attr("purchase_id");
  $.ajax({ // create an AJAX call...
      data: "id="+purchase_id+"&collection_status="+collection_status, // get the form data
      type: "post", // GET or POST
      url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/purchase/adjustCollectionStatus/action",
      success: function(response) { // on success..
        $("#collection_status_display_"+purchase_id).html(response);
      }
  });
});
</script>
