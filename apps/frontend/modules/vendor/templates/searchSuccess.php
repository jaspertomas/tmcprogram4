<?php use_helper('I18N', 'Date'); ?>
<h1>Supplier Search</h1>
<table border="1">
  <tr>
    <td>Name</td>
    <td>Bank Account</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Notes</td>
  </tr>
  
  <?php 
  $matchfound=false;
  foreach($vendors as $vendor)
  if(strtolower($vendor->getName())==strtolower($searchstring)){$matchfound=true;break;}
  if(!$matchfound){
  ?>
  <tr>
    <td><?php echo $searchstring?> (New Vendor) </td>
    <td><?php echo link_to("Create New Vendor","vendor/new?name=".$searchstring) ?></td>
    <td></td>
    <td></td>
    <td><?php echo link_to("Create Purchase","purchase/new?vendor_name=".$searchstring) ?></td>
    <td><?php //echo link_to("Create Incoming DR","delivery/new?vendor_name=".$searchstring) ?></td>
    <td></td>
  </tr>
  <?php } ?>
  <?php foreach($vendors as $vendor){?>
  <tr>
    <td><?php echo $vendor->getName() ?></td>
    <td><?php echo $vendor->getBankAcct() ?></td>
    <?php if($counter_receipt_id!=null){?>
      <td><?php echo link_to("Use in Counter Receipt","counter_receipt/processChooseVendor?id=".$counter_receipt_id."&vendor_id=".$vendor->getId()) ?></td>
    <?php } ?>
    <td><?php echo link_to("View","vendor/view?id=".$vendor->getId()) ?></td>
    <td><?php echo link_to("View Unpaid","vendor/viewUnpaid?id=".$vendor->getId()) ?></td>
    <td><?php echo link_to("Create Purchase","purchase/new?vendor_id=".$vendor->getId()) ?></td>
    <td><?php if(sfConfig::get('custom_enable_supplier_return'))echo link_to("Create Return","returns/new?client_id=".$vendor->getId()."&client_class=Vendor");?></td>
    <?php if($sf_user->hasCredential(array('admin', 'encoder'),false)){?>
      <td><?php echo link_to("Create Counter Receipt","counter_receipt/new?vendor_id=".$vendor->getId()) ?></td>
    <?php } ?>
    <td><?php echo $vendor->getNotes() ?></td>
  </tr>
  <?php } ?>
</table>
