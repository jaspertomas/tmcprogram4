<?php use_helper('I18N', 'Date') ?>
<h1>Vendor History: <?php echo $vendor->getName() ?></h1>

<?php echo link_to("New Purchase Order","purchase/new?vendor_id=".$vendor->getId()) ?>
<br><?php echo link_to("Edit","vendor/edit?id=".$vendor->getId()) ?>
<br>
<br>Address: <?php echo $vendor->getAddr1()?>
<br>Address 2: <?php echo $vendor->getAddr2()?>
<br>Address 3: <?php echo $vendor->getAddr3()?>
<br>Phone: <?php echo $vendor->getPhone1()?>
<br>Tin No: <?php echo $vendor->getTinNo()?>
<br><?php //echo link_to("Add Quote","quote/new?vendor_id=".$vendor->getId()) ?>
<br>
<br>

<table border=1 width="1200">
<tr>
  <td width="16%"><h2><center><?php echo link_to("Purchases","vendor/view?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Unpaid Purchase Orders","vendor/viewUnpaid?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Returns and Replacements","vendor/viewReturns?id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Consignment Payments","consignment_payment/index?vendor_id=".$vendor->getId()) ?></center></h2></td>
  <td width="16%"><h2><center>Counter Receipts</center></h2></td>
  <td width="16%"><h2><center><?php echo link_to("Vouchers","vendor/viewVouchers?id=".$vendor->getId()) ?></center></h2></td>
</tr>
</table>


<?php if(count($counterReceipts)==0){?>
<br>No counter receipts found
<?php }else{ ?>

<br>
<b>Page <?php echo $page;?></b>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."&page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."&page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo "<b>".$apage."</b> ";
  else echo link_to("$apage",$pagepath."&page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."&page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."&page=".$pagecount))." ";

?>

<br>
<br>

<table border=1>
  <tr>
    <td>Date</td>
    <td>Purchase</td>
    <td>Status</td>
    <td>Product</td>
    <td>Qty</td>
    <td>Price</td>
    <td>Discrate</td>
    <td>Discamt</td>
    <td>Total</td>
  </tr>
  <?php foreach($counterReceipts as $counterReceipt){?>
  <tr>
    <td><?php echo MyDateTime::frommysql($counterReceipt->getDate())->toshortdate() ?></td>
    <td><?php echo link_to($counterReceipt,"counter_receipt/view?id=".$counterReceipt->getId()) ?></td>
    <td><?php echo $counterReceipt->getStatus() ?></td>
  </tr>
  <?php /*foreach($counterReceipt->getPurchasedetail() as $detail){?>
  <tr>
  	<td></td>
  	<td></td>
  	<td></td>
    <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
    <td><?php echo $detail->getQty() ?></td>
    <td><?php echo $detail->getPrice() ?></td>
    <td><?php echo $detail->getDiscrate() ?></td>
    <td><?php echo $detail->getDiscamt() ?></td>
    <td><?php echo $detail->getTotal() ?></td>
  </tr>
  <?php }*/?>
  <?php } ?>
</table>

<b>Page <?php echo $page;?></b>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."&page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."&page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo "<b>".$apage."</b> ";
  else echo link_to("$apage",$pagepath."&page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."&page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."&page=".$pagecount))." ";

?>

<?php } ?>

<br>
<br>
