<?php

require_once dirname(__FILE__).'/../lib/vendorGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/vendorGeneratorHelper.class.php';

/**
 * vendor actions.
 *
 * @package    sf_sandbox
 * @subpackage vendor
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class vendorActions extends autoVendorActions
{
  public function executeViewReturns(sfWebRequest $request)
  {
    $this->vendor = $this->getRoute()->getObject();
    $this->returnss = $this->vendor->getReturns();
    $this->replacements = $this->vendor->getReplacements();
  }
  public function executeViewVouchers(sfWebRequest $request)
  {
    $this->itemsperpage=25;
    $this->page=$request->getParameter("page");
    if($this->page==null)$this->page=1;
    $this->vendor = $this->getRoute()->getObject();
    $this->vouchers=$this->vendor->getVouchersDescByPage($this->page,$this->itemsperpage);
    $this->pagecount=$this->vendor->getVouchersPageCount($this->itemsperpage);
    $this->pagepath="vendor/viewVouchers?id=".$this->vendor->getId();
  }
  public function executeViewCounterReceipts(sfWebRequest $request)
  {
    $this->itemsperpage=25;
    $this->page=$request->getParameter("page");
    if($this->page==null)$this->page=1;
    $this->vendor = $this->getRoute()->getObject();
    $this->counterReceipts=$this->vendor->getCounterReceiptsDescByPage($this->page,$this->itemsperpage);
    $this->pagecount=$this->vendor->getCounterReceiptsPageCount($this->itemsperpage);
    $this->pagepath="vendor/viewCounterReceipts?id=".$this->vendor->getId();
  }
  public function executeView(sfWebRequest $request)
  {
    $this->itemsperpage=25;
    $this->page=$request->getParameter("page");
    if($this->page==null)$this->page=1;
    $this->vendor = $this->getRoute()->getObject();
    $this->purchases=$this->vendor->getPurchasesDescByPage($this->page,$this->itemsperpage);
    $this->pagecount=$this->vendor->getPurchasesPageCount($this->itemsperpage);
    $this->pagepath="vendor/view?id=".$this->vendor->getId();
  }
  public function executeViewUnpaid(sfWebRequest $request)
  {
    $this->vendor = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->invoice);

    $this->total=0;
    $this->totaldue=0;
    
    $this->purchases=$this->vendor->getUnpaidPurchases("desc");
    
    foreach($this->purchases as $purchase)
    {
      $this->total+=$purchase->getBalance();
      if($purchase->isDue())
        $this->totaldue+=$purchase->getBalance();
    }
  }
  public function executeViewUnpaidPdf(sfWebRequest $request)
  {
    $this->executeViewUnpaid($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeSearch(sfWebRequest $request)
  {
    $this->counter_receipt_id=null;
    if($request->getParameter("counter_receipt_id"))
      $this->counter_receipt_id=$request->getParameter("counter_receipt_id");
  
    $query=Doctrine_Query::create()
        ->from('Vendor i')
      	->where('i.name like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("i.name")
        ;

  	$results=$query->execute();
  	
  	/*
  	if(count($results)==1)
  	{
            $this->vendor=$results[0];
            $this->redirect("vendor/view?id=".$this->vendor->getId());
  	}
  	else
  	*/
    {
        $this->vendors=$results;
    }
    $this->searchstring=$request->getParameter("searchstring");
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->vendor = new Vendor();
    
    //set vendor if param vendor_name
    if($request->hasParameter("name"))
    {
      $this->vendor->setName($request->getParameter("name"));
    }

    $this->form = $this->configuration->getForm($this->vendor);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    //only admin can create or edit suppliers
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");

    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $vendor = $form->save();
        //recalc duedate for all purchases
        Doctrine_Query::create()
          ->update('Purchase p')
          ->set('p.duedate', 'DATE_ADD( date, INTERVAL '.$vendor->getTerms().' DAY )')
          ->where('p.vendor_id=?',$vendor->getId())
          ->execute();
        
        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $vendor)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@vendor_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect(array('sf_route' => 'vendor_edit', 'sf_subject' => $vendor));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeAdjustCollectionNotes(sfWebRequest $request)
  {
    $vendor=Doctrine_Query::create()
        ->from('Vendor c')
      	->where('c.id = '.$request->getParameter('id'))
      	->fetchOne();
  	$vendor->setCollectionNotes($request->getParameter('collection_notes'));
  	$vendor->save();
  	die();
  }
  public function executeFiles(sfWebRequest $request)
  {
    $this->vendor=MyModel::fetchOne("Vendor",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('vendor');
    $this->file->setParentId($this->vendor->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="vendor"')
      ->andWhere('f.parent_id='.$this->vendor->getId())
      ->execute();
  }

  public function executeMerge1(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
    
  }
  public function executeMerge2(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
 
    $this->merger_id=$request->getParameter("merger_vendor_id");
    $this->mergee_id=$request->getParameter("mergee_vendor_id");
    $this->merger=Doctrine_Query::create()
        ->from('Vendor p')
        ->where("p.id = '".$this->merger_id."'")
        ->fetchOne();
    $this->mergee=Doctrine_Query::create()
        ->from('Vendor p')
        ->where("p.id = '".$this->mergee_id."'")
        ->fetchOne();
   
  }
  public function executeMerge3(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
 
    $merger=Doctrine_Query::create()
        ->from('Vendor p')
        ->where("p.id = '".$request->getParameter("merger_vendor_id")."'")
        ->fetchOne();
    $mergee=Doctrine_Query::create()
        ->from('Vendor p')
        ->where("p.id = '".$request->getParameter("mergee_vendor_id")."'")
        ->fetchOne();
   
    //merger merges into mergee
    //merger changes vendor id
    Doctrine_Query::create()
      ->update('Purchase p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('OutCheck p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Voucher p')
      ->set('p.client_id',$mergee->getId())
      ->where('p.client_id = ?', $merger->getId())
      ->andWhere("p.client_type = 'Vendor'")
      ->execute();
    Doctrine_Query::create()
      ->update('CounterReceipt p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('CounterReceiptDetail p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Quote p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('ConsignmentPayment p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Pricelist p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('PurchaseDr p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('PurchaseDrDetail p')
      ->set('p.vendor_id',$mergee->getId())
      ->where('p.vendor_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Returns p')
      ->set('p.client_id',$mergee->getId())
      ->where('p.client_id = ?', $merger->getId())
      ->andWhere('p.client_class = "Vendor"')
      ->execute();
    Doctrine_Query::create()
      ->update('Replacement p')
      ->set('p.client_id',$mergee->getId())
      ->where('p.client_id = ?', $merger->getId())
      ->andWhere('p.client_class = "Vendor"')
      ->execute();
    
    $merger->delete();

    $this->getUser()->setFlash('msg', $merger." merged into ".$mergee, true);
  }
}
