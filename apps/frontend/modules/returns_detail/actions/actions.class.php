<?php

require_once dirname(__FILE__).'/../lib/returns_detailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/returns_detailGeneratorHelper.class.php';

/**
 * returns_detail actions.
 *
 * @package    sf_sandbox
 * @subpackage returns_detail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class returns_detailActions extends autoReturns_detailActions
{
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());
    
    $qty=intval($requestparams["qty"]);
    if($qty<1)
    {
      $this->getUser()->setFlash('error', 'Invalid Quantity', true);
      return $this->redirect("returns/view?id=".$requestparams['returns_id']);
    }
  
    //detect if return already has an rdetail of same product id
    $rdetail=Doctrine_Query::create()
    ->from('ReturnsDetail rd')
    ->where('rd.returns_id = '.$requestparams['returns_id'])
    ->andWhere('rd.product_id = '.$requestparams['product_id'])
    ->fetchOne();

    //if rdetail with same product id exists, adjust qty for that
    if($rdetail!=null)
    {
      $ref=$rdetail->getRef();
      
      if($ref!=null)
      {
        //adjust ref qty returned
        $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
        $ref->save();
      }
    }
    //else create new rdetail
    else
    {
      $rdetail=new ReturnsDetail();
      $rdetail->setProductId($requestparams['product_id']);
      $rdetail->setReturnsId($requestparams['returns_id']);

      $returns=$rdetail->getReturns();
      $rdetail->setRefClass($returns->getIsSales()?"Invoicedetail":"Purchasedetail");
      
      //try to find an Invoicedetail or Purchasedetail that matches product_id
      if($returns->getIsSales())
      {
        $ref=Doctrine_Query::create()
          ->from('Invoicedetail id')
          ->where('id.invoice_id = '.$rdetail->getReturns()->getRefId())
          ->andWhere('id.product_id = '.$requestparams['product_id'])
          ->fetchOne();
      }else{
        $ref=Doctrine_Query::create()
          ->from('Purchasedetail id')
          ->where('id.purchase_id = '.$rdetail->getReturns()->getRefId())
          ->andWhere('id.product_id = '.$requestparams['product_id'])
          ->fetchOne();
      }
      //if invoicedetail or purchasedetail is found, use it as ref
      if($ref!=null)
      {
        $rdetail->setRefId($ref->getId());
        //adjust ref qty returned
        $ref->setQtyReturned($ref->getQtyReturned()-$rdetail->getQty()+$qty);
        $ref->save();
      }

    }

    //adjust rdetail qty and calc prices
    $rdetail->setPrice($requestparams['price']);
    $rdetail->setTotal($requestparams['price']*$qty);
    $rdetail->setQty($qty);
    $rdetail->save();

    //adjust stock
    $rdetail->updateStockEntry();

    $this->getUser()->setFlash('notice', 'Successfully returned '.$rdetail->getProduct(), false);


    $this->redirect("returns/view?id=".$rdetail->getReturnsId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $returns_detail=$this->getRoute()->getObject();

    if(count($returns_detail->getReplacementDetail())>0)
    {
      $this->getUser()->setFlash('error', "Cannot delete this returned item - replacements exist.");
      $this->redirect($request->getReferer());
    }
    
    if ($returns_detail->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("returns/view?id=".$returns_detail->getReturnsId());
  }
}
