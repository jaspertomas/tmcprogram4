<?php

/**
 * android actions.
 *
 * @package    sf_sandbox
 * @subpackage android
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class androidActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $products= Doctrine_Query::create()
        ->from('Product p')
      	->execute()->getData();
      	
  	$array=array();
  	foreach($products as $product)
  	{
  	  $array[]=array(
  	    "id"=>$product->getId()
  	    ,"name"=>$product->getName()
//  	    ,"description"=>$product->getDescription()
  	    ,"producttype_id"=>$product->getProducttypeId()
  	    ,"minsellprice"=>$product->getMinSellPrice()
  	    ,"maxsellprice"=>$product->getMaxSellPrice()
  	    ,"minbuyprice"=>$product->getMinBuyPrice()
  	    ,"maxbuyprice"=>$product->getMaxBuyPrice()
  	    );
  	}
  	echo json_encode($array);
  	die();
  }
  public function executeSearch(sfWebRequest $request)
  {
//http://localhost/tmcprogram4/web/frontend_dev.php/android/search?type=producttype&searchstring=light&access_token=7be5f38d867415b03358c7d3227f15a851da96db
	$json = json_decode(file_get_contents('php://input'),true);
	$data=$json["user"];
	//var_dump($json);

    $access_token=sha1(sfConfig::get('custom_program_label'));
    if($json["access_token"]!=$access_token)
    {
      echo json_encode(array("success"=>false,"error"=>"Access denied"));
      die();
    }

    $keywords=explode(" ",$json["searchstring"]);
foreach($keywords as $index=>$keyword)if(trim($keyword)=="")unset($keywords[$index]);

	$searchtype=$json["type"];
	switch($searchtype)
{
case "product":
    echo json_encode(array("success"=>true,"keywords"=>$keywords,"results"=>self::productSearch($keywords)));
    die();
case "producttype":
    echo json_encode(array("success"=>true,"results"=>self::search("Producttype",$keywords)));
    die();
case "vendor":
    echo json_encode(array("success"=>true,"results"=>self::search("Vendor",$keywords)));
    die();
case "customer":
    echo json_encode(array("success"=>true,"results"=>self::search("Customer",$keywords)));
    die();
case "voucher":
    echo json_encode(array("success"=>true,"results"=>self::voucherSearch($keywords)));
    die();
case "invoice":
    echo json_encode(array("success"=>true,"results"=>self::invoiceSearch($keywords)));
    die();
case "purchase":
    echo json_encode(array("success"=>true,"results"=>self::purchaseSearch($keywords)));
    die();
case "folder":
    echo json_encode(array("success"=>true,"results"=>self::folderSearch($keywords)));
    die();
default: 
    echo json_encode(array("success"=>false,"error"=>"Invalid search type"));
    die();

}

  }
private function folderSearch($keywords)
{
    $query=Doctrine_Query::create()
	->select("id, title as name")
        ->from('Folder p')
        ->orderBy("p.title")
	->limit(100)
      	;
    foreach($keywords as $keyword)
{
    	$query->andWhere("p.title LIKE '%".$keyword."%' or p.description LIKE '%".$keyword."%' or p.keywords LIKE '%".$keyword."%'");
}
    return $query->fetchArray();
}
private function productSearch($keywords)
{
    $query=Doctrine_Query::create()
	->select("id, name")
        ->from('Product p')
        ->orderBy("p.name")
	->limit(100)
      	;
    foreach($keywords as $keyword)
{
    	$query->andWhere("p.name LIKE '%".$keyword."%' or p.description LIKE '%".$keyword."%'");
}
    return $query->fetchArray();
}
private function voucherSearch($keywords)
{
    $query=Doctrine_Query::create()
	->select("p.id, concat(p.no,' - ',p.payee,' - ',p.particulars) as name")
        ->from('Voucher p')
        ->orderBy("p.no")
	->limit(100)
      	;
    foreach($keywords as $keyword)
{
    	$query->andWhere("p.no = '".$keyword."' or p.particulars LIKE '%".$keyword."%' or p.payee LIKE '%".$keyword."%' or p.date = '".$keyword."'");
}
    return $query->fetchArray();
}
private function invoiceSearch($keywords)
{
    $query=Doctrine_Query::create()
	->select("i.id, concat(i.date, ' - ',it.name,' ',i.invno,' - ', c.name,' - ', i.total) as name")
        ->from('Invoice i, i.InvoiceTemplate it, i.Customer c')
        ->orderBy("i.invno")
	->limit(100)
      	;
    foreach($keywords as $keyword)
{
    	$query->andWhere("i.invno ='".$keyword."' or i.date = '".$keyword."' or c.name LIKE '%".$keyword."%'");
}
    return $query->fetchArray();
}
private function purchaseSearch($keywords)
{
    $query=Doctrine_Query::create()
	->select("i.id, concat(i.date, ' - ',it.name,' ',i.pono,' - ', c.name,' - ', i.total) as name")
        ->from('Purchase i, i.PurchaseTemplate it, i.Vendor c')
        ->orderBy("i.pono")
	->limit(100)
      	;
    foreach($keywords as $keyword)
{
    	$query->andWhere("i.pono ='".$keyword."' or i.date = '".$keyword."' or c.name LIKE '%".$keyword."%'");
}
    return $query->fetchArray();
}
private function search($table, $keywords)
{
		//search in name
    $query=Doctrine_Query::create()
	->select("id, name")
        ->from($table.' p')
        ->orderBy("p.name")
	->limit(100)
      	;
    foreach($keywords as $keyword)
    	$query->andWhere("p.name LIKE '%".$keyword."%'");
    return $query->fetchArray();
}


//-----pollwatcher code---------
//this accepts new user data in json form
  //step 1: validate access token
  //step 2: validate data complete and in good order
  //step 3: save sf_guard_user and user_data
  //step 4: create response (success: true, user_id:x)
  //or if validation fails, create error response (success:false)
  public function executeRegister(sfWebRequest $request)
  {
	$json = json_decode(file_get_contents('php://input'),true);
	$data=$json["user"];
	//var_dump($json);

    $access_token=sha1(sfConfig::get('custom_program_label'));
    if($json["access_token"]!=$access_token)
    {
      echo json_encode(array("success"=>false,"error"=>"Access denied"));
      die();
    }
    
    //validation
    $user=Doctrine_Query::create()
        ->from('SfGuardUser u')
      	->where('u.username = "'.$data["username"].'"')
      	->fetchOne();
    if($user==null)
    {
      echo json_encode(array("success"=>false,"error"=>"Invalid username or password."));
      die();
    }
    if(sha1($user->getSalt().$json["password"])!=$user->getPassword())
    {
      echo json_encode(array("success"=>false,"error"=>"Invalid username or password."));
      die();
    }

	$output=array("success"=>true);
	$output['sf_guard_user_id']=$user->getId();
    
    //create data array to be sent back to android app
    $this->getResponse()->setHttpHeader('Content-type','application/json');
    echo json_encode($output);

    die();
  }
  public function executeUpload(sfWebRequest $request)
  {
	$json = json_decode(file_get_contents('php://input'),true);
	//var_dump($json);

    $access_token=sha1(sfConfig::get('custom_program_label'));
    if($json["access_token"]!=$access_token)
    {
      echo json_encode(array("success"=>false,"error"=>"Access denied"));
      die();
    }

	$output=array("success"=>true);
	$record_ids=array();    
    foreach($json["records"] as $record)
    {
      //intelligently guess file type =D
      $filenamesegments=explode(".",$record["filename"]);
      $extension=strtolower($filenamesegments[1]);

    	$r=new File();
      $uploadlocation=FileTable::genUploadLocation();//yyyymm of upload date
      	$r->setUploadlocation($uploadlocation);
    	$r->setParentClass($record["parent_class"]);
    	$r->setParentId($record["parent_id"]);
    	$r->setFile($record["filename"]);
    	$r->setFilename($record["title"].".".$extension);
    	$r->setTitle($record["title"]);
    	$r->setDescription($record["description"]);
    	$r->setKeywords($record["keywords"]);
    	$r->setFilesize($record["filesize"]);
    	$r->setFiletype($record["filetype"]);

//    	$r->setSfGuardUserId($record["sf_guard_user_id"]);
//    	$r->setDatetime($record["datetime"]);
//    	$r->setParentClass($record["record_type"]);
//    	$r->setParentId($record["record_type"]);
    	$r->save();
    	$record_ids[]=$record["filename"];//send back filename; android uses it to delete old records
    	$this->stringToImageFile($record["image_encoded"],$uploadlocation,$record["filename"]);
    }
	$output['record_ids']=$record_ids;
    //create data array to be sent back to android app
    $this->getResponse()->setHttpHeader('Content-type','application/json');
    echo json_encode($output);

    die();
  }
  public function stringToImageFile($base,$subfolder,$image_name)
  {
    $binary = base64_decode($base);
    if (!file_exists('path/to/directory'))
      mkdir('uploads//files//'.$subfolder, 0777, true);
    $file = fopen("uploads//files//".$subfolder."/".$image_name, "wb"); // 
    fwrite($file, $binary);
    fclose($file);          
  }
}
