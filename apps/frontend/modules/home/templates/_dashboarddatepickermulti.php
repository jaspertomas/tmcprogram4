<?php use_helper('I18N', 'Date') ?>
<h3><?php echo link_to("Back to Dashboard","home/dashboard?date=".$toform->getObject()->getDate())?></h3>
<?php echo form_tag_for(new InvoiceForm(),"home/$action")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>

<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $toform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Month","home/$action?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","home/$action?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","home/$action?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br>
    <?php //echo link_to("Yesterday","home/$action?invoice[date][day]=".$yesterday->getDay()."&invoice[date][month]=".$yesterday->getMonth()."&invoice[date][year]=".$yesterday->getYear()); ?>
    <?php //echo link_to("Tomorrow","home/$action?invoice[date][day]=".$tomorrow->getDay()."&invoice[date][month]=".$tomorrow->getMonth()."&invoice[date][year]=".$tomorrow->getYear()); ?>
<br>

</form>
