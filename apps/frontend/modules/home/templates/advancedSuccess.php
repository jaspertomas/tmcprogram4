<h1>Advanced Tools</h1>


<h2>
<?php echo link_to("Merge Products","product/merge1")?>
<br><?php echo link_to("Merge Customers","customer/merge1")?>
<br><?php echo link_to("Merge Suppliers","vendor/merge1")?>
</h2>


<table>
<tr valign=top>
  <td width=50%>
<?php echo link_to("Invoices",'invoice/index'); ?>
<br><?php echo link_to("Purchases",'purchase/index'); ?>
<br><?php echo link_to("Invoice Templates",'invoice_template/index'); ?>
<br><?php echo link_to("Purchase Templates",'purchase_template/index'); ?>
<!--br><?php //echo link_to("Terms",'terms/index'); ?>-->
<br><?php echo link_to("Products",'product/index'); ?>
<br><?php echo link_to("Product Types",'producttype/index'); ?>
<br><?php echo link_to("Customers",'customer/index'); ?>
<br><?php echo link_to("Vendors",'vendor/index'); ?>
<br><?php echo link_to("Passbooks",'passbook/index'); ?>
<br><?php echo link_to("Biller Types",'biller_type/index'); ?>
<br><?php echo link_to("Billers",'biller/index'); ?>
<br><?php echo link_to("Billees",'billee/index'); ?>
<br><?php echo link_to("Employees",'employee/index'); ?>
<br><?php echo link_to("Daily Sales Report Separate TJL",'invoice/dsrmultiForProductCategory'); ?>
<br>
<br>Calculators:
<br><?php echo link_to("Price List Recalculation",'product/recalc'); ?>
<br><?php echo link_to("Stock Recalculation",'product/recalcStock'); ?>
<br><?php echo link_to("Profit Recalculation",'profitdetail/calcProfit'); ?>
<br><?php echo link_to("Profit Report",'profitdetail/report'); ?>
<br><?php echo link_to("Product Quota Recalculation",'product/calcQuota'); ?>
<br><?php echo link_to("Stock Zero Out",'stock/zeroOut'); ?>
<br><?php echo link_to("Voucher Input Wizard",'voucher/wizardInput'); ?>
<br><?php echo link_to("Voucher Recode",'voucher/genCodeForAll'); ?>
<br><?php echo link_to("Voucher Reset Out Check ID",'voucher/resetOutCheckId'); ?>
<br><?php echo link_to("Regenerate Stock Entries for Purchase",'purchasedetail/regenStockentry'); ?>
<br><?php echo link_to("Regenerate Stock Entries for Invoice",'invoicedetail/regenStockentry'); ?>
<br><?php echo link_to("Recalculate Profit for Purchase",'purchasedetail/recalcProfit'); ?>
<br><?php echo link_to("Recalculate Invoices",'invoice/massRecalc'); ?>
<br><?php echo link_to("Recalculate Invoices for Product Category (TJL)",'invoice/massRecalcForProductCategory'); ?>
<br><?php echo link_to("Mass Generate Passbook Entries",'voucher/massGenPassbookEntry'); ?>
<br><?php echo link_to("Mass Generate Vouchers",'voucher/massGenerateVouchers'); ?>
<br>
<br>This is to fix purchases that are fully received but have null receive date
<br><?php echo link_to("Purchase Reset Receive Date",'purchase/resetReceiveDate'); ?> 
<br>

<?php if($sf_user->hasCredential(array('admin'), false)){?>
<br>Maintainance:
<br><?php echo link_to("Purchase Received Status Recalculation",'maintainance/calcPurchaseReceivedStatus'); ?>
<br><?php echo link_to("Set Color to Purchase Detail",'maintainance/setColorToPurchaseDetail'); ?>
<br><?php echo link_to("Receive all purchases",'maintainance/receiveAllPurchases'); ?>
<?php } ?>
  </td>
  <td width=50%>
<br>Accounting:
<br><?php echo link_to("Recalc Voucher Subaccount Fullnames",'voucher_subaccount/recalcFullname'); ?>
<!--
<br><?php echo link_to("Accounts",'account'); ?>
<br><?php echo link_to("Account Categories",'account'); ?>
<br><?php echo link_to("Journals",'journal'); ?>
<br><?php echo link_to("Income Statement",'invoice/incstate'); ?>
-->
<br>
<br>Product Conversion:
<!--br><a href="/productchange2/productchangedetector.php">Product Change</a-->
<br><?php echo link_to("Product Conversion Templates",'conversion/index'); ?>
<br><?php echo link_to("New Product Conversion Template",'conversion/new'); ?>
<br>
<br>Commission:
<br><?php echo link_to("Sales Commission",'profitdetail/commission'); ?> 
<br><?php echo link_to("Technician's Commission",'invoice/commission'); ?> 
<br><?php echo link_to("Sales Report by Salesman",'invoice/salesReportBySalesman'); ?> 
<br>
<br>Diagnostics:
<br><?php echo link_to("Purchase Double Payment Check",'purchase/doublePaymentCheck'); ?> 
<br><?php echo link_to("Invoice DR Diagnostics",'invoice/drDiagnostics'); ?> 
<br><?php echo link_to("Purchase DR Diagnostics",'purchase/drDiagnostics'); ?> 
<br>
<br>Product Type Management:
<br><?php echo link_to("Merge Product Types","producttype/mergeProductTypes")?>
<br><?php echo link_to("Mass Add Products to Product Types","producttype/massAddProducts")?>
<br><?php echo link_to("Mass Add Products from Miscellaneous to Product Types","producttype/miscellaneousSearch")?>
<br>

<br>Forecasting:
<br><?php echo link_to("Products sold annually per category","accounting/totalSalesTanksThisYear?year=2023&productcategory=TJL")?>
<br>


  </td>
</tr>
</table>