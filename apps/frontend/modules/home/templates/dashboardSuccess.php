<?php use_helper('I18N', 'Date') ?>
<?php include_partial('home/dashboarddatepicker',array("form"=>$form,"action"=>"dashboard")) ?>
<br>
<br>
<font color=red>
<h1><?php echo "Accounts Receivable from the last 12 months:".link_to(" P".MyDecimal::format($unpaidinvoicestotaloneyear),"home/unpaidinvoices?startdate=$oneyearago&enddate=$today");?></h1>
</font>
<table>
    <tr align="center">
      <td>
      </td>
      <td><h3>Unclosed<br>Invoices</h3></td>
      <td><h3>Unclosed<br>Purchases</h3></td>
      <td><h3>Unpaid<br>Invoices</h3></td>
      <td><h3>Unpaid<br>Purchases</h3></td>
      <td><h3>Unreleased<br>Invoice DRs</h3></td>
      <td><h3>Unreturned<br>Invoice DRs</h3></td>
      <td><h3>Unreceived<br>Purchase DRs</h3></td>
      <td><h3>Unreturned<br>Purchase DRs</h3></td>
    </tr>
    <tr align="center">
      <td><h3>Today<br>(<?php echo MyDateTime::frommysql($today)->toshortdate()?>)</h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedinvoicescounttoday." &nbsp;","home/unclosedinvoices?startdate=$today&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedpurchasescounttoday." &nbsp;","home/unclosedpurchases?startdate=$today&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidinvoicescounttoday." &nbsp;","home/unpaidinvoices?startdate=$today&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidpurchasescounttoday." &nbsp;","home/unpaidpurchases?startdate=$today&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreleasedinvoicedrscounttoday." &nbsp;","home/unreleasedinvoicedrs?startdate=$today&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedinvoicedrscounttoday." &nbsp;","home/unreturnedinvoicedrs?startdate=$today&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreceivedpurchasedrscounttoday." &nbsp;","home/unreceivedpurchasedrs?startdate=$today&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedpurchasedrscounttoday." &nbsp;","home/unreturnedpurchasedrs?startdate=$today&enddate=$today");?></h3></td>
    </tr>
    <tr align="center">
      <td><h3>Last 7 Days<br>(<?php echo MyDateTime::frommysql($sevendaysago)->toshortdate()." - ".MyDateTime::frommysql($today)->toshortdate() ?>)</h3></td></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedinvoicescountsevendays." &nbsp;","home/unclosedinvoices?startdate=$sevendaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedpurchasescountsevendays." &nbsp;","home/unclosedpurchases?startdate=$sevendaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidinvoicescountsevendays." &nbsp;","home/unpaidinvoices?startdate=$sevendaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidpurchasescountsevendays." &nbsp;","home/unpaidpurchases?startdate=$sevendaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreleasedinvoicedrscountsevendays." &nbsp;","home/unreleasedinvoicedrs?startdate=$sevendaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedinvoicedrscountsevendays." &nbsp;","home/unreturnedinvoicedrs?startdate=$sevendaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreceivedpurchasedrscountsevendays." &nbsp;","home/unreceivedpurchasedrs?startdate=$sevendaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedpurchasedrscountsevendays." &nbsp;","home/unreturnedpurchasedrs?startdate=$sevendaysago&enddate=$today");?></h3></td>
    </tr>
    <tr align="center">
      <td><h3>Last 30 Days<br>(<?php echo MyDateTime::frommysql($thirtydaysago)->toshortdate()." - ".MyDateTime::frommysql($today)->toshortdate()?>)</h3></td></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedinvoicescountthirtydays." &nbsp;","home/unclosedinvoices?startdate=$thirtydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedpurchasescountthirtydays." &nbsp;","home/unclosedpurchases?startdate=$thirtydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidinvoicescountthirtydays." &nbsp;","home/unpaidinvoices?startdate=$thirtydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidpurchasescountthirtydays." &nbsp;","home/unpaidpurchases?startdate=$thirtydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreleasedinvoicedrscountthirtydays." &nbsp;","home/unreleasedinvoicedrs?startdate=$thirtydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedinvoicedrscountthirtydays." &nbsp;","home/unreturnedinvoicedrs?startdate=$thirtydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreceivedpurchasedrscountthirtydays." &nbsp;","home/unreceivedpurchasedrs?startdate=$thirtydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedpurchasedrscountthirtydays." &nbsp;","home/unreturnedpurchasedrs?startdate=$thirtydaysago&enddate=$today");?></h3></td>
    </tr>
    <tr align="center">
      <td><h3>Last 90 Days<br>(<?php echo MyDateTime::frommysql($ninetydaysago)->toshortdate()." - ".MyDateTime::frommysql($today)->toshortdate()?>)</h3></td></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedinvoicescountninetydays." &nbsp;","home/unclosedinvoices?startdate=$ninetydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedpurchasescountninetydays." &nbsp;","home/unclosedpurchases?startdate=$ninetydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidinvoicescountninetydays." &nbsp;","home/unpaidinvoices?startdate=$ninetydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidpurchasescountninetydays." &nbsp;","home/unpaidpurchases?startdate=$ninetydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreleasedinvoicedrscountninetydays." &nbsp;","home/unreleasedinvoicedrs?startdate=$ninetydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedinvoicedrscountninetydays." &nbsp;","home/unreturnedinvoicedrs?startdate=$ninetydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreceivedpurchasedrscountninetydays." &nbsp;","home/unreceivedpurchasedrs?startdate=$ninetydaysago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedpurchasedrscountninetydays." &nbsp;","home/unreturnedpurchasedrs?startdate=$ninetydaysago&enddate=$today");?></h3></td>
    </tr>
    <tr align="center">
      <td><h3>Last Year<br>(<?php echo MyDateTime::frommysql($oneyearago)->toshortdate()." - ".MyDateTime::frommysql($today)->toshortdate()?>)</h3></td></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedinvoicescountoneyear." &nbsp;","home/unclosedinvoices?startdate=$oneyearago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedpurchasescountoneyear." &nbsp;","home/unclosedpurchases?startdate=$oneyearago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidinvoicescountoneyear." &nbsp;","home/unpaidinvoices?startdate=$oneyearago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidpurchasescountoneyear." &nbsp;","home/unpaidpurchases?startdate=$oneyearago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreleasedinvoicedrscountoneyear." &nbsp;","home/unreleasedinvoicedrs?startdate=$oneyearago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedinvoicedrscountoneyear." &nbsp;","home/unreturnedinvoicedrs?startdate=$oneyearago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreceivedpurchasedrscountoneyear." &nbsp;","home/unreceivedpurchasedrs?startdate=$oneyearago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedpurchasedrscountoneyear." &nbsp;","home/unreturnedpurchasedrs?startdate=$oneyearago&enddate=$today");?></h3></td>
    </tr>
    <tr align="center">
      <td><h3>Last 10 Years<br>(<?php echo MyDateTime::frommysql($tenyearsago)->toshortdate()." - ".MyDateTime::frommysql($today)->toshortdate()?>)</h3></td></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedinvoicescounttenyears." &nbsp;","home/unclosedinvoices?startdate=$tenyearsago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unclosedpurchasescounttenyears." &nbsp;","home/unclosedpurchases?startdate=$tenyearsago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidinvoicescounttenyears." &nbsp;","home/unpaidinvoices?startdate=$tenyearsago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unpaidpurchasescounttenyears." &nbsp;","home/unpaidpurchases?startdate=$tenyearsago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreleasedinvoicedrscounttenyears." &nbsp;","home/unreleasedinvoicedrs?startdate=$tenyearsago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedinvoicedrscounttenyears." &nbsp;","home/unreturnedinvoicedrs?startdate=$tenyearsago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreceivedpurchasedrscounttenyears." &nbsp;","home/unreceivedpurchasedrs?startdate=$tenyearsago&enddate=$today");?></h3></td>
      <td><h3><?php echo link_to("&nbsp; ".$unreturnedpurchasedrscounttenyears." &nbsp;","home/unreturnedpurchasedrs?startdate=$tenyearsago&enddate=$today");?></h3></td>
    </tr>
</table>
