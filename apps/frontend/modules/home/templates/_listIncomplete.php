<?php
$red="#faa";
$yellow="#ffc";
$green="#6d6";
$plain="#eee";
?>
<h2>Incomplete</h2>

<table border=1>
  <tr>
    <td>Supplier</td>
    <td>Po No.</td>
    <td>Sold To</td>
    <td>Supplier<br>Invoice</td>
    <td>Received</td>
    <td>Status</td>
  </tr>
    <?php foreach($purchases as $purchase){
        $purchase_details=$purchase->getPurchasedetail();
        ?>
    <tr>
      <td><?php echo $purchase->getVendor()//." ".$purchase->getVendorName() ?></td>
      <td><?php echo link_to($purchase->getPurchaseTemplate()."<br>".$purchase->getPono(),"purchase/view?id=".$purchase->getId()) ?></td>
<!--
      <td><?php 
        //.($purchase->getCheque()?("; Check No.:".$purchase->getCheque().": ".$purchase->getChequedate()):"") 
        //print each product as a link
        foreach($purchase_details as $pd)
          echo link_to($pd->getProduct()->getName(),"product/view?id=".$pd->getProductId())." <br> ";
      ?></td>
      <td><?php echo MyDecimal::format($purchase->getTotal()) ?></td>
      <td><?php echo $purchase->getTerms() ?> / <?php echo MyDateTime::frommysql($purchase->getDuedate())->toshortdate() ?></td>
      <td><?php echo $purchase->getEmployee() ?></td>

-->
      <!-----------SOLD TO--------->
      <?php 
        $invoice_ids=$purchase->getInvoiceIds();
        $profit_details=$purchase->getProfitDetails();
        $sold_count=0;
        foreach($profit_details as $pd)
        {
          $sold_count+=$pd->getQty();
        }
        $item_count=0;
        foreach($purchase_details as $pd)
        {
          $item_count+=$pd->getQty();
        }
        switch ($sold_count) {
          case 0: $bgcolor=$red;break;
          case $item_count: $bgcolor=$green;break;
          default: $bgcolor=$yellow;break;
        }
      ?>
      <td bgcolor="<?php echo $bgcolor;?>">
        <?php
          echo $sold_count."/".$item_count.": ";
          echo $purchase->getInvno().": "; 
          if($invoice_ids!=null)
          {
            $invnos=explode(",",$purchase->getInvnos());
            $invoice_ids=explode(",",$invoice_ids);
            for($i=0;$i<count($invnos);$i++) 
              echo link_to($invnos[$i],"invoice/view?id=".$invoice_ids[$i])." ";
          }
        ?>
        
      </td>
      <!-----------SUPPLIER INVOICE--------->
      <?php 
        $vi=$purchase->getVendorInvoice();
        $imagecount=$purchase->getImageCount();
        if(trim($vi)!=""){?>
          <td bgcolor="<?php echo $green;?>"><?php echo $vi?></td>
        <?php }else if($imagecount!=0){?>
          <td bgcolor="<?php echo $yellow;?>">Image taken</td>
      <?php }else{?>
          <td bgcolor="<?php echo $red;?>"><?php echo link_to("None","purchase/view?id=".$purchase->getId());?></td>
      <?php }?>
      <!--
      <td><?php //$cr=$purchase->getCounterReceipt();if($cr)echo link_to($cr->getCode(),"counter_receipt/view?id=".$cr->getId())?></td>
      -->
      <!-----------RECEIVE STATUS--------->
      <?php 
        $receivestatus=$purchase->getReceivedStatus();
        switch ($receivestatus) {
          case "Not Received": $bgcolor=$red;break;
          case "Partially Received": $bgcolor=$yellow;break;
          case "Fully Received": $bgcolor=$green;break;
        }
        if($receivestatus!="Fully Received"){?>
          <td bgcolor="<?php echo $bgcolor;?>"><?php echo link_to($receivestatus,"purchase/view?id=".$purchase->getId());?></td>
        <?php }else{?>
          <td bgcolor="<?php echo $bgcolor;?>"><?php echo $receivestatus;?></td>
        <?php }?>
      <!-----------STATUS--------->
      <?php
          $status=$purchase->getStatus();
          if($status=="Cancelled") $bgcolor=$red;
          else $bgcolor=$plain;
      ?>
      <td bgcolor="<?php echo $bgcolor;?>"><?php echo $status; ?></td>
    </tr>
    <?php }?>
</table>