<?php include_partial('home/dashboarddatepickermulti',array("form"=>$form,"toform"=>$toform,"action"=>"unclosedinvoices")) ?>

<h2>Unclosed Invoices</h2>
<h3>From <?php echo MyDateTime::frommysql($startdate)->toshortdate();?> to <?php echo MyDateTime::frommysql($enddate)->toshortdate();?></h3>

<table border=1>
  <tr>
    <td>Ref</td>
    <td>Date</td>
    <td>Customer</td>
    <td>Salesman</td>
    <td>Particulars</td>
    <td>Total Price</td>
    <td>Status</td>
    <td>Notes</td>
  </tr>
  <?php foreach($items as $invoice){?>
  <tr>
      <td><?php echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
      <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId()) ?></td>
      <td><?php echo $invoice->getEmployee() ?></td>
      <td><?php echo $invoice->getParticularsString().($invoice->getCheque()?("; Check No.:".$invoice->getCheque().": ".$invoice->getChequedate()):"") ?></td>
      <td><?php echo $invoice->getTotal() ?></td>
      <td><?php echo $invoice->getStatus() ?></td>
      <td><?php echo $invoice->getNotes() ?></td>
  </tr>
  <?php }?>
</table>
