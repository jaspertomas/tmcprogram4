<?php include_partial('home/dashboarddatepickermulti',array("form"=>$form,"toform"=>$toform,"action"=>"unclosedpurchases")) ?>

<h2>Unclosed Purchases</h2>
<h3>From <?php echo MyDateTime::frommysql($startdate)->toshortdate();?> to <?php echo MyDateTime::frommysql($enddate)->toshortdate();?></h3>

<table border=1>
  <tr>
    <td>Ref</td>
    <td>Date</td>
    <td>Supplier</td>
    <td>Salesman</td>
    <td>Particulars</td>
    <td>Total Price</td>
    <td>Status</td>
    <td>Notes</td>
  </tr>
  <?php foreach($items as $purchase){?>
  <tr>
      <td><?php echo link_to($purchase,"purchase/view?id=".$purchase->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate() ?></td>
      <td><?php echo link_to($purchase->getVendor(),"vendor/view?id=".$purchase->getVendorId()) ?></td>
      <td><?php echo $purchase->getEmployee() ?></td>
      <td><?php echo $purchase->getParticularsString().($purchase->getCheque()?("; Check No.:".$purchase->getCheque().": ".$purchase->getChequedate()):"") ?></td>
      <td><?php echo $purchase->getTotal() ?></td>
      <td><?php echo $purchase->getStatus() ?></td>
      <td><?php echo $purchase->getMemo() ?></td>
  </tr>
  <?php }?>
</table>
