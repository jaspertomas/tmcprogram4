<?php use_helper('I18N', 'Date') ?>

<?php include_partial('home/invoicelistdatepicker',array("form"=>$form)) ?>


<table border=1 width="800">
<tr>
  <td width="50%"><h2><center>Invoices</center></h2></td>
  <td width="50%"><h2><center><?php echo link_to("Purchases","home/purchaselist") ?></center></h2></td>
</tr>
</table>

<h2>Invoices</h2>

<table border=1>
    <tr valign="top">
        <td>
        <?php include_partial('home/invoices_forapproval',array("invoices"=>$invoices)) ?>
        </td>
        <td>
        <?php include_partial('home/invoices_checkedout',array("invoices"=>$invoices)) ?>
        </td>
        <td>
        <?php include_partial('home/invoices_new',array("invoices"=>$invoices)) ?>
        </td>
        <td>
        <?php include_partial('home/invoices_closed',array("invoices"=>$invoices)) ?>
        </td>
        <td>
        <?php include_partial('home/invoices_cancelled',array("invoices"=>$invoices)) ?>
        </td>
    </tr>
</table>
