<?php include_partial('home/dashboarddatepickermulti',array("form"=>$form,"toform"=>$toform,"action"=>"unreturnedinvoicedrs")) ?>

<h2>Unreturned Invoice DRs</h2>
<h3>From <?php echo MyDateTime::frommysql($startdate)->toshortdate();?> to <?php echo MyDateTime::frommysql($enddate)->toshortdate();?></h3>

<table border=1>
  <tr>
    <td>Ref</td>
    <td>Date</td>
    <td>Customer</td>
    <td>Salesman</td>
    <td>Particulars</td>
    <td>Status</td>
    <td>Notes</td>
  </tr>
  <?php foreach($items as $invoice_dr){?>
  <tr>
      <td><?php echo link_to($invoice_dr,"invoice_dr/view?id=".$invoice_dr->getId()) ?></td>
      <td><?php echo MyDateTime::fromdatetime($invoice_dr->getDatetime())->toshortdate() ?></td>
      <td><?php echo link_to($invoice_dr->getCustomer(),"customer/view?id=".$invoice_dr->getCustomerId()) ?></td>
      <td><?php //echo $invoice_dr->getEmployee() ?></td>
      <td><?php echo $invoice_dr->getParticularsString() ?></td>
      <td><?php //echo $invoice_dr->getStatus() ?></td>
      <td><?php echo $invoice_dr->getNotes() ?></td>
  </tr>
  <?php }?>
</table>
