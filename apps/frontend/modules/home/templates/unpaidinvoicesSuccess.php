<?php include_partial('home/dashboarddatepickermulti',array("form"=>$form,"toform"=>$toform,"action"=>"unpaidinvoices")) ?>

<h2>Unpaid Invoices</h2>
<h3>From <?php echo MyDateTime::frommysql($startdate)->toshortdate();?> to <?php echo MyDateTime::frommysql($enddate)->toshortdate();?></h3>

<table border=1>
  <tr>
    <td>Ref</td>
    <td>Date</td>
    <td>Customer</td>
    <td>Days</td>
    <td>Check Date</td>
    <td>Tanks</td>
    <td>Pumps</td>
    <td>Total</td>
    <td>Due Date</td>
    <td>Salesman</td>
    <td>Countered</td>
    <td>Status</td>
    <td>Particulars</td>
    <td>Notes</td>
  </tr>
  <?php foreach($items as $invoice){?>
  <tr>
      <td><?php echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ?></td>
      <td><?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?></td>
      <td><?php echo link_to($invoice->getCustomer(),"customer/view?id=".$invoice->getCustomerId()) ?></td>
      <td><?php echo $invoice->getTerms() ?></td>
      <td><?php  ?></td>
      <td><?php echo $invoice->getTotalForTanks() ?></td>
      <td><?php echo $invoice->getTotalForPumps() ?></td>
      <td><?php echo $invoice->getTotal() ?></td>
      <td><?php if($invoice->getStatus()=="Pending")echo MyDateTime::frommysql($invoice->getDuedate())->toshortdate() ?></td>
      <td><?php echo $invoice->getEmployee() ?></td>
      <td><?php  ?></td>
      <td><?php echo $invoice->getStatus() ?></td>
      <td><?php echo $invoice->getParticularsString().($invoice->getCheque()?("; Check No.:".$invoice->getCheque().": ".$invoice->getChequedate()):"") ?></td>
      <td><?php echo $invoice->getNotes() ?></td>
  </tr>
  <?php }?>
</table>
