<?php include_partial('home/dashboarddatepickermulti',array("form"=>$form,"toform"=>$toform,"action"=>"unreceivedpurchasedrs")) ?>

<h2>Unreceived Purchase DRs</h2>
<h3>From <?php echo MyDateTime::frommysql($startdate)->toshortdate();?> to <?php echo MyDateTime::frommysql($enddate)->toshortdate();?></h3>

<table border=1>
  <tr>
    <td>Ref</td>
    <td>Date</td>
    <td>Supplier</td>
    <td>Salesman</td>
    <td>Particulars</td>
    <td>Status</td>
    <td>Notes</td>
  </tr>
  <?php foreach($items as $purchase_dr){?>
  <tr>
      <td><?php echo link_to($purchase_dr,"purchase_dr/view?id=".$purchase_dr->getId()) ?></td>
      <td><?php echo MyDateTime::fromdatetime($purchase_dr->getDatetime())->toshortdate() ?></td>
      <td><?php echo link_to($purchase_dr->getVendor(),"vendor/view?id=".$purchase_dr->getVendorId()) ?></td>
      <td><?php //echo $purchase_dr->getEmployee() ?></td>
      <td><?php echo $purchase_dr->getParticularsString() ?></td>
      <td><?php //echo $purchase_dr->getStatus() ?></td>
      <td><?php echo $purchase_dr->getNotes() ?></td>
  </tr>
  <?php }?>
</table>
