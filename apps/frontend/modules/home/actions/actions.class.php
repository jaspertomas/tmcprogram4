<?php

/**
 * home actions.
 *
 * @package    sf_sandbox
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
  if($this->getUser()->hasCredential(array('admin','encoder'),false))
  $this->redirect("home/dashboard");
  elseif($this->getUser()->hasCredential(array('inventory'),false))
  $this->redirect("invoice/dashboard");
  elseif($this->getUser()->hasCredential(array('cashier'),false))
  $this->redirect("invoice/list");
  elseif($this->getUser()->hasCredential(array('sales'),false))
  $this->redirect("invoice/list");
	}
  public function executeError(sfWebRequest $request)
  {
  	$this->msg=$request->getParameter("msg");

  }
  public function executeAdvanced(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
  }
  public function executeEncoderTools(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin','encoder'),false))
      return $this->redirect("home/error?msg=Access Denied");
  }
  public function executeAccountingTools(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin','encoder'),false))
      return $this->redirect("home/error?msg=Access Denied");
  }
  public function executeWarranty(sfWebRequest $request)
  {
    $invoice=new Invoice();
    $invoice->setDate(MyDate::today());
    $this->form=new InvoiceForm($invoice);
  }
  public function executeWarrantyPdf(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");
    $this->date=$requestparams["date"];

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeInvoicelist(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("date"))
      $invoice->setDate($request->getParameter("date"));
    elseif(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->invoice=$invoice;

    $this->invoices=Doctrine_Query::create()
        ->from('Invoice i')
        ->andWhere('date="'.$this->invoice->getDate().'"')
        ->execute();
        
  
  }

  public function executePurchaselist(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("date"))
      $invoice->setDate($request->getParameter("date"));
    elseif(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->invoice=$invoice;

    $this->purchases=Doctrine_Query::create()
      ->from('Purchase p')
    	// ->where('p.invno = ""')
      ->andWhere('p.date="'.$this->invoice->getDate().'"')
    	->execute();
  
    //this is for unreceived or not yet arrived purchases
    $this->unreceivedpurchases=Doctrine_Query::create()
        ->from('Purchase p')
      	->where('received_status!="Fully Received"')
      	->andWhere('status!="Cancelled"')
      	->orderBy('p.vendor_id desc, p.date desc')
      	->limit(20)
      	->execute();

  	//put vendors into an array, sorted by id
  	$this->vendors=array();
  	$this->vendorpurchases=array();
  	foreach($this->purchases as $purchase)
  	{
      if(!isset($this->vendors[$purchase->getVendorId()]))
    	  $this->vendors[$purchase->getVendorId()]=$purchase->getVendor();
    	//put purchases into a 2d array under the same id as the vendor
  	  $this->vendorpurchases[$purchase->getVendorId()][]=$purchase;
  	}
  
  }  

  public function executeDashboard(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("date"))
      $invoice->setDate($request->getParameter("date"));
    else if(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    //$invoice->getDate() sometimes needs zero padding for month
    //do this to fix that
    $this->today=$invoice->getDate();
    $this->sevendaysago=MyDateTime::frommysql($this->today)->adddays(-7)->tomysql();
    $this->thirtydaysago=MyDateTime::frommysql($this->today)->adddays(-30)->tomysql();
    $this->ninetydaysago=MyDateTime::frommysql($this->today)->adddays(-90)->tomysql();
    $this->oneyearago=MyDateTime::frommysql($this->today)->adddays(-365)->tomysql();
    $this->tenyearsago=MyDateTime::frommysql($this->today)->adddays(-3650)->tomysql();
    //------------INVOICES--------------------
    $this->unclosedinvoicescounttoday = InvoiceTable::countUnclosedByDate($this->today);
    $this->unclosedpurchasescounttoday = PurchaseTable::countUnclosedByDate($this->today);
    $this->unclosedinvoicescountsevendays = InvoiceTable::countUnclosedByDateRange($this->sevendaysago, $this->today);
    $this->unclosedpurchasescountsevendays = PurchaseTable::countUnclosedByDateRange($this->sevendaysago, $this->today);
    $this->unclosedinvoicescountthirtydays = InvoiceTable::countUnclosedByDateRange($this->thirtydaysago, $this->today);
    $this->unclosedpurchasescountthirtydays = PurchaseTable::countUnclosedByDateRange($this->thirtydaysago, $this->today);
    $this->unclosedinvoicescountninetydays = InvoiceTable::countUnclosedByDateRange($this->ninetydaysago, $this->today);
    $this->unclosedpurchasescountninetydays = PurchaseTable::countUnclosedByDateRange($this->ninetydaysago, $this->today);
    $this->unclosedinvoicescountoneyear = InvoiceTable::countUnclosedByDateRange($this->oneyearago, $this->today);
    $this->unclosedpurchasescountoneyear = PurchaseTable::countUnclosedByDateRange($this->oneyearago, $this->today);

    $this->unpaidinvoicescounttoday = InvoiceTable::countUnpaidByDate($this->today);
    $this->unpaidpurchasescounttoday = PurchaseTable::countUnpaidByDate($this->today);
    $this->unpaidinvoicescountsevendays = InvoiceTable::countUnpaidByDateRange($this->sevendaysago, $this->today);
    $this->unpaidpurchasescountsevendays = PurchaseTable::countUnpaidByDateRange($this->sevendaysago, $this->today);
    $this->unpaidinvoicescountthirtydays = InvoiceTable::countUnpaidByDateRange($this->thirtydaysago, $this->today);
    $this->unpaidpurchasescountthirtydays = PurchaseTable::countUnpaidByDateRange($this->thirtydaysago, $this->today);
    $this->unpaidinvoicescountninetydays = InvoiceTable::countUnpaidByDateRange($this->ninetydaysago, $this->today);
    $this->unpaidpurchasescountninetydays = PurchaseTable::countUnpaidByDateRange($this->ninetydaysago, $this->today);
    $this->unpaidinvoicescountoneyear = InvoiceTable::countUnpaidByDateRange($this->oneyearago, $this->today);
    $this->unpaidinvoicestotaloneyear = InvoiceTable::totalUnpaidByDateRange($this->oneyearago, $this->today);
    $this->unpaidpurchasescountoneyear = PurchaseTable::countUnpaidByDateRange($this->oneyearago, $this->today);

    $this->unreleasedinvoicedrscounttoday = InvoiceDrTable::countUnreleasedByDate($this->today);
    $this->unreceivedpurchasedrscounttoday = PurchaseDrTable::countUnreceivedByDate($this->today);
    $this->unreleasedinvoicedrscountsevendays = InvoiceDrTable::countUnreleasedByDateRange($this->sevendaysago, $this->today);
    $this->unreceivedpurchasedrscountsevendays = PurchaseDrTable::countUnreceivedByDateRange($this->sevendaysago, $this->today);
    $this->unreleasedinvoicedrscountthirtydays = InvoiceDrTable::countUnreleasedByDateRange($this->thirtydaysago, $this->today);
    $this->unreceivedpurchasedrscountthirtydays = PurchaseDrTable::countUnreceivedByDateRange($this->thirtydaysago, $this->today);
    $this->unreleasedinvoicedrscountninetydays = InvoiceDrTable::countUnreleasedByDateRange($this->ninetydaysago, $this->today);
    $this->unreceivedpurchasedrscountninetydays = PurchaseDrTable::countUnreceivedByDateRange($this->ninetydaysago, $this->today);
    $this->unreleasedinvoicedrscountoneyear = InvoiceDrTable::countUnreleasedByDateRange($this->oneyearago, $this->today);
    $this->unreceivedpurchasedrscountoneyear = PurchaseDrTable::countUnreceivedByDateRange($this->oneyearago, $this->today);

    $this->unreturnedinvoicedrscounttoday = InvoiceDrTable::countUnreturnedByDate($this->today);
    $this->unreturnedpurchasedrscounttoday = PurchaseDrTable::countUnreturnedByDate($this->today);
    $this->unreturnedinvoicedrscountsevendays = InvoiceDrTable::countUnreturnedByDateRange($this->sevendaysago, $this->today);
    $this->unreturnedpurchasedrscountsevendays = PurchaseDrTable::countUnreturnedByDateRange($this->sevendaysago, $this->today);
    $this->unreturnedinvoicedrscountthirtydays = InvoiceDrTable::countUnreturnedByDateRange($this->thirtydaysago, $this->today);
    $this->unreturnedpurchasedrscountthirtydays = PurchaseDrTable::countUnreturnedByDateRange($this->thirtydaysago, $this->today);
    $this->unreturnedinvoicedrscountninetydays = InvoiceDrTable::countUnreturnedByDateRange($this->ninetydaysago, $this->today);
    $this->unreturnedpurchasedrscountninetydays = PurchaseDrTable::countUnreturnedByDateRange($this->ninetydaysago, $this->today);
    $this->unreturnedinvoicedrscountoneyear = InvoiceDrTable::countUnreturnedByDateRange($this->oneyearago, $this->today);
    $this->unreturnedpurchasedrscountoneyear = PurchaseDrTable::countUnreturnedByDateRange($this->oneyearago, $this->today);

    $this->unclosedinvoicescounttenyears = InvoiceTable::countUnclosedByDateRange($this->tenyearsago, $this->today);
    $this->unclosedpurchasescounttenyears = PurchaseTable::countUnclosedByDateRange($this->tenyearsago, $this->today);
    $this->unpaidinvoicescounttenyears = InvoiceTable::countUnpaidByDateRange($this->tenyearsago, $this->today);
    $this->unpaidpurchasescounttenyears = PurchaseTable::countUnpaidByDateRange($this->tenyearsago, $this->today);
    $this->unreleasedinvoicedrscounttenyears = InvoiceDrTable::countUnreleasedByDateRange($this->tenyearsago, $this->today);
    $this->unreceivedpurchasedrscounttenyears = PurchaseDrTable::countUnreceivedByDateRange($this->tenyearsago, $this->today);
    $this->unreturnedinvoicedrscounttenyears = InvoiceDrTable::countUnreturnedByDateRange($this->tenyearsago, $this->today);
    $this->unreturnedpurchasedrscounttenyears = PurchaseDrTable::countUnreturnedByDateRange($this->tenyearsago, $this->today);

    // $this->events = EventTable::fetchByDatenParentclass($this->today,"Invoice");
    // $this->vouchers = VoucherTable::fetchPettyCashVouchersByDate($this->today);

    // $this->idrs = InvoiceDrTable::countUnreleasedByDate($invoice->getDate());
    // $this->pdrs = PurchaseDrTable::countUnreceivedByDate($invoice->getDate());
  }
  public function executeUnclosedinvoices(sfWebRequest $request)
  {
    $this->processMultiDateParams($request);
    $this->items = InvoiceTable::fetchUnclosedByDateRange($this->startdate, $this->enddate);
  }
  public function executeUnclosedpurchases(sfWebRequest $request)
  {
    $this->processMultiDateParams($request);
    $this->items = PurchaseTable::fetchUnclosedByDateRange($this->startdate, $this->enddate);
  }
  public function executeUnpaidinvoices(sfWebRequest $request)
  {
    $this->processMultiDateParams($request);
    $this->items = InvoiceTable::fetchUnpaidByDateRange($this->startdate, $this->enddate);
  }
  public function executeUnpaidpurchases(sfWebRequest $request)
  {
    $this->processMultiDateParams($request);
    $this->items = PurchaseTable::fetchUnpaidByDateRange($this->startdate, $this->enddate);
  }
  public function executeUnreleasedinvoicedrs(sfWebRequest $request)
  {
    $this->processMultiDateParams($request);
    $this->items = InvoiceDrTable::fetchUnreleasedByDateRange($this->startdate, $this->enddate);
  }
  public function executeUnreturnedinvoicedrs(sfWebRequest $request)
  {
    $this->processMultiDateParams($request);
    $this->items = InvoiceDrTable::fetchUnreturnedByDateRange($this->startdate, $this->enddate);
  }
  public function executeUnreceivedpurchasedrs(sfWebRequest $request)
  {
    $this->processMultiDateParams($request);
    $this->items = PurchaseDrTable::fetchUnreceivedByDateRange($this->startdate, $this->enddate);
  }
  public function executeUnreturnedpurchasedrs(sfWebRequest $request)
  {
    $this->processMultiDateParams($request);
    $this->items = PurchaseDrTable::fetchUnreturnedByDateRange($this->startdate, $this->enddate);
  }

  function processMultiDateParams($request)
  {
    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if($request->hasParameter("startdate"))
      $invoice->setDate($request->getParameter("startdate"));
    else if(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    $this->form=new InvoiceForm($invoice);
    $this->startdate=$invoice->getDate();

    $requestparams=$request->getParameter("purchase");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];
    
    $purchase=new Purchase();
    if($request->hasParameter("enddate"))
      $purchase->setDate($request->getParameter("enddate"));
    else if(!$day or !$month or !$year)
      $purchase->setDate(MyDate::today());
    else
      $purchase->setDate($year."-".$month."-".$day);
    
    $this->toform=new PurchaseForm($purchase);
    $this->enddate=$purchase->getDate();
  }
  public function executeBundyClockConvert(sfWebRequest $request)
  {
    //do this only if methid is POST
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $input=$request->getParameter("input");
      $oldOutput=$request->getParameter("output");
      $outputByDate=array();

      //split old output into lines and segments
      $oldOutputLines=explode("\r\n",$oldOutput);
      $outputSegments=array();
      foreach($oldOutputLines as $line)
      {
        $outputSegments=explode("\t",str_replace("     ","\t",$line));
        $date=trim($outputSegments[3]);
        if(!isset($outputByDate[$date]))$outputByDate[$date]=array();
        $outputByDate[$date][]=$line."\r\n";
      }
  
      //split input into lines and segments
      $lines=explode("\r\n",$input);
      $segments=array();
      foreach($lines as $line)
      {
        $segments[]=explode("\t",$line);
      }
  
      //get employee names
      $names=array();
      $names[]=trim($segments[3][9]);
      $names[]=trim($segments[3][24]);
      $names[]=trim($segments[3][39]);

      //get start date and enddate
      $datesegments=explode("~",$segments[4][1]);
      $startdate=$datesegments[0];
      $enddate=$datesegments[1];
  
      //get start month and year
      $startdatesegments=explode("-",$startdate);
      $year=$startdatesegments[0];
      $month=$startdatesegments[1];
      $month=str_pad($month,2,"0",STR_PAD_LEFT);
      $day=$startdatesegments[2];

      //for each row
      for($i=27;$i<count($segments);$i++)
      {
        //if blank, skip
        if(trim($segments[$i][0])=="")continue;

        //get first column and remove the weekdate
        $datesegments=explode(" ",$segments[$i][0]);
        $day=$datesegments[0];
        $day=str_pad($day,2,"0",STR_PAD_LEFT);

        //increment month if day goes back to 01
        //increment year if month goes to 13
        //if not the first row, and date is 01, 
        if($i>28 and $day=="01")
        {
          //move month forward by 1
          $month++;

          //if month is 13, month should be 1, increment year
          if($month==13){$month=1;$year++;}

          //add 0 padding
          $month=str_pad($month,2,"0",STR_PAD_LEFT);
        }

        //this is the date for the current row
        $date=$year."-".$month."-".$day;

        //process time for employee 1
        //if name is not blank, convert row into logdata string
        if($names[0]!="")
        {
          $times=array();
          $times[]=$segments[$i][1];
          $times[]=$segments[$i][3];
          $times[]=$segments[$i][6];
          $times[]=$segments[$i][8];
          $times[]=$segments[$i][10];
          $times[]=$segments[$i][12];
          if(!isset($outputByDate[$date]))$outputByDate[$date]=array();
          $outputByDate[$date]=array_merge($outputByDate[$date],$this->calcTimes($names[0], $date, $times));
        }

        //if name is not blank, convert row into logdata string
        if($names[1]!="")
        {
          $times=array();
          $times[]=$segments[$i][16];
          $times[]=$segments[$i][18];
          $times[]=$segments[$i][21];
          $times[]=$segments[$i][23];
          $times[]=$segments[$i][25];
          $times[]=$segments[$i][27];
          if(!isset($outputByDate[$date]))$outputByDate[$date]=array();
          $outputByDate[$date]=array_merge($outputByDate[$date],$this->calcTimes($names[1], $date, $times));
        }

        //if name is not blank, convert row into logdata string
        if($names[2]!="")
        {
          $times=array();
          $times[]=$segments[$i][31];
          $times[]=$segments[$i][33];
          $times[]=$segments[$i][36];
          $times[]=$segments[$i][38];
          $times[]=$segments[$i][40];
          $times[]=$segments[$i][42];
          if(!isset($outputByDate[$date]))$outputByDate[$date]=array();
          $outputByDate[$date]=array_merge($outputByDate[$date],$this->calcTimes($names[2], $date, $times));
        }
      }

      //arrange by date
      ksort($outputByDate, SORT_STRING);

      //convert outputByDate into one long string
      $this->output="";
      foreach($outputByDate as $dateset)
      {
        foreach($dateset as $line)
        {
          $this->output.=$line;
        }
      }

    }
  }
  function calcTimes($name, $date, $times)
  {
    $output=array();
    $starttime="";
    $endtime="";
    //find first non-empty time, prepopulate start and end time with it
    //if you find it, exit loop and go to the next ine
    for($j=0;$j<6;$j++)
    {
      $time=$times[$j];
      if($time=="")continue;
      $starttime=$time;
      $endtime=$time;
      break;
    }
    //if none, return none
    if($j==6)return array();
    //get earliest and latest times. These become start and end times
    for($j++;$j<6;$j++)
    {
      $time=$times[$j];
      if($time=="")continue;
      if(MyTime::isEarlierThan($time,$starttime))$starttime=$time;
      if(MyTime::isLaterThan($time,$endtime))$endtime=$time;
    }
    $output[]="00	".$name."	TWS	 ".$date."     ".$starttime."	0\n";
    //only print endtime if endtime not the same as starttime
    if($starttime!=$endtime)
      $output[]="00	".$name."	TWS	 ".$date."     ".$endtime."	0\n";
    return $output;
  }
}
