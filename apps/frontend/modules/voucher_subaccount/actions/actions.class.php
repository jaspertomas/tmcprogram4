<?php

require_once dirname(__FILE__).'/../lib/voucher_subaccountGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/voucher_subaccountGeneratorHelper.class.php';

/**
 * voucher_subaccount actions.
 *
 * @package    sf_sandbox
 * @subpackage voucher_subaccount
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class voucher_subaccountActions extends autoVoucher_subaccountActions
{
	//this is by voucher date
	public function executeReport(sfWebRequest $request)
	{
		list($this->startdateform, $this->enddateform) = MyDateRangeHelper::getForms($request);
		$this->vsa=Fetcher::fetchOne("VoucherSubaccount",array("id"=>$request->getParameter("id")));
		$startdate=$this->startdateform->getObject()->getDate();
		$enddate=$this->enddateform->getObject()->getDate();

		$this->vouchers=Doctrine_Query::create()
			->from('Voucher v')
			->where('subaccount_id='.$this->vsa->getId())
			->andWhere('date >="'.$startdate.'"')
			->andWhere('date <="'.$enddate.'"')
			->orderBy('date')
			->execute();
	}
	public function executeMassReallocate(sfWebRequest $request)
	{
		list($this->startdateform, $this->enddateform) = MyDateRangeHelper::getForms($request);
		$this->vsa=Fetcher::fetchOne("VoucherSubaccount",array("id"=>$request->getParameter("id")));
		$startdate=$this->startdateform->getObject()->getDate();
		$enddate=$this->enddateform->getObject()->getDate();

		$this->vouchers=Doctrine_Query::create()
			->from('Voucher v')
			->where('subaccount_id='.$this->vsa->getId())
			->andWhere('date >="'.$startdate.'"')
			->andWhere('date <="'.$enddate.'"')
			->orderBy('date')
			->execute();

		Doctrine_Query::create()
			->update('Voucher v')
			->set('v.voucher_allocation_id',$this->vsa->getVoucherAllocationId())
			->where('subaccount_id='.$this->vsa->getId())
			->andWhere('date >="'.$startdate.'"')
			->andWhere('date <="'.$enddate.'"')
			->execute();
		$this->redirect($request->getReferer());
	}
	public function executeReportByYear(sfWebRequest $request)
	{
		$this->year=$request->getParameter("year");
		$this->vsa=Fetcher::fetchOne("VoucherSubaccount",array("id"=>$request->getParameter("id")));

		$this->vouchers=Doctrine_Query::create()
			->from('Voucher v')
			->where('subaccount_id='.$this->vsa->getId())
			->andWhere('year ="'.$this->year.'"')
			->orderBy('date')
			->execute();
	}
	public function executeReportByMonth(sfWebRequest $request)
	{
		$this->year=$request->getParameter("year");
		$this->month=intval($request->getParameter("month"));
		$this->vsa=Fetcher::fetchOne("VoucherSubaccount",array("id"=>$request->getParameter("id")));

		$this->vouchers=Doctrine_Query::create()
			->from('Voucher v')
			->where('subaccount_id='.$this->vsa->getId())
			->andWhere('year ='.$this->year)
			->andWhere('month ='.$this->month)
			->orderBy('date')
			->execute();
	}
	protected function processForm(sfWebRequest $request, sfForm $form)
	{
  
	  $requestparams=$request->getParameter($form->getName());
	  $requestparams["name"]=trim(str_replace(" (redirect)","",$requestparams["name"]));
	  if($requestparams["redirect_id"]!=null)$requestparams["name"]=$requestparams["name"]." (redirect)";
  
	  //if voucher type is check, passbook id cannot be empty
	  $voucher_type=Fetcher::fetchOne("VoucherType",array("id"=>$requestparams["voucher_type_id"]));
	  if($voucher_type->getName()=="Cheque" and $requestparams["passbook_id"]==null)
	  {
		$this->redirect("home/error?msg=Voucher Type is Cheque. Bank account cannot be empty.");
	  }
	  else if(($voucher_type->getName()!="Cheque" and $voucher_type->getName()!="Bank Transfer") and $requestparams["passbook_id"]!=null)
	  {
		$this->redirect("home/error?msg=Voucher Type is not Cheque / Bank Transfer. Bank should be empty.");
	  }

	  //calculate depth
	  //depth=parent's depth+1
	  //if no parent, depth=1
	  if(empty($requestparams["parent_id"]))$requestparams["depth"]=1;
	  else
	  {
		  $parent=Fetcher::fetchOne("VoucherSubaccount",array("id"=>$requestparams["parent_id"]));
		  $requestparams["depth"]=$parent->getDepth()+1;

		  //only do this if subaccount has a parent
		  //validate voucher account must match voucher account of parent subaccount
		  if($requestparams["voucher_account_id"]!=$parent->getVoucherAccountId())
		  {
			$message="Voucher Account must match parent's voucher account (".$parent->getVoucherAccount()->getName().")";
			return $this->redirect("home/error?msg=".$message);
		  }
	  }
  
	  $form->bind($requestparams, $request->getFiles($form->getName()));
	  if ($form->isValid())
	  {
		$notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';
  
		try {
		  $voucher_subaccount = $form->save();
		  $voucher_subaccount->calcFullname();
		} catch (Doctrine_Validator_Exception $e) {
  
		  $errorStack = $form->getObject()->getErrorStack();
  
		  $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
		  foreach ($errorStack as $field => $errors) {
			  $message .= "$field (" . implode(", ", $errors) . "), ";
		  }
		  $message = trim($message, ', ');
  
		  $this->getUser()->setFlash('error', $message);
		  return sfView::SUCCESS;
		}
  
		$this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $voucher_subaccount)));
  
		if ($request->hasParameter('_save_and_add'))
		{
		  $this->getUser()->setFlash('notice', $notice.' You can add another one below.');
  
		  $this->redirect('@voucher_subaccount_new');
		}
		else
		{
		  $this->getUser()->setFlash('notice', $notice);
  
		  $this->redirect(array('sf_route' => 'voucher_subaccount_edit', 'sf_subject' => $voucher_subaccount));
		}
	  }
	  else
	  {
		$this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
	  }
	}
	public function executeGetAncestorsAndChildren(sfWebRequest $request)
	{
		$vsa=Fetcher::fetchOne("VoucherSubaccount",array("id"=>$request->getParameter("id")));
		$children='[';
		$first=true;
		foreach($vsa->getChildren() as $child)
		{
			if(!$first)$children.=',';
			$first=false;
			$children.='{"id":'.$child->getId().',"name":"'.$child->getName().'"}';
		}
		$children.=']';

		$ancestors='[';
		$first=true;
		foreach($vsa->getancestors() as $ancestor)
		{
			if(!$first)$ancestors.=',';
			$first=false;
			$ancestors.='{"id":'.$ancestor->getId().',"name":"'.$ancestor->getName().'"}';
		}
		$ancestors.=']';

		echo '{"id":'.$vsa->getId().',"name":"'.$vsa->getName().'","ancestors":'.$ancestors.',"children":'.$children.'}';
		die();
	}
	public function executeTree(sfWebRequest $request)
	{
		$this->redirect("voucher_account/tree");
	}
	public function executeNewVoucher(sfWebRequest $request)
	{
		$requestparams=$request->getParameter("voucher");
  
		$day=$requestparams["date"]["day"];
		$month=$requestparams["date"]["month"];
		$year=$requestparams["date"]["year"];
	
		$voucher=new Voucher();
		if($request->hasParameter("date"))
		  $voucher->setDate($request->getParameter("date"));
		elseif(!$day or !$month or !$year)
		  $voucher->setDate(MyDate::today());
		else
		  $voucher->setDate($year."-".$month."-".$day);

		$vsa=Fetcher::fetchOne("VoucherSubaccount",array("id"=>$request->getParameter("id")));
		//if voucher subaccount has a redirect id, use that redirect id to get the actual subaccount
		if($vsa->getRedirectId()!=null)
			$vsa=Fetcher::fetchOne("VoucherSubaccount",array("id"=>$vsa->getRedirectId()));

		$voucher->setNo(VoucherTable::genCode());
		$voucher->setAccountId($vsa->getVoucherAccountId());
		$voucher->setSubaccountId($vsa->getId());

		// $voucher->setMonth($oldVoucher->getMonth());
		// $voucher->setYear($oldVoucher->getYear());
	
		if($vsa->getParticulars()!=null)$voucher->setParticulars($vsa->getParticulars());
		if($vsa->getNotes()!=null)$voucher->setNotes($vsa->getNotes());
		if($vsa->getAmount()!=null)$voucher->setAmount($vsa->getAmount());
		if($vsa->getPayee()!=null)$voucher->setPayee($vsa->getPayee());
		if($vsa->getVoucherTypeId()!=null)$voucher->setVoucherTypeId($vsa->getVoucherTypeId());
		else{$voucher_type=Fetcher::fetchOne("VoucherType");$voucher->setVoucherTypeId($voucher_type->getId());}
		if($vsa->getVoucherAllocationId()!=null)$voucher->setVoucherAllocationId($vsa->getVoucherAllocationId());
		else{$voucher_allocation=Fetcher::fetchOne("VoucherAllocation");$voucher->setVoucherAllocationId($voucher_allocation->getId());}
		$voucher->save();

		//create outcheck if necessary
		$voucher_type=$voucher->getVoucherType();
		$is_check=($voucher_type->getName()=="Cheque");
		$is_bank_transfer=($voucher_type->getName()=="Bank Transfer");
		if($is_check or $is_bank_transfer)
		{
			//if check does not exist
			//create it
			$check=new OutCheck();
			$check->setCode(OutCheckTable::genCode());
			$check->setVoucherAccountId($voucher->getAccountId());
			if($is_bank_transfer)
			  $check->setIsBankTransfer(1);
			else
			  $check->setIsBankTransfer(0);
			if($vsa->getPassbookId()!=null)$check->setPassbookId($vsa->getPassbookId());
			$check->setCheckNo($this->getUser()->getUsername()." ".date('h:i:s a'));
			
			$check->setCheckDate($voucher->getDate());

			$check->setAmount($voucher->getAmount());
			$check->setRemaining(0);
			// $check->setNotes($requestparams["notes"]);
			$check->save();

			$check->genVoucherOutPayment($voucher,$voucher->getAmount(),MyDate::today(),"");
		}

		$this->redirect("voucher/edit?id=".$voucher->getId());


	}
	public function executeRecalcFullname(sfWebRequest $request)
	{
		$subaccounts=Doctrine_Query::create()
			->from('VoucherSubaccount vs')
			->where('redirect_id is null')
			->orderBy('fullname')
			->execute();
		foreach($subaccounts as $s)$s->calcFullname();

		$this->redirect("voucher_account/tree");
	}
}
