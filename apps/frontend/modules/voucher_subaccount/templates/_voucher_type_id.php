

<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_customer_id">
<div>
<label for="voucher_subaccount_voucher_type_id">Voucher Type</label>
<div class="content">
<select name="voucher_subaccount[voucher_type_id]" id="voucher_subaccount_voucher_type_id" value=<?php echo $form->getObject()->getVoucherTypeId()?>>
  <?php 
  $voucher_types=Fetcher::fetch("VoucherType");
  foreach($voucher_types as $type){
  ?>
    <option value="<?php echo $type->getId()?>" <?php if($type->getId()==$form->getObject()->getVoucherTypeId())echo "selected=selected"?>    ><?php echo $type->getName()?></option>
  <?php } ?>
  </select>
</div>
</div>
</div>
