

<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_customer_id">
<div>
<label for="voucher_subaccount_redirect_id">Redirect to</label>
<div class="content">
<select name="voucher_subaccount[redirect_id]" id="voucher_subaccount_redirect_id" value=<?php echo $form->getObject()->getRedirectId()?>>
  <option></option>
  <?php 
    $subaccounts=Doctrine_Query::create()
			->from('VoucherSubaccount vs')
      ->where('redirect_id is null')
      ->orderBy('fullname')
			->execute();
    foreach($subaccounts as $sb){
  ?>
    <option value="<?php echo $sb->getId()?>" <?php if($sb->getId()==$form->getObject()->getRedirectId())echo "selected=selected"?>    ><?php echo $sb->getFullname()?></option>
  <?php } ?>
  </select>
</div>
</div>
</div>
