

<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_customer_id">
<div>
<label for="voucher_subaccount_passbook_id">Bank Account</label>
<div class="content">
<select name="voucher_subaccount[passbook_id]" id="voucher_subaccount_passbook_id" value=<?php echo $form->getObject()->getVoucherTypeId()?>>
  <option value=""></option>
  <?php 
  $passbooks=Fetcher::fetch("Passbook");
  foreach($passbooks as $type){
  ?>
    <option value="<?php echo $type->getId()?>" <?php if($type->getId()==$form->getObject()->getPassbookId())echo "selected=selected"?>    ><?php echo $type->getName()?></option>
  <?php } ?>
  </select>
</div>
</div>
</div>
