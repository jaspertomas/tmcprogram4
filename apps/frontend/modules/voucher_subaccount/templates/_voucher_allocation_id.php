

<div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_customer_id">
<div>
<label for="voucher_subaccount_voucher_allocation_id">Voucher Allocation</label>
<div class="content">
<select name="voucher_subaccount[voucher_allocation_id]" id="voucher_subaccount_voucher_allocation_id">
  <?php 
  $voucher_allocations=Fetcher::fetch("VoucherAllocation");
  foreach($voucher_allocations as $allocation){
  ?>
    <option value="<?php echo $allocation->getId()?>" <?php if($allocation->getId()==$form->getObject()->getVoucherAllocationId())echo "selected=selected"?>    ><?php echo $allocation->getName()?></option>
  <?php } ?>
  </select>
</div>
</div>
</div>
