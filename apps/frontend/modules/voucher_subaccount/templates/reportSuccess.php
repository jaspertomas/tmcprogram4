<?php
  function recursive($vsa, $startdate, $enddate)
  {
    $children=$vsa->getChildren();
    if(count($children)==0)return;
    foreach($children as $vsa){
      echo "<tr><td>";
          for($i=0;$i<$vsa->getDepth();$i++)echo "&nbsp;-&nbsp;";
          echo link_to($vsa->getName(),"voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate);
      echo "</td></tr>";
      recursive($vsa, $startdate, $enddate);
    }
  }
?>

<?php use_helper('I18N', 'Date') ?>
<?php $formpath="voucher_subaccount/report";?>
<?php $subaccountformpath="voucher_subaccount/report";?>
<?php $pdfformpath="voucher_subaccount/reportPdf";?>
<?php echo form_tag_for(new InvoiceForm(),$formpath)?>
<?php 
$startdate=MyDateTime::frommysql($startdateform->getObject()->getDate())->tomysql();
$enddate=MyDateTime::frommysql($enddateform->getObject()->getDate())->tomysql();
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
$startofthisyear=$today->getstartofyear();
$startofnextyear=$startofthisyear->addyears(1);
$startoflastyear=$startofthisyear->addyears(-1);
$endofthisyear=$startofthisyear->getendofyear();
$endofnextyear=$startofnextyear->getendofyear();
$endoflastyear=$startoflastyear->getendofyear();
$currentyear=$today->getYear();
$currentmonth=$today->getMonth();
?>

<input type="hidden" name="id" value="<?php echo $vsa->getId()?>">
<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $startdateform["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $enddateform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
<?php echo link_to("Last Year","$subaccountformpath?id=".$vsa->getId()."&startdate=".$startoflastyear->tomysql()."&enddate=".$endoflastyear->tomysql());?> | 
<?php echo link_to("This Year","$subaccountformpath?id=".$vsa->getId()."&startdate=".$startofthisyear->tomysql()."&enddate=".$endofthisyear->tomysql());?> | 
<?php echo link_to("Next Year","$subaccountformpath?id=".$vsa->getId()."&startdate=".$startofnextyear->tomysql()."&enddate=".$endofnextyear->tomysql());?> | 
<br>
<?php echo link_to("Last Month","$subaccountformpath?id=".$vsa->getId()."&startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","$subaccountformpath?id=".$vsa->getId()."&startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","$subaccountformpath?id=".$vsa->getId()."&startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br>
    <?php echo link_to("Yesterday","$subaccountformpath?id=".$vsa->getId()."&startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> | 
    <?php echo link_to("Tomorrow","$subaccountformpath?id=".$vsa->getId()."&startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> | 
<br>

</form>

<h1>Voucher Account Report by Voucher Date </h1>
<?php echo link_to("Go to Yearly Report","voucher_subaccount/reportByYear?id=".$vsa->getId()."&year=".$currentyear);?>
<br>
<?php echo link_to("Go to Monthly Report","voucher_subaccount/reportByMonth?id=".$vsa->getId()."&year=".$currentyear."&month=".$currentmonth);?>
<h2>
<?php echo link_to("Home","voucher_account/tree"."?startdate=".$startdate."&enddate=".$enddate);?> > 
<?php echo link_to($vsa->getVoucherAccount(),"voucher_account/report?id=".$vsa->getVoucherAccountId()."&startdate=".$startdate."&enddate=".$enddate);?>
<?php
foreach($vsa->getAncestors() as $ancestor)
  echo " > ".link_to($ancestor->getName(),"voucher_subaccount/report?id=".$ancestor->getId()."&startdate=".$startdate."&enddate=".$enddate);

echo " > ".link_to($vsa->getName(),"voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate);
?>

</h2>

Date: <?php echo MyDateTime::frommysql($startdate)->toshortdate(); ?> to <?php echo MyDateTime::frommysql($enddate)->toshortdate(); ?>

<br>Cash Sales: <?php echo MyDecimal::format($cashtotal)?>
<br>
<?php echo link_to("Edit","voucher_subaccount/edit?id=".$vsa->getId());?>
<br>
<?php echo link_to("Reallocate","voucher_subaccount/massReallocate?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate);?>
<br>
<?php 
$datearray=explode("-",$startdate);
$todatearray=explode("-",$enddate);
/*echo link_to("Print","$pdfformpath?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);*/?>
<h3>Sub Accounts</h3>
<table>
  <?php recursive($vsa, $startdate, $enddate);?>
<table>

<br>
<br>
<table border=1>
  <tr>
    <td>Voucher No.</td>
    <td>Date</td>
    <td>Payee</td>
    <td>Amount</td>
    <td>From Bank</td>
    <td>Particulars</td>
    <td>Notes</td>
    <td>Status</td>
    <td>Type</td>
    <td>Allocation</td>
    <td>Account</td>
    <td>Subaccount</td>
    <td>For Period</td>
  </tr>
    <?php foreach($vouchers as $voucher){?>
    <tr>
      <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId()) ?></td>
      <td><?php echo $voucher->getDate() ?></td>
      <td><?php echo $voucher->getPayee() ?></td>
      <td><?php echo MyDecimal::format($voucher->getAmount()) ?></td>
      <td><?php if($voucher->getOutCheckId())echo $voucher->getOutCheck()->getPassbook()->getName() ?></td>
      <td><?php echo $voucher->getParticulars() ?></td>
      <td><?php echo $voucher->getNotes() ?></td>
      <td><div <?php if($voucher->getStatus()=="Cancelled")echo 'style="color:red"'?>><?php echo $voucher->getStatus() ?></div></td>
      <td>
        <?php 
          $voucher_type=$voucher->getVoucherType()->getName();
          echo $voucher_type;
          if($voucher_type=="Cheque")
          {
            $check=$voucher->getOutCheck();
            echo " <br> ".MyDateTime::frommysql($check->getCheckDate())->toshortdate();
            echo " <br> ".$check->getCheckNo();
          }
        
        ?>
    </td>
      <td><?php echo $voucher->getVoucherAllocation() ?></td>
      <td><?php echo $voucher->getVoucherAccount() ?></td>
      <td><?php echo $voucher->getVoucherSubaccount() ?></td>
      <td><?php echo MyDate::shortNameForMonth($voucher->getMonth())." ".$voucher->getYear() ?></td>
      <td><?php echo link_to("Edit","voucher/edit?id=".$voucher->getId()) ?></td>
    </tr>
    <?php }?>
</table>

