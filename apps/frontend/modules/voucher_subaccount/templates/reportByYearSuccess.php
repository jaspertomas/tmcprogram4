<?php
  function recursive($vsa, $startdate, $enddate)
  {
    $children=$vsa->getChildren();
    if(count($children)==0)return;
    foreach($children as $vsa){
      echo "<tr><td>";
          for($i=0;$i<$vsa->getDepth();$i++)echo "&nbsp;-&nbsp;";
          echo link_to($vsa->getName(),"voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate);
      echo "</td></tr>";
      recursive($vsa, $startdate, $enddate);
    }
  }
?>

<?php use_helper('I18N', 'Date') ?>
<?php $formpath="voucher_account/report";?>
<?php $subaccountformpath="voucher_subaccount/reportByYear";?>
<?php $pdfformpath="voucher_subaccount/reportPdf";?>
<?php echo form_tag($subaccountformpath); ?>
<?php 
$today=MyDateTime::frommysql($year."-01-01"); 
$year=$today->getYear();
$month=$today->getMonth();
$startofthisyear=$today->getstartofyear();
$endofthisyear=$startofthisyear->getendofyear();
$nextyear=$startofthisyear->addyears(1);
$lastyear=$startofthisyear->addyears(-1);
$startofthismonth=$today->getstartofmonth();
$endofthismonth=$startofthismonth->getendofmonth();
?>

<input type="hidden" name="id" value="<?php echo $vsa->getId()?>">
<?php echo link_to("Last Year","$subaccountformpath?id=".$vsa->getId()."&year=".$lastyear->getYear());?> | 
<?php echo link_to("Next Year","$subaccountformpath?id=".$vsa->getId()."&year=".$nextyear->getYear());?> | 
<br>

</form>

<h1>Yearly Voucher Account Report: <?php echo $year ?></h1>
<?php echo link_to("Go to Monthly Report","voucher_subaccount/reportByMonth?id=".$vsa->getId()."&year=".$year."&month=".$month);?>
<br>
<?php echo link_to("Go to Report By Date","voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startofthismonth->tomysql()."&enddate=".$endofthisyear->tomysql());?>

<h2>
<?php echo link_to("Home","voucher_account/tree"."?startdate=".$startdate."&enddate=".$enddate);?> > 
<?php echo link_to($vsa->getVoucherAccount(),"voucher_account/report?id=".$vsa->getVoucherAccountId()."&startdate=".$startdate."&enddate=".$enddate);?>
<?php
foreach($vsa->getAncestors() as $ancestor)
  echo " > ".link_to($ancestor->getName(),"voucher_subaccount/report?id=".$ancestor->getId()."&startdate=".$startdate."&enddate=".$enddate);

echo " > ".link_to($vsa->getName(),"voucher_subaccount/report?id=".$vsa->getId()."&startdate=".$startdate."&enddate=".$enddate);
?>

</h2>

<?php echo link_to("Edit","voucher_subaccount/edit?id=".$vsa->getId());?>
<br>
<?php 
$datearray=explode("-",$startdate);
$todatearray=explode("-",$enddate);
/*echo link_to("Print","$pdfformpath?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);*/?>
<h3>Sub Accounts</h3>
<table>
  <?php recursive($vsa, $startdate, $enddate);?>
<table>

<br>
<br>
<table border=1>
  <tr>
    <td>Voucher No.</td>
    <td>Date</td>
    <td>Payee</td>
    <td>Amount</td>
    <td>From Bank</td>
    <td>Particulars</td>
    <td>Notes</td>
    <td>Status</td>
    <td>Type</td>
    <td>Allocation</td>
    <td>For Period</td>
  </tr>
    <?php foreach($vouchers as $voucher){?>
    <tr>
      <td><?php echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId()) ?></td>
      <td><?php echo $voucher->getDate() ?></td>
      <td><?php echo $voucher->getPayee() ?></td>
      <td><?php echo MyDecimal::format($voucher->getAmount()) ?></td>
      <td><?php if($voucher->getOutCheckId())echo $voucher->getOutCheck()->getPassbook()->getName() ?></td>
      <td><?php echo $voucher->getParticulars() ?></td>
      <td><?php echo $voucher->getNotes() ?></td>
      <td><div <?php if($voucher->getStatus()=="Cancelled")echo 'style="color:red"'?>><?php echo $voucher->getStatus() ?></div></td>
      <td>
        <?php 
          $voucher_type=$voucher->getVoucherType()->getName();
          echo $voucher_type;
          if($voucher_type=="Cheque")
          {
            $check=$voucher->getOutCheck();
            echo " <br> ".MyDateTime::frommysql($check->getCheckDate())->toshortdate();
            echo " <br> ".$check->getCheckNo();
          }
        
        ?>
    </td>
      <td><?php echo $voucher->getVoucherAllocation() ?></td>
      <td><?php echo MyDate::shortNameForMonth($voucher->getMonth())." ".$voucher->getYear() ?></td>
      <td><?php echo link_to("Edit","voucher/edit?id=".$voucher->getId()) ?></td>
    </tr>
    <?php }?>
</table>

