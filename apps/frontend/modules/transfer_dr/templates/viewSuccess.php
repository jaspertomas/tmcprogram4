<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h2><?php echo link_to("> Back to ".$transfer,"transfer/view?id=".$transfer->getId())?></h2>
<h1>
  <?php echo $transfer_dr->getCode(); ?>
  <?php if($transfer_dr->isCancelled()){?>
    (<font color=red>Cancelled</font>)
  <?php }else{?>
    (<?php echo $transfer_dr->transferStatusString(); ?>)
    <?php 
      if(!$transfer_dr->isTransferred())
      {
        $olddatetransferred=MyDateTime::fromdatetime($transfer_dr->getDatetime());
        $today=MyDateTime::now();
        echo "<br>".link_to("Transfer Today (".$today->toshortdate().")","transfer_dr/transfer?today=true&id=".$transfer_dr->getId());
        if($olddatetransferred->tomysql()!=MyDate::today())echo "<br>".link_to("Transfer ".
        $olddatetransferred->toshortdate(),"transfer_dr/transfer?id=".$transfer_dr->getId());
      }
      elseif($sf_user->hasCredential(array('admin'), false))
        echo link_to("Undo Transfer","transfer_dr/undoTransfer?id=".$transfer_dr->getId());
    ?>
  <?php }?>
</h1>
<?php slot('transaction_id', $transfer_dr->getId()) ?>
<?php slot('transaction_type', "TransferDr") ?>

<table>
  <tr valign=top>
    <td>
      <table>
          <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
          <?php echo form_tag_for($form,'transfer_dr/adjust')?> <?php echo $form['id'] ?>
          <?php }?>	

        <tr valign=top>
          <td>Transfer Direction:</td>
          <td><?php 
            echo $transfer->getWarehouseVector()->getName(); 
          ?></td>
        </tr>
        <tr>
          <td colspan=2>
          </td>
        </tr>
        <tr valign=top>
          <td>Date</td>
          <td><?php 
              if($transfer_dr->isPending()){
                if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false))
                {
                  echo $form['datetime'];
                } 
                else
                  echo $form->getObject()->getDatetime(); 
              }
          ?></td>
        </tr>
        <tr>
          <td>Description</td>
          <td><?php echo $transfer_dr->getDescription() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <textarea rows="2" cols="10" name="transfer_dr[description]" id="transfer_dr_description"><?php echo $transfer_dr->getDescription()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td>Notes</td>
          <td><?php echo $transfer_dr->getNotes() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <textarea rows="2" cols="10" name="transfer_dr[notes]" id="transfer_dr_notes"><?php echo $transfer_dr->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <input type="submit" value="Save"></form>
            <?php }?>	
          </td>
        </tr>

		</table>
  </tr>
</table>

            <!--allow cancel only if not transferred and not cancelled-->
            <?php if($transfer_dr->isPending())echo link_to('Cancel','transfer_dr/cancel?id='.$transfer_dr->getId(), array('confirm' => 'Cancel: Are you sure?')) ?> |
            <!--only admin can delete and undo cancel-->
            <?php if($sf_user->hasCredential(array('admin'), false) and !$transfer_dr->isTransferred()){?>
              <!--allow undo cancel only if cancelled-->
              <?php if($transfer_dr->isCancelled())echo link_to('Undo Cancel','transfer_dr/undoCancel?id='.$transfer_dr->getId(), array('confirm' => 'Undo Cancel: Are you sure?')) ?> |
              <?php echo link_to('Delete','transfer_dr/delete?id='.$transfer_dr->getId(), array('method' => 'delete', 'confirm' => 'Delete: Are you sure?')) ?> |
            <?php } ?>
            <?php if(!$transfer_dr->isPending() && !$transfer_dr->isCancelled())echo link_to("Print","transfer_dr/viewPdf?id=".$transfer_dr->getId());?> |
            <!--allow set date to purchase date only if not transferred and not cancelled-->
            <?php if($transfer_dr->isPending())echo "<br>".link_to("Set Stock Transfer date as DR date","transfer_dr/setPurchaseDateAsTransferDate?id=".$transfer_dr->getId());?> |
            <br><?php echo link_to("Recalculate","transfer_dr/recalc?id=".$transfer_dr->getId())?>
<hr>

<?php echo form_tag("transfer_dr/setQtys");?>
<input type=hidden id=id name=id value=<?php echo $transfer_dr->getId()?>>
<table border=1>
  <tr>
    <td>DR</td>
    <td><?php echo "Transferred"?></td>
    <td width=50%>Description</td>
    <td>Transfer</td>
    <td>Not yet</td>
  </tr>
  <tr>
    <td>Qty</td>
    <td></td>
    <td></td>
    <td>Date</td>
    <td>Transferred</td>
  </tr>
  <?php 
  	foreach($transfer->getDetails() as $transfer_detail){
      $dr_detail=$dr_details[$transfer_detail->getId()];
      if($dr_detail==null)echo "<font color=red>SOMETHING IS WRONG. PLEASE PRESS RECALCULATE";
      else $deliveryqty=$dr_detail->getQty();
  ?>
  <tr>
    <td>
      <?php if($transfer_dr->isPending()){?>
      <input id=qtys[<?php echo $transfer_detail->getId()?>] size=3 name=qtys[<?php echo $transfer_detail->getId()?>] value=<?php echo $deliveryqty?>>
      <br>
      <?php } ?>
      <?php echo $deliveryqty;?>
    </td>
    <td align=right ><?php if($dr_detail!=null)if($dr_detail->isTransferred())echo $deliveryqty;else echo 0; ?></td>
    <td><?php echo link_to($dr_detail->getProduct(),"stock/view?id=".$dr_detail->getToStock()->getId()) ?></td>
    <td align=right ><?php if($dr_detail!=null)if($dr_detail->isTransferred())echo MyDateTime::fromdatetime($dr_detail->getDatetime()) ?></td>
    <td align=center>
      <?php 
        $remaining=$transfer_detail->getQty()-$transfer_detail->getQtyTransferred();
        if($remaining>0)echo "<font color=red>".$remaining."</font>";
      ?>
    </td>
  <?php }?>
  
</table>
<?php if($transfer_dr->isPending()){?>
  <input type=submit value="Save">
<?php } ?>
</form>

<hr>
<br>
<br>
<br>
<br>


