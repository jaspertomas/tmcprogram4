<?php

require_once dirname(__FILE__).'/../lib/transfer_drGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/transfer_drGeneratorHelper.class.php';

/**
 * transfer_dr actions.
 *
 * @package    sf_sandbox
 * @subpackage transfer_dr
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class transfer_drActions extends autoTransfer_drActions
{
	public function executeNew(sfWebRequest $request)
	{
    $transfer=Fetcher::fetchOne("Transfer",array("id"=>$request->getParameter("transfer_id")));

    $transfer_dr = TransferDrTable::genDrForTransferId($transfer->getId(),$this->getUser()->getGuardUser());
    $this->redirect("transfer_dr/view?id=".$transfer_dr->getId());
	}
	public function executeView(sfWebRequest $request)
	{
	  //fetch return by id
	  $this->transfer_dr=Fetcher::fetchOne("TransferDr",array("id"=>$request->getParameter('id')));
  
	  $this->form = $this->configuration->getForm($this->transfer_dr);
  
    $this->transfer=$this->transfer_dr->getTransfer();

    //arrange returndetails into an array with transferredetail as key
	  $this->dr_details=$this->transfer_dr->getTransferDrDetailsIndexedByTransferDetailId();
	}
  /*
  public function executeFilter(sfWebRequest $request)
  {
    $this->setPage(1);

    $this->filters = $this->configuration->getFilterForm($this->getFilters());

    $requestparams=$request->getParameter($this->filters->getName());
    if($request->hasParameter("customer_id"))
      $requestparams["customer_id"]=$request->getParameter("customer_id");

    $this->filters->bind($requestparams);
    if ($this->filters->isValid())
    {
      $this->setFilters($this->filters->getValues());

      $this->redirect('@transfer_dr');
    }

    $this->pager = $this->getPager();
    $this->sort = $this->getSort();

    $this->setTemplate('index');
  }
*/
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());

    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $transfer_dr = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $transfer_dr)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect('transfer_dr/view?id='.$transfer_dr->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeSetQtys(sfWebRequest $request)
  {    //fetch return by id
    $dr=Fetcher::fetchOne("TransferDr",array("id"=>$request->getParameter('id')));
  
    if($dr->isTransferred())
    {
      $error="Cannot set qty, DR is already transferred";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('transfer_dr/view?id='.$dr->getId());
    }
    if($dr->isCancelled())
    {
      $error="Cannot set qty, DR is cancelled";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('transfer_dr/view?id='.$dr->getId());
    }
  
    foreach($request->getParameter('qtys') as $transferredetail_id => $qty)
    if($qty<0)
    {
      $error="Quantity cannot be negative";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('transfer_dr/view?id='.$dr->getId());
    }

    $dr_details=$dr->getTransferDrDetailsIndexedByTransferDetailId();
  
    // var_dump($request->getParameter('qtys'));die();
    foreach($request->getParameter('qtys') as $transfer_detail_id => $qty)
    {
      $transfer_detail=Fetcher::fetchOne("TransferDetail",array("id"=>$transfer_detail_id));
      $qty=intval($qty);
  
      //validate: qty must not be greater than remaining
      $qty_remaining=$transfer_detail->getQty()-$transfer_detail->getQtyTransferred();
  
      if($qty>$qty_remaining)
      {
        $error="Cannot transfer ".$qty." units, only ".$qty_remaining." units remaining";
        $this->getUser()->setFlash('error', $error);
        return $this->redirect('transfer_dr/view?id='.$dr->getId());
      }
  
      //if deliverydetail exists, just save the qty
      if(array_key_exists($transfer_detail_id, $dr_details))
      {
        $dr_detail=$dr_details[$transfer_detail_id];
        $dr_detail->setQty($qty);
        $dr_detail->save();
      }
    }
  
    $dr->calc();
  
    return $this->redirect("transfer_dr/view?id=".$dr->getId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $transfer_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $transfer_dr)));

    if ($transfer_dr->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("transfer/view?id=".$transfer_dr->getTransferId());
  }
  public function executeCancel(sfWebRequest $request)
  {
    $transfer_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $transfer_dr)));

    if ($transfer_dr->cascadeCancel())
    {
      $this->getUser()->setFlash('notice', 'The item was cancelled successfully.');
    }

    $this->redirect("transfer_dr/view?id=".$transfer_dr->getId());
  }
  public function executeUndoCancel(sfWebRequest $request)
  {
    $transfer_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $transfer_dr)));

    if ($transfer_dr->cascadeUndoCancel())
    {
      $this->getUser()->setFlash('notice', 'The item was uncancelled successfully.');
    }

    $this->redirect("transfer_dr/view?id=".$transfer_dr->getId());
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('transfer_dr');
    $id=$requestparams['id'];

    $this->forward404Unless(
    $this->transfer_dr=Doctrine_Query::create()
    ->from('TransferDr i')
    ->where('i.id = '.$id)
    ->fetchOne()
    , sprintf('TransferDr with id (%s) not found.', $request->getParameter('id')));

    $this->transfer_dr->setDescription($requestparams["description"]);
    $this->transfer_dr->setNotes($requestparams["notes"]);

  	$this->transfer_dr->save();

	  //set date and adjust stock entry date if necessary
    if(isset($requestparams["datetime"]))
    {
      $newdatetime=
        $requestparams["datetime"]["year"]."-".
        $requestparams["datetime"]["month"]."-".
        $requestparams["datetime"]["day"]."-".
        $requestparams["datetime"]["hour"]."-".
        $requestparams["datetime"]["minute"];
      $this->transfer_dr->setDatetime($newdatetime);
	  $this->transfer_dr->save();

	  foreach($this->transfer_dr->getTransferDrDetail() as $detail)
      {
        $detail->setDatetime($newdatetime);
        $detail->save();

        //update stockentry
        $stockentry=$detail->getFromStockentry();
        $stockentry->setDatetime($newdatetime);
        $stockentry->save();

        $stockentry=$detail->getToStockentry();
        $stockentry->setDatetime($newdatetime);
        $stockentry->save();
      }
      
      //no need for this, change date allowed only
      //when dr is not transferred
      //stock entries will be generated when it is transferred
      // $this->transfer_dr->setDateAndUpdateStockEntry($newdatetime);
    }

    $this->redirect($request->getReferer());
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeTransfer(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('TransferDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    if($request->getParameter('today')=="true")
    {
      $dr->setDatetime(MyDateTime::now()->todatetime());
      $dr->save();
    }
    $error=$dr->transfer();
    if($error!=false)
    {
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('transfer_dr/view?id='.$dr->getId());
    }

    //todo:create history

    $this->redirect($request->getReferer());
  }
  public function executeUndoTransfer(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('TransferDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    $dr->undoTransfer();

    //todo:create history

    $this->redirect($request->getReferer());
  }
  public function executeRecalc(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('TransferDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
    $dr->calc();
    $this->redirect($request->getReferer());
  }
}
