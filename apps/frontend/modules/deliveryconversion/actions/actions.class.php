<?php

require_once dirname(__FILE__).'/../lib/deliveryconversionGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/deliveryconversionGeneratorHelper.class.php';

/**
 * deliveryconversion actions.
 *
 * @package    sf_sandbox
 * @subpackage deliveryconversion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class deliveryconversionActions extends autoDeliveryconversionActions
{
  public function executeEdit(sfWebRequest $request)
  {
    $this->redirect("home/error?msg=conversion details cannot be edited, only deleted");
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $deliveryconversion = $form->save();
        $deliveryconversion->genDeliverydetails();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $deliveryconversion)));

      $this->getUser()->setFlash('notice', $notice);
      $this->redirect('delivery/view?id='.$deliveryconversion->getParentId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $deliveryconversion=$this->getRoute()->getObject();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $deliveryconversion)));

    if ($deliveryconversion->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect(strtolower($deliveryconversion->getParentClass()).'/view?id='.$deliveryconversion->getParentId());
  }
    public function executeSetQty(sfWebRequest $request)
    {
/*
        if($request->getParameter('value')==0)
        {
          $message="Invalid qty";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
*/
    
        $dc=Doctrine_Query::create()
        ->from('Deliveryconversion id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $dc->setQty($request->getParameter('value'));
        $dc->save();
        $dc->cascadeUpdateDeliverydetailQtys();

        $this->redirect($request->getReferer());
    }
    public function executeSetDescription(sfWebRequest $request)
    {
        $this->detail=Doctrine_Query::create()
        ->from('Deliveryconversion id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setDescription($request->getParameter('value'));
        $this->detail->save();
        
        $this->redirect($request->getReferer());
    }
    public function executeReset(sfWebRequest $request)
    {
        $dc=Doctrine_Query::create()
        ->from('Deliveryconversion id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        //delete all deliverydetails
        foreach($dc->getDeliverydetails() as $detail)
          $detail->cascadeDelete();

        //recreate deliverydetails
        $dc->genDeliverydetails();
        
        $this->redirect($request->getReferer());
    }
}
