<?php

require_once dirname(__FILE__).'/../lib/producttypeGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/producttypeGeneratorHelper.class.php';

/**
 * producttype actions.
 *
 * @package    sf_sandbox
 * @subpackage producttype
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class producttypeActions extends autoProducttypeActions
{
  public function executeArrangeIntoChildren(sfWebRequest $request)
  {
		$producttype=ProducttypeTable::fetch($request->getParameter("id"));
		$keywords=$request->getParameter("keywords");
		$keywords=explode(",",$keywords);
		
    foreach($keywords as $keyword)
    {
      $keyword=trim($keyword);
      if($keyword!="")
      $producttype->arrangeProductsIntoChildProductType($keyword);
    }

		$this->redirect($request->getReferer());
  }
  public function executeNameFormatHelp(sfWebRequest $request)
  {
  }
  public function executePriceFormulaHelp(sfWebRequest $request)
  {
  }
  public function executeAutocalcmaxsellprice(sfWebRequest $request)
  {
		$producttype=ProducttypeTable::fetch($request->getParameter("id"));
	  if($producttype->getMaxSellFormula())
  		$producttype->cascadeCalcProductMaxSellPrice();
		$this->redirect($request->getReferer());
  }
  public function executeAutocalcminsellprice(sfWebRequest $request)
  {
		$producttype=ProducttypeTable::fetch($request->getParameter("id"));
	  if($producttype->getMinSellFormula())
    	$producttype->cascadeCalcProductMinSellPrice();
		$this->redirect($request->getReferer());
  }
  public function executeAutocalcmaxbuyprice(sfWebRequest $request)
  {
		$producttype=ProducttypeTable::fetch($request->getParameter("id"));
	  if($producttype->getMaxBuyFormula())
    	$producttype->cascadeCalcProductMaxBuyPrice();
		$this->redirect($request->getReferer());
  }
  public function executeAutocalcminbuyprice(sfWebRequest $request)
  {
		$producttype=ProducttypeTable::fetch($request->getParameter("id"));
	  if($producttype->getMinBuyFormula())
    	$producttype->cascadeCalcProductMinBuyPrice();
		$this->redirect($request->getReferer());
  }
  public function executeAutocalcbaseprice(sfWebRequest $request)
  {
		$producttype=ProducttypeTable::fetch($request->getParameter("id"));
	  if($producttype->getBaseFormula())
    	$producttype->cascadeCalcProductBasePrice();
		$this->redirect($request->getReferer());
  }

  //all--------------------------
  public function executeAutocalcallprices(sfWebRequest $request)
  {
		$producttype=ProducttypeTable::fetch($request->getParameter("id"));
	  if($producttype->getMaxBuyFormula())
    	$producttype->cascadeCalcProductMaxBuyPrice();
	  if($producttype->getMinBuyFormula())
    	$producttype->cascadeCalcProductMinBuyPrice();
	  if($producttype->getMaxSellFormula())
  		$producttype->cascadeCalcProductMaxSellPrice();
	  if($producttype->getMinSellFormula())
    	$producttype->cascadeCalcProductMinSellPrice();
		$this->redirect($request->getReferer());
  }

  public function executeAutorename(sfWebRequest $request)
  {
		$producttype=ProducttypeTable::fetch($request->getParameter("id"));
		
    $producttype->cascadeAutorename();
		$this->redirect($request->getReferer());
  }
  public function executeViewAllPdf(sfWebRequest $request)
  {
    $this->executeViewAll($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeViewAll(sfWebRequest $request)
  {
    $this->producttypes=Doctrine_Query::create()
        ->from('Producttype p')
      	->orderBy('p.name')
      	->execute();
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeDir($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeView(sfWebRequest $request)
  {
    $this->redirect("producttype/dir?id=".$request->getParameter("id"));
  }
  /*
  public function executeTree(sfWebRequest $request)
  {
    $this->producttype=Fetcher::fetchOne("Producttype",array("id"=>$request->getParameter("id")));
    $this->levels=$request->getParameter("levels");
    if($this->levels==null)$this->levels=1;
  }
  */
  public function executeDir(sfWebRequest $request)
  {
    $this->levels=$request->getParameter("levels");
    if($this->levels=="")$this->levels=1;

    //date form
    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    //warehouse id
    $this->warehouse_id=$request->getParameter("warehouse_id");
    if(!$this->warehouse_id)
      $this->warehouse_id=SettingsTable::fetch('default_warehouse_id');
      
    $this->form=new InvoiceForm($invoice);
    //end date form

    $this->producttype = Fetcher::fetchById("Producttype",$request->getParameter("id")); 
    //hide products for misc
    if($request->getParameter("id")==1)
      $this->stockentries =  array();
    else
      $this->stockentries =  StockEntryTable::fetchByProductTypeIdAndWareHouseIdGroupByLatestAsOfDate($this->producttype->getId(),$this->warehouse_id,$invoice->getDate());
    


    //create array of product ids and qtys
    $array=array();
    foreach($this->stockentries as $entry)
    {
      if($entry==null || $entry==false)continue;
      $stock_id=$entry->getStockId();
      
      //initialize slot if necessary
      if(!isset($array[$stock_id]))
      {
        $product=$entry->getStock()->getProduct();
        $array[$stock_id]["product_name"]=$product->getName();
        $array[$stock_id]["product_id"]=$product->getId();
        $array[$stock_id]["endbalance"]=$entry->getBalance();
        $array[$stock_id]["stock_id"]=$stock_id;
        $array[$stock_id]["qty_reported"]=null;
        $array[$stock_id]["notes"]="";
        $array[$stock_id]["comment"]="";
        $array[$stock_id]["datetime"]=MyDateTime::fromdatetime($entry->getDatetime())->toshortdate();
      }

      //if this is a report stock entry,
      //special behavior
      if($entry->isReport())
      {
        $array[$stock_id]["qty_reported"]=$entry->getQtyReported();
        $array[$stock_id]["notes"]=$entry->getDescription();
        $array[$stock_id]["comment"]=$entry->getReportComment();
        continue;
      }

    }
    
    //sort by product name, then by product type name
    usort($array, function($a, $b) {
      return strnatcmp($a['product_name'], $b['product_name']);
    });

    $this->array=$array;
      	
    //expose ids of branches to template
    $children_ids=array();
    $children=$this->producttype->getChildren();
    foreach($children as $child)
    {
      $children_ids[]=$child->getId();
      foreach($child->getChildren() as $child)
      {
        $children_ids[]=$child->getId();
        foreach($child->getChildren() as $child)
        {
          $children_ids[]=$child->getId();
          foreach($child->getChildren() as $child)
          {
            $children_ids[]=$child->getId();
            foreach($child->getChildren() as $child)
            {
              $children_ids[]=$child->getId();
              
            }
          }
        }
      }
    }
    $this->children_ids=implode(",",$children_ids);
      	
  }
  public function executePriceWizard(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin','inventory'),false))
    return $this->redirect("home/error?msg=Insufficient Permissions");

    $this->producttype = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->producttype);
    $this->levels=$request->getParameter("levels");
    if($this->levels=="")$this->levels=1;
    $query=Doctrine_Query::create()
        ->from('Product p')
      	->where('p.producttype_id='.$this->producttype->getId())
	->orderBy('p.name')
	;
      	if($this->producttype->getId()==1 and $request->getParameter("showall")!=1)$query->limit('50');
      	$this->products=$query->execute();
  }
  public function executePricelist(sfWebRequest $request)
  {
    $this->producttype = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->producttype);
    $this->levels=$request->getParameter("levels");
    if($this->levels=="")$this->levels=1;
    $query=Doctrine_Query::create()
        ->from('Product p')
      	->where('p.producttype_id='.$this->producttype->getId())
	->orderBy('p.name')
	;
      	if($this->producttype->getId()==1 and $request->getParameter("showall")!=1)$query->limit(50);
      	$this->products=$query->execute();
  }
  public function executePricelistPdf(sfWebRequest $request)
  {
    $this->executePricelist($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeViewSpecs(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin','inventory'),false))
    return $this->redirect("home/error?msg=Insufficient Permissions");

    $this->executeDir($request);
    $this->setTemplate("viewSpecs");
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->producttype = new Producttype();
    $this->producttype->setParentId($request->getParameter("parent_id"));
    $this->producttype->setIsHidden(0);
    $this->form = $this->configuration->getForm($this->producttype);
  }
  public function executeEdit(sfWebRequest $request)
  {
    $this->producttype = $this->getRoute()->getObject();
    //prevent edit of home page
    if($this->producttype->isHome())$this->redirect("producttype/view?id=1");
    $this->form = $this->configuration->getForm($this->producttype);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $producttype = $form->save();
        $producttype->trimFormulas();
        $producttype->calcPath();
      
        //custom calculation
        $producttype->getParent()->calc();
        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $producttype)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');
        $this->redirect('@producttype_new?parent_id='.$producttype->getParentId());
     }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect("producttype/view?id=".$producttype->getId());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $parent=$this->getRoute()->getObject()->getParent();
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($this->getRoute()->getObject()->isRoot())
    {
      $this->getUser()->setFlash('error', 'Cannot delete root node');
    }
    elseif ($this->getRoute()->getObject()->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('producttype/view?id='.$parent->getId());
  }
/*
  //this version of delete includes cascade delete of products
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $producttype=$this->getRoute()->getObject();
    $parent=$producttype->getParent();
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $producttype)));

    foreach($producttype->getProduct() as $product)
    {
      $product->cascadeDelete();
    }

    if ($producttype->isRoot())
    {
      $this->getUser()->setFlash('error', 'Cannot delete root node');
    }
    elseif ($producttype->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('producttype/view?id='.$parent->getId());
  }
*/
  public function executeMassoper(sfWebRequest $request)
  {
    $parent_id=$request->getParameter("parent_id");
    $parent=ProducttypeTable::fetch($parent_id);
	  $children=$parent->getChildren();
	  $priority=$children[count($children)-1]->getPriority()+2;
    
  	$ids=$request->getParameter("ids");
  	
  	if($request->getParameter("submit")=="Set Status OK")
  	{
    	foreach($ids as $id)
    	{
    	  $producttype=ProducttypeTable::fetch($id);
    	  $producttype->setStatus("OK");
    	  $producttype->save();
    	}
  	}
  	elseif($request->getParameter("submit")=="Copy Schema")
  	{
    	foreach($ids as $id)
    	{
    	  $producttype=ProducttypeTable::fetch($id);
        $producttype->setProducttypeSchemaId($parent->getProducttypeSchemaId());
    	  $producttype->setSpeccount($parent->getSpeccount());
        $producttype->setSpec1($parent->getSpec1());
        $producttype->setSpec2($parent->getSpec2());
        $producttype->setSpec3($parent->getSpec3());
        $producttype->setSpec4($parent->getSpec4());
        $producttype->setSpec5($parent->getSpec5());
        $producttype->setSpec6($parent->getSpec6());
        $producttype->setSpec7($parent->getSpec7());
        $producttype->setSpec8($parent->getSpec8());
        $producttype->setSpec9($parent->getSpec9());
        $producttype->setProductNameFormat($parent->getProductNameFormat());
        $producttype->setProductDescriptionFormat($parent->getProductDescriptionFormat());
        $producttype->setBarcodeFormat($parent->getBarcodeFormat());
        $producttype->setBarcodeFormat_2($parent->getBarcodeFormat_2());
        $producttype->setBarcodeFormat_3($parent->getBarcodeFormat_3());
        $producttype->setBaseFormula($parent->getBaseFormula());
        $producttype->setMaxSellFormula($parent->getMaxSellFormula());
        $producttype->setMaxBuyFormula($parent->getMaxBuyFormula());
        $producttype->setMinSellFormula($parent->getMinSellFormula());
        $producttype->setMinBuyFormula($parent->getMinBuyFormula());
    	  $producttype->save();
    	}
  	}
  	elseif($request->getParameter("submit")=="Copy")
  	{
    	foreach($ids as $id)
    	{
    	  $producttype=ProducttypeTable::fetch($id);
    	  $newproducttype=$producttype->copyProducttype();

    	  $newproducttype->setParentId($parent_id);
    	  $newproducttype->setPriority($priority);$priority+=2;
    	  $newproducttype->save();
    	}
  	}
  	else //move
  	{
    	foreach($ids as $id)
    	{
    	  $producttype=ProducttypeTable::fetch($id);
    	  $producttype->setParentId($parent_id);
    	  $producttype->setPriority($priority);$priority+=2;
    	  $producttype->save();
    	}
  	}
  	$parent->calc();
    $this->redirect($request->getReferer());
  }
  public function executeMerge(sfWebRequest $request)
  {
		$this->producttype=ProducttypeTable::fetch($request->getParameter("producttype_id"));
		$this->producttype_id=$request->getParameter("producttype_id");
		$this->duplicate_ids=$request->getParameter("duplicate_ids");
	  $this->duplicateproducts=$this->producttype->getProductsByIds(explode(",",$request->getParameter("duplicate_ids")));
		$this->product=ProductTable::fetch($request->getParameter("product_id"));

		if($request->getParameter("submit")=="Execute" or $request->getParameter("submit")=="Execute and Delete")
		{
			foreach($this->duplicateproducts as $duplicate)
			{
				//if($product->getId()==1)die();
				foreach($duplicate->getInvoicedetail() as $detail)
				{
				  $detail->setProductId($this->product->getId());
				  $detail->save();
				}
				foreach($duplicate->getPurchasedetail() as $detail)
				{
				  $detail->setProductId($this->product->getId());
				  $detail->save();
				}
				if($request->getParameter("submit")=="Execute and Delete")
					$duplicate->cascadeDelete();
			}

			
			$this->redirect("producttype/view?id=".$this->producttype_id);
		}
  
  }
  public function executeProductmassoper(sfWebRequest $request)
  {
    $producttype_id=$request->getParameter("producttype_id");
    $producttype=ProducttypeTable::fetch($producttype_id);

  	$prefix=$request->getParameter("prefix");
  	$suffix=$request->getParameter("suffix");
  	$replace=$request->getParameter("replace");
  	$with=$request->getParameter("with");

  	$maxsellprices=$request->getParameter("maxsellprices");
  	$maxbuyprices=$request->getParameter("maxbuyprices");
  	$minsellprices=$request->getParameter("minsellprices");
  	$minbuyprices=$request->getParameter("minbuyprices");
  	$baseprices=$request->getParameter("baseprices");
    
  	$autocalcsellprices=$request->getParameter("autocalcsellprices");
  	$autocalcbuyprices=$request->getParameter("autocalcbuyprices");


  	$ids=$request->getParameter("product_ids");
	  $products=$producttype->getProductsByIds($ids);

		//if no checked products, do nothing
  	if(count($ids))
  	{
  		if($request->getParameter("submit")=="Merge")
  		{
  			$this->redirect("producttype/merge?producttype_id=".$producttype_id."&product_id=".$request->getParameter("input")."&duplicate_ids=".implode(",",$ids));
  			//$this->mainproduct=ProductTable::fetch($request->getParameter("input"));
  			//$this->products=$products;
//				$this->merge($mainproduct, $product);
  			//
  		}
  		//save publish
  		else if($request->getParameter("submit")=="Save")
  		{
		  	foreach($products as $product)
		  	{
		  		if($baseprices[$product->getId()]!="")
		  		{
						$product->setBaseprice(str_replace(",","",trim($baseprices[$product->getId()])));
		  		}
					
		  		if($maxsellprices[$product->getId()]!="")
		  		{
						$product->setMaxsellprice(str_replace(",","",$maxsellprices[$product->getId()]));
						$product->setAutocalcsellprice(0);
		  		}
					
		  		if($minsellprices[$product->getId()]!="")
		  		{
						$product->setMinsellprice(str_replace(",","",$minsellprices[$product->getId()]));
						$product->setAutocalcsellprice(0);
		  		}

		  		if($maxbuyprices[$product->getId()]!="")
		  		{
						$product->setMaxbuyprice(str_replace(",","",$maxbuyprices[$product->getId()]));
						$product->setAutocalcbuyprice(0);
		  		}

		  		if($minbuyprices[$product->getId()]!="")
		  		{
						$product->setMinbuyprice(str_replace(",","",$minbuyprices[$product->getId()]));
						$product->setAutocalcbuyprice(0);
		  		}

					$product->save();
		  	}
  		}
  		else if($request->getParameter("submit")=="Copy")
  		{
		  	foreach($products as $oldproduct)
		  	{
$product=new Product();
if(trim($request->getParameter("input"))=="")
	$product->setProducttypeId($oldproduct->getProducttypeId());
else
	$product->setProducttypeId($request->getParameter("input"));

$product->setName($oldproduct->getName()." copy");
$product->setDescription($oldproduct->getDescription()." copy");
$product->setBaseprice($oldproduct->getBaseprice());
$product->setSpec1($oldproduct->getSpec1());
$product->setSpec2($oldproduct->getSpec2());
$product->setSpec3($oldproduct->getSpec3());
$product->setSpec4($oldproduct->getSpec4());
$product->setSpec5($oldproduct->getSpec5());
$product->setSpec6($oldproduct->getSpec6());
$product->setSpec7($oldproduct->getSpec7());
$product->setSpec8($oldproduct->getSpec8());
$product->setSpec9($oldproduct->getSpec9());
$product->setCode($oldproduct->getCode());
$product->save();
				}
  		}
  		else if($request->getParameter("submit")=="Delete")
  		{
		  	foreach($products as $product)
		  	{
$product->delete();
				}
  		}
  		else if($request->getParameter("submit")=="Monitor")
  		{
		  	foreach($products as $product)
		  	{
					$product->setMonitored(1);
					$product->save();
				}
  		}
  		else if($request->getParameter("submit")=="Unmonitor")
  		{
		  	foreach($products as $product)
		  	{
					$product->setMonitored(0);
					$product->save();
				}
  		}
  		else if($request->getParameter("submit")=="Move")
  		{
		  	foreach($products as $product)
		  	{
					$product->setProducttypeId($request->getParameter("input"));
					$product->save();
				}
  		}
  		//string manipulation
			else 
			{
		  	foreach($products as $product)
		  	{
		  		//if(stripos($product->getName(),"2 ")!==false)
		  		{
						$product->setName($prefix.str_replace($replace,$with,$product->getName()).$suffix);
						
						//$product->setDescription($product->getName());
						$product->save();
		  		}
		  	}
			}
  	}

  	//$parent->calc();
    $this->redirect($request->getReferer());
  }
  public function executeProductmassoperspecs(sfWebRequest $request)
  {
    $producttype_id=$request->getParameter("producttype_id");
    $producttype=ProducttypeTable::fetch($producttype_id);

  	$prefix=$request->getParameter("prefix");
  	$suffix=$request->getParameter("suffix");
  	$replace=$request->getParameter("replace");
  	$with=$request->getParameter("with");

  	$spec1=$request->getParameter("spec1");
  	$spec2=$request->getParameter("spec2");
  	$spec3=$request->getParameter("spec3");
  	$spec4=$request->getParameter("spec4");
  	$spec5=$request->getParameter("spec5");
  	$spec6=$request->getParameter("spec6");
  	$spec7=$request->getParameter("spec7");
  	$spec8=$request->getParameter("spec8");
  	$spec9=$request->getParameter("spec9");
  	$code=$request->getParameter("code");
    
  	$autocalcsellprices=$request->getParameter("autocalcsellprices");
  	$autocalcbuyprices=$request->getParameter("autocalcbuyprices");


  	$ids=$request->getParameter("product_ids");
	  $products=$producttype->getProductsByIds($ids);

		//if no checked products, do nothing
  	if(count($ids))
  	{
  		if($request->getParameter("submit")=="Merge")
  		{
  			$this->redirect("producttype/merge?producttype_id=".$producttype_id."&product_id=".$request->getParameter("input")."&duplicate_ids=".implode(",",$ids));
  			//$this->mainproduct=ProductTable::fetch($request->getParameter("input"));
  			//$this->products=$products;
//				$this->merge($mainproduct, $product);
  			//
  		}
  		//save publish
  		else if($request->getParameter("submit")=="Save")
  		{
		  	foreach($products as $product)
		  	{
		  		if($spec1[$product->getId()]!="")
						$product->setSpec1(trim($spec1[$product->getId()]));
		  		if($spec2[$product->getId()]!="")
						$product->setSpec2(trim($spec2[$product->getId()]));
		  		if($spec3[$product->getId()]!="")
						$product->setSpec3(trim($spec3[$product->getId()]));
		  		if($spec4[$product->getId()]!="")
						$product->setSpec4(trim($spec4[$product->getId()]));
		  		if($spec5[$product->getId()]!="")
						$product->setSpec5(trim($spec5[$product->getId()]));
		  		if($spec6[$product->getId()]!="")
						$product->setSpec6(trim($spec6[$product->getId()]));
		  		if($spec7[$product->getId()]!="")
						$product->setSpec7(trim($spec7[$product->getId()]));
		  		if($spec8[$product->getId()]!="")
						$product->setSpec8(trim($spec8[$product->getId()]));
		  		if($spec9[$product->getId()]!="")
						$product->setSpec9(trim($spec9[$product->getId()]));
		  		if($code[$product->getId()]!="")
						$product->setCode(trim($code[$product->getId()]));
					
					$product->save();
		  	}
  		}
  		else if($request->getParameter("submit")=="Monitor")
  		{
		  	foreach($products as $product)
		  	{
					$product->setMonitored(1);
					$product->save();
				}
  		}
  		else if($request->getParameter("submit")=="Unmonitor")
  		{
		  	foreach($products as $product)
		  	{
					$product->setMonitored(0);
					$product->save();
				}
  		}
  		//string manipulation
			else 
			{
		  	foreach($products as $product)
		  	{
		  		//if(stripos($product->getName(),"2 ")!==false)
		  		{
						$product->setName($prefix.str_replace($replace,$with,$product->getName()).$suffix);
						
						//$product->setDescription($product->getName());
						$product->save();
		  		}
		  	}
			}
  	}

  	//$parent->calc();
    $this->redirect($request->getReferer());
  }
  public function executeSetstatus(sfWebRequest $request)
  {
    $producttype=MyModel::fetchOne("Producttype",array('id'=>$request->getParameter("id")));

    $producttype->setStatus($request->getParameter("color"));
    $producttype->save();

		$this->redirect($request->getReferer());
	}
  public function executeDump(sfWebRequest $request)
  {
    $this->products=Doctrine_Query::create()
        ->from('Product p')
		->orderBy('p.producttype_id,p.name')
		->execute();
	;
  }
  /*
  public function executeBarcode(sfWebRequest $request)
  {
    $this->products=Doctrine_Query::create()
      ->from('Product p')
		  ->orderBy('p.producttype_id,p.name')
		  ->execute();
	  ;
    $this->start=1;
  }
  */
  public function executeBarcodepdf(sfWebRequest $request)
  {
    $this->producttype=MyModel::fetchOne("Producttype",array('id'=>$request->getParameter("id")));
    $this->products = Doctrine_Query::create()
      ->from('Product p')
    	->where('p.producttype_id='.$this->producttype->getId())
		  ->orderBy('p.name')
      ->execute();

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
    
    switch($this->producttype->getProducttypeSchema()->getBarcodeTemplate())
    {
      case 'Simple 1 Column':
        $this->setTemplate('barcodesimple1pdf');
        break;
      case 'Simple 2 Column':
        $this->setTemplate('barcodesimple2pdf');
        break;
      case 'Simple 2 Column Green and White':
        $this->setTemplate('barcodesimple2gnwpdf');
        break;
      case 'Standard 3 Column':
        $this->setTemplate('barcodestandardpdf');
        break;
      default:
        $this->setTemplate('barcodesimple1pdf');
        break;
    }
  }
  public function executeParseSpecs(sfWebRequest $request)
  {
    $producttype=MyModel::fetchOne("Producttype",array('id'=>$request->getParameter("id")));

    $speccount=0;
    $maxspeccount=0;
    foreach($producttype->getProduct() as $product)
    {
      $speccount=$product->parseSpecs();
      if($maxspeccount<$speccount)$maxspeccount=$speccount;
    }

    //update speccount
    if($producttype->getSpecCount()<$maxspeccount)
    {
      $producttype->setSpecCount($maxspeccount);
      $producttype->save();
    }

    $this->redirect($request->getReferer());
  }
  
  //==============product quota system==============
  public function executeBelowQuotaIndex(sfWebRequest $request)
  {
    //select products where qty is below quota
     $this->producttypes = Doctrine_Query::create()
      ->from('Producttype p')
      ->orderBy('p.name')
      ->execute();
  }
  public function executeBelowQuota(sfWebRequest $request)
  {
    $this->producttype=null;
    if($request->hasParameter("id"))$this->producttype=MyModel::fetchOne("Producttype",array('id'=>$request->getParameter("id")));

    //select products where qty is below quota
     $query=Doctrine_Query::create()
      ->from('Stock s, s.Product p')
      ->where('s.currentqty < p.quota');
     if($request->hasParameter("id"))$query->andWhere('p.producttype_id=?',$request->getParameter("id"));
     $this->stocks = $query->execute();
  }
  public function executeBelowQuotaPdf(sfWebRequest $request)
  {
    $this->executeBelowQuota($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeFiles(sfWebRequest $request)
  {
    $this->producttype=MyModel::fetchOne("Producttype",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('producttype');
    $this->file->setParentId($this->producttype->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="producttype"')
      ->andWhere('f.parent_id='.$this->producttype->getId())
      ->execute();
  }
  public function executeToggleIsUpdated(sfWebRequest $request)
  {
    $producttype=MyModel::fetchOne("Producttype",array('id'=>$request->getParameter("id")));
    if($producttype->getIsUpdated())
      $producttype->setIsUpdated(null);
    else
      $producttype->setIsUpdated(1);
    $producttype->save();
    $this->getUser()->setFlash('notice', $notice.' Successfully set this product type to '.($producttype->getIsUpdated()?"Updated":"Not Updated").'.',true);
    $this->redirect($request->getReferer());
  }
    public function executeProcessDir(sfWebRequest $request)
    {
      //$this->producttype = Fetcher::fetchById("Producttype",$request->getParameter("id")); 
      $this->producttype_id = $request->getParameter("id"); 
      $input_qtys=$request->getParameter("qty_reported");
      
      //use parameter "balance" instead of "qty_reported"
      //to save unreported items
      if($request->getParameter("submit")=="Save Unreported")
      {
        $input_qtys=$request->getParameter("balance");
      }
      
      $input_notes=$request->getParameter("notes");
      $warehouse_id=$request->getParameter("warehouse_id");
      $date=$request->getParameter("date");
      
      //create array of stock ids from $input_qtys
      $input_stock_ids=array_keys($input_qtys);
      
      //load existing report stock entries 
      //from database
      $reports_stock_entries=Doctrine_Query::create()
        ->from('Stockentry se')
      	->whereIn('se.stock_id',$input_stock_ids)
      	->andWhere('se.type = "Report"')
      	->andWhere('se.datetime >= "'.$date.' 00:00"')
      	->andWhere('se.datetime <= "'.$date.' 23:59"')
      	->execute();

      //put these into an array by index
      $reports_se_by_stock_id=array();
      foreach($reports_stock_entries as $entry)
      {
        $reports_se_by_stock_id[$entry->getStockId()]=$entry;
      }

      //save qtys reported by identifying which stockentry, and if not found, create
      foreach($input_qtys as $stock_id=>$qty)
      {
        //if index exists
        if(isset($reports_se_by_stock_id[$stock_id]))
        {
          $se=$reports_se_by_stock_id[$stock_id];
          $notes=$input_notes[$stock_id];

          //if qty not specified
          //delete report
          if($qty==="")
          {
            $se->delete();
            continue;
          }
          //else if qty is specified
          else
          {

            //if qty is different
            if($se->getQtyReported()!=$qty)
            {
              $se->setQtyReported($qty);
              $se->setDescription($notes);
              $se->calcReport();
              $se->save();
              $se->getStock()->calcFromStockEntry($se);
            }
            //else if only notes is different
            //just update
            //do not calc
            else if($se->getDescription()!=$notes)
            {
              $se->setDescription($notes);
              $se->save();
            }
            //else nothing changed, do nothing
          }
        }
        //else $se does not exist
        else
        {
          //if qty not specified
          //do nothing
          if($qty==="")
          {
            continue;
          }
          //else create se
          else
          {
            $description=$input_notes[$stock_id];
            $stock=Fetcher::fetchById("Stock",$stock_id);

            $se=$stock->addEntry($date, 0, null, null ,"Report",$description,$this->getUser()->getGuardUser()->getId(),$qty);
          }
        }
      }
      
      $today=MyDateTime::frommysql($date); 
      return $this->redirect("producttype/dir?id=".$this->producttype_id."&invoice[date][day]=".$today->getDay()."&invoice[date][month]=".$today->getMonth()."&invoice[date][year]=".$today->getYear());
    }
  public function executeDirIndex(sfWebRequest $request)
  {
    //date form
    $requestparams=$request->getParameter("invoice");

    $day=$requestparams["date"]["day"];
    $month=$requestparams["date"]["month"];
    $year=$requestparams["date"]["year"];

    $invoice=new Invoice();
    if(!$day or !$month or !$year)
      $invoice->setDate(MyDate::today());
    else
      $invoice->setDate($year."-".$month."-".$day);

    //warehouse id
    $this->warehouse_id=$request->getParameter("warehouse_id");
    if(!$this->warehouse_id)
      $this->warehouse_id=SettingsTable::fetch('default_warehouse_id');
      
    //producttype id
    $this->producttype_id=$request->getParameter("producttype_id");
    if(!$this->producttype_id)
      $this->producttype_id=1;
      
    $this->form=new InvoiceForm($invoice);
    //end date form
  
/*
    //page system
    $itemsperpage=50;
    $item_class="Producttype";
    $this->pagepath="producttype/dirIndex";
    
    $page=$request->getParameter("page");
    if($page==null or $page<1)$page=1;
    $this->page=$page;
    $offset=$itemsperpage*($page-1);
    $itemcount=Doctrine_Query::create()
        ->from($item_class.' p')
      	->count();
    $pagecount=$itemcount/$itemsperpage;
    $pagecount=ceil($pagecount);
    $this->pagecount=$pagecount;
    //end page system

    $this->producttypes=Doctrine_Query::create()
        ->from('Producttype p')
       ->orderBy('p.name')
       ->offset($offset)
       ->limit($itemsperpage)
       ->execute();
*/  
  }

  public function executeMiscellaneousSearch(sfWebRequest $request)
  {
  }
  public function executeMiscellaneousSearchTest(sfWebRequest $request)
  {
    $this->searchstring=$request->getParameter("searchstring");
    $this->producttype_id=$request->getParameter("producttype_id");
    $this->producttype=Fetcher::fetchOne("Producttype",array("id"=>$this->producttype_id));
    $this->products=Doctrine_Query::create()
      ->from('Product p')
      ->where('p.producttype_id=1')
      ->andWhere('p.name like "%'.$this->searchstring.'%"')
      ->execute();
  }
  public function executeMiscellaneousSearchProcess(sfWebRequest $request)
  {
    $searchstring=$request->getParameter("searchstring");
    $producttype_id=$request->getParameter("producttype_id");
    $producttype=Fetcher::fetchOne("Producttype",array("id"=>$producttype_id));

    $products= Doctrine_Query::create()
       ->select('p.id')
       ->from('Product p')
       ->where('p.producttype_id=1')
       ->andWhere('p.name like "%'.$searchstring.'%"')
       ->fetchArray();
    $ids_array=array();
    foreach($products as $product)$ids_array[]=$product["id"];

    Doctrine_Query::create()
      ->update('Product p')
      ->set('p.producttype_id=?',$producttype->getId())
      ->set('p.general_producttype_id=?',$producttype->getId())
      ->set('p.specific_producttype_id=?',$producttype->getId())
      ->whereIn('p.id', $ids_array)
      ->execute();
    $this->redirect('producttype/miscellaneousSearch');
  }

  public function executeMassAddProducts(sfWebRequest $request)
  {
  }
  public function executeMassAddProductsTest(sfWebRequest $request)
  {
    $product_ids=$request->getParameter("product_ids");
    $producttype_id=$request->getParameter("producttype_id");
    $ids_array=explode(",",$product_ids);

    $this->products=Doctrine_Query::create()
        ->from('Product p')
        ->whereIn('p.id',$ids_array)
       ->execute();

    $this->producttype=Fetcher::fetchOne("Producttype",array("id"=>$producttype_id));

    $this->product_ids=$product_ids;
    $this->producttype_id=$producttype_id;
  }
  public function executeMassAddProductsProcess(sfWebRequest $request)
  {
    $product_ids=$request->getParameter("product_ids");
    $producttype_id=$request->getParameter("producttype_id");

    $ids_array=explode(",",$product_ids);

    $producttype=Fetcher::fetchOne("Producttype",array("id"=>$producttype_id));

    Doctrine_Query::create()
      ->update('Product p')
      ->set('p.producttype_id=?',$producttype->getId())
      ->set('p.general_producttype_id=?',$producttype->getId())
      ->set('p.specific_producttype_id=?',$producttype->getId())
      ->whereIn('p.id', $ids_array)
      ->execute();
    $this->redirect('producttype/massAddProducts');

  }

  public function executeMergeProductTypes(sfWebRequest $request)
  {
  }
  public function executeMergeTestProductTypes(sfWebRequest $request)
  {
    $old_producttype_ids=$request->getParameter("merger_producttype_ids");
    $new_producttype_id=$request->getParameter("mergee_producttype_id");
    $ids_array=explode(",",$old_producttype_ids);

    $this->old_producttypes=Doctrine_Query::create()
        ->from('Producttype p')
        ->whereIn('p.id',$ids_array)
       ->execute();

    $this->new_producttype=Fetcher::fetchOne("Producttype",array("id"=>$new_producttype_id));

    $this->old_producttype_ids=$old_producttype_ids;
    $this->new_producttype_id=$new_producttype_id;
  }
  public function executeMergeProcessProductTypes(sfWebRequest $request)
  {
    $old_producttype_ids=$request->getParameter("merger_producttype_ids");
    $new_producttype_id=$request->getParameter("mergee_producttype_id");

    $ids_array=explode(",",$old_producttype_ids);

    $old_producttypes=Doctrine_Query::create()
        ->from('Producttype p')
        ->whereIn('p.id',$ids_array)
       ->execute();

    $new_producttype=Fetcher::fetchOne("Producttype",array("id"=>$new_producttype_id));

    foreach($old_producttypes as $old_producttype)
    {
      Doctrine_Query::create()
      ->update('Product p')
      ->set('p.producttype_id=?',$new_producttype->getId())
      ->set('p.general_producttype_id=?',$new_producttype->getId())
      ->set('p.specific_producttype_id=?',$old_producttype->getId())
      ->whereIn('p.producttype_id', $old_producttype->getId())
      ->execute();
    }
    //set old product types to hidden
    Doctrine_Query::create()
    ->update('Producttype p')
    ->set('p.is_hidden=?',1)
    ->whereIn('p.id', $ids_array)
    ->execute();
    $this->redirect('producttype/mergeProductTypes');

  }
  public function executeMoveToProductTypes(sfWebRequest $request)
  {
    $old_producttype_ids=$request->getParameter("old_ids");
    $new_producttype_id=$request->getParameter("new_id");
  }
  public function executeListNotHidden(sfWebRequest $request)
  {
    $this->producttypes=Doctrine_Query::create()
      ->from('Producttype p')
      ->where('p.is_hidden=0')
      ->orderBy('p.name')
      ->execute();
  }
}
