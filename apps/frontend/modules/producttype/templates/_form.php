<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
<table>
<?php echo form_tag_for($form, '@producttype') ?>

<?php echo $form->renderHiddenFields(false) ?>
  <input name="producttype[path]" value="<?php echo $form->getObject()->getPath()?>" id="product_path" type="hidden">
  <input name="producttype[path_ids]" value="<?php echo $form->getObject()->getPathIds()?>" id="product_path_ids" type="hidden">
  <input name="producttype[parent_id]" value="<?php echo $form->getObject()->getParentId()?>" id="product_parent_id" type="hidden">
  <input name="producttype[priority]" value="<?php echo $form->getObject()->getPriority()?>" id="product_priority" type="hidden">

<?php if ($form->hasGlobalErrors()): ?>
  <?php echo $form->renderGlobalErrors() ?>
<?php endif; ?>

<tr>
  <td colspan=2>
    <?php include_partial('producttype/form_actions', array('producttype' => $producttype, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </td>
</tr>

<tr>
  <td>Name</td>
  <td><?php echo $form['name']; ?></td>
</tr>

<tr>
  <td>Description</td>
  <td><?php echo $form['description']; ?></td>
</tr>
<tr>
  <td>Is Updated</td>
  <td><?php echo $form['is_updated']; ?></td>
</tr>
<tr>
  <td>Status</td>
  <td><?php echo $form['status']; ?></td>
</tr>
<tr>
  <td>Notes</td>
  <td><?php echo $form['notes']; ?></td>
</tr>

<tr>
  <td colspan=10><hr></td>
</tr>
<tr>
  <td colspan=10><b>Product Naming Conventions</b> <?php echo link_to("(Help)","producttype/nameFormatHelp",array("target"=>"nameFormatHelp"));?></td>
</tr>
<tr>
  <td>Format for Product Name</td>
  <td><?php echo $form['product_name_format']; ?></td>
</tr>
<tr>
  <td>Format for Product Description</td>
  <td><?php echo $form['product_description_format']; ?></td>
</tr>
<tr>
  <td>Format for Barcode Sheet Labels</td>
  <td><?php echo $form['barcode_format']; ?></td>
</tr>
<tr>
  <td>Format for Barcode Sheet Labels 2nd Row</td>
  <td><?php echo $form['barcode_format_2']; ?></td>
</tr>
<tr>
  <td>Format for Barcode Sheet Labels 3rd Row</td>
  <td><?php echo $form['barcode_format_3']; ?></td>
</tr>
<tr>
  <td>Schema Template</td>
  <td><?php echo $form['producttype_schema_id']; ?></td>
</tr>
<tr>
  <td colspan=10><hr></td>
</tr>
<tr>
  <td colspan=10><b>Product Price Formulas</b> <?php echo link_to("(Help)","producttype/priceFormulaHelp",array("target"=>"priceFormulaHelp"));?></td>
</tr>
<tr>
  <td>Formula for Base Price</td>
  <td><?php echo $form['base_formula']; ?></td>
</tr>
<tr>
  <td>Formula for Maximum Selling Price</td>
  <td><?php echo $form['max_sell_formula']; ?></td>
</tr>
<tr>
  <td>Formula for Minimum Selling Price</td>
  <td><?php echo $form['min_sell_formula']; ?></td>
</tr>
<tr>
  <td>Formula for Maximum Purchase Price</td>
  <td><?php echo $form['max_buy_formula']; ?></td>
</tr>
<tr>
  <td>Formula for Minimum Purchase Price</td>
  <td><?php echo $form['min_buy_formula']; ?></td>
</tr>

<tr>
  <td colspan=10><hr></td>
</tr>
<tr>
  <td colspan=10><b>SPECS</b></td>
</tr>
<tr>
  <td>Number of Specifications</td>
  <td><?php echo $form['speccount']; ?></td>
</tr>
<tr>
  <td>Specification 1</td>
  <td><?php echo $form['spec1']; ?></td>
</tr>
<tr>
  <td>Specification 2</td>
  <td><?php echo $form['spec2']; ?></td>
</tr>
<tr>
  <td>Specification 3</td>
  <td><?php echo $form['spec3']; ?></td>
</tr>
<tr>
  <td>Specification 4</td>
  <td><?php echo $form['spec4']; ?></td>
</tr>
<tr>
  <td>Specification 5</td>
  <td><?php echo $form['spec5']; ?></td>
</tr>
<tr>
  <td>Specification 6</td>
  <td><?php echo $form['spec6']; ?></td>
</tr>
<tr>
  <td>Specification 7</td>
  <td><?php echo $form['spec7']; ?></td>
</tr>
<tr>
  <td>Specification 8</td>
  <td><?php echo $form['spec8']; ?></td>
</tr>
<tr>
  <td>Specification 9</td>
  <td><?php echo $form['spec9']; ?></td>
</tr>
<tr>
  <td>Is Hidden</td>
  <td><?php echo $form['is_hidden']; ?></td>
</tr>


<tr>
  <td colspan=2>
    <?php include_partial('producttype/form_actions', array('producttype' => $producttype, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </td>
</tr>

</form>
</table></div>

