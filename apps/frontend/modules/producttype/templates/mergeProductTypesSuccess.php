<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Merge Product Types</h1>
<br>
<?php echo form_tag("producttype/mergeTestProductTypes");?>
Product Type IDs to merge: 
<br>
<textarea name=merger_producttype_ids id=merger_producttype_ids>
</textarea>
<br><br>Product Type ID to merge into <input name=mergee_producttype_id id=mergee_producttype_id>
<br><br><input type=submit value=Test>
</form>
