<?php use_helper('I18N', 'Date') ?>

<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByName('ids[]'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked;
  } 
  return true;
}
function checkAllProductIds()
{
  var boxes = document.getElementsByName('product_ids[]'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('product_ids_header_checkbox').checked;
  } 
  return true;
}
/* ]]> */
</script>

<?php echo html_entity_decode($producttype->getBreadcrumbs()); ?>
&nbsp;&nbsp;<?php if(!$producttype->isRoot())echo link_to("(Edit)","producttype/edit?id=".$producttype->getId()) ?>
<hr>
<h1>Name Generator for Product Type: <?php echo $producttype->getName()." (".$producttype->getId().")" ?></h1>
<?php echo link_to("Go to Inventory Report","producttype/view?id=".$producttype->getId());?>
<br><?php echo link_to("Go to Price Generator","producttype/priceWizard?id=".$producttype->getId());?>
<br><?php echo link_to("Back to Product Type List","producttype/index");?>
<hr>
<br><?php echo link_to("Autorename Products according to Specs","producttype/autorename?id=".$producttype->getId(), array('confirm' => 'Rename all products: are you sure?'));?>
<hr>
<b></b>
<?php echo form_tag_for(new ProducttypeForm(),"producttype/productmassoperspecs")?>
<!--
Merge to product id / Move to product type id: <input name=input size=10 >
-->
<input type=hidden name=producttype_id value=<?php echo $producttype->getId()?> />
<!--
<br>
<input type=submit name=submit value=Merge />
-->
<input type=submit name=submit value=Save />
<!--
<input type=submit name=submit value=Rename />
<input type=submit name=submit value=Monitor />
<input type=submit name=submit value=Unmonitor />
<input type=submit name=submit value=Copy />
<input type=submit name=submit value=Move />
<input type=submit name=submit value=Delete />
<br>Rename: Prefix <input name=prefix size=10 > Suffix <input name=suffix size=10 > Replace <input name=replace size=10 > With <input name=with size=10 >
-->

<table border=1>
  <tr>
    <td>Id</td>
    <td><input id="product_ids_header_checkbox" onclick="checkAllProductIds();" type="checkbox"></td>
    <td>Product</td>
    <td>Description</td>
<?php if($producttype->getSpeccount()>=1){?>    
    <td><?php echo $producttype->getSpec1() ?></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=2){?>    
    <td><?php echo $producttype->getSpec2() ?></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=3){?>    
    <td><?php echo $producttype->getSpec3() ?></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=4){?>    
    <td><?php echo $producttype->getSpec4() ?></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=5){?>    
    <td><?php echo $producttype->getSpec5() ?></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=6){?>    
    <td><?php echo $producttype->getSpec6() ?></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=7){?>    
    <td><?php echo $producttype->getSpec7() ?></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=8){?>    
    <td><?php echo $producttype->getSpec8() ?></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=9){?>    
    <td><?php echo $producttype->getSpec9() ?></td>
<?php } ?>
    <td>Code</td>
  </tr>
  <?php
//      $maxtotal=0;
  	if($producttype->getId()==1)$products=array();
  	else $products=$producttype->getProducts();
  	foreach($products as $product){
  /* 
		//count number of invoicedetails * qty sold since a certain date
		$details = Doctrine_Query::create()
      ->from('Invoicedetail pd, pd.Invoice i')
      ->where('pd.product_id = '.$product->getId())
//      ->andWhere('i.date >= "2010-09-28"')
      ->execute();
      $max=0;
foreach($details as $detail){$max+=$detail->getQty();}

	$maxtotal+=$max;*/
  	
 	?>
  <tr>
    <td><?php echo $product->getId() ?></td>
    <td><input type=checkbox name="product_ids[]" value="<?php echo $product->getId()?>" /></td>
    <td><?php echo link_to($product->getName(),"product/view?id=".$product->getId()) ?></td>
    <td><?php echo $product->getDescription() ?></td>


<?php if($producttype->getSpeccount()>=1){?>    
    <td align=right><?php echo $product->getSpec1() ?><br><input name="spec1[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=2){?>    
    <td align=right><?php echo $product->getSpec2() ?><br><input name="spec2[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=3){?>    
    <td align=right><?php echo $product->getSpec3() ?><br><input name="spec3[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=4){?>    
    <td align=right><?php echo $product->getSpec4() ?><br><input name="spec4[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=5){?>    
    <td align=right><?php echo $product->getSpec5() ?><br><input name="spec5[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=6){?>    
    <td align=right><?php echo $product->getSpec6() ?><br><input name="spec6[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=7){?>    
    <td align=right><?php echo $product->getSpec7() ?><br><input name="spec7[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=8){?>    
    <td align=right><?php echo $product->getSpec8() ?><br><input name="spec8[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>
<?php if($producttype->getSpeccount()>=9){?>    
    <td align=right><?php echo $product->getSpec9() ?><br><input name="spec9[<?php echo $product->getId()?>]" size=1 /></td>
<?php } ?>

    <td align=right><?php echo $product->getCode() ?><br><input name="code[<?php echo $product->getId()?>]" size=1 /></td>


    <td><?php echo link_to("Edit","product/edit?id=".$product->getId()) ?></td>
    <td><?php echo link_to(
  'Delete',
  'product/delete?id='.$product->getId(),
  array('method' => 'delete', 'confirm' => 'Are you sure?')
) ?></td>
  </tr>
  <?php }?>
</table>
<!--
<input type=submit name=submit value=Merge />
-->
<input type=submit name=submit value=Save />
<!--
<input type=submit name=submit value=Rename />
<input type=submit name=submit value=Monitor />
<input type=submit name=submit value=Unmonitor />
<input type=submit name=submit value=Copy />
<input type=submit name=submit value=Move />
<input type=submit name=submit value=Delete />
-->
</form>

<hr>
<?php echo form_tag("producttype/view?id=".$producttype->getId(),array("method"=>"get"))?>
View <input name=levels size=1 value=<?php echo $levels?>><input type=submit name=submit value=Levels />
</form>

<br>
<?php echo form_tag_for(new ProducttypeForm(),"producttype/massoper")?>
<table>
  <tr>
    <td>Id</td>
    <td><input id="sf_admin_list_batch_checkbox" onclick="checkAll();" type="checkbox"></td>
    <td>Name</td>
    <td>Description</td>
  </tr>

  <?php include_partial("showchildren",array("producttype"=>$producttype,"children"=>$producttype->getChildren(),"levels"=>$levels,"level"=>0)); ?>

</table>

<?php echo link_to("Add Child Product Type","producttype/new?parent_id=".$producttype->getId()) ?>
<br>
<?php echo link_to("Add Product","product/new?producttype_id=".$producttype->getId()) ?>
<?php //echo link_to("Add Product","producttype/newProduct?producttype_id=".$producttype->getId()) ?>

<br>
<input name=parent_id size=1 >
<input type=submit name=submit value=Move />
<input type=submit name=submit value=Copy />
<input type=submit name=submit value="Copy Schema" />
</form>

<?php //echo $maxtotal?>
