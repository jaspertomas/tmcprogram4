<?php echo link_to("Print Inventory Sheet for All Products","producttype/viewAllPdf");?>

<h1>Daily Inventory Report: Product Type List </h1>

<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new InvoiceForm(),"producttype/dir")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
?>
<table>
  <tr>
    <td>Date<?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>Warehouse: <?php
      $w=new sfWidgetFormDoctrineChoice(array(
        'model'     => 'Warehouse',
        'add_empty' => false,
      ));
      echo $w->render('warehouse_id',$warehouse_id);
      ?>
    </td>
  </tr>
  <tr>
    <td>Product Type: <?php
      $w=new sfWidgetFormDoctrineChoice(array(
        'model'     => 'Producttype',
        'add_empty' => false,
      ));
      $w->addOption('order_by',array('name','asc'));
      echo $w->render('id',$producttype_id);
      ?>
    </td>
  </tr>
  <tr>
    <td><input type=submit value=View ></td>
  </tr>
</table>

<?php /*
    <?php echo link_to("Yesterday","producttype/dirIndex?invoice[date][day]=".$yesterday->getDay()."&invoice[date][month]=".$yesterday->getMonth()."&invoice[date][year]=".$yesterday->getYear()); ?>
    <?php echo link_to("Tomorrow","producttype/dirIndex?invoice[date][day]=".$tomorrow->getDay()."&invoice[date][month]=".$tomorrow->getMonth()."&invoice[date][year]=".$tomorrow->getYear()); ?>
</form>

<hr>

<!--page system-->
Page
<?php echo $page;?>
<br>
<?php
echo ($page==1?"First":link_to("First","producttype/listunreceived?page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."?page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo $apage." ";
  else echo link_to("$apage",$pagepath."?page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."?page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."?page=".$pagecount))." ";
?>
<!--end page system-->

<table>
<?php foreach($producttypes as $type){ ?>

<tr>
  <td><?php echo link_to($type->getName(),"producttype/dir?id=".$type->getId()."&invoice[date][day]=".$today->getDay()."&invoice[date][month]=".$today->getMonth()."&invoice[date][year]=".$today->getYear()); ?></td>
</tr>

<?php } ?>
</table>
*/?>
