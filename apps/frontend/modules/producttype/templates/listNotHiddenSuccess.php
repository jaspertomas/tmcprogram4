<?php use_helper('I18N', 'Date') ?>

<h2>Product Types</h2>
<?php echo link_to("Print Inventory Sheet for All Products","producttype/viewAllPdf");?>
<br>
<?php echo link_to("New Product Type","producttype/new");?>
<br>
<?php echo link_to("Go to Dynamic List","producttype");?>
<br>
<br>
<table border=1>
  <tr>
    <td>ID</td>
    <td>Name</td>
    <td>Description</td>
    <td>Edit</td>
  </tr>
  <?php foreach($producttypes as $producttype){?>
  <tr>
    <td><?php echo $producttype->getId()?></td>
    <td><?php echo link_to($producttype->getName(),"producttype/view?id=".$producttype->getId())?></td>
    <td><?php echo $producttype->getDescription()?></td>
    <td><?php echo link_to("Edit","producttype/edit?id=".$producttype->getId())?></td>
  </tr>
  <?php } ?>
</table>
