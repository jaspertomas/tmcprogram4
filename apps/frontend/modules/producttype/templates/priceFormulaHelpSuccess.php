<h1>Product Price Formulas</h1>
Product Types have the ability to set the 5 prices (base price, min and max buy prices and sell prices) automatically for all the products in them.

<h2>Keywords</h2>
<b>base</b>: Base Price
<br>
<b>maxsell</b>: Maximum Selling Price (Standard selling price)
<br>
<b>minsell</b>: Minimum Selling Price (Can't sell below this price)
<br>
<b>maxbuy</b>: Maximum Purchase Price
<br>
<b>minbuy</b>: Minimum Purchase Price
<br>
<h2>Operators</h2>
<b>Arithmetic operators</b>: + - * / separated by spaces from other keywords
<br>
&emsp;Usage: 
<br>
&emsp;&emsp;base * 1.5 gives base price multiplied by 1.5
<br>
&emsp;&emsp;maxbuy + 50 gives a profit of 50 pesos on top of the maximum purchase price
<br>
<br>
<b>Less</b>: Discount
<br>
&emsp;Usage: 
<br>
&emsp;&emsp;base less 15 less 5 gives base price with a 5% discount on top of a 15% discount
<br>
<br>
<b>Round</b>: Round off to the nearest 1, 10, 100, etc. 
<br>
&emsp;Usage: 
<br>
&emsp;&emsp;123 round 100 gives 200
<br>
&emsp;&emsp;1234 round 100 gives 1300
<br>
&emsp;&emsp;12345 round 100 gives 12400

