<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByName('ids[]'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked;
  } 
  return true;
}
function checkAllProductIds()
{
  var boxes = document.getElementsByName('product_ids[]'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('product_ids_header_checkbox').checked;
  } 
  return true;
}
/* ]]> */
</script>

<?php echo html_entity_decode($producttype->getBreadcrumbs()); ?>
&nbsp;&nbsp;<?php if(!$producttype->isRoot())echo link_to("(Edit)","producttype/edit?id=".$producttype->getId()) ?>
<?php /*
<hr>Is Updated: <?php echo $producttype->getIsUpdated()?"Yes":"No" ?> (<?php echo link_to("Set to ". ($producttype->getIsUpdated()?"No":"Yes"),"producttype/toggleIsUpdated?id=".$producttype->getId())?>)
<br><?php echo link_to("Print Inventory Sheet","producttype/viewPdf?id=".$producttype->getId());?>
<br><?php echo link_to("View Products Below Quota","producttype/belowQuota?id=".$producttype->getId());?>
<br><?php echo link_to("View Files","producttype/files?id=".$producttype->getId());?>
*/?>
<hr>
<h1>Inventory Report for Product Type: <?php echo $producttype->getName()." (".$producttype->getId().")" ?></h1>
<?php echo link_to("Go to Price Generator","producttype/priceWizard?id=".$producttype->getId());?>
<br><?php echo link_to("Go to Name Generator / Specs Editor","producttype/viewSpecs?id=".$producttype->getId());?>
<br><?php echo link_to("Back to Product Type List","producttype/listNotHidden");?>
<hr>
<br>
<!---------------------------------------->

<?php use_helper('I18N', 'Date') ?>
<?php echo form_tag_for(new InvoiceForm(),"producttype/dir")?>
<?php 
$today=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday=MyDateTime::frommysql($form->getObject()->getDate()); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($form->getObject()->getDate()); 
$tomorrow->adddays(1);
?>
<table>
  <tr>
    <td>Date<?php echo $form["date"] ?></td>
  </tr>
  <tr>
    <td>Warehouse: <?php
      $w=new sfWidgetFormDoctrineChoice(array(
        'model'     => 'Warehouse',
        'add_empty' => false,
      ));
      echo $w->render('warehouse_id',$warehouse_id);
      ?>
    </td>
  </tr>
  <tr>
    <td>Product Type: <?php
      $w=new sfWidgetFormDoctrineChoice(array(
        'model'     => 'Producttype',
        'add_empty' => false,
      ));
      $w->addOption('order_by',array('name','asc'));
      echo $w->render('id',$producttype->getId());
      ?>
    </td>
  </tr>
  <tr>
    <td><input type=submit value=View ></td>
  </tr>
</table>
    <?php echo link_to("Yesterday","producttype/dir?id=".$producttype->getId()."&invoice[date][day]=".$yesterday->getDay()."&invoice[date][month]=".$yesterday->getMonth()."&invoice[date][year]=".$yesterday->getYear()); ?>
    <?php echo link_to("Tomorrow","producttype/dir?id=".$producttype->getId()."&invoice[date][day]=".$tomorrow->getDay()."&invoice[date][month]=".$tomorrow->getMonth()."&invoice[date][year]=".$tomorrow->getYear()); ?>
</form>


Date: <?php echo MyDateTime::frommysql($form->getObject()->getDate())->toshortdate();$datearray=explode("-",$form->getObject()->getDate());?>

<br><?php echo link_to("Print","producttype/viewPdf?id=".$producttype->getId()."&invoice[date][year]=".$datearray[0]."&invoice[date][month]=".$datearray[1]."&invoice[date][day]=".$datearray[2]);?>

<br>
<br>
<?php echo form_tag("producttype/processDir");?>
<input type=hidden name=date id=date value="<?php echo $form->getObject()->getDate()?>">
<input type=hidden name=warehouse_id id=warehouse_id value="<?php echo $warehouse_id?>">
<input type=hidden name=id id=id value="<?php echo $producttype->getId()?>">
<table border=1>
  <td>
    <?php if(count($array)!=0){?>
      <input name=submit type=submit value=Save >
      <!--
      <input name=submit type=submit value="Save Unreported" >
      -->
    <?php } ?>
  </td>
  <tr>
    <td>Product</td>
    <td>Qty</td>
    <td>Rpt</td>
    <td>As of</td>
    <td>Comment</td>
    <td>Update</td>
    <td>Notes</td>
  </tr>
<?php foreach($array as $stock_id=>$item){?>
  <tr>
    <td><?php echo link_to($item["product_name"],"stock/view?id=".$item['stock_id'])?></td>
    <td align=right><?php echo $item["endbalance"]-0?></td>
    <td align=right><?php echo $item["qty_reported"]?></td>
    <td align=right><?php echo $item["datetime"]?></td>
    <td align=center><?php echo $item["comment"]?></td>
    <td align=right>
      <?php if($sf_user->hasCredential(array('admin','encoder'),false)){?>
      
        <!--If Save Unreported, This is saved instead-->
        <input type=hidden name=balance[<?php echo $item['stock_id']?>] id=balance[<?php echo $item['stock_id']?>] value=<?php if($item["qty_reported"]===null)echo $item["endbalance"];else echo $item["qty_reported"];?>>

        <input size=1 name=qty_reported[<?php echo $item['stock_id']?>] id=qty_reported[<?php echo $item['stock_id']?>] value=<?php echo $item["qty_reported"]?>>
      <?php } ?>
    </td>
    <td align=right>
      <?php echo $item["notes"]?>    
      <?php if($sf_user->hasCredential(array('admin','encoder'),false)){?>
        <input size=1 name=notes[<?php echo $item['stock_id']?>] id=notes[<?php echo $item['stock_id']?>] value="<?php echo $item["notes"]?>">
      <?php } ?>
    </td>
    <td>Print Barcode</td>
    <td><?php echo link_to("Small","product/barcodethermalpdf?product_id=".$item["product_id"]) ?></td>
    <td><?php echo link_to("Large","product/barcodethermallargepdf?product_id=".$item["product_id"]) ?></td>
  </tr>
  <?php } ?>
  <td>
    <?php if(count($array)!=0){?>
      <input name=submit type=submit value=Save >
      <!--
      <input name=submit type=submit value="Save Unreported" >
      -->
    <?php } ?>
  </td>
</table>
</form>

<br>
<br>
<hr>
<?php echo html_entity_decode($producttype->getBreadcrumbs()); ?>
<h2>Branches</h2>

<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked;
  } 
  return true;
}
/* ]]> */
</script>

<?php echo form_tag("producttype/dir?id=".$producttype->getId(),array("method"=>"get"))?>
View <input name=levels size=1 value=<?php echo $levels?>><input type=submit name=submit value=Levels />
</form>
<br>
<?php echo form_tag_for(new ProducttypeForm(),"producttype/massoper")?>
<table>
  <tr>
    <td>Id</td>
    <td><input id="sf_admin_list_batch_checkbox" onclick="checkAll();" type="checkbox"></td>
    <td>Name</td>
    <td>Description</td>
    <td>Type</td>
  </tr>

  <?php include_partial("showchildren",array("producttype"=>$producttype,"children"=>$producttype->getChildren(),"levels"=>$levels,"level"=>0)); ?>

</table>

<?php echo link_to("Add Child","producttype/new?parent_id=".$producttype->getId()) ?>
<br>
<br>
<input name=parent_id size=1 >
<input type=submit name=submit value=Move />
<input type=submit name=submit value=Copy />
</form>

<hr/>
<h3>Branch IDS</h3>
<textarea cols=50 rows=5>
<?php echo($children_ids); ?>
</textarea>
