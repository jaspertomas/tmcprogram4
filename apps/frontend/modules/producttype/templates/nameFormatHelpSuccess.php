<h1>Product Naming Conventions</h1>
Product Types have the ability to set names of its products using the product's specifications.
This page shows you how.

<h2>Keywords</h2>
<b>Specification Names</b>: The product type Bestank BSTR has 2 specifications: liters and gallons. Adding these specification names to the product name format will add the spec value to it.
<br>
&emsp;Usage: producttype+liters gives Bestank BSTR640
<br>
<br>
<b>ProductType</b>: This adds the product type's name into the product name (or description).
<br>
&emsp;Usage: producttype+ +liters gives Bestank BSTR 640 (Notice the space between producttype and liters)
<br>
<br>
<b>ProductTypeDesc</b>: This adds the product type's description into the product name (or description).
<br>
&emsp;Usage: producttypedesc+ +liters gives Vertical Stainless Storage Tank 640
<br>
<br>
<b>Pad</b>: Padding allows you make name fragments of the same length, making the products easier to sort alphabetically. Example: Alphabetically, Bestank 640 will come after Bestank 1000. To fix this, there must be an additional space before 640, making it Bestank&nbsp;&nbsp;640. Now, Bestank 640 will come before Bestank 1000. 
<br>
&emsp;Usage: producttype+ +pad+6+liters will put spaces before the liters spec so that it is always 6 characters long. This gives Bestank BSTR&nbsp;&nbsp;&nbsp;640 with 3 spaces between BSTR and 640
<br>
<br>
<b>Apostrophe</b>: By putting an apostrophe before a word, you can add that word to the name / description without it being translated.
<br>
&emsp;Usage: producttypedesc+ +liters+ +'Liters gives Vertical Stainless Storage Tank 640 Liters
<br>
<br>
<b>Code</b>: The product Goulds SJ10 has a product code - SJ10. Adding the word "code" to the naming format adds this product code to the name
<br>
&emsp;Usage: producttype+ +code gives Goulds SJ10
<br>
<br>
<b>Others</b>: words that the program doesn't understand will appear in the name as is
<br>
&emsp;Usage: producttype+ +code+asdf gives Goulds SJ10asdf
<br>
<br>

