<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Mass Add Products to Product Types</h1>
<?php echo form_tag("producttype/massAddProductsProcess");?>
  <input type="hidden" name="product_ids" value="<?php echo $product_ids?>">
  <input type="hidden" name="producttype_id" value="<?php echo $producttype_id?>">

  <h3>Products to Add</h3>
  <?php foreach($products as $p){?>    
    <?php echo $p->getName()?><br>
  <?php } ?>

  <h3>Product Type to Add Into</h3>
  <?php echo $producttype->getName()?>
    
  <br><br><input type=submit value=Submit>

</form>
