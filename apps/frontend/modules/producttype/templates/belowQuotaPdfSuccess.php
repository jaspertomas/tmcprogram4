<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');
$date=MyDate::today();

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$title='Products that Need Reordering: '
  .($producttype?$producttype->getName():"ALL Products")
  ;
$pdf->SetTitle(sfConfig::get('custom_company_name').": ".$title);

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(20, 10, 20, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================

/*
// 	Image ($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
$pdf->Image(sfConfig::get('custom_company_header_image'), '', '', 100, '', 'PNG', '', '', false, 300, 'C', false, false, 0, false, false, false);
$pdf->write(5,"\n\n\n\n");
$pdf->SetFont('helvetica', '', 10, '', true);
//$pdf->write(0,sfConfig::get('custom_company_address'),'',false,'C',true,0,false,false,0,0);
$pdf->write(0,sfConfig::get('custom_company_email'),'',false,'C',true,0,false,false,0,0);
$pdf->write(5,"\n");
*/
$pdf->write(0,sfConfig::get('custom_company_name'),'',false,'C',true,0,false,false,0,0);
$pdf->write(0,$title,'',false,'C',true,0,false,false,0,0);
$pdf->write(0,"Date: ".MyDateTime::frommysql($date)->toshortdate(),'',false,'C',true,0,false,false,0,0);
$pdf->write(5,"\n");

//====TABLE HEADER===========================

$widths=array(10,55,55,15,15);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl = <<<EOD
<table border="1">
<thead>
 <tr>
  <td width="$widths[0]" align="center"><b>ID</b></td>
  <td width="$widths[1]" align="center"><b>Name</b></td>
  <td width="$widths[2]" align="center"><b>Description</b></td>
  <td width="$widths[3]" align="center"><b>Quota</b></td>
  <td width="$widths[4]" align="center"><b>Qty</b></td>
 </tr>
</thead>
EOD;

//===TABLE BODY============================
$height=1;
  foreach($stocks as $stock)
  {
    $product=$stock->getProduct();
    $content=array(
      $product->getId(),
      $product->getName(),
      $product->getDescription(),
      $product->getQuota(),
      $stock->getCurrentqty(),
    );
    
$tbl .= <<<EOD
<tr>
<td width="$widths[0]" align="center">$content[0]</td>
<td width="$widths[1]" align="center">$content[1]</td>
<td width="$widths[2]" align="center">$content[2]</td>
<td width="$widths[3]" align="right">$content[3]</td>
<td width="$widths[4]" align="right">$content[4]</td>
</tr>
EOD;
  }


//===TABLE END============================
$tbl .= <<<EOD
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

//===AFTER TABLE=====================
/*
$pdf->write(0,"\n\n\tPlease prepare a cheque payable to ",'',false,'L',false,0,false,true,0,0);
$pdf->SetFont('helveticaB', '', 10, '', true);//bold font
$pdf->write(0,sfConfig::get('custom_cheques_payable_to').".",'',false,'L',true,0,false,false,0,0);
$pdf->SetFont('helvetica', '', 10, '', true);//normal font

$pdf->write(0,"\n\n\tRespectfully yours,",'',false,'L',true,0,false,false,0,0);
$pdf->write(0,"\n\n\t".sfConfig::get('custom_billing_statement_signatory_name'),'',false,'L',true,0,false,false,0,0);
$pdf->write(0,"\t".sfConfig::get('custom_billing_statement_signatory_position'),'',false,'L',true,0,false,false,0,0);
*/

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('BelowQuota-'.MyDateTime::frommysql(MyDate::today())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

