<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Search Miscellaneous Product Type</h1>

<?php echo form_tag("producttype/miscellaneousSearchTest");?>
Search:
<input name="searchstring"/>
<br><br>Product Type ID to add into <input name=producttype_id id=producttype_id>
<br><br><input type=submit value=Test>
</form>
