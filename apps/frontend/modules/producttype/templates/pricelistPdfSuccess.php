<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLEGAL", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$title='Inventory Sheet for Product Type: '.$producttype->getName();
$pdf->SetTitle($title);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

$pdf->write(0,"Tradewind Visayas",'',false,'',true,0,false,false,0,0);
$pdf->write(0,$title,'',false,'',true,0,false,false,0,0);
$pdf->SetFont('dejavusans', '', 12, '', true);
$pdf->write(0,"Printed on: ".MyDateTime::frommysql(MyDate::today())->toshortdate(),'',false,'',true,0,false,false,0,0);
$pdf->write(0,"",'',false,'',true,0,false,false,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);

//$pdf->write(0,"Total Sales: ".MyDecimal::format($total),'',false,'',true,0,false,false,0,0);

// ---------------------------------------------------------

//$pdf->SetFont('dejavusans', '', 12, '', true);
$contents=array();
       $contents[]=array(
          'Product',
          'Qty',
          );
/*
foreach($producttype->getProduct() as $product)
{
  $stock=$stockarray[$product->getId()];
	$contents[]=array(
      $product->getName(),
      $stock->getCurrentQty()>0?number_format($stock->getCurrentQty(),0,".",","):" --- ",
      );
}
*/
foreach($producttype->getProducts() as $product)
//if stock entry found
if(isset($stockarray[$product->getId()]))
{
  
  $stock=$stockarray[$product->getId()];
  $contents[]=array(
      $product->getName(),
      //$stock->getCurrentQty()>0?number_format($stock->getCurrentQty(),0,".",","):" --- ",
      number_format($stock->getCurrentQty(),0,".",","),
      );
}
//else if no stock entry
else
{
  $contents[]=array(
      $product->getName(),
      "0",
      );
}

$widths=array(170,20);
foreach($contents as $content)
{
  $height=1;
  $heightmultiplier=6.5;
  foreach($content as $index=>$txt) 
  {
    $numlines=$pdf->getNumLines($txt,$widths[$index],false,true,'','');
    if($height<$numlines*6.5)$height=$numlines*6.5;
  }
  $pdf->MultiCell($widths[0], $height, $content[0], 1, 'L', 0, 0, '', '', true,0,true);
  $pdf->MultiCell($widths[1], $height, $content[1], 1, 'R', 0, 1, '', '', true,0,true);
}

// ---------------------------------------------------------


// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Pricelist-'.$producttype->getName().'-'.MyDateTime::frommysql(MyDate::today())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

