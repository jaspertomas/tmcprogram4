<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Search Miscellaneous Product Type</h1>

<?php echo form_tag("producttype/miscellaneousSearchProcess");?>
Search String: <?php echo $searchstring?>
<br><br>Products:
<br><br><?php foreach($products as $product){?>
  <?php echo $product->getName()?><br>
<?php } ?>

<br><br>Product Type to add into: 
<?php echo $producttype->getName()?><br>

<input type=hidden name=producttype_id value=<?php echo $producttype->getId()?>>
<input type=hidden name=searchstring value="<?php echo $searchstring?>">
<br><br><input type=submit value=Submit>
</form>
