<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Merge Product Types</h1>
<?php echo form_tag("producttype/mergeProcessProductTypes");?>
  <input type="hidden" name="merger_producttype_ids" value="<?php echo $old_producttype_ids?>">
  <input type="hidden" name="mergee_producttype_id" value="<?php echo $new_producttype_id?>">

  <h3>Product Types to Merge</h3>
  <?php foreach($old_producttypes as $p){?>    
    <?php echo $p->getName()?><br>
  <?php } ?>

  <h3>Product Type to Merge Into</h3>
  <?php echo $new_producttype->getName()?>
    
  <br><br><input type=submit value=Submit>

</form>
