<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

//require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

class MYPDF extends TCPDF {
    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF("P", PDF_UNIT, "GOVERNMENTLEGAL", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$title='Inventory Sheet for All Products';
$pdf->SetTitle($title);

// remove default header/footer
$pdf->setPrintHeader(false);
//$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 10, 10,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

$pdf->write(0,sfConfig::get('custom_company_name'),'',false,'',true,0,false,false,0,0);
$pdf->write(0,$title,'',false,'',true,0,false,false,0,0);
$pdf->SetFont('dejavusans', '', 12, '', true);
$pdf->write(0,"Printed on: ".MyDateTime::frommysql(MyDate::today())->toshortdate(),'',false,'',true,0,false,false,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);

//$pdf->write(0,"Total Sales: ".MyDecimal::format($total),'',false,'',true,0,false,false,0,0);

// ---------------------------------------------------------

//width
$widths=array(100,10,34,34);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;
$colspan3=(10+12+12)*$scale;



foreach($producttypes as $producttype)
{
  $products=Doctrine_Query::create()
    ->from('Product p')
    ->where('p.producttype_id='.$producttype->getId())
    ->orderBy('p.name')
    ->execute();

  $productids=array();
  foreach($products as $product)
    $productids[]=$product->getId();

  $stocks=Doctrine_Query::create()
    ->from('Stock s')
    ->whereIn('s.product_id', $productids)
    ->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
    ->execute();
  $stockarray=array();
  foreach($stocks as $stock)
  {
   $stockarray[$stock->getProductId()]=$stock;
  }
  
  //$pdf->SetFont('dejavusans', '', 14, '', true);
  $pdf->write(0,'Product Type: '.$producttype->getName(),'',false,'',true,0,false,false,0,0);

  //$pdf->SetFont('dejavusans', '', 12, '', true);

//header
$contents=array(
  'Product',
  'Qty',
  'In Storage',
  'On Display',
  );
$tbl = <<<EOD
<table border="1">
<thead>
 <tr>
  <th width="$widths[0]" align="center">$contents[0]</th>
  <th width="$widths[1]" align="center">$contents[1]</th>
  <th width="$widths[2]" align="center">$contents[2]</th>
  <th width="$widths[3]" align="center">$contents[3]</th>
 </tr>
</thead>
EOD;
$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="center"></td>
  <td width="$widths[1]" align="center"></td>
  <td width="$widths[2]" align="center"></td>
  <td width="$widths[3]" align="center"></td>
 </tr>
EOD;
//end header

  $contents=array();
  foreach($producttype->getProducts() as $product)
  //if stock entry found
  if(isset($stockarray[$product->getId()]))
  {
    
    $stock=$stockarray[$product->getId()];
	  $contents[]=array(
        $product->getName(),
        //$stock->getCurrentQty()>0?number_format($stock->getCurrentQty(),0,".",","):" --- ",
        number_format($stock->getCurrentQty(),0,".",","),
      '',
      '',
        );
  }
  //else if no stock entry
  else
  {
	  $contents[]=array(
        $product->getName(),
        "0",
      '',
      '',
        );
  }

  foreach($contents as $content)
  {
$tbl .= <<<EOD
 <tr>
  <td width="$widths[0]" align="left">$content[0]</td>
  <td width="$widths[1]" align="right">$content[1]</td>
  <td width="$widths[2]" align="center"></td>
  <td width="$widths[3]" align="center"></td>
 </tr>
EOD;
  }
$tbl .= <<<EOD
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');
  
  
  
  
  
}    



// ---------------------------------------------------------


// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Inventory-All-'.MyDateTime::frommysql(MyDate::today())->tomysql().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

