<?php

//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
//$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLEGAL", true, 'UTF-8', false);
$pdf = new TCPDF("P", PDF_UNIT, "LETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Tradewind Mdsg Corp Daily Sales Report');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(7.5, 4, 5,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

$style = array(
	'position' => '',
	'align' => 'C',
	'stretch' => false,
	'fitwidth' => true,
	'cellfitalign' => '',
	'border' => false,
	'padding' => '0',
	'fgcolor' => array(0,0,0),
	'bgcolor' => false, //array(255,255,255),
	'text' => true,
	'font' => 'helvetica',
	'fontsize' => 8,
	'stretchtext' => 4
);

//map of cells to be printed; initially all turned off 
$pos=array();

//turn them on if they are between start and end
foreach($products as $count=>$product)
{
  $row=$count/3;
  $col=$count%3;
  if(!isset($pos[$row]))$pos[$row]=array();
  $pos[$row][$col]=$count;
}

//barcode height
$h=13;
//column width
$w=70;

foreach($pos as $line)
{
  if(isset($line[0]))
    $pdf->write1DBarcode(str_pad($products[$line[0]]->getId(),7,"0",STR_PAD_LEFT)."P", 'C128A', '6', '', '', $h, 0.5, $style, 'T');
  if(isset($line[1]))
    $pdf->write1DBarcode(str_pad($products[$line[1]]->getId(),7,"0",STR_PAD_LEFT)."P", 'C128A', '77', '', '', $h, 0.5, $style, 'T');
  if(isset($line[2]))
    $pdf->write1DBarcode(str_pad($products[$line[2]]->getId(),7,"0",STR_PAD_LEFT)."P", 'C128A', '148', '', '', $h, 0.5, $style, 'T');

  $pdf->write1DBarcode('4800529501110', 'EAN13', '500', '', '', $h, 0.4, $style, 'N');

  if(isset($line[0]))
    $pdf->Cell($w, 0, html_entity_decode($products[$line[0]]->getName()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if(isset($line[1]))
    $pdf->Cell($w, 0, html_entity_decode($products[$line[1]]->getName()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if(isset($line[2]))
    $pdf->Cell($w, 0, html_entity_decode($products[$line[2]]->getName()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  $pdf->ln();

  if(isset($line[0]))
    $pdf->Cell($w, 0, $product->getMaxsellprice().'     '.MyTMC::encode($product->getMaxbuyprice()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if(isset($line[1]))
    $pdf->Cell($w, 0, $product->getMaxsellprice().'     '.MyTMC::encode($product->getMaxbuyprice()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if(isset($line[2]))
    $pdf->Cell($w, 0, $product->getMaxsellprice().'     '.MyTMC::encode($product->getMaxbuyprice()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  $pdf->ln();


  $pdf->SetFont('dejavusans', '', 2, '', true);
    $pdf->ln();
  $pdf->SetFont('dejavusans', '', 8, '', true);
}



// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

