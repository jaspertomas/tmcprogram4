<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByName('ids[]'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked;
  } 
  return true;
}
function checkAllProductIds()
{
  var boxes = document.getElementsByName('product_ids[]'); 
  for(var index = 0; index < boxes.length; index++) 
  { 
    box = boxes[index]; 
    if (box.type == 'checkbox') 
      box.checked = document.getElementById('product_ids_header_checkbox').checked;
  } 
  return true;
}
/* ]]> */
</script>

<?php echo html_entity_decode($producttype->getBreadcrumbs()); ?>
&nbsp;&nbsp;<?php if(!$producttype->isRoot())echo link_to("(Edit)","producttype/edit?id=".$producttype->getId()) ?>
<br>
<hr>
<h1>Price Generator for Product Type: <?php echo $producttype->getName()."(".$producttype->getId().")" ?></h1>
<?php echo link_to("Go to Inventory Report","producttype/view?id=".$producttype->getId());?>
<br><?php echo link_to("Go to Name Generator / Specs Editor","producttype/viewSpecs?id=".$producttype->getId());?>
<br><?php echo link_to("Back to Product Type List","producttype/index");?>
<hr>Is Updated: <?php echo $producttype->getIsUpdated()?"Yes":"No" ?> (<?php echo link_to("Set to ". ($producttype->getIsUpdated()?"No":"Yes"),"producttype/toggleIsUpdated?id=".$producttype->getId())?>)

<br>
<?php echo link_to("Edit Specifications","producttype/viewSpecs?id=".$producttype->getId());?>
<br><?php echo link_to("Parse Specifications","producttype/parseSpecs?id=".$producttype->getId(), array('confirm' => 'Parse specifications: are you sure?'));?>
<br><?php echo link_to("Create Barcodes","producttype/barcodepdf?id=".$producttype->getId());?>


<br>
<br>
Edit product type to set parameters used by the functions below

<br><?php echo link_to("Autorename Products","producttype/autorename?id=".$producttype->getId(), array('confirm' => 'Rename all products: are you sure?'));?>
<br><?php echo link_to("Autocalc Base Prices","producttype/autocalcbaseprice?id=".$producttype->getId(), array('confirm' => 'Recalculate base prices: are you sure?'));?>
<br><?php echo link_to("Autocalc Max Sell Prices","producttype/autocalcmaxsellprice?id=".$producttype->getId(), array('confirm' => 'Recalculate max selling prices: are you sure?'));?>
<br><?php echo link_to("Autocalc Min Sell Prices","producttype/autocalcminsellprice?id=".$producttype->getId(), array('confirm' => 'Recalculate min selling prices: are you sure?'));?>
<br><?php echo link_to("Autocalc Max Buy Prices","producttype/autocalcmaxbuyprice?id=".$producttype->getId(), array('confirm' => 'Recalculate max vendor prices: are you sure?'));?>
<br><?php echo link_to("Autocalc Min Buy Prices","producttype/autocalcminbuyprice?id=".$producttype->getId(), array('confirm' => 'Recalculate min vendor prices: are you sure?'));?>
<hr>
<?php echo count($products); ?> Items Found
<br><?php echo link_to("Add Product","product/new?producttype_id=".$producttype->getId()) ?>
<?php echo form_tag_for(new ProducttypeForm(),"producttype/productmassoper")?>
Merge to product id / Move to product type id: <input name=input size=10 >
<input type=hidden name=producttype_id value=<?php echo $producttype->getId()?> />
<br>
<input type=submit name=submit value=Merge />
<input type=submit name=submit value=Save />
<input type=submit name=submit value=Rename />
<input type=submit name=submit value=Monitor />
<input type=submit name=submit value=Unmonitor />
<input type=submit name=submit value=Copy />
<input type=submit name=submit value=Move />
<input type=submit name=submit value=Delete />
<br>Rename: Prefix <input name=prefix size=10 > Suffix <input name=suffix size=10 > Replace <input name=replace size=10 > With <input name=with size=10 >



<table border=1>
  <tr>
    <td>Id</td>
    <td><input id="product_ids_header_checkbox" onclick="checkAllProductIds();" type="checkbox"></td>
    <td>Product</td>
    <td>Description</td>
    <td>Base</td>
    <td>Max Sale</td>
    <td>Min Sale</td>
    <td>Max Vendor</td>
    <td>Min Vendor</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Price</td>
    <td>Price</td>
    <td>Price</td>
    <td>Price</td>
    <td>Price</td>
  </tr>
  <?php
//      $maxtotal=0;
    //show products in misc product type, or don't
    //according to configuration setting
    //don't if there are too many products causing page to crash
    if(sfConfig::get('custom_product_type_misc_show_products'))
    {
      $products=$producttype->getProducts();
    }
    else
    {
    	if($producttype->getId()==1)$products=array();
    	else $products=$producttype->getProducts();
    }
  	foreach($products as $product){
  /* 
		//count number of invoicedetails * qty sold since a certain date
		$details = Doctrine_Query::create()
      ->from('Invoicedetail pd, pd.Invoice i')
      ->where('pd.product_id = '.$product->getId())
//      ->andWhere('i.date >= "2010-09-28"')
      ->execute();
      $max=0;
foreach($details as $detail){$max+=$detail->getQty();}

	$maxtotal+=$max;*/
  	
 	?>
  <tr>
    <td><?php echo $product->getId() ?></td>
    <td><input type=checkbox name="product_ids[]" value="<?php echo $product->getId()?>" /></td>
    <td><?php echo link_to($product->getName(),"product/view?id=".$product->getId()) ?></td>
    <td><?php echo $product->getDescription() ?></td>

    <td align=right><?php echo MyDecimal::format($product->getBaseprice()==""?0:$product->getBaseprice()) ?><br><input name="baseprices[<?php echo $product->getId()?>]" size=1 /></td>

    <td align=right><?php echo MyDecimal::format($product->getMaxsellprice()==""?0:$product->getMaxsellprice()) ?><br><input name="maxsellprices[<?php echo $product->getId()?>]" size=1 /></td>
    <td align=right><?php echo MyDecimal::format($product->getMinsellprice()==""?0:$product->getMinsellprice()) ?><br><input name="minsellprices[<?php echo $product->getId()?>]" size=1 /></td>
    <td align=right><?php echo MyDecimal::format($product->getMaxbuyprice()==""?0:$product->getMaxbuyprice()) ?><br><input name="maxbuyprices[<?php echo $product->getId()?>]" size=1 /></td>
    <td align=right><?php echo MyDecimal::format($product->getMinbuyprice()==""?0:$product->getMinbuyprice()) ?><br><input name="minbuyprices[<?php echo $product->getId()?>]" size=1 /></td>

    <td><?php echo link_to("Edit","product/edit?id=".$product->getId()) ?></td>
    <td><?php echo link_to(
  'Delete',
  'product/delete?id='.$product->getId(),
  array('method' => 'delete', 'confirm' => 'Are you sure?')
) ?></td>
  <td><?php echo $product->calcMaxSellPrice()?></td>
  </tr>
  <?php }?>
</table>
<input type=submit name=submit value=Merge />
<input type=submit name=submit value=Save />
<input type=submit name=submit value=Rename />
<input type=submit name=submit value=Monitor />
<input type=submit name=submit value=Unmonitor />
<input type=submit name=submit value=Copy />
<input type=submit name=submit value=Move />
<input type=submit name=submit value=Delete />
</form>

<hr>
<?php echo form_tag("producttype/view?id=".$producttype->getId(),array("method"=>"get"))?>
View <input name=levels size=1 value=<?php echo $levels?>><input type=submit name=submit value=Levels />
</form>

<br>
<?php echo form_tag_for(new ProducttypeForm(),"producttype/massoper")?>
<table>
  <tr>
    <td>Id</td>
    <td><input id="sf_admin_list_batch_checkbox" onclick="checkAll();" type="checkbox"></td>
    <td>Name</td>
    <td>Description</td>
  </tr>

  <?php include_partial("showchildren",array("producttype"=>$producttype,"children"=>$producttype->getChildren(),"levels"=>$levels,"level"=>0)); ?>

</table>

<?php echo link_to("Add Child Product Type","producttype/new?parent_id=".$producttype->getId()) ?>

<br>
<input name=parent_id size=1 >
<input type=submit name=submit value=Move />
<input type=submit name=submit value=Copy />
<input type=submit name=submit value="Copy Schema" />
<input type=submit name=submit value="Set Status OK" />
</form>

<?php //echo $maxtotal?>

<hr>

<h2>Group products into product types</h2>

<?php echo form_tag("producttype/arrangeIntoChildren");?>
<input type=hidden name="id" id="id" value="<?php echo $producttype->getId()?>">
Enter search keywords here
<br><textarea name=keywords id=keywords rows=5 cols=50></textarea>
<br><input type=submit value=Submit>
</form>


