<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
<table>
<?php echo form_tag_for($form, '@product') ?>
<?php echo $form->renderHiddenFields(false) ?>
  
<?php if ($form->hasGlobalErrors()): ?>
  <?php echo $form->renderGlobalErrors() ?>
<?php endif; ?>

<?php $producttype=$product->getProducttype();?>
<tr>
  <td colspan=2>
    <?php include_partial('product/form_actions', array('product' => $product, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </td>
</tr>
<tr>
  <td colspan=10>* Leave name empty to auto generate product name based on product type and specs<br>(Only works if name formula for product type is set.)</td>
</tr>

<tr>
  <td>Name</td>
  <td><?php echo $form['name']; ?></td>
</tr>

<tr>
  <td>Code</td>
  <td><?php echo $form['code']; ?>(Example: J10S)</td>
</tr>
<tr>
  <td>Description</td>
  <td><?php echo $form['description']; ?></td>
</tr>
<tr>
  <td>Brand</td>
  <td><?php echo $form['brand_id']; ?></td>
</tr>
<tr>
  <td>Product Type</td>
  <td><?php echo $form['producttype_id']; ?></td>
</tr>
<tr>
  <td>Product Category</td>
  <td><?php echo $form['productcategory']; ?></td>
</tr>
<tr>
  <td>Quota</td>
  <td><?php echo $form['quota']; ?></td>
</tr>

<tr>
  <td colspan=10><hr></td>
</tr>
<tr>
  <td colspan=10><b>SPECS</b></td>
</tr>
<?php if($producttype->getSpeccount()>=1){?>    
<tr>
  <td><?php echo $producttype->getSpec1(); ?></td>
  <td><?php echo $form['spec1']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec1]" value="<?php echo $form->getObject()->getSpec1()?>" id="product_spec1" type="hidden">
<?php } ?>

<?php if($producttype->getSpeccount()>=2){?>    
<tr>
  <td><?php echo $producttype->getSpec2(); ?></td>
  <td><?php echo $form['spec2']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec2]" value="<?php echo $form->getObject()->getSpec2()?>" id="product_spec2" type="hidden">
<?php } ?>

<?php if($producttype->getSpeccount()>=3){?>    
<tr>
  <td><?php echo $producttype->getSpec3(); ?></td>
  <td><?php echo $form['spec3']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec3]" value="<?php echo $form->getObject()->getSpec3()?>" id="product_spec3" type="hidden">
<?php } ?>

<?php if($producttype->getSpeccount()>=4){?>    
<tr>
  <td><?php echo $producttype->getSpec4(); ?></td>
  <td><?php echo $form['spec4']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec4]" value="<?php echo $form->getObject()->getSpec4()?>" id="product_spec4" type="hidden">
<?php } ?>

<?php if($producttype->getSpeccount()>=5){?>    
<tr>
  <td><?php echo $producttype->getSpec5(); ?></td>
  <td><?php echo $form['spec5']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec5]" value="<?php echo $form->getObject()->getSpec5()?>" id="product_spec5" type="hidden">
<?php } ?>

<?php if($producttype->getSpeccount()>=6){?>    
<tr>
  <td><?php echo $producttype->getSpec6(); ?></td>
  <td><?php echo $form['spec6']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec6]" value="<?php echo $form->getObject()->getSpec6()?>" id="product_spec6" type="hidden">
<?php } ?>

<?php if($producttype->getSpeccount()>=7){?>    
<tr>
  <td><?php echo $producttype->getSpec7(); ?></td>
  <td><?php echo $form['spec7']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec7]" value="<?php echo $form->getObject()->getSpec7()?>" id="product_spec7" type="hidden">
<?php } ?>

<?php if($producttype->getSpeccount()>=8){?>    
<tr>
  <td><?php echo $producttype->getSpec8(); ?></td>
  <td><?php echo $form['spec8']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec8]" value="<?php echo $form->getObject()->getSpec8()?>" id="product_spec8" type="hidden">
<?php } ?>

<?php if($producttype->getSpeccount()>=9){?>    
<tr>
  <td><?php echo $producttype->getSpec9(); ?></td>
  <td><?php echo $form['spec9']; ?></td>
</tr>
<?php }else{ ?>
  <input name="product[spec9]" value="<?php echo $form->getObject()->getSpec9()?>" id="product_spec9" type="hidden">
<?php } ?>

<tr>
  <td colspan=10><hr></td>
</tr>

<?php if($sf_user->hasCredential(array('admin'),false) or sfConfig::get('custom_non_admins_can_edit_product_prices')){ ?>
<tr>
  <td>Base Price</td>
  <td><?php echo $form['baseprice']; ?>(Brochure price)</td>
</tr>
<tr>
  <td>Max Sell Price</td>
  <td><?php echo $form['maxsellprice']; ?>(Suggested Retail Price)</td>
</tr>
<tr>
  <td>Min Sell Price</td>
  <td><?php echo $form['minsellprice']; ?>(Lowest discount selling price)</td>
</tr>
<tr>
  <td>Max Buy Price</td>
  <td><?php echo $form['maxbuyprice']; ?>(Maximum Capital)</td>
</tr>
<tr>
  <td>Min Buy Price</td>
  <td><?php echo $form['minbuyprice']; ?>(Minimum Capital)</td>
</tr>
<tr>
  <td>Auto Calc Sell Price</td>
  <td><?php echo $form['autocalcsellprice']; ?>(Update selling price when entered in Invoice)</td>
</tr>
<tr>
  <td>Auto Calc Buy Price</td>
  <td><?php echo $form['autocalcbuyprice']; ?>(Update capital price when entered in PO)</td>
</tr>

<?php }else{ ?>

  <input value="<?php echo $form->getObject()->getBaseprice() ?>" name="product[baseprice]" id="product_baseprice" type="hidden">
  <input value="<?php echo $form->getObject()->getMaxsellprice() ?>" name="product[maxsellprice]" id="product_maxsellprice" type="hidden">
  <input value="<?php echo $form->getObject()->getMinsellprice() ?>" name="product[minsellprice]" id="product_minsellprice" type="hidden">
  <input value="<?php echo $form->getObject()->getMaxbuyprice() ?>" name="product[maxbuyprice]" id="product_maxbuyprice" type="hidden">
  <input value="<?php echo $form->getObject()->getMinbuyprice() ?>" name="product[minbuyprice]" id="product_minbuyprice" type="hidden">
  <input value="<?php echo $form->getObject()->getAutocalcsellprice() ?>" name="product[autocalcsellprice]" id="product_autocalcsellprice" type="hidden">
  <input value="<?php echo $form->getObject()->getAutocalcbuyprice() ?>" name="product[autocalcbuyprice]" id="product_autocalcbuyprice" type="hidden">

<?php } ?>

<tr>
  <td>Competitor Price</td>
  <td><?php echo $form['competitor_price']; ?>(Competitor's Selling price)</td>
</tr>
<tr>
  <td>Is a Service</td>
  <td><?php echo $form['is_service']; ?>(This is a service, not a product (1 for yes, 0 for no))</td>
</tr>
<tr>
  <td>Is Hidden</td>
  <td><?php echo $form['is_hidden']; ?>(Hide obsolete product (1 for yes, 0 for no))</td>
</tr>
<tr>
  <td>Is Monitored</td>
  <td><?php echo $form['monitored']; ?>(1 for yes, 0 for no)</td>
</tr>

<?php if($sf_user->hasCredential(array('admin'),false)){ ?>
<tr>
  <td>Allow Zero Price</td>
  <td><?php echo $form['is_allow_zeroprice']; ?></td>
</tr>
<?php }else{ ?>
<input value="<?php echo $form->getObject()->getIsAllowZeroprice() ?>" name="product[is_allow_zeroprice]" id="product_is_allow_zeroprice" type="hidden">
<?php } ?>

<tr>
  <td>Notes</td>
  <td><?php echo $form['notes']; ?></td>
</tr>

<tr>
  <td colspan=2>
    <?php include_partial('product/form_actions', array('product' => $product, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </td>
</tr>

</form>
</table></div>



