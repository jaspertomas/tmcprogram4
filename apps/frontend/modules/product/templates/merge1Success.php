<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Merge Products</h1>
<br>
Remember to backup your database first!
<br>
<?php echo form_tag("product/merge2");?>
Product ID to merge: <input name=merger_product_id id=merger_product_id>
<br>Product ID to merge into <input name=mergee_product_id id=mergee_product_id>
<br><input type=submit value=Test>
</form>
