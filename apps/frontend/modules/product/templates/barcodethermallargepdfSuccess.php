<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$width=60;
$height=40;
$pageLayout = array($width, $height);
$pdf = new TCPDF("P", PDF_UNIT, $pageLayout, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Barcode');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(2, 1, 2,1);
$pdf->SetAutoPageBreak(TRUE, 0);

//set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();


//barcode height
$h=25;
//column width
$w=50;
// vertical spacing
$v=50;
$style = array(
	'border' => false,
	'padding' => false,
	// 'fgcolor' => array(128,0,0),
	// 'bgcolor' => false
);
$filler=100-($product->getId()%100);
$pdf->write2DBarcode('pro;'.$product->getId().";".$filler.";".sfConfig::get('custom_server_code'), 'QRCODE,Q', 
6, 2, $h, $h);


$pdf->SetFont('dejavusans', '', 10, '', true);

$pdf->write(0,'','',false,'',true,0,false,false,0,0);
$pdf->write(0,'','',false,'',true,0,false,false,0,0);
$pdf->write(0,'','',false,'',true,0,false,false,0,0);
$pdf->write(0,'','',false,'',true,0,false,false,0,0);
$pdf->write(0,'','',false,'',true,0,false,false,0,0);
$pdf->write(0,'','',false,'',true,0,false,false,0,0);
$pdf->write(0,$product->getMaxsellprice()."   ".MyTMC::encode($product->getMaxbuyprice()),'',false,'',true,0,false,false,0,0);

$name=$product->getName();
$name=str_replace("&quot;","\"",$name);
$pdf->write(0,$name,'',false,'',true,0,false,false,0,0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('barcodes.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

