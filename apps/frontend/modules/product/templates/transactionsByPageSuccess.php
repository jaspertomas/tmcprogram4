<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<?php echo link_to("<h2>See All Transactions</h2>","product/transactionsByPage?id=".$product->getId()."&page=-1");?>

<h1>Product Transactions by Page: <?php echo $product?></h1>
<table>
  <tr>
    <td><?php echo link_to("Edit Product","product/edit?id=".$product->getId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("Create Barcodes","product/barcode?id=".$product->getId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("View Inventory","product/inventory?id=".$product->getId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("View Price List","product/pricelist?id=".$product->getId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("View Files","product/files?id=".$product->getId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("Report Defective Product","product/reportDefect?id=".$product->getId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("View Transactions By Date","product/transactions?id=".$product->getId()) ?></td>
  </tr>
  <tr>
    <td><?php echo link_to("View Unpaid Transactions","product/transactionsUnpaid?id=".$product->getId());?></td>
  </tr>
  <tr>
    <!--td>Is Updated: <?php //echo $product->getIsUpdated()?"Yes":"No" ?> (<?php //echo link_to("Set to ". ($product->getIsUpdated()?"No":"Yes"),"product/toggleIsUpdated?id=".$product->getId())?>)</td-->
  </tr>
            
</table>

<?php
$first=true;
$firstdate=MyDate::today();
$totalqty=0;
?>
<h2>Sales</h2>

<h4>
Total Sale: <?php echo MyDecimal::format($total)?>
<br>
Total Paid Sale: <?php echo MyDecimal::format($totalpaid)?>
<br>
Total Unpaid Sale: <?php echo MyDecimal::format($totalunpaid)?>
</h4>

<?php if($page!=-1){ ?>
<table>
  <tr>
    <td>Page:</td>
    <td><?php echo link_to(" First ","product/transactionsByPage?id=".$product->getId()."&page=1");?></td>
    <td><?php echo link_to(" Previous ","product/transactionsByPage?id=".$product->getId()."&page=".($page-1));?></td>
    <?php for($i=0;$i<$invoicepages;$i++){?>
    <td>
      <?php 
        if($page==$i)echo $i+1;
        else echo link_to($i+1,"product/transactionsByPage?id=".$product->getId()."&page=".$i);
      ?>
    </td>
    <?php } ?>
    <td><?php echo link_to(" Next ","product/transactionsByPage?id=".$product->getId()."&page=".($page+1));?></td>
    <td><?php echo link_to(" Last ","product/transactionsByPage?id=".$product->getId()."&page=".($invoicepages-1));?></td>
  </tr>
<table>
<?php } ?>

<table border=1>
  <tr>
    <td>Product</td>
    <td>Date</td>
    <td>Description</td>
    <td>Qty</td>
    <td>Unit</td>
    <td>Discount</td>
    <td>Discount</td>
    <td>Total</td>
    <td>Customer</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Price</td>
    <td>Rate</td>
    <td>Amount</td>
    <td></td>
    <td></td>
  </tr>
  <?php foreach($invoicedetails as $detail){$invoice=$detail->getInvoice();?>
  <tr>
    <td><?php echo link_to($invoice,"invoice/view?id=".$detail->getInvoiceId()) ?></td>
    <td>
      <?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?>
      <?php if($first){$first=false;$firstdate=$invoice->getDate();}?>
    </td>
    <td><?php echo $detail->getDescription() ?></td>
    <td align=right>
      <?php echo $detail->getQty() ?>
      <?php $totalqty+=$detail->getQty(); ?>
    </td>
    <td align=right><?php echo $detail->getPrice() ?></td>
    <td align=right><?php echo $detail->getDiscrate() ?></td>
    <td align=right><?php echo $detail->getDiscamt() ?></td>
    <td align=right align=right><?php echo $detail->getTotal() ?></td>
    <td><?php echo $invoice->getCustomer() ?></td>
    <td>
      <font <?php echo $invoice->getFancyStatusColor();?>>
        <?php echo $invoice->getFancyStatusString();?>
      </font>
    </td>
    <td><?php //$quote=$detail->getSimilarQuote();if($quote)echo link_to("View Quote","quote/edit?id=".$quote->getId()); ?></td>
  </tr>
  <?php }?>
</table>

<h2>Purchases</h2>
<?php if($page!=-1){ ?>
<table>
  <tr>
    <td>Page:</td>
    <td><?php echo link_to(" First ","product/transactionsByPage?id=".$product->getId()."&page=1");?></td>
    <td><?php echo link_to(" Previous ","product/transactionsByPage?id=".$product->getId()."&page=".($page-1));?></td>
    <?php for($i=0;$i<$purchasepages;$i++){?>
    <td>
      <?php 
        if($page==$i)echo $i+1;
        else echo link_to($i+1,"product/transactionsByPage?id=".$product->getId()."&page=".$i);
      ?>
    </td>
    <?php } ?>
    <td><?php echo link_to(" Next ","product/transactionsByPage?id=".$product->getId()."&page=".($page+1));?></td>
    <td><?php echo link_to(" Last ","product/transactionsByPage?id=".$product->getId()."&page=".($invoicepages-1));?></td>
  </tr>
<table>
<?php } ?>

<table border=1>
  <tr>
    <td>Product</td>
    <td>Vendor</td>
    <td>Date</td>
    <td>Description</td>
    <td>Qty</td>
    <td>Unit</td>
    <td>Discount</td>
    <td>Discount</td>
    <td>Total</td>
    <td>Vendor</td>
  </tr>
  <tr>
    <td></td>
    <td>Invoice</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Price</td>
    <td>Rate</td>
    <td>Amount</td>
    <td></td>
    <td></td>
  </tr>
  <?php foreach($purchasedetails as $detail){$purchase=$detail->getPurchase()?>
  <tr>
    <td><?php echo link_to($purchase,"purchase/view?id=".$detail->getPurchaseId()) ?></td>
    <td><?php echo $purchase->getVendorInvoice() ?></td>
    <td><?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate() ?></td>
    <td><?php echo $detail->getDescription() ?></td>
    <td align=right><?php echo $detail->getQty() ?></td>
    <td align=right><?php echo $detail->getPrice() ?></td>
    <td align=right><?php echo $detail->getDiscrate() ?></td>
    <td align=right><?php echo $detail->getDiscamt() ?></td>
    <td align=right><?php echo $detail->getTotal() ?></td>
    <td><?php echo $purchase->getVendor() ?></td>
    <td>
      <font <?php if($purchase->getStatus()=="Cancelled")echo "color=red";?>>
      <?php echo $purchase->getStatus() ?>
      </font>
    </td>
    <td><?php //$quote=$detail->getSimilarQuote();echo link_to("View Quote","quote/edit?id=".$quote->getId()); ?></td>
  </tr>
  <?php }?>
</table>
