<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "LETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Tradewind Mdsg Corp Daily Sales Report');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(7.5, 10, 5,5);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

$style = array(
	'position' => '',
	'align' => 'C',
	'stretch' => false,
	'fitwidth' => true,
	'cellfitalign' => '',
	'border' => false,
	'padding' => '0',
	'fgcolor' => array(0,0,0),
	'bgcolor' => false, //array(255,255,255),
	'text' => true,
	'font' => 'helvetica',
	'fontsize' => 8,
	'stretchtext' => 4
);

//map of cells to be printed; initially all turned off 
$pos=array(
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      array(-1,-1,-1,-1),
      );

//turn them on if they are between start and end
foreach(range(0,29) as $count)
{
  if($count>=$start and $count<=$end )
  {
    $row=$count/4;
    $col=$count%4;
    $pos[$row][$col]=1;
  }
}

//barcode height
$h=30;
//column width
$w=50;
// vertical spacing
$v=50;
$style = array(
	'border' => false,
	'padding' => auto,
	// 'fgcolor' => array(128,0,0),
	// 'bgcolor' => false
);

$pdf->ln();
$pdf->ln();

// foreach($pos as $line)
$filler=100-($product->getId()%100);
for($i=0;$i<count($pos);$i++)
{
  $line=$pos[$i];

  if($line[0]==1)
    $pdf->write2DBarcode('pro;'.$product->getId().";".$filler.";".sfConfig::get('custom_server_code'), 'QRCODE,Q', ($w*0)+6, ($i*$v)+5, $h, $h, $style, 'N');
  if($line[1]==1)
    $pdf->write2DBarcode('pro;'.$product->getId().";".$filler.";".sfConfig::get('custom_server_code'), 'QRCODE,Q', ($w*1)+6, ($i*$v)+5, $h, $h, $style, 'N');
  if($line[2]==1)
    $pdf->write2DBarcode('pro;'.$product->getId().";".$filler.";".sfConfig::get('custom_server_code'), 'QRCODE,Q', ($w*2)+6, ($i*$v)+5, $h, $h, $style, 'N');
  if($line[3]==1)
    $pdf->write2DBarcode('pro;'.$product->getId().";".$filler.";".sfConfig::get('custom_server_code'), 'QRCODE,Q', ($w*3)+6, ($i*$v)+5, $h, $h, $style, 'N');

  if($line[0]==1)
    $pdf->Cell($w, 0, $product->getMaxsellprice().'     '.MyTMC::encode($product->getMaxbuyprice()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if($line[1]==1)
    $pdf->Cell($w, 0, $product->getMaxsellprice().'     '.MyTMC::encode($product->getMaxbuyprice()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if($line[2]==1)
    $pdf->Cell($w, 0, $product->getMaxsellprice().'     '.MyTMC::encode($product->getMaxbuyprice()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if($line[3]==1)
    $pdf->Cell($w, 0, $product->getMaxsellprice().'     '.MyTMC::encode($product->getMaxbuyprice()), 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);


  $name=$product->getName();
  $name=str_replace("&quot;","\"",$name);

  if($line[0]==1)
    $pdf->MultiCell($w, 0, $name, 0, 'L', false, 1, ($w*0)+7, (($v)*$i)+40);
    // $pdf->Cell($w, 0, $name, 0, 0);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if($line[1]==1)
    $pdf->MultiCell($w, 0, $name, 0, 'L', false, 1, ($w*1)+7, (($v)*$i)+40);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if($line[2]==1)
    $pdf->MultiCell($w, 0, $name, 0, 'L', false, 1, ($w*2)+7, (($v)*$i)+40);
  else
    $pdf->Cell($w, 0, '', 0, 0);

  if($line[3]==1)
    $pdf->MultiCell($w, 0, $name, 0, 'L', false, 1, ($w*3)+7, (($v)*$i)+40);
  else
    $pdf->Cell($w, 0, '', 0, 0);



  $pdf->SetFont('dejavusans', '', 8, '', true);
    $pdf->ln();
    $pdf->ln();
  $pdf->SetFont('dejavusans', '', 8, '', true);
}



// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('barcodes.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

