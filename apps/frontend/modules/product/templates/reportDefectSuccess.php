<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Report Defective Product: <?php echo link_to($product,"product/view?id=".$stock->getProductId())." in warehouse ".$stock->getWarehouse()?></h1>

<table>
  <tr>
    <td><?php echo link_to("Back","product/view?id=".$product->getId()) ?></td>
  </tr>
</table>

<table>
<tr>
<td>Choose a different warehouse:</td>
<td><?php echo form_tag("product/reportDefect");?>
<input type=hidden name=id value=<?php echo $product->getId()?>>
<select name=warehouse_id>
<?php foreach($warehouses as $w){?>
  <option <?php if($w->getId()==$warehouse_id)echo "selected=selected"?> value=<?php echo $w->getId()?>><?php echo $w->getName()?></option>
<?php }?>
</select>
</td>
<td><input type=submit value=Submit></td>
</tr>
</table>

<br>


<?php echo form_tag("product/processReportDefect"); ?>
<input type=hidden name=stockentry[stock_id] value=<?php echo $stock->getId()?>  >
    <?php echo $form->renderHiddenFields(false) ?>
<table>
	<tr>
		<td>Date</td>
		<td>Qty</td>
		<td>Description</td>
	</tr>
	<tr>
		<td><?php echo $form["date"]; ?></td>
		<td><input name="stockentry[qty]" id="stockentry_qty" type="text" value=1 size=10></td>
		<td><?php echo $form["description"]; ?></td>
		<td><input type=submit name=submit value=Save  ></td>
	</tr>
</table>
</form>


<br>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Stock In</td>
    <td>Stock Out</td>
    <td>Balance</td>
    <td>Ref</td>
    <td>Type</td>
    <td>Description</td>
  </tr>
  <?php foreach($stock->getStockentriesDesc() as $detail){?>
  <tr>
    <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
    <td align=right><?php if($detail->getQty()>0)echo MyDecimal::format($detail->getQty()) ?></td>
    <td align=right><?php if($detail->getQty()<0)echo MyDecimal::format($detail->getQty()*-1) ?></td>
    <td align=right><?php echo $detail->getBalance() ?></td>
    <td><?php 
      
      if($detail->getRefClass()=="Invoicedetail"){$ref=$detail->getRef();if($ref)echo link_to($ref->getInvoice(),"invoice/view?id=".$ref->getInvoiceId());else echo "Invoice not found. Please do a stock recalculation.";}
      else if($detail->getRefClass()=="Purchasedetail"){$ref=$detail->getRef();echo link_to($ref->getPurchase(),"purchase/view?id=".$ref->getPurchaseId());}
      else if($detail->getRefClass())echo link_to($detail->getRef(),strtolower($detail->getRefClass())."/view?id=".$detail->getRefId());
      ?></td>
    <td><?php echo $detail->getType() ?></td>
    <td><?php echo $detail->getDescription() ?></td>
    <td><?php if($detail->getType())echo link_to('Delete','stockentry/delete?id='.$detail->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?></td>
  </tr>
  <?php }?>
</table>

