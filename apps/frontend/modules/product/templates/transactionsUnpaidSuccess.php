<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<?php echo form_tag("product/transactions");?>
<input type=hidden id=id name="id" value=<?php echo $product->getId()?>>

<?php use_helper('I18N', 'Date') ?>


<h1>Unpaid Product Transactions: <?php echo $product?></h1>
<h4>Total Unpaid Sale: <?php echo MyDecimal::format($total)?></h4>
<h4><?php echo link_to("See All Transactions","product/transactions?id=".$product->getId()."&see_all=true");?></h4>


<?php
$first=true;
$firstdate=MyDate::today();
$totalqty=0;
?>

<h2>Sales</h2>
<table border=1>
  <tr>
    <td>Product</td>
    <td>Date</td>
    <td>Description</td>
    <td>Qty</td>
    <td>Unit</td>
    <td>Discount</td>
    <td>Discount</td>
    <td>Total</td>
    <td>Customer</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Price</td>
    <td>Rate</td>
    <td>Amount</td>
    <td></td>
    <td></td>
  </tr>
  <?php foreach($invoicedetails as $detail){$invoice=$detail->getInvoice();?>
  <tr>
    <td><?php echo link_to($invoice,"invoice/view?id=".$detail->getInvoiceId()) ?></td>
    <td>
      <?php echo MyDateTime::frommysql($invoice->getDate())->toshortdate() ?>
      <?php if($first){$first=false;$firstdate=$invoice->getDate();}?>
    </td>
    <td><?php echo $detail->getDescription() ?></td>
    <td align=right>
      <?php echo $detail->getQty() ?>
      <?php $totalqty+=$detail->getQty(); ?>
    </td>
    <td align=right><?php echo $detail->getPrice() ?></td>
    <td align=right><?php echo $detail->getDiscrate() ?></td>
    <td align=right><?php echo $detail->getDiscamt() ?></td>
    <td align=right align=right><?php echo $detail->getTotal() ?></td>
    <td><?php echo $invoice->getCustomer() ?></td>
    <td>
      <font <?php echo $invoice->getFancyStatusColor();?>>
        <?php echo $invoice->getFancyStatusString();?>
      </font>
    </td>
    <td><?php //$quote=$detail->getSimilarQuote();if($quote)echo link_to("View Quote","quote/edit?id=".$quote->getId()); ?></td>
  </tr>
  <?php }?>
</table>

<h2>Purchases</h2>
<table border=1>
  <tr>
    <td>Product</td>
    <td>Vendor</td>
    <td>Date</td>
    <td>Description</td>
    <td>Qty</td>
    <td>Unit</td>
    <td>Discount</td>
    <td>Discount</td>
    <td>Total</td>
    <td>Vendor</td>
  </tr>
  <tr>
    <td></td>
    <td>Invoice</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Price</td>
    <td>Rate</td>
    <td>Amount</td>
    <td></td>
    <td></td>
  </tr>
  <?php foreach($purchasedetails as $detail){$purchase=$detail->getPurchase()?>
  <tr>
    <td><?php echo link_to($purchase,"purchase/view?id=".$detail->getPurchaseId()) ?></td>
    <td><?php echo $purchase->getVendorInvoice() ?></td>
    <td><?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate() ?></td>
    <td><?php echo $detail->getDescription() ?></td>
    <td align=right><?php echo $detail->getQty() ?></td>
    <td align=right><?php echo $detail->getPrice() ?></td>
    <td align=right><?php echo $detail->getDiscrate() ?></td>
    <td align=right><?php echo $detail->getDiscamt() ?></td>
    <td align=right><?php echo $detail->getTotal() ?></td>
    <td><?php echo $purchase->getVendor() ?></td>
    <td>
      <font <?php if($purchase->getStatus()=="Cancelled")echo "color=red";?>>
      <?php echo $purchase->getStatus() ?>
      </font>
    </td>
    <td><?php //$quote=$detail->getSimilarQuote();echo link_to("View Quote","quote/edit?id=".$quote->getId()); ?></td>
  </tr>
  <?php }?>
</table>

<?php

$firstdate = new DateTime($firstdate);
$lastdate = new DateTime(MyDate::today());
$interval = $lastdate->diff($firstdate);

// While %d will only output the number of days not already covered by the
// month.
//echo $interval->format('%y years, %m month, %d days');
$y=$interval->format('%y');
$m=$interval->format('%m');
$totalmonths=$y*12+$m;
if($totalmonths==0)
  $yearlyavgqty=0;
else
  $yearlyavgqty=intval(12*$totalqty/$totalmonths);

//fetch stock
$stock=Doctrine_Query::create()
    ->from('Stock s')
  	->where('s.product_id = '.$product->getId())
  	->andWhere('s.warehouse_id = '.SettingsTable::fetch('default_warehouse_id'))
  	->fetchOne();

//calc reorder qty
if($stock)$currqty=$stock->getCurrentQty();
else $currqty=0;

$reorderqty=($yearlyavgqty/2)-$currqty;
if($reorderqty<0)$reorderqty=0;
?>

<br>
Average units sold per year: <?php echo $yearlyavgqty ?> 
<br>Current Qty: <?php echo $currqty?> 
<br>Recommended reorder level: <?php echo $reorderqty ?>
<?php
$product->setQuota($yearlyavgqty/2);
$product->save();
?>
