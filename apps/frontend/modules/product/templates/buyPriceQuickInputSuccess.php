<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Sell Price Quick Input</h2>
For quick updating of vendor prices

<!--
<font color=red>
<?php //foreach($errors as $error)echo $error."<br>"; ?>
</font>
<font color=green>
<?php //foreach($messages as $message)echo $message."<br>"; ?>
</font-->

<?php echo form_tag_for(new ProductForm(),"product/buyPriceQuickInput")?>

Paste from spreadsheet:
<br>
<input name=submit type=submit value="Save">

<br><textarea rows=15 cols=80 name=inputstring ><?php echo $inputstring?></textarea>
</form>



