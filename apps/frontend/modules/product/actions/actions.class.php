<?php

require_once dirname(__FILE__).'/../lib/productGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/productGeneratorHelper.class.php';

/**
 * product actions.
 *
 * @package    sf_sandbox
 * @subpackage product
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class productActions extends autoProductActions
{
  //these are prices from the purchase->vendor
  public function executeBuyPriceFromVendor(sfWebRequest $request)
  {
    $product=Fetcher::fetchOne("Product",array('id'=>$request->getParameter('id')));
    $this->quoteprices=$product->getBuyPriceFromVendor($request->getParameter('vendor_id'));
    $this->setLayout(false);
  }
  public function executeNew(sfWebRequest $request)
  {
    $this->product = new Product();
    
    if($request->hasParameter('producttype_id'))
      $this->product->setProducttypeId($request->getParameter('producttype_id'));
    
    $this->form = $this->configuration->getForm($this->product);
  }
  public function executeView(sfWebRequest $request)
  {
    $this->product = $this->getRoute()->getObject();
    $this->stock=$this->product->getStock(SettingsTable::fetch('default_warehouse_id'));
    $this->redirect("stock/view?id=".$this->stock->getId());
  }
  public function executeSoldTo(sfWebRequest $request)
  {
    $this->product = $this->getRoute()->getObject();
    $this->stock=$this->product->getStock(SettingsTable::fetch('default_warehouse_id'));
    $this->redirect("stock/soldTo?id=".$this->stock->getId());
  }
  public function executeTransactions(sfWebRequest $request)
  {
    $product_id=$request->getParameter("id");
    $this->product = Fetcher::fetchOne("Product",array("id"=>$product_id));
    $this->see_all=$request->getParameter("see_all");

    if($this->see_all)
    {
      $today=MyDateTime::today();
      $startofthismonth=$today->getstartofmonth();
      $endofthismonth=$startofthismonth->getendofmonth();
      $this->startdate=$startofthismonth;
      $this->enddate=$endofthismonth;

      $invoice=new Invoice();
      $invoice->setDate($this->startdate->tomysql());
      $this->fromform=new InvoiceForm($invoice);
  
      $purchase=new Purchase();
      $purchase->setDate($this->enddate->tomysql());
      $this->toform=new PurchaseForm($purchase);
  
      $this->startdate=$invoice->getDate();
      $this->enddate=$purchase->getDate();

      $this->purchasedetails=PurchasedetailTable::fetchByProduct($this->product);
      $this->invoicedetails=InvoicedetailTable::fetchByProduct($this->product);
    }
    else
    {
      $requestparams=$request->getParameter("invoice");

      $day=$requestparams["date"]["day"];
      $month=$requestparams["date"]["month"];
      $year=$requestparams["date"]["year"];
  
      $invoice=new Invoice();
      if($request->hasParameter("startdate"))
        $invoice->setDate($request->getParameter("startdate"));
      elseif(!$day or !$month or !$year)
        $invoice->setDate(MyDateTime::today()->getstartofmonth()->tomysql());
      else
        $invoice->setDate($year."-".$month."-".$day);
  
      $requestparams=$request->getParameter("purchase");
      $day=$requestparams["date"]["day"];
      $month=$requestparams["date"]["month"];
      $year=$requestparams["date"]["year"];
      $purchase=new Purchase();
      if($request->hasParameter("enddate"))
        $purchase->setDate($request->getParameter("enddate"));
      elseif(!$day or !$month or !$year)
        $purchase->setDate(MyDateTime::today()->getendofmonth()->tomysql());
      else
        $purchase->setDate($year."-".$month."-".$day);
  
      $this->fromform=new InvoiceForm($invoice);
      $this->toform=new PurchaseForm($purchase);
  
      $this->startdate=$invoice->getDate();
      $this->enddate=$purchase->getDate();
  
      $this->purchasedetails=PurchasedetailTable::fetchByProductAndDateRange($this->product,$this->startdate,$this->enddate);
      $this->invoicedetails=InvoicedetailTable::fetchByProductAndDateRange($this->product,$this->startdate,$this->enddate);
    }
    // $this->form = $this->configuration->getForm($this->product);

    $this->total=0;
    $this->totalpaid=0;
    $this->totalunpaid=0;
    foreach($this->invoicedetails as $detail)
    {
      $status=$detail->getInvoice()->getStatus();
      if($status=="Cancelled")continue;
      $this->total+=$detail->getTotal();
      switch($status)
      {
        case "Pending":
          $this->totalunpaid+=$detail->getTotal();
          break;
        case "Paid":
          $this->totalpaid+=$detail->getTotal();
          break;
      }
    }

  }


  public function executeTransactionsUnpaid(sfWebRequest $request)
  {
    $product_id=$request->getParameter("id");
    $this->product = Fetcher::fetchOne("Product",array("id"=>$product_id));
    $this->invoicedetails=InvoicedetailTable::fetchByProductUnpaid($this->product);
    $this->total=0;
    foreach($this->invoicedetails as $detail)
    {
      $this->total+=$detail->getTotal();
    }
  }

  public function executeTransactionsByPage(sfWebRequest $request)
  {
    $product_id=$request->getParameter("id");
    $this->product = Fetcher::fetchOne("Product",array("id"=>$product_id));
    $itemsperpage=10;

    $page=$request->getParameter("page");
    $this->page=$page;
    if($page==-1)
    {
      $this->purchasedetails=PurchasedetailTable::fetchByProduct($this->product);
      $this->invoicedetails=InvoicedetailTable::fetchByProduct($this->product);
    }
    else{
      if($page==null)$page=0;
      $this->purchasedetailcount=PurchasedetailTable::countByProduct($this->product);
      $this->purchasedetails=PurchasedetailTable::fetchByProductAndPage($this->product,$page,$itemsperpage);
      $this->purchasepages=ceil($this->purchasedetailcount/$itemsperpage);
      $this->invoicedetailcount=InvoicedetailTable::countByProduct($this->product);
      $this->invoicedetails=InvoicedetailTable::fetchByProductAndPage($this->product,$page,$itemsperpage);
      $this->invoicepages=ceil($this->invoicedetailcount/$itemsperpage);
    }
    $this->total=0;
    $this->totalpaid=0;
    $this->totalunpaid=0;
    foreach($this->invoicedetails as $detail)
    {
      $status=$detail->getInvoice()->getStatus();
      if($status=="Cancelled")continue;
      $this->total+=$detail->getTotal();
      switch($status)
      {
        case "Pending":
          $this->totalunpaid+=$detail->getTotal();
          break;
        case "Paid":
          $this->totalpaid+=$detail->getTotal();
          break;
      }
    }
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $isnew=$form->getObject()->isNew();
    $requestparams=$request->getParameter($form->getName());

    //remove periods and "&" from names
    $requestparams["name"]=str_replace("."," ",$requestparams["name"]);
    $requestparams["name"]=addslashes($requestparams["name"]);

    $product=$form->getObject();
    $product->setProducttypeId($requestparams['producttype_id']);
    $product->setSpec1($requestparams['spec1']);
    $product->setSpec2($requestparams['spec2']);
    $product->setSpec3($requestparams['spec3']);
    $product->setSpec4($requestparams['spec4']);
    $product->setSpec5($requestparams['spec5']);
    $product->setSpec6($requestparams['spec6']);
    $product->setSpec7($requestparams['spec7']);
    $product->setSpec8($requestparams['spec8']);
    $product->setSpec9($requestparams['spec9']);
    $producttype=$product->getProducttype();

    if($isnew)
    {
      $duplicate=Fetcher::fetchOne("Product",array("name"=>"\"".$requestparams["name"]."\""));
      if($duplicate)
      {
        return $this->redirect('product/duplicateError?id='.$duplicate->getId());
      }
    }

    //if no name, calc name
    if($requestparams['name']=="" and $producttype)
    {
      $requestparams["name"]=$product->calcName();
    }
    else if($isnew)
    {
      //if this is not edit, and product with same name exists, error
      $duplicate=Fetcher::fetchOne("Product",array("name"=>"\"".$requestparams["name"]."\""));
      if($duplicate)
      {
        return $this->redirect('product/duplicateError?id='.$duplicate->getId());
      }
    }
    
    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $product = $form->save();
        $saverequired=false;
        //turn on autocalcsellprice if producttype is misc
        /*
        if($product->getAutocalcsellprice()==0 and $product->getProducttypeId()==1)
        {
          $product->setAutocalcsellprice(1);
          $saverequired=true;
        }
        else
        */
        if($product->getAutocalcsellprice()==1 and $product->getProducttypeId()!=1)
        {
          $product->setAutocalcsellprice(0);
          $saverequired=true;
        }
        if($product->getDescription()=="")
        {
          $product->setDescription($product->calcDescription());
          $saverequired=true;
        }
        if($product->getBaseprice()==0)
        {
          $product->setBaseprice(floatval($product->calcBasePrice()));
          $saverequired=true;
        }
        if($product->getMinbuyprice()==0)
        {
          $product->setMinbuyprice(floatval($product->calcMinBuyPrice()));
          $saverequired=true;
        }
        if($product->getMaxbuyprice()==0)
        {
          $product->setMaxbuyprice(floatval($product->calcMaxBuyPrice()));
          $saverequired=true;
        }
        if($product->getMinsellprice()==0)
        {
          $product->setMinsellprice(floatval($product->calcMinSellPrice()));
          $saverequired=true;
        }
        if($product->getMaxsellprice()==0)
        {
          $product->setMaxsellprice(floatval($product->calcMaxSellPrice()));
          $saverequired=true;
        }
        if($saverequired)
          $product->save();
        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $product)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@product_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);
        $this->redirect(array('sf_route' => 'product_edit', 'sf_subject' => $product));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  
  public function executeSearch(sfWebRequest $request)
  {
    $this->searchstring = $request->getParameter("searchstring");
    $requestparams=$request->getParameter("invoicedetail");
    $this->product_id = $requestparams["product_id"];

    $this->warehouses=WarehouseTable::fetchAll();

    $this->products=array();
    
    if($this->searchstring!="")
    {
     $this->products = Doctrine_Query::create()
      ->from('Product p, p.Invoicedetail id, id.Invoice inv, inv.Customer c, p.Purchasedetail pd, pd.Purchase po, po.Vendor v')
      ->orderBy('p.name')
      ->where('p.name like "%'.$this->searchstring.'%"')
      ->execute();
    }
    else
    {
      if($this->product_id)
     $this->products = Doctrine_Query::create()
      ->from('Product p')
      ->where('p.id ='.$this->product_id)
      ->execute();
    }
  }
  public function executeInvoicesearch(sfWebRequest $request)
  {
    $this->searchstring = $request->getParameter("searchstring");
    $requestparams=$request->getParameter("invoicedetail");
    $this->product_id = $requestparams["product_id"];

    $this->warehouses=WarehouseTable::fetchAll();

    $this->products=array();
    
    if($this->searchstring!="")
    {
     $this->products = Doctrine_Query::create()
      ->from('Product p, p.Invoicedetail id, id.Invoice inv, inv.Customer c')
      ->orderBy('p.name')
      ->where('p.name like "%'.$this->searchstring.'%"')
      ->execute();
    }
    else
    {
      if($this->product_id)
     $this->products = Doctrine_Query::create()
      ->from('Product p, p.Invoicedetail id, id.Invoice inv, inv.Customer c')
      ->where('p.id ='.$this->product_id)
      ->execute();
    }
  }
  public function executePurchasesearch(sfWebRequest $request)
  {
    $this->searchstring = $request->getParameter("searchstring");
    $requestparams=$request->getParameter("invoicedetail");
    $this->product_id = $requestparams["product_id"];

    $this->warehouses=WarehouseTable::fetchAll();

    $this->products=array();
    
    if($this->searchstring!="")
    {
     $this->products = Doctrine_Query::create()
      ->from('Product p, p.Purchasedetail pd, pd.Purchase po, po.Vendor v')
      ->orderBy('p.name')
      ->where('p.name like "%'.$this->searchstring.'%"')
      ->execute();
    }
    else
    {
      if($this->product_id)
     $this->products = Doctrine_Query::create()
      ->from('Product p, p.Purchasedetail pd, pd.Purchase po, po.Vendor v')
      ->where('p.id ='.$this->product_id)
      ->execute();
    }
  }
  public function executeStocksearch(sfWebRequest $request)
  {
    $this->searchstring = $request->getParameter("searchstring");
    $requestparams=$request->getParameter("invoicedetail");
    $this->product_id = $requestparams["product_id"];

    $this->warehouses=WarehouseTable::fetchAll();

    $this->products=array();
    
    if($this->searchstring!="")
    {
     $this->products = Doctrine_Query::create()
      ->from('Product p')
      ->orderBy('p.name')
      ->where('p.name like "%'.$this->searchstring.'%"')
      ->execute();
    }
    else
    {
      if($this->product_id)
     $this->products = Doctrine_Query::create()
      ->from('Product p, p.Stock s, s.Warehouse w')
      ->where('p.id ='.$this->product_id)
      ->execute();
    }
  }
  
  protected function executeBatchCreateproducttype(sfWebRequest $request)
  {
    $producttypedata=$request->getParameter('producttype');
    $producttype_name=$producttypedata["name"];
    $ids = $request->getParameter('ids');

		//search for producttype of given name
		$producttype=MyModel::fetchOne("Producttype",array('name'=>'"'.$producttype_name.'"'));

		//if not found,
		if(!$producttype)
		{
		  //create new product type
		  $producttype=new Producttype();
		  $producttype["name"]=$producttype_name;
		  $producttype["parent_id"]=1;
		  $producttype->calcPath();
		}

		$producttype_id=$producttype->getId();

		//set products to producttype
    $records = Doctrine_Query::create()
      ->from('Product')
      ->whereIn('id', $ids)
      ->execute();

    foreach ($records as $record)
    {
      $record->setProducttypeId($producttype_id);
      //$record->setName(str_replace("Pressure Tank","Tank",$record->getName()));
      $record->save();
    }
    $this->redirect($request->getReferer());
    
    
  }
  protected function executeBatchSetproducttype(sfWebRequest $request)
  {
    $producttypedata=$request->getParameter('product');
    $producttype_id=$producttypedata["producttype_id"];

    $ids = $request->getParameter('ids');

    $records = Doctrine_Query::create()
      ->from('Product')
      ->whereIn('id', $ids)
      ->execute();

    foreach ($records as $record)
    {
      $record->setProducttypeId($producttype_id);
      //$record->setName(str_replace("Pressure Tank","Tank",$record->getName()));
      $record->save();
    }
    $this->redirect($request->getReferer());
  }
  public function executePricelist(sfWebRequest $request)
  {
    $this->product = $this->getRoute()->getObject();
		$this->redirect("producttype/view?id=".$this->product->getProducttypeId());
  }
  public function executeInventory(sfWebRequest $request)
  {
    $this->product = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->product);

    #if there is only 1 warehouse, redirect to its stock card
    $warehousecount = Doctrine_Query::create()
      ->from('Warehouse w')
      ->count();
    if($warehousecount==1)
    {
      $stock = Doctrine_Query::create()
        ->from('Stock s')
        ->where("s.product_id=".$this->product->getId())
        ->fetchOne();
  		return $this->redirect("stock/view?id=".$stock->getId());
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $product=$this->getRoute()->getObject();

    $id=Doctrine_Query::create()
        ->from('Invoicedetail i')
        ->where("i.product_id=".$product->getId())
        ->fetchOne();
    $pd=Doctrine_Query::create()
        ->from('Purchasedetail i')
        ->where("i.product_id=".$product->getId())
        ->fetchOne();
    if($id!=null or $pd!=null)
    {
      $this->getUser()->setFlash('error', 'Cannot delete product - transactions exist.');
      return $this->redirect($request->getReferer());
    }

    if ($product->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("producttype/view?id=".$product->getProducttypeId());
  }
  public function executeBarcode(sfWebRequest $request)
  {
    $this->product = $this->getRoute()->getObject();
    $this->start=1;
  }
  public function executeBarcodethermalpdf(sfWebRequest $request)
  {
    $this->product_id=$request->getParameter("product_id");
    $this->product = Doctrine_Query::create()
      ->from('Product p')
      ->where('id = '.$this->product_id)
      ->fetchOne();

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeBarcodethermallargepdf(sfWebRequest $request)
  {
    $this->executeBarcodethermalpdf($request);
  }
  public function executeBarcodepdf(sfWebRequest $request)
  {
    $this->start=$request->getParameter("start");
    $this->qty=$request->getParameter("qty");
    $this->product_id=$request->getParameter("product_id");
    $this->product = Doctrine_Query::create()
      ->from('Product p')
      ->where('id = '.$this->product_id)
      ->fetchOne();

    $this->start--;
    $this->end=$this->start+$this->qty-1;


    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeQuickinput(sfWebRequest $request)
  {
		//this will only handle inputstring and parsing. 
		//All other functions will be delegated.

		//---initialize vars-------------------------------------------------------------------------------
		$this->linestrings=array();
		$this->cellstrings=array();
		$this->inputstring="";
		$this->errors=array();
		$this->messages=array();
		$this->generate=false;
		$this->tested=false;
    //---------case 2: method=get, init vars but do nothing: [ok]---------
  	//show empty
		if($request->getMethod()=="GET")
    {
    }
  	//---------case 3: method=post: process data ---------
		else
		{
	    $this->inputstring=$request->getParameter("inputstring");
			$this->parse();
	    $this->tested=true;
	    if($request->getParameter("submit")=="Save")
  	    $this->generate=true;

			//test parsed data
			//var_dump($this->cellstrings);

      //processing:
      //save each item under product type
      //expect 2 columns: producttype and product
      //optional column:price?
      foreach($this->cellstrings as $row)
      {
        $producttypename=$row[0];
        $productname=$row[1];

        //check if product exists
         $product = Doctrine_Query::create()
          ->from('Product p')
          ->where('p.name ="'.$productname.'"')
          ->fetchOne();
        if($product)
        {
          $this->errors[]="Product ".$productname." already exists.";
        }


        $producttype=null;
        if($producttypename)
        {
         $producttype = Doctrine_Query::create()
          ->from('Producttype pt')
          ->where('pt.path ="'.$producttypename.'"')
          ->fetchOne();
        }
        if(!$producttype)
        {
          $this->errors[]="Producttype ".$producttypename." not found.";
        }

        if(count($this->errors)==0)
        if($this->generate)
        {
          $product=new Product();
          $product->setName($productname);
          if($producttype)
          {
            $product->setProducttypeId($producttype->getId());
          }
          $product->save();
          $this->messages[]="Product ".$productname." created.";
        }
      }

/*
			$this->pricelistdata->process($this->cellstrings,$request);
			$this->producttypedata->process($this->cellstrings);
			$this->productdata->process($this->cellstrings,$this->producttypedata);
			$this->quotedata->process($this->cellstrings,$request);
			$this->productsfromproducttypedata->process($this->cellstrings,$this->producttypedata);
			

			if($request->getParameter("submit")=="Save" and count($this->errors)==0)
			{
				$this->pricelistdata->save();
				$this->producttypedata->save();
				$this->productdata->save();
				$this->quotedata->save();
			}

			//---update input string-----------------------------------------------------------------------
			$this->inputstring=
				$this->pricelistdata->getInputString().
				$this->producttypedata->getInputString().
				$this->productdata->getInputString();
		  */
		}
	}	
	private function parse()
	{
		$this->linestrings=explode("\n",$this->inputstring);
		$this->cellstrings=array();
		foreach($this->linestrings as $linestring)
		{
		  if(trim($linestring)=="")continue;
			$cellarray=explode("\t",$linestring);
			//trim each cell
			foreach($cellarray as &$cell)
				$cell=trim($cell);
			$this->cellstrings[]=$cellarray;
		}
	}
  public function executeRecalc(sfWebRequest $request)
  {
      //default values
    $this->interval=100;
    $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->products=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->products = Doctrine_Query::create()
      ->from('Product p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();
        foreach ($this->products as $product) {
          //rebuild quotes in case they're deleted or out of date
          foreach ($product->getPurchasedetail() as $detail) {
            $detail->updateProduct();
          }        
            $product->calcSellPrice();
            $product->calcBuyPrice();
        }
     $this->start=$this->end+1;
  }

  public function executeRecalcStock(sfWebRequest $request)
  {
      //default values
    $this->interval=100;
    $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->products=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->products = Doctrine_Query::create()
      ->from('Product p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();
        foreach ($this->products as $product) {
            foreach ($product->getStock() as $stock) {
                $stock->calc($stock->getDatetime());
            }
        }
     $this->start=$this->end+1;
  }
  public function executeReportDefect(sfWebRequest $request)
  {
    $this->product=Doctrine_Query::create()
      ->from('Product p')
    	->where('p.id = '.$request->getParameter('id'))
    	->fetchOne();

    $this->forward404Unless($this->product, sprintf('Invoice with id (%s) not found.', $request->getParameter('id')));

    //choose default warehouse by default, allow specifying of warehouse_id
    if($request->hasParameter('warehouse_id'))
      $this->warehouse_id=$request->getParameter('warehouse_id');
    else
      $this->warehouse_id=SettingsTable::fetch("default_warehouse_id");
    $this->stock = $this->product->getStock($this->warehouse_id);
    
    //create stock if it doesn't exist
    if($this->stock==null)
    {
      $this->stock=new Stock();
      $this->stock->setProductId($this->product->getId());
      $this->stock->setWarehouseId($this->warehouse_id);
      $this->stock->setCurrentQty(0);
      $this->stock->setDatetime(MyDate::today());
      $this->stock->save();
    }

    //create form for new stock entry
    $this->stockentry=new Stockentry();
    $this->stockentry->setDatetime(MyDate::today());
    $this->stockentry->setStockId($this->stock->getId());
    $this->form=new StockentryForm($this->stockentry);
    
    //fetch warehouses to be displayed
    $this->warehouses = Doctrine_Query::create()
      ->from('Warehouse w')
      ->execute();
      
    
  }
  public function executeProcessReportDefect(sfWebRequest $request)
  {
    //imitate structure of processForm()
    $requestparam=$request->getParameter('stockentry');
    $form = new StockEntryForm();
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

    //validation
    //defect means qty goes down
    //qty must be negative and not 0
    if($requestparam['qty']==0 or $requestparam['qty']=="")
    {
      //invalid qty
      //error msg
      $this->getUser()->setFlash('error', 'Invalid Quantity');
      $this->redirect($request->getReferer());
    }
    elseif($requestparam['qty']>0)
    {
      $requestparam['qty']*=-1;
    }
    
    //if no errors, save
    if ($form->isValid())
    {
      try 
      {
        $stock=StockTable::fetchById($requestparam['stock_id']);

        $qty=$requestparam['qty'];
        $date=$requestparam['date']['year']."-".$requestparam['date']['month']."-".$requestparam['date']['day'];
        $type='Defective';
        $description=$requestparam['description'];

        $stockentry=$stock->addEntry($date, $qty, null, null ,$type,$description,$this->getUser()->getGuardUser()->getId());
      } 
      catch (Doctrine_Validator_Exception $e) 
      {
        $this->getUser()->setFlash('error', 'There was an error. Please check your entry.');
        $this->redirect($request->getReferer());
      }
    }

    //go back to report defect form
    $this->redirect($request->getReferer());
  }
  public function executeFiles(sfWebRequest $request)
  {
    $this->product=MyModel::fetchOne("Product",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('product');
    $this->file->setParentId($this->product->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="product"')
      ->andWhere('f.parent_id='.$this->product->getId())
      ->execute();
  }
  public function executeToggleIsUpdated(sfWebRequest $request)
  {
    $product=MyModel::fetchOne("Product",array('id'=>$request->getParameter("id")));
    if($product->getIsUpdated())
      $product->setIsUpdated(null);
    else
      $product->setIsUpdated(1);
    $product->save();
    $this->getUser()->setFlash('notice', $notice.' Successfully set this product to '.($product->getIsUpdated()?"Updated":"Not Updated").'.',true);
    $this->redirect($request->getReferer());
  }
  public function executebuyPriceQuickInput(sfWebRequest $request)
  {
    if($request->getMethod()=="GET")
    {
      $this->inputstring="";
    }
    //else if post
    else
    {
      $inputstring=$request->getParameter("inputstring");
      $this->inputstring=$inputstring;
      $lines=explode("\n",$inputstring);
      
      //Date 	Product 	Invoice No. 	Sale Price 	Qty 	Total Sale
      $counter=0;
      $productsprocessed=0;
      foreach($lines as $line)
      {
        $counter++;
        
        //skip empty lines
        if(trim($line)=="")continue;
        
        //parse
        $segments=explode("\t",$line);
        $productname=$segments[1];
        $price=trim($segments[6]);

        //validate
        if(count($segments)<7)
        {
          $this->getUser()->setFlash('error', "No price (column 7) for line ".$counter);
          return $this->redirect('product/buyPriceQuickInput');
        }
        //if price is not numeric or price is not positive number
        if(!is_numeric($price) or $price<=0)
        {
          $this->getUser()->setFlash('error', "No price for: ".$productname);
          return $this->redirect('product/buyPriceQuickInput');
        }
        $product=Doctrine_Query::create()
        ->from('Product p')
        ->where("p.name = '".$productname."'")
        ->fetchOne();
        if($product==null)
        {
          $this->getUser()->setFlash('error', "Invalid product: ".$productname);
          return $this->redirect('product/buyPriceQuickInput');
        }

      }
   
      //save   
      foreach($lines as $line)
      {
         $counter++;
        
        //skip empty lines
        if(trim($line)=="")continue;
        
        //parse
        $segments=explode("\t",$line);
        $productname=$segments[1];
        $price=trim($segments[6]);
        $product=Doctrine_Query::create()
        ->from('Product p')
        ->where("p.name = '".$productname."'")
        ->fetchOne();
        $product->setMaxbuyprice($price);
        $product->setMinbuyprice($price);
        $product->save();

        $productsprocessed++;
     }
     $this->getUser()->setFlash('msg', $productsprocessed." products processed");    }
  }
  public function executeMerge1(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
    
  }
  public function executeMerge2(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
 
    $this->merger_id=$request->getParameter("merger_product_id");
    $this->mergee_id=$request->getParameter("mergee_product_id");
    $this->merger=Doctrine_Query::create()
        ->from('Product p')
        ->where("p.id = '".$this->merger_id."'")
        ->fetchOne();
    $this->mergee=Doctrine_Query::create()
        ->from('Product p')
        ->where("p.id = '".$this->mergee_id."'")
        ->fetchOne();
   
  }
  public function executeMerge3(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential(array('admin'),false))
      return $this->redirect("home/error?msg=Access Denied");
 
    $merger=Doctrine_Query::create()
        ->from('Product p')
        ->where("p.id = '".$request->getParameter("merger_product_id")."'")
        ->fetchOne();
    $mergee=Doctrine_Query::create()
        ->from('Product p')
        ->where("p.id = '".$request->getParameter("mergee_product_id")."'")
        ->fetchOne();
   
    //merger merges into mergee
    //merger changes product id
    Doctrine_Query::create()
      ->update('Conversiondetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Deliverydetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Invoicedetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
/*
    Doctrine_Query::create()
      ->update('Priceitem p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
*/
    Doctrine_Query::create()
      ->update('Profitdetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Purchasedetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Returnsdetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Replacementdetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('Quotationdetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
      Doctrine_Query::create()
      ->update('Quote p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('InvoiceDrDetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    Doctrine_Query::create()
      ->update('PurchaseDrDetail p')
      ->set('p.product_id',$mergee->getId())
      ->where('p.product_id = ?', $merger->getId())
      ->execute();
    
    $default_warehouse_id=SettingsTable::fetch("default_warehouse_id");
    $merger_stock=Doctrine_Query::create()
        ->from('Stock p')
        ->where('p.product_id = ?', $merger->getId())
        ->andWhere('p.warehouse_id = ?', $default_warehouse_id)
        ->fetchOne();
    $mergee_stock=Doctrine_Query::create()
        ->from('Stock p')
        ->where('p.product_id = ?', $mergee->getId())
        ->andWhere('p.warehouse_id = ?', $default_warehouse_id)
        ->fetchOne();

    //if mergee stock doesn't exist, create
    if(!$mergee_stock)
    {
      $mergee_stock=new Stock();
      $mergee_stock->setProductId($mergee->getId());
      $mergee_stock->setWarehouseId($default_warehouse_id);
      $mergee_stock->setCurrentQty(0);
      $mergee_stock->setDatetime(MyDate::today());
      $mergee_stock->save();
    }

    //if merger stock exist, merge into mergee
    if($merger_stock)
    {
      Doctrine_Query::create()
        ->update('Stockentry p')
        ->set('p.stock_id',$mergee_stock->getId())
        ->where('p.stock_id = ?', $merger_stock->getId())
        ->execute();
      $merger_stock->delete();
    }
    //else, no need to merge stock entries

    $mergee_stock->calc();
    $merger->delete();

    $this->getUser()->setFlash('msg', $merger." merged into ".$mergee, true);
  }
  public function executeCalcQuota(sfWebRequest $request)
  {
    //disabled, profit calculation is now automatic
    //return $this->redirect("home/error?msg=This function is now disabled; profit calculation is now automatic");
    
     $lastproduct = Doctrine_Query::create()
      ->from('Product i')
	    ->orderBy('i.id desc')
      ->fetchOne();
  
    if(!$lastproduct)
    {
  	  return $this->redirect("home/error?msg=No products exist to process");
    }
  
      //default values
    $this->interval=100;
    $this->start=$lastproduct->getId();

    if($request->getMethod()!="POST")
       return;
       
    if(isset($_REQUEST["interval"]))
      $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
      $this->start=$request->getParameter("start");
    $this->end=$this->start-$this->interval+1;
    
    $this->products = Doctrine_Query::create()
      ->from('Product i')
      ->where('i.id >='.$this->end)
      ->andWhere('i.id <='.$this->start)
      ->orderBy('i.id desc')
      ->execute();

    foreach ($this->products as $product) {
      $product->calcQuota();
      $product->save(); 
    }

    $this->start=$this->end-1;
  }
  public function executeDuplicateError(sfWebRequest $request)
  {
    $this->product=Fetcher::fetchOne("Product",array("id"=>$request->getParameter("id")));
  }
}
