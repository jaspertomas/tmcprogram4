<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>


<?php /*
<?php
$startdate=MyDateTime::frommysql($startdateform->getObject()->getDate())->tomysql();
$enddate=MyDateTime::frommysql($enddateform->getObject()->getDate())->tomysql();
$today=MyDateTime::frommysql($startdate);
$yesterday=MyDateTime::frommysql($startdate);
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate);
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
$startofthisyear=$today->getstartofyear();
$startofnextyear=$startofthisyear->addyears(1);
$startoflastyear=$startofthisyear->addyears(-1);
$endofthisyear=$startofthisyear->getendofyear();
$endofnextyear=$startofnextyear->getendofyear();
$endoflastyear=$startoflastyear->getendofyear();
$currentyear=$today->getYear();
$currentmonth=$today->getMonth();
?>

<?php echo form_tag_for(new InvoiceForm(),"passbook/view")?>
<input type=hidden name="id" id="id" value="<?php echo $passbook->getId()?>"  >
<input type=hidden name="passbook[id]" id="id" value="<?php echo $passbook->getId()?>"  >
Bank Account: <?php
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'Passbook',
  'add_empty' => true,
));
echo $w->render('id',(isset($passbook)?$passbook->getId():null));
?>

<table>
  <tr>
    <td>From Date</td>
    <td><?php echo $startdateform["date"] ?></td>
  </tr>
  <tr>
    <td>To Date</td>
    <td><?php echo $enddateform["date"] ?></td>
    <td><input type=submit value=View ></td>
  </tr>
</table>
</form>
<?php echo link_to("Last Year","passbook/view?id=".$id."&startdate=".$startoflastyear->tomysql()."&enddate=".$endoflastyear->tomysql());?> |
<?php echo link_to("This Year","passbook/view?id=".$id."&startdate=".$startofthisyear->tomysql()."&enddate=".$endofthisyear->tomysql());?> |
<?php echo link_to("Next Year","passbook/view?id=".$id."&startdate=".$startofnextyear->tomysql()."&enddate=".$endofnextyear->tomysql());?> |
<br>
<?php echo link_to("Last Month","passbook/view?id=".$id."&startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> |
<?php echo link_to("This Month","passbook/view?id=".$id."&startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> |
<?php echo link_to("Next Month","passbook/view?id=".$id."&startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> |
<br>
    <?php echo link_to("Yesterday","passbook/view?id=".$id."&startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> |
    <?php echo link_to("Tomorrow","passbook/view?id=".$id."&startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> |
<br>
*/?>

<?php echo form_tag_for(new InvoiceForm(),"passbook/view")?>
<input type=hidden name="id" id="id" value="<?php echo $passbook->getId()?>"  >
<input type=hidden name="passbook[id]" id="id" value="<?php echo $passbook->getId()?>"  >
Bank Account: <?php
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'Passbook',
  'add_empty' => true,
));
echo $w->render('id',(isset($passbook)?$passbook->getId():null));
?>
<input type=submit value=View >
</form>

<h1>Passbook Monitoring </h1>
<h2><?php echo $passbook->getName()?></h2>

<table>
  <tr>
    <td><?php echo link_to("Recalculate","passbook/calc?id=".$passbook->getId()) ?></td>
  </tr>
  <tr>
    <td>
      Is Updated: <?php echo $passbook->getIsUpdated()?"Yes":"No" ?> (<?php echo link_to("Set to ". ($passbook->getIsUpdated()?"No":"Yes"),"passbook/toggleIsUpdated?id=".$passbook->getId())?>)

      <?php //echo link_to("Calculate","passbook/calc?id=".$passbook->getId()) ?>
    </td>
  </tr>
  <tr>
    <td><?php //echo link_to("View Transactions By Page","product/transactionsByPage?id=".$passbook->getProductId()) ?></td>
  </tr>
  <tr>
    <td><?php //echo link_to("View Transactions By Date","product/transactions?id=".$passbook->getProductId()) ?></td>
  </tr>
</table>

<?php //echo link_to("Add Detail","passbook_entry/new?id=".$passbook->getId()) ?>


<?php if($sf_user->hasCredential(array('admin','encoder'), false)){?>
<hr>

<b>New Passbook Record</b>
<!--inventory records form-->
<?php 
  $passbook_entry=new PassbookEntry();
  $passbook_entry->setDatetime(MyDateTime::now()->todatetime());
  $form=new PassbookEntryForm($passbook_entry); 
?>
<?php echo form_tag_for($form,"@passbook_entry"); ?>
<input type=hidden name=redirect_to value="view"  >
<input type=hidden name="passbook_entry[passbook_id]" id="passbook_entry_passbook_id" value="<?php echo $passbook->getId()?>"  >
    <?php echo $form->renderHiddenFields(false) ?>
		<input name="passbook_entry[qty]" id="passbook_entry_qty" type="hidden" value="0">    
		<input name="passbook_entry[type]" id="passbook_entry_type" type="hidden" value="Report">
		<input name="passbook_entry[trans_type]" id="passbook_entry_type" type="hidden" value="REPORT">
		<input name="passbook_entry[is_report]" id="passbook_entry_type" type="hidden" value="1">
<table>
	<tr>
		<td>Date/Time</td>
		<td>Actual Qty</td>
		<td>Notes</td>
	</tr>
	<tr>
		<td><?php echo $form["datetime"]; ?></td>
		<td><input name="passbook_entry[qty_reported]" id="passbook_entry_qty_reported" type="text" size=10></td>
		<td><?php echo $form["description"]; ?></td>
		<td><input type=submit name=submit value=Save  ></td>
	</tr>
</table>
</form>


<?php } ?>    

<br>

<b>Page <?php echo $page;?></b>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."&page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."&page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo "<b>".$apage."</b> ";
  else echo link_to("$apage",$pagepath."&page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."&page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."&page=".$pagecount))." ";

?>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Passbook In</td>
    <td>Passbook Out</td>
    <td>Balance</td>
    <td>Ref / Actual Qty</td>
    <td>Check No.</td>
    <td>Account</td>
    <td>Customer / Supplier ; Notes</td>
    <td>Created By</td>
    <td>Status</td>
    <td></td>
  </tr>
  <?php 
    foreach($passbookentries as $se){
        $is_conversion=false;
        $ref=$se->getRef();
        $parent=null;
        $parentref=null;
        $profitdetails=array();
  ?>
  <tr>

  <td><!-- date -->
      <?php 
          echo $se->getDate();
      ?>
    </td>
    <td align=right><!-- Passbook In -->
      <?php if($se->getQty()>0)echo MyDecimal::format($se->getQty()) ?>
      </td>
    <td align=right><!-- Passbook Out -->
      <?php if($se->getQty()<0)echo MyDecimal::format($se->getQty()*-1) ?>
    </td>
    <td align=right><!-- Balance -->
      <?php 
        if($se->getType()=="Report")
          echo $se->getQtyReported();
        else
          if($se->getQty()!=0) echo MyDecimal::format($se->getBalance());
      ?>
    </td>
    <td><!-- Ref -->
      <?php 
        switch($se->getRefClass())
        {
          case "Voucher":
            $voucher=$ref;
            echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId());
            break;
          case "InCheck":
            $inCheck=$ref;
            echo link_to($inCheck->getCode(),"in_check/view?id=".$inCheck->getId());
            break;
          default:
            if($se->getType()=="Report")
              echo "Prev Balance ".$se->getBalance();
            break;
        }
      ?>
    </td>
    <td><!-- check no -->
      <?php 
        switch($se->getRefClass())
        {
          case "Voucher":
            $voucher=$ref;
            $check=$voucher->getOutCheck();
            if($check->getIsBankTransfer()==1)
              echo "Bank Transfer";
            else
              echo $check->getCheckNo();
            break;
          case "InCheck":
            $check=$ref;
            if($check->getIsBankTransfer()==1)
              echo "Bank Transfer";
            else
              echo $check->getCheckNo();
            break;
          default:
            if($se->getType()=="Report")
              echo $se->getReportComment();
            break;
        }
    
    ?></td>
    <td><!-- account -->
      <?php 
        switch($se->getRefClass())
        {
          case "Voucher":
            echo $voucher->getVoucherAccount();
            break;
          case "InCheck":
            break;
          default:
            break;
        }
    
    ?></td>
    <td>
      <!--customer/supplier/notes-->
      <?php 
        switch($se->getRefClass())
        {
          case "Voucher":
            $voucher=$ref;
            echo $voucher->getPayee()."; ".$se->getDescription();
            break;
          case "InCheck":
            $inCheck=$ref;
            foreach($inCheck->getInvoice() as $invoice){
              echo link_to($invoice,"invoice/view?id=".$invoice->getId())." (".$invoice->getCustomer()->getName().")";
            }
            foreach($inCheck->getEvents() as $event){
              $invoice=$event->getParent();
              echo link_to($invoice,"invoice/view?id=".$invoice->getId())." (".$invoice->getCustomer()->getName().")";
            }
            break;
        }
      ?>
    </td>
    <td>
      <?php
        $created_by=false; 
        if($se->getCreatedById())
          $created_by = Doctrine_Query::create()
          ->from('SfGuardUser s')
          ->where("s.id=".$se->getCreatedById())
          ->fetchOne();
        if($created_by)echo $created_by->getUsername(); 
      ?>
    </td>

    <!-- status -->
    <td>
      <font <?php if($voucher->getStatus()=="Cancelled")echo "color=red";?>>
      <?php if($voucher->getStatus()!="Pending")echo $voucher->getStatus()?></td>
      </font>
    <?php 
      if($sf_user->hasCredential(array('admin'), false))if($se->getType()=="Adjustment" or $se->getType()=="Report")echo link_to('Delete','passbook_entry/delete?id='.$se->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>


    <?php 
      switch($se->getRefClass())
      {
        case "Purchasedetail":
          $purchase=$ref->getPurchase();
          $files=$purchase->getFiles();
          if(count($files)>0)echo link_to(count($files)." File".(count($files)>1?"s":""),"purchase/files?id=".$ref->getPurchaseId());
          break;
      }
    ?>



      
    </td>
    
    <?php 
      $qtyarray=array(null,null,null,null,null,null,null,null,null);
      $colorarray=array(null,null,null,null,null,null,null,null,null);
      $fontcolorarray=array(null,null,null,null,null,null,null,null,null);
      $color="";
      $bold=false;
      $openbold="";
      $closebold="";
      switch($se->getRefClass())
      {
        case "Purchasedetail":
          $bold=true;
          $openbold="<b>";
          $closebold="</b>";
          $qtyarray[$ref->getSlot()]=str_replace(".00","",$ref->getQty());
          $colorarray[$ref->getSlot()]=$ref->getColor();
          $fontcolorarray[$ref->getSlot()]=($ref->getColorIndex()>3)?"white":"black";
          break;
        case "Invoicedetail":
          //there's really only one, but who cares
          $profitdetails= $ref->getProfitdetail();
          foreach($profitdetails as $pdetail)
          {
            $qtyarray[$pdetail->getSlot()]=link_to(str_replace(".00","",$pdetail->getQty()),"passbook/disconnectInvoiceAndPurchase?profit_detail_id=".$pdetail->getId(), array('confirm' => 'Are you sure?'));
            $colorarray[$pdetail->getSlot()]=$pdetail->getColor();
            $fontcolorarray[$pdetail->getSlot()]=($pdetail->getColorIndex()>3)?"white":"black";
          }
          break;
      }
    ?>
    <?php if($qtyarray[1]!=null)echo "<td align=center bgcolor=".$colorarray[1]."><font color=".$fontcolorarray[1].">".$openbold.$qtyarray[1].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[2]!=null)echo "<td align=center bgcolor=".$colorarray[2]."><font color=".$fontcolorarray[2].">".$openbold.$qtyarray[2].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[3]!=null)echo "<td align=center bgcolor=".$colorarray[3]."><font color=".$fontcolorarray[3].">".$openbold.$qtyarray[3].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[4]!=null)echo "<td align=center bgcolor=".$colorarray[4]."><font color=".$fontcolorarray[4].">".$openbold.$qtyarray[4].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[5]!=null)echo "<td align=center bgcolor=".$colorarray[5]."><font color=".$fontcolorarray[5].">".$openbold.$qtyarray[5].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[6]!=null)echo "<td align=center bgcolor=".$colorarray[6]."><font color=".$fontcolorarray[6].">".$openbold.$qtyarray[6].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[7]!=null)echo "<td align=center bgcolor=".$colorarray[7]."><font color=".$fontcolorarray[7].">".$openbold.$qtyarray[7].$closebold."</font></td>";else echo "<td></td>";?>
    <?php if($qtyarray[8]!=null)echo "<td align=center bgcolor=".$colorarray[8]."><font color=".$fontcolorarray[8].">".$openbold.$qtyarray[8].$closebold."</font></td>";else echo "<td></td>";?>

    <td><?php //echo $se->getId(); ?></td>
  </tr>
  <?php }//end foreach $passbookentries as $se?>      
</table>

<b>Page <?php echo $page;?></b>
<br>
<?php
echo ($page==1?"First":link_to("First",$pagepath."&page=1"))." ";
echo ($page==1?"Previous":link_to("Previous",$pagepath."&page=".($page-1)))." ";
foreach(range(1,$pagecount) as $apage)
{
  if($apage<$page-20)continue;
  if($apage>$page+20)continue;
  if($apage==$page)echo "<b>".$apage."</b> ";
  else echo link_to("$apage",$pagepath."&page=".$apage)." ";
}

echo ($page==$pagecount?"Next":link_to("Next",$pagepath."&page=".($page+1)))." ";
echo ($page==$pagecount?"Last":link_to("Last",$pagepath."&page=".$pagecount))." ";

?>

<br>
<br>

<input id="copy_text">
<script>
function copy(mystring)
{
  var copyText = document.getElementById("copy_text");
  copyText.value=mystring;
  copyText.select();
  document.execCommand("copy");
  //alert(copyText.value+"copied to clipboard");
}
</script>
