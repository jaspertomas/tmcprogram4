<!--PAGINATION-->
<?php if($pages>0){?>
<?php echo link_to("Latest","passbook/view?id=".$passbook->getId()."&page=1")." ";?>
<?php if($current_page!=1)echo link_to("Next","passbook/view?id=".$passbook->getId()."&page=".($current_page-1));else echo "Next"; echo " ";?>
<?php foreach(range(1,$pages,1) as $page){
  if($page==$current_page)echo $page." ";
  elseif(
    ($page>$current_page-9 and $page<$current_page+9)
    or ($page==$current_page+20)
    or ($page==$current_page-20)
    or ($page==$current_page+50)
    or ($page==$current_page-50)
    or ($page==$current_page+100)
    or ($page==$current_page-100)
    or ($page==$current_page+200)
    or ($page==$current_page-200)
    or ($page==$current_page+500)
    or ($page==$current_page-500)
    or ($page==$current_page+1000)
    or ($page==$current_page-1000)
  ){
?>
  <?php echo link_to($page,"passbook/view?id=".$passbook->getId()."&page=".$page)." ";?>
<?php }} ?>
<?php if($current_page!=$pages)echo link_to("Previous","passbook/view?id=".$passbook->getId()."&page=".($current_page+1));else echo "Previous";echo " ";?>
<?php echo link_to("Earliest","passbook/view?id=".$passbook->getId()."&page=".$pages)." ";?>
<?php } ?>
<!--END PAGINATION-->

