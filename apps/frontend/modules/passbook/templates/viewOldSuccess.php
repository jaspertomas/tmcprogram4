
<h1>Bank Account <?php echo $passbook->getName()?></h1>

<table>
<tr>
    <td>Account No.</td>
    <td><?php echo $passbook->getAccountNo()?></td>
</tr>
</table>

<br><?php echo link_to("Add Entries","passbook/addEntries?id=".$passbook->getId());?>

<h2>Entries:</h2>

<?php include_partial('view_pagination', array('pages' => $pages, 'current_page' => $current_page, 'passbook' => $passbook)) ?>

<table border=1>
  <tr>
    <td>Date</td>
    <td>Type</td>
    <td>Details</td>
    <td>Deposit</td>
    <td>Withdraw</td>
    <td>Balance</td>
  </tr>

<?php foreach($entries as $detail){ if($detail->getId()>0)?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td>
        <?php echo $detail->getTransType() ?>
        <?php echo link_to("Scan","PassbookEntry/findMatch?id=".$detail->getId(),array("target"=>"_target"))?>
      </td>
      <td><?php echo $detail->getMeta() ?></td>
      <td><?php echo $detail->getDeposit()!=0?$detail->getDeposit():"" ?></td>
      <td><?php echo $detail->getWithdrawal()!=0?$detail->getWithdrawal():"" ?></td>
      <td><?php echo $detail->getBalance() ?></td>
      <td><?php echo $detail->getDescription() ?></td>
      <td><?php $client=$detail->getClient();if($client)echo $client; ?></td>
    </tr>
  <?php }?>

</table>

<?php include_partial('view_pagination', array('pages' => $pages, 'current_page' => $current_page, 'passbook' => $passbook)) ?>

