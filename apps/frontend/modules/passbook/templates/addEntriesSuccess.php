<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h1>Bank Account <?php echo $passbook->getName()?></h1>

<table>
<tr>
    <td>Account No.</td>
    <td><?php echo $passbook->getAccountNo()?></td>
</tr>
</table>

<br><?php echo link_to("BACK","passbook/view?id=".$passbook->getId());?>

<h2>Add Entries</h2>

<?php echo form_tag("passbook/processAddEntries")?>

Format: <select name=format id=format>
<option value="md">Metrobank Direct</option>
<option value="mbos">MBOS</option>
</select>

<br><input type=submit value=Submit>
<br>
<input type=hidden name=id id=id value=<?php echo $passbook->getId()?>>
<textarea name=data id=data rows=10 cols=80>
</textarea>
<br><input type=submit value=Submit>
</form>
