
<h1>Bank Account <?php echo $passbook->getName()?></h1>

<table>
<tr>
    <td>Account No.</td>
    <td><?php echo $passbook->getAccountNo()?></td>
</tr>
</table>

<br><?php echo link_to("Add More Entries","passbook/addEntries?id=".$passbook->getId());?>

<h2>Results:</h2>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Type</td>
    <td>Details</td>
    <td>Deposit</td>
    <td>Withdraw</td>
    <td>Description</td>
  </tr>
<?php foreach($results as $detail){?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td>
        <?php echo $detail->getTransType() ?>
        <?php if($detail->getClientType()==null)echo link_to("Scan","PassbookEntry/findMatch?id=".$detail->getId(),array("target"=>"_target"))?>
      </td>
      <td><?php echo $detail->getMeta() ?></td>
      <td><?php echo $detail->getDeposit()!=0?$detail->getDeposit():"" ?></td>
      <td><?php echo $detail->getWithdrawal()!=0?$detail->getWithdrawal():"" ?></td>
      <td><?php echo $detail->getDescription() ?></td>
    </tr>

    <?php $check=$detail->getClient();if($check){ ?>
        <tr>
          <td></td>
          <td>MATCH <?php echo link_to("(Unmatch)","PassbookEntry/unMatch?id=".$detail->getId())?></td>
          <td><?php echo link_to($check->getCode(),$check->getModule()."/view?id=".$check->getId()) ?></td>
          <td><?php echo MyDateTime::frommysql($check->getCheckDate())->toshortdate() ?></td>
          <td><?php echo $check->getCheckNo() ?></td>
          <td><?php echo $check->genDefaultDescription() ?></td>
        </tr>
    <?php }else{ ?>
      <!--json checks-->
      <?php $checks=$detail->getJsonChecks();
      if(count($checks)>0)
      foreach($checks as $check){?>
        <tr>
          <td></td>
          <td>
            Possible
            <!--show only if check does not yet have a match-->
            <?php if($check->getPassbookEntryId()==null)echo link_to("Match","PassbookEntry/match?client_id=".$check->getId()."&id=".$detail->getId())?>
          </td>
          <td><?php echo link_to($check->getCode(),$check->getModule()."/view?id=".$check->getId()) ?></td>
          <td><?php echo MyDateTime::frommysql($check->getCheckDate())->toshortdate() ?></td>
          <td><?php echo $check->getCheckNo() ?></td>
          <td>
            <?php echo $check->getAmount() ?>
            <br><?php echo $check->genDefaultDescription() ?>
          </td>
        </tr>
      <?php } ?>
    <?php } ?>
<?php } ?>
</table>

