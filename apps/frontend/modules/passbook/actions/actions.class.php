<?php

require_once dirname(__FILE__).'/../lib/passbookGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/passbookGeneratorHelper.class.php';

/**
 * passbook actions.
 *
 * @package    sf_sandbox
 * @subpackage passbook
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class passbookActions extends autoPassbookActions
{
    public function executeResults(sfWebRequest $request)
    {
      $this->passbook=Fetcher::fetchOne("Passbook",array("id"=>$request->getParameter("id")));

      //these passbook entries went through processing      
      $first=$request->getParameter("first");
      $last=$request->getParameter("last");
      $this->results=Doctrine_Query::create()
        ->from('PassbookEntry e')
        ->where('e.id >= '.$first)
        ->andWhere('e.id <= '.$last)
        ->execute();
    }
    public function executeViewOld(sfWebRequest $request)
    {
      $this->current_page=$request->getParameter("page");
      if(!$this->current_page)$this->current_page=1;
      $this->passbook=$this->getRoute()->getObject();
      $entries=$this->passbook->getEntriesByPage($this->current_page);
      if(0)
      {
        $this->entries=$entries;
      }
      else
      {
        $this->entries=array();
        //reverse order of entries
        for ($i = $entries->count()-1; $i >= 0; $i--) {
          $this->entries[]=$entries->get($i);
        }
      }

      $this->pages=$this->passbook->getPages();
    }
    public function executeAddEntries(sfWebRequest $request)
    {
      $this->passbook=$this->getRoute()->getObject();
    }
    public function executeProcessAddEntries(sfWebRequest $request)
    {
      $format=$request->getParameter("format");

      $passbook=Fetcher::fetchOne("Passbook",array("id"=>$request->getParameter("id")));
      $input=$request->getParameter("data");
      //remove commas
      //they turn numbers into strings
      $input=str_replace(",","",$input);
      $input=explode("\n",$input);
      
      $data=array();
      for($i=0;$i<count($input);$i++)
      {
        $line=$input[$i];
        //if not empty
        if(trim($line)!="")
        {
          //add it to lines
          $line=explode("\t",trim($input[$i]));
          
          $datesegments=explode("/",trim($line[0]));
          $date=$datesegments[2]."-".$datesegments[0]."-".$datesegments[1];
          $line[0]=$date;
        
          $data[]=$line;
        }

          
      }

      //mbos is latest first
      //md is latest last
      //if format is mbos, reverse array
      if($format=="mbos")
        $data=array_reverse($data);
 
      //get latest entry from database
      $last=$passbook->getLastEntry();

      //if last entry exists
      $i=0;
      if($last)
      {
        $lastdate=$last->getDate();
        $lastbalance=$last->getBalance();

        //look for a match in date and balance of new entries
        for(;$i<count($data);$i++)
        {
          $line=$data[$i];
          if($format=="mbos")
          {
            $date=$line[0];
            $balance=$line[11];
          }
          else if($format=="md")
          {
            $date=$line[0];
            $balance=$line[5];
          }
          if($date==$lastdate and $balance==$lastbalance)
          {
            $i++;
            break;
          }
        }
        //if end reached, match not found
        if($i==count($data))
        {
          //error message
            $message="Sorry, cannot find match for previous balance of ".$lastbalance." on ".MyDateTime::toshort($lastdate);
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
        }
      }
      
      //by now, old entries skipped
      //start saving new entries
      $first=true;
      $firstEntry=null;
      for(;$i<count($data);$i++)
      {
        $line=$data[$i];
        //format: mbos: date	1	2	3	4	checkno	type	7	out	in	10	balance
        //format: md: date	type	checkno	out	in	balance

        if($format=="mbos")
        {
          $date=$line[0];
          $checkno=$line[5];
          $type=$line[6];
          $out=$line[8];
          $in=$line[9];
          $description=$line[10];
          $balance=$line[11];
        }
        else if($format=="md")
        {
          $date=$line[0];
          $type=$line[1];
          $checkno=$line[2];
          $out=$line[3];
          $in=$line[4];
          $balance=$line[5];
          $description=$line[6];
        }
        
        //validate format
        //mbos has a column 11
        if($format=="mbos" and !isset($line[11]))
        {
          //error message
            $message="Wrong format";
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
        }
        //metrobank direct has no column 11
        else if($format=="md" and isset($line[11]))
        {
          //error message
            $message="Wrong format";
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
        }

        //validate date
        if(!MyDateTime::frommysql($date)->isvalid())
        {
          //error message
            $message="Invalid date: ".$date." on line ".($i+1);
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
        }

        $entry=new PassbookEntry();
        $entry->setDate($date);
        $entry->setTransType($type);
        $entry->setMeta($checkno);
        $entry->setWithdrawal($out);
        $entry->setDeposit($in);
        $entry->setBalance($balance);
        $entry->setDescription($description);
        $entry->setPassbookId($passbook->getId());
        $entry->save();
        
        //save id of first entry
        if($first)
        {
          $first=false;
          $firstEntry=$entry;
        }
        
        $entry->findMatch();
      }
      $lastEntry=$entry;
      
      return $this->redirect("passbook/results?first=".$firstEntry->getId()."&last=".$lastEntry->getId()."&id=".$passbook->getId());
    }
    public function executeView(sfWebRequest $request)
    {
      $this->id=$request->getParameter("id");
      if($this->id==null)
        $this->passbook=Fetcher::fetchOne("Passbook");
      else
        $this->passbook=Fetcher::fetchOne("Passbook",array("id"=>$this->id));
  
      //auto set date to today, unless date is specified
      list($this->startdateform, $this->enddateform) = MyDateRangeHelper::getForms($request);
      $startdate=$this->startdateform->getObject()->getDate();
      $enddate=$this->enddateform->getObject()->getDate();
  
      $this->itemsperpage=50;
      $this->page=$request->getParameter("page");
      if($this->page==null)$this->page=1;
      $this->form = $this->configuration->getForm($this->passbook);
      $this->passbookentries=$this->passbook->getPassbookEntriesDescByPage($this->page,$this->itemsperpage);
      $this->pagecount=$this->passbook->getPassbookentriesPageCount($this->itemsperpage);
      $this->pagepath="passbook/view?id=".$this->passbook->getId();
    }
    public function executeCalc(sfWebRequest $request)
    {
      $this->stock = $this->getRoute()->getObject();
      $this->stock->calc($this->stock->getDatetime());
      $this->redirect($request->getReferer());
      
    }
  }
