<?php

require_once dirname(__FILE__).'/../lib/counter_receipt_detailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/counter_receipt_detailGeneratorHelper.class.php';

/**
 * counter_receipt_detail actions.
 *
 * @package    sf_sandbox
 * @subpackage counter_receipt_detail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class counter_receipt_detailActions extends autoCounter_receipt_detailActions
{
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter("counter_receipt_detail");
    $requestparams["pono"]=trim($requestparams["pono"]);
    $counter_receipt=Fetcher::fetchOne("CounterReceipt",array("id"=>$requestparams["counter_receipt_id"]));

    //validate
    //if pono is blank, error
    $pono=$requestparams["pono"];
    if($pono=="")
    {
      $message="PO Number is required";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect("counter_receipt/view?id=".$requestparams["counter_receipt_id"]);
      //return sfView::SUCCESS;
    }

      //find purchase with po_number
      $purchase=Doctrine_Query::create()
        ->from('Purchase p')
        ->where('pono = "'.$pono.'"')
        ->fetchOne();
      //if not found, try flexible search
      if($purchase==null)
      {
        $purchase=Doctrine_Query::create()
        ->from('Purchase p')
        ->where('pono like "%'.$pono.'%"')
        ->fetchOne();
      }
      if($purchase==null)
      {
        $message="Invalid PO Number";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect("counter_receipt/view?id=".$requestparams["counter_receipt_id"]);
        //return sfView::SUCCESS;
      }
      if($purchase->getVendorId()!=$counter_receipt->getVendorId())
      {
        $message="Purchase Order ".$pono." does not belong to ".$counter_receipt->getVendor();
        $this->getUser()->setFlash('error', $message);
        return $this->redirect("counter_receipt/view?id=".$requestparams["counter_receipt_id"]);
        //return sfView::SUCCESS;
      }
      //validate PO is not cancelled
      if($purchase->getStatus()=="Cancelled")
      {
        $message="Purchase Order ".$pono." is already cancelled";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect("counter_receipt/view?id=".$requestparams["counter_receipt_id"]);
        //return sfView::SUCCESS;
      }
      //validate purchase is not yet entered into this counter receipt
      $duplicate=Doctrine_Query::create()
      ->from('CounterReceiptDetail i')
      ->where('i.pono = "'.$pono.'"')
      ->andWhere('i.counter_receipt_id = '.$counter_receipt->getId())
      ->fetchOne();
      if($duplicate)
      {
        $message="Purchase Order ".$pono." is already in this counter receipt ";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect("counter_receipt/view?id=".$requestparams["counter_receipt_id"]);
        //return sfView::SUCCESS;
      }
      
      //validate purchase is not yet paid
      $voucher_total=0;
      foreach($purchase->getVouchers() as $voucher)
      {
        $voucher_total+=$voucher->getAmount();
      }
      $requestparams["amount"]=$purchase->getTotal()-$voucher_total;
      if($requestparams["amount"]==0)
      {
        $message="Purchase Order ".$pono." is already fully paid ";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect("counter_receipt/view?id=".$requestparams["counter_receipt_id"]);
        //return sfView::SUCCESS;
      }


    //validate PO does not yet have a counter receipt
    $cr=$purchase->getCounterReceipt();
    if($cr!=null){
      $message="Sorry, Purchase Order already belongs to Counter Receipt ".$cr->getCode();
      $this->getUser()->setFlash('error', $message);
      return $this->redirect("counter_receipt/view?id=".$requestparams["counter_receipt_id"]);
  }

      $requestparams["purchase_id"]=$purchase->getId();
    //if amount not set, use purchase total
    if($requestparams["invoice_number"]=="")
      $requestparams["invoice_number"]=$purchase->getVendorInvoice();
    if($requestparams["amount"]==0)
      $requestparams["amount"]=$purchase->getTotal();
    
    $form->bind($requestparams);
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $counter_receipt_detail = $form->save();
        $purchase=$counter_receipt_detail->getPurchase();
        $purchase->calc();
        $purchase->save();
        $counter_receipt=$counter_receipt_detail->getCounterReceipt();
        $counter_receipt->calc();
        $counter_receipt->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $counter_receipt_detail)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect("counter_receipt/view?id=".$counter_receipt_detail->getCounterReceiptId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $detail=$this->getRoute()->getObject();
    $purchase=$detail->getPurchase();
    $counter_receipt=$detail->getCounterReceipt();
    
    if ($detail->delete())
    {
      $purchase->calc();
      $purchase->save();
      $counter_receipt->calc();
      $counter_receipt->save();
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("counter_receipt/view?id=".$detail->getCounterReceiptId());
  }
  public function executeAdjust(sfWebRequest $request)
  {
      $id=$request->getParameter('id');

      $this->forward404Unless(
      $this->counter_receipt_detail=Doctrine_Query::create()
      ->from('CounterReceiptDetail i')
      ->where('i.id = '.$id)
      ->fetchOne()
      , sprintf('CounterReceiptDetail with id (%s) not found.', $request->getParameter('id')));

      $oldamount=$this->counter_receipt_detail->getAmount();

      if($request->hasParameter('invoice_number'))
        $this->counter_receipt_detail->setInvoiceNumber($request->getParameter('invoice_number'));

      if($request->hasParameter('amount'))
        $this->counter_receipt_detail->setAmount(str_replace(",","",$request->getParameter('amount')));

      if($request->hasParameter('notes'))
        $this->counter_receipt_detail->setNotes($request->getParameter('notes'));

      $this->counter_receipt_detail->save();

      //if amount changed, recalc parents
      if($oldamount!=$this->counter_receipt_detail->getAmount())
      {
        $purchase=$this->counter_receipt_detail->getPurchase();
        $purchase->calc();
        $purchase->save();
        $counter_receipt=$this->counter_receipt_detail->getCounterReceipt();
        $counter_receipt->calc();
        $counter_receipt->save();
      }

      $this->redirect($request->getReferer());
  }
  public function executeInspect(sfWebRequest $request)
  {
      $this->counter_receipt_detail=Doctrine_Query::create()
      ->from('CounterReceiptDetail i')
      ->where('i.id = '.$request->getParameter('id'))
      ->fetchOne();
      
      $this->counter_receipt_detail->setIsInspected($request->getParameter("value"));
      $this->counter_receipt_detail->save();
      
      $this->redirect($request->getReferer());
  }
}
