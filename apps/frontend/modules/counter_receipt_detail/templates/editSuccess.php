<?php use_helper('I18N', 'Date') ?>
<?php include_partial('counter_receipt_detail/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit Counter receipt detail', array(), 'messages') ?></h1>

  <?php include_partial('counter_receipt_detail/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('counter_receipt_detail/form_header', array('counter_receipt_detail' => $counter_receipt_detail, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('counter_receipt_detail/form', array('counter_receipt_detail' => $counter_receipt_detail, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('counter_receipt_detail/form_footer', array('counter_receipt_detail' => $counter_receipt_detail, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
