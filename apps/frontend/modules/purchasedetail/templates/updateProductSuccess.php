<?php use_helper('I18N', 'Date') ?>
<h1>Purchase Detail Product Update</h1>

<table>
  <tr valign=top>
    <td>
			<table>
				<tr>
					<td>ID</td>

					<td><?php echo $purchase->getId() ?></td>
				</tr>
				<tr>
					<td>Date</td>
					<td><?php echo $purchase->getDate() ?></td>
				</tr>
				<tr>
					<td>PO / Cash Voucher No.</td>
					<td><?php echo $purchase->getPono() ?></td>
				</tr>
				<tr>
					<td>Purchasedetail Id</td>
					<td><?php echo $purchdetail->getId() ?></td>
				</tr>
				<tr>
					<td>Purchasedetail Description</td>
					<td><?php echo $purchdetail->getDescription() ?></td>
				</tr>
			</table>

    </td>
    <td>
    </td>
  </tr>
</table>

Search:				<input id=purchaseproductsearchinput autocomplete="off" size=60 value="<?php 



$desc= $purchdetail->getDescription();
$desc=str_replace("(","",$desc);
$desc=str_replace(")","",$desc);
$desc=str_replace("Mtrs.","-",$desc);
$desc=str_replace(" male","-male",$desc);
$desc=str_replace("degrees","deg",$desc);
$desc=str_replace("with Brass Thread","",$desc);
echo $desc;
?>">
<div id="purchasesearchresult"></div>



<script type="text/javascript">
//set price textbox to read only
//$("#purchasedetail_price").prop('readonly', true);
//set product name
$("#purchaseproductsearchinput").prop('value', $("#product_name").html());
//set price to default price
$("#purchasedetail_price").prop('value', $("#product_min_price").html());
//select purchno
$("#purchase_purchno").focus();
$("#purchase_purchno").select(); 
//if no product id set, disable save button
if($("#purchasedetail_product_id").val()=='')	 		  
  $("#purchase_detail_submit").prop("disabled",true);

//------Purchase (not header) product search-----------
//$("#purchaseproductsearchinput").keyup(function(){
//$("#purchaseproductsearchinput").on('input propertychange paste', function() {
$("#purchaseproductsearchinput").on('keyup', function(event) {
    //product has been edited. disable save button
    $("#purchase_detail_submit").prop("disabled",true);
	//if 3 or more letters in search box
//    if($("#purchaseproductsearchinput").val().length>=3)
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
	    $.ajax({url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/productsearch/updateProductSearch?searchstring="+$("#purchaseproductsearchinput").val()+"&purchasedetail_id=<?php echo $purchdetail->getId() ?>", success: function(result){
	 		  $("#purchasesearchresult").html(result);
	    }});
    //else clear
    else
 		  $("#purchasesearchresult").html("");
});

</script>
