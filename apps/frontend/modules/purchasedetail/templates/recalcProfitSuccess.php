<h1>Recalculate Profit for Purchase Details</h1>
Before, profit calculations did not take discounts into account.
<br>This fixes them.
<?php 
echo form_tag("purchasedetail/recalcProfit"); 
?>
<br>If an error occurs, decrease the interval.
<br>
<br>Interval: <input name=interval value="<?php echo $interval?>">
<br>Start: <input name=start value="<?php echo $start?>" >
<br>
<br><button name=submit onclick="myFunction(); ">START</button>
</form>
<br>
<br>
<?php
foreach($purchasedetails as $purchasedetail)
{
    echo "<br>".$purchasedetail->getId();
}
?>
<script>
var count=<?php echo count($purchasedetails)?>;
//run automatically if there are purchasedetails to process
//stop automatically if there are no more purchasedetails to process
if(count!=0)document.getElementsByName("submit")[0].click();
function myFunction() {
    document.getElementsByName("submit")[0].innerHTML ="Please Wait";
}
    </script>
