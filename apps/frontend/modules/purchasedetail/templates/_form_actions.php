<ul class="sf_admin_actions">
<?php if ($form->isNew()): ?>
<?php else: ?>
  <?php echo $helper->linkToDelete($form->getObject(), array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>
  <?php echo link_to("Back","purchase/view?id=".$form->getObject()->getPurchaseId()) ?>
<?php endif; ?>
</ul>
