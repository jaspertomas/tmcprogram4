<?php

require_once dirname(__FILE__).'/../lib/purchasedetailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/purchasedetailGeneratorHelper.class.php';

/**
 * purchasedetail actions.
 *
 * @package    sf_sandbox
 * @subpackage purchasedetail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class purchasedetailActions extends autoPurchasedetailActions
{
  public function executeNew(sfWebRequest $request)
  {
    $this->purchasedetail = new Purchasedetail();
    $purchase_id=$request->getParameter("purchase_id");
    $this->purchasedetail->setPurchaseId($purchase_id);
    $purchase=$this->purchasedetail->getPurchase();
    $this->purchasedetail->setQty(1);
    $this->purchasedetail->setDiscrate($purchase->getDiscrate());
    $this->form = $this->configuration->getForm($this->purchasedetail);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());

    $isnew=$form->getObject()->isNew();
    
    //if edit, determine if qty or price has been edited
    //if so, processbarcode is necessary
    $qtyorpriceedited=false;
    if(!$isnew)
    {
      if($requestparams['price']!=$form->getObject()->getPrice())$qtyorpriceedited=true;
      elseif($requestparams['qty']!=$form->getObject()->getQty())$qtyorpriceedited=true;
      elseif($requestparams['discrate']!=$form->getObject()->getDiscrate())$qtyorpriceedited=true;
      elseif($requestparams['discamt']!=$form->getObject()->getDiscamt())$qtyorpriceedited=true;
    }
    
    if($requestparams["qty"]==0)
    {
      $message="Invalid Quantity";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    //generate slot and color if new
    if($requestparams['slot']==0 or $requestparams['color']==null)
    {
      list($slot, $color) = PurchasedetailTable::getNextSlotAndColor($requestparams['$product_id']);
      $requestparams['slot']=$slot;
      $requestparams['color']=$color;
    }

    $form->bind($requestparams);
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $purchasedetail = $form->save();
        $purchasedetail->setBarcode(str_pad($purchasedetail->getId(),10,"0",STR_PAD_LEFT)."D");
        $purchasedetail->calcRemaining();  
        if($qtyorpriceedited)
          $this->processBarcode($purchasedetail,$isnew);
        //$purchasedetail->updateStockentry();
        $purchasedetail->updateProduct();
        $purchasedetail->updateProfitdetail();
        
        //custom calculation
        if($purchasedetail->getDescription()=="")
          $purchasedetail->setDescription($purchasedetail->getProduct()->getDescription());
        $purchasedetail->calc();
        $purchasedetail->save();
        $purchase=$purchasedetail->getPurchase();
        $purchase->calc();
        $purchase->save();
        
        ////create purchaseconversion if necessary, 
        ////allowing conversion from parts to finished product in dr
        //look for the conversion that this product belongs to, if any
        $conversiondetails=$purchasedetail->getProduct()->getConversiondetail();
        $conversion=null;
        foreach($conversiondetails as $cd)
        {
          $conversion=$cd->getConversion();

          //if conversion exists (if product belongs to a conversion)
          //and the purchase does not have a purchaseconversion for it yet
          //create
          if($conversion)
          {
            $purchaseconversion=Doctrine_Query::create()
            ->from('Purchaseconversion dc')
            ->where('dc.purchase_id = '.$purchase->getId())
            ->andWhere('dc.conversion_id = '.$conversion->getId())
            ->fetchOne();
            //if $purchaseconversion does not exist
            if($purchaseconversion==null)
            {
              //create
              $purchaseconversion=new Purchaseconversion();
              $purchaseconversion->setPurchaseId($purchase->getId());
              $purchaseconversion->setConversionId($conversion->getId());
              $purchaseconversion->setQty(0);
              $purchaseconversion->save();
            }
          }
        }

      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $purchasedetail)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');
        $this->redirect('@purchasedetail_new?purchase_id='.$purchase->getId());
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect('purchase/view?id='.$purchasedetail->getPurchaseId());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  private function processBarcode($purchdetail,$isnew)
  {
     $pds=Doctrine_Query::create()
        ->from('Profitdetail pd')
      	->where('pd.purchasedetail_id = '.$purchdetail->getId())
      	->orderBy('pd.id desc')
      	->execute();
  	  foreach($pds as $pd)
  	  {
  	      $invdetail=$pd->getInvoicedetail();
  	  
         //give back qty in profitdetail to invdetail and purchdetail
         $purchdetail->setRemaining($purchdetail->getRemaining()+$pd->getQty());
         $invdetail->setRemaining($invdetail->getRemaining()+$pd->getQty());
		     
         //process barcode / create new profitdetail
         //calculate qty to process
         //it's purchdetail->remaining or invdetail->remaining, whichever is lower
	       $processed=$purchdetail->getRemaining();
	       if($processed>$invdetail->getRemaining())$processed=$invdetail->getRemaining();
	       if($processed<0)$processed=0;

         //give qty to process from invdetail and purchdetail to profitdetail 
         $pd->setQty($processed);
		     $purchdetail->setRemaining($purchdetail->getRemaining()-$pd->getQty());
	       $invdetail->setRemaining($invdetail->getRemaining()-$pd->getQty());
         //if remaining=0, profitcalcluated=1
		     $invdetail->setIsProfitcalculated($invdetail->getRemaining()==0?1:0);
	       $invdetail->save();
	       //end of invdetail calculation

		     $pd->setProductId($invdetail->getProductId());
		     $pd->setInvoiceId($invdetail->getInvoiceId());
		     $pd->setPurchaseId($purchdetail->getPurchaseId());
		     $pd->setInvoicedetailId($invdetail->getId());
		     $pd->setPurchasedetailId($purchdetail->getId());
		     $pd->setInvoicedate($invdetail->getInvoice()->getDate());
		     $pd->setPurchasedate($purchdetail->getPurchase()->getDate());
		     $buyprice=$purchdetail->getTotal()/$purchdetail->getQty();//buyprice per unit
		     $sellprice=$invdetail->getTotal()/$invdetail->getQty();//sellprice per unit
		     $pd->setSellprice($sellprice);
		     $pd->setBuyprice($buyprice);
		     $pd->setProfitperunit($sellprice-$buyprice);
		     if($buyprice!=0)
			     $pd->setProfitrate(($sellprice/$buyprice)-1);
		     //else buyprice per unit is 0 - I got it for free
		     else
			     $pd->setProfitrate("100");//avoid divide by 0
		     $pd->setProfit($pd->getProfitperunit()*$processed);
		     $pd->save();
    }
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $purchase=$this->getRoute()->getObject()->getPurchase();
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($this->getRoute()->getObject()->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $purchase->calc();
    $purchase->save();
    $this->redirect('purchase/view?id='.$purchase->getId());
  }
  public function executeUpdateProduct(sfWebRequest $request)
  {
     $this->purchdetail=Doctrine_Query::create()
        ->from('Purchasedetail pd')
      	->where('pd.product_id = 4')//uncomment this on prod
      	->orderBy('pd.id desc')
      	->fetchOne();
    	$this->detailform=new PurchasedetailForm($this->purchdetail);
      if($this->purchdetail==null)
      {
        $this->redirect('home/error?msg='."no more records to process");
      }
    	$this->purchase=$this->purchdetail->getPurchase();
  }
  public function executeProcessUpdateProduct(sfWebRequest $request)
  {
     $product=Doctrine_Query::create()
        ->from('Product p')
      	->where('p.id = '.$request->getParameter("product_id"))
      	->fetchOne();
     $purchdetail=Doctrine_Query::create()
        ->from('Purchasedetail pd')
      	->where('pd.id = '.$request->getParameter("purchasedetail_id"))
      	->fetchOne();
      	
  	//update purchasedetails that match description of selected purchasedetail 
      $products= Doctrine_Query::create()
        ->update('Purchasedetail pd')
        ->set('pd.product_id',$product->getId())
        ->where('pd.description = ?', array($purchdetail->getDescription()))
        ->execute();

    $note=new Note();
    $note->setName($product->getId());
    $note->setDescription($purchdetail->getDescription());
    $note->save();

    $this->redirect($request->getReferer());
  }
  public function executeSetPriceByTotal(sfWebRequest $request)
  {
    $request->setParameter('value',$request->getParameter('total')/$request->getParameter('qty'));
    $this->executeSetPrice($request);
  }
  public function executeSetPrice(sfWebRequest $request)
  {
      if($request->getParameter('value')<=0)
      {
        $message="Invalid price";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
  
      $this->detail=Doctrine_Query::create()
      ->from('Purchasedetail id')
      ->where('id.id = '.$request->getParameter('id'))
      ->fetchOne();

      $conn = Doctrine_Manager::connection();
      try {
        $conn->beginTransaction();
        $this->detail->setPrice(str_replace(",","",$request->getParameter('value')));
        $this->detail->calc();
        $this->detail->save($conn);
        $this->detail->updateProduct();
        $this->detail->updateProfitdetail();
        $this->detail->getPurchase()->calc();
        $this->detail->getPurchase()->save($conn);
        $this->detail->updateProfitdetail($conn);
        $conn->commit();
      } catch (Exception $e) {
        if ($conn)$conn->rollback();
        $this->getUser()->setFlash('error', "Error updating price");
     }

      //todo: auto calc vat entry here

      $this->redirect($request->getReferer());
  }
  public function executeSetQty(sfWebRequest $request)
  {
      /*
      //prevent invalid qty of 0
      if($request->getParameter('value')==0)
      {
        $message="Invalid qty";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
      }
      */
      //if allow zero qty, uncomment this
      ///*
        if($request->getParameter('value')==0)
        {
          $message="Warning: Zero Quantity";
          $this->getUser()->setFlash('error', $message);
        }
      //*/
  
      //fetch purchasedetail
      $this->detail=Doctrine_Query::create()
      ->from('Purchasedetail id, id.Purchase i')
      ->where('id.id = '.$request->getParameter('id'))
      ->fetchOne();

      $this->detail->setQty($request->getParameter('value'));
      $this->detail->calc();
      $this->detail->calcRemaining();
      $this->detail->save();
      $this->detail->getPurchase()->calc();
      $this->detail->getPurchase()->save();

      $this->redirect($request->getReferer());
  }
  public function executeSetQtyReceived(sfWebRequest $request)
  {
      $this->detail=Doctrine_Query::create()
      ->from('Purchasedetail id, id.Purchase i')
      ->where('id.id = '.$request->getParameter('id'))
      ->fetchOne();

      $this->detail->setQtyReceived($request->getParameter('value'));
      $this->detail->calc();
      $this->detail->save();
      $this->detail->getPurchase()->calc();
      $this->detail->getPurchase()->save();
      $this->detail->updateStockentry();
      
      $purchase=$this->detail->getPurchase();
      $purchase->calcReceivedStatus();
      $purchase->save();

      $this->redirect($request->getReferer());
  }
  public function executeInspect(sfWebRequest $request)
  {
      $this->purchasedetail=Doctrine_Query::create()
      ->from('Purchasedetail i')
      ->where('i.id = '.$request->getParameter('id'))
      ->fetchOne();
      
      $this->purchasedetail->setIsInspected($request->getParameter("value"));
      $this->purchasedetail->save();
      
      $this->redirect($request->getReferer());
  }
  public function executeRegenStockentry(sfWebRequest $request)
  {
  	die("ACCESS DENIED");

      //default values
    $this->interval=100;
    $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->purchasedetails=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->purchasedetails = Doctrine_Query::create()
      ->from('Purchasedetail p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();
    $se_raw = Doctrine_Query::create()
      ->from('Stockentry se')
      ->where('se.ref_class="Purchasedetail"')
      ->andWhere('se.ref_id <='.$this->end)
      ->andWhere('se.ref_id >='.$this->start)
      ->execute();
    $this->stockentries=array();
    foreach($se_raw as $se)
      $this->stockentries[$se->getRefId()]=$se;

    foreach($this->purchasedetails as $pd)
    {
      if(!isset($this->stockentries[$pd->getId()]))
      {
        $purchase=$pd->getPurchase();
        if($purchase!=null and $purchase->getStatus()!="Cancelled")
        {
          $pd->updateStockentry();
          echo "regenerated: ".$pd->getId()."<br>";
        }
      }
    }

     $this->start=$this->end+1;
  }
  public function executeRecalcProfit(sfWebRequest $request)
  {
  	// die("ACCESS DENIED");

      //default values
    $this->interval=100;
    $this->start=1;

    //if method = get
    if(!isset($_REQUEST["submit"]))
    {
      $this->purchasedetails=array();  
     return;
    }
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start+$this->interval-1;
    
     $this->purchasedetails = Doctrine_Query::create()
      ->from('Purchasedetail p')
      ->where('p.id <='.$this->end)
      ->andWhere('p.id >='.$this->start)
      ->execute();

    foreach($this->purchasedetails as $pd)
    {
      $pd->updateProfitdetail();
    }

     $this->start=$this->end+1;
  }
  public function executeView(sfWebRequest $request)
  {
    $item=Fetcher::fetchOne("Purchasedetail",array("id"=>$request->getParameter("id")));
    return $this->redirect("purchase/view?id=".$item->getPurchaseId());
  }
}
