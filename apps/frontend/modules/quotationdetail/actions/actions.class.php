<?php

require_once dirname(__FILE__).'/../lib/quotationdetailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/quotationdetailGeneratorHelper.class.php';

/**
 * quotationdetail actions.
 *
 * @package    sf_sandbox
 * @subpackage quotationdetail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class quotationdetailActions extends autoQuotationdetailActions
{
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter("quotationdetail");

    $isnew=$form->getObject()->isNew();
    
    //if edit, determine if qty or price has been edited
    //if so, processbarcode is necessary
    $qtyorpriceedited=false;
    if(!$isnew)
    {
      if($requestparams['price']!=$form->getObject()->getPrice())$qtyorpriceedited=true;
      elseif($requestparams['qty']!=$form->getObject()->getQty())$qtyorpriceedited=true;
      elseif($requestparams['discrate']!=$form->getObject()->getDiscrate())$qtyorpriceedited=true;
      elseif($requestparams['discamt']!=$form->getObject()->getDiscamt())$qtyorpriceedited=true;
    }
    
    if(!isset($requestparams['is_discounted']))$requestparams['is_discounted']=false;
    
    //is_discounted is checkbox. if it is false, it needs to be created explicitly    
    if(!$requestparams["is_discounted"])$requestparams["is_discounted"]=0;
    else $requestparams["is_discounted"]=1;

    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $quotationdetail = $form->save();
        $quotationdetail->calc();
        $quotationdetail->save();
        $quotation=$quotationdetail->getQuotation();
        $quotation->calc();
        $quotation->save();
        
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $quotationdetail)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect("quotation/view?id=".$quotationdetail->getQuotationId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
    public function executeSetPrice(sfWebRequest $request)
    {
        if($request->getParameter('value')<=0)
        {
          $message="Invalid price";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
    
        $this->detail=Doctrine_Query::create()
        ->from('Quotationdetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $product=$this->detail->getProduct();

        //if value is below minimum selling price
        //and is_discounted is false
        //show error
        if($request->getParameter('value')<$product->getMinSellPrice() and $this->detail->getIsDiscounted()==0)
        {
            $message="Sorry, price too low. Minimum price for ".$product->getName()." is ".$product->getMinSellPrice();
            $this->getUser()->setFlash('error', $message);
            return $this->redirect($request->getReferer());
        }

        $this->detail->setPrice($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $this->detail->getQuotation()->calc();
        $this->detail->getQuotation()->save();
	      //$this->detail->updateProfitdetail();
        
        //todo: auto calc vat entry here

        $this->redirect($request->getReferer());
    }
    public function executeSetQty(sfWebRequest $request)
    {
        if($request->getParameter('value')==0)
        {
          $message="Invalid qty";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }
    
        $this->detail=Doctrine_Query::create()
        ->from('Quotationdetail id, id.Quotation i')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setQty($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $this->detail->getQuotation()->calc();
        $this->detail->getQuotation()->save();

        $this->redirect($request->getReferer());
    }
    public function executeSetDiscrate(sfWebRequest $request)
    {
        //reject anything except numbers and spaces
        $pattern = '/^[0-9 ]+$/';
        if ( !preg_match ($pattern, $request->getParameter('value')) and $request->getParameter('value')!="")
        {
          $message="Invalid discount rate";
          $this->getUser()->setFlash('error', $message);
          return $this->redirect($request->getReferer());
        }    
    
        $this->detail=Doctrine_Query::create()
        ->from('Quotationdetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setDiscrate($request->getParameter('value'));
        $this->detail->calc();
        $this->detail->save();
        $this->detail->getQuotation()->calc();
        $this->detail->getQuotation()->save();

        $this->redirect($request->getReferer());
    }
    public function executeSetDescription(sfWebRequest $request)
    {
        $this->detail=Doctrine_Query::create()
        ->from('Quotationdetail id')
        ->where('id.id = '.$request->getParameter('id'))
        ->fetchOne();

        $this->detail->setDescription($request->getParameter('value'));
        $this->detail->save();

        $this->redirect($request->getReferer());
    }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($this->getRoute()->getObject()->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('@quotationdetail');
  }
}
