<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
  <?php echo form_tag_for($form, '@event') ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

  <?php if($event->getType()=="CashCollect"){?>
    <table>
      <tr>
        <th><label for="date">Date</label></th>
        <td><?php echo $form['date']->render()?></td>
      </tr>
      <tr>
        <th><label for="amount">Amount</label></th>
        <td><?php echo $form['amount']->render()?></td>
      </tr>
      <tr>
        <th><label for="notes">Notes</label></th>
        <td><?php echo $form['notes']->render()?></td>
      </tr>
    </table>
  <?php }elseif($event->getType()=="ChequeCollect"){ ?>
    <table>
      <tr>
        <th><label for="date">Date</label></th>
        <td><?php echo $form['date']?></td>
      </tr>
      <tr>
        <th><label for="amount">Amount</label></th>
        <td><?php echo $form['amount']?></td>
      </tr>
      <tr>
        <th><label for="detail1">Cheque No.</label></th>
        <td><?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
      </tr>
      <tr>
        <th><label for="checkcleardate">Cheque Clear Date</label></th>
        <td><?php echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
      </tr>
      <tr>
        <th><label for="notes">Notes</label></th>
        <td><?php echo $form['notes']?></td>
      </tr>
    </table>
  <?php } ?>
  
    <?php include_partial('event/form_actions', array('event' => $event, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </form>
</div>
