<?php

require_once dirname(__FILE__).'/../lib/counter_receiptGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/counter_receiptGeneratorHelper.class.php';

/**
 * counter_receipt actions.
 *
 * @package    sf_sandbox
 * @subpackage counter_receipt
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class counter_receiptActions extends autoCounter_receiptActions
{
  public function executeNew(sfWebRequest $request)
  {
    $this->counter_receipt = new CounterReceipt();
    $this->counter_receipt->setCode(CounterReceiptTable::genCode());
    $this->counter_receipt->setDate(MyDate::today());

    //set vendor id if param vendor_id
    if($request->hasParameter("vendor_id"))
    {
      $this->counter_receipt->setVendorId($request->getParameter("vendor_id"));
    }
    
    $this->form = $this->configuration->getForm($this->counter_receipt);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());
    $code=$requestparams["code"];

    $duplicate=Doctrine_Query::create()
    ->from('CounterReceipt i')
    ->where('i.code = "'.$code.'"')
    ->fetchOne();
    if($duplicate!=null)
    {
      $message="Counter Receipt with code ".$code." already exists";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    $form->bind($requestparams, $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $counter_receipt = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $counter_receipt)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect("counter_receipt/view?id=".$counter_receipt->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  public function executeView(sfWebRequest $request)
  {
    $this->forward404Unless(
      $this->counter_receipt=Doctrine_Query::create()
        ->from('CounterReceipt cr')
      	->where('id = '.$request->getParameter('id'))
      	->fetchOne()
  	, sprintf('Counter Receipt with id (%s) not found.', $request->getParameter('id')));

    #new counter receipt detail form
    $this->detail = new CounterReceiptDetail();
    $this->detail->setCounterReceiptId($this->counter_receipt->getId());
    $this->detail->setVendorId($this->counter_receipt->getVendorId());
    if($request->getParameter("product_id"))
    {
      $detail->setProductId($request->getParameter("product_id"));
      $this->product=$detail->getProduct();
      $this->product_is_set=true;
    }
    $this->form = new CounterReceiptForm($this->counter_receipt);
    $this->detailform = new CounterReceiptDetailForm($this->detail);
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);
    $this->message=$request->getParameter("message");

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeAdjust(sfWebRequest $request)
  {
      $requestparams=$request->getParameter('counter_receipt');
      $id=$requestparams['id'];
  
      $this->forward404Unless(
      $this->counter_receipt=Doctrine_Query::create()
      ->from('CounterReceipt i')
      ->where('i.id = '.$id)
      ->fetchOne()
      , sprintf('CounterReceipt with id (%s) not found.', $request->getParameter('id')));

      if(isset($requestparams["cheque_number"]))
        $this->counter_receipt->setChequeNumber($requestparams['cheque_number']);

      if(isset($requestparams["cheque_date"]))
        $this->counter_receipt->setChequeDate($requestparams['cheque_date']);

      if(isset($requestparams["amount"]))
        $this->counter_receipt->setAmount(str_replace(",","",$requestparams['amount']));

      if(isset($requestparams["code"]))
        $this->counter_receipt->setCode($requestparams['code']);

      if(isset($requestparams["notes"]))
        $this->counter_receipt->setNotes($requestparams['notes']);

      $this->counter_receipt->save();

      $this->redirect($request->getReferer());
  }
  /*
  public function executeChooseVendor(sfWebRequest $request)
  {
    $this->forward404Unless(
    $this->counter_receipt=Doctrine_Query::create()
        ->from('CounterReceipt i, i.Vendor c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('CounterReceipt with id (%s) not found.', $request->getParameter('id')));
  	
	}
	*/
  public function executeProcessChooseVendor(sfWebRequest $request)
  {
    $this->forward404Unless(
    $counter_receipt=Doctrine_Query::create()
        ->from('CounterReceipt i, i.Vendor c')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('CounterReceipt with id (%s) not found.', $request->getParameter('id')));

    //fetch vendor name or id
  	$vendor_name=null;
  	$vendor_id=null;
    if($request->getParameter("vendor_name"))
      $vendor_name=$request->getParameter("vendor_name");
    elseif($request->getParameter("vendor_id"))
      $vendor_id=$request->getParameter("vendor_id");
    else
      return $this->redirect("home/error?msg="."No vendor specified");

    //vendor name supplied, potential new vendor
    //search for vendor,
    //if non-existent, create
    $vendor=null;
    if($vendor_name)
    {
      $vendor=Doctrine_Query::create()
        ->from('Vendor c')
      	->where('c.name = "'.$vendor_name.'"')
      	->fetchOne();
    	if(!$vendor)
    	{
    	  $vendor=new Vendor();
    	  $vendor->setName($vendor_name);
    	  $vendor->save();
    	}
    }
    else if($vendor_id)
    {
      $vendor=Doctrine_Query::create()
        ->from('Vendor c')
      	->where('c.id = '.$vendor_id)
      	->fetchOne();
    }

    $counter_receipt->setVendorId($vendor->getId());
    $counter_receipt->save();
    $this->redirect("counter_receipt/view?id=".$request->getParameter('id'));
	}
  public function executeFiles(sfWebRequest $request)
  {
    $this->counter_receipt=MyModel::fetchOne("CounterReceipt",array('id'=>$request->getParameter("id")));
    $this->file=new File();
    $this->file->setParentClass('counter_receipt');
    $this->file->setParentId($this->counter_receipt->getId());
    $this->form=new FileForm($this->file);

    $this->files=Doctrine_Query::create()
      ->from('File f')
      ->where('f.parent_class="counter_receipt"')
      ->andWhere('f.parent_id='.$this->counter_receipt->getId())
      ->execute();
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    $counter_receipt=$this->getRoute()->getObject();
    if ($counter_receipt->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('@counter_receipt');
  }
  public function executeQuickInput(sfWebRequest $request)
  {
		//this will only handle inputstring and parsing. 
		//All other functions will be delegated.

		//---initialize vars-------------------------------------------------------------------------------
		$this->linestrings=array();
		$this->linesegments=array();
		$this->inputstring="";
		$this->errors=array();
		$this->warnings=array();
		$this->messages=array();
		$this->generate=false;
    $this->crs=array();
    $this->pos=array();
    //---------case 2: method=get, init vars but do nothing: [ok]---------
  	//show empty
		if($request->getMethod()=="GET")
    {
      $this->inputstrings="";
    }
  	//---------case 3: method=post: process data ---------
		else
		{
//	    $this->inputstring=$request->getParameter("inputstring");
//			$this->parse();
	    $this->inputstrings=$request->getParameter("inputstring");
      foreach(explode("---",$this->inputstrings) as $this->inputstring)
      {	    
			$this->parse();

			//test parsed data
			//var_dump($this->linesegments);

      //processing:
      $cr=null;
      $vendor=null;
      $details=array();
      $purchasetotal=0;
      foreach($this->linesegments as $row)
      {
        //if row 0 has data, this row contains counter receipt data
        //process counter receipt data
        if($row[0]!="")
        {
          if($row[0]=="")
            $this->errors[]="Vendor Name is required";
          if($row[1]=="")
            $this->errors[]="Counter Receipt Code is required";

          $vendorname=$row[0];
          $vendor = Doctrine_Query::create()
            ->from('Vendor v')
            ->where("v.name = '".$vendorname."'")
            ->fetchOne();
          if($vendor==null)
          {
            $this->errors[]="Vendor ".$vendorname." does not exist.";
            break;
          }
          else
          {
            $cr=new CounterReceipt();
            $cr->setVendorId($vendor->getId());
            $cr->setCode($row[1]);
            $cr->setChequeNumber($row[2]);
            $cr->setAmount(str_replace(",","",$row[3]));

          }// end if($vendor==null)
        }// end if($row[0]!="")

        //save receiptdetail data
        if(!isset($row[4]))$row[4]="";
        if(!isset($row[5]))$row[5]="";
        if(!isset($row[6]))$row[6]="";
        if(!isset($row[7]))$row[7]="";
        if(!isset($row[8]))$row[8]="";
        
        if($row[4]=="")
        {
          $this->errors[]="Invoice number is required";
        }


        $pono=$row[6];
        $purchase = Doctrine_Query::create()
          ->from('Purchase p')
          ->where('p.pono ="'.$pono.'"')
          ->fetchOne();

        $detail=new CounterReceiptDetail();
        if($vendor!=null)
          $detail->setVendorId($vendor->getId());
        if($vendor!=null)
          $detail->setCounterReceiptId($cr->getId());
        if($purchase!=null)
        {
          if($purchase->getStatus()=="Paid")
          {
            $this->errors[]="PO ".$pono." is already paid.";
            $this->pos[]=$purchase;
            //break;
          }
        
          $detail->setPurchaseId($purchase->getId());
          $detail->setInvoiceNumber($row[4]);
          $detail->setNotes($row[5]." ".$row[7]);
          $detail->setPono($pono);
          $detail->setAmount($purchase->getTotal());
          $detail->setIsTally(1);
          $purchasetotal+=$detail->getAmount();
        }
        else
        {
          $this->warnings[]="Purchase ".$pono." does not exist.";
          //$detail->setPurchaseId($purchase->getId());
          $detail->setInvoiceNumber($row[4]);
          $detail->setNotes($row[5]." ".$row[7]);
          $detail->setPono($pono);
          $detail->setAmount(str_replace(",","",$row[8]));
          $detail->setIsTally(1);
          $purchasetotal+=$detail->getAmount();
        }
        $details[]=$detail;

      }


      //no errors found
      //save mode - save all records
      if(count($this->errors)==0)
      {

        $conn = Doctrine_Manager::connection();
        try {
          //reversable database transaction
          $conn->beginTransaction();
          //if po total does not match cheque amount, set as not tally
          if($purchasetotal==$cr->getAmount())
            $cr->setIsTally(1);
          else
          {
            $this->warnings[]="CR ".$cr->getCode()." does not tally";
            $cr->setIsTally(0);
            $cr->setNotes($cr->getNotes()."; purchase total: ".$purchasetotal."; diff: ".($purchasetotal-$cr->getAmount()));
          }
          //save counter receipt
          $cr->save($conn);
          //save counter receipt details
          foreach ($details as $detail){
            $detail->setCounterReceiptId($cr->getId());
            $detail->save($conn);
          }
          $this->crs[]=$cr;


          //set related po as paid
          foreach ($details as $detail){
            if($detail->getPurchaseId()!=null)
            {
              $purchase=$detail->getPurchase();
              $purchase->setStatus("Paid");
              $purchase->setCounterReceiptDetailId($detail->getId());
              $purchase->setCounterReceiptId($cr->getId());
              $purchase->setMemo($purchase->getMemo()."; Paid under Counter Receipt ".$cr->getCode());
              $purchase->save($conn);
            }
          }
          $conn->commit();
          $this->messages[]="Counter Receipt created.";
        } catch (Exception $e) {
          if ($conn)
            $conn->rollback();
          $this->errors[]="Error saving counter receipt: ".$e->getMessage();
        }
      }
      
      if(count($this->errors)!=0)break;
      }// end foreach($this->inputstrings as $this->inputstring)
		}
	}	
	private function parse()
	{
		$this->linestrings=explode("\n",$this->inputstring);
		$this->linesegments=array();
		foreach($this->linestrings as $linestring)
		{
		  //ignore empty line
		  if(trim($linestring)=="")continue;
			$cellarray=explode("\t",$linestring);
			//trim each cell
			foreach($cellarray as &$cell)
				$cell=trim($cell);
			$this->linesegments[]=$cellarray;
		}
	}
  public function executeInspect(sfWebRequest $request)
  {
      $this->counter_receipt=Doctrine_Query::create()
      ->from('CounterReceipt i')
      ->where('i.id = '.$request->getParameter('id'))
      ->fetchOne();
      
      $value=$request->getParameter("value");
      
      $this->counter_receipt->setIsInspected($value);
      $this->counter_receipt->save();
      
      //update counter_receipt details
      Doctrine_Query::create()
        ->update('CounterReceiptDetail d')
        ->set('d.is_inspected',$value)
        ->where('d.counter_receipt_id = '.$this->counter_receipt->getId())
        ->execute();
 
      if($value==1)
        $this->counter_receipt->setIsTally(1);
      else
        $this->counter_receipt->setIsTally(0);
      $this->counter_receipt->save();
      
      $this->redirect($request->getReferer());
  }
  public function executePayments(sfWebRequest $request)
  {
    $this->forward404Unless(
    $this->counter_receipt=Doctrine_Query::create()
        ->from('CounterReceipt cr, cr.Vendor v')
      	->where('cr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('counter_receipt with id (%s) not found.', $request->getParameter('id')));
    $this->form = $this->configuration->getForm($this->counter_receipt);
  }
  public function executeRecalc(sfWebRequest $request)
  {
    $this->forward404Unless(
    $this->counter_receipt=Doctrine_Query::create()
        ->from('CounterReceipt cr, cr.Vendor v')
      	->where('cr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('counter_receipt with id (%s) not found.', $request->getParameter('id')));
    $this->counter_receipt->calc(); //force recalculate purchase orders too
    $this->counter_receipt->save();
    $this->redirect($request->getReferer());
  }
  public function executeCheckExists(sfWebRequest $request)
  {
    $cr=Fetcher::fetchOne("CounterReceipt",array("code"=>$request->getParameter("code")));
    if($cr!=null)
      echo json_encode(array("id"=>$cr->getId(), "total"=>$cr->getTotal(), "customer"=>$cr->getVendor()->getName()));
    else 
      echo "";
    die();
  }
}
