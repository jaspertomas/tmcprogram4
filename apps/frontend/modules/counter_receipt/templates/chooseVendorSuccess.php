<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
	Counter Receipt: Choose Vendor 
</h1>
<?php slot('transaction_id', $counter_receipt->getId()) ?>
<?php slot('transaction_type', "CounterReceipt") ?>

<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Code</td>
          <td>
            <?php echo $counter_receipt->getCode(); ?>
          </td>
        </tr>
        <tr>
          <td>Date</td>
          <td><?php echo $counter_receipt->getDate() ?></td>
        </tr>
        <!--tr>
          <td>PO No.</td>
          <td><?php //echo $counter_receipt->getPonumber() ?></td>
        </tr-->
        <tr>
          <td>Vendor</td>
          <td><?php echo link_to($counter_receipt->getVendor(),"vendor/view?id=".$counter_receipt->getVendorId(),array("target"=>"edit_vendor")); ?></td>
        </tr>
		</table>
    <td>
      <table>
		    <tr>
          <td>Total</td>
          <td><?php echo $counter_receipt->getAmount() ?></td>
        </tr>
      </table>
    </td>
    <td>
			<table>
        <tr>
          <td>Notes: </td>
          <td><?php echo $counter_receipt->getNotes() ?></td>
        </tr>
		  </table>
    </td>
  </tr>
  <tr hidden=true id=counter_receipt_edit_password_tr class=password_tr >
    <td align=center colspan=10>Enter manager password:<input id=counter_receipt_edit_password_input type=password><input type=button value="Submit Password" id=counter_receipt_edit_password_button></td>
  </tr> 
</table>
<hr>
<?php
//if user is salesman or encoder
//if($sf_user->hasCredential(array('admin', 'sales', 'encoder'), false)){?>

Search Vendor: <input id=counter_receiptvendorsearchinput autocomplete="off" size=12> 
<div id="counter_receiptsearchresult"></div>

<?php echo form_tag('counter_receipt/processChooseVendor')?> 



<?php //} ?>
<script>
$("#counter_receiptvendorsearchinput").focus();
$("#counter_receiptvendorsearchinput").keyup(function(event){
	//if 3 or more letters in search box
    //if($("#vendorsearchinput").val().length>=3){
    
    //if enter key is pressed
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
	    $.ajax(
	      {
	        url: "<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/vendor/search?searchstring="+$("#counter_receiptvendorsearchinput").val()+"&counter_receipt_id="+<?php echo $counter_receipt->getId()?>, 
	        success: function(result){
	 		  $("#counter_receiptsearchresult").html(result);
	    }});
    }
});

</script>
