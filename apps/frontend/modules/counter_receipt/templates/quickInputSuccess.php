<?php use_helper('I18N', 'Date') ?>
<h1>Counter Receipt Quick Input</h2>

<font color=red>
<?php foreach($errors as $error)echo $error."<br>"; ?>
</font>
<font color=magenta>
<?php foreach($warnings as $warning)echo $warning."<br>"; ?>
</font>
<font color=green>
<?php foreach($messages as $message)echo $message."<br>"; ?>
</font>
<?php foreach($crs as $cr)echo link_to("CR ".$cr->getCode(),"counter_receipt/view?id=".$cr->getId())."<br>"; ?>
<?php foreach($pos as $po)echo link_to("PO ".$po->getPono(),"purchase/view?id=".$po->getId())."<br>"; ?>

<?php echo form_tag("counter_receipt/quickInput")?>

Paste from spreadsheet:
<br>
<input name=submit type=submit value="Save">

<br><textarea rows=15 cols=80 name=inputstring ><?php echo $inputstrings?></textarea>
</form>

<hr>
<h3>Sample Input:</h3>
Paste this into a spreadsheet
<br><textarea cols=80 rows=5>
supplier	cr_code	cheque	amt	invno	notes	po	notes	amt
Colossal Merchandising Corporation	2254	1193032	329559.4	12656		3393		
				12546		3298	24700	
				12548		3298b	28880	28880

</textarea>

<br>* There are lots of times when PO price is based on currently known buy prices and are wrong (different from sales invoice price).
<br>In these cases, the actual price is put into the PO notes column, to later be used to update the po and product buy price at the user's discretion
<br>Perhaps in the future, it will be better to have automatic updating of po price 
<br>
<br>* Supplier name should be exact
