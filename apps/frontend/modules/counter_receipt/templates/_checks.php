<table border=1>
<tr valign=bottom>
  <th>Receive<br>Date</th>
  <th>Amount</th>
  <th>Remaining</th>
  <th>Check<br>Voucher</th>
  <th>Check No.</th>
  <th>Check<br>Date</th>
  <th>Pay From</th>
  <th>Pay For</th>
</tr>

<?php foreach($vendor->getChecks() as $check){ 
  $out_payments=$check->getOutPayment();
  $particulars=array();
  foreach($out_payments as $e)
  {
    if(!isset($particulars[$e->getParentName()]))
    $particulars[$e->getParentName()]=link_to($e->getParent(),"counter_receipt/payments?id=".$e->getParentId());
  }
?>
<tr>
    <td><?php echo MyDateTime::frommysql($check->getReceiveDate())->toShortDate()?></td>
    <td align=right><?php echo number_format ( $check->getAmount(),2 )?></td>
    <td align=right><?php echo number_format ( $check->getRemaining(),2 )?></td>
    <td><?php echo link_to($check->getCode(),"out_check/view?id=".$check->getId())?></td>
    <td><?php echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
    <td><?php echo MyDateTime::frommysql($check->getCheckDate())->toShortDate()?></td>
    <td><?php echo $check->getPassbook()?></td>
    <td><?php echo implode(", ",$particulars)?></td>
    <td><?php echo link_to('Edit','out_check/edit?client_class=CounterReceipt&client_id='.$counter_receipt->getId().'&id='.$check->getId()) ?>

      </td>
    <td>
  <?php echo link_to(
    'Delete',
    'out_check/delete?id='.$check->getId(),
    array('method' => 'delete', 'confirm' => 'Are you sure?')
  ) ?>

      </td>
  <?php } ?>
</tr>
</table>
