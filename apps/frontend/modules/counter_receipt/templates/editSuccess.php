<?php use_helper('I18N', 'Date') ?>
<?php include_partial('counter_receipt/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit Counter Receipt for '.$form->getObject()->getVendor(), array(), 'messages') ?></h1>

  <?php include_partial('counter_receipt/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('counter_receipt/form_header', array('counter_receipt' => $counter_receipt, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('counter_receipt/form', array('counter_receipt' => $counter_receipt, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('counter_receipt/form_footer', array('counter_receipt' => $counter_receipt, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
