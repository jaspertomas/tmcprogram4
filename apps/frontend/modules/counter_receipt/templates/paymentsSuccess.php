<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
	Counter Receipt <?php echo $counter_receipt->getCode(); ?> Payments <?php echo link_to("(Back)","counter_receipt/view?id=".$counter_receipt->getId())?>
</h1>
<?php slot('transaction_id', $counter_receipt->getId()) ?>
<?php slot('transaction_type', "CounterReceipt") ?>

<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($counter_receipt->getDate())->toshortdate() ?></td>
        </tr>
        <!--tr>
          <td>PO No.</td>
          <td><?php //echo $counter_receipt->getPonumber() ?></td>
        </tr-->
        <tr>
          <td>Supplier</td>
          <td><?php echo link_to($counter_receipt->getVendor(),"vendor/view?id=".$counter_receipt->getVendorId(),array("target"=>"edit_vendor"))." (".$counter_receipt->getVendor()->getTinNo().")"; ?></td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td>Total</td>
          <td><b><?php echo $counter_receipt->getAmount() ?></b></td>
        </tr>
        <tr>
          <td>Balance</td>
          <td><b><?php echo $counter_receipt->getBalance() ?></b></td>
        </tr>
        <tr>
	        <td><b>Status</b></td>
	        <td>
            <font <?php 
            
            
            switch($counter_receipt->getStatus())
            {
              case "Paid":echo "color=blue";break;
              case "Pending":echo "color=red";break;
              case "Cancelled":echo "color=red";break;
              case "Overpaid":echo "color=red";break;
            }
            ?>>
	        <?php 
            /*
              if status=paid,
                if checkcleardate > today, 
                  status = pending. 
                else 
                  status = paid

            if($counter_receipt->getStatus()=="Paid"){
            $today=MyDateTime::today();
            $checkcleardate=MyDateTime::frommysql($counter_receipt->getCheckcleardate());
            $status="Paid";
            if($checkcleardate->islaterthan($today))$status="Check to clear on ".$checkcleardate->toshortdate();
            echo $status;

            }
            else 
            */
            echo $counter_receipt->getStatus();

            ?>
            </font>
          </td>
		    </tr>
        <?php if($counter_receipt->getBalance()!=0){?>
        <tr valign=top>
          <td>Cheque Payment P</td>
          <td>
            <?php echo form_tag("out_check/new?client_class=CounterReceipt&client_id=".$counter_receipt->getId());?>
            <input size=10 name=chequeamt id=chequeamt value=<?php echo $counter_receipt->getBalance()?>>
            <input type=submit value=Submit>
            </form>
          </td>
        </tr>
        <?php } ?>
      </table>
    </td>
    <td>
			<table>
        <tr>
          <td>Notes</td>
          <td><?php echo $counter_receipt->getNotes() ?></td>
        </tr>
  		</table>
    </td>
  </tr>
</table>

<hr><h2>Payments:</h2>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Type</td>
    <td>Amount</td>
    <td>Check Voucher</td>
    <td>Check No</td>
    <td>Check Date</td>
    <td>Notes</td>
  </tr>
  <?php foreach($counter_receipt->getOutPaymentsByDate() as $detail){$check=$detail->getOutCheck()?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getDate())->toshortdate() ?></td>
      <td><?php echo $detail->getType() ?></td>
      <td align=right><?php echo $detail->getAmount() ?></td>
      <td><?php if($check)echo link_to($check->getCode(),"out_check/view?id=".$check->getId())?></td>
      <td><?php if($check)echo $check->getIsBankTransfer()?"BANK TRANSFER":$check->getCheckNo()?></td>
      <td><?php if($check)echo MyDateTime::frommysql($check->getCheckDate())->toshortdate()?></td>
      <td><?php echo $detail->getNotes() ?></td>
      <td><?php echo link_to('Edit','out_payment/edit?id='.$detail->getId()) ?></td>
      <td>
        <?php echo link_to(
          'Delete',
          'out_payment/delete?id='.$detail->getId(),
          array('method' => 'delete', 'confirm' => 'Are you sure?')
        ) ?>
      </td>
    </tr>
  <?php }?>
</table>

<hr>
<h2>Check History for <?php echo $counter_receipt->getVendor()?></h1>
<?php include_partial('checks', array('select' => false,'counter_receipt' => $counter_receipt,'vendor' => $counter_receipt->getVendor())) ?>

<script>
var manager_password="<?php 
	    $setting=Doctrine_Query::create()
	        ->from('Settings s')
	      	->where('name="manager_password"')
	      	->fetchOne();
      	if($setting!=null)echo $setting->getValue();
?>";
$('#counter_receipt_edit').click(function(out_payment) {
    out_payment.prout_paymentDefault();
    var pass=prompt("ENTER MANAGER PASSWORD","");
    //if password is correct
    if (pass==manager_password){
       window.location = $(this).attr('href');
    }
    //if cancelled
    else if(pass==null){}
    else
    {
        	alert("WRONG PASSWORD");
    }
    
});
</script>
