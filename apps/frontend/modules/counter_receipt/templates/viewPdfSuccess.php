<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2010-08-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @copyright 2004-2009 Nicola Asuni - Tecnick.com S.r.l (www.tecnick.com) Via Della Pace, 11 - 09044 - Quartucciu (CA) - ITALY - www.tecnick.com - info@tecnick.com
 * @link http://tcpdf.org
 * @license http://www.gnu.org/copyleft/lesser.html LGPL
 * @since 2008-03-04
 */

require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF("P", PDF_UNIT, "GOVERNMENTLETTER", true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle(sfConfig::get('custom_company_name').' Counter Receipt Check Voucher '.$counter_receipt->getCode());

//footer data
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(20, 10, 20, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// helvetica is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or helvetica to reduce file size.
$pdf->startPageGroup();

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

//====HEADER===========================



$tbl = "";

//-------------------

$content=array(
  $counter_receipt->getCode(),
  MyDateTime::frommysql($counter_receipt->getDate())->toshortdate(),
//  $counter_receipt->getPonumber(),
  );
  
$supplier=$counter_receipt->getVendor()->getName();
$notes=$counter_receipt->getNotes();

$tbl .= <<<EOD
<table>
 <tr>
  <td align="left">Counter Receipt No.: $content[0]</td>
  <td align="right">Date: $content[1]</td>
 </tr>
 <tr>
  <td align="left" width="100%">Supplier: $supplier</td>
 </tr>
 <tr>
  <td align="left" width="100%">Notes: $notes</td>
 </tr>
</table>
EOD;



/*

$chequeno=$counter_receipt->getChequeNumber();
$chequedate=MyDateTime::frommysql($counter_receipt->getChequeDate())->toshortdate();
$chequeamt=$counter_receipt->getAmount();
$tbl .= <<<EOD
<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Supplier</td>
          <td>$supplier</td>
        </tr>
				<tr>
					<td>Cheque No.</td>
					<td>$chequeno</td>				
			  </tr>
				<tr>
					<td>Cheque Date</td>
					<td>$chequedate</td>				
			  </tr>
				<tr>
					<td>Cheque Amount</td>
					<td>$chequeamt</td>				
			  </tr>
  		</table>
    </td>
    <td>
			<table>
        <tr>
          <td>Notes: </td>
          <td>$notes</td>
        </tr>
		  </table>
    </td>
  </tr>
</table>
EOD;
*/

//$pdf->write(5,"\n");
//$pdf->write(0,"\t".$message,'',false,'L',true,0,false,false,0,0);

//====TABLE HEADER===========================

$widths=array(20,20,20,20,50);
$scale=3.9;
foreach($widths as $index=>$width)$widths[$index]*=$scale;

$tbl .= <<<EOD
<hr>
<h2 align="center">Counter Receipt Check Voucher</h2>
<table border="1"  align="center">
<thead>
 <tr>
    <td width="$widths[0]">Date</td>
    <td width="$widths[1]">PO Number</td>
    <td width="$widths[2]">Supplier Invoice</td>
    <td width="$widths[3]">Amount</td>
    <td width="$widths[4]">Notes</td>
 </tr>
</thead>
EOD;


//===TABLE BODY============================
	$counter=0;
	foreach($counter_receipt->getCounterReceiptDetail() as $detail)
  {
		$counter++;
    $purchase=$detail->getPurchase();
    if($purchase)
    { 
      $content=array(
        $purchase->getDate(),
        $purchase->getPono(),
        $detail->getInvoiceNumber(),
        MyDecimal::format($detail->getAmount()),
        $detail->getNotes(),
      );
    }else{ 
      $content=array(
        "",
        "PO ".$detail->getPono(),
        $detail->getInvoiceNumber(),
        MyDecimal::format($detail->getAmount()),
        $detail->getNotes(),
      );
    } 

  
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]">$content[0]</td>
    <td width="$widths[1]">$content[1]</td>
    <td width="$widths[2]">$content[2]</td>
    <td width="$widths[3]" align="right">$content[3]</td>
    <td width="$widths[4]">$content[4]</td>
  </tr>
EOD;
  }    


//table footer
$content=array(
  "",
  "",
  "Total:",
  MyDecimal::format($counter_receipt->getAmount()),
  "",
  );
$tbl .= <<<EOD
  <tr>
    <td width="$widths[0]" align="center">$content[0]</td>
    <td width="$widths[1]" >$content[1]</td>
    <td width="$widths[2]" align="right">$content[2]</td>
    <td width="$widths[3]" align="right">$content[3]</td>
    <td width="$widths[4]" align="right"><b>$content[4]</b></td>
  </tr>
EOD;

$tbl .= <<<EOD
</table>
EOD;

//----CHEQUES---------------
$total=0;

$tbl .= <<<EOD
<h3 align="left">Checks</h3>
<table border="1">
  <tr>
    <td>Date</td>
    <td>Cheque Number</td>
    <td>Amount</td>
  </tr>
EOD;

/*
foreach($counter_receipt->getCounterReceiptCheque() as $cheque)
{
  $total+=$cheque->getAmount();
  $date=MyDateTime::frommysql($cheque->getDate())->toshortdate();
  $chequeno=$cheque->getChequeNumber();
  $chequeamt=$cheque->getAmount();
$tbl .= <<<EOD
  <tr>
    <td>$date</td>
    <td>$chequeno</td>
    <td>$chequeamt</td>
  </tr>
EOD;
}
*/
$tbl .= <<<EOD
  <tr>
    <td></td>
    <td>under</td>
    <td>construction</td>
  </tr>
EOD;

  $totalformatted=number_format($total,2,".",",");
$tbl .= <<<EOD
  <tr align=right>
	  <td></td>
	  <td>Total</td>
	  <td>$totalformatted</td>
  </tr>
</table>
<br>
<br>
EOD;

//--------------------

//footer

$tbl .= <<<EOD
<table border="1">
 <tr>
  <td align="center">Prepared By:</td>
  <td align="center">Approved By:</td>
  <td align="center">Received By:</td>
 </tr>
 <tr valign="bottom">
  <td align="center"><br><br><br><br></td>
  <td align="center"></td>
  <td align="center"></td>
 </tr>
</table>
EOD;


$pdf->writeHTML($tbl, true, false, false, false, '');

//-----------------


//--------------------------------------------------

/*
method Write [line 6138]
mixed Write( float $h, string $txt, [mixed $link = ''], [boolean $fill = false], [string $align = ''], [boolean $ln = false], [int $stretch = 0], [boolean $firstline = false], [boolean $firstblock = false], [float $maxh = 0], [float $wadj = 0])
*/
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output($counter_receipt->getCode(), 'I');

//============================================================+
// END OF FILE
//============================================================+

