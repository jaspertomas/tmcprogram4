<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>
<h1>
	Counter Receipt <?php echo $counter_receipt->getCode()?>
</h1>
<?php slot('transaction_id', $counter_receipt->getId()) ?>
<?php slot('transaction_type', "CounterReceipt") ?>

<?php if($sf_user->hasCredential(array('admin', 'encoder'), false)){?>
<?php echo form_tag_for($form,'counter_receipt/adjust')?> 
<?php echo $form['id'] ?>
<?php }?>	
<table>
  <tr valign=top>
    <td>
      <table>
        <tr>
          <td>Date</td>
          <td><?php echo MyDateTime::frommysql($counter_receipt->getDate())->toshortdate() ?></td>
        </tr>
        <tr>
          <td>Code: </td>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'encoder'), false)){?>
              <input size=10 name="counter_receipt[code]" id="counter_receipt_code" value="<?php echo $counter_receipt->getCode()?>">
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td>Supplier</td>
          <td><?php echo link_to($counter_receipt->getVendor(),"vendor/view?id=".$counter_receipt->getVendorId(),array("target"=>"edit_vendor")); ?> <?php //echo link_to("(Choose)","counter_receipt/chooseVendor?id=".$counter_receipt->getId())?></td>
        </tr>
        <tr>
        <?php if($sf_user->hasCredential(array('admin'), false)){?>
          <td>
            <font color=<?php switch($detail->getIsInspected()){case 0:echo "black";break;case 1:echo "green";break;case -1:echo "red";break;}?>>
              <?php switch($counter_receipt->getIsInspected()){case 0:echo "Unchecked<br>";break;case 1:echo "Checked<br>";break;case -1:echo "Error";break;}?>
            </font>

          </td>
          <td>
            <?php if($counter_receipt->getIsInspected()!=1)echo link_to("Check","counter_receipt/inspect?id=".$counter_receipt->getId()."&value=1") ?> 
            <?php if($counter_receipt->getIsInspected()!=0)echo link_to("Uncheck","counter_receipt/inspect?id=".$counter_receipt->getId()."&value=0") ?> 
            <?php if($counter_receipt->getIsInspected()!=-1)echo link_to("Error","counter_receipt/inspect?id=".$counter_receipt->getId()."&value=-1") ?>
          </td>
        <?php } ?>
        </tr>
  		</table>
    </td>
    <td>
			<table>
        <tr>
          <td colspan=2>Notes: 
          <?php echo $counter_receipt->getNotes() ?></td>
        </tr>
        <tr colspan=2>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'encoder'), false)){?>
              <textarea rows="2" cols="20" name="counter_receipt[notes]" id="counter_receipt_notes"><?php echo $counter_receipt->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <?php /*
        <tr>
          <td>Amount Tally: </td>
          <td><?php echo ($counter_receipt->getIsTally()?"True":"False") ?></td>
        </tr>
        */?>
        <tr>
          <td colspan=2>
            <?php if($sf_user->hasCredential(array('admin', 'encoder'), false)){?>
              <input type="submit" value="Save"></form>
            <?php }?>	
          </td>
        </tr>
		  </table>
    </td>

    <td>
      <table>
        <tr>
          <td>Total</td>
          <td><b><?php echo MyDecimal::format($counter_receipt->getAmount()) ?></b></td>
        </tr>
        <tr>
          <td>Balance</td>
          <td><b><?php echo MyDecimal::format($counter_receipt->getBalance()) ?></b></td>
        </tr>
      <tr>
	        <td colspan=2><b>Status</b>
            <font <?php 
            
            
            switch($counter_receipt->getStatus())
            {
              case "Paid":echo "color=blue";break;
              case "Pending":echo "color=red";break;
              case "Cancelled":echo "color=red";break;
              case "Overpaid":echo "color=red";break;
            }
            ?>>
	        <?php 
            /*
              if status=paid,
                if checkcleardate > today, 
                  status = pending. 
                else 
                  status = paid

            if($counter_receipt->getStatus()=="Paid"){
            $today=MyDateTime::today();
            $checkcleardate=MyDateTime::frommysql($counter_receipt->getCheckcleardate());
            $status="Paid";
            if($checkcleardate->islaterthan($today))$status="Check to clear on ".$checkcleardate->toshortdate();
            echo $status;

            }
            else 
            */
            echo $counter_receipt->getStatus();

            ?>
            </font>
          </td>
		    </tr>
        <tr valign=top>
        <td>Vouchers </td>
            <td>
            <?php 
              $vouchers=$counter_receipt->getVouchers();
              foreach($vouchers as $voucher)
                echo link_to($voucher->getNo(),"voucher/view?id=".$voucher->getId())." P".MyDecimal::format($voucher->getAmount()).($voucher->isCancelled()?" <font color=red>(Cancelled)</font>":"")."<br>";
              if($counter_receipt->getStatus()=="Pending") echo link_to("Generate Check Voucher","voucher/new?counter_receipt_id=".$counter_receipt->getId());
            ?>
          </td>
        </tr>

        <!--
        <?php //if($counter_receipt->getBalance()!=0){?>
        <tr valign=top>
          <td>Cheque Payment P</td>
          <td>
            <?php //echo form_tag("out_check/new?client_class=CounterReceipt&client_id=".$counter_receipt->getId());?>
            <input size=10 name=chequeamt id=chequeamt value=<?php echo $counter_receipt->getBalance()?>>
            <input type=submit value=Submit>
            </form>
          </td>
        </tr>
        <?php //} ?>
        <tr>
          <td colspan=2><?php //echo link_to("View Payments","counter_receipt/payments?id=".$counter_receipt->getId());?></td>
        </tr>
          -->

      </table>
    </td>


  </tr>
  <tr hidden=true id=counter_receipt_edit_password_tr class=password_tr >
    <td align=center colspan=10>Enter manager password:<input id=counter_receipt_edit_password_input type=password><input type=button value="Submit Password" id=counter_receipt_edit_password_button></td>
  </tr> 
</table>
</form>

            <?php echo link_to("Edit","counter_receipt/edit?id=".$counter_receipt->getId(),array("id"=>"counter_receipt_edit")) ?> |
            <?php echo link_to('Delete','counter_receipt/delete?id='.$counter_receipt->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?> |
            <?php echo link_to("View Files","counter_receipt/files?id=".$counter_receipt->getId());?> |
            <?php echo link_to("Print","counter_receipt/viewPdf?id=".$counter_receipt->getId());?>
            <br><?php echo link_to("Recalculate","counter_receipt/recalc?id=".$counter_receipt->getId()); ?>

<?php
//if user is salesman or encoder
if($sf_user->hasCredential(array('admin', 'encoder'), false)){
?>
<?php //echo link_to("Add Detail","counter_receipt_detail/new?counter_receipt_id=".$counter_receipt->getId()) ?>

<hr>
<!--
<b>Search purchase:</b> <input id=counter_receiptpurchasesearchinput autocomplete="off" value="<?php echo $searchstring ?>"> 				<input type=button value="Clear" id=counter_receiptclearsearch> <span id=pleasewait></span>
-->

<?php echo form_tag_for($detailform,"@counter_receipt_detail",array("id"=>"new_counter_receipt_detail_form")); ?>
<input type=hidden name=counter_receipt_detail[counter_receipt_id] value=<?php echo $counter_receipt->getId()?>  >
    <?php echo $detailform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>PO Number</td>
		<td>Supplier Invoice</td>
    <!--
		<td>Amount</td>
		-->
		<td>Notes</td>
	</tr>

	<tr>
	  <!--
		<td>
		  <div id=purchasename>
			<?php if($detailform->getObject()->getPurchaseId())echo $detailform->getObject()->getPurchase(); else echo "No item selected";?>
			</div>
		</td>
		-->
		<td><input size="10" name="counter_receipt_detail[pono]" value="" id="counter_receipt_detail_pono" type="text" autofocus></td>
		<td><input size="20" name="counter_receipt_detail[invoice_number]" value="" id="counter_receipt_detail_invoice_number" type="text"></td>
    <!--
		<td><input size="8" name="counter_receipt_detail[amount]" value="0.00" id="counter_receipt_detail_amount" type="text"></td>
		-->
		<td><input name="counter_receipt_detail[notes]" id="counter_receipt_detail_notes" type="text"></td>
		<td><input type=submit name=submit id=counter_receipt_detail_submit value=Save  ></form></td>
	</tr>
</table>
*Please check if the Supplier Invoice No. and Amount match what is shown below


<?php } ?>

<div id="counter_receiptsearchresult"></div>

<hr>


<!-----counter_receipt detail table------>


<?php 
  $total=0; 
  $po_total=0; 
?>
<table border=1>
  <tr>
    <td>PO</td>
    <td>PO Date</td>
    <td>PO Total</td>
    <td>PO Invoices</td>
    <td>PO Status</td>
    <td>Supplier<br>Invoice</td>
    <td>Payment<br>Amount</td>
    <td>Notes</td>
	  <td></td>
	  <td></td>
  </tr>
  <?php 
  	$counter=0;
  	foreach($counter_receipt->getCounterReceiptDetailDesc() as $detail){
  		$counter++;?>
  <tr>
    <td>
      <?php 
        $purchase=$detail->getPurchase();
        $po_total+=$purchase->getTotal();
      ?>
      <?php /*
      <?php if($detail->getPurchaseId()!=null){ ?>
        <?php echo link_to($detail->getPurchase(),"purchase/view?id=".$detail->getPurchaseId()); ?>
        <?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate() ?>,
        P<?php echo MyDecimal::format($purchase->getTotal()) ?>
    <?php }else{ ?>
    */?>
      <?php //} ?>
        <?php echo link_to($purchase,"purchase/view?id=".$purchase->getId()); ?>
    </td>
    <td>
        <?php echo MyDateTime::frommysql($purchase->getDate())->toshortdate(); ?>
    </td>
    <td>
        <?php echo MyDecimal::format($purchase->getTotal()); ?>
    </td>
    <td>
        <?php echo $purchase->getVendorInvoice()?>
    </td>
    <td align=center>
      <?php echo $purchase->getStatus(); ?>
    </td>
    <td align=right>
      <?php echo $detail->getInvoiceNumber() ?>
      <?php echo form_tag("counter_receipt_detail/adjust");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=invoice_number size=20 name=invoice_number value="<?php echo $detail->getInvoiceNumber() ?>">
      </form>
    </td>
    <td align=right>
      <?php echo MyDecimal::format($detail->getAmount()); $total+=$detail->getAmount(); ?>
      <?php echo form_tag("counter_receipt_detail/adjust");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=amount size=8 name=amount value="<?php echo MyDecimal::format($detail->getAmount());?>">
      </form>
    </td>
    <td align=right>
      <?php echo $detail->getNotes(); ?>
      <?php echo form_tag("counter_receipt_detail/adjust");?>
      <input type=hidden id=id name=id value=<?php echo $detail->getId()?>>
      <input id=notes size=20 name=notes>
      </form>
    </td>

  <?php if($sf_user->hasCredential(array('admin', 'encoder'), false)){?>
    <td align=center>
      <?php echo link_to("Edit","counter_receipt_detail/edit?id=".$detail->getId(),array("class"=>"counter_receipt_detail_edit","detail_id"=>$detail->getId())) ?>
      <br><?php echo link_to('Delete', 'counter_receipt_detail/delete?id='.$detail->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
    </td>
  <?php if($sf_user->hasCredential(array('admin'), false)){?>
    <td align=center>
      <font color=<?php switch($detail->getIsInspected()){case 0:echo "black";break;case 1:echo "green";break;case -1:echo "red";break;}?>>
        <?php switch($detail->getIsInspected()){case 0:echo "Unchecked<br>";break;case 1:echo "Checked<br>";break;case -1:echo "Error";break;}?>
      </font>
      <?php if($detail->getIsInspected()!=1)echo link_to("Check<br>","counter_receipt_detail/inspect?id=".$detail->getId()."&value=1") ?> 
      <?php if($detail->getIsInspected()!=0)echo link_to("Uncheck<br>","counter_receipt_detail/inspect?id=".$detail->getId()."&value=0") ?> 
      <?php if($detail->getIsInspected()!=-1)echo link_to("Error","counter_receipt_detail/inspect?id=".$detail->getId()."&value=-1") ?>
    </td>
  <?php } ?>
</tr>
<?php }?>
  <?php }?>
  
  
  <tr align=right>
	  <td>PO Total</td>
	  <td>
	    <font <?php if($total!=$po_total)echo "color=red"; ?>>
  	    <?php echo number_format($po_total,2,".",",")?>
	    </font>
    </td>
	  <td></td>
	  <td></td>
	  <td>Total</td>
	  <td><?php echo number_format($total,2,".",",")?></td>
	  <td></td>
	  <td></td>
	  <td></td>
  </tr>
</table>

<hr>

<!--counter receipt cheques-->
<?php /*
<h2>Cheques</h2>

<?php //echo form_tag_for($chequeform,"@counter_receipt_cheque",array("id"=>"new_counter_receipt_cheque_form")); ?>
<input type=hidden name=counter_receipt_cheque[counter_receipt_id] value=<?php echo $counter_receipt->getId()?>  >
<?php //echo $chequeform->renderHiddenFields(false) ?>

<table>
	<tr>
		<td>Date</td>
		<td>Cheque Number</td>
		<td>Amount</td>
	</tr>

	<tr>
		<td><?php //echo $chequeform["date"]?></td>
		<td><input size="8" name="counter_receipt_cheque[cheque_number]" value="" id="counter_receipt_cheque_pono" type="text"></td>
		<td><input size="8" name="counter_receipt_cheque[amount]" value="0.00" id="counter_receipt_cheque_amount" type="text"></td>
		<td><input type=submit name=submit id=counter_receipt_cheque_submit value=Save  ></td>
	</tr>
</table>
</form>

<hr>


<!-----counter_receipt cheque table------>

<?php //$total=0; ?>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Cheque Number</td>
    <td>Amount</td>
    <td></td>
    <td></td>
  </tr>
  <?php //foreach($counter_receipt->getCounterReceiptCheque() as $cheque){$total+=$cheque->getAmount();?>
  <tr>
    <td><?php //echo MyDateTime::frommysql($cheque->getDate())->toshortdate();?></td>
    <td><?php //echo $cheque->getChequeNumber();?></td>
    <td><?php //echo $cheque->getAmount();?></td>
    <td><?php //echo link_to('Edit','counter_receipt_cheque/edit?id='.$cheque->getId()) ?></td>
    <td><?php //echo link_to('Delete','counter_receipt_cheque/delete?id='.$cheque->getId(),array('method' => 'delete', 'confirm' => 'Are you sure?')) ?></td>
  </tr>
  <?php //}?>
  
  
  <tr align=right>
	  <td></td>
	  <td>Total</td>
	  <td><?php //echo number_format($total,2,".",",")?></td>
    <td></td>
	  <td></td>
  </tr>
</table>
<script type="text/javascript">
</script>
*/?>

