  <?php use_helper('I18N', 'Date') ?>
<?php 
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>

<h3>Similar Saved Commission Payments</h3>
<table>
<?php foreach($commission_payments as $cp){?>
  <tr>
    <td>
  <?php echo link_to($cp,"commission_payment/view?id=".$cp->getId()) ?>
    </td>
  </tr>
<?php } ?>
</table>
<hr>


<?php echo form_tag("profitdetail/commission");?>

Salesman: <?php
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'Employee',
  'add_empty' => true,
));
echo $w->render('salesman_id',(isset($salesman)?$salesman->getId():null));
?><br>

<?php
//show date form
$startDateForm = new sfWidgetFormDate();
$endDateForm = new sfWidgetFormDate();
echo "From ".$startDateForm->render('startdatesplit',$startdate);
echo " to ".$endDateForm->render('enddatesplit',$enddate);
?>
<input type=submit value="View">
</form>
<?php echo link_to("Last Month","profitdetail/commission?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql()."&salesman_id=".$salesman_id);?> | 
<?php echo link_to("This Month","profitdetail/commission?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql()."&salesman_id=".$salesman_id);?> | 
<?php echo link_to("Next Month","profitdetail/commission?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql()."&salesman_id=".$salesman_id);?> | 
<br><?php echo link_to("Yesterday","profitdetail/commission?startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql()."&salesman_id=".$salesman_id);?> | 
<?php echo link_to("Tomorrow","profitdetail/commission?startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql()."&salesman_id=".$salesman_id);?> | 

<h1>Commission Calculation <?php if(isset($salesman))echo "for ".$salesman->getName(); else echo "(no salesman selected )"?></h1>

<table>
  <tr>
    <td>From Date:</td>
    <td><?php echo MyDateTime::frommysql($startdate)->toshortdate(); ?> </td>
  </tr>
  <tr>
    <td>To Date:</td>
    <td><?php echo MyDateTime::frommysql($enddate)->toshortdate(); ?> </td>
  </tr>
  <tr>
    <td>Profit:</td>
    <td><div id=profittotal2></td>
  </tr>
  <tr>
    <td>Profit Above Minimum:</td>
    <td><div id="profittotalaboveminimum_2"></div></td>
  </tr>
  <tr>
    <td>Commission on Profit:</td>
    <td><div id=profitcommission2></td>
  </tr>
  <tr>
    <td>Total Sale:</td>
    <td><div id=saletotal2></td>
  </tr>
  <?php if(sfConfig::get('custom_commission_type')=="gross_sale"){ ?>
  <tr>
    <td><b>Commission on Total Sale:</b></td>
    <td><div id=grosscommission2></td>
  </tr>
  <?php } ?>
  <tr>
    <td>Total Sale with PO:</td>
    <td><div id=accountedsaletotal2></td>
  </tr>
  <tr>
    <td>Total Sale without PO:</td>
    <td><div id=unaccountedsaletotal2></td>
  </tr>
  <tr>
    <td>Commission on Sale without PO:</td>
    <td><div id=unaccountedsalecommission2></td>
  </tr>
  <tr>
    <td>Total Commission:</td>
    <td><div id=totalcommission2></td>
  </tr>
</table>
<br>
<?php $invoice_ids=array();?>
<?php echo form_tag("profitdetail/saveCommissionPayment");?>
Save this document as: <input name="name">
<input type=hidden name="invoice_ids" id=invoice_ids_2>
<input type=hidden name="salesman_id" value="<?php echo $salesman_id?>">
<input type=hidden name="startdate" value="<?php echo $startdate?>">
<input type=hidden name="enddate" value="<?php echo $enddate?>">
<input type=submit value="Save">
</form>

<?php 
/*
$datearray=explode("-",$startdate);
$todatearray=explode("-",$enddate);
echo link_to("Print","profitdetail/dsrmultipdf?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);*/?>


<br>
<br>
<?php 
$unaccountedsaletotal=0;
$profittotal=0;
$profittotalaboveminimum=0; //this is the total of profit at or above minimum profit rate
$purchasetotal=0;
$accountedsaletotal=0;
?>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Product</td>
    <td>Invoice No.</td>
    <td>PO No.</td>
    <td>Qty</td>
    <td>Sale Price</td>
    <td>Purchase Price</td>
    <td>Total Sale</td>
    <td>Profit</td>
    <td>Less Vat</td>
    <td>Net Profit</td>
    <td>Profit rate</td>
    <td>Commissionable Amount</td>
  </tr>
    <?php 
      foreach($profitdetails as $detail)
      if($detail->getInvoice()->getStatus()=="Paid"){
        $invoice=$detail->getInvoice();
        $invoice_template=$invoice->getInvoiceTemplate();
        $invoice_is_invoice=$invoice_template->getIsInvoice();
        $invoice_is_interoffice=$invoice_template->getIsDr();
        $invoice_ids[$detail->getInvoiceId()]="";
        $buyprice=$detail->getBuyprice();
        $sellprice=$detail->getSellprice();
        $qty=$detail->getQty();
        $pdetail=$detail->getPurchasedetail();
    ?>
    <tr align=right>
      <td align=left><?php echo MyDateTime::frommysql($detail->getInvoicedate())->toshortdate(); ?></td>
      <td align=left><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td align=left><?php echo link_to($invoice->getInvoiceTemplate()." ".$invoice->getInvno(),"invoice/view?id=".$invoice->getId()) ?></td>
      <td align=left><?php echo link_to($detail->getPurchase()->getPurchaseTemplate()." ".$detail->getPurchase()->getPono(),"purchase/view?id=".$detail->getPurchaseId()) ?></td>
      <td><?php echo $qty ?></td>
      <td><?php echo MyDecimal::format($detail->getSellprice()) ?></td>
      <td><?php echo MyDecimal::format($detail->getBuyprice()); ?></td>
      <td><?php 
      $totalsale=$detail->getSellprice()*$qty; $accountedsaletotal+=$totalsale; echo MyDecimal::format($totalsale); ?></td>
      <td>
        <?php 
          $grossprofit=$sellprice-$buyprice;
          if($sellprice<=0)
            $deduction=0;
          //if interoffice and (deduct vat even if interoffice == false)
          elseif($invoice_is_interoffice and !sfConfig::get('custom_profit_based_commission_calc_deduct_vat_on_interoffice'))
            $deduction=0;
          else
            $deduction=$sellprice*.12;
          $netprofit=$grossprofit-$deduction;
          $totalnetprofit=$netprofit*$qty;
          if($buyprice==0)$profitrate=100;
          else $profitrate=floatval($netprofit/$buyprice*100);

          $purchasetotal+=$buyprice*$qty;
          $profittotal+=$totalnetprofit;

          echo MyDecimal::format($grossprofit); 
        ?>
      </td>
      <td><?php echo MyDecimal::format($deduction) ?></td>
      <td><?php echo MyDecimal::format($netprofit) ?></td>
      <td>
        <font <?php if($profitrate>50 and $netprofit>1000)echo "color=green"; else if($profitrate<0)echo "color=red"; ?>>
        <?php echo number_format($profitrate, 0); ?>%
        </font>
      </td>
      <td><?php 
        if($profitrate>=12 or $profitrate<0)
        {echo $totalnetprofit;$profittotalaboveminimum+=$totalnetprofit;} 
      ?></td>
      <td><?php echo $detail->getInvoice()->getCustomer() ?></td>
      <!--
      <td><?php echo $pdetail->getDiscrate()?></td>
      -->
    </tr>
    <?php }?>

    <!--sales without po, but product has buyprice: use buyprice to calculate profit-->
    <?php foreach($detailswithbuyprice as $detail)
      if($detail->getInvoice()->getStatus()=="Paid")
      if($detail->getRemaining()!=0)
      {
      $invoice=$detail->getInvoice();
      $invoice_template=$invoice->getInvoiceTemplate();
      $invoice_is_invoice=$invoice_template->getIsInvoice();
      $invoice_is_interoffice=$invoice_template->getIsDr();
      $buyprice=$detailbuyprices[$detail->getId()];
      $sellprice=$detail->getPrice();
      $invoice_ids[$detail->getInvoiceId()]="";
      $qty=$detail->getQty();
    ?>
    <tr align=right>
      <td align=left><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td align=left><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td align=left><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td></td>
      <td><?php echo $qty ?></td>
      <td><?php echo $detail->getPrice() ?></td>
      <td><?php echo $buyprice; ?></td>
      <td><?php $totalsale=$detail->getPrice()*$qty; $accountedsaletotal+=$totalsale; echo MyDecimal::format($totalsale); ?></td>
      <td>
        <?php 
          $grossprofit=$sellprice-$buyprice;
          if($sellprice<=0)
            $deduction=0;
          //if interoffice and (deduct vat even if interoffice == false)
          elseif($invoice_is_interoffice and !sfConfig::get('custom_profit_based_commission_calc_deduct_vat_on_interoffice'))
            $deduction=0;
          else
            $deduction=$sellprice*.12;
          $netprofit=$grossprofit-$deduction;
          $totalnetprofit=$netprofit*$qty;
          if($buyprice==0)$profitrate=100;
          else $profitrate=floatval($netprofit/$buyprice*100);

          $purchasetotal+=$buyprice*$qty;
          $profittotal+=$totalnetprofit;

          echo MyDecimal::format($grossprofit); 
        ?>
      </td>
      <td><?php echo MyDecimal::format($deduction) ?></td>
      <td><?php echo MyDecimal::format($netprofit) ?></td>
      <td>
        <font <?php if($profitrate>50 and $netprofit>1000)echo "color=green"; else if($profitrate<0)echo "color=red"; ?>>
        <?php echo number_format($profitrate, 0); ?>%
        </font>
      </td>
      <td><?php 
        if($profitrate>=12 or $profitrate<0)
        {echo $totalnetprofit;$profittotalaboveminimum+=$totalnetprofit;} 
      ?></td>
      <td><?php echo $detail->getInvoice()->getCustomer() ?></td>
    </tr>
    <?php }?>

    <!--services-->
    <?php /*foreach($detailsofservices as $detail)if($detail->getInvoice()->getStatus()=="Paid"){
    ?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td></td>
      <td><?php echo intval($detail->getRemaining()) ?></td>
      <td align=right><?php echo $detail->getPrice() ?></td>
      <td align=right>0</td>
      <td>100%</td>
      <td align=right><?php echo $detail->getRemaining()*$detail->getPrice() ?></td>
      <td align=right><?php echo $detail->getRemaining()*$detail->getPrice() ?></td>
    </tr>
    <?php }*/?>
  <tr align=right>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Total:</td>
    <td><?php echo MyDecimal::format($accountedsaletotal,",",".")?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td><?php echo MyDecimal::format($profittotalaboveminimum,",",".")?></td>
  </tr>
  <tr>
  <tr>
    <td>Date</td>
    <td>Product</td>
    <td>Invoice No.</td>
    <td>PO No.</td>
    <td>Qty</td>
    <td>Sale Price</td>
    <td>Purchase Price</td>
    <td>Total Sale</td>
    <td>Profit</td>
    <td>Less Vat</td>
    <td>Net Profit</td>
    <td>Profit rate</td>
    <td>Commissionable Amount</td>
  </tr>
  </tr>
</table>
*Services such as installation and delivery not included

<h2>Uncomputable Invoices (no product cost information)</h2>
<br><?php echo link_to("Buy Price Quick Input",'product/buyPriceQuickInput'); ?>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Product</td>
    <td>Invoice No.</td>
    <td>Sale Price</td>
    <td>Qty</td>
    <td>Total Sale</td>
  </tr>
    <?php foreach($detailsnotcalculated as $detail)if($detail->getInvoice()->getStatus()=="Paid"){
      $invoice_ids[$detail->getInvoiceId()]="";
    ?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td align=right><?php $price=$detail->getTotal()/$detail->getQty(); echo MyDecimal::format($price) ?></td>
      <td align=right><?php echo $detail->getRemaining() ?></td>
      <td align=right><?php $total=$detail->getTotal(); echo MyDecimal::format($total,",","."); $unaccountedsaletotal+=$total; ?></td>
      <td><?php echo $detail->getInvoice()->getCustomer() ?></td>
    </tr>
    <?php }?>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Total</td>
    <td><?php echo MyDecimal::format($unaccountedsaletotal,",",".")?></td>
</table>

<?php
//use profittotal or profittotalaboveminimum, whichever is lesser
$baseprofit=$profittotal;
if($baseprofit>$profittotalaboveminimum)$baseprofit=$profittotalaboveminimum;

$commissionrate=0;
if(isset($salesman))$commissionrate=$salesman->getCommission()/100;
$profitcommission=$baseprofit*$commissionrate;
$profitcommission=round($profitcommission,2);

$totalprofitrate=0;
if($purchasetotal!=0)$totalprofitrate=$baseprofit/$purchasetotal;
$unaccountedsaleprofit=$unaccountedsaletotal*$totalprofitrate;
$unaccountedsalecommission=$unaccountedsaleprofit*$commissionrate;
$unaccountedsalecommission=round($unaccountedsalecommission,2);
$totalcommission=$profitcommission+$unaccountedsalecommission;
?>
<?php //echo $totalprofitrate?>
<input type=hidden id=profittotalaboveminimum_1 value=<?php echo MyDecimal::format($profittotalaboveminimum,",",".")?>>
<input type=hidden id=profittotal1 value=<?php echo MyDecimal::format($profittotal,",",".")?>>
<input type=hidden id=profitcommission1 value=<?php echo MyDecimal::format($profitcommission,",",".")?>>
<input type=hidden id=saletotal1 value=<?php echo MyDecimal::format($accountedsaletotal+$unaccountedsaletotal,",",".")?>>
<input type=hidden id=grosscommission1 value=<?php echo MyDecimal::format(($accountedsaletotal+$unaccountedsaletotal)*$commissionrate,",",".")?>>
<input type=hidden id=accountedsaletotal1 value=<?php echo MyDecimal::format($accountedsaletotal,",",".")?>>
<input type=hidden id=unaccountedsaletotal1 value=<?php echo MyDecimal::format($unaccountedsaletotal,",",".")?>>
<input type=hidden id=unaccountedsalecommission1 value=<?php echo MyDecimal::format($unaccountedsalecommission,",",".")?>>
<input type=hidden id=totalcommission1 value=<?php echo MyDecimal::format($totalcommission,",",".")?>>
<input type=hidden id=invoice_ids_1 value=<?php echo implode(",",array_keys($invoice_ids)) ?>>

<script>
$("#profittotalaboveminimum_2").html($("#profittotalaboveminimum_1").val());
$("#profittotal2").html($("#profittotal1").val());
$("#profitcommission2").html($("#profitcommission1").val());
$("#saletotal2").html($("#saletotal1").val());
$("#grosscommission2").html($("#grosscommission1").val());
$("#accountedsaletotal2").html($("#accountedsaletotal1").val());
$("#unaccountedsaletotal2").html($("#unaccountedsaletotal1").val());
$("#unaccountedsalecommission2").html($("#unaccountedsalecommission1").val());
$("#totalcommission2").html($("#totalcommission1").val());
$("#invoice_ids_2").val($("#invoice_ids_1").val());
</script>

<hr>
<h2>Invoices not yet Paid</h2>

<?php
$unaccountedsaletotal=0;
$profittotal=0;
$profittotalaboveminimum=0; //this is the total of profit at or above minimum profit rate
$purchasetotal=0;

?>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Product</td>
    <td>Invoice No.</td>
    <td>PO No.</td>
    <td>Qty</td>
    <td>Sale Price</td>
    <td>Purchase Price</td>
  </tr>
    <?php foreach($profitdetails as $detail)if($detail->getInvoice()->getStatus()!="Paid"){?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoicedate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td><?php echo link_to($detail->getPurchase()->getPurchaseTemplate()." ".$detail->getPurchase()->getPono(),"purchase/view?id=".$detail->getPurchaseId()) ?></td>
      <td><?php echo intval($detail->getQty()) ?></td>
      <td align=right><?php echo $detail->getSellprice() ?></td>
      <td align=right><?php echo $detail->getBuyprice(); ?></td>
      <td><?php echo $detail->getInvoice()->getCustomer() ?></td>
    </tr>
    <?php }?>

    <!--sales without po, but product has buyprice: use buyprice to calculate profit-->
    <?php foreach($detailswithbuyprice as $detail)
      if($detail->getInvoice()->getStatus()!="Paid")
      if($detail->getRemaining()!=0)
      {
      $buyprice=$detailbuyprices[$detail->getId()];
    ?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>

      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td></td>
      <td><?php echo intval($detail->getRemaining()) ?></td>
      <td align=right><?php echo $detail->getPrice() ?></td>
      <td align=right><?php echo $buyprice; ?></td>
      <td><?php echo $detail->getInvoice()->getCustomer() ?></td>
    </tr>
    <?php }?>

    <!--services-->
    <?php /*foreach($detailsofservices as $detail)if($detail->getInvoice()->getStatus()=="Paid"){
    ?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td></td>
      <td><?php echo intval($detail->getRemaining()) ?></td>
      <td align=right><?php echo $detail->getPrice() ?></td>
      <td align=right>0</td>
      <td>100%</td>
      <td align=right><?php echo $detail->getRemaining()*$detail->getPrice() ?></td>
      <td align=right><?php echo $detail->getRemaining()*$detail->getPrice() ?></td>
    </tr>
    <?php }*/?>
  <tr>
    <td>Date</td>
    <td>Product</td>
    <td>Invoice No.</td>
    <td>PO No.</td>
    <td>Qty</td>
    <td>Sale Price</td>
    <td>Purchase Price</td>
  </tr>
</table>

<h2>Uncomputable Invoices (no product cost information)</h2>
<br><?php echo link_to("Buy Price Quick Input",'product/buyPriceQuickInput'); ?>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Product</td>
    <td>Invoice No.</td>
    <td>Sale Price</td>
    <td>Qty</td>
    <td>Total Sale</td>
  </tr>
    <?php foreach($detailsnotcalculated as $detail)if($detail->getInvoice()->getStatus()!="Paid"){?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td align=right><?php $price=$detail->getTotal()/$detail->getQty(); echo MyDecimal::format($price) ?></td>
      <td align=right><?php echo $detail->getRemaining() ?></td>
      <td align=right><?php $total=$detail->getTotal(); echo MyDecimal::format($total); $unaccountedsaletotal+=$total; ?></td>
      <td><?php echo $detail->getInvoice()->getCustomer() ?></td>
    </tr>
    <?php }?>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Total</td>
    <td align=right><?php echo MyDecimal::format($unaccountedsaletotal)?></td>
</table>

