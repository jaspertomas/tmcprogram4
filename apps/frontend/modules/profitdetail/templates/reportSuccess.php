  <?php use_helper('I18N', 'Date') ?>
<?php 
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
$startofthismonth=$today->getstartofmonth();
$startofnextmonth=$startofthismonth->addmonths(1);
$startoflastmonth=$startofthismonth->addmonths(-1);
$endofthismonth=$startofthismonth->getendofmonth();
$endofnextmonth=$startofnextmonth->getendofmonth();
$endoflastmonth=$startoflastmonth->getendofmonth();
?>

<?php echo form_tag("profitdetail/report");?>

Salesman: <?php
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'Employee',
  'add_empty' => true,
));
echo $w->render('salesman_id',(isset($salesman)?$salesman->getId():null));
?><br>

<?php
//show date form
$startDateForm = new sfWidgetFormDate();
$endDateForm = new sfWidgetFormDate();
echo "From ".$startDateForm->render('startdatesplit',$startdate);
echo " to ".$endDateForm->render('enddatesplit',$enddate);
?>
<input type=submit value="View">
</form>
<?php echo link_to("Last Month","profitdetail/report?startdate=".$startoflastmonth->tomysql()."&enddate=".$endoflastmonth->tomysql());?> | 
<?php echo link_to("This Month","profitdetail/report?startdate=".$startofthismonth->tomysql()."&enddate=".$endofthismonth->tomysql());?> | 
<?php echo link_to("Next Month","profitdetail/report?startdate=".$startofnextmonth->tomysql()."&enddate=".$endofnextmonth->tomysql());?> | 
<br><?php echo link_to("Yesterday","profitdetail/report?startdate=".$yesterday->tomysql()."&enddate=".$yesterday->tomysql());?> | 
<?php echo link_to("Tomorrow","profitdetail/report?startdate=".$tomorrow->tomysql()."&enddate=".$tomorrow->tomysql());?> | 

<h1>Profit Report <?php if(isset($salesman))echo "for ".$salesman->getName()?></h1>
Date: 
<?php echo MyDateTime::frommysql($startdate)->toshortdate(); ?> 
to 
<?php echo MyDateTime::frommysql($enddate)->toshortdate(); ?> 

<br>Profit: <?php echo MyDecimal::format($profittotal)?>
<br>Total Sale without PO: <?php echo MyDecimal::format($unaccountedsaletotal)?>
<br>
<?php 
$datearray=explode("-",$startdate);
$todatearray=explode("-",$enddate);
echo link_to("Print","invoice/dsrmultipdf?invoice[date][year]=".
                $datearray[0]."&invoice[date][month]=".
                $datearray[1]."&invoice[date][day]=".
                $datearray[2]."&purchase[date][year]=".
                $todatearray[0]."&purchase[date][month]=".
                $todatearray[1]."&purchase[date][day]=".
                $todatearray[2]);?>


<br>
<br>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Product</td>
    <td>Invoice No.</td>
    <td>PO No.</td>
    <td>Sale Price</td>
    <td>Purchase Price</td>
    <td>Profit per unit</td>
    <td>Profit rate</td>
    <td>Qty</td>
    <td>Profit</td>
  </tr>
    <?php foreach($profitdetails as $detail){?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoicedate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td><?php echo link_to($detail->getPurchase()->getPurchaseTemplate()." ".$detail->getPurchase()->getPono(),"purchase/view?id=".$detail->getPurchaseId()) ?></td>
      <td><?php echo $detail->getSellprice() ?></td>
      <td><?php echo $detail->getBuyprice() ?></td>
      <td><?php echo $detail->getProfitperunit() ?></td>
      <td><?php echo $detail->getProfitrate() ?></td>
      <td><?php echo $detail->getQty() ?></td>
      <td><?php echo $detail->getProfit() ?></td>
      <td><?php //echo link_to("Edit","invoice/edit?id=".$detail->getId()) ?></td>
    </tr>
    <?php }?>

    <!--sales without po, but product has buyprice: use buyprice to calculate profit-->
    <?php foreach($detailswithbuyprice as $detail){
      $buyprice=$detailbuyprices[$detail->getId()];
    ?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td></td>
      <td><?php echo $detail->getPrice() ?></td>
      <td><?php echo $buyprice ?></td>
      <td><?php $profit=$detail->getPrice()-$buyprice; echo $profit ?></td>
      <td><?php echo $detail->getPrice()==0?0:round($profit/$buyprice,2)?></td>
      <td><?php echo $detail->getRemaining() ?></td>
      <td><?php echo $detail->getRemaining()*$profit ?></td>
    </tr>
    <?php }?>

    <!--services-->
    <?php foreach($detailsofservices as $detail){
    ?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td></td>
      <td><?php echo $detail->getPrice() ?></td>
      <td></td>
      <td><?php echo $detail->getPrice() ?></td>
      <td>100</td>
      <td><?php echo $detail->getRemaining() ?></td>
      <td><?php echo $detail->getRemaining()*$detail->getPrice() ?></td>
    </tr>
    <?php }?>
</table>

<h2>Uncomputable Invoices (no product cost information)</h2>
<br>Total sale: <?php echo MyDecimal::format($unaccountedsaletotal)?>
<br>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Product</td>
    <td>Invoice No.</td>
    <td>Sale Price</td>
    <td>Qty</td>
    <td>Total Sale</td>
  </tr>
    <?php foreach($detailsnotcalculated as $detail){?>
    <tr>
      <td><?php echo MyDateTime::frommysql($detail->getInvoice()->getDate())->toshortdate(); ?></td>
      <td><?php echo link_to($detail->getProduct(),"product/view?id=".$detail->getProductId()) ?></td>
      <td><?php echo link_to($detail->getInvoice()->getInvoiceTemplate()." ".$detail->getInvoice()->getInvno(),"invoice/view?id=".$detail->getInvoiceId()) ?></td>
      <td><?php $price=$detail->getTotal()/$detail->getQty(); echo $price ?></td>
      <td><?php echo $detail->getRemaining() ?></td>
      <td><?php echo $detail->getRemaining()*$detail->getPrice() ?></td>
    </tr>
    <?php }?>
</table>



