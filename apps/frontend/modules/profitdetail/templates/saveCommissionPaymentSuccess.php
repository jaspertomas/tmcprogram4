<h2>Error saving commission payment</h2>

<h3>These invoices already belong to a commission payment:</h3>
<hr>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Invoice</td>
    <td>Status</td>
    <td>Product</td>
    <td>Qty</td>
    <td>Price</td>
    <td>Discrate</td>
    <td>Discamt</td>
    <td>Total</td>
  </tr>
  <?php foreach($invoices as $invoice){?>
  <tr>
    <td><?php echo $invoice->getDate() ?></td>
    <td><?php echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ?></td>
    <td><?php echo $invoice->getStatus() ?></td>
    <td>Commission Payment: <?php echo link_to($invoice->getCommissionPayment(),"commission_payment/view?id=".$invoice->getCommissionPaymentId()) ?></td>
  </tr>
  <?php foreach($invoice->getInvoicedetail() as $detail){?>
  <tr>
  	<td></td>
  	<td></td>
  	<td></td>
    <td><?php echo $detail->getDescription() ?></td>
    <td><?php echo $detail->getQty() ?></td>
    <td><?php echo $detail->getPrice() ?></td>
    <td><?php echo $detail->getDiscrate() ?></td>
    <td><?php echo $detail->getDiscamt() ?></td>
    <td><?php echo $detail->getTotal() ?></td>
  </tr>
  <?php }?>
  <?php } ?>
</table>


