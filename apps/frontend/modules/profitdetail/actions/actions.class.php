<?php

require_once dirname(__FILE__).'/../lib/profitdetailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/profitdetailGeneratorHelper.class.php';

/**
 * profitdetail actions.
 *
 * @package    sf_sandbox
 * @subpackage profitdetail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class profitdetailActions extends autoProfitdetailActions
{
  public function executeReset(sfWebRequest $request)
  {
  /*
delete from profitdetail;
update invoicedetail set remaining=qty,is_profitcalculated=0;
update purchasedetail set remaining=qty,is_profitcalculated=0;
  
  */

//doctrine query delete
    Doctrine_Query::create()
        ->delete('Profitdetail p')
        ->execute();

//doctrine query delete
    Doctrine_Query::create()
        ->update('Invoicedetail i')
        ->set('i.remaining','i.qty')
        ->set('i.is_profitcalculated','0')
        ->execute();
    Doctrine_Query::create()
        ->update('Purchasedetail p')
        ->set('p.remaining','p.qty')
        ->execute();
        
    $this->redirect($request->getReferer());
  
  }
  public function executeCalcProfit(sfWebRequest $request)
  {
    //disabled, profit calculation is now automatic
    //return $this->redirect("home/error?msg=This function is now disabled; profit calculation is now automatic");
    
     $this->orphandetails = Doctrine_Query::create()
      ->from('Invoicedetail i')
      ->where('i.invoice_id is null')
      ->execute();

     $lastinvoicedetail = Doctrine_Query::create()
      ->from('Invoicedetail i')
      ->where('i.is_cancelled!=1')
	  ->orderBy('i.id desc')
	  ->limit(1)
      ->fetchOne();
  
    if(!$lastinvoicedetail)
    {
  	  return $this->redirect("home/error?msg=No invoice details exist to recalculate");
    }
  
      //default values
    $this->interval=100;
    $this->start=$lastinvoicedetail->getId();

	if($request->getMethod()!="POST")
     return;
     
    if(isset($_REQUEST["interval"]))
        $this->interval=$request->getParameter("interval");
    if(isset($_REQUEST["start"]))
        $this->start=$request->getParameter("start");
     $this->end=$this->start-$this->interval+1;
    
     $this->invoicedetails = Doctrine_Query::create()
      ->from('Invoicedetail i')
      ->where('i.id >='.$this->end)
      ->andWhere('i.id <='.$this->start)
	  ->andWhere('i.is_profitcalculated =0')
      ->andWhere('i.is_cancelled!=1')
	  ->orderBy('i.id desc')
      ->execute();
      /*
      //delete existing profit details for this invoice detail
    Doctrine_Query::create()
        ->delete('Profitdetail pd')
      	->where('pd.invoicedetail_id='.$invdetail->getId())
        ->execute();
        */      
        foreach ($this->invoicedetails as $invdetail) {
		     $purchdetails = Doctrine_Query::create()
		      ->from('Purchasedetail p')
		      ->where('p.product_id ='.$invdetail->getProductId())
		      ->andWhere('p.remaining >0')
		      ->andWhere('p.price >0')
      ->andWhere('p.is_cancelled!=1')
			  ->orderBy('p.id desc')
		      ->execute();
		      
		     $leftover=$invdetail->getQty(); 
		     $processed=0;
		     foreach($purchdetails as $purchdetail)
		     //continue only if there is any invdetail products left to process
		     if($leftover>0)
		     {
			     if($purchdetail->getQty()<$leftover) 
			     {
			     	$processed=$purchdetail->getRemaining();
			     	$leftover-=$purchdetail->getRemaining();
			     	$purchdetail->setRemaining(0);
			     	$purchdetail->save();
			     }
			     else
			     {
			     	$processed=$leftover;
			     	$purchdetail->setRemaining($purchdetail->getRemaining()-$leftover);
			     	$purchdetail->save();
			     	$leftover=0;
			     }
			     $pd=new ProfitDetail();
			     $pd->setProductId($invdetail->getProductId());
			     $pd->setInvoiceId($invdetail->getInvoiceId());
			     $pd->setPurchaseId($purchdetail->getPurchaseId());
			     $pd->setInvoicedetailId($invdetail->getId());
			     $pd->setPurchasedetailId($purchdetail->getId());
			     $pd->setInvoicedate($invdetail->getInvoice()->getDate());
			     $pd->setPurchasedate($purchdetail->getPurchase()->getDate());
			     $buyprice=$purchdetail->getTotal()/$purchdetail->getQty();
			     $sellprice=$invdetail->getTotal()/$invdetail->getQty();
			     $pd->setSellprice($sellprice);
			     $pd->setBuyprice($buyprice);
			     $pd->setProfitperunit($sellprice-$buyprice);
			     if($buyprice!=0)
				     $pd->setProfitrate(($sellprice/$buyprice)-1);
			     else
				     $pd->setProfitrate("100");
			     $pd->setQty($processed);
			     $pd->setProfit($pd->getProfitperunit()*$processed);
			     $pd->save();
			     
		     }
		     $invdetail->setRemaining($leftover); 
		     $invdetail->setIsProfitcalculated(1);
		     $invdetail->save(); 
		      
        }
     $this->start=$this->end-1;
  }
  public function executeReport(sfWebRequest $request)
  {
    $this->salesman_id=null;
  
    $requestparams=$request->getParameter("invoice");
    
    //process start and end dates
    //startdate
    if($request->hasParameter("startdate"))
    {
      $startdate=$request->getParameter("startdate");
    }
    elseif($request->hasParameter("startdatesplit"))
    {
      $requestparams=$request->getParameter("startdatesplit");
      $day=str_pad($requestparams["day"], 2, "0", STR_PAD_LEFT);
      $month=str_pad($requestparams["month"], 2, "0", STR_PAD_LEFT);
      $year=$requestparams["year"];
      $startdate=$year."-".$month."-".$day;
    }
    else
    {
      $startdate=MyDate::today();
    }
    $this->startdate=$startdate;

    //enddate
    if($request->hasParameter("enddate"))
    {
      $enddate=$request->getParameter("enddate");
    }
    elseif($request->hasParameter("enddatesplit"))
    {
      $requestparams=$request->getParameter("enddatesplit");
      $day=str_pad($requestparams["day"], 2, "0", STR_PAD_LEFT);
      $month=str_pad($requestparams["month"], 2, "0", STR_PAD_LEFT);
      $year=$requestparams["year"];
      $enddate=$year."-".$month."-".$day;
    }
    else
    {
      $enddate=MyDate::today();
    }
    $this->enddate=$enddate;
 
  	if($this->startdate>$this->enddate)
  	{
  	  return $this->redirect("home/error?msg=Start date cannot be later than end date");
  	}

    //fetch profit records from database
    $salesman_id=$request->getParameter('salesman_id');
    if($salesman_id!=null)
    {
      $this->salesman_id=$salesman_id;

      $this->salesman=Doctrine_Query::create()
        ->from('Employee e')
        ->where('e.id='.$salesman_id)
        ->fetchOne();

      $commission_unpaid=true;//show only invoices with commission not yet paid
      $this->profitdetails = ProfitdetailTable::fetchByDateRangeAndSalesmanId($startdate,$enddate,$salesman_id,$commission_unpaid);
      $this->invoicedetails = InvoicedetailTable::fetchByDateRangeAndSalesmanIdAndHasRemaining($startdate,$enddate,$salesman_id,$commission_unpaid);
    }
    else
    {
      if($request->getParameter("commission"))
      {
      $this->profitdetails = array();
      //has remaining means not completely connected with purchasedetails
      //meaning not completely accounted for in profit details
      $this->invoicedetails = array();
      }
      else
      {
        $this->profitdetails = ProfitdetailTable::fetchByDateRange($startdate,$enddate);
        //has remaining means not completely connected with purchasedetails
        //meaning not completely accounted for in profit details
        $this->invoicedetails = InvoicedetailTable::fetchByDateRangeAndHasRemaining($startdate,$enddate);
      }
    }

    $this->profittotal=0;
    $this->unaccountedsaletotal=0;
    $this->detailswithbuyprice=array();
    $this->detailbuyprices=array();
    $this->detailsofservices=array();
    $this->detailsnotcalculated=array();

    //calculations start
    //calc total profit in profit details 
    foreach($this->profitdetails as $detail)
      $this->profittotal+=$detail->getProfit();

    //include profits not in profit details
    //this includes services
    //and invoice details with remaining qty (unpartnered with purchasedetail)
    foreach($this->invoicedetails as $detail)
    {
      if($detail->getIsVat())continue;//ignore vat entries
    
      //detect buy price; check max buy price first
      $buyprice=$detail->getProduct()->getMaxbuyprice();
      //if max buy price is 0, check min buy price
      if($buyprice==0)$buyprice=$detail->getProduct()->getMinbuyprice();
      
      //if it's a service, 
      //is_service=1 means labor - repairs, etc - cost is employee salary
      //is_service=2 means production (control panels, etc) - 
          //cost of materials will be deducted via expenses
      //is_service=3 means delivery fee
      if($detail->getProduct()->getIsService()!=0)
      {
        //add to profit total
        $this->profittotal+=$detail->getRemaining()*$detail->getPrice();
        $this->detailsofservices[$detail->getId()]=$detail;
      }
      //if still no buy price, add to unaccounted sale
      elseif($buyprice==0)
      {
        //add to unaccounted sales
        $this->unaccountedsaletotal+=$detail->getRemaining()*$detail->getPrice();
        
        $this->detailsnotcalculated[$detail->getId()]=$detail;
      }
      //if not service 
      //and product buy price used
      else
      {
        //use buy price to calculate profit
        $profit=$detail->getPrice()-$buyprice;
        //add to profit total
        $this->profittotal+=$detail->getRemaining()*$profit;
        
        $this->detailswithbuyprice[$detail->getId()]=$detail;
        $this->detailbuyprices[$detail->getId()]=$buyprice;
      }
    }
  }
  public function executeCommission(sfWebRequest $request)
  {
    $this->transaction_invoice_template_id=Fetcher::fetchOne("InvoiceTemplate",array("prefix"=>"'TR'"))->getId();
    $this->interoffice_invoice_template_id=Fetcher::fetchOne("InvoiceTemplate",array("prefix"=>"'IRS'"))->getId();

    $request->setParameter('commission',true);
    $this->executeReport($request);
    
    $this->commission_payments=Doctrine_Query::create()
      ->from('CommissionPayment cp')
    	->andWhere('cp.startdate = "'.$this->startdate.'"')
    	->andWhere('cp.enddate = "'.$this->enddate.'"')
    	->andWhere('cp.employee_id = '.$this->salesman_id)
    	->execute();
    	
    //special minimum profit rates for certain product types
    //tanks: 12% minimum profit rate (if profit rate is equal to or above 12%, give commission)
    $this->producttype_ids=explode(",",SettingsTable::fetch('commission_min_profit_product_type_ids'));
    $this->minprofitrates=explode(",",SettingsTable::fetch('commission_min_profit_rates'));
    $this->defaultminimumprofitrate=SettingsTable::fetch('commission_default_min_profit_rate');
    
  }
  public function executeSaveCommissionPayment(sfWebRequest $request)
  {
    $invoice_ids=explode(",",$request->getParameter("invoice_ids"));

    //validate none of the invoices belong to any commission_payments
    $this->invoices=Doctrine_Query::create()
      ->from('Invoice i')
    	->whereIn("id",$invoice_ids)
    	->andWhere('i.commission_payment_id is not null')
    	->execute();

    if(count($this->invoices)>0)
    {
      return;
    }

    $cp=new CommissionPayment();
    $cp->setDateCreated(MyDate::today());
    $cp->setName($request->getParameter("name"));
    $cp->setStartdate($request->getParameter("startdate"));
    $cp->setEnddate($request->getParameter("enddate"));
    $cp->setEmployeeId(intval($request->getParameter("salesman_id")));
    $cp->save();  

    Doctrine_Query::create()
      ->update('Invoice i')
      ->set('i.commission_payment_id',$cp->getId())
      ->whereIn("id",$invoice_ids)
      ->execute();
        
    $this->redirect("commission_payment/view?id=".$cp->getId());
    
  }
}
