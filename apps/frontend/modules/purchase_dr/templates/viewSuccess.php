<?php use_helper('I18N', 'Date') ?>
<?php if ($sf_user->hasFlash('msg')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('msg') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

<h2><?php echo link_to("> Back to ".$purchase,"purchase/view?id=".$purchase->getId())?></h2>
<h1>
  <?php echo $purchase_dr->getTypeString(); ?> <?php echo $purchase_dr->getCode(); ?>
  <?php if($purchase_dr->isCancelled()){?>
    (<font color=red>Cancelled</font>)
  <?php }else{?>
    (<?php echo $purchase_dr->receiveStatusString(); ?>)
    <?php 
      if(!$purchase_dr->isReceived())
      {
        $olddatereceived=MyDateTime::fromdatetime($purchase_dr->getDatetime());
        $today=MyDateTime::now();
        echo "<br>".link_to($purchase_dr->getTypeActionString()." Today (".$today->toshortdate().")","purchase_dr/receive?today=true&id=".$purchase_dr->getId());
        if($olddatereceived->tomysql()!=MyDate::today())echo "<br>".link_to($purchase_dr->getTypeActionString()." ".
        $olddatereceived->toshortdate(),"purchase_dr/receive?id=".$purchase_dr->getId());
      }
      elseif($sf_user->hasCredential(array('admin'), false))
        echo link_to("Undo ".$purchase_dr->getTypeActionString(),"purchase_dr/undoReceive?id=".$purchase_dr->getId());
    ?>
  <?php }?>
</h1>
<?php slot('transaction_id', $purchase_dr->getId()) ?>
<?php slot('transaction_type', "PurchaseDr") ?>

<table>
  <tr valign=top>
    <td>
      <table>
          <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
          <?php echo form_tag_for($form,'purchase_dr/adjust')?> <?php echo $form['id'] ?>
          <?php }?>	
        <tr valign=top>
          <td>Deliver to Warehouse:</td>
          <td>
          <select name="purchase_dr[warehouse_id]">
            <?php foreach($warehouses as $warehouse){ ?>
            <option value=<?php echo $warehouse->getId()?> <?php if($warehouse->getId()==$purchase_dr->getWarehouseId())echo "selected=selected"?>><?php echo $warehouse->getName()?></option>
            <?php } ?>
          </select>
          </td>
        </tr>
        <tr valign=top>
          <td>Date</td>
          <td><?php echo MyDateTime::fromdatetime($purchase_dr->getDatetime()) ?></td>
        </tr>
        <tr>
          <td colspan=2>
            <?php if($purchase_dr->isPending()){?>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
            <?php echo $form['datetime'] ?>
            <?php } ?>
            <?php }?>	
          </td>
        </tr>
        <tr valign=top>
          <td>Vendor</td>
          <td><?php echo link_to($purchase_dr->getVendor(),"vendor/view?id=".$purchase_dr->getVendorId()); ?> </td>
        </tr>
        <tr valign=top>
          <td >Purchase:</td>
          <td>
            <?php if($purchase_dr->getPurchaseId())echo link_to($purchase,"purchase/view?id=".$purchase_dr->getPurchaseId())?>&nbsp;
          </td>
        </tr>
		</table>
    <td>
      <table>
        <tr>
          <td>Notes</td>
          <td><?php echo $purchase_dr->getNotes() ?><br>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <textarea rows="2" cols="10" name="purchase_dr[notes]" id="purchase_dr_notes"><?php echo $purchase_dr->getNotes()?></textarea>   
            <?php }?>	
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <?php if($sf_user->hasCredential(array('admin', 'sales', 'cashier', 'encoder'), false)){?>
              <input type="submit" value="Save"></form>
            <?php }?>	
          </td>
        </tr>
		  </table>
    </td>
  </tr>
</table>

            <!--allow cancel only if not received and not cancelled-->
            <?php if($purchase_dr->isPending())echo link_to('Cancel','purchase_dr/cancel?id='.$purchase_dr->getId(), array('confirm' => 'Cancel: Are you sure?')) ?> |
            <!--only admin can delete and undo cancel-->
            <?php if($sf_user->hasCredential(array('admin'), false) and !$purchase_dr->isReceived()){?>
              <!--allow undo cancel only if cancelled-->
              <?php if($purchase_dr->isCancelled())echo link_to('Undo Cancel','purchase_dr/undoCancel?id='.$purchase_dr->getId(), array('confirm' => 'Undo Cancel: Are you sure?')) ?> |
              <?php echo link_to('Delete','purchase_dr/delete?id='.$purchase_dr->getId(), array('method' => 'delete', 'confirm' => 'Delete: Are you sure?')) ?> |
            <?php } ?>
            <?php echo link_to("Print","purchase_dr/viewPdf?id=".$purchase_dr->getId());?> |
            <!--allow set date to purchase date only if not received and not cancelled-->
            <?php if($purchase_dr->isPending())echo "<br>".link_to("Set purchase date as receive date","purchase_dr/setPurchaseDateAsReceiveDate?id=".$purchase_dr->getId());?> |
            <br><?php echo link_to("Recalculate","purchase_dr/recalc?id=".$purchase_dr->getId())?>
<hr>

<?php echo form_tag("purchase_dr/setQtys");?>
<input type=hidden id=id name=id value=<?php echo $purchase_dr->getId()?>>
<table border=1>
  <tr>
    <td>Purchase</td>
    <td>DR</td>
    <td><?php echo $purchase_dr->isReturn()?"Returned":"Received"?></td>
    <td>Remaining</td>
    <td width=50%>Description</td>
    <td>Receive</td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>Qty</td>
    <td>Qty</td>
    <td>Qty</td>
    <td></td>
    <td>Date</td>
  </tr>
  <?php 
  	foreach($purchase->getDetails() as $purchasedetail){
      $dr_detail=$dr_details[$purchasedetail->getId()];
      if($dr_detail==null)echo "<font color=red>SOMETHING IS WRONG. PLEASE PRESS RECALCULATE";
      else $deliveryqty=$dr_detail->getQty();

      //for returns, 
      //for cosmetic purposes only
      //invert quantities
      if($purchase_dr->isReturn())$deliveryqty*=-1;
  ?>
  <tr>
    <td><?php echo $purchasedetail->getQty() ?></td>
    <td>
      <?php if($purchase_dr->isPending()){?>
      <input id=qtys[<?php echo $purchasedetail->getId()?>] size=3 name=qtys[<?php echo $purchasedetail->getId()?>] value=<?php echo $deliveryqty?>>
      <br>
      <?php } ?>
      <?php echo $deliveryqty;?>
    </td>
    <td align=right ><?php if($dr_detail!=null)if($dr_detail->isReceived())echo $deliveryqty;else echo 0; ?></td>
    <td align=center>
      <?php 
        //disable: ignore prodict if is_service
        //if product is service, ignore
        // if($purchasedetail->getProduct()->isService())$remaining=0;
        // else
          $remaining=$purchasedetail->getQty()-$purchasedetail->getQtyReceived();

        //for returns, 
        //for cosmetic purposes only
        //invert quantities
        if($purchase_dr->isReturn())$remaining*=-1;

        if($remaining!=0)echo "<font color=red>".$remaining."</font>";
      ?>
    </td>
    <td><?php echo link_to($dr_detail->getProduct(),"stock/view?id=".$dr_detail->getStock()->getId()) ?></td>
    <td align=right ><?php if($dr_detail!=null)if($dr_detail->isReceived())echo MyDateTime::fromdatetime($dr_detail->getDatetime()) ?></td>
  <?php }?>
  
</table>
<?php if($purchase_dr->isPending()){?>
  <input type=submit value="Save">
<?php } ?>
</form>

<hr>
<br>
<br>
<br>
<br>


