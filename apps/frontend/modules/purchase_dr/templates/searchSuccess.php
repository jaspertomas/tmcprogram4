<?php use_helper('I18N', 'Date'); ?>
<h1>Purchase Search</h1>
<table border="1">
  <tr>
    <td>Inv Number</td>
    <td>Customer</td>
    <td>Particulars</td>
    <td>Total</td>
    <td>Status</td>
  </tr>
  
  <?php 
  $matchfound=false;
  foreach($purchases as $purchase)
  if(strtolower($purchase->getName())==strtolower($searchstring)){$matchfound=true;break;}
  if(!$matchfound){
  ?>
  <tr>
    <td><?php echo link_to($searchstring." (Create New)","purchase/new?name=".$searchstring) ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php } ?>
  <?php foreach($purchases as $purchase){?>
  <tr>
    <td><?php echo link_to($purchase->getName(),"purchase/view?id=".$purchase->getId()) ?></td>
    <td><?php echo $purchase->getCustomer() ?></td>
    <td><?php echo $purchase->getParticularsString() ?></td>
    <td><?php echo $purchase->getTotal() ?></td>
    <td><?php echo $purchase->getStatus() ?></td>
  </tr>
  <?php } ?>
</table>
