<style type="text/css" media="print">
@page {
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<table border=0>
  <tr>
    <td width=15>&nbsp;</td>
    <td width=320>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><?php echo $customer->getName()?></td>
    <td><?php if($terms>0)echo $customer->getTerms()." Days";else echo "Cash";?></td>
  </tr>
  <tr>
    <td></td>
    <td><?php echo $customer->getTinNo()?>&nbsp;</td>
    <td><?php echo MyDateTime::frommysql($purchase->getDate())->toprettydate()?></td>
  </tr>
  <tr>
    <td></td>
    <td><?php echo $customer->getAddress()?>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><?php //echo $customer->getAddressLine2()?>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>


<table border=0>
  <tr>
    <td width=25></td>
    <td width=25></td>
    <td width=158></td>
    <td width=67></td>
    <td width=67></td>
  </tr>
  <?php foreach($details as $detail){?>
  <tr>
    <td align=right><?php echo $detail->getQty()?></td>
    <td>&nbsp;</td>
    <td><?php echo $detail->getProduct()?></td>
    <td align=right><?php echo $detail->getPrice()?></td>
    <td align=right><?php echo $detail->getTotal()?></td>
  </tr>
  <?php } ?>
</table>
