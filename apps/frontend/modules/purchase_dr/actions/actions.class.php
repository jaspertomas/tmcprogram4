<?php

require_once dirname(__FILE__).'/../lib/purchase_drGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/purchase_drGeneratorHelper.class.php';

/**
 * purchase_dr actions.
 *
 * @package    sf_sandbox
 * @subpackage purchase_dr
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class purchase_drActions extends autoPurchase_drActions
{
  public function executeFilter(sfWebRequest $request)
  {
    $this->setPage(1);

    $this->filters = $this->configuration->getFilterForm($this->getFilters());

    $requestparams=$request->getParameter($this->filters->getName());
    if($request->hasParameter("customer_id"))
      $requestparams["customer_id"]=$request->getParameter("customer_id");

    $this->filters->bind($requestparams);
    if ($this->filters->isValid())
    {
      $this->setFilters($this->filters->getValues());

      $this->redirect('@purchase_dr');
    }

    $this->pager = $this->getPager();
    $this->sort = $this->getSort();

    $this->setTemplate('index');
  }

  public function executeNew(sfWebRequest $request)
  {
    $purchase=Fetcher::fetchOne("Purchase",array("id"=>$request->getParameter("purchase_id")));

    $purchase_dr = PurchaseDrTable::genDrForPurchaseId($purchase->getId(),$this->getUser()->getGuardUser());
    $this->redirect("purchase_dr/view?id=".$purchase_dr->getId());
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $purchase_dr = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $purchase_dr)));

      $this->getUser()->setFlash('notice', $notice);

      $this->redirect('purchase_dr/view?id='.$purchase_dr->getId());
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

  public function executeView(sfWebRequest $request)
  {
    //fetch return by id
    $this->purchase_dr=Fetcher::fetchOne("PurchaseDr",array("id"=>$request->getParameter('id')));

    $this->form = $this->configuration->getForm($this->purchase_dr);

    $this->purchase=$this->purchase_dr->getPurchase();

    //arrange returndetails into an array with purchasedetail as key
    $this->dr_details=$this->purchase_dr->getPurchaseDrDetailsIndexedByPurchasedetailId();
    
    $this->warehouses=Fetcher::fetch("Warehouse",array());
    // $this->default_warehouse=Fetcher::fetch("Warehouse",array("id"=>SettingsTable::fetch("default_warehouse_id")));
    /*
    $detail=new PurchaseDrDetail();
    $detail->setQty(1);
    $this->product_is_set=false;
    $this->product=null;
    $this->detailform = new PurchaseDrDetailForm($detail);
    $this->detail = $detail;
    $this->searchstring=$this->request->getParameter('searchstring');
    */
  }
  public function executeSetQtys(sfWebRequest $request)
  {
    //fetch return by id
    $dr=Fetcher::fetchOne("PurchaseDr",array("id"=>$request->getParameter('id')));

    if($dr->isReceived())
    {
      $error="Cannot set qty, DR is already received";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('purchase_dr/view?id='.$dr->getId());
    }
    if($dr->isCancelled())
    {
      $error="Cannot set qty, DR is cancelled";
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('purchase_dr/view?id='.$dr->getId());
    }

    //arrange returndetails into an array with purchasedetail as key
    //Usage:
    //$dr_detail = $dr_details[$purchasedetail_id] 
    $dr_details=$dr->getPurchaseDrDetailsIndexedByPurchasedetailId();


    // var_dump($request->getParameter('qtys'));die();
    foreach($request->getParameter('qtys') as $purchasedetail_id => $qty)
    {
      $purchasedetail=Fetcher::fetchOne("Purchasedetail",array("id"=>$purchasedetail_id));
      $qty=intval($qty);

      //for returns, 
      //for cosmetic purposes only
      //invert quantities
      if($dr->isReturn())$qty*=-1;

      //validate: qty must not be greater than remaining
      $qty_remaining=$purchasedetail->getQty()-$purchasedetail->getQtyReceived();

      $action_string="Cannot receive";
      if($dr->isReturn())$action_string="Cannot return";
      if($qty>$qty_remaining)
      {
        $error=$action_string." ".$qty." units, only ".$qty_remaining." units remaining";
        $this->getUser()->setFlash('error', $error);
        return $this->redirect('purchase_dr/view?id='.$dr->getId());
      }

      //if deliverydetail exists, just save the qty
      if(array_key_exists($purchasedetail_id, $dr_details))
      {
        $dr_detail=$dr_details[$purchasedetail_id];
        $dr_detail->setQty($qty);
        $dr_detail->save();

        //set as return if qty is negative
        $dr_detail->setIsReturn(($qty<0)?1:0);
      }
      //else if deliverydetail does not exist, create it
      else 
      {
        //if qty is 0, don't do anything
        if($qty==0)continue;
      
        $dr_detail=new PurchaseDrDetail();
        $dr_detail->setPurchaseDrId($dr->getId());
        $dr_detail->setPurchaseId($dr->getPurchaseId());
        $dr_detail->setCustomerId($dr->getCustomerId());
        $dr_detail->setWarehouseId($dr->getWarehouseId());

        $dr_detail->setPurchasedetailId($purchasedetail_id);
        $id=$purchasedetail;
        $dr_detail->setDatetime($dr->getDatetime());
        $dr_detail->setProductId($id->getProductId());
        $dr_detail->setQty($qty);
        //set as return if qty is negative
        $dr_detail->setIsReturn(($qty<0)?1:0);

        $user=$this->getUser()->getGuardUser();
        $dr_detail->setCreatedById($user->getId());
        $dr_detail->setUpdatedById($user->getId());
        $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
        $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr_detail->save();
      }
    }

    $dr->calc();

    return $this->redirect("purchase_dr/view?id=".$dr->getId());
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $purchase_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $purchase_dr)));

    if ($purchase_dr->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect("purchase/view?id=".$purchase_dr->getPurchaseId());
  }
  public function executeCancel(sfWebRequest $request)
  {
    $purchase_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $purchase_dr)));

    if ($purchase_dr->cascadeCancel())
    {
      $this->getUser()->setFlash('notice', 'The item was cancelled successfully.');
    }

    $this->redirect("purchase_dr/view?id=".$purchase_dr->getId());
  }
  public function executeUndoCancel(sfWebRequest $request)
  {
    $purchase_dr=$this->getRoute()->getObject();
    
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $purchase_dr)));

    if ($purchase_dr->cascadeUndoCancel())
    {
      $this->getUser()->setFlash('notice', 'The item was uncancelled successfully.');
    }

    $this->redirect("purchase_dr/view?id=".$purchase_dr->getId());
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter('purchase_dr');
    $id=$requestparams['id'];

    $this->forward404Unless(
    $this->purchase_dr=Doctrine_Query::create()
    ->from('PurchaseDr i')
    ->where('i.id = '.$id)
    ->fetchOne()
    , sprintf('PurchaseDr with id (%s) not found.', $request->getParameter('id')));

    $this->purchase_dr->setNotes($requestparams["notes"]);
    $this->purchase_dr->setWarehouseId($requestparams["warehouse_id"]);
    Doctrine_Query::create()
      ->update('PurchaseDrDetail pdd')
      ->set('pdd.warehouse_id=?',$requestparams["warehouse_id"])
      ->where('pdd.purchase_dr_id=?', $this->purchase_dr->getId())
      ->execute();
    
    //set date and adjust stock entry date if necessary
    if(isset($requestparams["datetime"]))
    {
      $newdatetime=
        $requestparams["datetime"]["year"]."-".
        $requestparams["datetime"]["month"]."-".
        $requestparams["datetime"]["day"]."-".
        $requestparams["datetime"]["hour"]."-".
        $requestparams["datetime"]["minute"];
      $this->purchase_dr->setDatetime($newdatetime);
      foreach($this->purchase_dr->getPurchaseDrDetail() as $detail)
      {
        $detail->setDatetime($newdatetime);
        $detail->save();

			  //update stockentry
			  $stockentry=$detail->getStockentry();
			  $stockentry->setDatetime($newdatetime);
			  $stockentry->save();
			  $stockentry->getStock()->calcFromStockEntry($stockentry);
      }
      
      //no need for this, change date allowed only
      //when dr is not received
      //stock entries will be generated when it is received
      // $this->purchase_dr->setDateAndUpdateStockEntry($newdatetime);
    }
    
    $this->purchase_dr->save();

    $this->redirect($request->getReferer());
  }
  public function executeViewPdf(sfWebRequest $request)
  {
    $this->executeView($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeReceive(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('PurchaseDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
    
    if($request->getParameter('today')=="true")
    {
      $dr->setDatetime(MyDateTime::now()->todatetime());
      $dr->save();
    }
    $error=$dr->receive();
    if($error!=false)
    {
      $this->getUser()->setFlash('error', $error);
      return $this->redirect('purchase_dr/view?id='.$dr->getId());
    }

    //todo:create history

    $this->redirect($request->getReferer());
  }
  public function executeUndoReceive(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('PurchaseDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    $dr->undoReceive();

    //todo:create history

    $this->redirect($request->getReferer());
  }
  //this can only be called if DR is not yet received
  public function executeSetPurchaseDateAsReceiveDate(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('PurchaseDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));

    $purchase=$dr->getPurchase();
    $dr->setDatetime($purchase->getDate());
    $dr->save();

    $this->redirect($request->getReferer());
  }
  public function executeRecalc(sfWebRequest $request)
  {
    $this->forward404Unless(
      $dr=Doctrine_Query::create()
        ->from('PurchaseDr dr')
      	->where('dr.id = '.$request->getParameter('id'))
      	->fetchOne()
    , sprintf('Return with id (%s) not found.', $request->getParameter('id')));
    $dr->regenMissingDetails($this->getUser()->getGuardUser());
    $dr->calc();
    $this->redirect($request->getReferer());
  }
  public function executeRecalcAll(sfWebRequest $request)
  {
    echo "ACCESS DENIED";die();
    $drs=Doctrine_Query::create()
      ->from('PurchaseDr dr')
      ->execute();
    foreach($drs as $dr)
    {
      //$dr->calc();
      $purchase=$dr->getPurchase();
      $dr->setCode(str_replace("IDR","PDR",$dr->getCode()));
      $dr->save();
      $purchase->setIsDrBased(1);
      $purchase->save();
    }
    $this->redirect("home/index");
  }
}
