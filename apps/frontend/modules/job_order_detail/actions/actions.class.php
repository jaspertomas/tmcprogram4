<?php

require_once dirname(__FILE__).'/../lib/job_order_detailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/job_order_detailGeneratorHelper.class.php';

/**
 * job_order_detail actions.
 *
 * @package    sf_sandbox
 * @subpackage job_order_detail
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class job_order_detailActions extends autoJob_order_detailActions
{
  public function executeNew(sfWebRequest $request)
  {
    $this->job_order_detail = new JobOrderDetail();
    $job_order_id=$request->getParameter("job_order_id");
    $this->job_order_detail->setJobOrderId($job_order_id);
    $job_order=$this->job_order_detail->getJobOrder();
    $this->job_order_detail->setQty(1);
    $this->form = $this->configuration->getForm($this->job_order_detail);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $requestparams=$request->getParameter($form->getName());

    $isnew=$form->getObject()->isNew();
    
    if($requestparams["qty"]==0)
    {
      $message="Invalid Quantity";
      $this->getUser()->setFlash('error', $message);
      return $this->redirect($request->getReferer());
    }

    $product=Fetcher::fetchOne("Product",array("id"=>$requestparams["product_id"]));
    if($product->getProductCategory()=="Sheet" && $requestparams["qty"]>0)
    {
        $message="Quantity of raw materials should be negative (".$product->getName().")";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
    }

    //generate slot and color if new
    if($requestparams['slot']==0 or $requestparams['color']==null)
    {
      list($slot, $color) = JobOrderDetailTable::getNextSlotAndColor($requestparams['$product_id']);
      $requestparams['slot']=$slot;
      $requestparams['color']=$color;
    }

    $form->bind($requestparams);
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $job_order_detail = $form->save();
        // $job_order_detail->setBarcode(str_pad($job_order_detail->getId(),10,"0",STR_PAD_LEFT)."D");
        // $job_order_detail->calcRemaining();  
        //$job_order_detail->updateStockentry();
        
        //custom calculation
        // $job_order=$job_order_detail->getJobOrder();
        // $job_order->calc();
        // $job_order->save();
        
        ////create job_orderconversion if necessary, 
        ////allowing conversion from parts to finished product in dr
        /*
        //look for the conversion that this product belongs to, if any
        $conversiondetails=$job_order_detail->getProduct()->getConversiondetail();
        $conversion=null;
        foreach($conversiondetails as $cd)
        {
          $conversion=$cd->getConversion();

          //if conversion exists (if product belongs to a conversion)
          //and the job_order does not have a job_orderconversion for it yet
          //create
          if($conversion)
          {
            $job_orderconversion=Doctrine_Query::create()
            ->from('JobOrderconversion dc')
            ->where('dc.job_order_id = '.$job_order->getId())
            ->andWhere('dc.conversion_id = '.$conversion->getId())
            ->fetchOne();
            //if $job_orderconversion does not exist
            if($job_orderconversion==null)
            {
              //create
              $job_orderconversion=new JobOrderconversion();
              $job_orderconversion->setJobOrderId($job_order->getId());
              $job_orderconversion->setConversionId($conversion->getId());
              $job_orderconversion->setQty(0);
              $job_orderconversion->save();
            }
          }
        }
        */

      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $job_order_detail)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');
        $this->redirect('@job_order_detail_new?job_order_id='.$job_order->getId());
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect('job_order/view?id='.$job_order_detail->getJobOrderId());
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
/*
  private function processBarcode($purchdetail,$isnew)
  {
     $pds=Doctrine_Query::create()
        ->from('Profitdetail pd')
      	->where('pd.job_order_detail_id = '.$purchdetail->getId())
      	->orderBy('pd.id desc')
      	->execute();
  	  foreach($pds as $pd)
  	  {
  	      $invdetail=$pd->getInvoicedetail();
  	  
         //give back qty in profitdetail to invdetail and purchdetail
         $purchdetail->setRemaining($purchdetail->getRemaining()+$pd->getQty());
         $invdetail->setRemaining($invdetail->getRemaining()+$pd->getQty());
		     
         //process barcode / create new profitdetail
         //calculate qty to process
         //it's purchdetail->remaining or invdetail->remaining, whichever is lower
	       $processed=$purchdetail->getRemaining();
	       if($processed>$invdetail->getRemaining())$processed=$invdetail->getRemaining();
	       if($processed<0)$processed=0;

         //give qty to process from invdetail and purchdetail to profitdetail 
         $pd->setQty($processed);
		     $purchdetail->setRemaining($purchdetail->getRemaining()-$pd->getQty());
	       $invdetail->setRemaining($invdetail->getRemaining()-$pd->getQty());
         //if remaining=0, profitcalcluated=1
		     $invdetail->setIsProfitcalculated($invdetail->getRemaining()==0?1:0);
	       $invdetail->save();
	       //end of invdetail calculation

		     $pd->setProductId($invdetail->getProductId());
		     $pd->setInvoiceId($invdetail->getInvoiceId());
		     $pd->setJobOrderId($purchdetail->getJobOrderId());
		     $pd->setInvoicedetailId($invdetail->getId());
		     $pd->setJobOrderDetailId($purchdetail->getId());
		     $pd->setInvoicedate($invdetail->getInvoice()->getDate());
		     $pd->setJobOrderdate($purchdetail->getJobOrder()->getDate());
		     $buyprice=$purchdetail->getTotal()/$purchdetail->getQty();//buyprice per unit
		     $sellprice=$invdetail->getTotal()/$invdetail->getQty();//sellprice per unit
		     $pd->setSellprice($sellprice);
		     $pd->setBuyprice($buyprice);
		     $pd->setProfitperunit($sellprice-$buyprice);
		     if($buyprice!=0)
			     $pd->setProfitrate(($sellprice/$buyprice)-1);
		     //else buyprice per unit is 0 - I got it for free
		     else
			     $pd->setProfitrate("100");//avoid divide by 0
		     $pd->setProfit($pd->getProfitperunit()*$processed);
		     $pd->save();
    }
  }
  */
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $job_order=$this->getRoute()->getObject()->getJobOrder();
    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($this->getRoute()->getObject()->cascadeDelete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $job_order->calc();
    $job_order->save();
    $this->redirect('job_order/view?id='.$job_order->getId());
  }
  /*
  public function executeUpdateProduct(sfWebRequest $request)
  {
     $this->detail=Doctrine_Query::create()
        ->from('JobOrderDetail pd')
      	->where('pd.product_id = 4')//uncomment this on prod
      	->orderBy('pd.id desc')
      	->fetchOne();
      $this->detailform=new JobOrderDetailForm($this->detail);
      if($this->detail==null)
      {
        $this->redirect('home/error?msg='."no more records to process");
      }
      $this->job_order=$this->detail->getJobOrder();
  }
  */
  public function executeProcessUpdateProduct(sfWebRequest $request)
  {
     $product=Doctrine_Query::create()
        ->from('Product p')
      	->where('p.id = '.$request->getParameter("product_id"))
      	->fetchOne();
     $purchdetail=Doctrine_Query::create()
        ->from('JobOrderDetail pd')
      	->where('pd.id = '.$request->getParameter("job_order_detail_id"))
      	->fetchOne();
      	
  	//update job_order_details that match description of selected job_order_detail 
      $products= Doctrine_Query::create()
        ->update('JobOrderDetail pd')
        ->set('pd.product_id',$product->getId())
        ->where('pd.description = ?', array($purchdetail->getDescription()))
        ->execute();

    $note=new Note();
    $note->setName($product->getId());
    $note->setDescription($purchdetail->getDescription());
    $note->save();

    $this->redirect($request->getReferer());
  }
  public function executeSetQty(sfWebRequest $request)
  {
    //prevent invalid qty of 0
    if($request->getParameter('value')==0)
    {
        $message="Invalid qty";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
    }

    //fetch job_order_detail
    $this->detail=Doctrine_Query::create()
    ->from('JobOrderDetail id, id.JobOrder i')
    ->where('id.id = '.$request->getParameter('id'))
    ->fetchOne();

    $product=$this->detail->getProduct();
    if($product->getProductCategory()=="Sheet" && $request->getParameter('value')>0)
    {
        $message="Quantity of raw materials should be negative (".$product->getName().")";
        $this->getUser()->setFlash('error', $message);
        return $this->redirect($request->getReferer());
    }

    $this->detail->setQty($request->getParameter('value'));
    $this->detail->save();
    $jo=$this->detail->getJobOrder();
    $jo->calc();//check DRs and calculate production status
    $jo->save();

    $this->redirect($request->getReferer());
  }

  public function executeView(sfWebRequest $request)
  {
    $item=Fetcher::fetchOne("JobOrderDetail",array("id"=>$request->getParameter("id")));
    return $this->redirect("job_order/view?id=".$item->getJobOrderId());
  }
}
