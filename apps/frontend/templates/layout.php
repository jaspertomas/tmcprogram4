<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
  	<?php use_javascript('jquery-1.10.2.js') ?>
    <?php include_javascripts() ?>
    <style>
      nav {
          background-color:#EEEEEE;
          padding:10px 0;
          display:inline;
      }
      nav ul {
          list-style-type:none;
          margin:0;
          padding:0;
          display:inline;
      }
      nav ul li {
          display:inline-block;
          position:relative;
          display:inline;
      }    
            
      nav li ul {    
          background-color:#EEEEEE;
          position:absolute;
          left:0;
/*          top:40px; /* make this equal to the line-height of the links (specified below) */
          width:300px;
      }
      nav li li {
          position:relative;
          margin:0;
          display:block;
      }
      nav li li ul {
          background-color:#EEEEEE;
          position:absolute;
          top:0;
          left:300px; /* make this equal to the width of the sub nav above */
          margin:0;
      }
      ul.sub-menu {
          display:none;
      }
    </style>
    
  </head>
  <body bgcolor="#EEEEEE">
  
  
              <?php if($sf_user->getGuardUser()){ ?>
            		<div style="float:right;">
            		
    <nav>
    <ul>
        <li class="dropdown">
            <?php echo "Welcome ".$sf_user->getGuardUser()->getUsername(); ?>
            <ul class="sub-menu">
              <?php if($sf_user->getGuardUser()->getIsSuperAdmin()){ ?>
                <li><?php echo link_to("Users","@sf_guard_user"); ?></li>
                <li><?php echo link_to("Mgr Passwd","employee/mgrpasswd"); ?></li>
              <?php } ?>
            </ul>
        </li>
    </ul>
    </nav>
            		
            		
            		<?php 
                  echo " | ".
                    link_to("Logout","@sf_guard_signout");
                    //" | ".
                    //link_to("Edit Profile",url_for("@user_edit?id=".$sf_user->getGuardUser()->getUser()->getId())); ?>
                </div>

              <?php }else{ ?>
            		<div style="float:right;">
              		<?php echo link_to("Please login","@sf_guard_signin"); ?>
                </div>
                <br>
          		<?php } ?>

    <nav>
    <ul>
        <li><b><?php echo link_to(sfConfig::get('custom_program_label'),'home/index'); ?></b> | </li>
  <?php if($sf_user->getGuardUser()){ //check if signed in?>

    <!--links for admin and encoder-->
    <?php if($sf_user->hasCredential(array('admin', 'encoder'), false)){?>
    
        <li class="dropdown">
            Reports | 
            <ul class="sub-menu">
              <li><?php echo link_to("Dashboard",'home/dashboard'); ?></li>
              <li><?php echo link_to("Daily Sales Report",'invoice/dsr'); ?></li>
              <li><?php echo link_to("Daily Purchases Report",'purchase/dsr'); ?></li>
              <li><?php echo link_to("Daily Inventory Report",'invoice/dir'); ?></li>
              <li><?php echo link_to("Floating Check Report",'voucher/dsrmulti'); ?></li>
              <li><?php echo link_to("Uncollected Invoices",'invoice/listunpaid'); ?></li>
              <li><?php echo link_to("Uncollected Products",'invoice/listunpaidByProduct'); ?></li>
              <li><?php echo link_to("Unarrived Purchases",'purchase/listunreceived'); ?></li>
              <li><?php echo link_to("Unpaid Purchases",'purchase/listunpaid'); ?></li>
              <li><?php echo link_to("Salesman Commission Calculation",'profitdetail/commission'); ?></li>
              <li><?php echo link_to("Salesman Commission by Gross Sale",'invoice/commissionGrossSale'); ?></li>
              <li><?php echo link_to("Salesman Commission Payment",'commission_payment/index'); ?></li>
              <li><?php echo link_to("Technician Commission Calculation",'invoice/commission'); ?></li>
              <li><?php echo link_to("Inventory Issues",'stockentry/reportIssues'); ?></li>
            </ul>
        </li>
        
        <li class="dropdown">
            Transactions | 
            <ul class="sub-menu">
              <li><b>Invoices</b></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Unclosed Invoices",'invoice/list'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("All Invoices",'invoice/index'); ?></li>
              <li><b>Purchase Orders</b></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Incomplete POs",'purchase/list'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("All Purchase Orders",'purchase/index'); ?></li>
              <li><b>Others</b></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Job Orders",'job_order/index'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Stock Transfers",'transfer/index'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Quotations",'quotation/index'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Product Conversion",'delivery/conversionWizard'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Expense Vouchers",'voucher/dashboard'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Counter Receipts",'counter_receipt/index'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Counter Receipt Quick Input",'counter_receipt/quickInput'); ?></li>
              <li><b>Returns and Replacements</b></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Customer Returns",'returns/filter?client_class=Customer'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Supplier Returns",'returns/filter?client_class=Vendor'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Customer Replacements",'replacement/filter?client_class=Customer'); ?></li>
              <li>&nbsp;&nbsp;&nbsp;<?php echo link_to("Supplier Replacements",'replacement/filter?client_class=Vendor'); ?></li>
            </ul>
        </li>

        <li class="dropdown">
            Products | 
            <ul class="sub-menu">
              <li><?php echo link_to("New Product",'product/new'); ?></li>
              <li><?php echo link_to("Product Types / Inventory Sheets",'producttype/listNotHidden'); ?></li>
              <li><?php echo link_to("Product Type Daily Inventory",'producttype/dirIndex?sort=name&sort_type=asc'); ?></li>
              <li><?php echo link_to("Product Reordering",'productsearch/quotaDashboard'); ?></li>
              <li><?php echo link_to("Product Conversion Wizard","delivery/conversionWizard"); ?></li>
              <li><?php echo link_to("View All Products",'product/index'); ?></li>
            </ul>
        </li>

        <li class="dropdown">
            Others | 
            <ul class="sub-menu">
              <li><?php echo link_to("New Customer",'customer/new'); ?></li>
              <li><?php echo link_to("New Supplier",'vendor/new'); ?></li>
              <li><?php echo link_to("Orders",'customer_order/index'); ?></li>
              <li><?php echo link_to("Concerns",'concern/index'); ?></li>
              <li><?php echo link_to("Folders",'folder/index'); ?></li>
              <li><?php echo link_to("Files",'file/search'); ?></li>
              <li><?php echo link_to("DR",'delivery/dashboard'); ?></li>
              <li><?php echo link_to("Todo",'todo/index'); ?></li>
              <li><?php echo link_to("Notes",'note/index'); ?></li>
              <li><?php echo link_to("Warranty Sticker",'home/warranty'); ?></li>
              <?php if($sf_user->hasCredential(array('admin', 'hr'), false)){?>
              <li><?php echo link_to("Employees",'employee/index'); ?></li>
              <?php } ?>
              <li><?php echo link_to("Encoder Tools",'home/encoderTools'); ?></li>
              <li><?php echo link_to("Advanced Tools",'home/advanced'); ?></li>
              <li><?php echo link_to("Accounting Tools",'home/accountingTools'); ?></li>
              <li><?php echo link_to("Outgoing Checks Monitoring",'voucher/passbookReport'); ?></li>
              <li><?php echo link_to("Passbook Monitoring",'passbook/view'); ?></li>
              <li><?php echo link_to("Biometric Assistant",'home/bundyClockConvert'); ?></li>
            </ul>
        </li>



    
     <!--links for cashier-->
    <?php }else if($sf_user->hasCredential(array('cashier'), false)){?>
      <li class="dropdown">
        Reports | 
        <ul class="sub-menu">
          <li><?php echo link_to("Daily Sales Report",'invoice/dsr'); ?></li>
          <li><?php echo link_to("Daily Purchases Report",'purchase/dsr'); ?></li>
          <li><?php echo link_to("Uncollected Invoices",'invoice/listunpaid'); ?></li>
          <li><?php echo link_to("Unpaid Purchases",'purchase/listunpaid'); ?></li>
          <li><?php //echo link_to("Salesman Commission",'profitdetail/commission'); ?></li>
          <li><?php //echo link_to("Technician Commission",'invoice/commission'); ?></li>
        </ul>
    </li>
    <?php echo link_to("Unpaid Invoices",'invoice/listunpaid'); ?> | 
    <?php echo link_to("Unpaid Purchases",'purchase/listunpaid'); ?> | 
    <?php echo link_to("Expense Vouchers",'voucher/dashboard'); ?> | 
    <?php echo link_to("Accounting Tool",'voucher/dashboardMultiForAccounting'); ?> | 
<!--
    <?php //echo link_to("Folders",'folder/index'); ?> | 
    <?php //echo link_to("Files",'file/search'); ?> | 
    <?php //echo link_to("DR",'delivery/dashboard'); ?> | 
-->    
    <!--links for sales-->
	<?php }else if($sf_user->hasCredential(array('sales'), false)){?>
    <?php echo link_to("Product Reordering",'productsearch/quotaDashboard'); ?> | 
    <?php echo link_to("Folders",'folder/index'); ?> | 
    <?php echo link_to("Files",'file/search'); ?> | 
    <?php echo link_to("DR",'delivery/dashboard'); ?> | 
	<?php } ?>    
	<?php } //end check if signed in?>
    </ul>
</nav>  

    
    
    
    <p/>
		<table>
		<tr valign=top>
			<td>
				Search:  
			</td>
			<td align=center>
				<!--?php //echo form_tag('productsearch/index') ?>
					<input id="searchstring" name="searchstring">
					<input type=hidden name="transaction_id" value=<?php include_slot('transaction_id') ?>
					<input type=hidden name="transaction_type" value=<?php include_slot('transaction_type') ?>
					<input value="Search Product" type="submit">
				</form-->
				<input id=productsearchinput autocomplete="off" size=8><br>Product
			</td>

			<td align=center>
				<?php echo form_tag('invoice/search') ?>
					<input id="searchstring" name="searchstring" size=8><br><input value="Invoice" type="submit">
				</form>
				<!--input id=invoicesearchinput autocomplete="off"> Search Invoice |-->
			</td>

			<td align=center>
				<?php echo form_tag('purchase/search') ?>
					<input id="searchstring" name="searchstring" size=8><br><input value="PO / Voucher" type="submit">
				</form>
				<!--input id=purchasesearchinput autocomplete="off"> Search Purchase |-->
			</td>
			<td align=center>
				<input id=customersearchinput autocomplete="off" size=8><br>Customer
			</td>
			<td align=center>
				<input id=vendorsearchinput autocomplete="off" size=8><br>Supplier
			</td>
			<td align=center>
				<input id=othersearchinput autocomplete="off" size=8><br>Others
			</td>
		</tr>
		</table>
		<div id="searchresult"></div>
  <hr>
    <?php echo $sf_content ?>
  </body>
<script>

var baseurl="<?php echo "http://".$_SERVER['SERVER_NAME'].str_replace("index.php","",$_SERVER['SCRIPT_NAME'])?>/";

$("#productsearchinput").keyup(function(event){
	//if 3 or more letters in search box
    //if($("#productsearchinput").val().length>=3){

    //if enter key is pressed
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
	    $.ajax({url: baseurl+"productsearch/index?searchstring="+$("#productsearchinput").val()+"&transaction_id=<?php include_slot('transaction_id') ?>&transaction_type=<?php include_slot('transaction_type') ?>", success: function(result){
	 		  $("#searchresult").html(result);
	    }});
    }
    //else clear
    //else
 		//  $("#searchresult").html("");
});
$("#customersearchinput").keyup(function(event){
	//if 3 or more letters in search box
    //if($("#customersearchinput").val().length>=3){
    
    //if enter key is pressed
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
	    $.ajax({url: baseurl+"customer/search?searchstring="+$("#customersearchinput").val(), success: function(result){
	 		  $("#searchresult").html(result);
	    }});
    }
    //else clear
    //else
 		//  $("#searchresult").html("");
});
$("#vendorsearchinput").keyup(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
	    $.ajax({url: baseurl+"vendor/search?searchstring="+$("#vendorsearchinput").val(), success: function(result){
	 		  $("#searchresult").html(result);
	    }});
    }
    //else clear
    //else
 		//  $("#searchresult").html("");
});
$("#invoicesearchinput").keyup(function(){
	//if 3 or more letters in search box
    if($("#invoicesearchinput").val().length>0)
	    $.ajax({url: baseurl+"invoice/search?searchstring="+$("#invoicesearchinput").val(), success: function(result){
	 		  $("#searchresult").html(result);
	    }});
    //else clear
    //else
 		//  $("#searchresult").html("");
});
$("#purchasesearchinput").keyup(function(){
	//if 3 or more letters in search box
    if($("#purchasesearchinput").val().length>0)
	    $.ajax({url: baseurl+"purchase/search?searchstring="+$("#purchasesearchinput").val(), success: function(result){
	 		  $("#searchresult").html(result);
	    }});
});
$("#othersearchinput").keyup(function(event){
    //if enter key is pressed
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13')
    {
	    $.ajax({url: baseurl+"search/search?searchstring="+$("#othersearchinput").val(), success: function(result){
	 		  $("#searchresult").html(result);
	    }});
    }
});
/*
$("#clearsearch").click(function(){
 		  $("#searchresult").html("");
	    //hide all password entry boxes
	    $(".password_tr").attr('hidden',true);
});
*/
//drop down menus
$(document).ready(function() {
    $( '.dropdown' ).hover(
        function(){
            $(this).children('.sub-menu').slideDown(0);
        },
        function(){
            $(this).children('.sub-menu').delay(100).slideUp(0); 
        }
    );
}); // end ready
</script>
  
</html>
