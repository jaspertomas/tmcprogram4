ALTER TABLE `purchasedetail` ADD `is_inspected` TINYINT NOT NULL DEFAULT '0';
ALTER TABLE `counter_receipt` ADD `is_inspected` TINYINT NOT NULL DEFAULT '0';
ALTER TABLE `counter_receipt_detail` ADD `is_inspected` TINYINT NOT NULL DEFAULT '0';

update purchase set is_inspected=0;
