ALTER TABLE `deliverydetail` ADD `discrate` VARCHAR( 20 ) NOT NULL DEFAULT '' AFTER `price` ;
ALTER TABLE `deliverydetail` ADD `discprice` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0' AFTER `discrate` ;
ALTER TABLE `deliverydetail` CHANGE `price` `price` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0',
CHANGE `total` `total` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0';

ALTER TABLE `customer` ADD `discrate` VARCHAR( 20 ) NOT NULL DEFAULT '' after `name`;
ALTER TABLE `vendor` ADD `discrate` VARCHAR( 20 ) NOT NULL DEFAULT '' after `name`;


