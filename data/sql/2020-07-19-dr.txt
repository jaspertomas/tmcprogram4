


CREATE TABLE IF NOT EXISTS `invoice_dr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `description` VARCHAR(50) NULL,
  `datetime` datetime NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `notes` text,
  `is_return` tinyint(4) DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `invoice_id_idx` (`invoice_id`),
  KEY `customer_id` (`customer_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;



CREATE TABLE IF NOT EXISTS `invoice_dr_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `invoice_dr_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `invoicedetail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `is_return` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_dr_id` (`invoice_dr_id`),
  KEY `invoicedetail_id` (`invoicedetail_id`),
  KEY `product_id` (`product_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `purchase_dr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `description` VARCHAR(50) NULL,
  `datetime` datetime NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `notes` text,
  `is_return` tinyint(4) DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `purchase_id_idx` (`purchase_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `purchase_dr_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `purchase_dr_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `purchasedetail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `is_return` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_dr_id` (`purchase_dr_id`),
  KEY `purchasedetail_id` (`purchasedetail_id`),
  KEY `product_id` (`product_id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `vendor_id` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `invoice_dr`
  ADD CONSTRAINT `invoice_dr_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `invoice_dr_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `invoice_dr_ibfk_3` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`);

ALTER TABLE `invoice_dr_detail`
  ADD CONSTRAINT `invoice_dr_detail_ibfk_6` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `invoice_dr_detail_ibfk_1` FOREIGN KEY (`invoice_dr_id`) REFERENCES `invoice_dr` (`id`),
  ADD CONSTRAINT `invoice_dr_detail_ibfk_2` FOREIGN KEY (`invoicedetail_id`) REFERENCES `invoicedetail` (`id`),
  ADD CONSTRAINT `invoice_dr_detail_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `invoice_dr_detail_ibfk_4` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `invoice_dr_detail_ibfk_5` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`);

ALTER TABLE `purchase_dr`
  ADD CONSTRAINT `purchase_dr_ibfk_1` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`id`),
  ADD CONSTRAINT `purchase_dr_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `purchase_dr_ibfk_3` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`);

ALTER TABLE `purchase_dr_detail`
  ADD CONSTRAINT `purchase_dr_detail_ibfk_6` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `purchase_dr_detail_ibfk_1` FOREIGN KEY (`purchase_dr_id`) REFERENCES `purchase_dr` (`id`),
  ADD CONSTRAINT `purchase_dr_detail_ibfk_2` FOREIGN KEY (`purchasedetail_id`) REFERENCES `purchasedetail` (`id`),
  ADD CONSTRAINT `purchase_dr_detail_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `purchase_dr_detail_ibfk_4` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`id`),
  ADD CONSTRAINT `purchase_dr_detail_ibfk_5` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`);

ALTER TABLE `invoicedetail` ADD `qty_released` INT NOT NULL DEFAULT '0' AFTER `qty`;

ALTER TABLE `invoice_dr` ADD `is_released` TINYINT NULL AFTER `is_return`;
ALTER TABLE `invoice_dr_detail` ADD `is_released` TINYINT NULL AFTER `is_return`;
ALTER TABLE `purchase_dr` ADD `is_received` TINYINT NULL AFTER `is_return`;
ALTER TABLE `purchase_dr_detail` ADD `is_received` TINYINT NULL AFTER `is_return`;

ALTER TABLE `invoice_dr_detail` 
ADD `stock_entry_id` INT NULL ,
ADD `created_by_id` INT NOT NULL ,
ADD `updated_by_id` INT NOT NULL ,
ADD `created_at` DATETIME NOT NULL ,
ADD `updated_at` DATETIME NOT NULL;

ALTER TABLE `purchase_dr_detail` 
ADD `stock_entry_id` INT NULL ,
ADD `created_by_id` INT NOT NULL ,
ADD `updated_by_id` INT NOT NULL ,
ADD `created_at` DATETIME NOT NULL ,
ADD `updated_at` DATETIME NOT NULL;

ALTER TABLE `stockentry` ADD `created_at` DATETIME NOT NULL;

update stockentry set created_at=date;


ALTER TABLE `invoice` ADD `is_dr_based` TINYINT NULL DEFAULT '1' AFTER `is_archived`;
ALTER TABLE `purchase` ADD `is_dr_based` TINYINT NULL DEFAULT '1' AFTER `is_archived`;
update invoice set is_dr_based=null;
update purchase set is_dr_based=null;

-- please use invoice_dr/recalcAll and purchase_dr/recalcAll if necessary

ALTER TABLE `invoice_dr` ADD `is_cancelled` TINYINT NULL AFTER `is_released`;
ALTER TABLE `invoice_dr_detail` ADD `is_cancelled` TINYINT NULL AFTER `is_released`;
ALTER TABLE `purchase_dr` ADD `is_cancelled` TINYINT NULL AFTER `is_received`;
ALTER TABLE `purchase_dr_detail` ADD `is_cancelled` TINYINT NULL AFTER `is_received`;

-- ---------------STOCK-------------------
ALTER TABLE `stock` CHANGE `date` `datetime` DATETIME NOT NULL;
ALTER TABLE `stockentry` CHANGE `date` `datetime` DATETIME NOT NULL;
