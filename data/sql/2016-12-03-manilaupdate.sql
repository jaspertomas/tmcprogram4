ALTER TABLE `voucher` ADD FOREIGN KEY ( `voucher_allocation_id` ) REFERENCES `voucher_allocation` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `producttype_schema` CHANGE `barcode_template` `barcode_template` ENUM( 'Simple 1 Column', 'Simple 2 Column', 'Simple 2 Column Green and White', 'Standard 3 Column' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Simple 1 Column';

ALTER TABLE `product` CHANGE `autocalcbuyprice` `autocalcbuyprice` TINYINT( 4 ) NOT NULL DEFAULT '1';

CREATE TABLE IF NOT EXISTS `todo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text,
  `priority` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

