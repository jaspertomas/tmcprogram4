
CREATE TABLE IF NOT EXISTS `counter_receipt_cheque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `counter_receipt_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `cheque_number` varchar(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `counter_receipt_id` (`counter_receipt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `counter_receipt_cheque`
  ADD CONSTRAINT `counter_receipt_cheque_ibfk_1` FOREIGN KEY (`counter_receipt_id`) REFERENCES `counter_receipt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

