ALTER TABLE `voucher_subaccount` 
add `voucher_type_id` int(11) NULL,
add `voucher_allocation_id` int(11) NULL,
add `passbook_id` int(11) NULL,
add `payee` varchar(128) DEFAULT NULL,
add `amount` decimal(10,2) DEFAULT NULL,
add `particulars` tinytext DEFAULT NULL,
add `is_pay_to_self` tinyint(4) DEFAULT NULL;

ALTER TABLE `voucher_subaccount` CHANGE `name` `name` VARCHAR( 100 ) NOT NULL;
ALTER TABLE `voucher_subaccount` ADD `fullname` VARCHAR( 200 ) NULL DEFAULT NULL AFTER `name`;
ALTER TABLE `voucher_subaccount` ADD `notes` TEXT NULL DEFAULT NULL AFTER `particulars`;
ALTER TABLE `voucher_subaccount` ADD `redirect_id` INT NULL DEFAULT NULL AFTER `parent_id`;
