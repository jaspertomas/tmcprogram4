
ALTER TABLE `invoicedetail` ADD `with_commission` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0',
ADD `with_commission_total` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0';

ALTER TABLE `invoicedetail` ADD `is_vat` TINYINT NOT NULL DEFAULT '0';

INSERT INTO `settings` (
`id` ,
`name` ,
`value`
)
VALUES (
NULL , 'vat_product_id', '987'
);

INSERT INTO `settings` (
`id` ,
`name` ,
`value`
)
VALUES (
NULL , 'default_customer_id', '1'
);

