ALTER TABLE `in_check` CHANGE `amount` `amount` DECIMAL( 10, 2 ) NOT NULL ;
ALTER TABLE `out_check` CHANGE `amount` `amount` DECIMAL( 10, 2 ) NOT NULL ;

-- ---------------

ALTER TABLE `passbook_entry` ADD `meta` VARCHAR( 10 ) NULL ,
ADD `description` VARCHAR( 100 ) NULL ;


ALTER TABLE `passbook_entry` CHANGE `transaction_code` `trans_type` ENUM( "CASH/CHECK DEPOSIT ", "CHECK ENCASHMENT ", "CREDIT MEMO ", "DEBIT MEMO ", "IMIN-CLRNG-BAT ", "IN-HOUSE CHECK DEPOSIT ", "INWARD CHECK ", "LOAN PAYMENT ", "LOCAL CHECK DEPOSIT " ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `passbook_entry` ADD `json` TEXT NULL ;

ALTER TABLE `voucher` CHANGE `amount` `amount` DECIMAL( 10, 2 ) NOT NULL ;
ALTER TABLE `product` CHANGE `competitor_price` `competitor_price` DECIMAL( 10, 2 ) NULL DEFAULT NULL ;
ALTER TABLE `replacement_detail` CHANGE `price` `price` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0',
CHANGE `total` `total` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0';
ALTER TABLE `returns_detail` CHANGE `price` `price` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0',
CHANGE `total` `total` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0';
