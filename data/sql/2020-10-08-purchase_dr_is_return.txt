ALTER TABLE `stockentry` CHANGE `type` `type` ENUM('Adjustment','Incoming DR','Outgoing DR','Conversion DR','Invoice','InvoiceReturn','InvoiceReplacement','InvoiceConversion','Purchase','PurchaseReturn','PurchaseReplacement','PurchaseConversion','Report','InvoiceDr','PurchaseDr') NULL DEFAULT NULL;
ALTER TABLE `stockentry` CHANGE `created_at` `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `invoice_dr` CHANGE `qty_for_release` `qty_for_release` INT(11) NOT NULL DEFAULT '0', CHANGE `qty_for_return` `qty_for_return` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `purchase_dr` CHANGE `qty_for_receive` `qty_for_receive` INT(11) NOT NULL DEFAULT '0', CHANGE `qty_for_return` `qty_for_return` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `invoice_dr` ADD `is_return` TINYINT(1) NULL DEFAULT NULL AFTER `notes`;
ALTER TABLE `purchase_dr` ADD `is_return` TINYINT(1) NULL DEFAULT NULL AFTER `notes`;

update `invoice_dr` set is_return=1 WHERE qty_for_return>0 and qty_for_release=0;
update `purchase_dr` set is_return=1 WHERE qty_for_return>0 and qty_for_receive=0;

UPDATE `product` SET `is_service` = '1' WHERE `name` like "%holding tax%";
