
CREATE TABLE IF NOT EXISTS `consignment_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` tinytext,
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`vendor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

ALTER TABLE `consignment_payment`
  ADD CONSTRAINT `consignment_payment_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`);

ALTER TABLE `profitdetail` ADD `consignment_payment_id` INT NULL ;
ALTER TABLE `profitdetail` ADD INDEX ( `consignment_payment_id` ) ;
ALTER TABLE `profitdetail` ADD FOREIGN KEY ( `consignment_payment_id` ) REFERENCES `consignment_payment` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;


