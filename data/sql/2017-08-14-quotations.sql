
CREATE TABLE IF NOT EXISTS `quotation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `salesman_id` int(11) NOT NULL,
  `opening` text,
  `closing` text,
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `salesman_id` (`salesman_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `quotationdetail`
--

CREATE TABLE IF NOT EXISTS `quotationdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_discounted` tinyint(4) DEFAULT NULL,
  `unittotal` tinyint(4) DEFAULT NULL,
  `discrate` varchar(20) NOT NULL DEFAULT '0',
  `discamt` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `quotation_id` (`quotation_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

ALTER TABLE `quotation`
  ADD CONSTRAINT `quotation_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `quotation_ibfk_2` FOREIGN KEY (`salesman_id`) REFERENCES `employee` (`id`);

ALTER TABLE `quotationdetail`
  ADD CONSTRAINT `quotationdetail_ibfk_1` FOREIGN KEY (`quotation_id`) REFERENCES `quotation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `quotationdetail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE;

