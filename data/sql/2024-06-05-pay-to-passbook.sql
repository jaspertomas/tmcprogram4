

CREATE TABLE IF NOT EXISTS passbook_pay_to (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  account_no varchar(20) NOT NULL,
  location varchar(30) NOT NULL,
  currentqty decimal(10,2) NOT NULL DEFAULT '0.00',
  datetime datetime NOT NULL,
  is_updated tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table passbook
--

INSERT INTO passbook_pay_to (id, name, account_no, location, currentqty, datetime, is_updated) VALUES
(2, '(Closed) MBTC Lily / Jonathan Tomas', '026-7-026-51253-7', 'mbtc soler', '679173.91', '0000-00-00 00:00:00', NULL),
(3, 'MBTC Tradewind Water System', '026-7-026-51652-4', 'mbtc soler', '195861.96', '0000-00-00 00:00:00', NULL),
(4, '(Do Not Use) BDO Lily / Jonathan Tomas', 'x', 'manila', '-40750.00', '0000-00-00 00:00:00', NULL),
(5, 'MBTC Tacloban', '007-122-52947-5', 'Visayas', '-600.00', '0000-00-00 00:00:00', NULL),
(6, 'MBTC Lily Ong Tomas', '026-7-026-51904-3', 'MBTC Arranque', '-62046.00', '0000-00-00 00:00:00', NULL),
(7, 'BDO Lily O Tomas Personal', '002610031749', 'BDO Arranque', '2140234.39', '0000-00-00 00:00:00', NULL),
(8, 'Supplier', 'x', 'x', '292175.60', '0000-00-00 00:00:00', NULL),
(9, 'Union Bank', '001680005665', 'Soler', '11750.00', '0000-00-00 00:00:00', NULL);


ALTER TABLE out_check ADD passbook_pay_to_id INT NULL AFTER passbook_entry_id;
ALTER TABLE out_check ADD INDEX ( passbook_pay_to_id );
ALTER TABLE out_check ADD FOREIGN KEY ( passbook_pay_to_id ) REFERENCES passbook_pay_to (
id
);