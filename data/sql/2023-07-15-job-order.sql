

CREATE TABLE IF NOT EXISTS `job_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  -- `invnos` varchar(100) DEFAULT NULL,
  -- `invoice_ids` varchar(100) DEFAULT NULL,
  `notes` text,
  `date` date NOT NULL,
  `date_completed` date DEFAULT NULL,
  `is_temporary` tinyint(4) NOT NULL DEFAULT '0',
  `is_completed` tinyint(4) NOT NULL DEFAULT '0',
  `is_cancelled` tinyint(4) NOT NULL DEFAULT '0',
  `was_closed` tinyint(1) NOT NULL DEFAULT '0',
  -- `is_stock` tinyint(1) DEFAULT NULL,
  `production_status` enum('Pending','Partial','Complete') NOT NULL DEFAULT 'Pending',
  `meta` text null,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `job_order_conversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_order_id` int(11) NOT NULL,
  `conversion_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `description` tinytext,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_order_id` (`job_order_id`),
  KEY `conversion_id` (`conversion_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `job_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_order_id` int(11) NOT NULL,
  `description` text,
  `qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_produced` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  -- `barcode` varchar(13) DEFAULT NULL,
  `is_cancelled` tinyint(4) NOT NULL DEFAULT '0',
  `remaining` decimal(10,2) DEFAULT '0.00',
  `color` enum('red','orange','yellow','green','blue','indigo','violet') DEFAULT NULL,
  `slot` int(4) NOT NULL DEFAULT '0',
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id_idx` (`product_id`),
  KEY `job_order_id_idx` (`job_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `job_order_dr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `job_order_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `qty_to_produce` int(11) NOT NULL DEFAULT '0',
  `qty_to_consume` int(11) NOT NULL DEFAULT '0',
  `notes` text,
  `is_produced` tinyint(4) NOT NULL DEFAULT '0',
  `is_cancelled` tinyint(4) NOT NULL DEFAULT '0',
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `job_order_id_idx` (`job_order_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `job_order_dr_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `job_order_dr_id` int(11) NOT NULL,
  `job_order_id` int(11) NOT NULL,
  `job_order_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `is_produced` tinyint(4) NOT NULL DEFAULT '0',
  `is_cancelled` tinyint(4) NOT NULL DEFAULT '0',
  `stock_entry_id` int(11) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `updated_by_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_order_dr_id` (`job_order_dr_id`),
  KEY `job_order_detail_id` (`job_order_detail_id`),
  KEY `product_id` (`product_id`),
  KEY `job_order_id` (`job_order_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
--  Constraints for dumped tables
-- 

-- 
--  Constraints for table `job_order_conversion`
-- 
ALTER TABLE `job_order_conversion`
  ADD CONSTRAINT `job_order_conversion_ibfk_1` FOREIGN KEY (`job_order_id`) REFERENCES `job_order` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `job_order_conversion_ibfk_2` FOREIGN KEY (`conversion_id`) REFERENCES `conversion` (`id`) ON UPDATE CASCADE;

-- 
--  Constraints for table `job_order_detail`
-- 
ALTER TABLE `job_order_detail`
  ADD CONSTRAINT `job_order_detail_ibfk_1` FOREIGN KEY (`job_order_id`) REFERENCES `job_order` (`id`),
  ADD CONSTRAINT `job_order_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

-- 
--  Constraints for table `job_order_dr`
-- 
ALTER TABLE `job_order_dr`
  ADD CONSTRAINT `job_order_dr_ibfk_1` FOREIGN KEY (`job_order_id`) REFERENCES `job_order` (`id`),
  ADD CONSTRAINT `job_order_dr_ibfk_3` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`);

-- 
--  Constraints for table `job_order_dr_detail`
-- 
ALTER TABLE `job_order_dr_detail`
  ADD CONSTRAINT `job_order_dr_detail_ibfk_1` FOREIGN KEY (`job_order_dr_id`) REFERENCES `job_order_dr` (`id`),
  ADD CONSTRAINT `job_order_dr_detail_ibfk_2` FOREIGN KEY (`job_order_detail_id`) REFERENCES `job_order_detail` (`id`),
  ADD CONSTRAINT `job_order_dr_detail_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `job_order_dr_detail_ibfk_4` FOREIGN KEY (`job_order_id`) REFERENCES `job_order` (`id`),
  ADD CONSTRAINT `job_order_dr_detail_ibfk_5` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`);
