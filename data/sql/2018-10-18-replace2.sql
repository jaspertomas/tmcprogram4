ALTER TABLE `stockentry` CHANGE `type` `type` ENUM( 'Adjustment', 'Incoming DR', 'Outgoing DR', 'Conversion DR', 'Invoice', 'InvoiceReturn', 'InvoiceReplacement', 'InvoiceConversion', 'Purchase', 'PurchaseReturn', 'PurchaseReplacement', 'PurchaseConversion' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;

