ALTER TABLE  `stockentry` ADD  `is_report` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `type`;
UPDATE stockentry SET is_report =1 WHERE TYPE =  "Report";
-- then do stock recalc

-- for counter receipt
ALTER TABLE counter_receipt DROP INDEX cheque_number;
ALTER TABLE `counter_receipt` DROP `cheque_number` ,
DROP `cheque_date` ;

ALTER TABLE  `counter_receipt_detail` CHANGE  `invoice_number`  `invoice_number` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
