
CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_class` varchar(20) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `description` tinytext,
  `filename` varchar(50) DEFAULT NULL,
  `file` varchar(100) NOT NULL,
  `keywords` text,
  `is_folder` tinyint(4) NOT NULL DEFAULT '0',
  `filesize` int(11) DEFAULT '0',
  `filetype` varchar(100) DEFAULT NULL,
  `uploadlocation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

