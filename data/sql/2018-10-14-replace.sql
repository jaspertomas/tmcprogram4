--
--

-- ALTER TABLE `purchasedetail` ADD `qty_replaced` INT NOT NULL default 0;
-- ALTER TABLE `invoicedetail` ADD `qty_replaced` INT NOT NULL default 0;
ALTER TABLE `returns_detail` ADD `qty_replaced` INT NOT NULL default 0;

CREATE TABLE IF NOT EXISTS `replacement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Invoice','Purchase', 'Customer', 'Vendor', '') DEFAULT 'Invoice',
  `code` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `ref_class` enum('Invoice','Purchase', '') DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `client_class` enum('Customer','Vendor') DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `returns_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `invno` varchar(20) DEFAULT NULL,
  `pono` varchar(20) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `status` enum('Pending','Complete','Cancelled') DEFAULT 'Complete',
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `warehouse_id_idx` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `replacement_detail`
--

CREATE TABLE IF NOT EXISTS `replacement_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_class` enum('Invoicedetail','Purchasedetail') DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `replacement_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL DEFAULT '0',
  `total` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `replacement_id` (`replacement_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `replacement_detail` ADD FOREIGN KEY ( `replacement_id` ) REFERENCES `replacement` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `replacement_detail` ADD FOREIGN KEY ( `product_id` ) REFERENCES `product` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `replacement` ADD INDEX ( `returns_id` ) ;

ALTER TABLE `replacement` ADD FOREIGN KEY ( `returns_id` ) REFERENCES `returns` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `replacement` ADD FOREIGN KEY ( `warehouse_id` ) REFERENCES `warehouse` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `returns` ADD FOREIGN KEY ( `warehouse_id` ) REFERENCES `warehouse` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `replacement_detail` ADD `returns_detail_id` INT NOT NULL AFTER `ref_id` ;
ALTER TABLE `replacement_detail` ADD INDEX ( `returns_detail_id` ) ;
ALTER TABLE `replacement_detail` ADD FOREIGN KEY ( `returns_detail_id` ) REFERENCES `returns_detail` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;
