
CREATE TABLE IF NOT EXISTS `voucher_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ;

--
-- Dumping data for table `account`
--

INSERT INTO `voucher_account` (`id`, `name`) VALUES
(22, 'Travel'),
(23, 'Merchandise Expense'),
(24, 'Medical'),
(25, 'Equipment & Supplies'),
(26, 'Taxes and Fees'),
(27, 'Insurance'),
(30, 'Utils-Water'),
(31, 'Utils-Landline'),
(32, 'Utils-Mobile'),
(33, 'Utils-Electricity'),
(34, 'Food'),
(35, 'Miscellaneous'),
(36, 'Salary & Commission');

-- delete foreign key to account.id
ALTER TABLE `voucher` DROP FOREIGN KEY `voucher_ibfk_1` ;
-- create new foreign key to voucher_account.id
ALTER TABLE `voucher` ADD FOREIGN KEY ( `account_id` ) REFERENCES `voucher_account` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

-- todo: select * from voucher where account_id=24, look around for insurance stuff, change to 27 (insurance)

-- todo: make sure petty cash accounts are all greater than 21

-- delete voucher accounts from accounts table
delete from account where id >21;
-- delete petty cash from accounts category table
delete from account_category where id =13;

