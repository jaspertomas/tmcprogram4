--customer notes field is too short (60 chars). this will make it a lot longer. -----
ALTER TABLE `customer` CHANGE `notepad` `notepad` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;

--now there is no more such thing as a cancelled stock entry. -----
--stock entries are deleted when invoices are cancelled------------
ALTER TABLE `stockentry` DROP `is_cancelled` ;
ALTER TABLE `quote` DROP `is_cancelled` ;

--created producttype schema system----------

CREATE TABLE IF NOT EXISTS `producttype_schema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `product_name_format` varchar(100) DEFAULT NULL,
  `product_description_format` varchar(100) DEFAULT NULL,
  `barcode_format` varchar(100) DEFAULT NULL,
  `barcode_format_2` varchar(100) DEFAULT NULL,
  `max_buy_formula` varchar(100) DEFAULT NULL,
  `min_buy_formula` varchar(100) DEFAULT NULL,
  `max_sell_formula` varchar(100) DEFAULT NULL,
  `min_sell_formula` varchar(100) DEFAULT NULL,
  `base_formula` varchar(100) DEFAULT NULL,
  `speccount` int(11) NOT NULL DEFAULT '0',
  `spec1` varchar(30) DEFAULT NULL,
  `spec2` varchar(30) DEFAULT NULL,
  `spec3` varchar(30) DEFAULT NULL,
  `spec4` varchar(30) DEFAULT NULL,
  `spec5` varchar(30) DEFAULT NULL,
  `spec6` varchar(30) DEFAULT NULL,
  `spec7` varchar(30) DEFAULT NULL,
  `spec8` varchar(30) DEFAULT NULL,
  `spec9` varchar(30) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
);
INSERT INTO `producttype_schema` (`id`, `name`, `description`, `product_name_format`, `product_description_format`, `barcode_format`, `barcode_format_2`, `max_buy_formula`, `min_buy_formula`, `max_sell_formula`, `min_sell_formula`, `base_formula`, `speccount`, `spec1`, `spec2`, `spec3`, `spec4`, `spec5`, `spec6`, `spec7`, `spec8`, `spec9`, `notes`) VALUES (NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
ALTER TABLE `producttype_schema` ADD `barcode_template` ENUM( 'Simple 1 Column', 'Simple 2 Column', 'Simple 2 Column Green and White', 'Standard 3 Column' ) NOT NULL DEFAULT 'Simple 1 Column' AFTER `barcode_format_2` 
ALTER TABLE `producttype` ADD `barcode_format_2` VARCHAR( 100 ) NULL AFTER `barcode_format` ;
ALTER TABLE `producttype` ADD `barcode_format_3` VARCHAR( 100 ) NULL AFTER `barcode_format_2` ;
ALTER TABLE `producttype` ADD `producttype_schema_id` INT NOT NULL DEFAULT '1' AFTER `parent_id` ;
ALTER TABLE `producttype` ADD INDEX ( `producttype_schema_id` ) ;
ALTER TABLE `producttype` ADD FOREIGN KEY ( `producttype_schema_id` ) REFERENCES `producttype_schema` (
`id`
) ON DELETE RESTRICT ON UPDATE CASCADE ;



