ALTER TABLE `in_check` CHANGE `check_no` `check_no` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;

ALTER TABLE `out_check` CHANGE `check_no` `check_no` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;
-- ---------------

ALTER TABLE `voucher` ADD `out_check_id` INT NULL ;
ALTER TABLE `voucher` ADD INDEX ( `out_check_id` ) ;
ALTER TABLE `voucher` ADD FOREIGN KEY ( `out_check_id` ) REFERENCES `out_check` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

CREATE TABLE IF NOT EXISTS `out_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `parent_class` varchar(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_name` varchar(20) DEFAULT NULL,
  `child_class` varchar(20) DEFAULT NULL,
  `children_id` varchar(20) DEFAULT NULL,
  `date` date NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `detail1` varchar(20) DEFAULT NULL,
  `detail2` varchar(20) DEFAULT NULL,
  `detail3` varchar(20) DEFAULT NULL,
  `notes` text,
  `checkcleardate` date DEFAULT NULL,
  `out_check_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `out_check_id` (`out_check_id`)
);

ALTER TABLE `out_payment`
  ADD CONSTRAINT `out_payment_ibfk_1` FOREIGN KEY (`out_check_id`) REFERENCES `out_check` (`id`);

-- supplier invoice number no longer unique on counter receipt
`ALTER TABLE counter_receipt_detail DROP INDEX vendor_id_2;`

-- remove foregn key on purchase id
-- counter receipt detail becomes in between for many to many of counter receipt and purchase
ALTER TABLE `purchase` DROP FOREIGN KEY `purchase_ibfk_3` ;
ALTER TABLE `purchase` DROP FOREIGN KEY `purchase_ibfk_4` ;
ALTER TABLE `purchase` DROP `counter_receipt_id` ,
DROP `counter_receipt_detail_id` ;

-- -----COUNTER RECEIPT----------
//out_cheque replace functionality of counter_receipt_cheque
DROP TABLE `counter_receipt_cheque`;

ALTER TABLE `counter_receipt` ADD `balance` DECIMAL( 10, 2 ) NULL DEFAULT '0' AFTER `amount` ,
ADD `status` ENUM( 'Pending', 'Paid', 'Cancelled', 'Overpaid' ) NULL DEFAULT 'Pending' AFTER `balance` ;


ALTER TABLE `out_check` 
  ADD `is_bank_transfer` TINYINT( 1 ) NOT NULL DEFAULT '0',
  ADD `remaining` decimal(10,2) DEFAULT '0.00';

ALTER TABLE `out_check` CHANGE `clear_date` `cleared_date` DATE NULL DEFAULT NULL ,
CHANGE `notes` `notes` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;


ALTER TABLE `counter_receipt_detail` ADD `is_paid` TINYINT( 1 ) NOT NULL DEFAULT '0';
