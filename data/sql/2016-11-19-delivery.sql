--
-- Table structure for table `conversion`
--

CREATE TABLE IF NOT EXISTS `conversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

ALTER TABLE `conversion` ADD `description` TINYTEXT NULL ;
-- ALTER TABLE `invoicedetail` ADD `undelivered_qty` INT NOT NULL DEFAULT '0';
-- ALTER TABLE `purchasedetail` ADD `undelivered_qty` INT NOT NULL DEFAULT '0';
-- if invoice.is_temporary=0, stock entries exist. set invoicedetail.undelivered_qty = 0
-- else set invoicedetail.undelivered_qty = qty
-- update invoicedetail join invoice ... where is_temporary==0

-- --------------------------------------------------------

--
-- Table structure for table `conversiondetail`
--

CREATE TABLE IF NOT EXISTS `conversiondetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversion_id` int(11) NOT NULL,
  `type` enum('from','to') NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `conversion_id` (`conversion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Incoming','Outgoing','Conversion','Transfer') DEFAULT 'Incoming',
  `code` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `ref_class` enum('invoice','purchase','delivery') DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `client_class` enum('customer','vendor','warehouse') DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `invno` varchar(20) DEFAULT NULL,
  `pono` varchar(20) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `status` enum('Pending','Complete','Cancelled') DEFAULT 'Complete',
  `delivery_method` enum('Pickup','Delivery') NOT NULL DEFAULT 'Delivery',
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `warehouse_id_idx` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliverydetail`
--

CREATE TABLE IF NOT EXISTS `deliverydetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_class` enum('invoicedetail','purchasedetail') DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `delivery_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL DEFAULT '0',
  `total` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `delivery_id` (`delivery_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;




CREATE TABLE IF NOT EXISTS `deliveryconversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) NOT NULL,
  `conversion_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`),
  KEY `delivery_id` (`delivery_id`),
  KEY `conversion_id` (`conversion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `deliveryconversion`
--
ALTER TABLE `deliveryconversion`
  ADD CONSTRAINT `deliveryconversion_ibfk_2` FOREIGN KEY (`conversion_id`) REFERENCES `conversion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `deliveryconversion_ibfk_1` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON UPDATE CASCADE;


--
-- Constraints for table `conversiondetail`
--
ALTER TABLE `conversiondetail`
  ADD CONSTRAINT `conversiondetail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `conversiondetail_ibfk_1` FOREIGN KEY (`conversion_id`) REFERENCES `conversion` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `delivery`
--
ALTER TABLE `delivery`
  ADD CONSTRAINT `delivery_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `deliverydetail`
--
ALTER TABLE `deliverydetail`
  ADD CONSTRAINT `deliverydetail_ibfk_1` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `deliverydetail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE;

ALTER TABLE `deliverydetail` CHANGE `ref_class` `ref_class` ENUM( 'invoicedetail', 'purchasedetail', 'deliveryconversion' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;

ALTER TABLE `stockentry` CHANGE `type` `type` VARCHAR( 15 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
