-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 20, 2017 at 03:04 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Table structure for table `commission_payment`
--


CREATE TABLE IF NOT EXISTS `commission_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `date_created` date DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commission_payment`
--
ALTER TABLE `commission_payment`
  ADD CONSTRAINT `commission_payment_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON UPDATE CASCADE;

-- ----------------------------------------------
ALTER TABLE `invoice` ADD `commission_payment_id` INT NULL ;

ALTER TABLE `invoice` ADD INDEX ( `commission_payment_id` ) ;

ALTER TABLE `invoice` ADD FOREIGN KEY ( `commission_payment_id` ) REFERENCES `commission_payment` (
`id`
) ON DELETE RESTRICT ON UPDATE CASCADE ;

-- ------------------------------------------------

ALTER TABLE `employee` CHANGE `commission` `commission` DECIMAL( 5, 2 ) NOT NULL ;
