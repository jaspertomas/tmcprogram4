ALTER TABLE `invoice` CHANGE `saletype` `saletype` ENUM( 'Cash', 'Cheque', 'Account', 'Other','Partial' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Cash';
update invoice set saletype="Partial" where saletype="Other";
ALTER TABLE `invoice` CHANGE `saletype` `saletype` ENUM( 'Cash', 'Cheque', 'Account', 'Partial' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Cash';

ALTER TABLE `purchase` CHANGE `type` `type` ENUM( 'Cash', 'Cheque', 'Account', 'Other','Partial' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Cash';
update purchase set type="Partial" where type="Other";
ALTER TABLE `purchase` CHANGE `type` `type` ENUM( 'Cash', 'Cheque', 'Account', 'Partial' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Cash';

