CREATE TABLE IF NOT EXISTS `transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `notes` text,
  `date` date NOT NULL,
  `warehouse_vector_id` int(11) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `to_warehouse_id` int(11) DEFAULT NULL,
  `date_completed` date DEFAULT NULL,
  `is_temporary` tinyint(4) NOT NULL DEFAULT '0',
  `is_completed` tinyint(4) NOT NULL DEFAULT '0',
  `is_cancelled` tinyint(4) NOT NULL DEFAULT '0',
  `was_closed` tinyint(1) NOT NULL DEFAULT '0',
  `transfer_status` enum('Pending','Partial','Complete') NOT NULL DEFAULT 'Pending',
  `meta` text,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `warehouse_vector_id` (`warehouse_vector_id`),
  KEY `to_warehouse_id` (`to_warehouse_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;



CREATE TABLE IF NOT EXISTS `transfer_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_id` int(11) NOT NULL,
  `description` text,
  `qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_transferred` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `is_cancelled` tinyint(4) NOT NULL DEFAULT '0',
  `remaining` decimal(10,2) DEFAULT '0.00',
  `color` enum('red','orange','yellow','green','blue','indigo','violet') DEFAULT NULL,
  `slot` int(4) NOT NULL DEFAULT '0',
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id_idx` (`product_id`),
  KEY `transfer_id_idx` (`transfer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;


drop table IF EXISTS transfer_dr;
CREATE TABLE `transfer_dr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `description` tinytext,
  `datetime` datetime NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `qty_to_transfer` int(11) NOT NULL DEFAULT '0',
  `notes` text,
  `is_transferred` tinyint(4) DEFAULT NULL,
  `is_cancelled` tinyint(4) DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `updated_by_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `transfer_dr_ibfk_1` (`transfer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;


drop table IF EXISTS transfer_dr_detail;
CREATE TABLE `transfer_dr_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `transfer_dr_id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `transfer_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `is_transferred` tinyint(4) DEFAULT NULL,
  `is_cancelled` tinyint(4) DEFAULT NULL,
  `stock_entry_id` int(11) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `updated_by_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transfer_dr_id` (`transfer_dr_id`),
  KEY `product_id` (`product_id`),
  KEY `transfer_dr_detail_ibfk_2` (`transfer_detail_id`),
  KEY `transfer_dr_detail_ibfk_4` (`transfer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;


ALTER TABLE `transfer`
  ADD CONSTRAINT `transfer_ibfk_1` FOREIGN KEY (`warehouse_vector_id`) REFERENCES `warehouse_vector` (`id`),
  ADD CONSTRAINT `transfer_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`),
  ADD CONSTRAINT `transfer_ibfk_3` FOREIGN KEY (`to_warehouse_id`) REFERENCES `warehouse` (`id`);

ALTER TABLE `transfer_detail`
  ADD CONSTRAINT `transfer_detail_ibfk_1` FOREIGN KEY (`transfer_id`) REFERENCES `transfer` (`id`),
  ADD CONSTRAINT `transfer_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

ALTER TABLE `transfer_dr`
  ADD CONSTRAINT `transfer_dr_ibfk_1` FOREIGN KEY (`transfer_id`) REFERENCES `transfer` (`id`);

ALTER TABLE `transfer_dr_detail`
  ADD CONSTRAINT `transfer_dr_detail_ibfk_1` FOREIGN KEY (`transfer_dr_id`) REFERENCES `transfer_dr` (`id`),
  ADD CONSTRAINT `transfer_dr_detail_ibfk_2` FOREIGN KEY (`transfer_detail_id`) REFERENCES `transfer_detail` (`id`),
  ADD CONSTRAINT `transfer_dr_detail_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `transfer_dr_detail_ibfk_4` FOREIGN KEY (`transfer_id`) REFERENCES `transfer` (`id`);


ALTER TABLE `product` CHANGE `productcategory` `productcategory` ENUM( '', 'TJL', 'Sheet' ) NOT NULL DEFAULT '';