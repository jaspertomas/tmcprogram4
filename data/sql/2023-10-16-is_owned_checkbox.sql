ALTER TABLE `customer` CHANGE `is_owned` `is_owned` tinyint(1) NULL DEFAULT NULL;
update customer set is_owned=null where is_owned=0;
ALTER TABLE `customer` CHANGE `is_suki` `is_suki` tinyint(1) NULL DEFAULT NULL;
update customer set is_suki=null where is_suki=0;