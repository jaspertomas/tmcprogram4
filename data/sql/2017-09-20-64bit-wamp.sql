ALTER TABLE `product` DROP `created_at` ,
DROP `updated_at` ;

ALTER TABLE `quote` CHANGE `discrate` `discrate` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `discamt` `discamt` DECIMAL( 10, 2 ) NULL ;

ALTER TABLE `deliverydetail` CHANGE `description` `description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `invoicedetail` CHANGE `description` `description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `purchasedetail` CHANGE `description` `description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;

