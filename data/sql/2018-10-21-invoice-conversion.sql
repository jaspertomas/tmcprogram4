ALTER TABLE `product` ADD `conversion_id` INT NULL ;
ALTER TABLE `product` ADD INDEX ( `conversion_id` ) ;
ALTER TABLE `product` ADD FOREIGN KEY ( `conversion_id` ) REFERENCES `conversion` (
`id`
) ON DELETE SET NULL ON UPDATE CASCADE ;

ALTER TABLE `deliverydetail` DROP FOREIGN KEY `deliverydetail_ibfk_1` ;
ALTER TABLE `deliverydetail` DROP INDEX `delivery_id` ;
ALTER TABLE `deliverydetail` CHANGE `delivery_id` `parent_id` INT( 11 ) NOT NULL ;
ALTER TABLE `deliverydetail` ADD `parent_class` ENUM( 'Invoice', 'Purchase', 'Delivery' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `ref_id` ;
update deliverydetail set parent_class="Delivery";

ALTER TABLE `deliveryconversion` DROP FOREIGN KEY `deliveryconversion_ibfk_1` ;
ALTER TABLE `deliveryconversion` DROP INDEX `delivery_id` ;
ALTER TABLE `deliveryconversion` CHANGE `delivery_id` `parent_id` INT( 11 ) NOT NULL ;
ALTER TABLE `deliveryconversion` ADD `parent_class` ENUM( 'Invoice', 'Purchase', 'Delivery' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `id` ;
update deliveryconversion set parent_class="Delivery";

ALTER TABLE `deliveryconversion` ADD `ref_class` VARCHAR( 20 ) NULL AFTER `parent_id` ;
ALTER TABLE `deliveryconversion` ADD `ref_id` INT( 11 ) NULL AFTER `ref_class` ;

