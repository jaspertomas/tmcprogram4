ALTER TABLE `invoice` 
ADD `total_for_pumps` DECIMAL( 10, 2 ) NULL after total_for_product_category,
ADD `total_for_tanks` DECIMAL( 10, 2 )  NULL after total_for_product_category;