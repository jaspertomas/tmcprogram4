ALTER TABLE `invoice_template` ADD `is_invoice` TINYINT( 1 ) NULL DEFAULT NULL ,
ADD `is_dr` TINYINT( 1 ) NULL DEFAULT NULL ;

-- please set is_invoice and is_dr to appropriate values 

ALTER TABLE `purchase_template` ADD `is_po` TINYINT( 1 ) NULL DEFAULT NULL ,
ADD `is_return` TINYINT( 1 ) NULL DEFAULT NULL ;

-- please set is_po and is_backload to appropriate values 

ALTER TABLE `invoice` ADD `is_archived` TINYINT( 1 ) NULL DEFAULT 0;
ALTER TABLE `purchase` ADD `is_archived` TINYINT( 1 ) NULL DEFAULT 0;

ALTER TABLE `invoice_template` ADD `prefix` VARCHAR( 10 ) NULL DEFAULT NULL;
ALTER TABLE `purchase_template` ADD `prefix` VARCHAR( 10 ) NULL DEFAULT NULL;
ALTER TABLE `purchase` CHANGE `pono` `pono` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `purchase` ADD `terms` INT NOT NULL DEFAULT 0;
ALTER TABLE `invoice` ADD `terms` INT NOT NULL DEFAULT 0;
ALTER TABLE `purchase` ADD `delivery_status` ENUM( 'Pending', 'Incomplete', 'Received' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Pending';

ALTER TABLE `purchasedetail` ADD `qty_received` INT NOT NULL DEFAULT '0' AFTER `qty` ;

ALTER TABLE `purchase` ADD `invoice_id` INT NULL;
ALTER TABLE `purchase` ADD `is_stock` TINYINT( 1 ) NULL DEFAULT NULL;
