-- make check number for out_check unique
update out_check set check_no=id where check_no is null or check_no = "" or check_no = "1";
ALTER TABLE `out_check` CHANGE `check_no` `check_no` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `out_check` ADD UNIQUE (
`check_no`
)