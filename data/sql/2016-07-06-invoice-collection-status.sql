ALTER TABLE `invoice` ADD `collection_status` ENUM( 'Due', 'Bill Sent', 'Cheque Ready' ) NOT NULL DEFAULT 'Due';
ALTER TABLE `purchase` ADD `collection_status` ENUM( 'Due', 'Counter Received', 'Cheque Ready' ) NOT NULL DEFAULT 'Due';

or

ALTER TABLE `invoice` CHANGE `collection_status` `collection_status` ENUM( 'Due', 'Bill Sent', 'Cheque Ready' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Due';
ALTER TABLE `purchase` CHANGE `collection_status` `collection_status` ENUM( 'Due', 'Counter Received', 'Cheque Ready' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Due';

ALTER TABLE `customer` ADD `terms` INT NOT NULL DEFAULT '0';
ALTER TABLE `vendor` ADD `terms` INT NOT NULL DEFAULT '0';

ALTER TABLE `invoice` DROP FOREIGN KEY `invoice_terms_id_terms_id` ;
ALTER TABLE `invoice` DROP `terms_id` ;
ALTER TABLE `invoice` DROP `datepaid` ;
ALTER TABLE `purchase` DROP FOREIGN KEY `purchase_terms_id_terms_id` ;
ALTER TABLE `purchase` DROP `terms_id` ;


-- calculate all invoice.duedate
UPDATE invoice LEFT JOIN customer ON invoice.customer_id = customer.id SET invoice.duedate = DATE_ADD( invoice.date, INTERVAL customer.terms DAY );
UPDATE purchase LEFT JOIN vendor ON purchase.vendor_id = vendor.id SET purchase.duedate = DATE_ADD( purchase.date, INTERVAL vendor.terms DAY );

-- make vendor structure more like customer
ALTER TABLE `vendor` CHANGE `taxid` `tin_no` VARCHAR( 60 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `vendor` CHANGE `note` `notes` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
