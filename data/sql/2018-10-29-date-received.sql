ALTER TABLE `invoice` ADD `datereleased` DATE NOT NULL AFTER `date` ;
update invoice set datereleased=date;

ALTER TABLE `invoice` ADD `transaction_code` VARCHAR( 20 ) NOT NULL AFTER `invno` ;
update invoice set transaction_code= LPAD(id,6,'0');
ALTER TABLE `invoice` ADD UNIQUE (`transaction_code`);

UPDATE returns SET code = REPLACE( code, "-", "" ) ;
UPDATE replacement SET code = REPLACE( code, "-", "" ) ;
