

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE IF NOT EXISTS `voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` varchar(8) NOT NULL,
  `account_id` int(11) NOT NULL,
  `voucher_type_id` int(11) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `particulars` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `voucher_type_id` (`voucher_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `voucher` ADD `voucher_allocation_id` INT NOT NULL AFTER `voucher_type_id` ;
-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `voucher_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `voucher_type`
--

INSERT INTO `voucher_type` (`id`, `name`) VALUES
(1, 'Petty Cash'),
(2, 'Cheque'),
(3, 'Bank Transfer'),
(4, 'Personal'),
(5, 'Other');

-- ---------------------------------------------------


--
--

CREATE TABLE IF NOT EXISTS `voucher_allocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `voucher_type`
--

INSERT INTO `voucher_allocation` (`id`, `name`) VALUES
(1, 'Tradewind'),
(2, 'Seawings'),
(3, 'Malabon');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `voucher`
--
ALTER TABLE `voucher`
  ADD CONSTRAINT `voucher_ibfk_2` FOREIGN KEY (`voucher_type_id`) REFERENCES `voucher_type` (`id`),
  ADD CONSTRAINT `voucher_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`);


INSERT INTO `account` (`id`, `code`, `name`, `account_type_id`, `account_category_id`, `is_special`, `currentqty`, `date`) VALUES
(22, 'Travel', 'Travel', 5, NULL, NULL, NULL, NULL),
(23, 'Insurance', 'Insurance', 5, NULL, NULL, NULL, NULL),
(24, 'Medical', 'Medical', 5, NULL, NULL, NULL, NULL),
(25, 'Construction', 'Construction', 5, NULL, NULL, NULL, NULL),
(26, 'Taxes and Fees', 'Taxes and Fees', 5, NULL, NULL, NULL, NULL),
(30, 'Utils-Water', 'Utils-Water', 5, NULL, NULL, NULL, NULL),
(31, 'Utils-Landline', 'Utils-Landline', 5, NULL, NULL, NULL, NULL),
(32, 'Utils-Mobile', 'Utils-Mobile', 5, NULL, NULL, NULL, NULL),
(33, 'Utils-Electricity', 'Utils-Electricity', 5, NULL, NULL, NULL, NULL),
(34, 'Food', 'Food', 5, NULL, NULL, NULL, NULL),
(35, 'Miscellaneous', 'Miscellaneous', 5, NULL, NULL, NULL, NULL),
(36, 'Salary', 'Salary', 5, NULL, NULL, NULL, NULL);

INSERT INTO `account_category` (`id` ,`name` ,`code` ,`account_type_id` ,`parent_code`)VALUES (13 , 'Petty Cash Accounts', '', '5', NULL);
UPDATE account SET account_category_id =13 WHERE id >20;


-- -update----------------------
ALTER TABLE `voucher` ADD FOREIGN KEY ( `voucher_allocation_id` ) REFERENCES `voucher_allocation` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `producttype_schema` CHANGE `barcode_template` `barcode_template` ENUM( 'Simple 1 Column', 'Simple 2 Column', 'Simple 2 Column Green and White', 'Standard 3 Column' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Simple 1 Column';
