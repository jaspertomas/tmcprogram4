ALTER TABLE `invoice` CHANGE `saletype` `saletype` ENUM( 'Cash', 'Cheque', 'Account', 'Partial', 'Bank Transfer' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Cash';
ALTER TABLE `purchase` CHANGE `type` `type` ENUM( 'Cash', 'Cheque', 'Account', 'Partial', 'Bank Transfer' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Account';

#create is_updated to take over duties of status
#deprecate status until better use is found for it
ALTER TABLE `producttype` ADD `is_updated` TINYINT(1) NULL;
UPDATE producttype SET is_updated =1 WHERE STATUS = 'OK';
update producttype set status='OK';
ALTER TABLE `producttype` CHANGE `status` `status` ENUM( 'OK' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'OK';

ALTER TABLE `product` ADD `is_updated` TINYINT(1) NULL;
ALTER TABLE `stock` ADD `is_updated` TINYINT(1) NULL;

