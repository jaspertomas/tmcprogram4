
CREATE TABLE IF NOT EXISTS `counter_receipt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `cheque_number` varchar(30) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `notes` tinytext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `cheque_number` (`cheque_number`),
  KEY `supplier_id` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `counter_receipt_detail`
--

CREATE TABLE IF NOT EXISTS `counter_receipt_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `counter_receipt_id` int(11) NOT NULL,
  `pono` varchar(10) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) NOT NULL,
  `invoice_number` varchar(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `notes` tinytext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vendor_id_2` (`vendor_id`,`invoice_number`),
  KEY `counter_receipt_id` (`counter_receipt_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `purchase_id` (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `counter_receipt`
--
ALTER TABLE `counter_receipt`
  ADD CONSTRAINT `counter_receipt_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`);

--
-- Constraints for table `counter_receipt_detail`
--
ALTER TABLE `counter_receipt_detail`
  ADD CONSTRAINT `counter_receipt_detail_ibfk_1` FOREIGN KEY (`counter_receipt_id`) REFERENCES `counter_receipt` (`id`),
  ADD CONSTRAINT `counter_receipt_detail_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `counter_receipt_detail_ibfk_3` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`id`);

ALTER TABLE `counter_receipt_detail` ADD `is_tally` TINYINT NOT NULL DEFAULT '0';
ALTER TABLE `counter_receipt` ADD `is_tally` TINYINT NOT NULL DEFAULT '0';
ALTER TABLE `purchase` ADD `counter_receipt_id` INT NULL ;
ALTER TABLE `purchase` ADD `counter_receipt_detail_id` INT NULL ;
ALTER TABLE `purchase` ADD INDEX ( `counter_receipt_id` ) ;
ALTER TABLE `purchase` ADD INDEX ( `counter_receipt_detail_id` ) ;
ALTER TABLE `purchase` ADD FOREIGN KEY ( `counter_receipt_id` ) REFERENCES `counter_receipt` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `purchase` ADD FOREIGN KEY ( `counter_receipt_detail_id` ) REFERENCES `counter_receipt_detail` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;
