INSERT INTO `sf_guard_permission` (`id` ,`name` ,`description` ,`created_at` ,`updated_at`)VALUES (NULL , 'banker', 'update cheque status', '', '');

ALTER TABLE `voucher` CHANGE `check_status` `check_status` ENUM( 'Pending', 'Released', 'Cleared', 'On Hold', 'Cancelled' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Pending';

ALTER TABLE `out_check` CHANGE `receive_date` `receive_date` DATE NULL;

ALTER TABLE `voucher` ADD `is_cancelled` TINYINT NULL;

ALTER TABLE `out_check` ADD `is_cancelled` TINYINT NULL;

ALTER TABLE `in_check` ADD `is_cancelled` TINYINT NULL;
