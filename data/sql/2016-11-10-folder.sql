ALTER TABLE `file` DROP `is_folder` ;

CREATE TABLE IF NOT EXISTS `folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `description` tinytext,
  `keywords` text,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `folder`
  ADD CONSTRAINT `folder_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `folder` (`id`);

INSERT INTO `folder` (`id`, `parent_id`, `title`, `description`, `keywords`) VALUES
(1, NULL, 'Root Folder', '', '');

