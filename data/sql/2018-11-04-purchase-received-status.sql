ALTER TABLE `purchase` ADD `received_status` ENUM( 'Not Received', 'Partially Received', 'Fully Received' ) NOT NULL DEFAULT 'Not Received';

Run Purchase Received Status Recalculation
http://localhost/tmcprogram4/web/frontend_dev.php/maintainance/calcPurchaseReceivedStatus
