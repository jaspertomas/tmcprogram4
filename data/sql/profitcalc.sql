ALTER TABLE `purchasedetail` ADD `remaining` DECIMAL(10,2) NULL DEFAULT '0';
ALTER TABLE `invoicedetail` ADD `remaining` DECIMAL(10,2) NULL DEFAULT '0';
ALTER TABLE `invoicedetail` ADD `is_profitcalculated` TINYINT(4) NULL DEFAULT '0';


CREATE TABLE IF NOT EXISTS `profitdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `invoicedetail_id` int(11) NOT NULL,
  `purchasedetail_id` int(11) NOT NULL,
  `invoicedate` date NOT NULL,
  `purchasedate` date NOT NULL,
  `buyprice` decimal(10,2) NOT NULL,
  `sellprice` decimal(10,2) NOT NULL,
  `profitperunit` decimal(10,2) NOT NULL,
  `profitrate` decimal(10,2) NOT NULL,
  `qty` decimal(10,2) NOT NULL,
  `profit` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `invoicedetail_id` (`invoicedetail_id`),
  KEY `purchasedetail_id` (`purchasedetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `profitdetail`
  ADD CONSTRAINT `profitdetail_ibfk_5` FOREIGN KEY (`purchasedetail_id`) REFERENCES `purchasedetail` (`id`),
  ADD CONSTRAINT `profitdetail_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `profitdetail_ibfk_2` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `profitdetail_ibfk_3` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`id`),
  ADD CONSTRAINT `profitdetail_ibfk_4` FOREIGN KEY (`invoicedetail_id`) REFERENCES `invoicedetail` (`id`);


update purchasedetail set remaining = qty;
update invoicedetail set remaining = qty;
delete from profitdetail;
