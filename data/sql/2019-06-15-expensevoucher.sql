ALTER TABLE `voucher` CHANGE `no` `no` VARCHAR( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;

ALTER TABLE `voucher` CHANGE `payee` `payee` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;

ALTER TABLE `voucher` ADD `biller_id` INT NULL ;
ALTER TABLE `voucher` ADD INDEX ( `biller_id` ) ;
ALTER TABLE `voucher` ADD FOREIGN KEY ( `biller_id` ) REFERENCES `biller` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

-- -----BILLER TYPE----------------
CREATE TABLE IF NOT EXISTS `biller_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `biller` CHANGE `biller_type` `biller_type_id` INT( 11 ) NOT NULL ;
ALTER TABLE `biller` ADD INDEX ( `biller_type_id` ) ;
ALTER TABLE `biller` ADD FOREIGN KEY ( `biller_type_id` ) REFERENCES `biller_type` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;


