--
--

ALTER TABLE `purchasedetail` ADD `qty_returned` INT NOT NULL default 0;
ALTER TABLE `invoicedetail` ADD `qty_returned` INT NOT NULL default 0;


CREATE TABLE IF NOT EXISTS `returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Invoice','Purchase') DEFAULT 'Invoice',
  `code` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `ref_class` enum('Invoice','Purchase') DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `client_class` enum('Customer','Vendor') DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `invno` varchar(20) DEFAULT NULL,
  `pono` varchar(20) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `status` enum('Pending','Complete','Cancelled') DEFAULT 'Complete',
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `warehouse_id_idx` (`warehouse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `returns_detail`
--

CREATE TABLE IF NOT EXISTS `returns_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_class` enum('Invoicedetail','Purchasedetail') DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `returns_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL DEFAULT '0',
  `total` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `returns_id` (`returns_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `returns_detail` ADD FOREIGN KEY ( `returns_id` ) REFERENCES `returns` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `returns_detail` ADD FOREIGN KEY ( `product_id` ) REFERENCES `product` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;


-- ------------------------------
@@ -311,7 +311,7 @@ CommissionPayment:
       fixed: false
       unsigned: false
       primary: false
-      notnull: true
+      notnull: false
       autoincrement: false
   relations:
     Employee:
@@ -629,6 +629,7 @@ CounterReceiptCheque:
       fixed: false
       unsigned: false
       primary: false
+      default: '0.00'
       notnull: true
       autoincrement: false
   relations:
@@ -1357,7 +1358,7 @@ Event:
       notnull: false
       autoincrement: false
     children_id:
-      type: string(10)
+      type: string(20)
       fixed: false
       unsigned: false
       primary: false
@@ -3822,6 +3823,197 @@ Quote:
       local: vendor_id
       foreign: id
       type: one

