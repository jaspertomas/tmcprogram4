ALTER TABLE `settings` ADD `description` VARCHAR( 255 ) NULL AFTER `name` ;
ALTER TABLE `settings` CHANGE `value` `value` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

-- here are product ids of tank products. profits of 12% and up will suffice for these products to be included in commission calculations
INSERT INTO `settings` (
`id` ,
`name` ,
`value`,
`description` 
)
VALUES (
NULL , 'commission_min_profit_product_type_ids', '94, 95, 98, 99, 100, 101, 103, 106, 107, 149, 150, 151, 152, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 172, 173, 174, 229, 230, 231, 232, 233, 234, 235, 238, 239, 240, 241, 242, 243, 245, 246, 247, 248, 249 ', ''
), (
NULL , 'commission_min_profit_rates', '12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12 ', ''
);
INSERT INTO `settings` (
`id` ,
`name` ,
`description` ,
`value`
)
VALUES (
NULL , 'commission_default_min_profit_rate', 'invoicedetail profit rates must be equal or above this to be included in commission calculations', '15'
);

