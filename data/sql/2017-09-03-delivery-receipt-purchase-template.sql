ALTER TABLE `quote` CHANGE `discrate` `discrate` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `quote` CHANGE `discamt` `discamt` DECIMAL(10) NULL;

INSERT INTO `purchase_template` (`id` ,`name`)VALUES (100 , 'Delivery Receipt');

INSERT INTO `settings` (`id`, `name`, `description`, `value`) VALUES (NULL, 'purchase_template_delivery_receipt_id', "id of purchase template for delivery receipts", '100');
INSERT INTO `settings` (`id` ,`name` ,`description` ,`value`) VALUES (NULL , 'latest_migration', "latest migration sql file run" , '2017-09-03-delivery-receipt-purchase-template.sql');



ALTER TABLE `stockentry` ADD `created_by_id` INT NULL ;
--ALTER TABLE `stockentry` CHANGE `created_by_id` `created_by_id` INT( 11 ) NULL ;
