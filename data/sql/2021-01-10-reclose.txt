ALTER TABLE `invoice` ADD `was_closed` TINYINT(1) NULL DEFAULT NULL;
ALTER TABLE `purchase` ADD `was_closed` TINYINT(1) NULL DEFAULT NULL;

update invoice set was_closed=1 where is_temporary=0;
update purchase set was_closed=1 where is_temporary=0;
