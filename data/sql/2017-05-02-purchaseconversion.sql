CREATE TABLE IF NOT EXISTS `purchaseconversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `conversion_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `conversion_id` (`conversion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `purchaseconversion`
--
ALTER TABLE `purchaseconversion`
  ADD CONSTRAINT `purchaseconversion_ibfk_2` FOREIGN KEY (`conversion_id`) REFERENCES `conversion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchaseconversion_ibfk_1` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`id`) ON UPDATE CASCADE;


