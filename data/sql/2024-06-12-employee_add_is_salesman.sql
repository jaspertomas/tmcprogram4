ALTER TABLE `employee` ADD `is_salesman` TINYINT NULL AFTER `is_technician`;
ALTER TABLE `employee` CHANGE `is_technician` `is_technician` TINYINT( 4 ) NULL;

update employee set is_technician=null where is_technician=0;
update employee set is_salesman=null where is_salesman=0;
