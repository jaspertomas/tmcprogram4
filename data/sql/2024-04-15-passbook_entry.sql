update in_check set passbook_entry_id = null;
update out_check set passbook_entry_id = null;

delete from passbook_entry;

ALTER TABLE `passbook`
  ADD `currentqty` decimal(10,2) NOT NULL DEFAULT '0.00',
  ADD `datetime` datetime NOT NULL,
  ADD `is_updated` tinyint(1) DEFAULT NULL;

ALTER TABLE `passbook_entry`
  ADD `datetime` datetime NOT NULL,
  ADD `qty` decimal(10,2) NOT NULL,
  ADD `qty_reported` float NOT NULL DEFAULT '0',
  ADD `qty_missing` float NOT NULL DEFAULT '0',
  ADD `ref_class` varchar(20) DEFAULT NULL,
  ADD `ref_id` int(11) DEFAULT NULL,
  ADD `type` enum('InCheck','OutCheck','Report') DEFAULT NULL,
  ADD `is_report` tinyint(1) NOT NULL DEFAULT '0',
  ADD `created_by_id` int(11) DEFAULT NULL,
  ADD `created_at` datetime NOT NULL;

-- ---------------------
ALTER TABLE `passbook_entry` CHANGE `datetime` `datetime` DATETIME NULL;
ALTER TABLE `passbook_entry` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `passbook_entry` CHANGE `trans_type` `trans_type` ENUM( 'CASH/CHECK DEPOSIT', 'CHECK ENCASHMENT', 'CREDIT MEMO', 'DEBIT MEMO', 'IMIN-CLRNG-BAT', 'IN-HOUSE CHECK DEPOSIT', 'INWARD CHECK', 'LOAN PAYMENT', 'LOCAL CHECK DEPOSIT', 'REPORT' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `passbook_entry` CHANGE `client_id` `client_name` VARCHAR( 128 ) NULL DEFAULT NULL;
ALTER TABLE `passbook_entry` DROP `date`;