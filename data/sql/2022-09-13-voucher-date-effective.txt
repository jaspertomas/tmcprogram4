ALTER TABLE `voucher` ADD `date_effective` DATE NULL AFTER `time`;
update voucher set date_effective=date;
-- vouchers with checks - date date_effective is check date
UPDATE voucher INNER JOIN out_check ON voucher.out_check_id = out_check.id SET voucher.date_effective = out_check.check_date;

