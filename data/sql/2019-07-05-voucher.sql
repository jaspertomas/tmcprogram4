ALTER TABLE `voucher` CHANGE `time` `time` VARCHAR( 8 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;


ALTER TABLE `out_check` DROP FOREIGN KEY `out_check_ibfk_2` ;
UPDATE out_check SET biller_id = NULL;
ALTER TABLE `out_check` CHANGE `biller_id` `voucher_account_id` INT( 11 ) NULL DEFAULT NULL ;
ALTER TABLE `out_check` ADD FOREIGN KEY ( `voucher_account_id` ) REFERENCES `voucher_account` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

