ALTER TABLE `stockentry` ADD `qty_reported` FLOAT NOT NULL DEFAULT '0' AFTER `qty` ;
ALTER TABLE `stockentry` ADD `qty_missing` FLOAT NOT NULL DEFAULT '0' AFTER `qty_reported` ;
ALTER TABLE `stockentry` CHANGE `type` `type` ENUM( 'Adjustment', 'Incoming DR', 'Outgoing DR', 'Conversion DR', 'Invoice', 'InvoiceReturn', 'InvoiceReplacement', 'InvoiceConversion', 'Purchase', 'PurchaseReturn', 'PurchaseReplacement', 'PurchaseConversion', 'Report' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;

