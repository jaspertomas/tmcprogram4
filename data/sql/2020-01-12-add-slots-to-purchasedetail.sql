ALTER TABLE `purchasedetail` ADD `slot` INT( 4 ) NOT NULL DEFAULT '0';
ALTER TABLE `profitdetail` ADD `slot` INT( 4 ) NOT NULL DEFAULT '0';

ALTER TABLE `purchase` CHANGE `invno` `sold_to` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;

ALTER TABLE `purchase` 
ADD `invnos` VARCHAR( 70 ) NULL AFTER `sold_to` ,
ADD `invoice_ids` VARCHAR( 30 ) NULL AFTER `invnos` ;

--UNDO---
ALTER TABLE `purchase` CHANGE `sold_to` `invno` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;

