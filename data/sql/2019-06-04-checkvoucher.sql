-- phpMyAdmin SQL Dump
-- version 3.4.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2019 at 08:53 AM
-- Server version: 5.5.27
-- PHP Version: 7.1.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tmcprogram4`
--

-- --------------------------------------------------------

--
-- Table structure for table `billee`
--

CREATE TABLE IF NOT EXISTS `billee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `billed_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `biller`
--

CREATE TABLE IF NOT EXISTS `biller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `biller_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `in_check`
--

CREATE TABLE IF NOT EXISTS `in_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `check_no` varchar(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `receive_date` date NOT NULL,
  `check_date` date NOT NULL,
  `cleared_date` date DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `billee_id` int(11) DEFAULT NULL,
  `notes` text,
  `passbook_id` int(11) NOT NULL,
  `passbook_entry_id` int(11) DEFAULT NULL,
  `remaining` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `billee_id` (`billee_id`),
  KEY `passbook_id` (`passbook_id`),
  KEY `passbook_entry_id` (`passbook_entry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

CREATE TABLE IF NOT EXISTS `issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `name` tinytext NOT NULL,
  `problem` text,
  `solution` text,
  `status` enum('open','solved','cancelled') NOT NULL DEFAULT 'open',
  `created_by_id` int(11) DEFAULT NULL,
  `solved_by_id` int(11) DEFAULT NULL,
  `client_type` varchar(20) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `out_check`
--

CREATE TABLE IF NOT EXISTS `out_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `check_no` varchar(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `receive_date` int(11) NOT NULL,
  `check_date` date NOT NULL,
  `clear_date` date NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `notes` text NOT NULL,
  `passbook_id` int(11) NOT NULL,
  `passbook_entry_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `passbook_entry_id` (`passbook_entry_id`),
  KEY `passbook_id` (`passbook_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `biller_id` (`biller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `passbook`
--

CREATE TABLE IF NOT EXISTS `passbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `account_no` varchar(20) NOT NULL,
  `location` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `passbook_entry`
--

CREATE TABLE IF NOT EXISTS `passbook_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `deposit` decimal(10,2) NOT NULL DEFAULT '0.00',
  `withdrawal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `transaction_code` varchar(2) NOT NULL,
  `passbook_id` int(11) NOT NULL,
  `client_type` varchar(20) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `passbook_id` (`passbook_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `in_check`
--
ALTER TABLE `in_check`
  ADD CONSTRAINT `in_check_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `in_check_ibfk_2` FOREIGN KEY (`billee_id`) REFERENCES `billee` (`id`),
  ADD CONSTRAINT `in_check_ibfk_3` FOREIGN KEY (`passbook_id`) REFERENCES `passbook` (`id`),
  ADD CONSTRAINT `in_check_ibfk_4` FOREIGN KEY (`passbook_entry_id`) REFERENCES `passbook_entry` (`id`);

--
-- Constraints for table `out_check`
--
ALTER TABLE `out_check`
  ADD CONSTRAINT `out_check_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `out_check_ibfk_2` FOREIGN KEY (`biller_id`) REFERENCES `biller` (`id`),
  ADD CONSTRAINT `out_check_ibfk_3` FOREIGN KEY (`passbook_id`) REFERENCES `passbook` (`id`),
  ADD CONSTRAINT `out_check_ibfk_4` FOREIGN KEY (`passbook_entry_id`) REFERENCES `passbook_entry` (`id`);

--
-- Constraints for table `passbook_entry`
--
ALTER TABLE `passbook_entry`
  ADD CONSTRAINT `passbook_entry_ibfk_1` FOREIGN KEY (`passbook_id`) REFERENCES `passbook` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `event` ADD `in_check_id` INT NULL ;
ALTER TABLE `event` ADD INDEX ( `in_check_id` ) ;
ALTER TABLE `event` ADD FOREIGN KEY ( `in_check_id` ) REFERENCES `in_check` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `invoice` ADD `in_check_id` INT NULL ;
ALTER TABLE `invoice` ADD INDEX ( `in_check_id` ) ;
ALTER TABLE `invoice` ADD FOREIGN KEY ( `in_check_id` ) REFERENCES `in_check` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE `in_check` ADD `is_bank_transfer` TINYINT( 1 ) NOT NULL DEFAULT '0';

