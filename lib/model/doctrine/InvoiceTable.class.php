<?php


class InvoiceTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Invoice');
    }
    public static function fetchOneByInvno($invno)
    {
      return Doctrine_Query::create()
        ->from('Invoice i')
        ->where('invno="'.$invno.'"')
        ->fetchOne();
    }
    public static function fetchByDate($date)
    {
      return Doctrine_Query::create()
        ->from('Invoice i')
        ->where('date="'.$date.'"')
        ->orderBy('i.invno')
        ->execute();
    }
    public static function fetchByDateRange($fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->orderBy('i.date,i.invno')
        ->execute();
    }
  function genInterofficeInvno()
  {
    $prefix="";
    $year=date("y");
    $defaultinvno=$prefix.$year."-0001";
    //find previous invoice with invno format "DR17-0000"
    $previousdr= Doctrine_Query::create()
      ->from('Invoice i')
      ->where('i.invno like "'.$prefix.$year.'-%"')
      ->orderBy('invno desc')
      ->fetchOne();
    //if previous invoice found
    if($previousdr!=null)
    {
      //take previous invoice
      //convert invno to integer and add 1
      //then pad with zeros and add prefix"
      $defaultinvno=preg_replace('/[A-Za-z]+/', '', $previousdr->getInvno());
      $segments=explode("-",$defaultinvno);
      $defaultinvno=$segments[count($segments)-1];
      $defaultinvno=intval($defaultinvno);
      $defaultinvno++;
      $defaultinvno=$prefix.$year."-".str_pad($defaultinvno,4,0,STR_PAD_LEFT);
    }
    return $defaultinvno;
  }
    public static function genTransactionCode()
    {
      $year=date("y");
    
      $last=Doctrine_Query::create()
        ->from('Invoice i')
        ->where('transaction_code like "'.$year.'%"')
        ->orderBy(' transaction_code desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getTransactionCode();
        $code=substr($code,2);
        $code+=1;
      }
      return $year."".str_pad($code, 4, "0", STR_PAD_LEFT);
    }
    //------------
    public static function fetchUnclosedByDateRange($fromdate,$todate,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('Invoice i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('is_temporary!=0')
        ->andWhere('status!="Cancelled"')
        ->orderBy('i.date desc');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnclosedByDate($date)
    {
      return self::fetchUnclosedByDateRange($date,$date);
    }
    public static function countUnclosedByDateRange($fromdate,$todate)
    {
      return self::fetchUnclosedByDateRange($fromdate,$todate,true);
    }
    public static function countUnclosedByDate($date)
    {
      return self::fetchUnclosedByDateRange($date,$date,true);
    }
    //------------
    public static function fetchUnpaidByDateRange($fromdate,$todate,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('Invoice i, i.Customer c')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('i.is_temporary=0')
        ->andWhere('i.status!="Paid"')
        ->andWhere('i.status!="Cancelled"')
        // ->andWhere('c.is_owned!=1')
        ->orderBy('i.duedate desc');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnpaidByDate($date)
    {
      return self::fetchUnpaidByDateRange($date,$date);
    }
    public static function countUnpaidByDateRange($fromdate,$todate)
    {
      return self::fetchUnpaidByDateRange($fromdate,$todate,true);
    }
    public static function countUnpaidByDate($date)
    {
      return self::fetchUnpaidByDateRange($date,$date,true);
    }
    public static function totalUnpaidByDateRange($fromdate,$todate)
    {
      $query = Doctrine_Query::create()
        ->from('Invoice i, i.Customer c')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('i.is_temporary=0')
        ->andWhere('i.status!="Paid"')
        ->andWhere('i.status!="Cancelled"')
        // ->andWhere('c.is_owned!=1')
        ->orderBy('i.date desc');
      $invoices=$query->execute();
      $total=0;
      foreach($invoices as $invoice)
      {
        $total+=$invoice->getTotal();
      }
      return $total;
    }
  }
