<?php


class JobOrderDrTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('JobOrderDr');
    }
    public static function genDrForJobOrderId($job_order_id,$user)
    {

        //if an existing unproduced dr exists, update it and use that
        $existingDr=Fetcher::fetchOne("JobOrderDr",array("job_order_id"=>$job_order_id, "is_produced"=>"0"));
        if($existingDr!=null)
        {
            $existingDr->updateToJobOrder();
            return $existingDr;
        }

        $dr = new JobOrderDr();
        $dr->setJobOrderId($job_order_id);
        $job_order=$dr->getJobOrder();

        $dr->setDatetime(MyDateTime::now()->todatetime());
        $dr->genCode();
        $dr->setWarehouseId(sfConfig::get('custom_default_production_warehouse_id'));

        $dr->setCreatedById($user->getId());
        $dr->setUpdatedById($user->getId());
        $dr->setCreatedAt(MyDateTime::now()->todatetime());
        $dr->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr->save();

        foreach($job_order->getJobOrderDetail() as $id)
        {
            //create new detail
            //set job_order data
            $dr_detail=new JobOrderDrDetail();
            $dr_detail->setJobOrderDrId($dr->getId());
            $dr_detail->setJobOrderId($dr->getJobOrderId());
            $dr_detail->setWarehouseId($dr->getWarehouseId());

            //set job_orderdetail data
            $dr_detail->setJobOrderDetailId($id->getId());
            $dr_detail->setDatetime($dr->getDatetime());
            $dr_detail->setProductId($id->getProductId());

            //set qty data
            $qty_remaining=$id->getQty()-$id->getQtyProduced();

            // //if product is service, ignore
            // if($id->getProduct()->getIsService()==1)
            //   $dr_detail->setQty(0);
            // //else do normally
            // else
              $dr_detail->setQty($qty_remaining);
            
            $dr_detail->setCreatedById($user->getId());
            $dr_detail->setUpdatedById($user->getId());
            $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
            $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
            $dr_detail->save();

            $dr_detail->updateStockEntry(0);
        }

        $dr->calc();

        return $dr;
    }
    public static function fetchByDateRange($fromdatetime,$todatetime)
    {
      return Doctrine_Query::create()
        ->from('JobOrderDr i')
        ->where('i.datetime >= "'.$fromdatetime.'"')
        ->andWhere('i.datetime <= "'.$todatetime.'"')
        ->orderBy('i.code')
        ->execute();
    }
    public static function fetchByDate($datetime)
    {
      return self::fetchByDateRange($datetime,$datetime);
    }
    public static function fetchUnproducedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('JobOrderDr i')
        ->where('i.datetime >= "'.$fromdatetime.' 00:00:00"')
        ->andWhere('i.datetime <= "'.$todatetime.' 23:59:59"')
        ->andWhere('(i.is_produced = 0)')
        ->andWhere('(i.is_cancelled = 0)')
        ->orderBy('i.code');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnproducedByDate($datetime)
    {
      return self::fetchUnproducedByDateRange($datetime,$datetime);
    }
    public static function countUnproducedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      return self::fetchUnproducedByDateRange($fromdatetime,$todatetime,true);
    }
    public static function countUnproducedByDate($datetime)
    {
      return self::fetchUnproducedByDateRange($datetime,$datetime,true);
    }
    public static function fetchUnreturnedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('JobOrderDr i')
        ->where('i.datetime >= "'.$fromdatetime.' 00:00:00"')
        ->andWhere('i.datetime <= "'.$todatetime.' 23:59:59"')
        ->andWhere('(i.is_produced = 0)')
        ->andWhere('(i.is_cancelled = 0)')
        ->orderBy('i.code');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnreturnedByDate($datetime)
    {
      return self::fetchUnreturnedByDateRange($datetime,$datetime);
    }
    public static function countUnreturnedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      return self::fetchUnreturnedByDateRange($fromdatetime,$todatetime,true);
    }
    public static function countUnreturnedByDate($datetime)
    {
      return self::fetchUnreturnedByDateRange($datetime,$datetime,true);
    }
}