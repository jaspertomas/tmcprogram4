<?php


class PurchaseDrTable extends Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PurchaseDr');
    }
    public static function genDrForPurchaseId($purchase_id,$user)
    {

        //if an existing unreceived dr exists, update it and use that
        $existingDr=Fetcher::fetchOne("PurchaseDr",array("purchase_id"=>$purchase_id, "is_received"=>null));
        if($existingDr!=null)
        {
            $existingDr->updateToPurchase();
            return $existingDr;
        }

        $dr = new PurchaseDr();
        $dr->setPurchaseId($purchase_id);
        $purchase=$dr->getPurchase();

        //scan invoicedetails, see if there are any returns
        //if yes, this dr is a return
        $is_return=false;
        foreach($purchase->getPurchasedetail() as $id)
        {
          $qty_remaining=$id->getQty()-$id->getQtyReceived();
          //disable: ignore prodict if is_service
          // if(!$id->getProduct()->isService() and $qty_remaining<0)
          if($qty_remaining<0)
          {
            $is_return=true;
            break;
          }
        }

        $dr->setVendorId($purchase->getVendorId());
        $dr->setDatetime(MyDateTime::now()->todatetime());
        $dr->genCode();
        $dr->setIsReturn($is_return?1:null);
        $setting=Fetcher::fetchOne("Settings",array("name"=>"'default_warehouse_id'"));
        $dr->setWarehouseId($setting->getValue());

        $dr->setCreatedById($user->getId());
        $dr->setUpdatedById($user->getId());
        $dr->setCreatedAt(MyDateTime::now()->todatetime());
        $dr->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr->save();

        foreach($purchase->getPurchasedetail() as $id)
        {
            //create new detail
            //set purchase data
            $dr_detail=new PurchaseDrDetail();
            $dr_detail->setPurchaseDrId($dr->getId());
            $dr_detail->setPurchaseId($dr->getPurchaseId());
            $dr_detail->setVendorId($dr->getVendorId());
            $dr_detail->setWarehouseId($dr->getWarehouseId());

            //set purchasedetail data
            $dr_detail->setPurchasedetailId($id->getId());
            $dr_detail->setDatetime($dr->getDatetime());
            $dr_detail->setProductId($id->getProductId());

            //set qty data
            $qty_remaining=$id->getQty()-$id->getQtyReceived();

            //if product is service, ignore
            if($id->getProduct()->getIsService()==1)
              $dr_detail->setQty(0);
            //if dr is return, set non return dr details to 0
            else if($is_return and $qty_remaining>=0)
              $dr_detail->setQty(0);
            //else do normally
            else
              $dr_detail->setQty($qty_remaining);
            
            //set as return if qty is negative
            $dr_detail->setIsReturn(($qty_remaining<0)?1:0);
    
            $dr_detail->setCreatedById($user->getId());
            $dr_detail->setUpdatedById($user->getId());
            $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
            $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
            $dr_detail->save();

            $dr_detail->updateStockEntry(0);
        }

        $dr->calc();

        return $dr;
    }
    public static function fetchByDateRange($fromdatetime,$todatetime)
    {
      return Doctrine_Query::create()
        ->from('PurchaseDr i')
        ->where('i.datetime >= "'.$fromdatetime.'"')
        ->andWhere('i.datetime <= "'.$todatetime.'"')
        ->orderBy('i.code')
        ->execute();
    }
    public static function fetchByDate($datetime)
    {
      return self::fetchByDateRange($datetime,$datetime);
    }
    public static function fetchUnreceivedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('PurchaseDr i')
        ->where('i.datetime >= "'.$fromdatetime.' 00:00:00"')
        ->andWhere('i.datetime <= "'.$todatetime.' 23:59:59"')
        ->andWhere('(i.is_received is null or i.is_received = 0)')
        ->andWhere('(i.is_cancelled is null or i.is_cancelled = 0)')
        ->andWhere('(i.is_return is  null or i.is_return = 0)')
        ->orderBy('i.datetime desc');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnreceivedByDate($datetime)
    {
      return self::fetchUnreceivedByDateRange($datetime,$datetime);
    }
    public static function countUnreceivedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      return self::fetchUnreceivedByDateRange($fromdatetime,$todatetime,true);
    }
    public static function countUnreceivedByDate($datetime)
    {
      return self::fetchUnreceivedByDateRange($datetime,$datetime,true);
    }
    public static function fetchUnreturnedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('PurchaseDr i')
        ->where('i.datetime >= "'.$fromdatetime.' 00:00:00"')
        ->andWhere('i.datetime <= "'.$todatetime.' 23:59:59"')
        ->andWhere('(i.is_received is null or i.is_received = 0)')
        ->andWhere('(i.is_cancelled is null or i.is_cancelled = 0)')
        ->andWhere('(i.is_return = 1)')
        ->orderBy('i.datetime desc');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnreturnedByDate($datetime)
    {
      return self::fetchUnreturnedByDateRange($datetime,$datetime);
    }
    public static function countUnreturnedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      return self::fetchUnreturnedByDateRange($fromdatetime,$todatetime,true);
    }
    public static function countUnreturnedByDate($datetime)
    {
      return self::fetchUnreturnedByDateRange($datetime,$datetime,true);
    }
}
