<?php

/**
 * PurchaseDr
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class PurchaseDr extends BasePurchaseDr
{
  function __toString(){return "Purchase Dr ".$this->getCode();}
  function getName(){return $this->getCode();}
  function cascadeDelete()
  {
    //unreceive first
    if($this->isReceived())
      $this->undoReceive();

    foreach($this->getPurchaseDrDetail() as $detail)
    {
        $detail->deleteStockEntry();
        $detail->delete();
    }

    /*        
    if($this->getRefClass()=="purchase")
    {
      $purchase=$this->getRef();
      $purchase->setDatereceived(null);
      $purchase->save();
    }
    */
        
    return $this->delete();
  }  
  function cascadeCancel()
  {
    //unreceive first
    if($this->isReceived())
      $this->undoReceive();

    foreach($this->getPurchaseDrDetail() as $detail)
    {
      $detail->setIsCancelled(1);
      $detail->save();
    }

    /*        
    if($this->getRefClass()=="purchase")
    {
      $purchase=$this->getRef();
      $purchase->setDatereceived(null);
      $purchase->save();
    }
    */
        
    $this->setIsCancelled(1);
    $this->save();
    return true;
  }
  function cascadeUndoCancel()
  {
    foreach($this->getPurchaseDrDetail() as $detail)
    {
      $detail->setIsCancelled(null);
      $detail->save();
    }

    $this->setIsCancelled(null);
    $this->save();
    return true;
  }
  function getRef()
  {
    return $this->getPurchase();
  }
  function getRefNo($ref)
  {
    if($ref==null)$ref=$this->getPurchase();
    return $ref->getInvno();
  }
  function getClient()
  {
    return $this->getPurchase()->getVendor();
  }
  function getStatusColor()
  {
    switch($this->getStatus())
    {
      case "Pending": return "red";
      case "Complete": return "green";
      case "Cancelled": return "black";
    }
  }
  function getPaymentStatusColor()
  {
    switch($this->getPaymentStatus())
    {
      case "Pending": return "red";
      case "Paid": return "green";
      case "Cancelled": return "black";
    }
  }
  //calculate qty_for_return and qty_for_receive
  function calc()
  {
    $qty_for_receive=0;
    $qty_for_return=0;
    foreach($this->getPurchaseDrDetail() as $detail)
    {
      $qty=$detail->getQty();
      if($qty<0)
          $qty_for_return-=$qty;
      else
          $qty_for_receive+=$qty;
    }
    $this->setQtyForReceive($qty_for_receive);
    $this->setQtyForReturn($qty_for_return);
    $this->save();
  }
  function regenMissingDetails($user)
  {
    $purchase=$this->getPurchase();
    $purchasedetails=$purchase->getPurchasedetails();

    $dr_details=$this->getPurchaseDrDetailsIndexedByPurchasedetailId();

    // var_dump($request->getParameter('qtys'));die();
    foreach($purchasedetails as $purchasedetail)
    {
      //if deliverydetail does not exist, create it

      if($dr_details[$purchasedetail->getId()]==null)
      {
        $dr_detail=new PurchaseDrDetail();
        $dr_detail->setPurchaseDrId($this->getId());
        $dr_detail->setPurchaseId($this->getPurchaseId());
        $dr_detail->setVendorId($this->getVendorId());
        $dr_detail->setWarehouseId($this->getWarehouseId());

        $dr_detail->setPurchasedetailId($purchasedetail->getId());
        $dr_detail->setDatetime($this->getDatetime());
        $dr_detail->setProductId($purchasedetail->getProductId());
        // $dr_detail->setQty($purchasedetail->getQty());
        $dr_detail->setQty(0);
        //set as return if qty is negative
        $dr_detail->setIsReturn(($qty<0)?1:0);

        $dr_detail->setCreatedById($user->getId());
        $dr_detail->setUpdatedById($user->getId());
        $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
        $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr_detail->save();
      }
    }
  }
  function getParticularsString()
  {
    $array=array();
  
    foreach($this->getPurchaseDrDetail() as $detail)
    {
      $string=intval($detail->getQty())." x ".$detail->getProduct()->getName()." (".$detail->getProduct()->getProductcategory().")";
      if($detail->getQty()<0)$string.=" (return)";

      $array[]=$string;
    }
    
    return implode("; ",$array);
  }
  function getIsSales()
  {
    return $this->getClientClass()=="Customer";
  }
  //this purchaseDr true if the return was created 
  //not from an purchase or purchase
  //but directly from a customer or supplier
  function isClientBased()
  {
    return $this->getType()=="Customer" or $this->getType()=="Vendor";
  }
  //this purchaseDr true if the return was created
  //from an purchase or purchase
  function isTransactionBased()
  {
    return $this->getType()=="Purchase" or $this->getType()=="Purchase";
  }
  function getPrettyType()
  {
    return $this->getType()=="Vendor"?"Supplier":$this->getType();
  }
/*
  function setDateAndUpdateStockEntry($datetime)
  {
    $olddate=$this->getDatetime();
    $this->setDatetime($datetime);
    $newdate=$this->getDatetime();
    
    //if date changed, update dates of all stock entries, and recalc stock entries
    if($olddate!=$newdate)
    {
      //determine which is earlier - newdate or olddate
      //the earlier date will determine when to start recalculating
      $olddatetime=MyDateTime::frommysql($olddate);
      $newdatetime=MyDateTime::frommysql($newdate);
      if($olddatetime->isearlierthan($newdatetime))
        $earlierdate=$olddate;
      else
        $earlierdate=$newdate;
        
      //for each stock entry, recalculate from before earlier date
      //find entry before earlier date
      //and start recalculating from that stock entry
      foreach($this->getPurchaseDrDetail() as $detail)
      {
        $entry=$detail->getStockEntry();
        $entry->setDate($newdate);
        $entry->save();
        $lastentry=$entry->getLastBeforeDate($earlierdate);
        $entry->getStock()->calcFromStockEntry($lastentry);
      }
    }
  }
  */
  public function genCode()
  {
    $prefix="PDR";
    $invno=$this->getPurchase()->getPono();   
    $last=Doctrine_Query::create()
      ->from('PurchaseDr i')
      ->where('purchase_id = '.$this->getPurchaseId())
      ->orderBy(' code desc')
      ->fetchOne();
    if($last==null)
    {
      $code="a";
    }
    else
    {
      $code=$last->getCode();
      $code=explode($invno,$code);
      $code=$code[1];
      $code++;
    }
    $code=$prefix."".$invno."".$code;
    $this->setCode($code); 
    
  }
  public function isCancelled()
  {
    return $this->getIsCancelled()==1;
  }
  public function isReceived()
  {
    return $this->getIsReceived()==1;
  }
  public function isPending()
  {
    return !$this->isReceived() and !$this->isCancelled();
  }
  public function isReturn()
  {
    return $this->getIsReturn()==1;
  }
  public function receiveStatusString()
  {
    if($this->isReturn())
      return $this->getIsReceived()==1?"Returned":"Not Returned";
    else
      return $this->getIsReceived()==1?"Received":"Not Received";
  }
  public function statusString()
  {
    if($this->isCancelled())return "Cancelled";
    else return $this->getIsReceived()==1?"Received":"Not Received";
  }
  //arrange dr_details into an array with purchasedetail as key
  //Usage:
  //$dr_details = $dr->getPurchaseDrDetailsIndexedByPurchasedetailId();
  //$dr_detail = $dr_details[$purchasedetail_id];
  public function getPurchaseDrDetailsIndexedByPurchasedetailId()
  {
    $dr_details=array();
    foreach($this->getPurchaseDrDetail() as $dr_detail)
      $dr_details[$dr_detail->getPurchasedetailId()]=$dr_detail;
    return $dr_details;
  }

  public function receive()
  {
    //load purchase from database
    $purchase=$this->getPurchase();
    $purchasedetails=$purchase->getPurchasedetails();
    //dr_details array with purchasedetail as key
    //Usage: $dr_detail = $dr_details[$purchasedetail_id];
    $dr_details=$this->getPurchaseDrDetailsIndexedByPurchasedetailId();

    //validation
    //for each purchase detail
    $total_receive_qty=0;
    $total_return_qty=0;
    foreach($purchasedetails as $purchasedetail)
    {
      $dr_detail = $dr_details[$purchasedetail->getId()];
      $qty=$dr_detail->getQty();
      if($qty==0)continue;

      if($qty>0)$total_receive_qty+=$qty;
      else if($qty<0)$total_return_qty-=$qty;

      //validate: qty must not be greater than remaining
      $qty_remaining=$purchasedetail->getQty()-$purchasedetail->getQtyReceived();
      if($qty>$qty_remaining)
      {
        $error="Cannot receive ".$qty." units of ".$purchasedetail->getProduct().", only ".$qty_remaining." units remaining";
        return $error;
      }
    }
    //prevent receive if dr is empty
    if($total_receive_qty==0 and $total_return_qty==0)
    {
      $error="Cannot receive empty DR";
      return $error;
    }

    //receive
    foreach($purchasedetails as $purchasedetail)
    {
      $dr_detail = $dr_details[$purchasedetail->getId()];
      $dr_detail->receive($purchasedetail);
    }

    $this->setIsReceived(1);
    $this->save();

    return false;
  }

  public function undoReceive()
  {
    //load purchase from database
    $purchase=$this->getPurchase();
    $purchasedetails=$purchase->getPurchasedetails();
    //dr_details array with purchasedetail as key
    //Usage: $dr_detail = $dr_details[$purchasedetail_id];
    $dr_details=$this->getPurchaseDrDetailsIndexedByPurchasedetailId();

    //undo receive
    foreach($purchasedetails as $purchasedetail)
    {
      $dr_detail = $dr_details[$purchasedetail->getId()];
      if($dr_detail!=null)
        $dr_detail->undoReceive($purchasedetail);
    }

    $this->setIsReceived(0);
    $this->save();

  }
  //make it so that it looks like a freshly generated dr
  //do this on an unreceived dr when Generate DR is clicked
  function updateToPurchase()
  {
    $this->setDatetime(MyDateTime::now()->todatetime());
    $this->save();
    $dr_details = $this->getPurchaseDrDetailsIndexedByPurchasedetailId();
    foreach($this->getPurchase()->getPurchasedetail() as $purchasedetail)
    {
      $dr_detail = $dr_details[$purchasedetail->getId()];
      if($dr_detail!=null)
      {
        $dr_detail->setQty($purchasedetail->getQtyUnreceived());
        $dr_detail->save();
      }
    }

  }
  //in improvised sold_to form,
  //first dr is the only one that will appear
  //standing in for the purchase
  function isFirstDr()
  {
    return substr($this->getCode(),-1)=='a';
  }
  function getTypeString()
  {
    if($this->isReturn())return "Purchase Delivery Return";
    else return "Purchase Delivery Receipt";
  }
  function getTypeActionString()
  {
    if($this->isReturn())return "Return";
    else return "Receive";
  }
}
