<?php


class FileTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('File');
    }
    public static function genUploadLocation()
    {
        return date('Ym', time());//yyyymm of upload date
    }
}
