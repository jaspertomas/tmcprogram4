<?php


class DeliveryTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Delivery');
    }
  public static function selectByRef($ref_class,$ref_id)
  {
        return Doctrine_Query::create()
          ->from('Delivery d')
          ->where('d.ref_class = "'.$ref_class.'"')
          ->andWhere('d.ref_id = '.$ref_id)
        	->execute()
          ;
  }
  //this creates a delivery with default code and date for the given type
  //it does not save the created delivery
  public static function createDefault($type)
  {
    //generate delivery
    $delivery=new Delivery();
    $delivery->setWarehouseId(SettingsTable::fetch("default_warehouse_id"));
    $delivery->setDate(MyDate::today());
    $delivery->setType($type);
    
    //generate serial code
    $prefix="";
    switch($type)
    {
      case "Incoming":$prefix="iDR";break;
      case "Outgoing":$prefix="oDR";break;
      case "Conversion":$prefix="Conv-";break;
    }
    $year=date("y");
    $defaultcode=$prefix.$year."-0001";
    //find previous delivery with code format "DR17-0000"
    $previousdr= Doctrine_Query::create()
      ->from('Delivery d')
      ->where('d.code like "'.$prefix.$year.'-%"')
      ->orderBy('code desc')
      ->fetchOne();
    //if previous delivery found
    if($previousdr!=null)
    {
      //take previous delivery
      //convert code to integer and add 1
      //then pad with zeros and add prefix"
      $defaultcode=preg_replace('/[A-Za-z]+/', '', $previousdr->getCode());
      $segments=explode("-",$defaultcode);
      $defaultcode=$segments[count($segments)-1];
      $defaultcode=intval($defaultcode);
      $defaultcode++;
      $defaultcode=$prefix.$year."-".str_pad($defaultcode,4,0,STR_PAD_LEFT);
    }
    $delivery->setCode($defaultcode);
    
    /*
    $delivery->setRefClass($ref_class);
    $delivery->setRefId($ref_id);
    $delivery->setClientClass($client_class);
    $delivery->setClientId($client_id);
    $delivery->setPono($pono);
    $delivery->setInvno($invno);
    $delivery->setNotes($notes);
    $delivery->save();
    */
    
    return $delivery;
  }
}
