<?php


class StockentryTable extends Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Stockentry');
    }
    public static function fetchByDate($date,$warehouse_id)
    {
      return Doctrine_Query::create()
        ->from('Stockentry i, i.Stock s, s.Product p')
        ->where('datetime>="'.$date.' 00:00:00"')
        ->andWhere('datetime<="'.$date.' 23:59:59"')
        ->andWhere('warehouse_id='.$warehouse_id.'')
        ->orderBy('datetime desc, id desc')
        ->execute();
    }
    public static function fetchByDateRange($fromdatetime,$todatetime,$warehouse_id)
    {
      return Doctrine_Query::create()
        ->from('Stockentry i, i.Stock s, s.Product p')
        ->where('i.datetime >= "'.$fromdatetime.' 00:00:00"')
        ->andWhere('i.datetime <= "'.$todatetime.' 23:59:59"')
        ->andWhere('warehouse_id='.$warehouse_id.'')
        ->orderBy('datetime desc, id desc')
        ->execute();
    }
    public static function fetchByProductTypeIdAndWarehouseIdGroupByLatestAsOfDate($producttype_id,$warehouse_id,$date)
    {
      $pdo = Doctrine_Manager::getInstance()->getCurrentConnection()->getDbh();

      //put stock_ids in an array for later use
      $query = 'select stock.id from stock inner join product on stock.product_id=product.id where warehouse_id = '.$warehouse_id.' AND producttype_id = '.$producttype_id.'';
      $stmt = $pdo->prepare($query);
      $stmt->execute();
      $results = $stmt->fetchAll();  
      $stock_ids=array();
      foreach($results as $result)
        $stock_ids[]=$result[0];

      //if no stock ids, return empty array
      if(count($stock_ids)==0)return array();
    
      /*
      //this doesn't always give the desired result
      //take stock entry ids and put them in an array
      $query = 'select s1.id from stockentry s1 inner join (select * from stockentry  WHERE stock_id IN ('.implode(",",$stock_ids).') and datetime <= "'.$date.'"  order by stockentry.datetime desc, stockentry.id desc ) s2 on s1.id = s2.id group by s1.stock_id ';
      $stmt = $pdo->prepare($query);
      $stmt->execute();
      $results = $stmt->fetchAll();  
      $stockentry_ids=array();
      foreach($results as $result)
        $stockentry_ids[]=$result[0];

      //load stock entries of the stock entry ids loaded above
      return Doctrine_Query::create()
        ->from('Stockentry s')
        ->whereIn('id',$stockentry_ids)
        ->execute();
      */

      $output=array();
      //get last stock entry for each stock
      foreach($stock_ids as $stock_id)
      {
        //this query is copied from stock->getLastForDate()
        $output[]=Doctrine_Query::create()
        ->from('Stockentry se')
        ->where('se.stock_id = '.$stock_id)
        ->andWhere('se.datetime <= "'.$date.' 23:59:59"')
        //ignore stockentries for invoice/purchase drs with qty=0
        //this causes an error
        // ->andWhere('se.qty != 0')
        ->orderBy('datetime desc, id desc')
        ->fetchOne();
      }
      return $output;
    }
}
