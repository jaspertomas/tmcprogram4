<?php


class TransferTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Transfer');
    }
    public static function genCode()
    {
      $last=Doctrine_Query::create()
        ->from('Transfer i')
        // ->where('code regexp "^[0-9]*$"')
        ->orderBy(' LPAD(code,6,0) desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getCode();
        $code+=1;
      }
      return str_pad($code, 6, "0", STR_PAD_LEFT);
      
    }
    public static function fetchOneByCode($code)
    {
      return Doctrine_Query::create()
        ->from('Transfer i')
        ->where('code="'.$code.'"')
        ->fetchOne();
    }
    public static function fetchByDate($date)
    {
      return Doctrine_Query::create()
        ->from('Transfer i')
        ->where('date="'.$date.'"')
        ->orderBy('i.code')
        ->execute();
    }
    public static function fetchByDateRange($fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Transfer i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->orderBy('i.date,i.code')
        ->execute();
    }

    public static function fetchUnclosedByDateRange($fromdate,$todate,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('Transfer i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('is_temporary!=0')
        ->andWhere('is_cancelled!=1')
        ->orderBy('i.code');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function countUnclosedByDateRange($fromdate,$todate)
    {
      return self::fetchUnclosedByDateRange($fromdate,$todate,true);
    }
    public static function fetchUnclosedByDate($date)
    {
      return self::fetchUnclosedByDateRange($date,$date);
    }
    public static function countUnclosedByDate($date)
    {
      return self::fetchUnclosedByDateRange($date,$date,true);
    }
}