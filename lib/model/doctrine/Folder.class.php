<?php

/**
 * Folder
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Folder extends BaseFolder
{
  function getParent()
  {
     return Doctrine_Query::create()
        ->from('Folder f')
        ->where('f.id = '.$this->getParentId())
        ->fetchOne();
  }
  function getChildren()
  {
     return Doctrine_Query::create()
      ->from('Folder f')
      ->where('f.parent_id = '.$this->getId())
      ->execute();
  }
  public function getBreadcrumbs($action="view")
  {
    $array=array();
    $folder=$this;
    
    if($this->getId()=="")
      $array[]=" > New Folder";
    else
      $array[]=" > ".link_to($folder->getTitle(),"folder/".$action."?id=".$folder->getId());
    
    while(!$folder->isRoot())
    {
      $parent=$folder->getParent();
      $array[]=" > ".link_to($parent->getTitle(),"folder/".$action."?id=".$parent->getId());
      $folder=$parent;
    }
    $array=array_reverse($array);

    return implode(" ",$array);
  }
  public function isRoot()
  {
    return ($this->getParentId()==null or $this->getParentId()==0)?true:false;
  }
}
