<?php


class ReturnsTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Returns');
    }
    public static function genInCode()
    {
      $prefix="SRet";//sales return
      return self::genCode($prefix);
    }
    public static function genOutCode()
    {
      $prefix="PRet";//purchase return
      return self::genCode($prefix);
    }
    public static function genCode($prefix)
    {
      $year=date("y");
    
      $last=Doctrine_Query::create()
        ->from('Returns i')
        ->where('code like "'.$prefix.$year.'%"')
        ->orderBy(' code desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getCode();
        $code=substr($code,6);
        $code+=1;
      }
      return $prefix."".$year."".str_pad($code, 4, "0", STR_PAD_LEFT);
      
    }
}
