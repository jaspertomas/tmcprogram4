<?php

class Fetcher
{
  public static function fetchById($class,$id)
  {
      return Doctrine_Query::create()
        ->from($class.' s')
      	->where('s.id = '.$id)
      	->fetchOne();
  }
  public static function fetch($class,$array)
  {
      $query= Doctrine_Query::create()
        ->from($class.' s')
    		->where("1");

      foreach($array as $key=>$value)
      	$query->andWhere($key.' = '.$value);

    	return $query->execute();
  }
  public static function fetchWithOrderBy($class,$array,$orderBy)
  {
      $query= Doctrine_Query::create()
        ->from($class.' s')
    		->where("1");

      foreach($array as $key=>$value)
      	$query->andWhere($key.' = '.$value);

      return $query->orderBy($orderBy)->execute();
  }
  public static function fetchOne($class,$array)
  {
      $query= Doctrine_Query::create()
        ->from($class.' s')
    		->where("1");
    		
      foreach($array as $key=>$value)
      {
        if($value==null)
          $query->andWhere($key.' is null ');
        else
          $query->andWhere($key.' = '.$value);
      }

    	return $query->fetchOne();
  }
}
