<?php


class InvoiceDrTable extends Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('InvoiceDr');
    }
    public static function genDrForInvoiceId($invoice_id,$user)
    {

        //if an existing unreleased dr exists, update it and use that
        $existingDr=Fetcher::fetchOne("InvoiceDr",array("invoice_id"=>$invoice_id, "is_released"=>null));
        if($existingDr!=null)
        {
            $existingDr->updateToInvoice();
            return $existingDr;
        }

        $dr = new InvoiceDr();
        $dr->setInvoiceId($invoice_id);
        $invoice=$dr->getInvoice();

        //scan invoicedetails, see if there are any returns
        //if yes, this dr is a return
        $is_return=false;
        foreach($invoice->getInvoicedetail() as $id)
        {
          $qty_remaining=$id->getQty()-$id->getQtyReleased();
          //disable: ignore prodict if is_service
          // if(!$id->getProduct()->isService() and $qty_remaining<0)
          if($qty_remaining<0)
          {
            $is_return=true;
            break;
          }
        }

        $dr->setCustomerId($invoice->getCustomerId());
        $dr->setDatetime(MyDateTime::now()->todatetime());
        $dr->genCode();
        $dr->setIsReturn($is_return?1:null);
        $setting=Fetcher::fetchOne("Settings",array("name"=>"'default_warehouse_id'"));
        $dr->setWarehouseId($setting->getValue());

        $dr->setCreatedById($user->getId());
        $dr->setUpdatedById($user->getId());
        $dr->setCreatedAt(MyDateTime::now()->todatetime());
        $dr->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr->save();

        foreach($invoice->getInvoicedetail() as $id)
        {
            //create new detail
            //set invoice data
            $dr_detail=new InvoiceDrDetail();
            $dr_detail->setInvoiceDrId($dr->getId());
            $dr_detail->setInvoiceId($dr->getInvoiceId());
            $dr_detail->setCustomerId($dr->getCustomerId());
            $dr_detail->setWarehouseId($dr->getWarehouseId());

            //set invoicedetail data
            $dr_detail->setInvoicedetailId($id->getId());
            $dr_detail->setDatetime($dr->getDatetime());
            $dr_detail->setProductId($id->getProductId());

            //set qty data
            $qty_remaining=$id->getQty()-$id->getQtyReleased();

          //disable: ignore prodict if is_service
            //if product is service, ignore
            // if($id->getProduct()->isService())
            //   $dr_detail->setQty(0);
            // else 
            //if dr is return, set non return dr details to 0
            if($is_return and $qty_remaining>=0)
              $dr_detail->setQty(0);
            //else do normally
            else
              $dr_detail->setQty($qty_remaining);
            
            //set as return if qty is negative
            $dr_detail->setIsReturn(($qty_remaining<0)?1:0);
    
            $dr_detail->setCreatedById($user->getId());
            $dr_detail->setUpdatedById($user->getId());
            $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
            $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
            $dr_detail->save();

            $dr_detail->updateStockEntry(0);
        }

        $dr->calc();

        return $dr;
    }
    public static function genDrForInvoiceReturnId($invoice_return_id,$user, $is_return)
    {
      /*
        //if an existing unreleased dr exists, update it and use that
        $existingDr=Fetcher::fetchOne("InvoiceDr",array("invoice_return_id"=>$invoice_return_id, "is_released"=>null));
        if($existingDr!=null)
        {
            // $existingDr->updateToInvoice();
            return $existingDr;
        }
      */

        $dr = new InvoiceDr();
        $dr->setInvoiceReturnId($invoice_return_id);
        $invoice_return=$dr->getInvoiceReturn();
        $dr->setInvoiceId($invoice_return->getInvoiceId());
        $invoice=$dr->getInvoice();

        $dr->setCustomerId($invoice->getCustomerId());
        $dr->setDatetime(MyDateTime::now()->todatetime());
        $dr->genCode();
        $dr->setIsReturn($is_return?1:null);
        $setting=Fetcher::fetchOne("Settings",array("name"=>"'default_warehouse_id'"));
        $dr->setWarehouseId($setting->getValue());

        $dr->setCreatedById($user->getId());
        $dr->setUpdatedById($user->getId());
        $dr->setCreatedAt(MyDateTime::now()->todatetime());
        $dr->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr->save();

        foreach($invoice->getInvoicedetail() as $id)
        {
            //create new detail
            //set invoice data
            $dr_detail=new InvoiceDrDetail();
            $dr_detail->setInvoiceDrId($dr->getId());
            $dr_detail->setInvoiceId($dr->getInvoiceId());
            $dr_detail->setCustomerId($dr->getCustomerId());
            $dr_detail->setWarehouseId($dr->getWarehouseId());

            //set invoicedetail data
            $dr_detail->setInvoicedetailId($id->getId());
            $dr_detail->setDatetime($dr->getDatetime());
            $dr_detail->setProductId($id->getProductId());

            //set qty data
            $qty_remaining=$id->getQty()-$id->getQtyReleased();

          //disable: ignore prodict if is_service
            //if product is service, ignore
            // if($id->getProduct()->isService())
            //   $dr_detail->setQty(0);
            // else 
            //if dr is return, set non return dr details to 0
            if($is_return and $qty_remaining>=0)
              $dr_detail->setQty(0);
            //else do normally
            else
              $dr_detail->setQty($qty_remaining);
            
            //set as return if qty is negative
            $dr_detail->setIsReturn(($qty_remaining<0)?1:0);
    
            $dr_detail->setCreatedById($user->getId());
            $dr_detail->setUpdatedById($user->getId());
            $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
            $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
            $dr_detail->save();

            $dr_detail->updateStockEntry(0);
        }

        $dr->calc();

        return $dr;
    }
    public static function fetchByDateRange($fromdatetime,$todatetime)
    {
      return Doctrine_Query::create()
        ->from('InvoiceDr i')
        ->where('i.datetime >= "'.$fromdatetime.'"')
        ->andWhere('i.datetime <= "'.$todatetime.'"')
        ->orderBy('i.code')
        ->execute();
    }
    public static function fetchByDate($datetime)
    {
      return self::fetchByDateRange($datetime,$datetime);
    }

    public static function fetchUnreleasedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('InvoiceDr i')
        ->where('i.datetime >= "'.$fromdatetime.' 00:00:00"')
        ->andWhere('i.datetime <= "'.$todatetime.' 23:59:59"')
        ->andWhere('(i.is_released is null or i.is_released = 0)')
        ->andWhere('(i.is_cancelled is null or i.is_cancelled = 0)')
        ->andWhere('(i.is_return is null or i.is_return = 0)')
        ->orderBy('i.datetime desc');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnreleasedByDate($datetime)
    {
      return self::fetchUnreleasedByDateRange($datetime,$datetime);
    }
    public static function countUnreleasedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      return self::fetchUnreleasedByDateRange($fromdatetime,$todatetime,true);
    }
    public static function countUnreleasedByDate($datetime)
    {
      return self::fetchUnreleasedByDateRange($datetime,$datetime,true);
    }
    public static function fetchUnreturnedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('InvoiceDr i')
        ->where('i.datetime >= "'.$fromdatetime.' 00:00:00"')
        ->andWhere('i.datetime <= "'.$todatetime.' 23:59:59"')
        ->andWhere('(i.is_released is null or i.is_released = 0)')
        ->andWhere('(i.is_cancelled is null or i.is_cancelled = 0)')
        ->andWhere('(i.is_return = 1)')
        ->orderBy('i.datetime desc');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnreturnedByDate($datetime)
    {
      return self::fetchUnreturnedByDateRange($datetime,$datetime);
    }
    public static function countUnreturnedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      return self::fetchUnreturnedByDateRange($fromdatetime,$todatetime,true);
    }
    public static function countUnreturnedByDate($datetime)
    {
      return self::fetchUnreturnedByDateRange($datetime,$datetime,true);
    }
}
