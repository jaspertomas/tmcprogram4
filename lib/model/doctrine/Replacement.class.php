<?php

/**
 * Replacement
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Replacement extends BaseReplacement
{
  function __toString(){return $this->getRefClass()." Replacement ".$this->getCode();}
  function getName(){return $this->getCode();}
  function cascadeDelete()
  {
    foreach($this->getReplacementDetail() as $detail)
    {
        $detail->delete();
    }
    return $this->delete();
  }  
  function getRef()
  {
    switch($this->getRefClass())
    {
      case "Invoice":
      case "Purchase":
        return Doctrine_Query::create()
          ->from(ucwords($this->getRefClass()).' i')
          ->where('i.id = '.$this->getRefId())
        	->fetchOne()
          ;
        break;
      default:
        return null;
    }
  }
  function getRefNo($ref)
  {
    switch($this->getRefClass())
    {
      case "Invoice":
        return $ref->getInvoiceTemplate()." ".$ref->getInvno();
      case "Purchase":
        return $ref->getPurchaseTemplate()." ".$ref->getPono();
      default:
        return null;
    }
  }
  function getClient()
  {
    switch($this->getClientClass())
    {
      case "Customer":
      case "Vendor":
        return Doctrine_Query::create()
          ->from(ucwords($this->getClientClass()).' i')
          ->where('i.id = '.$this->getClientId())
        	->fetchOne()
          ;
        break;
      default:
        $ref=$this->getRef();
        switch($this->getRefClass())
        {
          case "Invoice":return $ref->getCustomer();
          case "Purchase":return $ref->getVendor();
          default:
            return null;
        }
    }
  }
  function getStatusColor()
  {
    switch($this->getStatus())
    {
      case "Pending": return "red";
      case "Complete": return "green";
      case "Cancelled": return "black";
    }
  }
  function getPaymentStatusColor()
  {
    switch($this->getPaymentStatus())
    {
      case "Pending": return "red";
      case "Paid": return "green";
      case "Cancelled": return "black";
    }
  }
  function calc()
  {
    $total=0;
    foreach($this->getReplacementdetail() as $detail)
      $total+=$detail->getTotal();
    $this->setTotal($total);
  }
  function getParticularsString()
  {
    $array=array();
  
    foreach($this->getReplacementDetail() as $detail)
    {
      $string=intval($detail->getQty())." x ".$detail->getProduct()->getName()." (".$detail->getProduct()->getProductcategory().")";
      if($detail->getQty()<0)$string.=" (return)";

      $array[]=$string;
    }
    
    return implode("; ",$array);
  }
  function getIsSales()
  {
    return $this->getClientClass()=="Customer";
  }
  function getPrettyType()
  {
    return $this->getType()=="Vendor"?"Supplier":$this->getType();
  }

  function setDateAndUpdateStockEntry($date)
  {
    $olddate=$this->getDate();
    $this->setDate($date);
    $newdate=$this->getDate();
    
    //if date changed, update dates of all stock entries, and recalc stock entries
    if($olddate!=$newdate)
    {
      //determine which is earlier - newdate or olddate
      //the earlier date will determine when to start recalculating
      $olddatetime=MyDateTime::frommysql($olddate);
      $newdatetime=MyDateTime::frommysql($newdate);
      if($olddatetime->isearlierthan($newdatetime))
        $earlierdate=$olddate;
      else
        $earlierdate=$newdate;
        
      //for each stock entry, recalculate from before earlier date
      //find entry before earlier date
      //and start recalculating from that stock entry
      foreach($this->getReplacementDetail() as $detail)
      {
        $entry=$detail->getStockEntry();
        $entry->setDate($newdate);
        $entry->save();
        $lastentry=$entry->getLastBeforeDate($earlierdate);
        $entry->getStock()->calcFromStockEntry($lastentry);
      }
    }
  }
}
