<?php


class QuotationTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Quotation');
    }
  public static function getNextCode()
  {
    $last=Doctrine_Query::create()
    ->from('Quotation q')
  	->orderBy("code desc")
  	->fetchOne();
  	
  	if($last==null)
  	  return "0001";
	  else
	  {
	    $lastcode=intval($last->getCode())+1;
	    return str_pad ( $lastcode , 4, "0", STR_PAD_LEFT);
	  }
  }
}
