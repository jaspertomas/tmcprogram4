<?php


class VoucherTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Voucher');
    }
    public static function genCode()
    {
        $year=date("y");
        $month=date("m");
      $last=Doctrine_Query::create()
        ->from('Voucher v')
        ->where('no like "V'.$year.$month.'%"')
        ->orderBy(' no desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getNo();
        $code=substr($code,5);
        $code+=1;
      }
      return "V".$year.$month.str_pad($code, 4, "0", STR_PAD_LEFT);
    }
    public static function fetchByDate($date)
    {
      return Doctrine_Query::create()
        ->from('Voucher i, i.VoucherAccount a, i.VoucherType t')
        ->where('date="'.$date.'"')
        ->orderBy('i.date,i.time')
        ->execute();
    }
    public static function fetchPettyCashVouchersByDate($date)
    {
      return Doctrine_Query::create()
        ->from('Voucher i, i.VoucherAccount a, i.VoucherType t')
        ->where('date="'.$date.'"')
        ->andWhere('voucher_type_id=1')
        ->orderBy('i.date,i.time')
        ->execute();
    }
    public static function fetchByDateRange($fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Voucher i, i.VoucherAccount a, i.VoucherType t')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->orderBy('i.date,i.time')
        ->execute();
    }
    public static function fetchByEffectiveDateRange($fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Voucher i, i.VoucherAccount a, i.VoucherType t')
        ->where('i.date_effective >= "'.$fromdate.'"')
        ->andWhere('i.date_effective <= "'.$todate.'"')
        ->orderBy('i.date,i.time')
        ->execute();
    }
    public static function fetchUnreceivedUnclearedByCheckDateRangeAndAccount($fromdate,$todate,$passbook_id)
    {
      return Doctrine_Query::create()
        ->from('Voucher v, v.OutCheck c')
        ->where('c.check_date >= "'.$fromdate.'"')
        ->andWhere('c.check_date <= "'.$todate.'"')
        ->andWhere('c.receive_date is null')
        ->andWhere('c.cleared_date is null')
        ->andWhere('c.is_cancelled is null')
        ->andWhere('c.passbook_id = '.$passbook_id)
        ->orderBy('c.cleared_date')
        ->execute();
    }
    public static function fetchReceivedUnclearedByCheckDateRangeAndAccount($fromdate,$todate,$passbook_id)
    {
      return Doctrine_Query::create()
        ->from('Voucher v, v.OutCheck c')
        ->where('c.check_date >= "'.$fromdate.'"')
        ->andWhere('c.check_date <= "'.$todate.'"')
        ->andWhere('c.receive_date is not null')
        ->andWhere('c.cleared_date is null')
        ->andWhere('c.is_cancelled is null')
        ->andWhere('c.passbook_id = '.$passbook_id)
        ->orderBy('c.cleared_date')
        ->execute();
    }
    public static function fetchClearedByCheckDateRangeAndAccount($fromdate,$todate,$passbook_id)
    {
      return Doctrine_Query::create()
        ->from('Voucher v, v.OutCheck c')
        ->where('c.check_date >= "'.$fromdate.'"')
        ->andWhere('c.check_date <= "'.$todate.'"')
        ->andWhere('c.cleared_date is not null')
        ->andWhere('c.is_cancelled is null')
        ->andWhere('c.passbook_id = '.$passbook_id)
        ->orderBy('c.cleared_date')
        ->execute();
    }
    public static function fetchCancelledByCheckDateRangeAndAccount($fromdate,$todate,$passbook_id)
    {
      return Doctrine_Query::create()
        ->from('Voucher v, v.OutCheck c')
        ->where('c.check_date >= "'.$fromdate.'"')
        ->andWhere('c.check_date <= "'.$todate.'"')
        ->andWhere('c.is_cancelled is not null')
        ->andWhere('c.passbook_id = '.$passbook_id)
        ->orderBy('c.cleared_date')
        ->execute();
    }
}
