<?php


class PassbookEntryTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassbookEntry');
    }
    public static function fetchByDate($date)
    {
      return Doctrine_Query::create()
        ->from('PassbookEntry i, i.Passbook s, s.Product p')
        ->where('datetime>="'.$date.' 00:00:00"')
        ->andWhere('datetime<="'.$date.' 23:59:59"')
        ->orderBy('datetime desc, id desc')
        ->execute();
    }
    public static function fetchByDateRange($fromdatetime,$todatetime)
    {
      return Doctrine_Query::create()
        ->from('PassbookEntry i, i.Passbook s, s.Product p')
        ->where('i.datetime >= "'.$fromdatetime.' 00:00:00"')
        ->andWhere('i.datetime <= "'.$todatetime.' 23:59:59"')
        ->orderBy('datetime desc, id desc')
        ->execute();
    }
    /*
    public static function fetchByProductTypeIdGroupByLatestAsOfDate($producttype_id,$date)
    {
      $pdo = Doctrine_Manager::getInstance()->getCurrentConnection()->getDbh();

      //put passbook_ids in an array for later use
      $query = 'select passbook.id from passbook inner join product on passbook.product_id=product.id where warehouse_id = '.$warehouse_id.' AND producttype_id = '.$producttype_id.'';
      $stmt = $pdo->prepare($query);
      $stmt->execute();
      $results = $stmt->fetchAll();  
      $passbook_ids=array();
      foreach($results as $result)
        $passbook_ids[]=$result[0];

      //if no passbook ids, return empty array
      if(count($passbook_ids)==0)return array();
    
      $output=array();
      //get last passbook entry for each passbook
      foreach($passbook_ids as $passbook_id)
      {
        //this query is copied from passbook->getLastForDate()
        $output[]=Doctrine_Query::create()
        ->from('PassbookEntry se')
        ->where('se.passbook_id = '.$passbook_id)
        ->andWhere('se.datetime <= "'.$date.' 23:59:59"')
        //ignore passbookentries for invoice/purchase drs with qty=0
        //this causes an error
        // ->andWhere('se.qty != 0')
        ->orderBy('datetime desc, id desc')
        ->fetchOne();
      }
      return $output;
    }
    */
}