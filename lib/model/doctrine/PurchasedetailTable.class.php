<?php


class PurchasedetailTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Purchasedetail');
    }
    public static function fetchByProduct($product)
    {
      return Doctrine_Query::create()
        ->from('Purchasedetail pd, pd.Purchase i')
        ->where('pd.product_id = '.$product->getId())
        ->orderBy('i.date desc')
        ->execute();
    }
    public static function countByProduct($product)
    {
      return Doctrine_Query::create()
        ->from('Purchasedetail pd')
        ->where('pd.product_id = '.$product->getId())
        ->count();
    }
    public static function fetchByProductAndPage($product,$page,$perpage)
    {
      return Doctrine_Query::create()
        ->from('Purchasedetail pd, pd.Purchase i')
        ->where('pd.product_id = '.$product->getId())
        ->orderBy('i.date desc')
        ->limit($perpage)
        ->offset($page*$perpage)
        ->execute();
    }
    public static function fetchByProductAndDateRange($product,$fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Purchasedetail pd, pd.Purchase i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('pd.product_id = '.$product->getId())
        #->andWhere('i.is_temporary=0')
        ->orderBy('i.date desc')
        ->execute();
    }
    public static function getColorArray()
    {
      return array(null,'red','orange','yellow','green','blue','indigo','violet');
    }
    public static function getColorForIndex($index)
    {
      $colorarray=self::getColorArray();
      return $colorarray[$index];
    }
    //usage: list($slot, $color) = PurchasedetailTable::getNextSlotAndColor($requestparams['$product_id']);
    public static function getNextSlotAndColor($product_id)
    {
      $last=Doctrine_Query::create()
        ->from('Purchasedetail pd')
        ->where('pd.product_id = '.$product_id)
        ->orderBy('pd.id desc')
        ->limit(1)
        ->fetchOne();
      if($last==null)return array(1,'red');
      $slot=$last->getSlot()+1;
      if($slot>8)$slot=1;
      $colorIndex=$last->getColorIndex();
      if($colorIndex==null)$colorIndex=0;
      $colorIndex+=1;
      if($colorIndex>7)$colorIndex=1;
      return array($slot,self::getColorForIndex($colorIndex));
    }
    
}
