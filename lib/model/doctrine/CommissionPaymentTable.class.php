<?php


class CommissionPaymentTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CommissionPayment');
    }
}