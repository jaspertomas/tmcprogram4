<?php


class InCheckTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('InCheck');
    }
    public static function genCode()
    {
        $year=date("y");
        $month=date("m");
      $last=Doctrine_Query::create()
        ->from('InCheck c')
        ->where('code like "I'.$year.$month.'%"')
        ->orderBy(' code desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getCode();
        $code=substr($code,5);
        $code+=1;
      }
      return "I".$year.$month.str_pad($code, 4, "0", STR_PAD_LEFT);
    }
}
