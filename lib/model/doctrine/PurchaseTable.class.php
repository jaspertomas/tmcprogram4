<?php


class PurchaseTable extends Doctrine_Table
{
    public static function genBackLoadCode()
    {
      $year=date("Y");
    
      $last=Doctrine_Query::create()
        ->from('Purchase i')
        //->where('template_id='.SettingsTable::fetch("purchase_template_delivery_receipt_id"))
        ->where('pono like "BL'.$year.'-%"')
        ->orderBy(' LPAD(pono,10,0) desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getPono();
        $code=explode("-",$code);
        $code=$code[1];
        $code+=1;
      }
      return "BL".$year."-".str_pad($code, 3, "0", STR_PAD_LEFT);
      
    }
    public static function genPurchaseOrderCode()
    {
      $last=Doctrine_Query::create()
        ->from('Purchase i')
        //->where('template_id='.SettingsTable::fetch("purchase_template_delivery_receipt_id"))
        ->where('pono regexp "^[0-9]*$"')
        ->orderBy(' LPAD(pono,6,0) desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
//      var_dump($last->getId());die();
        $code=$last->getPono();
        $code+=1;
      }
      return str_pad($code, 6, "0", STR_PAD_LEFT);
      
    }
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Purchase');
    }
    public static function fetchOneByInvno($invno)
    {
      return Doctrine_Query::create()
        ->from('Purchase i')
        ->where('pono="'.$pono.'"')
        ->fetchOne();
    }
    public static function fetchByDate($date)
    {
      return Doctrine_Query::create()
        ->from('Purchase i')
        ->where('date="'.$date.'"')
        ->orderBy('i.pono')
        ->execute();
    }
    public static function fetchByDateRange($fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Purchase i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->orderBy('i.date,i.pono')
        ->execute();
    }
    public static function genFromInvoice($vendor,$invoice,$invoicedetail_ids,$qtys,$prices)
    {
      //generate po from invoice, include only given invoice details 
      $po=new Purchase();
      //$po->setPono(self::genPurchaseOrderCode());
      $po->setTemplateId(sfConfig::get('custom_default_po_template_id'));
      $po->genPono();
      $po->setInvno($invoice->getInvno());
      $po->setInvoiceId($invoice->getId());
      $po->setDate(MyDate::today());
      $po->setTerms($vendor->getTerms());
      $po->calcDueDate();
      $po->setVendorId($vendor->getId());
      $po->setEmployeeId(sfConfig::get('custom_default_po_employee_id'));
      $po->setVendorId($vendor->getId());
      
      $po->save();
      
      //create purchasedetails      
      foreach($invoicedetail_ids as $id)
      {
        $invoicedetail=MyModel::fetchOne("Invoicedetail",array('id'=>$id));
        $detail=new Purchasedetail();
        $detail->setProductId($invoicedetail->getProductId());
        $detail->setQty($qtys[$id]);
        $detail->setQtyReceived(0);
        $detail->setPurchaseId($po->getId());
        $detail->setPrice($prices[$id]);
        $detail->setSellprice($invoicedetail->getPrice());
        $detail->calc();
        $detail->save();
      }
      
      $po->calc();
      $po->save();
      return $po;
    }
    public static function fetchUnclosedByDateRange($fromdate,$todate,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('Purchase i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('is_temporary!=0')
        ->andWhere('status!="Cancelled"')
        ->orderBy('i.date desc');
      if($count)return $query->count();
      else return $query->execute();
    }
    //--------------------
    public static function countUnclosedByDateRange($fromdate,$todate)
    {
      return self::fetchUnclosedByDateRange($fromdate,$todate,true);
    }
    public static function fetchUnclosedByDate($date)
    {
      return self::fetchUnclosedByDateRange($date,$date);
    }
    public static function countUnclosedByDate($date)
    {
      return self::fetchUnclosedByDateRange($date,$date,true);
    }
    //--------------------
    public static function fetchUnpaidByDateRange($fromdate,$todate,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('Purchase i, i.Vendor c')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('i.is_temporary=0')
        ->andWhere('i.status!="Paid"')
        ->andWhere('i.status!="Cancelled"')
        // ->andWhere('c.is_owned!=1')
        ->orderBy('i.date desc');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function countUnpaidByDateRange($fromdate,$todate)
    {
      return self::fetchUnpaidByDateRange($fromdate,$todate,true);
    }
    public static function fetchUnpaidByDate($date)
    {
      return self::fetchUnpaidByDateRange($date,$date);
    }
    public static function countUnpaidByDate($date)
    {
      return self::fetchUnpaidByDateRange($date,$date,true);
    }
}
