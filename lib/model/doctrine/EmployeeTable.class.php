<?php


class EmployeeTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Employee');
    }
    public static function getTechnicians()
    {
        return Fetcher::fetch("Employee",array("is_technician"=>"1"));
    }
    public static function getSalesmen()
    {
        return Fetcher::fetch("Employee",array("is_salesman"=>"1"));
    }
}