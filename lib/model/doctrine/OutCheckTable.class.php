<?php


class OutCheckTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('OutCheck');
    }
    public static function genCode()
    {
        $year=date("y");
        $month=date("m");
      $last=Doctrine_Query::create()
        ->from('OutCheck c')
        ->where('code like "O'.$year.$month.'%"')
        ->orderBy(' code desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getCode();
        $code=substr($code,5);
        $code+=1;
      }
      return "O".$year.$month.str_pad($code, 4, "0", STR_PAD_LEFT);
    }
}
