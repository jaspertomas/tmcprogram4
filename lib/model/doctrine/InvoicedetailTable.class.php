<?php


class InvoicedetailTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Invoicedetail');
    }
    public static function fetchByProduct($product)
    {
      return Doctrine_Query::create()
        ->from('Invoicedetail id, id.Invoice i')
        ->where('id.product_id = '.$product->getId())
        ->orderBy('i.date desc')
        ->execute();
    }
    public static function fetchByProductUnpaid($product)
    {
      return Doctrine_Query::create()
        ->from('Invoicedetail id, id.Invoice i')
      	->where('i.status!="Paid"')
      	->andWhere('i.status!="Cancelled"')
      	->andWhere('i.hidden=0')
      	->andWhere('i.is_temporary=0')
        ->andWhere('id.product_id = '.$product->getId())
        ->orderBy('i.date desc')
        ->execute();
    }
    public static function countByProduct($product)
    {
      return Doctrine_Query::create()
        ->from('Invoicedetail id')
        ->where('id.product_id = '.$product->getId())
        ->count();
    }
    public static function fetchByProductAndPage($product,$page,$perpage)
    {
      return Doctrine_Query::create()
      ->from('Invoicedetail id, id.Invoice i')
      ->where('id.product_id = '.$product->getId())
        #->andWhere('i.is_temporary=0')
        ->orderBy('i.date desc')
        ->limit($perpage)
        ->offset($page*$perpage)
        ->execute();
    }    
    public static function fetchByDateRangeAndHasRemaining($fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Invoicedetail id, id.Invoice i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('id.remaining >0')
        ->andWhere('id.is_cancelled!=1')
        ->andWhere('i.is_temporary=0')
        ->orderBy('id.product_id')
        ->execute();
    }
    /*
    $is_commission_unpaid = true means return only invoices with commission not yet paid
    this is useful in calculating commission, to avoid paying commission twice on the same invoice
    */
    public static function fetchByDateRangeAndSalesmanIdAndHasRemaining($fromdate,$todate,$salesman_id,$is_commission_unpaid=false)
    {
      $query=Doctrine_Query::create()
        ->from('Invoicedetail id, id.Invoice i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('id.remaining !=0')
        ->andWhere('id.is_cancelled!=1')
        ->andWhere('i.is_temporary=0')
        //->andWhere('i.customer_id != 339')//tmc visayas
        ->andWhere('i.salesman_id = '.$salesman_id)
        ->orderBy('id.product_id');
        
      if($is_commission_unpaid)
        $query->andWhere('i.commission_payment_id is null');
        
      return $query->execute();
 
    }
    public static function fetchByProductAndDateRange($product,$fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Invoicedetail pd, pd.Invoice i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('pd.product_id = '.$product->getId())
        #->andWhere('i.is_temporary=0')
        ->orderBy('i.date desc')
        ->execute();
    }
    //this is to calculate sales reports by salesman 
    //calculate invoicedetail totals per date range
    //excluding vat and withholding tax
    public static function fetchByDateRangeAndSalesmanId($fromdate,$todate,$salesman_id)
    {
      $query=Doctrine_Query::create()
        ->from('Invoicedetail id, id.Invoice i, id.Product p')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('i.status!="Cancelled"')
        ->andWhere('i.salesman_id = '.$salesman_id)
        ->andWhere('id.is_vat = false')
        ->andWhere('p.is_tax = false')
        ->orderBy('i.date');
      return $query->execute();
    }
}
