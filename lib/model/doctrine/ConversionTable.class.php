<?php


class ConversionTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Conversion');
    }
/*

function genHoseConversion($name,$from_id,$to_id)
{
    $conversion = new Conversion();
    $conversion->setName($name." to per meter");
    $conversion->save();

    $product_ids=array($from_id);
    foreach($product_ids as $pid)
    {
      $detail=new Conversiondetail();
      $detail->setProductId($pid);
      $detail->setConversionId($conversion->getId());
      $detail->setQty(1);
      $detail->setType('from');
      $detail->save();
    }
    $detail=new Conversiondetail();
    $detail->setProductId($to_id);
    $detail->setConversionId($conversion->getId());
    $detail->setQty(100);
    $detail->setType('to');
    $detail->save();
}

function genHoseConversions()
{
  $array=array(
	717 => 135,
	80 => 495,
	81 => 328,
	23 => 257,
	24 => 258,
	562 => 580,
	513 => 509,
	506 => 507,
	);
  foreach($array as $from_id=>$to_id)
  {
      $product= Doctrine_Query::create()
        ->from('Product p')
        ->where('p.id = '.$from_id)
        ->fetchOne();
  ConversionTable::genHoseConversion($product->getName(),$from_id,$to_id);
  }
}

//this generates a conversion 
//given a name, a pump product id, a tank product id and a coupled product id
//the name is the name of the coupled product
function genCoupledConversion($name,$pump_id,$tank_id,$coupled_id)
{
		$conversion = new Conversion();
$conversion->setName("Parts to ".$name);
$conversion->save();

		$product_ids=array($pump_id,$tank_id,77,508,134);
    foreach($product_ids as $pid)
    {
      $detail=new Conversiondetail();
      $detail->setProductId($pid);
      $detail->setConversionId($conversion->getId());
      $detail->setQty(1);
      $detail->setType('from');
      $detail->save();
    }
    $detail=new Conversiondetail();
    $detail->setProductId($coupled_id);
    $detail->setConversionId($conversion->getId());
    $detail->setQty(1);
    $detail->setType('to');
    $detail->save();
}

//this generates several conversions
//given a 2d array consisting of 
//where the indexes are the product ids
//and the values are product ids of coupled products
//in this example,
//[0]=coupled 12 gals, [1]=coupled 21 gals, [2] = dynaflo bladder tank 6 gals
//customize as necessary
//don't forget to comment out after use
function genCoupledConversions()
{
$producttype_id=38;

$array=array(
52=>array(129,126,382),
683=>array(),
259=>array(131,133),
71=>array(74,125,75),
515=>array(538,524),	
571=>array(),			
570=>array(),
70=>array(275,274,278),
//18=>array(276,277,279),
);
$namesarray=array(
52=>"Eurostar DJM100",
683=>"General Pumps GP100S",
259=>"MAqua JET100",
71=>"Pentax PM45",
515=>"Speroni CAM100",	
571=>"STAC Jet 50",			
570=>"STAC Jet 75",
70=>"Stac PE45",
//18=>"Viva Jet100",
);
foreach($array as $id=>$couples)
{
      $product= Doctrine_Query::create()
        ->from('Product p')
  ->where('p.id = '.$id)
  ->fetchOne();
echo $product->getName()."<br>";

$name="Coupled ".$namesarray[$id]." + 12 Gals SS-304#16";
if(!isset($couples[0]))
{
//for 12 gals
echo "creating product ".$name."<br>";
$p=new Product();
$p->setName($name);
$p->setDescription($name);
$p->setProducttypeId($producttype_id);
$p->save();
$couples[0]=$p->getId();
}
$pump_id=$id;
$tank_id=73;
$coupled_id=$couples[0];
ConversionTable::genCoupledConversion($name,$pump_id,$tank_id,$coupled_id);

//for 21 gals
$name="Coupled ".$namesarray[$id]." + 21 Gals SS-304#16";
if(!isset($couples[1]))
{
echo "creating product ".$name."<br>";
$p=new Product();
$p->setName($name);
$p->setDescription($name);
$p->setProducttypeId($producttype_id);
$p->save();
$couples[1]=$p->getId();
}
$pump_id=$id;
$tank_id=76;
$coupled_id=$couples[0];
ConversionTable::genCoupledConversion($name,$pump_id,$tank_id,$coupled_id);


}
}
*/
}