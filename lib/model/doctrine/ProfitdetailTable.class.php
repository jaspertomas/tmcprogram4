<?php


class ProfitdetailTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Profitdetail');
    }
    public static function fetchByDate($date)
    {
      return Doctrine_Query::create()
        ->from('Profitdetail pd, pd.Invoice i')
        ->where('i.date="'.$date.'"')
        ->orderBy('i.invno')
        ->execute();
    }
    public static function fetchByDateRange($fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('Profitdetail pd, pd.Invoice i')
        ->where('pd.invoicedate >= "'.$fromdate.'"')
        ->andWhere('pd.invoicedate <= "'.$todate.'"')
        ->andWhere('i.is_temporary=0')
        ->orderBy('pd.product_id')
        ->execute();
    }
    /*
    $is_commission_unpaid = true means return only invoices with commission not yet paid
    this is useful in calculating commission, to avoid paying commission twice on the same invoice
    */
    public static function fetchByDateRangeAndSalesmanId($fromdate,$todate,$salesman_id,$is_commission_unpaid=false)
    {
      $query=Doctrine_Query::create()
        ->from('Profitdetail pd, pd.Invoicedetail id, id.Invoice i')
        ->where('pd.invoicedate >= "'.$fromdate.'"')
        ->andWhere('pd.invoicedate <= "'.$todate.'"')
        ->andWhere('i.is_temporary=0')
       //->andWhere('i.customer_id != 339')//tmc visayas
        ->andWhere('i.salesman_id = '.$salesman_id)
        ->orderBy('pd.product_id');

      if($is_commission_unpaid)
        $query->andWhere('i.commission_payment_id is null');

      return $query->execute();
    }

    //used by stock executeConnectInvoiceAndPurchase
    public static function connectDetails($product_id, $invoice, $invoice_detail_id, $purchase, $purchase_detail_id){
      $invoice_detail=Fetcher::fetchOne("Invoicedetail",array("id"=>$invoice_detail_id));
      $purchase_detail=Fetcher::fetchOne("Purchasedetail",array("id"=>$purchase_detail_id));

      // #check if both have remaining
      $remaining=$invoice_detail->getRemaining();
      if($purchase_detail->getRemaining()<$remaining)
        $remaining=$purchase_detail->getRemaining();
      if($remaining==0)return;

      // #no errors: connect invoice detail and purchase detail
      // #create profit detail
      // #calc invoice_detail
      // #calc purchase_detail
      // #total_remaining+=remaining
      $profit_detail=new Profitdetail();
      $profit_detail->setInvoicedetailId($invoice_detail->getId());
      $profit_detail->setPurchasedetailId($purchase_detail->getId());
      $profit_detail->setInvoiceId($invoice->getId());
      $profit_detail->setPurchaseId($purchase->getId());
      $profit_detail->setPurchaseId($purchase->getId());
      $profit_detail->setProductId($product_id);
      $profit_detail->setInvoicedate($invoice->getDate());
      $profit_detail->setPurchasedate($purchase->getDate());

      if($purchase_detail->getQty()==0)$profit_detail->setBuyprice(0);
      else $profit_detail->setBuyprice($purchase_detail->getTotal()/$purchase_detail->getQty());
      $profit_detail->setSellprice($invoice_detail->getPrice());
      $profit_detail->setQty($remaining);

      $profitperunit=$invoice_detail->getPrice()-$purchase_detail->getPrice();
      $profit_detail->setProfitperunit($profitperunit);
      #prevent divide by zero
      if($purchase_detail->getPrice()==0)
          $profit_detail->setProfitrate(100);
      else
          $profit_detail->setProfitrate($profitperunit/$purchase_detail->getPrice());
      $profit_detail->setProfit($profitperunit*$remaining);

      $profit_detail->setColor($purchase_detail->getColor());
      $profit_detail->setSlot($purchase_detail->getSlot());      
      $profit_detail->save();

      $invoice_detail->calcRemaining();
      $invoice_detail->save();

      $purchase_detail->calcRemaining();
      $purchase_detail->setSellprice($invoice_detail->getPrice());
      $purchase_detail->save();
  }
}
