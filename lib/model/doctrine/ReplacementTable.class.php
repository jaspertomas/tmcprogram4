<?php


class ReplacementTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Replacement');
    }
    public static function genCode($returns)
    {
      $oldreplacements=$returns->getReplacement();
      $count=count($oldreplacements);
      return str_replace("Ret","Rep",$returns->getCode()).chr(96 + $count+1);
    }
}
