<?php


class TransferDrTable extends Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TransferDr');
    }
    public static function genCode()
    {
      $last=Doctrine_Query::create()
        ->from('TransferDr i')
        // ->where('code regexp "^[0-9]*$"')
        ->orderBy(' LPAD(code,6,0) desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getCode();
        $code+=1;
      }
      return str_pad($code, 6, "0", STR_PAD_LEFT);
      
    }
    public static function genDrForTransferId($transfer_id,$user)
    {
        $dr = new TransferDr();
        $dr->setTransferId($transfer_id);
        $transfer=$dr->getTransfer();

        $dr->setDatetime(MyDateTime::now()->todatetime());
        $dr->genCode();

        $dr->setCreatedById($user->getId());
        $dr->setUpdatedById($user->getId());
        $dr->setCreatedAt(MyDateTime::now()->todatetime());
        $dr->setUpdatedAt(MyDateTime::now()->todatetime());
        $dr->save();

        foreach($transfer->getTransferDetail() as $td)
        {
            //create new detail
            //set transfer data
            $dr_detail=new TransferDrDetail();
            $dr_detail->setTransferDrId($dr->getId());
            $dr_detail->setTransferDetailId($td->getId());
            $dr_detail->setTransferId($dr->getTransferId());

            //set transferdetail data
            // $dr_detail->setTransferdetailId($td->getId());
            $dr_detail->setDatetime($dr->getDatetime());
            $dr_detail->setProductId($td->getProductId());

            $qty_remaining=$td->getQty()-$td->getQtyTransferred();
            $dr_detail->setQty($qty_remaining);
            
            $dr_detail->setCreatedById($user->getId());
            $dr_detail->setUpdatedById($user->getId());
            $dr_detail->setCreatedAt(MyDateTime::now()->todatetime());
            $dr_detail->setUpdatedAt(MyDateTime::now()->todatetime());
            $dr_detail->save();

            //create stock entries
            $dr_detail->getFromStockEntry();
            $dr_detail->getToStockEntry();
        }

        $dr->calc();

        return $dr;
    }
    public static function fetchByDateRange($fromdatetime,$todatetime)
    {
      return Doctrine_Query::create()
        ->from('TransferDr i')
        ->where('i.date >= "'.$fromdatetime.'"')
        ->andWhere('i.date <= "'.$todatetime.'"')
        ->orderBy('i.code')
        ->execute();
    }
    public static function fetchByDate($datetime)
    {
      return self::fetchByDateRange($datetime,$datetime);
    }
    public static function fetchUnreceivedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('TransferDr i')
        ->where('i.date >= "'.$fromdatetime)
        ->andWhere('i.date <= "'.$todatetime)
        ->andWhere('(i.is_received is null or i.is_received = 0)')
        ->andWhere('(i.is_cancelled is null or i.is_cancelled = 0)')
        ->andWhere('(i.is_return is  null or i.is_return = 0)')
        ->orderBy('i.code');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnreceivedByDate($datetime)
    {
      return self::fetchUnreceivedByDateRange($datetime,$datetime);
    }
    public static function countUnreceivedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      return self::fetchUnreceivedByDateRange($fromdatetime,$todatetime,true);
    }
    public static function countUnreceivedByDate($datetime)
    {
      return self::fetchUnreceivedByDateRange($datetime,$datetime,true);
    }
    public static function fetchUnreturnedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('TransferDr i')
        ->where('i.date >= "'.$fromdatetime)
        ->andWhere('i.date <= "'.$todatetime)
        ->andWhere('(i.is_received is null or i.is_received = 0)')
        ->andWhere('(i.is_cancelled is null or i.is_cancelled = 0)')
        ->andWhere('(i.is_return = 1)')
        ->orderBy('i.code');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function fetchUnreturnedByDate($datetime)
    {
      return self::fetchUnreturnedByDateRange($datetime,$datetime);
    }
    public static function countUnreturnedByDateRange($fromdatetime,$todatetime,$count=false)
    {
      return self::fetchUnreturnedByDateRange($fromdatetime,$todatetime,true);
    }
    public static function countUnreturnedByDate($datetime)
    {
      return self::fetchUnreturnedByDateRange($datetime,$datetime,true);
    }
}
