<?php


class DiscountLevelTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('DiscountLevel');
    }
	function getTotalDiscount($discountstring)
	{
		$segments=explode(" ",$discountstring);
		$base=100;
		foreach($segments as $segment)
		{
		$base=$base*(100-$segment)/100;
		}
		return 100-$base;
	}
}