<?php


class JobOrderTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('JobOrder');
    }
    public static function genCode()
    {
      $last=Doctrine_Query::create()
        ->from('JobOrder i')
        // ->where('code regexp "^[0-9]*$"')
        ->orderBy(' LPAD(code,6,0) desc')
        ->fetchOne();
      if($last==null)
      {
        $code="1";
      }
      else
      {
        $code=$last->getCode();
        $code+=1;
      }
      return str_pad($code, 6, "0", STR_PAD_LEFT);
      
    }
    public static function fetchOneByCode($code)
    {
      return Doctrine_Query::create()
        ->from('JobOrder i')
        ->where('code="'.$code.'"')
        ->fetchOne();
    }
    public static function fetchByDate($date)
    {
      return Doctrine_Query::create()
        ->from('JobOrder i')
        ->where('date="'.$date.'"')
        ->orderBy('i.code')
        ->execute();
    }
    public static function fetchByDateRange($fromdate,$todate)
    {
      return Doctrine_Query::create()
        ->from('JobOrder i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->orderBy('i.date,i.code')
        ->execute();
    }

    public static function fetchUnclosedByDateRange($fromdate,$todate,$count=false)
    {
      $query = Doctrine_Query::create()
        ->from('JobOrder i')
        ->where('i.date >= "'.$fromdate.'"')
        ->andWhere('i.date <= "'.$todate.'"')
        ->andWhere('is_temporary!=0')
        ->andWhere('is_cancelled!=1')
        ->orderBy('i.code');
      if($count)return $query->count();
      else return $query->execute();
    }
    public static function countUnclosedByDateRange($fromdate,$todate)
    {
      return self::fetchUnclosedByDateRange($fromdate,$todate,true);
    }
    public static function fetchUnclosedByDate($date)
    {
      return self::fetchUnclosedByDateRange($date,$date);
    }
    public static function countUnclosedByDate($date)
    {
      return self::fetchUnclosedByDateRange($date,$date,true);
    }
    /*
    public static function genFromInvoice($vendor,$invoice,$invoicedetail_ids,$qtys,$prices)
    {
      //generate po from invoice, include only given invoice details 
      $jo=new Purchase();
      //$jo->setCode(self::genPurchaseOrderCode());
      $jo->setTemplateId(sfConfig::get('custom_default_po_template_id'));
      $jo->genCode();
      $jo->setInvno($invoice->getInvno());
      $jo->setInvoiceId($invoice->getId());
      $jo->setDate(MyDate::today());
      $jo->setTerms($vendor->getTerms());
      $jo->calcDueDate();
      $jo->setVendorId($vendor->getId());
      $jo->setEmployeeId(sfConfig::get('custom_default_po_employee_id'));
      $jo->setVendorId($vendor->getId());
      
      $jo->save();
      
      //create purchasedetails      
      foreach($invoicedetail_ids as $id)
      {
        $invoicedetail=MyModel::fetchOne("Invoicedetail",array('id'=>$id));
        $detail=new Purchasedetail();
        $detail->setProductId($invoicedetail->getProductId());
        $detail->setQty($qtys[$id]);
        $detail->setQtyReceived(0);
        $detail->setPurchaseId($jo->getId());
        $detail->setPrice($prices[$id]);
        $detail->setSellprice($invoicedetail->getPrice());
        $detail->calc();
        $detail->save();
      }
      
      $jo->calc();
      $jo->save();
      return $jo;
    }

*/
}