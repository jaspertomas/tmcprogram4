<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('OutPayment', 'doctrine');

/**
 * BaseOutPayment
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $type
 * @property string $parent_class
 * @property integer $parent_id
 * @property string $parent_name
 * @property string $child_class
 * @property string $children_id
 * @property date $date
 * @property decimal $amount
 * @property string $detail1
 * @property string $detail2
 * @property string $detail3
 * @property string $notes
 * @property date $checkcleardate
 * @property integer $out_check_id
 * @property OutCheck $OutCheck
 * 
 * @method integer    getId()             Returns the current record's "id" value
 * @method string     getType()           Returns the current record's "type" value
 * @method string     getParent_class()   Returns the current record's "parent_class" value
 * @method integer    getParent_id()      Returns the current record's "parent_id" value
 * @method string     getParent_name()    Returns the current record's "parent_name" value
 * @method string     getChild_Class()    Returns the current record's "child_class" value
 * @method string     getChildren_id()    Returns the current record's "children_id" value
 * @method date       getDate()           Returns the current record's "date" value
 * @method decimal    getAmount()         Returns the current record's "amount" value
 * @method string     getDetail1()        Returns the current record's "detail1" value
 * @method string     getDetail2()        Returns the current record's "detail2" value
 * @method string     getDetail3()        Returns the current record's "detail3" value
 * @method string     getNotes()          Returns the current record's "notes" value
 * @method date       getCheCkCleardate() Returns the current record's "checkcleardate" value
 * @method integer    getOut_check_id()   Returns the current record's "out_check_id" value
 * @method OutCheck   getOutCheck()       Returns the current record's "OutCheck" value
 * @method OutPayment setId()             Sets the current record's "id" value
 * @method OutPayment setType()           Sets the current record's "type" value
 * @method OutPayment setParent_class()   Sets the current record's "parent_class" value
 * @method OutPayment setParent_id()      Sets the current record's "parent_id" value
 * @method OutPayment setParent_name()    Sets the current record's "parent_name" value
 * @method OutPayment setChild_Class()    Sets the current record's "child_class" value
 * @method OutPayment setChildren_id()    Sets the current record's "children_id" value
 * @method OutPayment setDate()           Sets the current record's "date" value
 * @method OutPayment setAmount()         Sets the current record's "amount" value
 * @method OutPayment setDetail1()        Sets the current record's "detail1" value
 * @method OutPayment setDetail2()        Sets the current record's "detail2" value
 * @method OutPayment setDetail3()        Sets the current record's "detail3" value
 * @method OutPayment setNotes()          Sets the current record's "notes" value
 * @method OutPayment setCheCkCleardate() Sets the current record's "checkcleardate" value
 * @method OutPayment setOut_check_id()   Sets the current record's "out_check_id" value
 * @method OutPayment setOutCheck()       Sets the current record's "OutCheck" value
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseOutPayment extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('out_payment');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('type', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('parent_class', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('parent_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('parent_name', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('child_class', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('children_id', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('date', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('amount', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('detail1', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('detail2', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('detail3', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('notes', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('checkcleardate', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('out_check_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('OutCheck', array(
             'local' => 'out_check_id',
             'foreign' => 'id'));
    }
}