<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('VoucherAllocation', 'doctrine');

/**
 * BaseVoucherAllocation
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $name
 * @property integer $priority
 * @property Doctrine_Collection $Voucher
 * 
 * @method integer             getId()       Returns the current record's "id" value
 * @method string              getName()     Returns the current record's "name" value
 * @method integer             getPriority() Returns the current record's "priority" value
 * @method Doctrine_Collection getVoucher()  Returns the current record's "Voucher" collection
 * @method VoucherAllocation   setId()       Sets the current record's "id" value
 * @method VoucherAllocation   setName()     Sets the current record's "name" value
 * @method VoucherAllocation   setPriority() Sets the current record's "priority" value
 * @method VoucherAllocation   setVoucher()  Sets the current record's "Voucher" collection
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVoucherAllocation extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('voucher_allocation');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('priority', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Voucher', array(
             'local' => 'id',
             'foreign' => 'voucher_allocation_id'));
    }
}