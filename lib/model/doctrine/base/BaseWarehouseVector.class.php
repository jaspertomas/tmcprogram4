<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('WarehouseVector', 'doctrine');

/**
 * BaseWarehouseVector
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $name
 * @property integer $from_warehouse_id
 * @property integer $to_warehouse_id
 * @property Warehouse $Warehouse
 * @property Warehouse $Warehouse_2
 * @property Doctrine_Collection $TransferDr
 * 
 * @method integer             getId()                Returns the current record's "id" value
 * @method string              getName()              Returns the current record's "name" value
 * @method integer             getFrom_warehouse_id() Returns the current record's "from_warehouse_id" value
 * @method integer             getTo_warehouse_id()   Returns the current record's "to_warehouse_id" value
 * @method Warehouse           getWarehouse()         Returns the current record's "Warehouse" value
 * @method Warehouse           getWarehouse_2()       Returns the current record's "Warehouse_2" value
 * @method Doctrine_Collection getTransferDr()        Returns the current record's "TransferDr" collection
 * @method WarehouseVector     setId()                Sets the current record's "id" value
 * @method WarehouseVector     setName()              Sets the current record's "name" value
 * @method WarehouseVector     setFrom_warehouse_id() Sets the current record's "from_warehouse_id" value
 * @method WarehouseVector     setTo_warehouse_id()   Sets the current record's "to_warehouse_id" value
 * @method WarehouseVector     setWarehouse()         Sets the current record's "Warehouse" value
 * @method WarehouseVector     setWarehouse_2()       Sets the current record's "Warehouse_2" value
 * @method WarehouseVector     setTransferDr()        Sets the current record's "TransferDr" collection
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseWarehouseVector extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('warehouse_vector');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('name', 'string', 30, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 30,
             ));
        $this->hasColumn('from_warehouse_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('to_warehouse_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Warehouse', array(
             'local' => 'from_warehouse_id',
             'foreign' => 'id'));

        $this->hasOne('Warehouse as Warehouse_2', array(
             'local' => 'to_warehouse_id',
             'foreign' => 'id'));

        $this->hasMany('TransferDr', array(
             'local' => 'id',
             'foreign' => 'warehouse_vector_id'));
    }
}