<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('InvoiceTemplate', 'doctrine');

/**
 * BaseInvoiceTemplate
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $name
 * @property integer $is_invoice
 * @property integer $is_dr
 * @property string $prefix
 * @property Doctrine_Collection $Invoice
 * 
 * @method integer             getId()         Returns the current record's "id" value
 * @method string              getName()       Returns the current record's "name" value
 * @method integer             getIs_InvoIce() Returns the current record's "is_invoice" value
 * @method integer             getIs_dr()      Returns the current record's "is_dr" value
 * @method string              getPrefix()     Returns the current record's "prefix" value
 * @method Doctrine_Collection getInvoice()    Returns the current record's "Invoice" collection
 * @method InvoiceTemplate     setId()         Sets the current record's "id" value
 * @method InvoiceTemplate     setName()       Sets the current record's "name" value
 * @method InvoiceTemplate     setIs_InvoIce() Sets the current record's "is_invoice" value
 * @method InvoiceTemplate     setIs_dr()      Sets the current record's "is_dr" value
 * @method InvoiceTemplate     setPrefix()     Sets the current record's "prefix" value
 * @method InvoiceTemplate     setInvoice()    Sets the current record's "Invoice" collection
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseInvoiceTemplate extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('invoice_template');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('name', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('is_invoice', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('is_dr', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('prefix', 'string', 10, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Invoice', array(
             'local' => 'id',
             'foreign' => 'template_id'));
    }
}