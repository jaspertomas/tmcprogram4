<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('CommissionPayment', 'doctrine');

/**
 * BaseCommissionPayment
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $name
 * @property enum $type
 * @property integer $employee_id
 * @property date $startdate
 * @property date $enddate
 * @property date $date_created
 * @property date $date_paid
 * @property decimal $base1
 * @property decimal $rate1
 * @property decimal $base2
 * @property decimal $rate2
 * @property decimal $commission
 * @property decimal $balance
 * @property string $notes
 * @property Employee $Employee
 * @property Doctrine_Collection $Invoice
 * 
 * @method integer             getId()           Returns the current record's "id" value
 * @method string              getName()         Returns the current record's "name" value
 * @method enum                getType()         Returns the current record's "type" value
 * @method integer             getEmployEE_id()  Returns the current record's "employee_id" value
 * @method date                getStartdate()    Returns the current record's "startdate" value
 * @method date                getEnddatE()      Returns the current record's "enddate" value
 * @method date                getDate_createD() Returns the current record's "date_created" value
 * @method date                getDate_paiD()    Returns the current record's "date_paid" value
 * @method decimal             getBase1()        Returns the current record's "base1" value
 * @method decimal             getRate1()        Returns the current record's "rate1" value
 * @method decimal             getBase2()        Returns the current record's "base2" value
 * @method decimal             getRate2()        Returns the current record's "rate2" value
 * @method decimal             getCommission()   Returns the current record's "commission" value
 * @method decimal             getBalance()      Returns the current record's "balance" value
 * @method string              getNotes()        Returns the current record's "notes" value
 * @method Employee            getEmployee()     Returns the current record's "Employee" value
 * @method Doctrine_Collection getInvoice()      Returns the current record's "Invoice" collection
 * @method CommissionPayment   setId()           Sets the current record's "id" value
 * @method CommissionPayment   setName()         Sets the current record's "name" value
 * @method CommissionPayment   setType()         Sets the current record's "type" value
 * @method CommissionPayment   setEmployEE_id()  Sets the current record's "employee_id" value
 * @method CommissionPayment   setStartdate()    Sets the current record's "startdate" value
 * @method CommissionPayment   setEnddatE()      Sets the current record's "enddate" value
 * @method CommissionPayment   setDate_createD() Sets the current record's "date_created" value
 * @method CommissionPayment   setDate_paiD()    Sets the current record's "date_paid" value
 * @method CommissionPayment   setBase1()        Sets the current record's "base1" value
 * @method CommissionPayment   setRate1()        Sets the current record's "rate1" value
 * @method CommissionPayment   setBase2()        Sets the current record's "base2" value
 * @method CommissionPayment   setRate2()        Sets the current record's "rate2" value
 * @method CommissionPayment   setCommission()   Sets the current record's "commission" value
 * @method CommissionPayment   setBalance()      Sets the current record's "balance" value
 * @method CommissionPayment   setNotes()        Sets the current record's "notes" value
 * @method CommissionPayment   setEmployee()     Sets the current record's "Employee" value
 * @method CommissionPayment   setInvoice()      Sets the current record's "Invoice" collection
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCommissionPayment extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('commission_payment');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('name', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('type', 'enum', 6, array(
             'type' => 'enum',
             'fixed' => 0,
             'unsigned' => false,
             'values' => 
             array(
              0 => 'Profit',
              1 => 'Gross',
             ),
             'primary' => false,
             'default' => 'Profit',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 6,
             ));
        $this->hasColumn('employee_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('startdate', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('enddate', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('date_created', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('date_paid', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('base1', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('rate1', 'decimal', 6, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 6,
             'scale' => '5',
             ));
        $this->hasColumn('base2', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('rate2', 'decimal', 6, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 6,
             'scale' => '5',
             ));
        $this->hasColumn('commission', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0.00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('balance', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0.00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('notes', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Employee', array(
             'local' => 'employee_id',
             'foreign' => 'id'));

        $this->hasMany('Invoice', array(
             'local' => 'id',
             'foreign' => 'commission_payment_id'));
    }
}