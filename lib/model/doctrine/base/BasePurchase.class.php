<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Purchase', 'doctrine');

/**
 * BasePurchase
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $pono
 * @property string $invno
 * @property string $invnos
 * @property string $invoice_ids
 * @property decimal $total
 * @property string $memo
 * @property decimal $tax
 * @property integer $vendor_id
 * @property string $vendor_name
 * @property integer $employee_id
 * @property integer $template_id
 * @property date $date
 * @property date $datereceived
 * @property date $duedate
 * @property string $vendor_invoice
 * @property string $discrate
 * @property decimal $discamt
 * @property enum $status
 * @property enum $type
 * @property decimal $cash
 * @property decimal $cheque
 * @property decimal $credit
 * @property string $chequeno
 * @property date $chequedate
 * @property decimal $balance
 * @property string $chequedata
 * @property integer $is_inspected
 * @property integer $is_temporary
 * @property enum $collection_status
 * @property integer $is_archived
 * @property integer $is_dr_based
 * @property integer $terms
 * @property enum $delivery_status
 * @property integer $invoice_id
 * @property integer $is_stock
 * @property enum $received_status
 * @property integer $was_closed
 * @property Employee $Employee
 * @property PurchaseTemplate $PurchaseTemplate
 * @property Vendor $Vendor
 * @property Doctrine_Collection $CounterReceiptDetail
 * @property Doctrine_Collection $Profitdetail
 * @property Doctrine_Collection $PurchaseDr
 * @property Doctrine_Collection $PurchaseDrDetail
 * @property Doctrine_Collection $Purchaseconversion
 * @property Doctrine_Collection $Purchasedetail
 * 
 * @method integer             getId()                   Returns the current record's "id" value
 * @method string              getPono()                 Returns the current record's "pono" value
 * @method string              getInvno()                Returns the current record's "invno" value
 * @method string              getInvnos()               Returns the current record's "invnos" value
 * @method string              getInvoIce_Ids()          Returns the current record's "invoice_ids" value
 * @method decimal             getToTal()                Returns the current record's "total" value
 * @method string              getMeMo()                 Returns the current record's "memo" value
 * @method decimal             getTax()                  Returns the current record's "tax" value
 * @method integer             getVendor_id()            Returns the current record's "vendor_id" value
 * @method string              getVendor_name()          Returns the current record's "vendor_name" value
 * @method integer             getEmployEE_id()          Returns the current record's "employee_id" value
 * @method integer             getTemplaTe_id()          Returns the current record's "template_id" value
 * @method date                getDate()                 Returns the current record's "date" value
 * @method date                getDatereceiveD()         Returns the current record's "datereceived" value
 * @method date                getDueDate()              Returns the current record's "duedate" value
 * @method string              getVendor_inVoice()       Returns the current record's "vendor_invoice" value
 * @method string              getDiscrate()             Returns the current record's "discrate" value
 * @method decimal             getDiscamt()              Returns the current record's "discamt" value
 * @method enum                getStatuS()               Returns the current record's "status" value
 * @method enum                getType()                 Returns the current record's "type" value
 * @method decimal             getCash()                 Returns the current record's "cash" value
 * @method decimal             getCheque()               Returns the current record's "cheque" value
 * @method decimal             getCredit()               Returns the current record's "credit" value
 * @method string              getChequeno()             Returns the current record's "chequeno" value
 * @method date                getChequedate()           Returns the current record's "chequedate" value
 * @method decimal             getBalance()              Returns the current record's "balance" value
 * @method string              getChequedata()           Returns the current record's "chequedata" value
 * @method integer             getIs_Inspected()         Returns the current record's "is_inspected" value
 * @method integer             getIs_temporary()         Returns the current record's "is_temporary" value
 * @method enum                getColleCtion_status()    Returns the current record's "collection_status" value
 * @method integer             getIs_archIved()          Returns the current record's "is_archived" value
 * @method integer             getIs_dr_based()          Returns the current record's "is_dr_based" value
 * @method integer             getTerms()                Returns the current record's "terms" value
 * @method enum                getDelivery_status()      Returns the current record's "delivery_status" value
 * @method integer             getInvoIce_Id()           Returns the current record's "invoice_id" value
 * @method integer             getIs_stock()             Returns the current record's "is_stock" value
 * @method enum                getReceived_status()      Returns the current record's "received_status" value
 * @method integer             getWas_closed()           Returns the current record's "was_closed" value
 * @method Employee            getEmployee()             Returns the current record's "Employee" value
 * @method PurchaseTemplate    getPurchaseTemplate()     Returns the current record's "PurchaseTemplate" value
 * @method Vendor              getVendor()               Returns the current record's "Vendor" value
 * @method Doctrine_Collection getCounterReceiptDetail() Returns the current record's "CounterReceiptDetail" collection
 * @method Doctrine_Collection getProfitdetail()         Returns the current record's "Profitdetail" collection
 * @method Doctrine_Collection getPurchaseDr()           Returns the current record's "PurchaseDr" collection
 * @method Doctrine_Collection getPurchaseDrDetail()     Returns the current record's "PurchaseDrDetail" collection
 * @method Doctrine_Collection getPurchaseconversion()   Returns the current record's "Purchaseconversion" collection
 * @method Doctrine_Collection getPurchasedetail()       Returns the current record's "Purchasedetail" collection
 * @method Purchase            setId()                   Sets the current record's "id" value
 * @method Purchase            setPono()                 Sets the current record's "pono" value
 * @method Purchase            setInvno()                Sets the current record's "invno" value
 * @method Purchase            setInvnos()               Sets the current record's "invnos" value
 * @method Purchase            setInvoIce_Ids()          Sets the current record's "invoice_ids" value
 * @method Purchase            setToTal()                Sets the current record's "total" value
 * @method Purchase            setMeMo()                 Sets the current record's "memo" value
 * @method Purchase            setTax()                  Sets the current record's "tax" value
 * @method Purchase            setVendor_id()            Sets the current record's "vendor_id" value
 * @method Purchase            setVendor_name()          Sets the current record's "vendor_name" value
 * @method Purchase            setEmployEE_id()          Sets the current record's "employee_id" value
 * @method Purchase            setTemplaTe_id()          Sets the current record's "template_id" value
 * @method Purchase            setDate()                 Sets the current record's "date" value
 * @method Purchase            setDatereceiveD()         Sets the current record's "datereceived" value
 * @method Purchase            setDueDate()              Sets the current record's "duedate" value
 * @method Purchase            setVendor_inVoice()       Sets the current record's "vendor_invoice" value
 * @method Purchase            setDiscrate()             Sets the current record's "discrate" value
 * @method Purchase            setDiscamt()              Sets the current record's "discamt" value
 * @method Purchase            setStatuS()               Sets the current record's "status" value
 * @method Purchase            setType()                 Sets the current record's "type" value
 * @method Purchase            setCash()                 Sets the current record's "cash" value
 * @method Purchase            setCheque()               Sets the current record's "cheque" value
 * @method Purchase            setCredit()               Sets the current record's "credit" value
 * @method Purchase            setChequeno()             Sets the current record's "chequeno" value
 * @method Purchase            setChequedate()           Sets the current record's "chequedate" value
 * @method Purchase            setBalance()              Sets the current record's "balance" value
 * @method Purchase            setChequedata()           Sets the current record's "chequedata" value
 * @method Purchase            setIs_Inspected()         Sets the current record's "is_inspected" value
 * @method Purchase            setIs_temporary()         Sets the current record's "is_temporary" value
 * @method Purchase            setColleCtion_status()    Sets the current record's "collection_status" value
 * @method Purchase            setIs_archIved()          Sets the current record's "is_archived" value
 * @method Purchase            setIs_dr_based()          Sets the current record's "is_dr_based" value
 * @method Purchase            setTerms()                Sets the current record's "terms" value
 * @method Purchase            setDelivery_status()      Sets the current record's "delivery_status" value
 * @method Purchase            setInvoIce_Id()           Sets the current record's "invoice_id" value
 * @method Purchase            setIs_stock()             Sets the current record's "is_stock" value
 * @method Purchase            setReceived_status()      Sets the current record's "received_status" value
 * @method Purchase            setWas_closed()           Sets the current record's "was_closed" value
 * @method Purchase            setEmployee()             Sets the current record's "Employee" value
 * @method Purchase            setPurchaseTemplate()     Sets the current record's "PurchaseTemplate" value
 * @method Purchase            setVendor()               Sets the current record's "Vendor" value
 * @method Purchase            setCounterReceiptDetail() Sets the current record's "CounterReceiptDetail" collection
 * @method Purchase            setProfitdetail()         Sets the current record's "Profitdetail" collection
 * @method Purchase            setPurchaseDr()           Sets the current record's "PurchaseDr" collection
 * @method Purchase            setPurchaseDrDetail()     Sets the current record's "PurchaseDrDetail" collection
 * @method Purchase            setPurchaseconversion()   Sets the current record's "Purchaseconversion" collection
 * @method Purchase            setPurchasedetail()       Sets the current record's "Purchasedetail" collection
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePurchase extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('purchase');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('pono', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('invno', 'string', 30, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 30,
             ));
        $this->hasColumn('invnos', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('invoice_ids', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('total', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('memo', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('tax', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('vendor_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('vendor_name', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('employee_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('template_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('date', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('datereceived', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('duedate', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('vendor_invoice', 'string', 25, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('discrate', 'string', 30, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 30,
             ));
        $this->hasColumn('discamt', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0.00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('status', 'enum', 9, array(
             'type' => 'enum',
             'fixed' => 0,
             'unsigned' => false,
             'values' => 
             array(
              0 => 'Pending',
              1 => 'Paid',
              2 => 'Cancelled',
              3 => 'Overpaid',
             ),
             'primary' => false,
             'default' => 'Pending',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 9,
             ));
        $this->hasColumn('type', 'enum', 13, array(
             'type' => 'enum',
             'fixed' => 0,
             'unsigned' => false,
             'values' => 
             array(
              0 => 'Cash',
              1 => 'Cheque',
              2 => 'Account',
              3 => 'Partial',
              4 => 'Bank Transfer',
             ),
             'primary' => false,
             'default' => 'Account',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 13,
             ));
        $this->hasColumn('cash', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0.00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('cheque', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0.00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('credit', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0.00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('chequeno', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('chequedate', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('balance', 'decimal', 10, array(
             'type' => 'decimal',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             'scale' => '2',
             ));
        $this->hasColumn('chequedata', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('is_inspected', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('is_temporary', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('collection_status', 'enum', 16, array(
             'type' => 'enum',
             'fixed' => 0,
             'unsigned' => false,
             'values' => 
             array(
              0 => 'Due',
              1 => 'Counter Received',
              2 => 'Cheque Ready',
             ),
             'primary' => false,
             'default' => 'Due',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 16,
             ));
        $this->hasColumn('is_archived', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('is_dr_based', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '1',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('terms', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('delivery_status', 'enum', 10, array(
             'type' => 'enum',
             'fixed' => 0,
             'unsigned' => false,
             'values' => 
             array(
              0 => 'Pending',
              1 => 'Incomplete',
              2 => 'Received',
             ),
             'primary' => false,
             'default' => 'Pending',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 10,
             ));
        $this->hasColumn('invoice_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('is_stock', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('received_status', 'enum', 18, array(
             'type' => 'enum',
             'fixed' => 0,
             'unsigned' => false,
             'values' => 
             array(
              0 => 'Not Received',
              1 => 'Partially Received',
              2 => 'Fully Received',
             ),
             'primary' => false,
             'default' => 'Not Received',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 18,
             ));
        $this->hasColumn('was_closed', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 1,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Employee', array(
             'local' => 'employee_id',
             'foreign' => 'id'));

        $this->hasOne('PurchaseTemplate', array(
             'local' => 'template_id',
             'foreign' => 'id'));

        $this->hasOne('Vendor', array(
             'local' => 'vendor_id',
             'foreign' => 'id'));

        $this->hasMany('CounterReceiptDetail', array(
             'local' => 'id',
             'foreign' => 'purchase_id'));

        $this->hasMany('Profitdetail', array(
             'local' => 'id',
             'foreign' => 'purchase_id'));

        $this->hasMany('PurchaseDr', array(
             'local' => 'id',
             'foreign' => 'purchase_id'));

        $this->hasMany('PurchaseDrDetail', array(
             'local' => 'id',
             'foreign' => 'purchase_id'));

        $this->hasMany('Purchaseconversion', array(
             'local' => 'id',
             'foreign' => 'purchase_id'));

        $this->hasMany('Purchasedetail', array(
             'local' => 'id',
             'foreign' => 'purchase_id'));
    }
}