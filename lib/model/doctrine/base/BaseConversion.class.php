<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Conversion', 'doctrine');

/**
 * BaseConversion
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property Doctrine_Collection $Conversiondetail
 * @property Doctrine_Collection $Deliveryconversion
 * @property Doctrine_Collection $JobOrderConversion
 * @property Doctrine_Collection $Product
 * @property Doctrine_Collection $Purchaseconversion
 * 
 * @method integer             getId()                 Returns the current record's "id" value
 * @method string              getName()               Returns the current record's "name" value
 * @method string              getDescription()        Returns the current record's "description" value
 * @method Doctrine_Collection getConversiondetail()   Returns the current record's "Conversiondetail" collection
 * @method Doctrine_Collection getDeliveryconversion() Returns the current record's "Deliveryconversion" collection
 * @method Doctrine_Collection getJobOrderConversion() Returns the current record's "JobOrderConversion" collection
 * @method Doctrine_Collection getProduct()            Returns the current record's "Product" collection
 * @method Doctrine_Collection getPurchaseconversion() Returns the current record's "Purchaseconversion" collection
 * @method Conversion          setId()                 Sets the current record's "id" value
 * @method Conversion          setName()               Sets the current record's "name" value
 * @method Conversion          setDescription()        Sets the current record's "description" value
 * @method Conversion          setConversiondetail()   Sets the current record's "Conversiondetail" collection
 * @method Conversion          setDeliveryconversion() Sets the current record's "Deliveryconversion" collection
 * @method Conversion          setJobOrderConversion() Sets the current record's "JobOrderConversion" collection
 * @method Conversion          setProduct()            Sets the current record's "Product" collection
 * @method Conversion          setPurchaseconversion() Sets the current record's "Purchaseconversion" collection
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseConversion extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('conversion');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('name', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('description', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Conversiondetail', array(
             'local' => 'id',
             'foreign' => 'conversion_id'));

        $this->hasMany('Deliveryconversion', array(
             'local' => 'id',
             'foreign' => 'conversion_id'));

        $this->hasMany('JobOrderConversion', array(
             'local' => 'id',
             'foreign' => 'conversion_id'));

        $this->hasMany('Product', array(
             'local' => 'id',
             'foreign' => 'conversion_id'));

        $this->hasMany('Purchaseconversion', array(
             'local' => 'id',
             'foreign' => 'conversion_id'));
    }
}