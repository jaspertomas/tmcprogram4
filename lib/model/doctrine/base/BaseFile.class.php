<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('File', 'doctrine');

/**
 * BaseFile
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $parent_class
 * @property integer $parent_id
 * @property string $title
 * @property string $description
 * @property string $filename
 * @property string $file
 * @property string $keywords
 * @property integer $filesize
 * @property string $filetype
 * @property string $uploadlocation
 * 
 * @method integer getId()             Returns the current record's "id" value
 * @method string  getParent_class()   Returns the current record's "parent_class" value
 * @method integer getParent_id()      Returns the current record's "parent_id" value
 * @method string  getTiTle()          Returns the current record's "title" value
 * @method string  getDescription()    Returns the current record's "description" value
 * @method string  getFilename()       Returns the current record's "filename" value
 * @method string  getFile()           Returns the current record's "file" value
 * @method string  getKeywords()       Returns the current record's "keywords" value
 * @method integer getFilesize()       Returns the current record's "filesize" value
 * @method string  getFiletype()       Returns the current record's "filetype" value
 * @method string  getUploadlocation() Returns the current record's "uploadlocation" value
 * @method File    setId()             Sets the current record's "id" value
 * @method File    setParent_class()   Sets the current record's "parent_class" value
 * @method File    setParent_id()      Sets the current record's "parent_id" value
 * @method File    setTiTle()          Sets the current record's "title" value
 * @method File    setDescription()    Sets the current record's "description" value
 * @method File    setFilename()       Sets the current record's "filename" value
 * @method File    setFile()           Sets the current record's "file" value
 * @method File    setKeywords()       Sets the current record's "keywords" value
 * @method File    setFilesize()       Sets the current record's "filesize" value
 * @method File    setFiletype()       Sets the current record's "filetype" value
 * @method File    setUploadlocation() Sets the current record's "uploadlocation" value
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseFile extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('file');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('parent_class', 'string', 20, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 20,
             ));
        $this->hasColumn('parent_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('title', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('description', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('filename', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('file', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('keywords', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('filesize', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('filetype', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('uploadlocation', 'string', 10, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}