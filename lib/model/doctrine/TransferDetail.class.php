<?php

/**
 * TransferDetail
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    sf_sandbox
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class TransferDetail extends BaseTransferDetail
{
    public $stock=null;
    public $stockentry=null;
    public function calcDr()
    {
      $transferred=0;
      foreach($this->getTransferDrDetail() as $drdetail)
        if($drdetail->getIsTransferred())
          $transferred+=$drdetail->getQty();
      $this->setQtyTransferred($transferred);
      $this->save();
    }
    //calculate qty_transferred
    public function calc()
    {
      $this->calcDr();
    }
    public function updateStockentry()
    {
      //get stock with $this->product_id
      $stock=$this->getStock();
    
      //fetch stockentry
      $stockentry=$this->getStockentry();
      //if stockentry does not exist
      if(!$stockentry)
      {
        //create it
        $qty=$this->getQtyTransferred();
        $ref_class="TransferDetail";
        $ref_id=$this->getId();
        $date=$this->getTransfer()->getDate();
        $type="Transfer";
        $description="";
        $stockentry=$stock->addEntry($date, $qty, $ref_class, $ref_id, $type, $description);
        return;
      }
      else
      {
        //there is possibility that $this->product_id has been changed
        //and therefore, stockentry has wrong product id
        //stock has correct product id, use it for comparison
        
            //if product id is different
            if($stockentry->getStockId()!=$stock->getId())
            {
              //delete, automatically adjust stock record
          $stockentry->delete();
  
          //create new entry properly in correct stock
          $ref_class="TransferDetail";
          $ref_id=$this->getId();
          $date=$this->getTransfer()->getDate();
          $qty=$this->getQtyTransferred();
          $type="Transfer";
          $description="";
          $this->getStock()->addEntry($date, $qty, $ref_class, $ref_id, $type, $description);
            }
            //if only qty is different
            else if($stockentry->getQty()!=$this->getQtyTransferred())
            {
              $stockentry->setQty($this->getQtyTransferred());
              $stockentry->setBalance(null);
              $stockentry->save();
              $stock->calcFromStockEntry($stockentry);
            }
      }
    }
    public function deleteStockentry()
    {
      $stockentry=$this->getStockentry();
      if($stockentry)$stockentry->delete();
    }
    public function cascadeDelete()
    {
      $this->deleteStockentry();
      return $this->delete();
    }
    function isCancelled(){return $this->getIsCancelled()==1;}
    function cascadeCancel()
    {
      $this->deleteStockentry();
      $this->setIsCancelled(1);
      $this->save();
    }
    public function getStock()
    {
      //this automatically creates a stock record if it doesn't exist
      return StockTable::fetch(SettingsTable::fetch("default_warehouse_id"), $this->getProductId());
    }
    public function getStockEntry()
    {
      //fetch from database
      return Doctrine_Query::create()
        ->from('Stockentry se')
        ->where('ref_class="TransferDetail"')
        ->andWhere('ref_id='.$this->getId())
        ->fetchOne();
    }
    public function getStockEntryDoNotCreate()
    {
      //fetch from database
      return getStockEntry();
    }
    public function getRef()
    {
      return $this->getTransfer();
    }
    public function getColorIndex()
    {
      switch($this->getColor())
      {
        case 'red':return 1;
        case 'orange':return 2;
        case 'yellow':return 3;
        case 'green':return 4;
        case 'blue':return 5;
        case 'indigo':return 6;
        case 'violet':return 7;
      }
    }
    public function getQtyUnproduced()
    {
      return $this->getQty()-$this->getQtyTransferred();
    }
}