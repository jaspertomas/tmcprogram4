<?php

class MyPurchaseHelper
{
  //printing from windows 8?
	public static function PrintablePurchaseBig($purchase)
	{
	  $QTY=0;
	  $ITEMS=2;
	  $UNITPRICE=4;
	  $TOTAL=6;
    $widths=array(5,1,40,1,12,1,12);
    $signlinewidth=12;
    $signlinespacingwidth=5;
    $signlineleftpad=2;

    $pagewidth=74;
  
    $vendor=$purchase->getVendor();
    $employee=$purchase->getEmployee();
    $details=$purchase->getPurchasedetail();
    $address_segments=MyString::getSegments($vendor->getAddr1()." ".$vendor->getAddr2()." ".$vendor->getAddr3(),73);
    $address1=$address_segments[0];
    if(isset($address_segments[1]))$address2=$address_segments[1];
    
    $date=MyDateTime::frommysql($purchase->getDate())->toprettydate();
    $terms=$vendor->getTerms();
    $terms=$terms>0 ? $terms." Days" : "Cash";

    //keep track of item lines so we can pad downward 15 lines to vat line
    $itemlinecount=0;
    //$totalsale=0; 
    //$vatentryexists=false; 
    
    
//$client=$purchase->getVendor();
//$templatename=$purchase->getPurchaseTemplate();

$invoice=$purchase->getInvoice();
if($invoice)
{
  $sold_to=$purchase->getInvno();
  $customer=$purchase->getInvoice()->getCustomer();  
  $customername=$customer->getName();
  $customerphone=$customer->getPhone1();
  $invoicedate=MyDateTime::frommysql($invoice->getDate())->toshortdate();
}
else
{
  if($purchase->getIsStock())
  {
    $sold_to="(Stock)";
    $customername='';
  }
  else
  {
    $sold_to="";
    $customername=$purchase->getInvno();
  }
  $customer="";  
  $customerphone="";
  $invoicedate="";
}

    $company_name=sfConfig::get('custom_company_name');
    $company_address=sfConfig::get('custom_company_address');

    $content="";
    $content.=str_pad("", (81-strlen($company_name))/2, " ", STR_PAD_LEFT).$company_name."\r\n";
    $content.=str_pad("", (81-strlen($company_address))/2, " ", STR_PAD_LEFT).$company_address."\r\n";
    $content.="                        ".sfConfig::get('custom_company_phone')."\r\n";
    //$content.="                             ".sfConfig::get('custom_company_email')."\r\n";
    //$content.="                                ".sfConfig::get('custom_company_tin')."\r\n";
    
    $content.="\r\n";
    $content.="                         PURCHASE ORDER No. ".$purchase->getPono()."\r\n";
    $content.="\r\n";

    $content.=str_pad("Date:  ".$date,  48, " ");//." Terms: ".$terms."\r\n";
    $content.="\r\n";
    $content.="Supplier:".$vendor->getName()."\r\n";
    $content.="Address: ".$address1."\r\n";
    if(isset($address2))
    {
      $content.="         ".$address2."\r\n";
      $itemlinecount++;
    }
    $content.=str_pad("Supplier Invoice: ".$purchase->getVendorInvoice(),  48, " ");
    $content.=" Sold To Invoice: ".$sold_to."\r\n";
    $content.=str_pad("Salesman: ".$purchase->getEmployee(),  48, " ");
    $content.=" Sold To: ".$customername."\r\n";
    $content.="\r\n";

    $content.=
      ""
      .str_pad("QTY", $widths[$QTY], " ",STR_PAD_LEFT)
      ." "
      .str_pad("ITEMS", $widths[$ITEMS], " ",STR_PAD_RIGHT)
      ." "
      .str_pad("UNIT PRICE", $widths[$UNITPRICE], " ",STR_PAD_LEFT)
      ." "
      .str_pad("TOTAL", $widths[$TOTAL], " ",STR_PAD_LEFT)
      ."\r\n";
    
    foreach($details as $detail){
      //hide vat entries
      //if($detail->getIsVat())continue;
      
      $product=$detail->getProduct();
      if(trim($detail->getDiscrate())!="")
      {
        $product.=" - P".MyDecimal::format($detail->getPrice())
          ." less "
          .$detail->getDiscrate()
          ;
      }      
      
      //split product name into segments
      //first segment is 25 chars long
      //each succeeding segment is 70 chars long
      $segments=MyString::getSegments($product,$widths[$ITEMS],2);
      if(isset($segments[1]))$segments2=MyString::getSegments($segments[1],$pagewidth-$widths[0]-$widths[1]-5);
      else $segments2=array();
      //product line 1
      $content.=
        ""
        .str_pad(str_replace(".00","",$detail->getQty()),  5, " ",STR_PAD_LEFT)
        ." "
        .str_pad($segments[0],  $widths[$ITEMS], " ",STR_PAD_RIGHT)
        ." "
        .str_pad(MyDecimal::format($detail->getTotal()/$detail->getQty()),  $widths[$UNITPRICE], " ",STR_PAD_LEFT)
        ." "
        .str_pad(MyDecimal::format($detail->getTotal()),  $widths[$TOTAL], " ",STR_PAD_LEFT)
        ."\r\n";
      $itemlinecount++;

      //product lines 2, 3, 4...
      foreach($segments2 as $segment)
      {
          $content.=
            ""
            ."     "
            ." "
            .$segment
            ."\r\n";
          $itemlinecount++;
      }
    }
    //pad down 17 lines to vat line
    for($i=$itemlinecount;$i<(12+9);$i++)
      $content.="\r\n";
      
    $content.=""
.str_pad("", $signlineleftpad, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
."TOTAL: "
      .str_pad(number_format($purchase->getTotal(),2,".",","), 12, " ", STR_PAD_LEFT)
      ."\r\n"
.str_pad("", $signlineleftpad, " ", STR_PAD_LEFT)
.str_pad("Prepared By", $signlinewidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("Approved By", $signlinewidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("Received By", $signlinewidth, " ", STR_PAD_LEFT)
      ."\r\n"
      ;
    
    return $content;
	}
	/*
  //printing from windows 7
	public static function PrintablePurchaseSmall($purchase)
	{
    $vendor=$purchase->getVendor();
    $employee=$purchase->getEmployee();
    $details=$purchase->getPurchasedetail();
    
    $date=MyDateTime::frommysql($purchase->getDate())->toprettydate();
    $terms=$vendor->getTerms();
    $terms=$terms>0 ? $terms." Days" : "Cash";
    $address_segments=$vendor->getAddressSegments();
    $address1=$address_segments[0];
    $address2=$address_segments[1];

    //keep track of item lines so we can pad downward 15 lines to vat line
    $itemlinecount=0;
    $totalsale=0; 
    //$vatentryexists=false; 

    $content="\r\n\r\n\r\n\r\n";
    $content.="         ".str_pad($vendor->getName(),  40, " ")."      ".$date."\r\n";
    $content.="\r\n";
    $content.="         ".str_pad($vendor->getTinNo(),  40, " ")."      ".$terms."\r\n";
    $content.="         ".$address1."\r\n";
    $content.="         ".$address2."\r\n";
    $content.="\r\n\r\n\r\n\r\n";
    foreach($details as $detail){
      //hide vat entries
      if($detail->getIsVat())continue;
      
      $segments=$detail->getProduct()->getNameSegments(25);
      $first=true;
      foreach($segments as $segment)
      {
        if($first)
        {
          $content.=
            "   "
            .str_pad($detail->getQty(),  5, " ",STR_PAD_LEFT)
            ."       "
            .str_pad($segment,  45, " ",STR_PAD_RIGHT)
            ." "
            .str_pad($detail->getPrice(),  13, " ",STR_PAD_LEFT)
            ." "
            .str_pad($detail->getTotal(),  13, " ",STR_PAD_LEFT)
            ."\r\n";
          $first=false;
          $itemlinecount++;
        }
        else
        {
          $content.=
            "   "
            ."     "
            ."       "
            .$segment
            ."\r\n";
          $itemlinecount++;
        }
      }
    }
    //pad down 22 lines to vat line
    for($i=$itemlinecount;$i<(15+7);$i++)
      $content.="\r\n";
      
    $content.=
      "   "
      ."     "
      ."       "
      ."                                             "
      ." "
      ."             "
      ." "
      .str_pad(number_format($purchase->getTotal(),2,".",","), 13, " ", STR_PAD_LEFT)
      ."\r\n";
    
    return $content;
	}
	*/
}

