<?php

class MyTime
{
  public static function compare($time1,$time2)
  {
    $segments=explode(":",$time1);
    $time1hour=$segments[0];
    $time1minute=$segments[1];
    $segments=explode(":",$time2);
    $time2hour=$segments[0];
    $time2minute=$segments[1];
    if($time1hour>$time2hour)return 1;
    else if($time1hour<$time2hour)return -1;
    else if($time1minute>$time2minute)return 1;
    else if($time1minute<$time2minute)return -1;
    else return 0;
  }
  public static function isEarlierThan($time1,$time2)
  {
    $result=MyTime::compare($time1,$time2);
    return $result==-1;
  }
  public static function isEarlierThanOrEqualTo($time1,$time2)
  {
    $result=MyTime::compare($time1,$time2);
    return $result!=1;
  }
  public static function isLaterThan($time1,$time2)
  {
    $result=MyTime::compare($time1,$time2);
    return $result==1;
  }
  public static function isLaterThanOrEqualTo($time1,$time2)
  {
    $result=MyTime::compare($time1,$time2);
    return $result!=-1;
  }
  public static function isEqualTo($time1,$time2)
  {
    $result=MyTime::compare($time1,$time2);
    return $result==0;
  }
}

