<?php
class MyDate
{
  public static function today() { 
    $today = getdate(); 
    return $today['year']."-".str_pad($today['mon'], 2, "0", STR_PAD_LEFT)."-".str_pad($today['mday'], 2, "0", STR_PAD_LEFT);
  }
  public static function shortNameForMonth($i){
    $array=self::shortMonths();
    return $array[$i];
  }
  public static function shortMonths(){
    return array(
      "",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    );
  }
}
