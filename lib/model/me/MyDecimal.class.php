<?php
class MyDecimal
{
  public static function format($amount)
  {
    if($amount==null)return "0.00";
    $amount=str_replace(",","",$amount);
    return number_format($amount,2,".",",");
  }

  /*
  var_dump(MyDecimal::toPesoString(1));
  > "one pesos only" 
  var_dump(MyDecimal::toPesoString(1.00));
  > "one pesos only" 
  var_dump(MyDecimal::toPesoString(1.12));
  > "one pesos and twelve centavos" 
  var_dump(MyDecimal::toPesoString(1.20));
  > "one pesos and twenty centavos"
   */
  public static function toPesoString($amount)
  {
    $segments=explode(".",strval($amount));
    $p=$segments[0];
    $c=$segments[1];
    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
    $c=str_pad($c, 2, "0");
    if($c=="00")
    $output = $f->format($p)." pesos only";
    else
    $output = $f->format($p)." pesos + ".$c."/100";
    $output = str_replace(" and "," ",$output);
    $output = str_replace(",","",$output);
    return $output;
  }
}
