<?php
class MyDateRangeHelper
{
  //usage:
  // list($this->startdateform, $this->enddateform) = MyDateRangeHelper::getForms($request);
  public static function getForms($request) { 
	  $requestparams=$request->getParameter("invoice");
  
	  $day=$requestparams["date"]["day"];
	  $month=$requestparams["date"]["month"];
	  $year=$requestparams["date"]["year"];
  
	  $invoice=new Invoice();
	  if($request->hasParameter("startdate"))
		$invoice->setDate($request->getParameter("startdate"));
	  elseif(!$day or !$month or !$year)
		$invoice->setDate(MyDate::today());
	  else
		$invoice->setDate($year."-".$month."-".$day);
  
	  $requestparams=$request->getParameter("purchase");
	  $day=$requestparams["date"]["day"];
	  $month=$requestparams["date"]["month"];
	  $year=$requestparams["date"]["year"];
	  $purchase=new Purchase();
	  if($request->hasParameter("enddate"))
		$purchase->setDate($request->getParameter("enddate"));
	  elseif(!$day or !$month or !$year)
		$purchase->setDate(MyDate::today());
	  else
		$purchase->setDate($year."-".$month."-".$day);
  
    return array(new InvoiceForm($invoice), new PurchaseForm($purchase));
 
  }
}
