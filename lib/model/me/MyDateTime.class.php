<?php
class MyDateTime
{
  /*
  Usage:
    echo MyDateTime::fromdatetime("2003-10-10 20:12:13");
    echo MyDateTime::frommysql("2003-10-10");
    echo MyDateTime::today();
    echo new MyDateTime(2001,2,3,4,5,6);

  */
  private $timestamp=0;
  function __construct($year,$month,$day,$hour,$minute,$second) {$this->timestamp=mktime($hour, $minute, $second, $month, $day, $year);}
  function copy(){return MyDateTime::fromtimestamp($this->timestamp);}
  function isvalid() { if($this->timestamp==0)return false;else return true; } 
  static function emptydate() { $date = new MyDateTime(0,0,0,0,0,0);$date->timestamp=0; return $date;} 
  static function today() 
  {
    $date = new DateTime("now", new DateTimeZone('Asia/Manila') );
    $today = array(
      (int)$date->format('Y'),
      (int)$date->format('m'),
      (int)$date->format('d'),
      (int)$date->format('H'),
      (int)$date->format('i'),
      (int)$date->format('s')
      );
    return new MyDateTime($today[0],$today[1],$today[2],$today[3],$today[4],$today[5]); 
  } 
  static function now() { return MyDateTime::today(); }

  static function toshort($string) { return MyDateTime::frommysql($string)->toshortdate(); } 

  static function frommysql($string) 
  { 
    if(MyTools::slugify($string)=="" or MyTools::slugify($string)=="n-a") 
      return self::emptydate(); 
    
    $string=str_replace("/","-",$string);
    $array=explode("-",$string); 
    //detect year, put it in front
    if(strlen($array[2])==4)
      return new MyDateTime($array[2],$array[0],$array[1],0,0,0); 
    else
      return new MyDateTime($array[0],$array[1],$array[2],0,0,0); 
  } 
  static function fromymd($year,$month,$day) { return new MyDateTime($year,$month,$day,0,0,0); } 
  static function frommdy($month,$day,$year) { return new MyDateTime($year,$month,$day,0,0,0); } 
  static function fromdatetime($string) 
  { 
    if(MyTools::slugify($string)=="" or MyTools::slugify($string)=="n-a") 
      return self::emptydate(); 
    
    $array=explode(" ",$string); 
    $datearray=explode("-",$array[0]);
    
    //if there is an $array[1]], continue
    if(count($array)>1)
      $timearray=explode(":",$array[1]);
    else
      $timearray=array(0,0,0);  
       
    return new MyDateTime($datearray[0],$datearray[1],$datearray[2],$timearray[0],$timearray[1],$timearray[2]); 
  } 
  static function fromdate($string)
  {
    if(MyTools::slugify($string)=="" or MyTools::slugify($string)=="n-a") 
      return self::emptydate(); 

    $array=explode(" ",$string); 
    $datearray=explode("-",$array[0]);

    return new MyDateTime($datearray[0],$datearray[1],$datearray[2],0,0,0); 
  } 
  static function fromtimestamp($timestamp) {$date=self::emptydate();$date->timestamp=$timestamp;return $date;}

  function totimestamp() { return $this->timestamp; }
  function __toString() 
  {
    if($this->timestamp==0)
      return "00-00-00 00:00:00"; 
    return date("F d, Y h:i:s a", $this->timestamp); 
  }

  function getampm() { return date("a", $this->timestamp); }
  function gethour() { return date("h", $this->timestamp); }
  function getminute() { return date("i", $this->timestamp); }
  function getsecond() { return date("s", $this->timestamp); }
  function getyear() { return date("Y", $this->timestamp); }
  function getmonth() { return date("m", $this->timestamp); }
  function getday() { return date("d", $this->timestamp); }

  function get24hour() { return date("H", $this->timestamp); }
  function get12hour() { return date("h", $this->timestamp); }

  function getlongmonth() { return date("F", $this->timestamp); }
  function getshortmonth() { return date("M", $this->timestamp); }
  
  function getweek() { return date("w", $this->timestamp); }
  function getshortweek() { return date("D", $this->timestamp); }
  function getlongweek() { return date("l", $this->timestamp); }

  function getisleapyear() { return date("L", $this->timestamp); }
  function getdaysofmonth() { return date("t", $this->timestamp); }

  function tomysql() { return $this->todate(); }
  function tommddyyyy() { return date("m-d-Y", $this->timestamp); }
  function todate() { return $this->getyear()."-".$this->getmonth()."-".$this->getday(); }
  function totime() { return $this->gethour().":".$this->getminute().":".$this->getsecond()." ".$this->getampm(); }
  function todatetime() { return $this->todate()." ".$this->totime(); }
  function toprettydate() { return date("F d, Y", $this->timestamp); }
  function toprettydatewithweek() { return date("F d, Y, ", $this->timestamp).$this->toweekday(); }
  function toshortdate() { return date("M d, Y", $this->timestamp); }
  function toshortdatewithweek() { return date("M d, Y, ", $this->timestamp).$this->toweekday(); }
  function toprettytime() { return date("h:i:s a", $this->timestamp); }
  function toprettytimeshort() { return date("h:i a", $this->timestamp); }

  function toweekday()
  {
    $weekIndex=date("w", $this->timestamp);
    switch($weekIndex)
    {
      case "0":return "Sunday";
      case "1":return "Monday";
      case "2":return "Tuesday";
      case "3":return "Wednesday";
      case "4":return "Thursday";
      case "5":return "Friday";
      case "6":return "Saturday";
    }

  }

  // function getstartofweek() { return $this->copy()->adddays(0-$this->getweek()); }
  // function getendofweek() { return $this->copy()->adddays(7-$this->getweek()); }

  function getthislastweekday($targetweekindex) { 
    //make it negative, coz negative % 7 is negative
    $targetweekindex=$targetweekindex-7;
    $weekindex=$this->getweek()-7;
    $count=0;
    //count down to match target week index
    while($weekindex%7!=$targetweekindex)
    {
      $count--;
      $weekindex--;
    }
    return $this->copy()->adddays($count); 
  }
  function getlastweekday($targetweekindex) { 
    //make it negative, coz negative % 7 is negative
    $targetweekindex=$targetweekindex-7;
    $weekindex=$this->getweek()-7;
    $count=0;
    //don't include today - start yesterday
    $count--;
    $weekindex--;
    //count down to match target week index
    while($weekindex%7!=$targetweekindex)
    {
      $count--;
      $weekindex--;
    }
    return $this->copy()->adddays($count); 
  }
  function getthisweekday($targetweekindex) { 
    $weekindex=$this->getweek();
    $count=0;
    //count up to match target week index
    while($weekindex%7!=$targetweekindex)
    {
      $count++;
      $weekindex++;
    }
    return $this->copy()->adddays($count); 
  }
  function getnextweekday($targetweekindex) { 
    $weekindex=$this->getweek();
    $count=0;
    //don't include today - start tomorrow
    $count++;
    $weekindex++;
    //count up to match target week index
    while($weekindex%7!=$targetweekindex)
    {
      $count++;
      $weekindex++;
    }
    return $this->copy()->adddays($count); 
  }

  function getthislastsunday() { return $this->getthislastweekday(7); }
  function getthislastsaturday() { return $this->getthislastweekday(6); }
  function getthislastfriday() { return $this->getthislastweekday(5); }
  function getthislastthursday() { return $this->getthislastweekday(4); }
  function getthislastwednesday() { return $this->getthislastweekday(3); }
  function getthislasttuesday() { return $this->getthislastweekday(2); }
  function getthislastmonday() { return $this->getthislastweekday(1); }

  function getlastsunday() { return $this->getlastweekday(7); }
  function getlastsaturday() { return $this->getlastweekday(6); }
  function getlastfriday() { return $this->getlastweekday(5); }
  function getlastthursday() { return $this->getlastweekday(4); }
  function getlastwednesday() { return $this->getlastweekday(3); }
  function getlasttuesday() { return $this->getlastweekday(2); }
  function getlastmonday() { return $this->getlastweekday(1); }

  function getthissunday() { return $this->getthisweekday(0); }
  function getthissaturday() { return $this->getthisweekday(6); }
  function getthisfriday() { return $this->getthisweekday(5); }
  function getthisthursday() { return $this->getthisweekday(4); }
  function getthiswednesday() { return $this->getthisweekday(3); }
  function getthistuesday() { return $this->getthisweekday(2); }
  function getthismonday() { return $this->getthisweekday(1); }

  function getnextsunday() { return $this->getnextweekday(0); }
  function getnextsaturday() { return $this->getnextweekday(6); }
  function getnextfriday() { return $this->getnextweekday(5); }
  function getnextthursday() { return $this->getnextweekday(4); }
  function getnextwednesday() { return $this->getnextweekday(3); }
  function getnexttuesday() { return $this->getnextweekday(2); }
  function getnextmonday() { return $this->getnextweekday(1); }

  function getlastweek() { return $this->copy()->adddays(-7); }
  function getnextweek() { return $this->copy()->adddays(7); }

  function getstartofmonth() { return new MyDateTime($this->getyear(), $this->getmonth(), 1, 0,0,0); }
  function getendofmonth() { $date= new MyDateTime($this->getyear(), $this->getmonth()+1, 1, 0,0,0); $date->timestamp-=1;return $date;}
  function getstartofquarter() 
  {
    $quarterstartmonth=0;
    switch($this->getmonth())
    {
      case 1: case 2: case 3: $quarterstartmonth=1;break; 
      case 4: case 5: case 6: $quarterstartmonth=4;break; 
      case 7: case 8: case 9: $quarterstartmonth=7;break; 
      case 10: case 11: case 12: $quarterstartmonth=10;break; 
    }
    return new MyDateTime($this->getyear(), $quarterstartmonth, 1, 0,0,0); 
  }
  function getendofquarter()
  {
    $quarterendmonth=0;
    switch($this->getmonth())
    {
      case 1: case 2: case 3: $quarterendmonth=3;break; 
      case 4: case 5: case 6: $quarterendmonth=6;break; 
      case 7: case 8: case 9: $quarterendmonth=9;break; 
      case 10: case 11: case 12: $quarterendmonth=12;break; 
    }
    $date= new MyDateTime($this->getyear(), $quarterendmonth+1, 1, 0,0,0); $date->timestamp-=1;return $date;
  }
  function getstartofyear() { return new MyDateTime($this->getyear(), 1, 1, 0,0,0); }
  function getendofyear() { $date= new MyDateTime($this->getyear()+1, 1, 1, 0,0,0); $date->timestamp-=1;return $date;}

  function addyears($num)  {return new MyDateTime($this->getyear()+$num, $this->getmonth(), $this->getday(), $this->gethour(),$this->getminute(),$this->getsecond());}
  function addmonths($num) 
  { 
    //if num is negative, thus subtraction of months
    if($num<0)
    {
      $yearaddend=ceil($num/12);
      $monthaddend=ceil($num%12);
    }
    else
    {
      $yearaddend=floor($num/12);
      $monthaddend=floor($num%12);
    }
    return new MyDateTime($this->getyear()+$yearaddend, $this->getmonth()+$monthaddend, $this->getday(), $this->gethour(),$this->getminute(),$this->getsecond());}
  function addweeks($num) { $this->timestamp+=($num*60*60*24*7);return $this; }
  function adddays($num) { $this->timestamp+=($num*60*60*24);return $this; }
  function addhours($num) { $this->timestamp+=($num*60*60);return $this; }
  function addminutes($num) { $this->timestamp+=($num*60);return $this; }
  function addseconds($num) { $this->timestamp+=$num;return $this; }

  function isearlierthan($date){return $this->timestamp < $date->totimestamp();}
  function islaterthan($date){return $this->timestamp > $date->totimestamp();}
  function isequalto($date){return $this->timestamp == $date->totimestamp();}
  function isearlierthanorequalto($date){return $this->timestamp <= $date->totimestamp();}
  function islaterthanorequalto($date){return $this->timestamp >= $date->totimestamp();}

  function isbefore($date){return $this->timestamp < $date->totimestamp();}
  function isafter($date){return $this->timestamp > $date->totimestamp();}
  function ison($date){return $this->timestamp == $date->totimestamp();}
  function isonorbefore($date){return $this->timestamp <= $date->totimestamp();}
  function isonorafter($date){return $this->timestamp >= $date->totimestamp();}
}

