<?php

class MyInvoiceHelper
{
  //printing from windows 8?
	public static function PrintableInvoiceBig($invoice)
	{
    $customer=$invoice->getCustomer();
    $employee=$invoice->getEmployee();
    $details=$invoice->getInvoicedetail();
    
    $date=MyDateTime::frommysql($invoice->getDate())->toshortdatewithweek();
    $terms=$customer->getTerms();
    $terms=$terms>0 ? $terms." Days" : "Cash";
    $address_segments=MyString::getSegments($customer->getAddress(),40);
    $address1=$address_segments[0];
    $address2="";
    if(isset($address_segments[1]))$address2=$address_segments[1];

    //keep track of item lines so we can pad downward 15 lines to vat line
    $itemlinecount=0;
    $totalcommission=0; 
    $totalsalewithcommission=0; 
    $totalsale=0; 
    //$vatentryexists=false; 
    $commissionexists=false; 

    //determine if commission exists  
	  foreach($details as $detail){
	    if($detail->getIsVat()==1);
	    else if($detail->getWithCommission()!=$detail->getPrice())
	      $commissionexists=true;
    }  

    $content="\r\n\r\n\r\n";
    $content.="         ".str_pad($customer->getName(),  40, " ")."      ".$date."\r\n";
    $content.="\r\n";
    $content.="         ".str_pad($customer->getTinNo(),  40, " ")."      ".$terms."\r\n";
    $content.="         ".$address1."\r\n";
    $content.="         ".$address2."\r\n";
    $content.="\r\n\r\n";
    foreach($details as $detail){
      //hide vat entries
      if($detail->getIsVat())continue;
      
      //$segments=$detail->getProduct()->getNameSegments();
      $segments=MyString::getSegments($detail->getProduct(),35);
      $first=true;
      foreach($segments as $segment)
      {
        if($first)
        {
          $content.=
            "   "
            .str_pad(str_replace(".00","",$detail->getQty()),  5, " ",STR_PAD_LEFT)
            ."       "
            .str_pad($segment,  35, " ",STR_PAD_RIGHT)
            ." "
            //.str_pad($detail->getPrice(),  13, " ",STR_PAD_LEFT)
            .str_pad($detail->getWithCommission(),  13, " ",STR_PAD_LEFT)
            ." "
            //.str_pad($detail->getTotal(),  13, " ",STR_PAD_LEFT)
            .str_pad($detail->getWithCommissionTotal(),  13, " ",STR_PAD_LEFT)
            ."\r\n";
          $first=false;
          $itemlinecount++;

          $totalcommission+=$detail->getWithCommissionTotal()-$detail->getTotal();
          $totalsalewithcommission+=$detail->getWithCommissionTotal();
        }
        else
        {
          $content.=
            "   "
            ."     "
            ."       "
            .$segment
            ."\r\n";
          $itemlinecount++;
        }
      }
    }
    //pad down 17 lines to vat line
    for($i=$itemlinecount;$i<(12+5);$i++)
      $content.="\r\n";
      
$withcommisionvat=$totalsalewithcommission/1.12;
    $content.=
      "   "
      ."     "
      ."       "
      ."                                   "
      ." "
      ."             "
      ." "
      .str_pad(number_format($withcommisionvat,2,".",","), 13, " ", STR_PAD_LEFT)
      ."\r\n";
      
    $content.=
      "   "
      ."     "
      ."       "
      ."                                   "
      ." "
      ."             "
      ." "
      .str_pad(number_format($totalsalewithcommission-$withcommisionvat,2,".",","), 13, " ", STR_PAD_LEFT)
      ."\r\n";
      
    $content.=
      "   "
      ."     "
      ."       "
      ."                                   "
      ." "
      ."             "
      ." "
      .str_pad(number_format($totalsalewithcommission,2,".",","), 13, " ", STR_PAD_LEFT)
      ."\r\n";
    
    return $content;
	}
  //printing from windows 7
	public static function PrintableInvoiceSmall($invoice)
	{
    $customer=$invoice->getCustomer();
    $employee=$invoice->getEmployee();
    $details=$invoice->getInvoicedetail();
    
    $date=MyDateTime::frommysql($invoice->getDate())->toshortdatewithweek();
    $terms=$customer->getTerms();
    $terms=$terms>0 ? $terms." Days" : "Cash";
    $address_segments=$customer->getAddressSegments();
    $address1=$address_segments[0];
    $address2=$address_segments[1];

    //keep track of item lines so we can pad downward 15 lines to vat line
    $itemlinecount=0;
    $totalcommission=0; 
    $totalsalewithcommission=0; 
    $totalsale=0; 
    //$vatentryexists=false; 
    $commissionexists=false; 

    //determine if commission exists  
	  foreach($details as $detail){
	    if($detail->getIsVat()==1);
	    else if($detail->getWithCommission()!=$detail->getPrice())
	      $commissionexists=true;
    }  

    $content="\r\n\r\n\r\n\r\n";
    $content.="         ".str_pad($customer->getName(),  40, " ")."      ".$date."\r\n";
    $content.="\r\n";
    $content.="         ".str_pad($customer->getTinNo(),  40, " ")."      ".$terms."\r\n";
    $content.="         ".$address1."\r\n";
    $content.="         ".$address2."\r\n";
    $content.="\r\n\r\n\r\n\r\n";
    foreach($details as $detail){
      //hide vat entries
      if($detail->getIsVat())continue;
      
      $segments=$detail->getProduct()->getNameSegments();
      $first=true;
      foreach($segments as $segment)
      {
        if($first)
        {
          $content.=
            "   "
            .str_pad($detail->getQty(),  5, " ",STR_PAD_LEFT)
            ."       "
            .str_pad($segment,  35, " ",STR_PAD_RIGHT)
            ." "
            //.str_pad($detail->getPrice(),  13, " ",STR_PAD_LEFT)
            .str_pad($detail->getWithCommission(),  13, " ",STR_PAD_LEFT)
            ." "
            //.str_pad($detail->getTotal(),  13, " ",STR_PAD_LEFT)
            .str_pad($detail->getWithCommissionTotal(),  13, " ",STR_PAD_LEFT)
            ."\r\n";
          $first=false;
          $itemlinecount++;

          $totalcommission+=$detail->getWithCommissionTotal()-$detail->getTotal();
          $totalsalewithcommission+=$detail->getWithCommissionTotal();
        }
        else
        {
          $content.=
            "   "
            ."     "
            ."       "
            .$segment
            ."\r\n";
          $itemlinecount++;
        }
      }
    }
    //pad down 22 lines to vat line
    for($i=$itemlinecount;$i<(15+7);$i++)
      $content.="\r\n";
      
$withcommisionvat=$totalsalewithcommission/1.12;
    $content.=
      "   "
      ."     "
      ."       "
      ."                                   "
      ." "
      ."             "
      ." "
      .str_pad(number_format($withcommisionvat,2,".",","), 13, " ", STR_PAD_LEFT)
      ."\r\n";
      
    $content.=
      "   "
      ."     "
      ."       "
      ."                                   "
      ." "
      ."             "
      ." "
      .str_pad(number_format($totalsalewithcommission-$withcommisionvat,2,".",","), 13, " ", STR_PAD_LEFT)
      ."\r\n";
      
    $content.="\r\n";
    $content.=
      "   "
      ."     "
      ."       "
      ."                                   "
      ." "
      ."             "
      ." "
      .str_pad(number_format($totalsalewithcommission,2,".",","), 13, " ", STR_PAD_LEFT)
      ."\r\n";
    
    return $content;
	}
	public static function PrintableStatement($invoice,$statement_code)
	{
	  $finalbalance=$invoice->getBalance();

	  $DATE=0;
	  $REF=2;
	  $DETAILS=4;
	  $AMOUNT=6;
	  $BALANCE=8;
    $widths=array(10,1,15,1,20,1,12,1,12);

    $signlinewidth=12;
    $signlinespacingwidth=5;
    $signlineleftpad=2;
	  $pagewidth=75;
  
    //keep track of item lines so we can pad downward 15 lines to vat line
    $itemlinecount=0;
    //$totalsale=0; 
    //$vatentryexists=false; 
    
    $company_name=sfConfig::get('custom_company_name');
    $company_address=sfConfig::get('custom_company_address');

    //if template is not "transaction"
    //show invno
    $template=$invoice->getInvoiceTemplate();
    if($template->getId()!=10)
    {
      $title1="Transaction Statement for ".$invoice->getInvoiceTemplate()->getPrefix()." ".$invoice->getInvno();    
    }
    //transaction
    //do not show invno because it is already shown elsewhere
    else
    {
      $title1="Transaction Statement";    
    }
    
    $datetime=MyDateTime::today();
    $date=$datetime->toshortdatewithweek();//." ".$datetime->toprettytimeshort();
    
    if($invoice->getStatus()=="Paid")
      $balance="FULLY PAID";
    else
    {
      $datetime=MyDateTime::today();
      $balance="Balance: P".MyDecimal::format($invoice->getBalance());
    }
    
    $customer=$invoice->getCustomer();
    $customername="Customer: ".$customer;
    $transaction_no="Transaction No.:".$invoice->getTransactionCode();
    
  
    $content="";

    //hide headers by config
    // $template=$invoice->getInvoiceTemplate();
    $showheader=true;
    if($template->getId()==10 and sfConfig::get('custom_no_headers_on_transaction'))$showheader=false;
    if($template->getId()==2 and sfConfig::get('custom_no_headers_on_interoffice'))$showheader=false;
    if($showheader)
    {
    $content.=str_pad("", ($pagewidth-strlen($company_name))/2, " ", STR_PAD_LEFT).$company_name."\r\n";
    $content.=str_pad("", ($pagewidth-strlen($company_address))/2, " ", STR_PAD_LEFT).$company_address."\r\n";
    $content.="                        ".sfConfig::get('custom_company_phone')."\r\n";
    }
    else
    {
      $content.="".sfConfig::get('custom_header_for_no_header');
    }

    $content.="\r\n";

    //$content.=str_pad("", ($pagewidth-strlen($title1))/2, " ", STR_PAD_LEFT).$title1."\r\n";
    $content.=$title1.str_pad("", (($pagewidth*3/4)-strlen($title1)), " ", STR_PAD_RIGHT).$date."\r\n";
    $content.=$transaction_no.str_pad("", (($pagewidth*3/4)-strlen($transaction_no)), " ", STR_PAD_RIGHT).$balance."\r\n";
    $content.=$customername.str_pad("", (($pagewidth*3/4)-strlen($customername)), " ", STR_PAD_RIGHT)."Salesman: ".$invoice->getEmployee()."\r\n";
    $content.="Address: ".$customer->getAddress()." ".$customer->getAddress2()."\r\n";
    //$content.="                             ".sfConfig::get('custom_company_email')."\r\n";
    //$content.="                                ".sfConfig::get('custom_company_tin')."\r\n";
    
    $content.="\r\n";

    $content.=
      ""
      .str_pad("DATE", $widths[$DATE], " ",STR_PAD_RIGHT)
      ." "
      .str_pad("REF", $widths[$REF], " ",STR_PAD_RIGHT)
      ." "
      .str_pad("DETAILS", $widths[$DETAILS], " ",STR_PAD_RIGHT)
      ." "
      .str_pad("AMOUNT", $widths[$AMOUNT], " ",STR_PAD_LEFT)
      ." "
      .str_pad("BALANCE", $widths[$BALANCE], " ",STR_PAD_LEFT)
      ."\r\n";
    
    if(
      $invoice->getStatus()=="Cancelled"
    )
    {
      $status=strtoupper($invoice->getStatus());
      //fancy status
      //if($invoice->getStatus()=="Paid")
        //$status="FULLY PAID";
    
      $detail=$invoice->getParticularsString();

      //split product name into segments
      //first segment is 25 chars long
      //each succeeding segment is 70 chars long
      $detailsegments=MyString::getSegments($detail,$widths[$DETAILS]-3,2);

      //hide succeeding lines of details
      //if(isset($detailsegments[1]))$detailsegments2=MyString::getSegments($detailsegments[1],$pagewidth-$widths[0]-$widths[1]-5);
      //else $detailsegments2=array();

      $elipsis="";
      if(isset($detailsegments[1]))$elipsis="...";
      $content.=
        ""
        .$invoice->getDate()
        ." "
        .str_pad($invoice->getInvoiceTemplate()->getPrefix()." ".$invoice->getInvno(),  $widths[$REF], " ",STR_PAD_RIGHT)
        ." "
        //line 1 of details
        .str_pad($detailsegments[0].$elipsis,  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
        ." "
        .str_pad($status, $widths[$AMOUNT], " ",STR_PAD_RIGHT)
        //.str_pad(MyDecimal::format($invoice->getTotal()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
        //." "
        //.str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
        ."\r\n";
      $itemlinecount++;

/*
      //product lines 2, 3, 4...
      foreach($detailsegments2 as $segment)
      {
          $content.=
            str_pad("", $widths[0]+$widths[1]+2, " ", STR_PAD_LEFT)
            .$segment
            ."\r\n";
          $itemlinecount++;
      }
*/
    }
    else
    {
      //hide vat entries
      //if($detail->getIsVat())continue;

      //do events first
      $events=Doctrine_Query::create()
      ->from('Event e')
    	->where("e.parent_id=".$invoice->getId())
    	->andWhere("e.parent_class='Invoice'")
      ->orderBy("e.date desc")
      ->execute();

      foreach($events as $event)
      {
        $type="";
        $details="";
        switch($event->getType())
        {
          case "ChequeCollect": 
            $type="Cheque Payment";
            $details=trim($event->getDetail1()." ".$event->getDetail2()." ".$event->getDetail3()." ".$event->getNotes());
            break;
          case "CashCollect": 
            $type="Cash Payment";
            break;
        }
        $content.=
          ""
          .$event->getDate()
          ." "
          .str_pad($type,  $widths[$REF], " ",STR_PAD_RIGHT)
          ." "
          //line 1 of details
          .str_pad($details,  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
          ." "
          .str_pad(MyDecimal::format(0-$event->getAmount()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
          ." "
          .str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
          ."\r\n";
        $finalbalance+=$event->getAmount();
        $itemlinecount++;
      }


      //do invoice payment if cash or cheque or partial
      if($invoice->getCash()>0)
      {
        $content.=
          ""
          .$invoice->getDate()
          ." "
          .str_pad("Cash Payment",  $widths[$REF], " ",STR_PAD_RIGHT)
          ." "
          //line 1 of details
          .str_pad("",  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
          ." "
          .str_pad(MyDecimal::format(0-$invoice->getCash()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
          ." "
          .str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
          ."\r\n";
        $finalbalance+=$invoice->getCash();
        $itemlinecount++;
      }
      if($invoice->getChequeamt()>0)
      {
        $content.=
          ""
          .$invoice->getDate()
          ." "
          .str_pad("Cheque Payment",  $widths[$REF], " ",STR_PAD_RIGHT)
          ." "
          //line 1 of details
          .str_pad($invoice->getCheque(),  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
          ." "
          .str_pad(MyDecimal::format(0-$invoice->getChequeamt()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
          ." "
          .str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
          ."\r\n";
        $finalbalance+=$invoice->getChequeamt();
        $itemlinecount++;
      }

      //now do invoice      
      $detail=$invoice->getParticularsString();

      //split product name into segments
      //first segment is 25 chars long
      //each succeeding segment is 70 chars long
      // if(false)
      // {
      //   //first line contains 25 chars of details
      //   $detailsegments=MyString::getSegments($detail,$widths[$DETAILS],2);
      //   if(isset($detailsegments[1]))
      //   $detailsegments2=MyString::getSegments($detail,$pagewidth-$widths[0]-$widths[1]-5);
      //   else $detailsegments2=array();
      //   }
      // else
      // {
      //   //all product details in line 2 onwards
      //   $detailsegments2=MyString::getSegments($detail,$pagewidth-$widths[0]-$widths[1]-5);
      // }
      //product line 1
      $content.=
        ""
        .$invoice->getDate()
        ." "
        .str_pad($invoice->getInvoiceTemplate()->getPrefix()." ".$invoice->getInvno(),  $widths[$REF], " ",STR_PAD_RIGHT)
        ." "
        //line 1 of details
        //.str_pad($detailsegments[0],  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
        ."                         "
        ." "
        .str_pad(MyDecimal::format($invoice->getTotal()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
        ." "
        .str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
        ."\r\n";
      $finalbalance-=$invoice->getTotal();
      $itemlinecount++;

      //product lines 2, 3, 4...
      $details=$invoice->getInvoicedetail();
      for($i=0;$i<count($details);$i++)
      {
          //to make things prettier
          //if segments are only 1 line and is short,
          //indent it to the details column
          // $segment=$detailsegments2[$i];
          $detail=$details[$i];
          $qtystring=$detail->getQty();
          $qtystring=str_replace(".00","",$qtystring);
          $discountstring="";
          if($detail->getDiscrate()!="")$discountstring=" less ".$detail->getDiscrate()."% ";
          $detailstring=$qtystring." x ".$detail->getProduct()->getName()." x P".MyDecimal::format($detail->getPrice()).$discountstring." = P".MyDecimal::format($detail->getTotal());
          $segments=MyString::getSegments($detailstring,$pagewidth-$widths[0]-$widths[1]-5);

          $widthstotal=$widths[4]+$widths[5]+$widths[6]+$widths[7]+$widths[8]-5;
          for($j=0;$j<count($segments);$j++)
          {
            $segment=$segments[$j];
            if($j!=0)
            {
              //indent it 5 chars deeper than the ref column
              $content.=
                str_pad("", $widths[0]+$widths[1]+7, " ", STR_PAD_LEFT)
                .$segment
                ."\r\n";
              }
              //indent it to the ref column
              else
              {
              $content.=
                str_pad("", $widths[0]+$widths[1]+2, " ", STR_PAD_LEFT)
                .$segment
                ."\r\n";
              }
              $itemlinecount++;
            }
      }
    }

    //pad down 17 lines to vat line
    for($i=$itemlinecount;$i<(12+6);$i++)
      $content.="\r\n";
      
    $content.=""
.str_pad("", $signlineleftpad, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
."Statement ".$statement_code
      ."\r\n"
.str_pad("", $signlineleftpad, " ", STR_PAD_LEFT)
.str_pad("Prepared By", $signlinewidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("Approved By", $signlinewidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("Received By", $signlinewidth, " ", STR_PAD_LEFT)
."\r\n"
."   This does not serve as your official receipt\r\n"
      ;
    return $content;
	}
}

