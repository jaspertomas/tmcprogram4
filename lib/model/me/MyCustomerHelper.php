<?php

class MyCustomerHelper
{
	public static function PrintableStatement($customer)
	{
	
    $invoices=Doctrine_Query::create()
      ->from('Invoice i')
    	->where("i.customer_id=".$customer->getId())
    	->andWhere("i.is_temporary=0")
      ->orderBy("i.date desc")
      ->execute();
      
	  $finalbalance=0;
    foreach($invoices as$invoice)if($invoice->getStatus()!="Paid" and $invoice->getStatus()!="Cancelled")
    {
      $finalbalance+=$invoice->getBalance();
    }
	
	  $DATE=0;
	  $REF=2;
	  $DETAILS=4;
	  $AMOUNT=6;
	  $BALANCE=8;
    $widths=array(10,1,15,1,20,1,12,1,12);

    $signlinewidth=12;
    $signlinespacingwidth=5;
    $signlineleftpad=2;
	  $pagewidth=75;
  
    //keep track of item lines so we can pad downward 15 lines to vat line
    $itemlinecount=0;
    //$totalsale=0; 
    //$vatentryexists=false; 
    
    $company_name=sfConfig::get('custom_company_name');
    $company_address=sfConfig::get('custom_company_address');
    $title1="Statement of Account for ".$customer->getName();
    $title2="Final balance as of ".MyDateTime::today()->toprettydate().": P".MyDecimal::format($finalbalance);
  
    $content="";
    $content.=str_pad("", ($pagewidth-strlen($company_name))/2, " ", STR_PAD_LEFT).$company_name."\r\n";
    $content.=str_pad("", ($pagewidth-strlen($company_address))/2, " ", STR_PAD_LEFT).$company_address."\r\n";
    $content.="                        ".sfConfig::get('custom_company_phone')."\r\n";

    $content.="\r\n";

    $content.=str_pad("", ($pagewidth-strlen($title1))/2, " ", STR_PAD_LEFT).$title1."\r\n";
    $content.=str_pad("", ($pagewidth-strlen($title2))/2, " ", STR_PAD_LEFT).$title2."\r\n";
    //$content.="                             ".sfConfig::get('custom_company_email')."\r\n";
    //$content.="                                ".sfConfig::get('custom_company_tin')."\r\n";
    
    $content.="\r\n";

    $content.=
      ""
      .str_pad("DATE", $widths[$DATE], " ",STR_PAD_RIGHT)
      ." "
      .str_pad("REF", $widths[$REF], " ",STR_PAD_RIGHT)
      ." "
      .str_pad("DETAILS", $widths[$DETAILS], " ",STR_PAD_RIGHT)
      ." "
      .str_pad("AMOUNT", $widths[$AMOUNT], " ",STR_PAD_LEFT)
      ." "
      .str_pad("BALANCE", $widths[$BALANCE], " ",STR_PAD_LEFT)
      ."\r\n";
    
    $invoicecount=0; //after the 5th invoice, hide events of paid invoices
    foreach($invoices as $invoice){
    if(
      $invoice->getStatus()=="Cancelled"
      or
      $invoice->getStatus()=="Paid" //and not full report
    )
    {
      $status=strtoupper($invoice->getStatus());
      //fancy status
      if($invoice->getStatus()=="Paid")
        $status="FULLY PAID";
    
      $detail=$invoice->getParticularsString();

      //split product name into segments
      //first segment is 25 chars long
      //each succeeding segment is 70 chars long
      $detailsegments=MyString::getSegments($detail,$widths[$DETAILS]-3,2);

      //hide succeeding lines of details
      //if(isset($detailsegments[1]))$detailsegments2=MyString::getSegments($detailsegments[1],$pagewidth-$widths[0]-$widths[1]-5);
      //else $detailsegments2=array();

      $elipsis="";
      if(isset($detailsegments[1]))$elipsis="...";
      $content.=
        ""
        .$invoice->getDate()
        ." "
        .str_pad($invoice->getInvoiceTemplate()->getPrefix()." ".$invoice->getInvno(),  $widths[$REF], " ",STR_PAD_RIGHT)
        ." "
        //line 1 of details
        .str_pad($detailsegments[0].$elipsis,  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
        ." "
        .str_pad($status, $widths[$AMOUNT], " ",STR_PAD_RIGHT)
        //.str_pad(MyDecimal::format($invoice->getTotal()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
        //." "
        //.str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
        ."\r\n";
      $itemlinecount++;

/*
      //product lines 2, 3, 4...
      foreach($detailsegments2 as $segment)
      {
          $content.=
            str_pad("", $widths[0]+$widths[1]+2, " ", STR_PAD_LEFT)
            .$segment
            ."\r\n";
          $itemlinecount++;
      }
*/
    }
    else
    {
      //hide vat entries
      //if($detail->getIsVat())continue;

      //do events first
      $events=Doctrine_Query::create()
      ->from('Event e')
    	->where("e.parent_id=".$invoice->getId())
    	->andWhere("e.parent_class='Invoice'")
      ->orderBy("e.date desc")
      ->execute();

      foreach($events as $event)
      {
        $type="";
        $details="";
        switch($event->getType())
        {
          case "ChequeCollect": 
            $type="Cheque Payment";
            $details=trim($event->getDetail1()." ".$event->getDetail2()." ".$event->getDetail3()." ".$event->getNotes());
            break;
          case "CashCollect": 
            $type="Cash Payment";
            break;
        }
        $content.=
          ""
          .$event->getDate()
          ." "
          .str_pad($type,  $widths[$REF], " ",STR_PAD_RIGHT)
          ." "
          //line 1 of details
          .str_pad($details,  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
          ." "
          .str_pad(MyDecimal::format(0-$event->getAmount()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
          ." "
          .str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
          ."\r\n";
        $finalbalance+=$event->getAmount();
        $itemlinecount++;
      }


      //do invoice payment if cash or cheque or partial
      if($invoice->getCash()>0)
      {
        $content.=
          ""
          .$invoice->getDate()
          ." "
          .str_pad("Cash Payment",  $widths[$REF], " ",STR_PAD_RIGHT)
          ." "
          //line 1 of details
          .str_pad("",  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
          ." "
          .str_pad(MyDecimal::format(0-$invoice->getCash()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
          ." "
          .str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
          ."\r\n";
        $finalbalance+=$invoice->getCash();
        $itemlinecount++;
      }
      if($invoice->getChequeamt()>0)
      {
        $content.=
          ""
          .$invoice->getDate()
          ." "
          .str_pad("Cheque Payment",  $widths[$REF], " ",STR_PAD_RIGHT)
          ." "
          //line 1 of details
          .str_pad($invoice->getCheque(),  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
          ." "
          .str_pad(MyDecimal::format(0-$invoice->getChequeamt()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
          ." "
          .str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
          ."\r\n";
        $finalbalance+=$invoice->getChequeamt();
        $itemlinecount++;
      }

      //now do invoice      
      $detail=$invoice->getParticularsString();

      //split product name into segments
      //first segment is 25 chars long
      //each succeeding segment is 70 chars long
      $detailsegments=MyString::getSegments($detail,$widths[$DETAILS],2);
      if(isset($detailsegments[1]))$detailsegments2=MyString::getSegments($detailsegments[1],$pagewidth-$widths[0]-$widths[1]-5);
      else $detailsegments2=array();
      //product line 1
      $content.=
        ""
        .$invoice->getDate()
        ." "
        .str_pad($invoice->getInvoiceTemplate()->getPrefix()." ".$invoice->getInvno(),  $widths[$REF], " ",STR_PAD_RIGHT)
        ." "
        //line 1 of details
        .str_pad($detailsegments[0],  $widths[$DETAILS], " ",STR_PAD_RIGHT) 
        ." "
        .str_pad(MyDecimal::format($invoice->getTotal()),  $widths[$AMOUNT], " ",STR_PAD_LEFT)
        ." "
        .str_pad(MyDecimal::format($finalbalance),  $widths[$BALANCE], " ",STR_PAD_LEFT)
        ."\r\n";
      $finalbalance-=$invoice->getTotal();
      $itemlinecount++;

      //product lines 2, 3, 4...
      foreach($detailsegments2 as $segment)
      {
          $content.=
            str_pad("", $widths[0]+$widths[1]+2, " ", STR_PAD_LEFT)
            .$segment
            ."\r\n";
          $itemlinecount++;
      }
    }
  }//end for each invoice

    //pad down 17 lines to vat line
    for($i=$itemlinecount;$i<(12+9);$i++)
      $content.="\r\n";
      
    $content.=""
.str_pad("", $signlineleftpad, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinewidth, "_", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
      ."\r\n"
.str_pad("", $signlineleftpad, " ", STR_PAD_LEFT)
.str_pad("Prepared By", $signlinewidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("Approved By", $signlinewidth, " ", STR_PAD_LEFT)
.str_pad("", $signlinespacingwidth, " ", STR_PAD_LEFT)
.str_pad("Received By", $signlinewidth, " ", STR_PAD_LEFT)
      ."\r\n"
      ;
    return $content;
	}
}

