<?php

class MyBarcode
{
  //for EAN13
  public static function standardize($str)
  {
    $str=str_pad($str,12,"0",STR_PAD_LEFT);

  	$sum=($str[1]+$str[3]+$str[5]+$str[7]+$str[9]+$str[11])*3+
        	$str[0]+$str[2]+$str[4]+$str[6]+$str[8]+$str[10];
  	$checksum=10-($sum%10);
  	if($checksum==10)$checksum=0;
  	return $str.$checksum;
  }
  public static function evaluateBarcode($barcode)
  {
    $suffix=substr($barcode,-1);//last letter
    $body=intval(substr($barcode,0,-1));//barcode minus last letter minus leading zeros
    
    //if barcode points to a product (last character of barcode is 'P')
    if($suffix=="P" or $suffix=="p")
    {
      //product id is rest of barcode minus leading zeros
    	//select product where id=barcode body
      $products=Doctrine_Query::create()
        ->from('Product p')
      	->where('p.id='.$body)
      	->execute();
    	//product not found
      if(!isset($products[0]))
      {
        $arr[0] = 0;
        echo json_encode($arr);
        exit();  
      }
      $product=$products[0];  
    }
    //if barcode points to a purchase detail (last character of barcode is 'D')
    else if($suffix=="D" or $suffix=="d")
    {
      //purchase detail id is rest of barcode minus leading zeros
    	//select purchase detail where id=barcode body
      $purchdetails=Doctrine_Query::create()
        ->from('purchasedetail d')
      	->where('d.id='.$body)
      	->execute();
    	//purchase detail not found
      if(!isset($purchdetails[0]))
      {
        $arr[0] = 0;
        echo json_encode($arr);
        exit();  
      }
    	$purchdetail=$purchdetails[0];
    	//select product where id=purchdetail's product id
      $products=Doctrine_Query::create()
        ->from('Product p')
      	->where('p.id='.$purchdetail->getProductId())
      	->execute();
    	//product not found
      if(!isset($products[0]))
      {
        $arr[0] = 0;
        echo json_encode($arr);
        exit();  
      }
      $product=$products[0];  
    }
    return array($suffix,$body,$purchdetail,$product);
  }
}

