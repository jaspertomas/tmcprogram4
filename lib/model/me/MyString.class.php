<?php

class MyString
{
  static function getSegments($string, $length, $maxlines=null)
  {
    $segments=explode(" ",trim($string));
    $linecounter=0;
    $outputarray=array();

    //if maxlines is not set, make it an unreachable number
    if($maxlines==null)$maxlines=count($segments)+1;

    //combine segments into lines but no longer than 40 chars
    $i=0;
    while($i<count($segments) and $linecounter < $maxlines)
    {
      $line="";
      $first=true;
      for(;$i<count($segments) and strlen($line." ".$segments[$i])<=$length;$i++)
      {
        if(!$first)$line.=" ";
        $first=false;
        $line.=$segments[$i];
      }
      $outputarray[$linecounter] = $line;
      $linecounter++;
    }
    //if segments remain after maxlines is reached, 
    //just keep adding all remaining segments to last line
    for(;$i<count($segments);$i++)
    {
      $outputarray[$linecounter-1].=" ".$segments[$i];
    }
    
    return $outputarray;
  /*
    $segments=explode(" ",trim($string));
    $segmentcount=count($segments);
    $line1="";
    $line2="";

    //line 1: combine segments but no longer than $length chars
    $i=0;
    $first=true;
    for(;$i < $segmentcount and strlen($line1." ".$segments[$i]) <= $length ;$i++)
    {
      if(!$first)$line1.=" ";
      $first=false;
      $line1.=$segments[$i];
    }
    //line 2: combine the rest of the segments
    $first=true;
    for(;$i<count($segments);$i++)
    {
      if(!$first)$line2.=" ";
      $first=false;
      $line2.=$segments[$i];
    }
    return array($line1,$line2);
*/
  }
}

