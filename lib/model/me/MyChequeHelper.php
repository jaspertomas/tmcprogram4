<?php

class MyChequeHelper
{
	public static function printChequePdf($voucher,$checkDate,$mode="")
	{
    $s="     ";
    $date=str_replace("Sep","Sept",MyDateTime::frommysql($checkDate)->tommddyyyy());
    $amount=$voucher->getAmount();
    $payee=$voucher->getPayee();
    $payeeString=str_pad($payee, 45, " ");
    $payeeLines="\n".$s.$s.$payeeString.MyDecimal::format($amount)."\n";

    //if amount string is too long, split it into 2 lines
    $payeetwolines=false;
    $payeefirsthalf="";
    $payeesecondhalf="";
    if(strlen($payee)>45)
    {
        $segments=explode(" ",$payee);
        $segmentcount=count($segments);
        $firsthalfcount=$segmentcount/2;
        $secondhalfcount=$segmentcount-$firsthalfcount;
        $firsthalf=implode(" ",array_slice($segments,0,$firsthalfcount));
        $firsthalf=str_pad($firsthalf, 45, " ");
        $secondhalf=implode(" ",array_slice($segments,$firsthalfcount));
        $secondhalf=str_pad($secondhalf, 45, " ");
        $payeeLines=      
            $s.$s.$firsthalf."\n".
            $s.$s.$secondhalf.MyDecimal::format($amount)."\n";

        $payeetwolines=true;
        $payeefirsthalf=$firsthalf;
        $payeesecondhalf=$secondhalf;
    }

    $amountString=MyDecimal::toPesoString($voucher->getAmount());
    $amountLines=
        $s.$s.$amountString."\n"."\n";

    //if amount string will fit in one line
    if(strlen($amountString)<=60)
    {
        $date="";//dates must be exact in boxes, do not print
        return 
            $s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$date."\n".
            $payeeLines.
            $amountLines
            ;  
    }
    //if amount string is too long, split it into 2 lines
    else
    {
        $segments=explode(" ",$amountString);
        $segmentcount=count($segments);
        $firsthalfcount=$segmentcount/2;
        $secondhalfcount=$segmentcount-$firsthalfcount;
        $firsthalf=implode(" ",array_slice($segments,0,$firsthalfcount));
        $secondhalf=implode(" ",array_slice($segments,$firsthalfcount));
        $firsthalf=str_pad($firsthalf, 45, " ");
        $secondhalf=str_pad($secondhalf, 45, " ");

        //if payee also needs 2 lines
        if($payeetwolines)
        {
            //put payee line 1 in line with date
    
            return 
            $s.$s.$payeefirsthalf.$date."\n".
            $s.$s.$payeesecondhalf."\n".
            $s.$s.$firsthalf.MyDecimal::format($amount)."\n".
            $s.$s.$secondhalf;
        }
        else //payee only needs 1 line
        {
            return 
            $s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$date."\n".
            $s.$s.$payeeString."\n".
            $s.$s.$firsthalf.MyDecimal::format($amount)."\n".
            $s.$s.$secondhalf;
        }
    }
  }
	public static function printCheque($voucher,$checkDate)
	{
    $s="     ";
    $date=str_replace("Sep","Sept",MyDateTime::frommysql($checkDate)->tommddyyyy());
    $amount=$voucher->getAmount();
    $payee=$voucher->getPayee();
    $payeeString=str_pad($payee, 45, " ");
    $payeeLines=$s.$s.$payeeString.$s.MyDecimal::format($amount)."\n";
    //if amount string is too long, split it into 2 lines
    if(strlen($payee)>45)
    {
        $segments=explode(" ",$payee);
        $segmentcount=count($segments);
        $firsthalfcount=$segmentcount/2;
        $secondhalfcount=$segmentcount-$firsthalfcount;
        $firsthalf=implode(" ",array_slice($segments,0,$firsthalfcount));
        $secondhalf=implode(" ",array_slice($segments,$firsthalfcount));
        $secondhalf=str_pad($secondhalf, 45, " ");
        $payeeLines=      
            $s.$s.$firsthalf."\n".
            $s.$s.$secondhalf.$s.MyDecimal::format($amount)."\n";
    }

    $amountString=MyDecimal::toPesoString($voucher->getAmount());
    $amountLines=
        "\n".
        $s.$s.$amountString."\n";

    //if amount string is too long, split it into 2 lines
    if(strlen($amountString)>60)
    {
        $segments=explode(" ",$amountString);
        $segmentcount=count($segments);
        $firsthalfcount=$segmentcount/2;
        $secondhalfcount=$segmentcount-$firsthalfcount;
        $firsthalf=implode(" ",array_slice($segments,0,$firsthalfcount));
        $secondhalf=implode(" ",array_slice($segments,$firsthalfcount));
        $amountLines=      
            $s.$s.$firsthalf."\n".
            $s.$s.$secondhalf."\n";
    }

    return 
      "\n"."\n".
      $s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$date."\n".
      $payeeLines.
      $amountLines
      ;
  }
	public static function printVoucher($voucher,$checkNo)
	{
    $content="";
/*
    $checkNo="";
    $out_payment=$voucher->getOutPayment();
    if($out_payment)$check=$out_payment->getOutCheck();
    if($check)$checkNo=$check->getCheckNo();
*/
    $content.= "\nCHECK VOUCHER";
    $content.= "\n";
    $content.= "\nCHECK NO: ".$checkNo;
    $content.= "\nAMOUNT: ".$voucher->getAmount();
    $content.= "\nCHECK DATE: ".MyDateTime::frommysql($voucher->getDate())->toshortdate();
    $content.= "\nPAYEE: ".$voucher->getPayee();
    $content.= "\nPARTICULARS: ".$voucher->getParticulars();
    $content.= "\nNOTES: ".$voucher->getNotes();
    $content.= "\nEXPENSE TYPE: ".$voucher->getVoucherAccount()->getName();
    $content.= "\nEXPENSE OF: ".$voucher->getVoucherAllocation()->getName();
    // $content.= "\nPayment Type: ".$voucher->getVoucherType()->getName();

/*
    $date=MyDateTime::frommysql($voucher->getDate())->toshortdate();
    $payee=$voucher->getPayee();
    $payee=str_pad($payee, 45, " ");
    $amount=$voucher->getAmount();
    $amountString=MyDecimal::toPesoString($voucher->getAmount());

    $s="     ";
return 
"\n".
$s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$s.$date."\n".
"\n".
$s.$s.$s.$payee.MyDecimal::format($amount)."\n".
"\n".
$s.$s.$s.$amountString."\n"
;
*/
    return $content;
  }
}

