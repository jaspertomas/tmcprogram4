<?php

/**
 * Quotation form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class QuotationForm extends BaseQuotationForm
{
  public function configure()
  {
    $this->widgetSchema['code']= new sfWidgetFormInputHidden();
    $this->widgetSchema['total']= new sfWidgetFormInputHidden();
  }
}
