<?php

/**
 * Producttype form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProducttypeForm extends BaseProducttypeForm
{
  public function configure()
  {
    $this->widgetSchema['parent_id']= new sfWidgetFormInputHidden();
    // $this->widgetSchema['is_hidden']= new sfWidgetFormInputHidden();
    $this->widgetSchema['name']->setAttribute('size',70);
    $this->widgetSchema['description']->setAttribute('size',70);
    $this->widgetSchema['product_name_format']->setAttribute('size',70);
    $this->widgetSchema['product_description_format']->setAttribute('size',70);
    $this->widgetSchema['barcode_format']->setAttribute('size',70);
    $this->widgetSchema['barcode_format_2']->setAttribute('size',70);
    $this->widgetSchema['max_sell_formula']->setAttribute('size',70);
    $this->widgetSchema['min_sell_formula']->setAttribute('size',70);
    $this->widgetSchema['max_buy_formula']->setAttribute('size',70);
    $this->widgetSchema['min_buy_formula']->setAttribute('size',70);
    $this->widgetSchema['base_formula']->setAttribute('size',70);
    $this->widgetSchema['is_updated'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
  }
}
