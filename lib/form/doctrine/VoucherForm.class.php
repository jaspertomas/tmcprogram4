<?php

/**
 * Voucher form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VoucherForm extends BaseVoucherForm
{
  public function configure()
  {
    //define dropdown box options via query
    $query=Doctrine::getTable('VoucherAccount')
      ->createQuery('a')
      ->orderBy('a.name')
      ;  
    $this->widgetSchema['account_id']->addOption("query",$query);
    $this->widgetSchema['particulars']->setAttribute('rows',2);
    $this->widgetSchema['amount']->setAttribute('size',5);
    $this->widgetSchema['no']->setAttribute('size',5);
    $this->widgetSchema['ref_type']= new sfWidgetFormInputHidden();
    $this->widgetSchema['ref_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['client_type']= new sfWidgetFormInputHidden();
    $this->widgetSchema['client_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['print_count']= new sfWidgetFormInputHidden();
    $this->widgetSchema['print_date']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_cancelled']= new sfWidgetFormInputHidden();
    $this->widgetSchema['check_status']= new sfWidgetFormInputHidden();
    $this->widgetSchema['invoice_cross_ref_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['purchase_cross_ref_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_pay_to_self'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
    $this->widgetSchema['is_checked'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
  }
}
