<?php

/**
 * Delivery form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class DeliveryForm extends BaseDeliveryForm
{
  public function configure()
  {
    $this->widgetSchema['warehouse_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['type']= new sfWidgetFormInputHidden();
    $this->widgetSchema['status']= new sfWidgetFormInputHidden();
    $this->widgetSchema['client_class']= new sfWidgetFormInputHidden();
    $this->widgetSchema['client_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['ref_class']= new sfWidgetFormInputHidden();
    $this->widgetSchema['ref_id']= new sfWidgetFormInputHidden();
  }
}
