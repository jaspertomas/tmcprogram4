<?php

/**
 * JobOrderDetail form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class JobOrderDetailForm extends BaseJobOrderDetailForm
{
  public function configure()
  {
    $this->widgetSchema['product_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['slot']= new sfWidgetFormInputHidden();
    $this->widgetSchema['color']= new sfWidgetFormInputHidden();
    $this->widgetSchema['qty']->setAttribute('size',1);
    $this->widgetSchema['qty_produced']->setAttribute('size',1);
    $this->widgetSchema['created_at']= new sfWidgetFormInputHidden();
    $this->widgetSchema['created_by_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['updated_at']= new sfWidgetFormInputHidden();
    $this->widgetSchema['updated_by_id']= new sfWidgetFormInputHidden();
  }
}
