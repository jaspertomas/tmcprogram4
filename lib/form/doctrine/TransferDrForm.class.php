<?php

/**
 * TransferDr form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TransferDrForm extends BaseTransferDrForm
{
  public function configure()
  {
/*
    //restrict choice of billers according to biller type
    $billers= Doctrine_Query::create()
      ->from('Biller b')
      ->where('b.biller_type_id ='.$biller_type_id)
      ->execute();
    $billersarray=array();
    foreach($billers as $t)$billersarray[$t->getId()]=$t->getName();
    $widgetschema=$this->form->getWidgetSchema();
    $widgetschema['biller_id']=new sfWidgetFormChoice(array('choices' => $billersarray));
*/
    $this->widgetSchema['created_at']= new sfWidgetFormInputHidden();
    $this->widgetSchema['created_by_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['updated_at']= new sfWidgetFormInputHidden();
    $this->widgetSchema['updated_by_id']= new sfWidgetFormInputHidden();
    // $this->widgetSchema['is_temporary']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_transferred']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_cancelled']= new sfWidgetFormInputHidden();
  }
}
