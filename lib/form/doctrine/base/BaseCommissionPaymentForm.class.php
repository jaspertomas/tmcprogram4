<?php

/**
 * CommissionPayment form base class.
 *
 * @method CommissionPayment getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCommissionPaymentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'name'         => new sfWidgetFormInputText(),
      'type'         => new sfWidgetFormChoice(array('choices' => array('Profit' => 'Profit', 'Gross' => 'Gross'))),
      'employee_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Employee'), 'add_empty' => false)),
      'startdate'    => new sfWidgetFormDate(),
      'enddate'      => new sfWidgetFormDate(),
      'date_created' => new sfWidgetFormDate(),
      'date_paid'    => new sfWidgetFormDate(),
      'base1'        => new sfWidgetFormInputText(),
      'rate1'        => new sfWidgetFormInputText(),
      'base2'        => new sfWidgetFormInputText(),
      'rate2'        => new sfWidgetFormInputText(),
      'commission'   => new sfWidgetFormInputText(),
      'balance'      => new sfWidgetFormInputText(),
      'notes'        => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'         => new sfValidatorString(array('max_length' => 50)),
      'type'         => new sfValidatorChoice(array('choices' => array(0 => 'Profit', 1 => 'Gross'), 'required' => false)),
      'employee_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Employee'))),
      'startdate'    => new sfValidatorDate(),
      'enddate'      => new sfValidatorDate(),
      'date_created' => new sfValidatorDate(array('required' => false)),
      'date_paid'    => new sfValidatorDate(array('required' => false)),
      'base1'        => new sfValidatorNumber(array('required' => false)),
      'rate1'        => new sfValidatorNumber(array('required' => false)),
      'base2'        => new sfValidatorNumber(array('required' => false)),
      'rate2'        => new sfValidatorNumber(array('required' => false)),
      'commission'   => new sfValidatorNumber(array('required' => false)),
      'balance'      => new sfValidatorNumber(array('required' => false)),
      'notes'        => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('commission_payment[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CommissionPayment';
  }

}
