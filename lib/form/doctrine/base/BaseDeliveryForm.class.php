<?php

/**
 * Delivery form base class.
 *
 * @method Delivery getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseDeliveryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'type'            => new sfWidgetFormChoice(array('choices' => array('Incoming' => 'Incoming', 'Outgoing' => 'Outgoing', 'Conversion' => 'Conversion', 'Transfer' => 'Transfer'))),
      'code'            => new sfWidgetFormInputText(),
      'date'            => new sfWidgetFormDate(),
      'ref_class'       => new sfWidgetFormChoice(array('choices' => array('invoice' => 'invoice', 'purchase' => 'purchase', 'delivery' => 'delivery'))),
      'ref_id'          => new sfWidgetFormInputText(),
      'client_class'    => new sfWidgetFormChoice(array('choices' => array('customer' => 'customer', 'vendor' => 'vendor', 'warehouse' => 'warehouse'))),
      'client_id'       => new sfWidgetFormInputText(),
      'warehouse_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'invno'           => new sfWidgetFormInputText(),
      'pono'            => new sfWidgetFormInputText(),
      'total'           => new sfWidgetFormInputText(),
      'status'          => new sfWidgetFormChoice(array('choices' => array('Pending' => 'Pending', 'Complete' => 'Complete', 'Cancelled' => 'Cancelled'))),
      'payment_status'  => new sfWidgetFormChoice(array('choices' => array('Pending' => 'Pending', 'Paid' => 'Paid', 'Cancelled' => 'Cancelled'))),
      'delivery_method' => new sfWidgetFormChoice(array('choices' => array('Pickup' => 'Pickup', 'Delivery' => 'Delivery'))),
      'notes'           => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'type'            => new sfValidatorChoice(array('choices' => array(0 => 'Incoming', 1 => 'Outgoing', 2 => 'Conversion', 3 => 'Transfer'), 'required' => false)),
      'code'            => new sfValidatorString(array('max_length' => 20)),
      'date'            => new sfValidatorDate(),
      'ref_class'       => new sfValidatorChoice(array('choices' => array(0 => 'invoice', 1 => 'purchase', 2 => 'delivery'), 'required' => false)),
      'ref_id'          => new sfValidatorInteger(array('required' => false)),
      'client_class'    => new sfValidatorChoice(array('choices' => array(0 => 'customer', 1 => 'vendor', 2 => 'warehouse'), 'required' => false)),
      'client_id'       => new sfValidatorInteger(array('required' => false)),
      'warehouse_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'required' => false)),
      'invno'           => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'pono'            => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'total'           => new sfValidatorNumber(array('required' => false)),
      'status'          => new sfValidatorChoice(array('choices' => array(0 => 'Pending', 1 => 'Complete', 2 => 'Cancelled'), 'required' => false)),
      'payment_status'  => new sfValidatorChoice(array('choices' => array(0 => 'Pending', 1 => 'Paid', 2 => 'Cancelled'), 'required' => false)),
      'delivery_method' => new sfValidatorChoice(array('choices' => array(0 => 'Pickup', 1 => 'Delivery'), 'required' => false)),
      'notes'           => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('delivery[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Delivery';
  }

}
