<?php

/**
 * ActiveStorageBlobs form base class.
 *
 * @method ActiveStorageBlobs getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseActiveStorageBlobsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'key'          => new sfWidgetFormInputText(),
      'filename'     => new sfWidgetFormInputText(),
      'content_type' => new sfWidgetFormInputText(),
      'metadata'     => new sfWidgetFormTextarea(),
      'byte_size'    => new sfWidgetFormInputText(),
      'checksum'     => new sfWidgetFormInputText(),
      'created_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'key'          => new sfValidatorString(array('max_length' => 255)),
      'filename'     => new sfValidatorString(array('max_length' => 255)),
      'content_type' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'metadata'     => new sfValidatorString(array('required' => false)),
      'byte_size'    => new sfValidatorInteger(),
      'checksum'     => new sfValidatorString(array('max_length' => 255)),
      'created_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('active_storage_blobs[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ActiveStorageBlobs';
  }

}
