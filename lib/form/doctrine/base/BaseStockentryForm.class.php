<?php

/**
 * Stockentry form base class.
 *
 * @method Stockentry getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseStockentryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'datetime'      => new sfWidgetFormDateTime(),
      'qty'           => new sfWidgetFormInputText(),
      'qty_reported'  => new sfWidgetFormInputText(),
      'qty_missing'   => new sfWidgetFormInputText(),
      'balance'       => new sfWidgetFormInputText(),
      'stock_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Stock'), 'add_empty' => false)),
      'ref_class'     => new sfWidgetFormInputText(),
      'ref_id'        => new sfWidgetFormInputText(),
      'type'          => new sfWidgetFormChoice(array('choices' => array('Adjustment' => 'Adjustment', 'Incoming DR' => 'Incoming DR', 'Outgoing DR' => 'Outgoing DR', 'Conversion DR' => 'Conversion DR', 'Invoice' => 'Invoice', 'InvoiceReturn' => 'InvoiceReturn', 'InvoiceReplacement' => 'InvoiceReplacement', 'InvoiceConversion' => 'InvoiceConversion', 'Purchase' => 'Purchase', 'PurchaseReturn' => 'PurchaseReturn', 'PurchaseReplacement' => 'PurchaseReplacement', 'PurchaseConversion' => 'PurchaseConversion', 'Report' => 'Report', 'InvoiceDr' => 'InvoiceDr', 'PurchaseDr' => 'PurchaseDr'))),
      'is_report'     => new sfWidgetFormInputText(),
      'description'   => new sfWidgetFormInputText(),
      'created_by_id' => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'datetime'      => new sfValidatorDateTime(),
      'qty'           => new sfValidatorNumber(),
      'qty_reported'  => new sfValidatorNumber(array('required' => false)),
      'qty_missing'   => new sfValidatorNumber(array('required' => false)),
      'balance'       => new sfValidatorNumber(array('required' => false)),
      'stock_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Stock'))),
      'ref_class'     => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'ref_id'        => new sfValidatorInteger(array('required' => false)),
      'type'          => new sfValidatorChoice(array('choices' => array(0 => 'Adjustment', 1 => 'Incoming DR', 2 => 'Outgoing DR', 3 => 'Conversion DR', 4 => 'Invoice', 5 => 'InvoiceReturn', 6 => 'InvoiceReplacement', 7 => 'InvoiceConversion', 8 => 'Purchase', 9 => 'PurchaseReturn', 10 => 'PurchaseReplacement', 11 => 'PurchaseConversion', 12 => 'Report', 13 => 'InvoiceDr', 14 => 'PurchaseDr'), 'required' => false)),
      'is_report'     => new sfValidatorInteger(array('required' => false)),
      'description'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'created_by_id' => new sfValidatorInteger(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('stockentry[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Stockentry';
  }

}
