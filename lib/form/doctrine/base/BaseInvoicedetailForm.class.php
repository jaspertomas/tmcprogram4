<?php

/**
 * Invoicedetail form base class.
 *
 * @method Invoicedetail getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInvoicedetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'invoice_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => true)),
      'product_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'barcode'               => new sfWidgetFormInputText(),
      'description'           => new sfWidgetFormTextarea(),
      'qty'                   => new sfWidgetFormInputText(),
      'qty_released'          => new sfWidgetFormInputText(),
      'price'                 => new sfWidgetFormInputText(),
      'buyprice'              => new sfWidgetFormInputText(),
      'discrate'              => new sfWidgetFormInputText(),
      'discprice'             => new sfWidgetFormInputText(),
      'total'                 => new sfWidgetFormInputText(),
      'discamt'               => new sfWidgetFormInputText(),
      'unittotal'             => new sfWidgetFormInputText(),
      'is_cancelled'          => new sfWidgetFormInputText(),
      'is_discounted'         => new sfWidgetFormInputText(),
      'remaining'             => new sfWidgetFormInputText(),
      'is_profitcalculated'   => new sfWidgetFormInputText(),
      'with_commission'       => new sfWidgetFormInputText(),
      'with_commission_total' => new sfWidgetFormInputText(),
      'is_vat'                => new sfWidgetFormInputText(),
      'qty_returned'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'invoice_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'required' => false)),
      'product_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'))),
      'barcode'               => new sfValidatorString(array('max_length' => 13, 'required' => false)),
      'description'           => new sfValidatorString(array('required' => false)),
      'qty'                   => new sfValidatorNumber(array('required' => false)),
      'qty_released'          => new sfValidatorInteger(array('required' => false)),
      'price'                 => new sfValidatorNumber(array('required' => false)),
      'buyprice'              => new sfValidatorNumber(array('required' => false)),
      'discrate'              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'discprice'             => new sfValidatorNumber(array('required' => false)),
      'total'                 => new sfValidatorNumber(array('required' => false)),
      'discamt'               => new sfValidatorNumber(array('required' => false)),
      'unittotal'             => new sfValidatorNumber(array('required' => false)),
      'is_cancelled'          => new sfValidatorInteger(array('required' => false)),
      'is_discounted'         => new sfValidatorInteger(array('required' => false)),
      'remaining'             => new sfValidatorNumber(array('required' => false)),
      'is_profitcalculated'   => new sfValidatorInteger(array('required' => false)),
      'with_commission'       => new sfValidatorNumber(array('required' => false)),
      'with_commission_total' => new sfValidatorNumber(array('required' => false)),
      'is_vat'                => new sfValidatorInteger(array('required' => false)),
      'qty_returned'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('invoicedetail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Invoicedetail';
  }

}
