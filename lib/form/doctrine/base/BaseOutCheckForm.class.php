<?php

/**
 * OutCheck form base class.
 *
 * @method OutCheck getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseOutCheckForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'code'               => new sfWidgetFormInputText(),
      'check_no'           => new sfWidgetFormInputText(),
      'amount'             => new sfWidgetFormInputText(),
      'receive_date'       => new sfWidgetFormDate(),
      'check_date'         => new sfWidgetFormDate(),
      'cleared_date'       => new sfWidgetFormDate(),
      'vendor_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'), 'add_empty' => true)),
      'voucher_account_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'), 'add_empty' => true)),
      'notes'              => new sfWidgetFormTextarea(),
      'passbook_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'), 'add_empty' => false)),
      'passbook_entry_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookEntry'), 'add_empty' => true)),
      'passbook_pay_to_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookPayTo'), 'add_empty' => true)),
      'is_bank_transfer'   => new sfWidgetFormInputText(),
      'remaining'          => new sfWidgetFormInputText(),
      'print_count'        => new sfWidgetFormInputText(),
      'print_date'         => new sfWidgetFormDate(),
      'is_cancelled'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'               => new sfValidatorString(array('max_length' => 10)),
      'check_no'           => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'amount'             => new sfValidatorNumber(),
      'receive_date'       => new sfValidatorDate(array('required' => false)),
      'check_date'         => new sfValidatorDate(),
      'cleared_date'       => new sfValidatorDate(array('required' => false)),
      'vendor_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'), 'required' => false)),
      'voucher_account_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'), 'required' => false)),
      'notes'              => new sfValidatorString(array('required' => false)),
      'passbook_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'))),
      'passbook_entry_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookEntry'), 'required' => false)),
      'passbook_pay_to_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookPayTo'), 'required' => false)),
      'is_bank_transfer'   => new sfValidatorInteger(array('required' => false)),
      'remaining'          => new sfValidatorNumber(array('required' => false)),
      'print_count'        => new sfValidatorInteger(array('required' => false)),
      'print_date'         => new sfValidatorDate(array('required' => false)),
      'is_cancelled'       => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('out_check[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OutCheck';
  }

}
