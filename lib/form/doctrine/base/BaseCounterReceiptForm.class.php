<?php

/**
 * CounterReceipt form base class.
 *
 * @method CounterReceipt getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCounterReceiptForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'vendor_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'), 'add_empty' => false)),
      'code'         => new sfWidgetFormInputText(),
      'date'         => new sfWidgetFormDate(),
      'amount'       => new sfWidgetFormInputText(),
      'balance'      => new sfWidgetFormInputText(),
      'status'       => new sfWidgetFormChoice(array('choices' => array('Pending' => 'Pending', 'Paid' => 'Paid', 'Cancelled' => 'Cancelled', 'Overpaid' => 'Overpaid'))),
      'notes'        => new sfWidgetFormTextarea(),
      'is_tally'     => new sfWidgetFormInputText(),
      'is_inspected' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'vendor_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'))),
      'code'         => new sfValidatorString(array('max_length' => 10)),
      'date'         => new sfValidatorDate(),
      'amount'       => new sfValidatorNumber(array('required' => false)),
      'balance'      => new sfValidatorNumber(array('required' => false)),
      'status'       => new sfValidatorChoice(array('choices' => array(0 => 'Pending', 1 => 'Paid', 2 => 'Cancelled', 3 => 'Overpaid'), 'required' => false)),
      'notes'        => new sfValidatorString(array('required' => false)),
      'is_tally'     => new sfValidatorInteger(array('required' => false)),
      'is_inspected' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('counter_receipt[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CounterReceipt';
  }

}
