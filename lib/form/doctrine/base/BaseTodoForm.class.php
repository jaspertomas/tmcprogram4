<?php

/**
 * Todo form base class.
 *
 * @method Todo getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTodoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'description' => new sfWidgetFormTextarea(),
      'priority'    => new sfWidgetFormChoice(array('choices' => array('Low' => 'Low', 'Medium' => 'Medium', 'High' => 'High'))),
      'ref_type'    => new sfWidgetFormInputText(),
      'ref_id'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'description' => new sfValidatorString(array('required' => false)),
      'priority'    => new sfValidatorChoice(array('choices' => array(0 => 'Low', 1 => 'Medium', 2 => 'High'), 'required' => false)),
      'ref_type'    => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'ref_id'      => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('todo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Todo';
  }

}
