<?php

/**
 * TransferDetail form base class.
 *
 * @method TransferDetail getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTransferDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'transfer_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Transfer'), 'add_empty' => false)),
      'description'     => new sfWidgetFormTextarea(),
      'qty'             => new sfWidgetFormInputText(),
      'qty_transferred' => new sfWidgetFormInputText(),
      'product_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'is_cancelled'    => new sfWidgetFormInputText(),
      'remaining'       => new sfWidgetFormInputText(),
      'color'           => new sfWidgetFormChoice(array('choices' => array('red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow', 'green' => 'green', 'blue' => 'blue', 'indigo' => 'indigo', 'violet' => 'violet'))),
      'slot'            => new sfWidgetFormInputText(),
      'created_by_id'   => new sfWidgetFormInputText(),
      'updated_by_id'   => new sfWidgetFormInputText(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'transfer_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Transfer'))),
      'description'     => new sfValidatorString(array('required' => false)),
      'qty'             => new sfValidatorNumber(array('required' => false)),
      'qty_transferred' => new sfValidatorInteger(array('required' => false)),
      'product_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'))),
      'is_cancelled'    => new sfValidatorInteger(array('required' => false)),
      'remaining'       => new sfValidatorNumber(array('required' => false)),
      'color'           => new sfValidatorChoice(array('choices' => array(0 => 'red', 1 => 'orange', 2 => 'yellow', 3 => 'green', 4 => 'blue', 5 => 'indigo', 6 => 'violet'), 'required' => false)),
      'slot'            => new sfValidatorInteger(array('required' => false)),
      'created_by_id'   => new sfValidatorInteger(array('required' => false)),
      'updated_by_id'   => new sfValidatorInteger(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('transfer_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransferDetail';
  }

}
