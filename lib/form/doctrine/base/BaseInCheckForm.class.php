<?php

/**
 * InCheck form base class.
 *
 * @method InCheck getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInCheckForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'code'              => new sfWidgetFormInputText(),
      'check_no'          => new sfWidgetFormInputText(),
      'amount'            => new sfWidgetFormInputText(),
      'receive_date'      => new sfWidgetFormDate(),
      'check_date'        => new sfWidgetFormDate(),
      'deposit_date'      => new sfWidgetFormDate(),
      'cleared_date'      => new sfWidgetFormDate(),
      'bounce_date'       => new sfWidgetFormDate(),
      'customer_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => true)),
      'billee_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Biller'), 'add_empty' => true)),
      'notes'             => new sfWidgetFormTextarea(),
      'passbook_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'), 'add_empty' => false)),
      'passbook_entry_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookEntry'), 'add_empty' => true)),
      'remaining'         => new sfWidgetFormInputText(),
      'is_bank_transfer'  => new sfWidgetFormInputText(),
      'is_cancelled'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'              => new sfValidatorString(array('max_length' => 10)),
      'check_no'          => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'amount'            => new sfValidatorNumber(),
      'receive_date'      => new sfValidatorDate(),
      'check_date'        => new sfValidatorDate(),
      'deposit_date'      => new sfValidatorDate(array('required' => false)),
      'cleared_date'      => new sfValidatorDate(array('required' => false)),
      'bounce_date'       => new sfValidatorDate(array('required' => false)),
      'customer_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'required' => false)),
      'billee_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Biller'), 'required' => false)),
      'notes'             => new sfValidatorString(array('required' => false)),
      'passbook_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'))),
      'passbook_entry_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookEntry'), 'required' => false)),
      'remaining'         => new sfValidatorNumber(array('required' => false)),
      'is_bank_transfer'  => new sfValidatorInteger(array('required' => false)),
      'is_cancelled'      => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('in_check[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InCheck';
  }

}
