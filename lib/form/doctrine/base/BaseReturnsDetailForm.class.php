<?php

/**
 * ReturnsDetail form base class.
 *
 * @method ReturnsDetail getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseReturnsDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'ref_class'    => new sfWidgetFormChoice(array('choices' => array('Invoicedetail' => 'Invoicedetail', 'Purchasedetail' => 'Purchasedetail'))),
      'ref_id'       => new sfWidgetFormInputText(),
      'returns_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Returns'), 'add_empty' => false)),
      'product_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'description'  => new sfWidgetFormInputText(),
      'qty'          => new sfWidgetFormInputText(),
      'price'        => new sfWidgetFormInputText(),
      'total'        => new sfWidgetFormInputText(),
      'qty_replaced' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'ref_class'    => new sfValidatorChoice(array('choices' => array(0 => 'Invoicedetail', 1 => 'Purchasedetail'), 'required' => false)),
      'ref_id'       => new sfValidatorInteger(array('required' => false)),
      'returns_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Returns'))),
      'product_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'))),
      'description'  => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'qty'          => new sfValidatorInteger(),
      'price'        => new sfValidatorNumber(array('required' => false)),
      'total'        => new sfValidatorNumber(array('required' => false)),
      'qty_replaced' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('returns_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReturnsDetail';
  }

}
