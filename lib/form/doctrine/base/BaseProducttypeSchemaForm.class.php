<?php

/**
 * ProducttypeSchema form base class.
 *
 * @method ProducttypeSchema getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseProducttypeSchemaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'name'                       => new sfWidgetFormInputText(),
      'description'                => new sfWidgetFormInputText(),
      'product_name_format'        => new sfWidgetFormInputText(),
      'product_description_format' => new sfWidgetFormInputText(),
      'barcode_format'             => new sfWidgetFormInputText(),
      'barcode_format_2'           => new sfWidgetFormInputText(),
      'barcode_format_3'           => new sfWidgetFormInputText(),
      'barcode_template'           => new sfWidgetFormChoice(array('choices' => array('Simple 1 Column' => 'Simple 1 Column', 'Simple 2 Column' => 'Simple 2 Column', 'Simple 2 Column Green and White' => 'Simple 2 Column Green and White', 'Standard 3 Column' => 'Standard 3 Column'))),
      'max_buy_formula'            => new sfWidgetFormInputText(),
      'min_buy_formula'            => new sfWidgetFormInputText(),
      'max_sell_formula'           => new sfWidgetFormInputText(),
      'min_sell_formula'           => new sfWidgetFormInputText(),
      'base_formula'               => new sfWidgetFormInputText(),
      'speccount'                  => new sfWidgetFormInputText(),
      'spec1'                      => new sfWidgetFormInputText(),
      'spec2'                      => new sfWidgetFormInputText(),
      'spec3'                      => new sfWidgetFormInputText(),
      'spec4'                      => new sfWidgetFormInputText(),
      'spec5'                      => new sfWidgetFormInputText(),
      'spec6'                      => new sfWidgetFormInputText(),
      'spec7'                      => new sfWidgetFormInputText(),
      'spec8'                      => new sfWidgetFormInputText(),
      'spec9'                      => new sfWidgetFormInputText(),
      'notes'                      => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                       => new sfValidatorString(array('max_length' => 50)),
      'description'                => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'product_name_format'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'product_description_format' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'barcode_format'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'barcode_format_2'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'barcode_format_3'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'barcode_template'           => new sfValidatorChoice(array('choices' => array(0 => 'Simple 1 Column', 1 => 'Simple 2 Column', 2 => 'Simple 2 Column Green and White', 3 => 'Standard 3 Column'), 'required' => false)),
      'max_buy_formula'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'min_buy_formula'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'max_sell_formula'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'min_sell_formula'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'base_formula'               => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'speccount'                  => new sfValidatorInteger(array('required' => false)),
      'spec1'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec2'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec3'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec4'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec5'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec6'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec7'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec8'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec9'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'notes'                      => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('producttype_schema[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ProducttypeSchema';
  }

}
