<?php

/**
 * Product form base class.
 *
 * @method Product getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseProductForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'name'                    => new sfWidgetFormInputText(),
      'brand_id'                => new sfWidgetFormInputText(),
      'producttype_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Producttype'), 'add_empty' => false)),
      'general_producttype_id'  => new sfWidgetFormInputText(),
      'specific_producttype_id' => new sfWidgetFormInputText(),
      'productcategory'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'TJL' => 'TJL', 'Sheet' => 'Sheet', 'Supplies' => 'Supplies'))),
      'qty'                     => new sfWidgetFormInputText(),
      'minbuyprice'             => new sfWidgetFormInputText(),
      'maxbuyprice'             => new sfWidgetFormInputText(),
      'minsellprice'            => new sfWidgetFormInputText(),
      'maxsellprice'            => new sfWidgetFormInputText(),
      'baseprice'               => new sfWidgetFormInputText(),
      'description'             => new sfWidgetFormTextarea(),
      'code'                    => new sfWidgetFormInputText(),
      'spec1'                   => new sfWidgetFormInputText(),
      'spec2'                   => new sfWidgetFormInputText(),
      'spec3'                   => new sfWidgetFormInputText(),
      'spec4'                   => new sfWidgetFormInputText(),
      'spec5'                   => new sfWidgetFormInputText(),
      'spec6'                   => new sfWidgetFormInputText(),
      'spec7'                   => new sfWidgetFormInputText(),
      'spec8'                   => new sfWidgetFormInputText(),
      'spec9'                   => new sfWidgetFormInputText(),
      'is_hidden'               => new sfWidgetFormInputText(),
      'autocalcsellprice'       => new sfWidgetFormInputText(),
      'autocalcbuyprice'        => new sfWidgetFormInputText(),
      'monitored'               => new sfWidgetFormInputText(),
      'barcode'                 => new sfWidgetFormInputText(),
      'is_service'              => new sfWidgetFormInputText(),
      'is_tax'                  => new sfWidgetFormInputText(),
      'competitor_price'        => new sfWidgetFormInputText(),
      'notes'                   => new sfWidgetFormTextarea(),
      'quota'                   => new sfWidgetFormInputText(),
      'is_updated'              => new sfWidgetFormInputText(),
      'is_allow_zeroprice'      => new sfWidgetFormInputText(),
      'is_fixed_price'          => new sfWidgetFormInputText(),
      'conversion_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                    => new sfValidatorString(array('max_length' => 200)),
      'brand_id'                => new sfValidatorInteger(array('required' => false)),
      'producttype_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Producttype'))),
      'general_producttype_id'  => new sfValidatorInteger(array('required' => false)),
      'specific_producttype_id' => new sfValidatorInteger(array('required' => false)),
      'productcategory'         => new sfValidatorChoice(array('choices' => array(0 => '', 1 => 'TJL', 2 => 'Sheet', 3 => 'Supplies'))),
      'qty'                     => new sfValidatorNumber(array('required' => false)),
      'minbuyprice'             => new sfValidatorNumber(array('required' => false)),
      'maxbuyprice'             => new sfValidatorNumber(array('required' => false)),
      'minsellprice'            => new sfValidatorNumber(array('required' => false)),
      'maxsellprice'            => new sfValidatorNumber(array('required' => false)),
      'baseprice'               => new sfValidatorNumber(array('required' => false)),
      'description'             => new sfValidatorString(array('required' => false)),
      'code'                    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'spec1'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec2'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec3'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec4'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec5'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec6'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec7'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec8'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec9'                   => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'is_hidden'               => new sfValidatorInteger(array('required' => false)),
      'autocalcsellprice'       => new sfValidatorInteger(array('required' => false)),
      'autocalcbuyprice'        => new sfValidatorInteger(array('required' => false)),
      'monitored'               => new sfValidatorInteger(array('required' => false)),
      'barcode'                 => new sfValidatorString(array('max_length' => 13, 'required' => false)),
      'is_service'              => new sfValidatorInteger(array('required' => false)),
      'is_tax'                  => new sfValidatorInteger(array('required' => false)),
      'competitor_price'        => new sfValidatorNumber(array('required' => false)),
      'notes'                   => new sfValidatorString(array('required' => false)),
      'quota'                   => new sfValidatorInteger(array('required' => false)),
      'is_updated'              => new sfValidatorInteger(array('required' => false)),
      'is_allow_zeroprice'      => new sfValidatorInteger(array('required' => false)),
      'is_fixed_price'          => new sfValidatorInteger(array('required' => false)),
      'conversion_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('product[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Product';
  }

}
