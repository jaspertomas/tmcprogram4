<?php

/**
 * Deliverydetail form base class.
 *
 * @method Deliverydetail getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseDeliverydetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'ref_class'    => new sfWidgetFormChoice(array('choices' => array('invoicedetail' => 'invoicedetail', 'purchasedetail' => 'purchasedetail', 'deliveryconversion' => 'deliveryconversion'))),
      'ref_id'       => new sfWidgetFormInputText(),
      'parent_class' => new sfWidgetFormChoice(array('choices' => array('Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Delivery' => 'Delivery'))),
      'parent_id'    => new sfWidgetFormInputText(),
      'product_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'description'  => new sfWidgetFormTextarea(),
      'qty'          => new sfWidgetFormInputText(),
      'price'        => new sfWidgetFormInputText(),
      'discrate'     => new sfWidgetFormInputText(),
      'discprice'    => new sfWidgetFormInputText(),
      'total'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'ref_class'    => new sfValidatorChoice(array('choices' => array(0 => 'invoicedetail', 1 => 'purchasedetail', 2 => 'deliveryconversion'), 'required' => false)),
      'ref_id'       => new sfValidatorInteger(array('required' => false)),
      'parent_class' => new sfValidatorChoice(array('choices' => array(0 => 'Invoice', 1 => 'Purchase', 2 => 'Delivery'))),
      'parent_id'    => new sfValidatorInteger(),
      'product_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'))),
      'description'  => new sfValidatorString(array('required' => false)),
      'qty'          => new sfValidatorInteger(),
      'price'        => new sfValidatorNumber(array('required' => false)),
      'discrate'     => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'discprice'    => new sfValidatorNumber(array('required' => false)),
      'total'        => new sfValidatorNumber(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('deliverydetail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Deliverydetail';
  }

}
