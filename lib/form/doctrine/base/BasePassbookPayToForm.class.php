<?php

/**
 * PassbookPayTo form base class.
 *
 * @method PassbookPayTo getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePassbookPayToForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'name'       => new sfWidgetFormInputText(),
      'account_no' => new sfWidgetFormInputText(),
      'location'   => new sfWidgetFormInputText(),
      'currentqty' => new sfWidgetFormInputText(),
      'datetime'   => new sfWidgetFormDateTime(),
      'is_updated' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'       => new sfValidatorString(array('max_length' => 50)),
      'account_no' => new sfValidatorString(array('max_length' => 20)),
      'location'   => new sfValidatorString(array('max_length' => 30)),
      'currentqty' => new sfValidatorNumber(array('required' => false)),
      'datetime'   => new sfValidatorDateTime(),
      'is_updated' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passbook_pay_to[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassbookPayTo';
  }

}
