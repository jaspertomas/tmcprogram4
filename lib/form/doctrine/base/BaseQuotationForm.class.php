<?php

/**
 * Quotation form base class.
 *
 * @method Quotation getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseQuotationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'code'        => new sfWidgetFormInputText(),
      'date'        => new sfWidgetFormDate(),
      'customer_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => false)),
      'salesman_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Employee'), 'add_empty' => false)),
      'opening'     => new sfWidgetFormTextarea(),
      'closing'     => new sfWidgetFormTextarea(),
      'total'       => new sfWidgetFormInputText(),
      'notes'       => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'        => new sfValidatorString(array('max_length' => 10)),
      'date'        => new sfValidatorDate(),
      'customer_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'))),
      'salesman_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Employee'))),
      'opening'     => new sfValidatorString(array('required' => false)),
      'closing'     => new sfValidatorString(array('required' => false)),
      'total'       => new sfValidatorNumber(array('required' => false)),
      'notes'       => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quotation[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Quotation';
  }

}
