<?php

/**
 * Producttype form base class.
 *
 * @method Producttype getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseProducttypeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'name'                       => new sfWidgetFormInputText(),
      'description'                => new sfWidgetFormInputText(),
      'parent_id'                  => new sfWidgetFormInputText(),
      'producttype_schema_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ProducttypeSchema'), 'add_empty' => false)),
      'priority'                   => new sfWidgetFormInputText(),
      'product_name_format'        => new sfWidgetFormInputText(),
      'product_description_format' => new sfWidgetFormInputText(),
      'barcode_format'             => new sfWidgetFormInputText(),
      'barcode_format_2'           => new sfWidgetFormInputText(),
      'barcode_format_3'           => new sfWidgetFormInputText(),
      'max_buy_formula'            => new sfWidgetFormInputText(),
      'min_buy_formula'            => new sfWidgetFormInputText(),
      'max_sell_formula'           => new sfWidgetFormInputText(),
      'min_sell_formula'           => new sfWidgetFormInputText(),
      'base_formula'               => new sfWidgetFormInputText(),
      'speccount'                  => new sfWidgetFormInputText(),
      'spec1'                      => new sfWidgetFormInputText(),
      'spec2'                      => new sfWidgetFormInputText(),
      'spec3'                      => new sfWidgetFormInputText(),
      'spec4'                      => new sfWidgetFormInputText(),
      'spec5'                      => new sfWidgetFormInputText(),
      'spec6'                      => new sfWidgetFormInputText(),
      'spec7'                      => new sfWidgetFormInputText(),
      'spec8'                      => new sfWidgetFormInputText(),
      'spec9'                      => new sfWidgetFormInputText(),
      'path_ids'                   => new sfWidgetFormInputText(),
      'path'                       => new sfWidgetFormInputText(),
      'status'                     => new sfWidgetFormChoice(array('choices' => array('OK' => 'OK'))),
      'notes'                      => new sfWidgetFormTextarea(),
      'is_updated'                 => new sfWidgetFormInputText(),
      'is_hidden'                  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'description'                => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'parent_id'                  => new sfValidatorInteger(array('required' => false)),
      'producttype_schema_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ProducttypeSchema'), 'required' => false)),
      'priority'                   => new sfValidatorInteger(array('required' => false)),
      'product_name_format'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'product_description_format' => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'barcode_format'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'barcode_format_2'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'barcode_format_3'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'max_buy_formula'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'min_buy_formula'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'max_sell_formula'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'min_sell_formula'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'base_formula'               => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'speccount'                  => new sfValidatorInteger(array('required' => false)),
      'spec1'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec2'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec3'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec4'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec5'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec6'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec7'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec8'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'spec9'                      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'path_ids'                   => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'path'                       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'status'                     => new sfValidatorChoice(array('choices' => array(0 => 'OK'), 'required' => false)),
      'notes'                      => new sfValidatorString(array('required' => false)),
      'is_updated'                 => new sfValidatorInteger(array('required' => false)),
      'is_hidden'                  => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('producttype[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Producttype';
  }

}
