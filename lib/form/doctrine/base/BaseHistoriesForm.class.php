<?php

/**
 * Histories form base class.
 *
 * @method Histories getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseHistoriesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'historable_id'   => new sfWidgetFormInputText(),
      'historable_type' => new sfWidgetFormInputText(),
      'description'     => new sfWidgetFormInputText(),
      'ref_type1'       => new sfWidgetFormInputText(),
      'ref_id1'         => new sfWidgetFormInputText(),
      'ref_type2'       => new sfWidgetFormInputText(),
      'ref_id2'         => new sfWidgetFormInputText(),
      'client_type1'    => new sfWidgetFormInputText(),
      'client_id1'      => new sfWidgetFormInputText(),
      'client_type2'    => new sfWidgetFormInputText(),
      'client_id2'      => new sfWidgetFormInputText(),
      'meta'            => new sfWidgetFormInputText(),
      'created_by_id'   => new sfWidgetFormInputText(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'historable_id'   => new sfValidatorInteger(array('required' => false)),
      'historable_type' => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'description'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ref_type1'       => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'ref_id1'         => new sfValidatorInteger(array('required' => false)),
      'ref_type2'       => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'ref_id2'         => new sfValidatorInteger(array('required' => false)),
      'client_type1'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'client_id1'      => new sfValidatorInteger(array('required' => false)),
      'client_type2'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'client_id2'      => new sfValidatorInteger(array('required' => false)),
      'meta'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_by_id'   => new sfValidatorInteger(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('histories[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Histories';
  }

}
