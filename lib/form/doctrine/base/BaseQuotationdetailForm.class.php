<?php

/**
 * Quotationdetail form base class.
 *
 * @method Quotationdetail getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseQuotationdetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'quotation_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Quotation'), 'add_empty' => false)),
      'product_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'description'   => new sfWidgetFormInputText(),
      'qty'           => new sfWidgetFormInputText(),
      'price'         => new sfWidgetFormInputText(),
      'is_discounted' => new sfWidgetFormInputText(),
      'unittotal'     => new sfWidgetFormInputText(),
      'discrate'      => new sfWidgetFormInputText(),
      'discamt'       => new sfWidgetFormInputText(),
      'total'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'quotation_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Quotation'))),
      'product_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'))),
      'description'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'qty'           => new sfValidatorInteger(),
      'price'         => new sfValidatorNumber(array('required' => false)),
      'is_discounted' => new sfValidatorInteger(array('required' => false)),
      'unittotal'     => new sfValidatorInteger(array('required' => false)),
      'discrate'      => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'discamt'       => new sfValidatorNumber(array('required' => false)),
      'total'         => new sfValidatorNumber(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('quotationdetail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Quotationdetail';
  }

}
