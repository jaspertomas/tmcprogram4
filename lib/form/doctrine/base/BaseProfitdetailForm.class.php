<?php

/**
 * Profitdetail form base class.
 *
 * @method Profitdetail getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseProfitdetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'product_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'invoice_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => false)),
      'purchase_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'), 'add_empty' => false)),
      'invoicedetail_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoicedetail'), 'add_empty' => false)),
      'purchasedetail_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchasedetail'), 'add_empty' => false)),
      'invoicedate'            => new sfWidgetFormDate(),
      'purchasedate'           => new sfWidgetFormDate(),
      'buyprice'               => new sfWidgetFormInputText(),
      'sellprice'              => new sfWidgetFormInputText(),
      'profitperunit'          => new sfWidgetFormInputText(),
      'profitrate'             => new sfWidgetFormInputText(),
      'qty'                    => new sfWidgetFormInputText(),
      'profit'                 => new sfWidgetFormInputText(),
      'consignment_payment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ConsignmentPayment'), 'add_empty' => true)),
      'color'                  => new sfWidgetFormChoice(array('choices' => array('red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow', 'green' => 'green', 'blue' => 'blue', 'indigo' => 'indigo', 'violet' => 'violet'))),
      'slot'                   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'product_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'))),
      'invoice_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'))),
      'purchase_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'))),
      'invoicedetail_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Invoicedetail'))),
      'purchasedetail_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Purchasedetail'))),
      'invoicedate'            => new sfValidatorDate(),
      'purchasedate'           => new sfValidatorDate(),
      'buyprice'               => new sfValidatorNumber(),
      'sellprice'              => new sfValidatorNumber(),
      'profitperunit'          => new sfValidatorNumber(),
      'profitrate'             => new sfValidatorNumber(),
      'qty'                    => new sfValidatorNumber(),
      'profit'                 => new sfValidatorNumber(),
      'consignment_payment_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ConsignmentPayment'), 'required' => false)),
      'color'                  => new sfValidatorChoice(array('choices' => array(0 => 'red', 1 => 'orange', 2 => 'yellow', 3 => 'green', 4 => 'blue', 5 => 'indigo', 6 => 'violet'), 'required' => false)),
      'slot'                   => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('profitdetail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Profitdetail';
  }

}
