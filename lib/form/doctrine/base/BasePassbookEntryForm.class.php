<?php

/**
 * PassbookEntry form base class.
 *
 * @method PassbookEntry getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePassbookEntryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'deposit'       => new sfWidgetFormInputText(),
      'withdrawal'    => new sfWidgetFormInputText(),
      'balance'       => new sfWidgetFormInputText(),
      'trans_type'    => new sfWidgetFormChoice(array('choices' => array('CASH/CHECK DEPOSIT' => 'CASH/CHECK DEPOSIT', 'CHECK ENCASHMENT' => 'CHECK ENCASHMENT', 'CREDIT MEMO' => 'CREDIT MEMO', 'DEBIT MEMO' => 'DEBIT MEMO', 'IMIN-CLRNG-BAT' => 'IMIN-CLRNG-BAT', 'IN-HOUSE CHECK DEPOSIT' => 'IN-HOUSE CHECK DEPOSIT', 'INWARD CHECK' => 'INWARD CHECK', 'LOAN PAYMENT' => 'LOAN PAYMENT', 'LOCAL CHECK DEPOSIT' => 'LOCAL CHECK DEPOSIT', 'REPORT' => 'REPORT'))),
      'passbook_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'), 'add_empty' => false)),
      'client_type'   => new sfWidgetFormInputText(),
      'client_name'   => new sfWidgetFormInputText(),
      'meta'          => new sfWidgetFormInputText(),
      'description'   => new sfWidgetFormInputText(),
      'json'          => new sfWidgetFormTextarea(),
      'datetime'      => new sfWidgetFormDateTime(),
      'qty'           => new sfWidgetFormInputText(),
      'qty_reported'  => new sfWidgetFormInputText(),
      'qty_missing'   => new sfWidgetFormInputText(),
      'ref_class'     => new sfWidgetFormInputText(),
      'ref_id'        => new sfWidgetFormInputText(),
      'type'          => new sfWidgetFormChoice(array('choices' => array('InCheck' => 'InCheck', 'OutCheck' => 'OutCheck', 'Report' => 'Report'))),
      'is_report'     => new sfWidgetFormInputText(),
      'created_by_id' => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'deposit'       => new sfValidatorNumber(array('required' => false)),
      'withdrawal'    => new sfValidatorNumber(array('required' => false)),
      'balance'       => new sfValidatorNumber(array('required' => false)),
      'trans_type'    => new sfValidatorChoice(array('choices' => array(0 => 'CASH/CHECK DEPOSIT', 1 => 'CHECK ENCASHMENT', 2 => 'CREDIT MEMO', 3 => 'DEBIT MEMO', 4 => 'IMIN-CLRNG-BAT', 5 => 'IN-HOUSE CHECK DEPOSIT', 6 => 'INWARD CHECK', 7 => 'LOAN PAYMENT', 8 => 'LOCAL CHECK DEPOSIT', 9 => 'REPORT'))),
      'passbook_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'))),
      'client_type'   => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'client_name'   => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'meta'          => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'description'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'json'          => new sfValidatorString(array('required' => false)),
      'datetime'      => new sfValidatorDateTime(array('required' => false)),
      'qty'           => new sfValidatorNumber(),
      'qty_reported'  => new sfValidatorNumber(array('required' => false)),
      'qty_missing'   => new sfValidatorNumber(array('required' => false)),
      'ref_class'     => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'ref_id'        => new sfValidatorInteger(array('required' => false)),
      'type'          => new sfValidatorChoice(array('choices' => array(0 => 'InCheck', 1 => 'OutCheck', 2 => 'Report'), 'required' => false)),
      'is_report'     => new sfValidatorInteger(array('required' => false)),
      'created_by_id' => new sfValidatorInteger(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passbook_entry[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassbookEntry';
  }

}
