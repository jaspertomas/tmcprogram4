<?php

/**
 * ArInternalMetadata form base class.
 *
 * @method ArInternalMetadata getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseArInternalMetadataForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'key'        => new sfWidgetFormInputHidden(),
      'value'      => new sfWidgetFormInputText(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'key'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('key')), 'empty_value' => $this->getObject()->get('key'), 'required' => false)),
      'value'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at' => new sfValidatorDateTime(),
      'updated_at' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ar_internal_metadata[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ArInternalMetadata';
  }

}
