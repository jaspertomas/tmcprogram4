<?php

/**
 * VoucherSubaccount form base class.
 *
 * @method VoucherSubaccount getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVoucherSubaccountForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'name'                  => new sfWidgetFormInputText(),
      'fullname'              => new sfWidgetFormInputText(),
      'voucher_account_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'), 'add_empty' => false)),
      'parent_id'             => new sfWidgetFormInputText(),
      'redirect_id'           => new sfWidgetFormInputText(),
      'depth'                 => new sfWidgetFormInputText(),
      'voucher_type_id'       => new sfWidgetFormInputText(),
      'voucher_allocation_id' => new sfWidgetFormInputText(),
      'passbook_id'           => new sfWidgetFormInputText(),
      'payee'                 => new sfWidgetFormInputText(),
      'amount'                => new sfWidgetFormInputText(),
      'particulars'           => new sfWidgetFormTextarea(),
      'notes'                 => new sfWidgetFormTextarea(),
      'is_pay_to_self'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                  => new sfValidatorString(array('max_length' => 100)),
      'fullname'              => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'voucher_account_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'))),
      'parent_id'             => new sfValidatorInteger(array('required' => false)),
      'redirect_id'           => new sfValidatorInteger(array('required' => false)),
      'depth'                 => new sfValidatorInteger(array('required' => false)),
      'voucher_type_id'       => new sfValidatorInteger(array('required' => false)),
      'voucher_allocation_id' => new sfValidatorInteger(array('required' => false)),
      'passbook_id'           => new sfValidatorInteger(array('required' => false)),
      'payee'                 => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'amount'                => new sfValidatorNumber(array('required' => false)),
      'particulars'           => new sfValidatorString(array('required' => false)),
      'notes'                 => new sfValidatorString(array('required' => false)),
      'is_pay_to_self'        => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('voucher_subaccount[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VoucherSubaccount';
  }

}
