<?php

/**
 * PurchaseDr form base class.
 *
 * @method PurchaseDr getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePurchaseDrForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'code'            => new sfWidgetFormInputText(),
      'description'     => new sfWidgetFormInputText(),
      'datetime'        => new sfWidgetFormDateTime(),
      'purchase_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'), 'add_empty' => false)),
      'vendor_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'), 'add_empty' => false)),
      'warehouse_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => false)),
      'qty_for_receive' => new sfWidgetFormInputText(),
      'qty_for_return'  => new sfWidgetFormInputText(),
      'notes'           => new sfWidgetFormTextarea(),
      'is_return'       => new sfWidgetFormInputText(),
      'is_received'     => new sfWidgetFormInputText(),
      'is_cancelled'    => new sfWidgetFormInputText(),
      'created_by_id'   => new sfWidgetFormInputText(),
      'updated_by_id'   => new sfWidgetFormInputText(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'            => new sfValidatorString(array('max_length' => 20)),
      'description'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'datetime'        => new sfValidatorDateTime(),
      'purchase_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'))),
      'vendor_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'))),
      'warehouse_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'))),
      'qty_for_receive' => new sfValidatorInteger(array('required' => false)),
      'qty_for_return'  => new sfValidatorInteger(array('required' => false)),
      'notes'           => new sfValidatorString(array('required' => false)),
      'is_return'       => new sfValidatorInteger(array('required' => false)),
      'is_received'     => new sfValidatorInteger(array('required' => false)),
      'is_cancelled'    => new sfValidatorInteger(array('required' => false)),
      'created_by_id'   => new sfValidatorInteger(array('required' => false)),
      'updated_by_id'   => new sfValidatorInteger(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('purchase_dr[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PurchaseDr';
  }

}
