<?php

/**
 * JobOrderDrDetail form base class.
 *
 * @method JobOrderDrDetail getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseJobOrderDrDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'datetime'            => new sfWidgetFormDateTime(),
      'job_order_dr_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrderDr'), 'add_empty' => false)),
      'job_order_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrder'), 'add_empty' => false)),
      'job_order_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrderDetail'), 'add_empty' => false)),
      'product_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'warehouse_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => false)),
      'qty'                 => new sfWidgetFormInputText(),
      'is_produced'         => new sfWidgetFormInputText(),
      'is_cancelled'        => new sfWidgetFormInputText(),
      'stock_entry_id'      => new sfWidgetFormInputText(),
      'created_by_id'       => new sfWidgetFormInputText(),
      'updated_by_id'       => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'datetime'            => new sfValidatorDateTime(),
      'job_order_dr_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrderDr'))),
      'job_order_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrder'))),
      'job_order_detail_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrderDetail'))),
      'product_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'))),
      'warehouse_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'))),
      'qty'                 => new sfValidatorInteger(),
      'is_produced'         => new sfValidatorInteger(array('required' => false)),
      'is_cancelled'        => new sfValidatorInteger(array('required' => false)),
      'stock_entry_id'      => new sfValidatorInteger(array('required' => false)),
      'created_by_id'       => new sfValidatorInteger(),
      'updated_by_id'       => new sfValidatorInteger(),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('job_order_dr_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'JobOrderDrDetail';
  }

}
