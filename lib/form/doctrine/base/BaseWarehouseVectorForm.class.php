<?php

/**
 * WarehouseVector form base class.
 *
 * @method WarehouseVector getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseWarehouseVectorForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'name'              => new sfWidgetFormInputText(),
      'from_warehouse_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => false)),
      'to_warehouse_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse_2'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'              => new sfValidatorString(array('max_length' => 30)),
      'from_warehouse_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'))),
      'to_warehouse_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse_2'))),
    ));

    $this->widgetSchema->setNameFormat('warehouse_vector[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WarehouseVector';
  }

}
