<?php

/**
 * MaxDiscountLevel form base class.
 *
 * @method MaxDiscountLevel getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMaxDiscountLevelForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'name'         => new sfWidgetFormInputText(),
      'description'  => new sfWidgetFormTextarea(),
      'priority'     => new sfWidgetFormInputText(),
      'less'         => new sfWidgetFormInputText(),
      'is_wholesale' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'         => new sfValidatorString(array('max_length' => 30)),
      'description'  => new sfValidatorString(array('required' => false)),
      'priority'     => new sfValidatorInteger(array('required' => false)),
      'less'         => new sfValidatorString(array('max_length' => 30)),
      'is_wholesale' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('max_discount_level[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MaxDiscountLevel';
  }

}
