<?php

/**
 * InvoiceReturn form base class.
 *
 * @method InvoiceReturn getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInvoiceReturnForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'return_type'   => new sfWidgetFormChoice(array('choices' => array('Refund' => 'Refund', 'Replace' => 'Replace'))),
      'invoice_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => false)),
      'customer_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => false)),
      'code'          => new sfWidgetFormInputText(),
      'date'          => new sfWidgetFormDate(),
      'reason'        => new sfWidgetFormTextarea(),
      'notes'         => new sfWidgetFormTextarea(),
      'status'        => new sfWidgetFormChoice(array('choices' => array('Pending' => 'Pending', 'Complete' => 'Complete'))),
      'is_cancelled'  => new sfWidgetFormInputText(),
      'created_by_id' => new sfWidgetFormInputText(),
      'updated_by_id' => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'return_type'   => new sfValidatorChoice(array('choices' => array(0 => 'Refund', 1 => 'Replace'))),
      'invoice_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'))),
      'customer_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'))),
      'code'          => new sfValidatorString(array('max_length' => 15)),
      'date'          => new sfValidatorDate(),
      'reason'        => new sfValidatorString(array('required' => false)),
      'notes'         => new sfValidatorString(array('required' => false)),
      'status'        => new sfValidatorChoice(array('choices' => array(0 => 'Pending', 1 => 'Complete'), 'required' => false)),
      'is_cancelled'  => new sfValidatorInteger(array('required' => false)),
      'created_by_id' => new sfValidatorInteger(array('required' => false)),
      'updated_by_id' => new sfValidatorInteger(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('invoice_return[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvoiceReturn';
  }

}
