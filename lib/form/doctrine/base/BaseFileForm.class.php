<?php

/**
 * File form base class.
 *
 * @method File getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'parent_class'   => new sfWidgetFormInputText(),
      'parent_id'      => new sfWidgetFormInputText(),
      'title'          => new sfWidgetFormInputText(),
      'description'    => new sfWidgetFormTextarea(),
      'filename'       => new sfWidgetFormInputText(),
      'file'           => new sfWidgetFormInputText(),
      'keywords'       => new sfWidgetFormTextarea(),
      'filesize'       => new sfWidgetFormInputText(),
      'filetype'       => new sfWidgetFormInputText(),
      'uploadlocation' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'parent_class'   => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'parent_id'      => new sfValidatorInteger(array('required' => false)),
      'title'          => new sfValidatorString(array('max_length' => 100)),
      'description'    => new sfValidatorString(array('required' => false)),
      'filename'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'file'           => new sfValidatorString(array('max_length' => 100)),
      'keywords'       => new sfValidatorString(array('required' => false)),
      'filesize'       => new sfValidatorInteger(array('required' => false)),
      'filetype'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'uploadlocation' => new sfValidatorString(array('max_length' => 10, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('file[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'File';
  }

}
