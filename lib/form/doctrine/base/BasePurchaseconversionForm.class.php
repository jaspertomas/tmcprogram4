<?php

/**
 * Purchaseconversion form base class.
 *
 * @method Purchaseconversion getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePurchaseconversionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'purchase_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'), 'add_empty' => false)),
      'conversion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'), 'add_empty' => false)),
      'qty'           => new sfWidgetFormInputText(),
      'description'   => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'purchase_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'))),
      'conversion_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'))),
      'qty'           => new sfValidatorInteger(),
      'description'   => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('purchaseconversion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Purchaseconversion';
  }

}
