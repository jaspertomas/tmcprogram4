<?php

/**
 * Deliveryconversion form base class.
 *
 * @method Deliveryconversion getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseDeliveryconversionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'parent_class'  => new sfWidgetFormChoice(array('choices' => array('Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Delivery' => 'Delivery'))),
      'parent_id'     => new sfWidgetFormInputText(),
      'ref_class'     => new sfWidgetFormInputText(),
      'ref_id'        => new sfWidgetFormInputText(),
      'conversion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'), 'add_empty' => false)),
      'qty'           => new sfWidgetFormInputText(),
      'description'   => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'parent_class'  => new sfValidatorChoice(array('choices' => array(0 => 'Invoice', 1 => 'Purchase', 2 => 'Delivery'))),
      'parent_id'     => new sfValidatorInteger(),
      'ref_class'     => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'ref_id'        => new sfValidatorInteger(array('required' => false)),
      'conversion_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'))),
      'qty'           => new sfValidatorInteger(),
      'description'   => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('deliveryconversion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Deliveryconversion';
  }

}
