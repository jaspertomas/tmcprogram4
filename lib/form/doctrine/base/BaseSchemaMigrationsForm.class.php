<?php

/**
 * SchemaMigrations form base class.
 *
 * @method SchemaMigrations getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSchemaMigrationsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'version' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'version' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('version')), 'empty_value' => $this->getObject()->get('version'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('schema_migrations[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SchemaMigrations';
  }

}
