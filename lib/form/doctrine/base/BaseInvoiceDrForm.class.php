<?php

/**
 * InvoiceDr form base class.
 *
 * @method InvoiceDr getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInvoiceDrForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'code'              => new sfWidgetFormInputText(),
      'description'       => new sfWidgetFormInputText(),
      'datetime'          => new sfWidgetFormDateTime(),
      'invoice_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => false)),
      'invoice_return_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('InvoiceReturn'), 'add_empty' => true)),
      'customer_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => false)),
      'warehouse_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => false)),
      'qty_for_release'   => new sfWidgetFormInputText(),
      'qty_for_return'    => new sfWidgetFormInputText(),
      'notes'             => new sfWidgetFormTextarea(),
      'is_return'         => new sfWidgetFormInputText(),
      'is_released'       => new sfWidgetFormInputText(),
      'is_cancelled'      => new sfWidgetFormInputText(),
      'created_by_id'     => new sfWidgetFormInputText(),
      'updated_by_id'     => new sfWidgetFormInputText(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'              => new sfValidatorString(array('max_length' => 20)),
      'description'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'datetime'          => new sfValidatorDateTime(),
      'invoice_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'))),
      'invoice_return_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('InvoiceReturn'), 'required' => false)),
      'customer_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'))),
      'warehouse_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'))),
      'qty_for_release'   => new sfValidatorInteger(array('required' => false)),
      'qty_for_return'    => new sfValidatorInteger(array('required' => false)),
      'notes'             => new sfValidatorString(array('required' => false)),
      'is_return'         => new sfValidatorInteger(array('required' => false)),
      'is_released'       => new sfValidatorInteger(array('required' => false)),
      'is_cancelled'      => new sfValidatorInteger(array('required' => false)),
      'created_by_id'     => new sfValidatorInteger(array('required' => false)),
      'updated_by_id'     => new sfValidatorInteger(array('required' => false)),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('invoice_dr[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvoiceDr';
  }

}
