<?php

/**
 * Vendor form base class.
 *
 * @method Vendor getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVendorForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'name'             => new sfWidgetFormInputText(),
      'discrate'         => new sfWidgetFormInputText(),
      'addr1'            => new sfWidgetFormInputText(),
      'addr2'            => new sfWidgetFormInputText(),
      'addr3'            => new sfWidgetFormInputText(),
      'vtype'            => new sfWidgetFormInputText(),
      'cont1'            => new sfWidgetFormInputText(),
      'cont2'            => new sfWidgetFormInputText(),
      'phone1'           => new sfWidgetFormTextarea(),
      'bank_acct'        => new sfWidgetFormInputText(),
      'email'            => new sfWidgetFormInputText(),
      'notes'            => new sfWidgetFormTextarea(),
      'is_owned'         => new sfWidgetFormInputText(),
      'tin_no'           => new sfWidgetFormInputText(),
      'terms'            => new sfWidgetFormInputText(),
      'collection_notes' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'             => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'discrate'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'addr1'            => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'addr2'            => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'addr3'            => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'vtype'            => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'cont1'            => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'cont2'            => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'phone1'           => new sfValidatorString(array('max_length' => 300, 'required' => false)),
      'bank_acct'        => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'email'            => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'notes'            => new sfValidatorString(array('required' => false)),
      'is_owned'         => new sfValidatorInteger(),
      'tin_no'           => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'terms'            => new sfValidatorInteger(array('required' => false)),
      'collection_notes' => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('vendor[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Vendor';
  }

}
