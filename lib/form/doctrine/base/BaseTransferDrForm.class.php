<?php

/**
 * TransferDr form base class.
 *
 * @method TransferDr getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTransferDrForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'code'                => new sfWidgetFormInputText(),
      'description'         => new sfWidgetFormTextarea(),
      'date'                => new sfWidgetFormDate(),
      'warehouse_vector_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('WarehouseVector'), 'add_empty' => true)),
      'warehouse_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'to_warehouse_id'     => new sfWidgetFormInputText(),
      'notes'               => new sfWidgetFormTextarea(),
      'is_transferred'      => new sfWidgetFormInputText(),
      'is_cancelled'        => new sfWidgetFormInputText(),
      'created_by_id'       => new sfWidgetFormInputText(),
      'updated_by_id'       => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'                => new sfValidatorString(array('max_length' => 20)),
      'description'         => new sfValidatorString(array('required' => false)),
      'date'                => new sfValidatorDate(),
      'warehouse_vector_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('WarehouseVector'), 'required' => false)),
      'warehouse_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'required' => false)),
      'to_warehouse_id'     => new sfValidatorInteger(array('required' => false)),
      'notes'               => new sfValidatorString(array('required' => false)),
      'is_transferred'      => new sfValidatorInteger(array('required' => false)),
      'is_cancelled'        => new sfValidatorInteger(array('required' => false)),
      'created_by_id'       => new sfValidatorInteger(array('required' => false)),
      'updated_by_id'       => new sfValidatorInteger(array('required' => false)),
      'created_at'          => new sfValidatorDateTime(array('required' => false)),
      'updated_at'          => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('transfer_dr[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransferDr';
  }

}
