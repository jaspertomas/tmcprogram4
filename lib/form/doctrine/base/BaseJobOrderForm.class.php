<?php

/**
 * JobOrder form base class.
 *
 * @method JobOrder getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseJobOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'code'              => new sfWidgetFormInputText(),
      'notes'             => new sfWidgetFormTextarea(),
      'date'              => new sfWidgetFormDate(),
      'date_completed'    => new sfWidgetFormDate(),
      'is_temporary'      => new sfWidgetFormInputText(),
      'is_completed'      => new sfWidgetFormInputText(),
      'is_cancelled'      => new sfWidgetFormInputText(),
      'was_closed'        => new sfWidgetFormInputText(),
      'production_status' => new sfWidgetFormChoice(array('choices' => array('Pending' => 'Pending', 'Partial' => 'Partial', 'Complete' => 'Complete'))),
      'meta'              => new sfWidgetFormTextarea(),
      'created_by_id'     => new sfWidgetFormInputText(),
      'updated_by_id'     => new sfWidgetFormInputText(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'              => new sfValidatorString(array('max_length' => 20)),
      'notes'             => new sfValidatorString(array('required' => false)),
      'date'              => new sfValidatorDate(),
      'date_completed'    => new sfValidatorDate(array('required' => false)),
      'is_temporary'      => new sfValidatorInteger(array('required' => false)),
      'is_completed'      => new sfValidatorInteger(array('required' => false)),
      'is_cancelled'      => new sfValidatorInteger(array('required' => false)),
      'was_closed'        => new sfValidatorInteger(array('required' => false)),
      'production_status' => new sfValidatorChoice(array('choices' => array(0 => 'Pending', 1 => 'Partial', 2 => 'Complete'), 'required' => false)),
      'meta'              => new sfValidatorString(array('required' => false)),
      'created_by_id'     => new sfValidatorInteger(array('required' => false)),
      'updated_by_id'     => new sfValidatorInteger(array('required' => false)),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('job_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'JobOrder';
  }

}
