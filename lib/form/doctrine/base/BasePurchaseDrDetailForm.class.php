<?php

/**
 * PurchaseDrDetail form base class.
 *
 * @method PurchaseDrDetail getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePurchaseDrDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'datetime'          => new sfWidgetFormDateTime(),
      'purchase_dr_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PurchaseDr'), 'add_empty' => false)),
      'purchase_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'), 'add_empty' => false)),
      'purchasedetail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchasedetail'), 'add_empty' => false)),
      'product_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'warehouse_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => false)),
      'vendor_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'), 'add_empty' => false)),
      'qty'               => new sfWidgetFormInputText(),
      'is_return'         => new sfWidgetFormInputText(),
      'is_received'       => new sfWidgetFormInputText(),
      'is_cancelled'      => new sfWidgetFormInputText(),
      'stock_entry_id'    => new sfWidgetFormInputText(),
      'created_by_id'     => new sfWidgetFormInputText(),
      'updated_by_id'     => new sfWidgetFormInputText(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'datetime'          => new sfValidatorDateTime(),
      'purchase_dr_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PurchaseDr'))),
      'purchase_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'))),
      'purchasedetail_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Purchasedetail'))),
      'product_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'))),
      'warehouse_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'))),
      'vendor_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'))),
      'qty'               => new sfValidatorInteger(),
      'is_return'         => new sfValidatorInteger(array('required' => false)),
      'is_received'       => new sfValidatorInteger(array('required' => false)),
      'is_cancelled'      => new sfValidatorInteger(array('required' => false)),
      'stock_entry_id'    => new sfValidatorInteger(array('required' => false)),
      'created_by_id'     => new sfValidatorInteger(),
      'updated_by_id'     => new sfValidatorInteger(),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('purchase_dr_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PurchaseDrDetail';
  }

}
