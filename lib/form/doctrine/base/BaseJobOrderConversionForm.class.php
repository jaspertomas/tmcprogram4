<?php

/**
 * JobOrderConversion form base class.
 *
 * @method JobOrderConversion getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseJobOrderConversionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'job_order_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrder'), 'add_empty' => false)),
      'conversion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'), 'add_empty' => false)),
      'qty'           => new sfWidgetFormInputText(),
      'description'   => new sfWidgetFormTextarea(),
      'created_by_id' => new sfWidgetFormInputText(),
      'updated_by_id' => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'job_order_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrder'))),
      'conversion_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'))),
      'qty'           => new sfValidatorInteger(),
      'description'   => new sfValidatorString(array('required' => false)),
      'created_by_id' => new sfValidatorInteger(array('required' => false)),
      'updated_by_id' => new sfValidatorInteger(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('job_order_conversion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'JobOrderConversion';
  }

}
