<?php

/**
 * Replacement form base class.
 *
 * @method Replacement getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseReplacementForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'type'         => new sfWidgetFormChoice(array('choices' => array('Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Customer' => 'Customer', 'Vendor' => 'Vendor', '' => ''))),
      'code'         => new sfWidgetFormInputText(),
      'date'         => new sfWidgetFormDate(),
      'ref_class'    => new sfWidgetFormChoice(array('choices' => array('Invoice' => 'Invoice', 'Purchase' => 'Purchase', '' => ''))),
      'ref_id'       => new sfWidgetFormInputText(),
      'client_class' => new sfWidgetFormChoice(array('choices' => array('Customer' => 'Customer', 'Vendor' => 'Vendor'))),
      'client_id'    => new sfWidgetFormInputText(),
      'returns_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Returns'), 'add_empty' => true)),
      'warehouse_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'invno'        => new sfWidgetFormInputText(),
      'pono'         => new sfWidgetFormInputText(),
      'total'        => new sfWidgetFormInputText(),
      'status'       => new sfWidgetFormChoice(array('choices' => array('Pending' => 'Pending', 'Complete' => 'Complete', 'Cancelled' => 'Cancelled'))),
      'notes'        => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'type'         => new sfValidatorChoice(array('choices' => array(0 => 'Invoice', 1 => 'Purchase', 2 => 'Customer', 3 => 'Vendor', 4 => ''), 'required' => false)),
      'code'         => new sfValidatorString(array('max_length' => 20)),
      'date'         => new sfValidatorDate(),
      'ref_class'    => new sfValidatorChoice(array('choices' => array(0 => 'Invoice', 1 => 'Purchase', 2 => ''), 'required' => false)),
      'ref_id'       => new sfValidatorInteger(array('required' => false)),
      'client_class' => new sfValidatorChoice(array('choices' => array(0 => 'Customer', 1 => 'Vendor'), 'required' => false)),
      'client_id'    => new sfValidatorInteger(array('required' => false)),
      'returns_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Returns'), 'required' => false)),
      'warehouse_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'required' => false)),
      'invno'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'pono'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'total'        => new sfValidatorNumber(array('required' => false)),
      'status'       => new sfValidatorChoice(array('choices' => array(0 => 'Pending', 1 => 'Complete', 2 => 'Cancelled'), 'required' => false)),
      'notes'        => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('replacement[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Replacement';
  }

}
