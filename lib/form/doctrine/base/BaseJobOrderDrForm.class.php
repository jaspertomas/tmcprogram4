<?php

/**
 * JobOrderDr form base class.
 *
 * @method JobOrderDr getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseJobOrderDrForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'code'           => new sfWidgetFormInputText(),
      'description'    => new sfWidgetFormInputText(),
      'datetime'       => new sfWidgetFormDateTime(),
      'job_order_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrder'), 'add_empty' => false)),
      'warehouse_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => false)),
      'qty_to_produce' => new sfWidgetFormInputText(),
      'qty_to_consume' => new sfWidgetFormInputText(),
      'notes'          => new sfWidgetFormTextarea(),
      'is_produced'    => new sfWidgetFormInputText(),
      'is_cancelled'   => new sfWidgetFormInputText(),
      'created_by_id'  => new sfWidgetFormInputText(),
      'updated_by_id'  => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'           => new sfValidatorString(array('max_length' => 20)),
      'description'    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'datetime'       => new sfValidatorDateTime(),
      'job_order_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrder'))),
      'warehouse_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'))),
      'qty_to_produce' => new sfValidatorInteger(array('required' => false)),
      'qty_to_consume' => new sfValidatorInteger(array('required' => false)),
      'notes'          => new sfValidatorString(array('required' => false)),
      'is_produced'    => new sfValidatorInteger(array('required' => false)),
      'is_cancelled'   => new sfValidatorInteger(array('required' => false)),
      'created_by_id'  => new sfValidatorInteger(array('required' => false)),
      'updated_by_id'  => new sfValidatorInteger(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('job_order_dr[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'JobOrderDr';
  }

}
