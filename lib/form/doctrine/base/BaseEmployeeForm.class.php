<?php

/**
 * Employee form base class.
 *
 * @method Employee getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEmployeeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'name'          => new sfWidgetFormInputText(),
      'username'      => new sfWidgetFormInputText(),
      'commission'    => new sfWidgetFormInputText(),
      'is_technician' => new sfWidgetFormInputText(),
      'is_salesman'   => new sfWidgetFormInputText(),
      'address'       => new sfWidgetFormTextarea(),
      'birthday'      => new sfWidgetFormDate(),
      'tin_no'        => new sfWidgetFormInputText(),
      'sss_no'        => new sfWidgetFormInputText(),
      'ph_no'         => new sfWidgetFormInputText(),
      'pi_no'         => new sfWidgetFormInputText(),
      'date_hired'    => new sfWidgetFormDate(),
      'position'      => new sfWidgetFormInputText(),
      'is_wholesale'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'          => new sfValidatorString(array('max_length' => 100)),
      'username'      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'commission'    => new sfValidatorNumber(),
      'is_technician' => new sfValidatorInteger(array('required' => false)),
      'is_salesman'   => new sfValidatorInteger(array('required' => false)),
      'address'       => new sfValidatorString(array('required' => false)),
      'birthday'      => new sfValidatorDate(array('required' => false)),
      'tin_no'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'sss_no'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'ph_no'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'pi_no'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'date_hired'    => new sfValidatorDate(array('required' => false)),
      'position'      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'is_wholesale'  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('employee[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Employee';
  }

}
