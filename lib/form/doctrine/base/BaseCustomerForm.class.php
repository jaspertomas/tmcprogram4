<?php

/**
 * Customer form base class.
 *
 * @method Customer getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCustomerForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'name'                  => new sfWidgetFormInputText(),
      'discrate'              => new sfWidgetFormInputText(),
      'tin_no'                => new sfWidgetFormInputText(),
      'address'               => new sfWidgetFormTextarea(),
      'address2'              => new sfWidgetFormTextarea(),
      'phone1'                => new sfWidgetFormInputText(),
      'faxnum'                => new sfWidgetFormInputText(),
      'email'                 => new sfWidgetFormInputText(),
      'note'                  => new sfWidgetFormInputText(),
      'rep'                   => new sfWidgetFormInputText(),
      'repno'                 => new sfWidgetFormInputText(),
      'rep2'                  => new sfWidgetFormInputText(),
      'rep2no'                => new sfWidgetFormInputText(),
      'taxitem'               => new sfWidgetFormInputText(),
      'notepad'               => new sfWidgetFormTextarea(),
      'salutation'            => new sfWidgetFormInputText(),
      'is_suki'               => new sfWidgetFormInputText(),
      'is_owned'              => new sfWidgetFormInputText(),
      'discount_level_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DiscountLevel'), 'add_empty' => false)),
      'max_discount_level_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MaxDiscountLevel'), 'add_empty' => false)),
      'terms'                 => new sfWidgetFormInputText(),
      'collection_notes'      => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                  => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'discrate'              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'tin_no'                => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'address'               => new sfValidatorString(array('required' => false)),
      'address2'              => new sfValidatorString(array('required' => false)),
      'phone1'                => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'faxnum'                => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'email'                 => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'note'                  => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'rep'                   => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'repno'                 => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'rep2'                  => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'rep2no'                => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'taxitem'               => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'notepad'               => new sfValidatorString(array('required' => false)),
      'salutation'            => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'is_suki'               => new sfValidatorInteger(array('required' => false)),
      'is_owned'              => new sfValidatorInteger(array('required' => false)),
      'discount_level_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('DiscountLevel'), 'required' => false)),
      'max_discount_level_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MaxDiscountLevel'), 'required' => false)),
      'terms'                 => new sfValidatorInteger(array('required' => false)),
      'collection_notes'      => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('customer[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Customer';
  }

}
