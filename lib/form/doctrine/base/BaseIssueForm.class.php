<?php

/**
 * Issue form base class.
 *
 * @method Issue getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIssueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'code'          => new sfWidgetFormInputText(),
      'name'          => new sfWidgetFormTextarea(),
      'problem'       => new sfWidgetFormTextarea(),
      'solution'      => new sfWidgetFormTextarea(),
      'status'        => new sfWidgetFormChoice(array('choices' => array('open' => 'open', 'solved' => 'solved', 'cancelled' => 'cancelled'))),
      'created_by_id' => new sfWidgetFormInputText(),
      'solved_by_id'  => new sfWidgetFormInputText(),
      'client_type'   => new sfWidgetFormInputText(),
      'client_id'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'          => new sfValidatorInteger(),
      'name'          => new sfValidatorString(),
      'problem'       => new sfValidatorString(array('required' => false)),
      'solution'      => new sfValidatorString(array('required' => false)),
      'status'        => new sfValidatorChoice(array('choices' => array(0 => 'open', 1 => 'solved', 2 => 'cancelled'), 'required' => false)),
      'created_by_id' => new sfValidatorInteger(array('required' => false)),
      'solved_by_id'  => new sfValidatorInteger(array('required' => false)),
      'client_type'   => new sfValidatorString(array('max_length' => 20)),
      'client_id'     => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('issue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Issue';
  }

}
