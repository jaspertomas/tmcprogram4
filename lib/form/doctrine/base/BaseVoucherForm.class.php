<?php

/**
 * Voucher form base class.
 *
 * @method Voucher getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVoucherForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'no'                           => new sfWidgetFormInputText(),
      'date'                         => new sfWidgetFormDate(),
      'time'                         => new sfWidgetFormInputText(),
      'date_effective'               => new sfWidgetFormDate(),
      'year'                         => new sfWidgetFormInputText(),
      'month'                        => new sfWidgetFormInputText(),
      'account_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'), 'add_empty' => true)),
      'subaccount_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherSubaccount'), 'add_empty' => true)),
      'voucher_type_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherType'), 'add_empty' => false)),
      'voucher_allocation_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAllocation'), 'add_empty' => false)),
      'payee'                        => new sfWidgetFormInputText(),
      'amount'                       => new sfWidgetFormInputText(),
      'particulars'                  => new sfWidgetFormTextarea(),
      'check_no'                     => new sfWidgetFormInputText(),
      'check_status'                 => new sfWidgetFormChoice(array('choices' => array('Pending' => 'Pending', 'Released' => 'Released', 'Cleared' => 'Cleared', 'On Hold' => 'On Hold', 'Cancelled' => 'Cancelled', 'N/A' => 'N/A'))),
      'notes'                        => new sfWidgetFormTextarea(),
      'out_check_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OutCheck'), 'add_empty' => true)),
      'ref_type'                     => new sfWidgetFormInputText(),
      'ref_id'                       => new sfWidgetFormInputText(),
      'client_type'                  => new sfWidgetFormInputText(),
      'client_id'                    => new sfWidgetFormInputText(),
      'print_count'                  => new sfWidgetFormInputText(),
      'print_date'                   => new sfWidgetFormDate(),
      'is_cancelled'                 => new sfWidgetFormInputText(),
      'is_help_needed'               => new sfWidgetFormInputText(),
      'is_pay_to_self'               => new sfWidgetFormInputText(),
      'invoice_cross_ref_system_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CrossRefSystem'), 'add_empty' => true)),
      'invoice_cross_ref_code'       => new sfWidgetFormInputText(),
      'invoice_cross_ref_id'         => new sfWidgetFormInputText(),
      'purchase_cross_ref_system_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CrossRefSystem_2'), 'add_empty' => true)),
      'purchase_cross_ref_type'      => new sfWidgetFormInputText(),
      'purchase_cross_ref_code'      => new sfWidgetFormInputText(),
      'purchase_cross_ref_id'        => new sfWidgetFormInputText(),
      'is_checked'                   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'no'                           => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'date'                         => new sfValidatorDate(),
      'time'                         => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'date_effective'               => new sfValidatorDate(array('required' => false)),
      'year'                         => new sfValidatorInteger(),
      'month'                        => new sfValidatorInteger(),
      'account_id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'), 'required' => false)),
      'subaccount_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherSubaccount'), 'required' => false)),
      'voucher_type_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherType'))),
      'voucher_allocation_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAllocation'))),
      'payee'                        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'amount'                       => new sfValidatorNumber(),
      'particulars'                  => new sfValidatorString(),
      'check_no'                     => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'check_status'                 => new sfValidatorChoice(array('choices' => array(0 => 'Pending', 1 => 'Released', 2 => 'Cleared', 3 => 'On Hold', 4 => 'Cancelled', 5 => 'N/A'), 'required' => false)),
      'notes'                        => new sfValidatorString(array('required' => false)),
      'out_check_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OutCheck'), 'required' => false)),
      'ref_type'                     => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'ref_id'                       => new sfValidatorInteger(array('required' => false)),
      'client_type'                  => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'client_id'                    => new sfValidatorInteger(array('required' => false)),
      'print_count'                  => new sfValidatorInteger(array('required' => false)),
      'print_date'                   => new sfValidatorDate(array('required' => false)),
      'is_cancelled'                 => new sfValidatorInteger(array('required' => false)),
      'is_help_needed'               => new sfValidatorInteger(array('required' => false)),
      'is_pay_to_self'               => new sfValidatorInteger(array('required' => false)),
      'invoice_cross_ref_system_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CrossRefSystem'), 'required' => false)),
      'invoice_cross_ref_code'       => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'invoice_cross_ref_id'         => new sfValidatorInteger(array('required' => false)),
      'purchase_cross_ref_system_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CrossRefSystem_2'), 'required' => false)),
      'purchase_cross_ref_type'      => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'purchase_cross_ref_code'      => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'purchase_cross_ref_id'        => new sfValidatorInteger(array('required' => false)),
      'is_checked'                   => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('voucher[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Voucher';
  }

}
