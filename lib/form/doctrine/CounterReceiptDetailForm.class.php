<?php

/**
 * CounterReceiptDetail form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CounterReceiptDetailForm extends BaseCounterReceiptDetailForm
{
  public function configure()
  {
    $this->widgetSchema['counter_receipt_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['vendor_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['purchase_id']= new sfWidgetFormInputHidden();
  }
}
