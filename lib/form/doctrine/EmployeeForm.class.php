<?php

/**
 * Employee form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EmployeeForm extends BaseEmployeeForm
{
  public function configure()
  {
    $this->widgetSchema['is_technician'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
    $this->widgetSchema['is_salesman'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
  }
}
