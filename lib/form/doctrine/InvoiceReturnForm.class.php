<?php

/**
 * InvoiceReturn form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceReturnForm extends BaseInvoiceReturnForm
{
  public function configure()
  {
    $this->widgetSchema['is_cancelled']= new sfWidgetFormInputHidden();
    $this->widgetSchema['created_by_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['updated_by_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['created_at']= new sfWidgetFormInputHidden();
    $this->widgetSchema['updated_at']= new sfWidgetFormInputHidden();
    $this->widgetSchema['status']= new sfWidgetFormInputHidden();
  }
}
