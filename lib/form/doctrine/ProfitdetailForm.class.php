<?php

/**
 * Profitdetail form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProfitdetailForm extends BaseProfitdetailForm
{
  public function configure()
  {
    $this->widgetSchema['slot']= new sfWidgetFormInputHidden();
    $this->widgetSchema['color']= new sfWidgetFormInputHidden();
  }
}
