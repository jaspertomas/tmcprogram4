<?php

/**
 * Conversiondetail form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ConversiondetailForm extends BaseConversiondetailForm
{
  public function configure()
  {
    $this->widgetSchema['product_id']= new sfWidgetFormInputHidden();
  }
}
