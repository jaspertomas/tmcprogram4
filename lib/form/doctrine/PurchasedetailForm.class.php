<?php

/**
 * Purchasedetail form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PurchasedetailForm extends BasePurchasedetailForm
{
  public function configure()
  {
    //$this->widgetSchema['product_id']->addOption('order_by',array('name','asc'));
    
    $this->widgetSchema['product_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['slot']= new sfWidgetFormInputHidden();
    $this->widgetSchema['color']= new sfWidgetFormInputHidden();
    $this->widgetSchema['discrate']->setAttribute('size',5);
    $this->widgetSchema['discamt']->setAttribute('size',5);
    $this->widgetSchema['sellprice']->setAttribute('size',5);
    $this->widgetSchema['price']->setAttribute('size',5);
    $this->widgetSchema['qty']->setAttribute('size',1);
    $this->widgetSchema['qty_received']->setAttribute('size',1);
    
  }
}
