<?php

/**
 * Transfer form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TransferForm extends BaseTransferForm
{
  public function configure()
  {
    $this->widgetSchema['created_at']= new sfWidgetFormInputHidden();
    $this->widgetSchema['created_by_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['updated_at']= new sfWidgetFormInputHidden();
    $this->widgetSchema['updated_by_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['meta']= new sfWidgetFormInputHidden();
    $this->widgetSchema['transfer_status']= new sfWidgetFormInputHidden();
    $this->widgetSchema['date_completed']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_temporary']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_completed']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_cancelled']= new sfWidgetFormInputHidden();
  }
}
