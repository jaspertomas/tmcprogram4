<?php

/**
 * Invoice form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceForm extends BaseInvoiceForm
{
  public function configure()
  {
    $this->widgetSchema['customer_id']->addOption('order_by',array('name','asc'));
    $this->widgetSchema['technician_id']= new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Employee'), 'add_empty' => true));
    $this->widgetSchema['is_temporary']= new sfWidgetFormInputHidden();
    $this->widgetSchema['status']= new sfWidgetFormInputHidden();
    $this->widgetSchema['transaction_code']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_dr_based']= new sfWidgetFormInputHidden();
    //$this->widgetSchema['is_temporary']= new sfWidgetFormChoice(array('choices' => array('2' => 'New', '1' => 'Checked Out', '0' => 'Closed')));
    
    unset($this->widgetSchema['terms_id']);
    unset($this->widgetSchema['total']);
    $this->widgetSchema['invno']->setAttribute('size',5);
    $this->widgetSchema['ponumber']->setAttribute('size',5);
    $this->widgetSchema['cash']->setAttribute('size',3);
    $this->widgetSchema['chequeamt']->setAttribute('size',3);
    $this->widgetSchema['credit']->setAttribute('size',3);
    $this->widgetSchema['terms']->setAttribute('size',3);
    $this->widgetSchema['cheque']->setAttribute('size',10);

/*
    $templates= Doctrine_Query::create()
      ->from('InvoiceTemplate it')
      ->where('it.name !="Inter Office"')
      ->execute();
    $templatesarray=array();
    foreach($templates as $t)$templatesarray[$t->getId()]=$t->getName();
    $this->widgetSchema['template_id']=new sfWidgetFormChoice(array('choices' => $templatesarray));
*/
    $this->widgetSchema['template_id']->addOption('add_empty', false);
  }
}
