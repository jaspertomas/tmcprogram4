<?php

/**
 * Customer form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CustomerForm extends BaseCustomerForm
{
  public function configure()
  {
    $this->widgetSchema['address']->setAttribute('size',70);
    $this->widgetSchema['address2']->setAttribute('size',70);
    $this->widgetSchema['phone1']->setAttribute('size',25);
    $this->widgetSchema['tin_no']->setAttribute('size',25);
    $this->widgetSchema['is_owned'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
    $this->widgetSchema['is_suki'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
  }
}
