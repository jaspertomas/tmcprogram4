<?php

/**
 * VoucherSubaccount form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VoucherSubaccountForm extends BaseVoucherSubaccountForm
{
  public function configure()
  {
    $this->widgetSchema['depth']= new sfWidgetFormInputHidden();
    $this->widgetSchema['parent_id']= new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherSubaccount'), 'add_empty' => true));
    // $this->widgetSchema['voucher_type_id']= new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherType'), 'add_empty' => true));
    // $this->widgetSchema['voucher_allocation_id']= new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAllocation'), 'add_empty' => true));
    $this->widgetSchema['is_pay_to_self'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
  }
}
