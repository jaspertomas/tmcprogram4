<?php

/**
 * Purchase form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PurchaseForm extends BasePurchaseForm
{
  public function configure()
  {
    $this->widgetSchema['vendor_id']->addOption('order_by',array('name','asc'));
    unset($this->widgetSchema['terms_id']);
    unset($this->widgetSchema['total']);
    //$this->widgetSchema['datereceived']= new sfWidgetFormInputHidden();
    $this->widgetSchema['terms']->setAttribute('size',3);
    $this->widgetSchema['is_stock'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
    $this->widgetSchema['vendor_invoice']->setAttribute('size',10);
    $this->widgetSchema['status']= new sfWidgetFormInputHidden();
    $this->widgetSchema['received_status']= new sfWidgetFormInputHidden();
    $this->widgetSchema['invnos']= new sfWidgetFormInputHidden();
    $this->widgetSchema['invoice_ids']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_dr_based']= new sfWidgetFormInputHidden();
    $this->widgetSchema['is_temporary']= new sfWidgetFormInputHidden();
  }
}
