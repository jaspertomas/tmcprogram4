<?php

/**
 * InvoiceTemplate filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseInvoiceTemplateFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_invoice' => new sfWidgetFormFilterInput(),
      'is_dr'      => new sfWidgetFormFilterInput(),
      'prefix'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'       => new sfValidatorPass(array('required' => false)),
      'is_invoice' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_dr'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'prefix'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('invoice_template_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvoiceTemplate';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'name'       => 'Text',
      'is_invoice' => 'Number',
      'is_dr'      => 'Number',
      'prefix'     => 'Text',
    );
  }
}
