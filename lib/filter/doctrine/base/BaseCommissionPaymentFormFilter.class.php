<?php

/**
 * CommissionPayment filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCommissionPaymentFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'type'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'Profit' => 'Profit', 'Gross' => 'Gross'))),
      'employee_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Employee'), 'add_empty' => true)),
      'startdate'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'enddate'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'date_created' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'date_paid'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'base1'        => new sfWidgetFormFilterInput(),
      'rate1'        => new sfWidgetFormFilterInput(),
      'base2'        => new sfWidgetFormFilterInput(),
      'rate2'        => new sfWidgetFormFilterInput(),
      'commission'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'balance'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'notes'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'         => new sfValidatorPass(array('required' => false)),
      'type'         => new sfValidatorChoice(array('required' => false, 'choices' => array('Profit' => 'Profit', 'Gross' => 'Gross'))),
      'employee_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Employee'), 'column' => 'id')),
      'startdate'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'enddate'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'date_created' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'date_paid'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'base1'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'rate1'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'base2'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'rate2'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'commission'   => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'balance'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'notes'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('commission_payment_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CommissionPayment';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'name'         => 'Text',
      'type'         => 'Enum',
      'employee_id'  => 'ForeignKey',
      'startdate'    => 'Date',
      'enddate'      => 'Date',
      'date_created' => 'Date',
      'date_paid'    => 'Date',
      'base1'        => 'Number',
      'rate1'        => 'Number',
      'base2'        => 'Number',
      'rate2'        => 'Number',
      'commission'   => 'Number',
      'balance'      => 'Number',
      'notes'        => 'Text',
    );
  }
}
