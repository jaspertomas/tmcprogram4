<?php

/**
 * Deliveryconversion filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDeliveryconversionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'parent_class'  => new sfWidgetFormChoice(array('choices' => array('' => '', 'Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Delivery' => 'Delivery'))),
      'parent_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ref_class'     => new sfWidgetFormFilterInput(),
      'ref_id'        => new sfWidgetFormFilterInput(),
      'conversion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'), 'add_empty' => true)),
      'qty'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'parent_class'  => new sfValidatorChoice(array('required' => false, 'choices' => array('Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Delivery' => 'Delivery'))),
      'parent_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ref_class'     => new sfValidatorPass(array('required' => false)),
      'ref_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'conversion_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Conversion'), 'column' => 'id')),
      'qty'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'description'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('deliveryconversion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Deliveryconversion';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'parent_class'  => 'Enum',
      'parent_id'     => 'Number',
      'ref_class'     => 'Text',
      'ref_id'        => 'Number',
      'conversion_id' => 'ForeignKey',
      'qty'           => 'Number',
      'description'   => 'Text',
    );
  }
}
