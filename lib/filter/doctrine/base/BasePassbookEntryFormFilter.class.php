<?php

/**
 * PassbookEntry filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePassbookEntryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'deposit'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'withdrawal'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'balance'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'trans_type'    => new sfWidgetFormChoice(array('choices' => array('' => '', 'CASH/CHECK DEPOSIT' => 'CASH/CHECK DEPOSIT', 'CHECK ENCASHMENT' => 'CHECK ENCASHMENT', 'CREDIT MEMO' => 'CREDIT MEMO', 'DEBIT MEMO' => 'DEBIT MEMO', 'IMIN-CLRNG-BAT' => 'IMIN-CLRNG-BAT', 'IN-HOUSE CHECK DEPOSIT' => 'IN-HOUSE CHECK DEPOSIT', 'INWARD CHECK' => 'INWARD CHECK', 'LOAN PAYMENT' => 'LOAN PAYMENT', 'LOCAL CHECK DEPOSIT' => 'LOCAL CHECK DEPOSIT', 'REPORT' => 'REPORT'))),
      'passbook_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'), 'add_empty' => true)),
      'client_type'   => new sfWidgetFormFilterInput(),
      'client_name'   => new sfWidgetFormFilterInput(),
      'meta'          => new sfWidgetFormFilterInput(),
      'description'   => new sfWidgetFormFilterInput(),
      'json'          => new sfWidgetFormFilterInput(),
      'datetime'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'qty'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_reported'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_missing'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ref_class'     => new sfWidgetFormFilterInput(),
      'ref_id'        => new sfWidgetFormFilterInput(),
      'type'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'InCheck' => 'InCheck', 'OutCheck' => 'OutCheck', 'Report' => 'Report'))),
      'is_report'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_by_id' => new sfWidgetFormFilterInput(),
      'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'deposit'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'withdrawal'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'balance'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'trans_type'    => new sfValidatorChoice(array('required' => false, 'choices' => array('CASH/CHECK DEPOSIT' => 'CASH/CHECK DEPOSIT', 'CHECK ENCASHMENT' => 'CHECK ENCASHMENT', 'CREDIT MEMO' => 'CREDIT MEMO', 'DEBIT MEMO' => 'DEBIT MEMO', 'IMIN-CLRNG-BAT' => 'IMIN-CLRNG-BAT', 'IN-HOUSE CHECK DEPOSIT' => 'IN-HOUSE CHECK DEPOSIT', 'INWARD CHECK' => 'INWARD CHECK', 'LOAN PAYMENT' => 'LOAN PAYMENT', 'LOCAL CHECK DEPOSIT' => 'LOCAL CHECK DEPOSIT', 'REPORT' => 'REPORT'))),
      'passbook_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Passbook'), 'column' => 'id')),
      'client_type'   => new sfValidatorPass(array('required' => false)),
      'client_name'   => new sfValidatorPass(array('required' => false)),
      'meta'          => new sfValidatorPass(array('required' => false)),
      'description'   => new sfValidatorPass(array('required' => false)),
      'json'          => new sfValidatorPass(array('required' => false)),
      'datetime'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'qty'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty_reported'  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty_missing'   => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'ref_class'     => new sfValidatorPass(array('required' => false)),
      'ref_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'type'          => new sfValidatorChoice(array('required' => false, 'choices' => array('InCheck' => 'InCheck', 'OutCheck' => 'OutCheck', 'Report' => 'Report'))),
      'is_report'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_by_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('passbook_entry_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassbookEntry';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'deposit'       => 'Number',
      'withdrawal'    => 'Number',
      'balance'       => 'Number',
      'trans_type'    => 'Enum',
      'passbook_id'   => 'ForeignKey',
      'client_type'   => 'Text',
      'client_name'   => 'Text',
      'meta'          => 'Text',
      'description'   => 'Text',
      'json'          => 'Text',
      'datetime'      => 'Date',
      'qty'           => 'Number',
      'qty_reported'  => 'Number',
      'qty_missing'   => 'Number',
      'ref_class'     => 'Text',
      'ref_id'        => 'Number',
      'type'          => 'Enum',
      'is_report'     => 'Number',
      'created_by_id' => 'Number',
      'created_at'    => 'Date',
    );
  }
}
