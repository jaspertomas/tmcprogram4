<?php

/**
 * Todo filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTodoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'description' => new sfWidgetFormFilterInput(),
      'priority'    => new sfWidgetFormChoice(array('choices' => array('' => '', 'Low' => 'Low', 'Medium' => 'Medium', 'High' => 'High'))),
      'ref_type'    => new sfWidgetFormFilterInput(),
      'ref_id'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'description' => new sfValidatorPass(array('required' => false)),
      'priority'    => new sfValidatorChoice(array('required' => false, 'choices' => array('Low' => 'Low', 'Medium' => 'Medium', 'High' => 'High'))),
      'ref_type'    => new sfValidatorPass(array('required' => false)),
      'ref_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('todo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Todo';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'description' => 'Text',
      'priority'    => 'Enum',
      'ref_type'    => 'Text',
      'ref_id'      => 'Number',
    );
  }
}
