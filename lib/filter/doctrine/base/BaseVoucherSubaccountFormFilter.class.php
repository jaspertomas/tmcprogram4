<?php

/**
 * VoucherSubaccount filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseVoucherSubaccountFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'fullname'              => new sfWidgetFormFilterInput(),
      'voucher_account_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'), 'add_empty' => true)),
      'parent_id'             => new sfWidgetFormFilterInput(),
      'redirect_id'           => new sfWidgetFormFilterInput(),
      'depth'                 => new sfWidgetFormFilterInput(),
      'voucher_type_id'       => new sfWidgetFormFilterInput(),
      'voucher_allocation_id' => new sfWidgetFormFilterInput(),
      'passbook_id'           => new sfWidgetFormFilterInput(),
      'payee'                 => new sfWidgetFormFilterInput(),
      'amount'                => new sfWidgetFormFilterInput(),
      'particulars'           => new sfWidgetFormFilterInput(),
      'notes'                 => new sfWidgetFormFilterInput(),
      'is_pay_to_self'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'                  => new sfValidatorPass(array('required' => false)),
      'fullname'              => new sfValidatorPass(array('required' => false)),
      'voucher_account_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VoucherAccount'), 'column' => 'id')),
      'parent_id'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'redirect_id'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'depth'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'voucher_type_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'voucher_allocation_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'passbook_id'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payee'                 => new sfValidatorPass(array('required' => false)),
      'amount'                => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'particulars'           => new sfValidatorPass(array('required' => false)),
      'notes'                 => new sfValidatorPass(array('required' => false)),
      'is_pay_to_self'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('voucher_subaccount_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VoucherSubaccount';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'name'                  => 'Text',
      'fullname'              => 'Text',
      'voucher_account_id'    => 'ForeignKey',
      'parent_id'             => 'Number',
      'redirect_id'           => 'Number',
      'depth'                 => 'Number',
      'voucher_type_id'       => 'Number',
      'voucher_allocation_id' => 'Number',
      'passbook_id'           => 'Number',
      'payee'                 => 'Text',
      'amount'                => 'Number',
      'particulars'           => 'Text',
      'notes'                 => 'Text',
      'is_pay_to_self'        => 'Number',
    );
  }
}
