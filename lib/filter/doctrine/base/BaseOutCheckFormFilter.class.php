<?php

/**
 * OutCheck filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseOutCheckFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'check_no'           => new sfWidgetFormFilterInput(),
      'amount'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'receive_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'check_date'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'cleared_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'vendor_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'), 'add_empty' => true)),
      'voucher_account_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'), 'add_empty' => true)),
      'notes'              => new sfWidgetFormFilterInput(),
      'passbook_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'), 'add_empty' => true)),
      'passbook_entry_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookEntry'), 'add_empty' => true)),
      'passbook_pay_to_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookPayTo'), 'add_empty' => true)),
      'is_bank_transfer'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'remaining'          => new sfWidgetFormFilterInput(),
      'print_count'        => new sfWidgetFormFilterInput(),
      'print_date'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'is_cancelled'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'code'               => new sfValidatorPass(array('required' => false)),
      'check_no'           => new sfValidatorPass(array('required' => false)),
      'amount'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'receive_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'check_date'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'cleared_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'vendor_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Vendor'), 'column' => 'id')),
      'voucher_account_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VoucherAccount'), 'column' => 'id')),
      'notes'              => new sfValidatorPass(array('required' => false)),
      'passbook_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Passbook'), 'column' => 'id')),
      'passbook_entry_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassbookEntry'), 'column' => 'id')),
      'passbook_pay_to_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassbookPayTo'), 'column' => 'id')),
      'is_bank_transfer'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'remaining'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'print_count'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'print_date'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'is_cancelled'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('out_check_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OutCheck';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'code'               => 'Text',
      'check_no'           => 'Text',
      'amount'             => 'Number',
      'receive_date'       => 'Date',
      'check_date'         => 'Date',
      'cleared_date'       => 'Date',
      'vendor_id'          => 'ForeignKey',
      'voucher_account_id' => 'ForeignKey',
      'notes'              => 'Text',
      'passbook_id'        => 'ForeignKey',
      'passbook_entry_id'  => 'ForeignKey',
      'passbook_pay_to_id' => 'ForeignKey',
      'is_bank_transfer'   => 'Number',
      'remaining'          => 'Number',
      'print_count'        => 'Number',
      'print_date'         => 'Date',
      'is_cancelled'       => 'Number',
    );
  }
}
