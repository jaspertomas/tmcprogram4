<?php

/**
 * Stockentry filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseStockentryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'datetime'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'qty'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_reported'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_missing'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'balance'       => new sfWidgetFormFilterInput(),
      'stock_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Stock'), 'add_empty' => true)),
      'ref_class'     => new sfWidgetFormFilterInput(),
      'ref_id'        => new sfWidgetFormFilterInput(),
      'type'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'Adjustment' => 'Adjustment', 'Incoming DR' => 'Incoming DR', 'Outgoing DR' => 'Outgoing DR', 'Conversion DR' => 'Conversion DR', 'Invoice' => 'Invoice', 'InvoiceReturn' => 'InvoiceReturn', 'InvoiceReplacement' => 'InvoiceReplacement', 'InvoiceConversion' => 'InvoiceConversion', 'Purchase' => 'Purchase', 'PurchaseReturn' => 'PurchaseReturn', 'PurchaseReplacement' => 'PurchaseReplacement', 'PurchaseConversion' => 'PurchaseConversion', 'Report' => 'Report', 'InvoiceDr' => 'InvoiceDr', 'PurchaseDr' => 'PurchaseDr'))),
      'is_report'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'   => new sfWidgetFormFilterInput(),
      'created_by_id' => new sfWidgetFormFilterInput(),
      'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'datetime'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'qty'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty_reported'  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty_missing'   => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'balance'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'stock_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Stock'), 'column' => 'id')),
      'ref_class'     => new sfValidatorPass(array('required' => false)),
      'ref_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'type'          => new sfValidatorChoice(array('required' => false, 'choices' => array('Adjustment' => 'Adjustment', 'Incoming DR' => 'Incoming DR', 'Outgoing DR' => 'Outgoing DR', 'Conversion DR' => 'Conversion DR', 'Invoice' => 'Invoice', 'InvoiceReturn' => 'InvoiceReturn', 'InvoiceReplacement' => 'InvoiceReplacement', 'InvoiceConversion' => 'InvoiceConversion', 'Purchase' => 'Purchase', 'PurchaseReturn' => 'PurchaseReturn', 'PurchaseReplacement' => 'PurchaseReplacement', 'PurchaseConversion' => 'PurchaseConversion', 'Report' => 'Report', 'InvoiceDr' => 'InvoiceDr', 'PurchaseDr' => 'PurchaseDr'))),
      'is_report'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'description'   => new sfValidatorPass(array('required' => false)),
      'created_by_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('stockentry_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Stockentry';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'datetime'      => 'Date',
      'qty'           => 'Number',
      'qty_reported'  => 'Number',
      'qty_missing'   => 'Number',
      'balance'       => 'Number',
      'stock_id'      => 'ForeignKey',
      'ref_class'     => 'Text',
      'ref_id'        => 'Number',
      'type'          => 'Enum',
      'is_report'     => 'Number',
      'description'   => 'Text',
      'created_by_id' => 'Number',
      'created_at'    => 'Date',
    );
  }
}
