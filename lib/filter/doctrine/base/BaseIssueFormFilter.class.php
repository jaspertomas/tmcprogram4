<?php

/**
 * Issue filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseIssueFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'name'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'problem'       => new sfWidgetFormFilterInput(),
      'solution'      => new sfWidgetFormFilterInput(),
      'status'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'open' => 'open', 'solved' => 'solved', 'cancelled' => 'cancelled'))),
      'created_by_id' => new sfWidgetFormFilterInput(),
      'solved_by_id'  => new sfWidgetFormFilterInput(),
      'client_type'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'client_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'code'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name'          => new sfValidatorPass(array('required' => false)),
      'problem'       => new sfValidatorPass(array('required' => false)),
      'solution'      => new sfValidatorPass(array('required' => false)),
      'status'        => new sfValidatorChoice(array('required' => false, 'choices' => array('open' => 'open', 'solved' => 'solved', 'cancelled' => 'cancelled'))),
      'created_by_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'solved_by_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'client_type'   => new sfValidatorPass(array('required' => false)),
      'client_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('issue_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Issue';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'code'          => 'Number',
      'name'          => 'Text',
      'problem'       => 'Text',
      'solution'      => 'Text',
      'status'        => 'Enum',
      'created_by_id' => 'Number',
      'solved_by_id'  => 'Number',
      'client_type'   => 'Text',
      'client_id'     => 'Number',
    );
  }
}
