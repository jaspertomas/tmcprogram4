<?php

/**
 * ReturnsDetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseReturnsDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ref_class'    => new sfWidgetFormChoice(array('choices' => array('' => '', 'Invoicedetail' => 'Invoicedetail', 'Purchasedetail' => 'Purchasedetail'))),
      'ref_id'       => new sfWidgetFormFilterInput(),
      'returns_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Returns'), 'add_empty' => true)),
      'product_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'description'  => new sfWidgetFormFilterInput(),
      'qty'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'total'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_replaced' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'ref_class'    => new sfValidatorChoice(array('required' => false, 'choices' => array('Invoicedetail' => 'Invoicedetail', 'Purchasedetail' => 'Purchasedetail'))),
      'ref_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'returns_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Returns'), 'column' => 'id')),
      'product_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'description'  => new sfValidatorPass(array('required' => false)),
      'qty'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty_replaced' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('returns_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReturnsDetail';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'ref_class'    => 'Enum',
      'ref_id'       => 'Number',
      'returns_id'   => 'ForeignKey',
      'product_id'   => 'ForeignKey',
      'description'  => 'Text',
      'qty'          => 'Number',
      'price'        => 'Number',
      'total'        => 'Number',
      'qty_replaced' => 'Number',
    );
  }
}
