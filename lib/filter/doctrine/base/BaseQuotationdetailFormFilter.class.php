<?php

/**
 * Quotationdetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseQuotationdetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'quotation_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Quotation'), 'add_empty' => true)),
      'product_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'description'   => new sfWidgetFormFilterInput(),
      'qty'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_discounted' => new sfWidgetFormFilterInput(),
      'unittotal'     => new sfWidgetFormFilterInput(),
      'discrate'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'discamt'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'total'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'quotation_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Quotation'), 'column' => 'id')),
      'product_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'description'   => new sfValidatorPass(array('required' => false)),
      'qty'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_discounted' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'unittotal'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'discrate'      => new sfValidatorPass(array('required' => false)),
      'discamt'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('quotationdetail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Quotationdetail';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'quotation_id'  => 'ForeignKey',
      'product_id'    => 'ForeignKey',
      'description'   => 'Text',
      'qty'           => 'Number',
      'price'         => 'Number',
      'is_discounted' => 'Number',
      'unittotal'     => 'Number',
      'discrate'      => 'Text',
      'discamt'       => 'Number',
      'total'         => 'Number',
    );
  }
}
