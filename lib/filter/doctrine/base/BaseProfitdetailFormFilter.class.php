<?php

/**
 * Profitdetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseProfitdetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'product_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'invoice_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => true)),
      'purchase_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'), 'add_empty' => true)),
      'invoicedetail_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoicedetail'), 'add_empty' => true)),
      'purchasedetail_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchasedetail'), 'add_empty' => true)),
      'invoicedate'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'purchasedate'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'buyprice'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sellprice'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'profitperunit'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'profitrate'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'profit'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'consignment_payment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ConsignmentPayment'), 'add_empty' => true)),
      'color'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow', 'green' => 'green', 'blue' => 'blue', 'indigo' => 'indigo', 'violet' => 'violet'))),
      'slot'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'product_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'invoice_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Invoice'), 'column' => 'id')),
      'purchase_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Purchase'), 'column' => 'id')),
      'invoicedetail_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Invoicedetail'), 'column' => 'id')),
      'purchasedetail_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Purchasedetail'), 'column' => 'id')),
      'invoicedate'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'purchasedate'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'buyprice'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'sellprice'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'profitperunit'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'profitrate'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty'                    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'profit'                 => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'consignment_payment_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ConsignmentPayment'), 'column' => 'id')),
      'color'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow', 'green' => 'green', 'blue' => 'blue', 'indigo' => 'indigo', 'violet' => 'violet'))),
      'slot'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('profitdetail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Profitdetail';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'product_id'             => 'ForeignKey',
      'invoice_id'             => 'ForeignKey',
      'purchase_id'            => 'ForeignKey',
      'invoicedetail_id'       => 'ForeignKey',
      'purchasedetail_id'      => 'ForeignKey',
      'invoicedate'            => 'Date',
      'purchasedate'           => 'Date',
      'buyprice'               => 'Number',
      'sellprice'              => 'Number',
      'profitperunit'          => 'Number',
      'profitrate'             => 'Number',
      'qty'                    => 'Number',
      'profit'                 => 'Number',
      'consignment_payment_id' => 'ForeignKey',
      'color'                  => 'Enum',
      'slot'                   => 'Number',
    );
  }
}
