<?php

/**
 * File filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'parent_class'   => new sfWidgetFormFilterInput(),
      'parent_id'      => new sfWidgetFormFilterInput(),
      'title'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'    => new sfWidgetFormFilterInput(),
      'filename'       => new sfWidgetFormFilterInput(),
      'file'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'keywords'       => new sfWidgetFormFilterInput(),
      'filesize'       => new sfWidgetFormFilterInput(),
      'filetype'       => new sfWidgetFormFilterInput(),
      'uploadlocation' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'parent_class'   => new sfValidatorPass(array('required' => false)),
      'parent_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title'          => new sfValidatorPass(array('required' => false)),
      'description'    => new sfValidatorPass(array('required' => false)),
      'filename'       => new sfValidatorPass(array('required' => false)),
      'file'           => new sfValidatorPass(array('required' => false)),
      'keywords'       => new sfValidatorPass(array('required' => false)),
      'filesize'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'filetype'       => new sfValidatorPass(array('required' => false)),
      'uploadlocation' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('file_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'File';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'parent_class'   => 'Text',
      'parent_id'      => 'Number',
      'title'          => 'Text',
      'description'    => 'Text',
      'filename'       => 'Text',
      'file'           => 'Text',
      'keywords'       => 'Text',
      'filesize'       => 'Number',
      'filetype'       => 'Text',
      'uploadlocation' => 'Text',
    );
  }
}
