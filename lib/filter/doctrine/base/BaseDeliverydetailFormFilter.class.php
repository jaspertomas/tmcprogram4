<?php

/**
 * Deliverydetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDeliverydetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ref_class'    => new sfWidgetFormChoice(array('choices' => array('' => '', 'invoicedetail' => 'invoicedetail', 'purchasedetail' => 'purchasedetail', 'deliveryconversion' => 'deliveryconversion'))),
      'ref_id'       => new sfWidgetFormFilterInput(),
      'parent_class' => new sfWidgetFormChoice(array('choices' => array('' => '', 'Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Delivery' => 'Delivery'))),
      'parent_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'product_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'description'  => new sfWidgetFormFilterInput(),
      'qty'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'discrate'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'discprice'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'total'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'ref_class'    => new sfValidatorChoice(array('required' => false, 'choices' => array('invoicedetail' => 'invoicedetail', 'purchasedetail' => 'purchasedetail', 'deliveryconversion' => 'deliveryconversion'))),
      'ref_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'parent_class' => new sfValidatorChoice(array('required' => false, 'choices' => array('Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Delivery' => 'Delivery'))),
      'parent_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'product_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'description'  => new sfValidatorPass(array('required' => false)),
      'qty'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'discrate'     => new sfValidatorPass(array('required' => false)),
      'discprice'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('deliverydetail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Deliverydetail';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'ref_class'    => 'Enum',
      'ref_id'       => 'Number',
      'parent_class' => 'Enum',
      'parent_id'    => 'Number',
      'product_id'   => 'ForeignKey',
      'description'  => 'Text',
      'qty'          => 'Number',
      'price'        => 'Number',
      'discrate'     => 'Text',
      'discprice'    => 'Number',
      'total'        => 'Number',
    );
  }
}
