<?php

/**
 * Invoicedetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseInvoicedetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'invoice_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => true)),
      'product_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'barcode'               => new sfWidgetFormFilterInput(),
      'description'           => new sfWidgetFormFilterInput(),
      'qty'                   => new sfWidgetFormFilterInput(),
      'qty_released'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'buyprice'              => new sfWidgetFormFilterInput(),
      'discrate'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'discprice'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'total'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'discamt'               => new sfWidgetFormFilterInput(),
      'unittotal'             => new sfWidgetFormFilterInput(),
      'is_cancelled'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_discounted'         => new sfWidgetFormFilterInput(),
      'remaining'             => new sfWidgetFormFilterInput(),
      'is_profitcalculated'   => new sfWidgetFormFilterInput(),
      'with_commission'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'with_commission_total' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_vat'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_returned'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'invoice_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Invoice'), 'column' => 'id')),
      'product_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'barcode'               => new sfValidatorPass(array('required' => false)),
      'description'           => new sfValidatorPass(array('required' => false)),
      'qty'                   => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty_released'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'                 => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'buyprice'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'discrate'              => new sfValidatorPass(array('required' => false)),
      'discprice'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total'                 => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'discamt'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'unittotal'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_cancelled'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_discounted'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'remaining'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_profitcalculated'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'with_commission'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'with_commission_total' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_vat'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'qty_returned'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('invoicedetail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Invoicedetail';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'invoice_id'            => 'ForeignKey',
      'product_id'            => 'ForeignKey',
      'barcode'               => 'Text',
      'description'           => 'Text',
      'qty'                   => 'Number',
      'qty_released'          => 'Number',
      'price'                 => 'Number',
      'buyprice'              => 'Number',
      'discrate'              => 'Text',
      'discprice'             => 'Number',
      'total'                 => 'Number',
      'discamt'               => 'Number',
      'unittotal'             => 'Number',
      'is_cancelled'          => 'Number',
      'is_discounted'         => 'Number',
      'remaining'             => 'Number',
      'is_profitcalculated'   => 'Number',
      'with_commission'       => 'Number',
      'with_commission_total' => 'Number',
      'is_vat'                => 'Number',
      'qty_returned'          => 'Number',
    );
  }
}
