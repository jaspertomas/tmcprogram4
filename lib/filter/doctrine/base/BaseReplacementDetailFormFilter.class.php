<?php

/**
 * ReplacementDetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseReplacementDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ref_class'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'Invoicedetail' => 'Invoicedetail', 'Purchasedetail' => 'Purchasedetail'))),
      'ref_id'            => new sfWidgetFormFilterInput(),
      'returns_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ReturnsDetail'), 'add_empty' => true)),
      'replacement_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Replacement'), 'add_empty' => true)),
      'product_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'description'       => new sfWidgetFormFilterInput(),
      'qty'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'total'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'ref_class'         => new sfValidatorChoice(array('required' => false, 'choices' => array('Invoicedetail' => 'Invoicedetail', 'Purchasedetail' => 'Purchasedetail'))),
      'ref_id'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'returns_detail_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ReturnsDetail'), 'column' => 'id')),
      'replacement_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Replacement'), 'column' => 'id')),
      'product_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'description'       => new sfValidatorPass(array('required' => false)),
      'qty'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('replacement_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReplacementDetail';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'ref_class'         => 'Enum',
      'ref_id'            => 'Number',
      'returns_detail_id' => 'ForeignKey',
      'replacement_id'    => 'ForeignKey',
      'product_id'        => 'ForeignKey',
      'description'       => 'Text',
      'qty'               => 'Number',
      'price'             => 'Number',
      'total'             => 'Number',
    );
  }
}
