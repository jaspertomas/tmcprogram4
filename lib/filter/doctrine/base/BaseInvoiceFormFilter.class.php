<?php

/**
 * Invoice filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseInvoiceFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'customer_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => true)),
      'customer_name'              => new sfWidgetFormFilterInput(),
      'customer_phone'             => new sfWidgetFormFilterInput(),
      'invno'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_code'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ponumber'                   => new sfWidgetFormFilterInput(),
      'notes'                      => new sfWidgetFormFilterInput(),
      'payonly'                    => new sfWidgetFormFilterInput(),
      'total'                      => new sfWidgetFormFilterInput(),
      'total_for_product_category' => new sfWidgetFormFilterInput(),
      'total_for_tanks'            => new sfWidgetFormFilterInput(),
      'total_for_pumps'            => new sfWidgetFormFilterInput(),
      'cheque'                     => new sfWidgetFormFilterInput(),
      'chequedate'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'date'                       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'datereleased'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'duedate'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'salesman_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Employee'), 'add_empty' => true)),
      'technician_id'              => new sfWidgetFormFilterInput(),
      'template_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('InvoiceTemplate'), 'add_empty' => true)),
      'cash'                       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'chequeamt'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'credit'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'discount_level_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DiscountLevel'), 'add_empty' => true)),
      'max_discount_level_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MaxDiscountLevel'), 'add_empty' => true)),
      'discrate'                   => new sfWidgetFormFilterInput(),
      'discamt'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'saletype'                   => new sfWidgetFormChoice(array('choices' => array('' => '', 'Cash' => 'Cash', 'Cheque' => 'Cheque', 'Account' => 'Account', 'Partial' => 'Partial', 'Bank Transfer' => 'Bank Transfer'))),
      'status'                     => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Paid' => 'Paid', 'Cancelled' => 'Cancelled', 'Overpaid' => 'Overpaid'))),
      'has_check'                  => new sfWidgetFormFilterInput(),
      'is_check_cleared'           => new sfWidgetFormFilterInput(),
      'dsrdeduction'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'balance'                    => new sfWidgetFormFilterInput(),
      'chequedata'                 => new sfWidgetFormFilterInput(),
      'checkcleardate'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'checkcollectevents'         => new sfWidgetFormFilterInput(),
      'hidden'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_inspected'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_temporary'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'collection_status'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Bill Sent' => 'Bill Sent', 'Cheque Ready' => 'Cheque Ready', 'Paid' => 'Paid'))),
      'commission_payment_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CommissionPayment'), 'add_empty' => true)),
      'is_archived'                => new sfWidgetFormFilterInput(),
      'is_dr_based'                => new sfWidgetFormFilterInput(),
      'terms'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'in_check_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('InCheck'), 'add_empty' => true)),
      'was_closed'                 => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'customer_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Customer'), 'column' => 'id')),
      'customer_name'              => new sfValidatorPass(array('required' => false)),
      'customer_phone'             => new sfValidatorPass(array('required' => false)),
      'invno'                      => new sfValidatorPass(array('required' => false)),
      'transaction_code'           => new sfValidatorPass(array('required' => false)),
      'ponumber'                   => new sfValidatorPass(array('required' => false)),
      'notes'                      => new sfValidatorPass(array('required' => false)),
      'payonly'                    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total'                      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total_for_product_category' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total_for_tanks'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total_for_pumps'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'cheque'                     => new sfValidatorPass(array('required' => false)),
      'chequedate'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'date'                       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'datereleased'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'duedate'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'salesman_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Employee'), 'column' => 'id')),
      'technician_id'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'template_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('InvoiceTemplate'), 'column' => 'id')),
      'cash'                       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'chequeamt'                  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'credit'                     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'discount_level_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('DiscountLevel'), 'column' => 'id')),
      'max_discount_level_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MaxDiscountLevel'), 'column' => 'id')),
      'discrate'                   => new sfValidatorPass(array('required' => false)),
      'discamt'                    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'saletype'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('Cash' => 'Cash', 'Cheque' => 'Cheque', 'Account' => 'Account', 'Partial' => 'Partial', 'Bank Transfer' => 'Bank Transfer'))),
      'status'                     => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Paid' => 'Paid', 'Cancelled' => 'Cancelled', 'Overpaid' => 'Overpaid'))),
      'has_check'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_check_cleared'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dsrdeduction'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'balance'                    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'chequedata'                 => new sfValidatorPass(array('required' => false)),
      'checkcleardate'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'checkcollectevents'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'hidden'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_inspected'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_temporary'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'collection_status'          => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Bill Sent' => 'Bill Sent', 'Cheque Ready' => 'Cheque Ready', 'Paid' => 'Paid'))),
      'commission_payment_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CommissionPayment'), 'column' => 'id')),
      'is_archived'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_dr_based'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'terms'                      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'in_check_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('InCheck'), 'column' => 'id')),
      'was_closed'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('invoice_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Invoice';
  }

  public function getFields()
  {
    return array(
      'id'                         => 'Number',
      'customer_id'                => 'ForeignKey',
      'customer_name'              => 'Text',
      'customer_phone'             => 'Text',
      'invno'                      => 'Text',
      'transaction_code'           => 'Text',
      'ponumber'                   => 'Text',
      'notes'                      => 'Text',
      'payonly'                    => 'Number',
      'total'                      => 'Number',
      'total_for_product_category' => 'Number',
      'total_for_tanks'            => 'Number',
      'total_for_pumps'            => 'Number',
      'cheque'                     => 'Text',
      'chequedate'                 => 'Date',
      'date'                       => 'Date',
      'datereleased'               => 'Date',
      'duedate'                    => 'Date',
      'salesman_id'                => 'ForeignKey',
      'technician_id'              => 'Number',
      'template_id'                => 'ForeignKey',
      'cash'                       => 'Number',
      'chequeamt'                  => 'Number',
      'credit'                     => 'Number',
      'discount_level_id'          => 'ForeignKey',
      'max_discount_level_id'      => 'ForeignKey',
      'discrate'                   => 'Text',
      'discamt'                    => 'Number',
      'saletype'                   => 'Enum',
      'status'                     => 'Enum',
      'has_check'                  => 'Number',
      'is_check_cleared'           => 'Number',
      'dsrdeduction'               => 'Number',
      'balance'                    => 'Number',
      'chequedata'                 => 'Text',
      'checkcleardate'             => 'Date',
      'checkcollectevents'         => 'Number',
      'hidden'                     => 'Number',
      'is_inspected'               => 'Number',
      'is_temporary'               => 'Number',
      'collection_status'          => 'Enum',
      'commission_payment_id'      => 'ForeignKey',
      'is_archived'                => 'Number',
      'is_dr_based'                => 'Number',
      'terms'                      => 'Number',
      'in_check_id'                => 'ForeignKey',
      'was_closed'                 => 'Number',
    );
  }
}
