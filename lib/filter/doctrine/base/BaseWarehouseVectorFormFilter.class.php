<?php

/**
 * WarehouseVector filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWarehouseVectorFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'from_warehouse_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'to_warehouse_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse_2'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'name'              => new sfValidatorPass(array('required' => false)),
      'from_warehouse_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse'), 'column' => 'id')),
      'to_warehouse_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse_2'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('warehouse_vector_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WarehouseVector';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'name'              => 'Text',
      'from_warehouse_id' => 'ForeignKey',
      'to_warehouse_id'   => 'ForeignKey',
    );
  }
}
