<?php

/**
 * Conversiondetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseConversiondetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'conversion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'), 'add_empty' => true)),
      'type'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'from' => 'from', 'to' => 'to'))),
      'product_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'qty'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'conversion_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Conversion'), 'column' => 'id')),
      'type'          => new sfValidatorChoice(array('required' => false, 'choices' => array('from' => 'from', 'to' => 'to'))),
      'product_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'qty'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('conversiondetail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Conversiondetail';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'conversion_id' => 'ForeignKey',
      'type'          => 'Enum',
      'product_id'    => 'ForeignKey',
      'qty'           => 'Number',
    );
  }
}
