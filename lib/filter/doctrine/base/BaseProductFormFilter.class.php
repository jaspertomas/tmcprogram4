<?php

/**
 * Product filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseProductFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'brand_id'                => new sfWidgetFormFilterInput(),
      'producttype_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Producttype'), 'add_empty' => true)),
      'general_producttype_id'  => new sfWidgetFormFilterInput(),
      'specific_producttype_id' => new sfWidgetFormFilterInput(),
      'productcategory'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'TJL' => 'TJL', 'Sheet' => 'Sheet', 'Supplies' => 'Supplies'))),
      'qty'                     => new sfWidgetFormFilterInput(),
      'minbuyprice'             => new sfWidgetFormFilterInput(),
      'maxbuyprice'             => new sfWidgetFormFilterInput(),
      'minsellprice'            => new sfWidgetFormFilterInput(),
      'maxsellprice'            => new sfWidgetFormFilterInput(),
      'baseprice'               => new sfWidgetFormFilterInput(),
      'description'             => new sfWidgetFormFilterInput(),
      'code'                    => new sfWidgetFormFilterInput(),
      'spec1'                   => new sfWidgetFormFilterInput(),
      'spec2'                   => new sfWidgetFormFilterInput(),
      'spec3'                   => new sfWidgetFormFilterInput(),
      'spec4'                   => new sfWidgetFormFilterInput(),
      'spec5'                   => new sfWidgetFormFilterInput(),
      'spec6'                   => new sfWidgetFormFilterInput(),
      'spec7'                   => new sfWidgetFormFilterInput(),
      'spec8'                   => new sfWidgetFormFilterInput(),
      'spec9'                   => new sfWidgetFormFilterInput(),
      'is_hidden'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'autocalcsellprice'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'autocalcbuyprice'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'monitored'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'barcode'                 => new sfWidgetFormFilterInput(),
      'is_service'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_tax'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'competitor_price'        => new sfWidgetFormFilterInput(),
      'notes'                   => new sfWidgetFormFilterInput(),
      'quota'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_updated'              => new sfWidgetFormFilterInput(),
      'is_allow_zeroprice'      => new sfWidgetFormFilterInput(),
      'is_fixed_price'          => new sfWidgetFormFilterInput(),
      'conversion_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Conversion'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'name'                    => new sfValidatorPass(array('required' => false)),
      'brand_id'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'producttype_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Producttype'), 'column' => 'id')),
      'general_producttype_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'specific_producttype_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'productcategory'         => new sfValidatorChoice(array('required' => false, 'choices' => array('' => '', 'TJL' => 'TJL', 'Sheet' => 'Sheet', 'Supplies' => 'Supplies'))),
      'qty'                     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'minbuyprice'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'maxbuyprice'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'minsellprice'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'maxsellprice'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'baseprice'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'description'             => new sfValidatorPass(array('required' => false)),
      'code'                    => new sfValidatorPass(array('required' => false)),
      'spec1'                   => new sfValidatorPass(array('required' => false)),
      'spec2'                   => new sfValidatorPass(array('required' => false)),
      'spec3'                   => new sfValidatorPass(array('required' => false)),
      'spec4'                   => new sfValidatorPass(array('required' => false)),
      'spec5'                   => new sfValidatorPass(array('required' => false)),
      'spec6'                   => new sfValidatorPass(array('required' => false)),
      'spec7'                   => new sfValidatorPass(array('required' => false)),
      'spec8'                   => new sfValidatorPass(array('required' => false)),
      'spec9'                   => new sfValidatorPass(array('required' => false)),
      'is_hidden'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'autocalcsellprice'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'autocalcbuyprice'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'monitored'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'barcode'                 => new sfValidatorPass(array('required' => false)),
      'is_service'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_tax'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'competitor_price'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'notes'                   => new sfValidatorPass(array('required' => false)),
      'quota'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_updated'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_allow_zeroprice'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_fixed_price'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'conversion_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Conversion'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('product_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Product';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'name'                    => 'Text',
      'brand_id'                => 'Number',
      'producttype_id'          => 'ForeignKey',
      'general_producttype_id'  => 'Number',
      'specific_producttype_id' => 'Number',
      'productcategory'         => 'Enum',
      'qty'                     => 'Number',
      'minbuyprice'             => 'Number',
      'maxbuyprice'             => 'Number',
      'minsellprice'            => 'Number',
      'maxsellprice'            => 'Number',
      'baseprice'               => 'Number',
      'description'             => 'Text',
      'code'                    => 'Text',
      'spec1'                   => 'Text',
      'spec2'                   => 'Text',
      'spec3'                   => 'Text',
      'spec4'                   => 'Text',
      'spec5'                   => 'Text',
      'spec6'                   => 'Text',
      'spec7'                   => 'Text',
      'spec8'                   => 'Text',
      'spec9'                   => 'Text',
      'is_hidden'               => 'Number',
      'autocalcsellprice'       => 'Number',
      'autocalcbuyprice'        => 'Number',
      'monitored'               => 'Number',
      'barcode'                 => 'Text',
      'is_service'              => 'Number',
      'is_tax'                  => 'Number',
      'competitor_price'        => 'Number',
      'notes'                   => 'Text',
      'quota'                   => 'Number',
      'is_updated'              => 'Number',
      'is_allow_zeroprice'      => 'Number',
      'is_fixed_price'          => 'Number',
      'conversion_id'           => 'ForeignKey',
    );
  }
}
