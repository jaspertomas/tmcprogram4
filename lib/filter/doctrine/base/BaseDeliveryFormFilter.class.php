<?php

/**
 * Delivery filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDeliveryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'type'            => new sfWidgetFormChoice(array('choices' => array('' => '', 'Incoming' => 'Incoming', 'Outgoing' => 'Outgoing', 'Conversion' => 'Conversion', 'Transfer' => 'Transfer'))),
      'code'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'ref_class'       => new sfWidgetFormChoice(array('choices' => array('' => '', 'invoice' => 'invoice', 'purchase' => 'purchase', 'delivery' => 'delivery'))),
      'ref_id'          => new sfWidgetFormFilterInput(),
      'client_class'    => new sfWidgetFormChoice(array('choices' => array('' => '', 'customer' => 'customer', 'vendor' => 'vendor', 'warehouse' => 'warehouse'))),
      'client_id'       => new sfWidgetFormFilterInput(),
      'warehouse_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'invno'           => new sfWidgetFormFilterInput(),
      'pono'            => new sfWidgetFormFilterInput(),
      'total'           => new sfWidgetFormFilterInput(),
      'status'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Complete' => 'Complete', 'Cancelled' => 'Cancelled'))),
      'payment_status'  => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Paid' => 'Paid', 'Cancelled' => 'Cancelled'))),
      'delivery_method' => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pickup' => 'Pickup', 'Delivery' => 'Delivery'))),
      'notes'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'type'            => new sfValidatorChoice(array('required' => false, 'choices' => array('Incoming' => 'Incoming', 'Outgoing' => 'Outgoing', 'Conversion' => 'Conversion', 'Transfer' => 'Transfer'))),
      'code'            => new sfValidatorPass(array('required' => false)),
      'date'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'ref_class'       => new sfValidatorChoice(array('required' => false, 'choices' => array('invoice' => 'invoice', 'purchase' => 'purchase', 'delivery' => 'delivery'))),
      'ref_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'client_class'    => new sfValidatorChoice(array('required' => false, 'choices' => array('customer' => 'customer', 'vendor' => 'vendor', 'warehouse' => 'warehouse'))),
      'client_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'warehouse_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse'), 'column' => 'id')),
      'invno'           => new sfValidatorPass(array('required' => false)),
      'pono'            => new sfValidatorPass(array('required' => false)),
      'total'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'status'          => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Complete' => 'Complete', 'Cancelled' => 'Cancelled'))),
      'payment_status'  => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Paid' => 'Paid', 'Cancelled' => 'Cancelled'))),
      'delivery_method' => new sfValidatorChoice(array('required' => false, 'choices' => array('Pickup' => 'Pickup', 'Delivery' => 'Delivery'))),
      'notes'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('delivery_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Delivery';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'type'            => 'Enum',
      'code'            => 'Text',
      'date'            => 'Date',
      'ref_class'       => 'Enum',
      'ref_id'          => 'Number',
      'client_class'    => 'Enum',
      'client_id'       => 'Number',
      'warehouse_id'    => 'ForeignKey',
      'invno'           => 'Text',
      'pono'            => 'Text',
      'total'           => 'Number',
      'status'          => 'Enum',
      'payment_status'  => 'Enum',
      'delivery_method' => 'Enum',
      'notes'           => 'Text',
    );
  }
}
