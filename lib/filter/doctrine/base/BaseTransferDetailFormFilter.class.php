<?php

/**
 * TransferDetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTransferDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'transfer_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Transfer'), 'add_empty' => true)),
      'description'     => new sfWidgetFormFilterInput(),
      'qty'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_transferred' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'product_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'is_cancelled'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'remaining'       => new sfWidgetFormFilterInput(),
      'color'           => new sfWidgetFormChoice(array('choices' => array('' => '', 'red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow', 'green' => 'green', 'blue' => 'blue', 'indigo' => 'indigo', 'violet' => 'violet'))),
      'slot'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_by_id'   => new sfWidgetFormFilterInput(),
      'updated_by_id'   => new sfWidgetFormFilterInput(),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'transfer_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Transfer'), 'column' => 'id')),
      'description'     => new sfValidatorPass(array('required' => false)),
      'qty'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty_transferred' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'product_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'is_cancelled'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'remaining'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'color'           => new sfValidatorChoice(array('required' => false, 'choices' => array('red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow', 'green' => 'green', 'blue' => 'blue', 'indigo' => 'indigo', 'violet' => 'violet'))),
      'slot'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_by_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('transfer_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransferDetail';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'transfer_id'     => 'ForeignKey',
      'description'     => 'Text',
      'qty'             => 'Number',
      'qty_transferred' => 'Number',
      'product_id'      => 'ForeignKey',
      'is_cancelled'    => 'Number',
      'remaining'       => 'Number',
      'color'           => 'Enum',
      'slot'            => 'Number',
      'created_by_id'   => 'Number',
      'updated_by_id'   => 'Number',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
