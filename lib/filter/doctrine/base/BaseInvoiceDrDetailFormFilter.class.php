<?php

/**
 * InvoiceDrDetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseInvoiceDrDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'datetime'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'invoice_dr_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('InvoiceDr'), 'add_empty' => true)),
      'invoice_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => true)),
      'invoicedetail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoicedetail'), 'add_empty' => true)),
      'product_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'warehouse_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'customer_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => true)),
      'qty'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_return'        => new sfWidgetFormFilterInput(),
      'is_released'      => new sfWidgetFormFilterInput(),
      'is_cancelled'     => new sfWidgetFormFilterInput(),
      'stock_entry_id'   => new sfWidgetFormFilterInput(),
      'created_by_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'updated_by_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'datetime'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'invoice_dr_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('InvoiceDr'), 'column' => 'id')),
      'invoice_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Invoice'), 'column' => 'id')),
      'invoicedetail_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Invoicedetail'), 'column' => 'id')),
      'product_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'warehouse_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse'), 'column' => 'id')),
      'customer_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Customer'), 'column' => 'id')),
      'qty'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_return'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_released'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_cancelled'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'stock_entry_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_by_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('invoice_dr_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvoiceDrDetail';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'datetime'         => 'Date',
      'invoice_dr_id'    => 'ForeignKey',
      'invoice_id'       => 'ForeignKey',
      'invoicedetail_id' => 'ForeignKey',
      'product_id'       => 'ForeignKey',
      'warehouse_id'     => 'ForeignKey',
      'customer_id'      => 'ForeignKey',
      'qty'              => 'Number',
      'is_return'        => 'Number',
      'is_released'      => 'Number',
      'is_cancelled'     => 'Number',
      'stock_entry_id'   => 'Number',
      'created_by_id'    => 'Number',
      'updated_by_id'    => 'Number',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
    );
  }
}
