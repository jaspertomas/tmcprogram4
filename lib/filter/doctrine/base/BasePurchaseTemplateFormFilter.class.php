<?php

/**
 * PurchaseTemplate filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePurchaseTemplateFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_po'     => new sfWidgetFormFilterInput(),
      'is_return' => new sfWidgetFormFilterInput(),
      'prefix'    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'      => new sfValidatorPass(array('required' => false)),
      'is_po'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_return' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'prefix'    => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('purchase_template_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PurchaseTemplate';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'name'      => 'Text',
      'is_po'     => 'Number',
      'is_return' => 'Number',
      'prefix'    => 'Text',
    );
  }
}
