<?php

/**
 * Transfer filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTransferFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'notes'               => new sfWidgetFormFilterInput(),
      'date'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'warehouse_vector_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('WarehouseVector'), 'add_empty' => true)),
      'warehouse_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'to_warehouse_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse_3'), 'add_empty' => true)),
      'date_completed'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'is_temporary'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_completed'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_cancelled'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'was_closed'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transfer_status'     => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Partial' => 'Partial', 'Complete' => 'Complete'))),
      'meta'                => new sfWidgetFormFilterInput(),
      'created_by_id'       => new sfWidgetFormFilterInput(),
      'updated_by_id'       => new sfWidgetFormFilterInput(),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'code'                => new sfValidatorPass(array('required' => false)),
      'notes'               => new sfValidatorPass(array('required' => false)),
      'date'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'warehouse_vector_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('WarehouseVector'), 'column' => 'id')),
      'warehouse_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse'), 'column' => 'id')),
      'to_warehouse_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse_3'), 'column' => 'id')),
      'date_completed'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'is_temporary'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_completed'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_cancelled'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'was_closed'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transfer_status'     => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Partial' => 'Partial', 'Complete' => 'Complete'))),
      'meta'                => new sfValidatorPass(array('required' => false)),
      'created_by_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('transfer_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Transfer';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'code'                => 'Text',
      'notes'               => 'Text',
      'date'                => 'Date',
      'warehouse_vector_id' => 'ForeignKey',
      'warehouse_id'        => 'ForeignKey',
      'to_warehouse_id'     => 'ForeignKey',
      'date_completed'      => 'Date',
      'is_temporary'        => 'Number',
      'is_completed'        => 'Number',
      'is_cancelled'        => 'Number',
      'was_closed'          => 'Number',
      'transfer_status'     => 'Enum',
      'meta'                => 'Text',
      'created_by_id'       => 'Number',
      'updated_by_id'       => 'Number',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
