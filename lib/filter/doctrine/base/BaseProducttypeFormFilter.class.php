<?php

/**
 * Producttype filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseProducttypeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                       => new sfWidgetFormFilterInput(),
      'description'                => new sfWidgetFormFilterInput(),
      'parent_id'                  => new sfWidgetFormFilterInput(),
      'producttype_schema_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ProducttypeSchema'), 'add_empty' => true)),
      'priority'                   => new sfWidgetFormFilterInput(),
      'product_name_format'        => new sfWidgetFormFilterInput(),
      'product_description_format' => new sfWidgetFormFilterInput(),
      'barcode_format'             => new sfWidgetFormFilterInput(),
      'barcode_format_2'           => new sfWidgetFormFilterInput(),
      'barcode_format_3'           => new sfWidgetFormFilterInput(),
      'max_buy_formula'            => new sfWidgetFormFilterInput(),
      'min_buy_formula'            => new sfWidgetFormFilterInput(),
      'max_sell_formula'           => new sfWidgetFormFilterInput(),
      'min_sell_formula'           => new sfWidgetFormFilterInput(),
      'base_formula'               => new sfWidgetFormFilterInput(),
      'speccount'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'spec1'                      => new sfWidgetFormFilterInput(),
      'spec2'                      => new sfWidgetFormFilterInput(),
      'spec3'                      => new sfWidgetFormFilterInput(),
      'spec4'                      => new sfWidgetFormFilterInput(),
      'spec5'                      => new sfWidgetFormFilterInput(),
      'spec6'                      => new sfWidgetFormFilterInput(),
      'spec7'                      => new sfWidgetFormFilterInput(),
      'spec8'                      => new sfWidgetFormFilterInput(),
      'spec9'                      => new sfWidgetFormFilterInput(),
      'path_ids'                   => new sfWidgetFormFilterInput(),
      'path'                       => new sfWidgetFormFilterInput(),
      'status'                     => new sfWidgetFormChoice(array('choices' => array('' => '', 'OK' => 'OK'))),
      'notes'                      => new sfWidgetFormFilterInput(),
      'is_updated'                 => new sfWidgetFormFilterInput(),
      'is_hidden'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'name'                       => new sfValidatorPass(array('required' => false)),
      'description'                => new sfValidatorPass(array('required' => false)),
      'parent_id'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'producttype_schema_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ProducttypeSchema'), 'column' => 'id')),
      'priority'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'product_name_format'        => new sfValidatorPass(array('required' => false)),
      'product_description_format' => new sfValidatorPass(array('required' => false)),
      'barcode_format'             => new sfValidatorPass(array('required' => false)),
      'barcode_format_2'           => new sfValidatorPass(array('required' => false)),
      'barcode_format_3'           => new sfValidatorPass(array('required' => false)),
      'max_buy_formula'            => new sfValidatorPass(array('required' => false)),
      'min_buy_formula'            => new sfValidatorPass(array('required' => false)),
      'max_sell_formula'           => new sfValidatorPass(array('required' => false)),
      'min_sell_formula'           => new sfValidatorPass(array('required' => false)),
      'base_formula'               => new sfValidatorPass(array('required' => false)),
      'speccount'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'spec1'                      => new sfValidatorPass(array('required' => false)),
      'spec2'                      => new sfValidatorPass(array('required' => false)),
      'spec3'                      => new sfValidatorPass(array('required' => false)),
      'spec4'                      => new sfValidatorPass(array('required' => false)),
      'spec5'                      => new sfValidatorPass(array('required' => false)),
      'spec6'                      => new sfValidatorPass(array('required' => false)),
      'spec7'                      => new sfValidatorPass(array('required' => false)),
      'spec8'                      => new sfValidatorPass(array('required' => false)),
      'spec9'                      => new sfValidatorPass(array('required' => false)),
      'path_ids'                   => new sfValidatorPass(array('required' => false)),
      'path'                       => new sfValidatorPass(array('required' => false)),
      'status'                     => new sfValidatorChoice(array('required' => false, 'choices' => array('OK' => 'OK'))),
      'notes'                      => new sfValidatorPass(array('required' => false)),
      'is_updated'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_hidden'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('producttype_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Producttype';
  }

  public function getFields()
  {
    return array(
      'id'                         => 'Number',
      'name'                       => 'Text',
      'description'                => 'Text',
      'parent_id'                  => 'Number',
      'producttype_schema_id'      => 'ForeignKey',
      'priority'                   => 'Number',
      'product_name_format'        => 'Text',
      'product_description_format' => 'Text',
      'barcode_format'             => 'Text',
      'barcode_format_2'           => 'Text',
      'barcode_format_3'           => 'Text',
      'max_buy_formula'            => 'Text',
      'min_buy_formula'            => 'Text',
      'max_sell_formula'           => 'Text',
      'min_sell_formula'           => 'Text',
      'base_formula'               => 'Text',
      'speccount'                  => 'Number',
      'spec1'                      => 'Text',
      'spec2'                      => 'Text',
      'spec3'                      => 'Text',
      'spec4'                      => 'Text',
      'spec5'                      => 'Text',
      'spec6'                      => 'Text',
      'spec7'                      => 'Text',
      'spec8'                      => 'Text',
      'spec9'                      => 'Text',
      'path_ids'                   => 'Text',
      'path'                       => 'Text',
      'status'                     => 'Enum',
      'notes'                      => 'Text',
      'is_updated'                 => 'Number',
      'is_hidden'                  => 'Number',
    );
  }
}
