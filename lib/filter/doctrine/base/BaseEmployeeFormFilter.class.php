<?php

/**
 * Employee filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEmployeeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'username'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'commission'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_technician' => new sfWidgetFormFilterInput(),
      'is_salesman'   => new sfWidgetFormFilterInput(),
      'address'       => new sfWidgetFormFilterInput(),
      'birthday'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'tin_no'        => new sfWidgetFormFilterInput(),
      'sss_no'        => new sfWidgetFormFilterInput(),
      'ph_no'         => new sfWidgetFormFilterInput(),
      'pi_no'         => new sfWidgetFormFilterInput(),
      'date_hired'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'position'      => new sfWidgetFormFilterInput(),
      'is_wholesale'  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'          => new sfValidatorPass(array('required' => false)),
      'username'      => new sfValidatorPass(array('required' => false)),
      'commission'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_technician' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_salesman'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'address'       => new sfValidatorPass(array('required' => false)),
      'birthday'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'tin_no'        => new sfValidatorPass(array('required' => false)),
      'sss_no'        => new sfValidatorPass(array('required' => false)),
      'ph_no'         => new sfValidatorPass(array('required' => false)),
      'pi_no'         => new sfValidatorPass(array('required' => false)),
      'date_hired'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'position'      => new sfValidatorPass(array('required' => false)),
      'is_wholesale'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('employee_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Employee';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'name'          => 'Text',
      'username'      => 'Text',
      'commission'    => 'Number',
      'is_technician' => 'Number',
      'is_salesman'   => 'Number',
      'address'       => 'Text',
      'birthday'      => 'Date',
      'tin_no'        => 'Text',
      'sss_no'        => 'Text',
      'ph_no'         => 'Text',
      'pi_no'         => 'Text',
      'date_hired'    => 'Date',
      'position'      => 'Text',
      'is_wholesale'  => 'Number',
    );
  }
}
