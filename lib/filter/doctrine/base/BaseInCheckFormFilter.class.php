<?php

/**
 * InCheck filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseInCheckFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'check_no'          => new sfWidgetFormFilterInput(),
      'amount'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'receive_date'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'check_date'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deposit_date'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'cleared_date'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'bounce_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'customer_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => true)),
      'billee_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Biller'), 'add_empty' => true)),
      'notes'             => new sfWidgetFormFilterInput(),
      'passbook_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Passbook'), 'add_empty' => true)),
      'passbook_entry_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassbookEntry'), 'add_empty' => true)),
      'remaining'         => new sfWidgetFormFilterInput(),
      'is_bank_transfer'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_cancelled'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'code'              => new sfValidatorPass(array('required' => false)),
      'check_no'          => new sfValidatorPass(array('required' => false)),
      'amount'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'receive_date'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'check_date'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'deposit_date'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'cleared_date'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'bounce_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'customer_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Customer'), 'column' => 'id')),
      'billee_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Biller'), 'column' => 'id')),
      'notes'             => new sfValidatorPass(array('required' => false)),
      'passbook_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Passbook'), 'column' => 'id')),
      'passbook_entry_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassbookEntry'), 'column' => 'id')),
      'remaining'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_bank_transfer'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_cancelled'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('in_check_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InCheck';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'code'              => 'Text',
      'check_no'          => 'Text',
      'amount'            => 'Number',
      'receive_date'      => 'Date',
      'check_date'        => 'Date',
      'deposit_date'      => 'Date',
      'cleared_date'      => 'Date',
      'bounce_date'       => 'Date',
      'customer_id'       => 'ForeignKey',
      'billee_id'         => 'ForeignKey',
      'notes'             => 'Text',
      'passbook_id'       => 'ForeignKey',
      'passbook_entry_id' => 'ForeignKey',
      'remaining'         => 'Number',
      'is_bank_transfer'  => 'Number',
      'is_cancelled'      => 'Number',
    );
  }
}
