<?php

/**
 * CounterReceiptDetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCounterReceiptDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'counter_receipt_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CounterReceipt'), 'add_empty' => true)),
      'pono'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'purchase_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'), 'add_empty' => true)),
      'vendor_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'), 'add_empty' => true)),
      'invoice_number'     => new sfWidgetFormFilterInput(),
      'amount'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'notes'              => new sfWidgetFormFilterInput(),
      'is_tally'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_inspected'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_paid'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'counter_receipt_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CounterReceipt'), 'column' => 'id')),
      'pono'               => new sfValidatorPass(array('required' => false)),
      'purchase_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Purchase'), 'column' => 'id')),
      'vendor_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Vendor'), 'column' => 'id')),
      'invoice_number'     => new sfValidatorPass(array('required' => false)),
      'amount'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'notes'              => new sfValidatorPass(array('required' => false)),
      'is_tally'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_inspected'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_paid'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('counter_receipt_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CounterReceiptDetail';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'counter_receipt_id' => 'ForeignKey',
      'pono'               => 'Text',
      'purchase_id'        => 'ForeignKey',
      'vendor_id'          => 'ForeignKey',
      'invoice_number'     => 'Text',
      'amount'             => 'Number',
      'notes'              => 'Text',
      'is_tally'           => 'Number',
      'is_inspected'       => 'Number',
      'is_paid'            => 'Number',
    );
  }
}
