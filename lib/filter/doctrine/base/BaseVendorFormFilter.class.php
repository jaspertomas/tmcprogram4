<?php

/**
 * Vendor filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseVendorFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'             => new sfWidgetFormFilterInput(),
      'discrate'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'addr1'            => new sfWidgetFormFilterInput(),
      'addr2'            => new sfWidgetFormFilterInput(),
      'addr3'            => new sfWidgetFormFilterInput(),
      'vtype'            => new sfWidgetFormFilterInput(),
      'cont1'            => new sfWidgetFormFilterInput(),
      'cont2'            => new sfWidgetFormFilterInput(),
      'phone1'           => new sfWidgetFormFilterInput(),
      'bank_acct'        => new sfWidgetFormFilterInput(),
      'email'            => new sfWidgetFormFilterInput(),
      'notes'            => new sfWidgetFormFilterInput(),
      'is_owned'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tin_no'           => new sfWidgetFormFilterInput(),
      'terms'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'collection_notes' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'             => new sfValidatorPass(array('required' => false)),
      'discrate'         => new sfValidatorPass(array('required' => false)),
      'addr1'            => new sfValidatorPass(array('required' => false)),
      'addr2'            => new sfValidatorPass(array('required' => false)),
      'addr3'            => new sfValidatorPass(array('required' => false)),
      'vtype'            => new sfValidatorPass(array('required' => false)),
      'cont1'            => new sfValidatorPass(array('required' => false)),
      'cont2'            => new sfValidatorPass(array('required' => false)),
      'phone1'           => new sfValidatorPass(array('required' => false)),
      'bank_acct'        => new sfValidatorPass(array('required' => false)),
      'email'            => new sfValidatorPass(array('required' => false)),
      'notes'            => new sfValidatorPass(array('required' => false)),
      'is_owned'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tin_no'           => new sfValidatorPass(array('required' => false)),
      'terms'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'collection_notes' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('vendor_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Vendor';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'name'             => 'Text',
      'discrate'         => 'Text',
      'addr1'            => 'Text',
      'addr2'            => 'Text',
      'addr3'            => 'Text',
      'vtype'            => 'Text',
      'cont1'            => 'Text',
      'cont2'            => 'Text',
      'phone1'           => 'Text',
      'bank_acct'        => 'Text',
      'email'            => 'Text',
      'notes'            => 'Text',
      'is_owned'         => 'Number',
      'tin_no'           => 'Text',
      'terms'            => 'Number',
      'collection_notes' => 'Text',
    );
  }
}
