<?php

/**
 * Replacement filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseReplacementFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'type'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Customer' => 'Customer', 'Vendor' => 'Vendor'))),
      'code'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'ref_class'    => new sfWidgetFormChoice(array('choices' => array('' => '', 'Invoice' => 'Invoice', 'Purchase' => 'Purchase'))),
      'ref_id'       => new sfWidgetFormFilterInput(),
      'client_class' => new sfWidgetFormChoice(array('choices' => array('' => '', 'Customer' => 'Customer', 'Vendor' => 'Vendor'))),
      'client_id'    => new sfWidgetFormFilterInput(),
      'returns_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Returns'), 'add_empty' => true)),
      'warehouse_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'invno'        => new sfWidgetFormFilterInput(),
      'pono'         => new sfWidgetFormFilterInput(),
      'total'        => new sfWidgetFormFilterInput(),
      'status'       => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Complete' => 'Complete', 'Cancelled' => 'Cancelled'))),
      'notes'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'type'         => new sfValidatorChoice(array('required' => false, 'choices' => array('Invoice' => 'Invoice', 'Purchase' => 'Purchase', 'Customer' => 'Customer', 'Vendor' => 'Vendor', '' => ''))),
      'code'         => new sfValidatorPass(array('required' => false)),
      'date'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'ref_class'    => new sfValidatorChoice(array('required' => false, 'choices' => array('Invoice' => 'Invoice', 'Purchase' => 'Purchase', '' => ''))),
      'ref_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'client_class' => new sfValidatorChoice(array('required' => false, 'choices' => array('Customer' => 'Customer', 'Vendor' => 'Vendor'))),
      'client_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'returns_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Returns'), 'column' => 'id')),
      'warehouse_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse'), 'column' => 'id')),
      'invno'        => new sfValidatorPass(array('required' => false)),
      'pono'         => new sfValidatorPass(array('required' => false)),
      'total'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'status'       => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Complete' => 'Complete', 'Cancelled' => 'Cancelled'))),
      'notes'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('replacement_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Replacement';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'type'         => 'Enum',
      'code'         => 'Text',
      'date'         => 'Date',
      'ref_class'    => 'Enum',
      'ref_id'       => 'Number',
      'client_class' => 'Enum',
      'client_id'    => 'Number',
      'returns_id'   => 'ForeignKey',
      'warehouse_id' => 'ForeignKey',
      'invno'        => 'Text',
      'pono'         => 'Text',
      'total'        => 'Number',
      'status'       => 'Enum',
      'notes'        => 'Text',
    );
  }
}
