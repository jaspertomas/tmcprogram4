<?php

/**
 * JobOrderDr filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseJobOrderDrFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'    => new sfWidgetFormFilterInput(),
      'datetime'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'job_order_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('JobOrder'), 'add_empty' => true)),
      'warehouse_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'qty_to_produce' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_to_consume' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'notes'          => new sfWidgetFormFilterInput(),
      'is_produced'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_cancelled'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_by_id'  => new sfWidgetFormFilterInput(),
      'updated_by_id'  => new sfWidgetFormFilterInput(),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'code'           => new sfValidatorPass(array('required' => false)),
      'description'    => new sfValidatorPass(array('required' => false)),
      'datetime'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'job_order_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('JobOrder'), 'column' => 'id')),
      'warehouse_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse'), 'column' => 'id')),
      'qty_to_produce' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'qty_to_consume' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'notes'          => new sfValidatorPass(array('required' => false)),
      'is_produced'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_cancelled'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_by_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('job_order_dr_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'JobOrderDr';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'code'           => 'Text',
      'description'    => 'Text',
      'datetime'       => 'Date',
      'job_order_id'   => 'ForeignKey',
      'warehouse_id'   => 'ForeignKey',
      'qty_to_produce' => 'Number',
      'qty_to_consume' => 'Number',
      'notes'          => 'Text',
      'is_produced'    => 'Number',
      'is_cancelled'   => 'Number',
      'created_by_id'  => 'Number',
      'updated_by_id'  => 'Number',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
