<?php

/**
 * Voucher filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseVoucherFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'no'                           => new sfWidgetFormFilterInput(),
      'date'                         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'time'                         => new sfWidgetFormFilterInput(),
      'date_effective'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'year'                         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'month'                        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'account_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAccount'), 'add_empty' => true)),
      'subaccount_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherSubaccount'), 'add_empty' => true)),
      'voucher_type_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherType'), 'add_empty' => true)),
      'voucher_allocation_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VoucherAllocation'), 'add_empty' => true)),
      'payee'                        => new sfWidgetFormFilterInput(),
      'amount'                       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'particulars'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'check_no'                     => new sfWidgetFormFilterInput(),
      'check_status'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Released' => 'Released', 'Cleared' => 'Cleared', 'On Hold' => 'On Hold', 'Cancelled' => 'Cancelled', 'N/A' => 'N/A'))),
      'notes'                        => new sfWidgetFormFilterInput(),
      'out_check_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OutCheck'), 'add_empty' => true)),
      'ref_type'                     => new sfWidgetFormFilterInput(),
      'ref_id'                       => new sfWidgetFormFilterInput(),
      'client_type'                  => new sfWidgetFormFilterInput(),
      'client_id'                    => new sfWidgetFormFilterInput(),
      'print_count'                  => new sfWidgetFormFilterInput(),
      'print_date'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'is_cancelled'                 => new sfWidgetFormFilterInput(),
      'is_help_needed'               => new sfWidgetFormFilterInput(),
      'is_pay_to_self'               => new sfWidgetFormFilterInput(),
      'invoice_cross_ref_system_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CrossRefSystem'), 'add_empty' => true)),
      'invoice_cross_ref_code'       => new sfWidgetFormFilterInput(),
      'invoice_cross_ref_id'         => new sfWidgetFormFilterInput(),
      'purchase_cross_ref_system_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CrossRefSystem_2'), 'add_empty' => true)),
      'purchase_cross_ref_type'      => new sfWidgetFormFilterInput(),
      'purchase_cross_ref_code'      => new sfWidgetFormFilterInput(),
      'purchase_cross_ref_id'        => new sfWidgetFormFilterInput(),
      'is_checked'                   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'no'                           => new sfValidatorPass(array('required' => false)),
      'date'                         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'time'                         => new sfValidatorPass(array('required' => false)),
      'date_effective'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'year'                         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'month'                        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'account_id'                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VoucherAccount'), 'column' => 'id')),
      'subaccount_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VoucherSubaccount'), 'column' => 'id')),
      'voucher_type_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VoucherType'), 'column' => 'id')),
      'voucher_allocation_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VoucherAllocation'), 'column' => 'id')),
      'payee'                        => new sfValidatorPass(array('required' => false)),
      'amount'                       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'particulars'                  => new sfValidatorPass(array('required' => false)),
      'check_no'                     => new sfValidatorPass(array('required' => false)),
      'check_status'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Released' => 'Released', 'Cleared' => 'Cleared', 'On Hold' => 'On Hold', 'Cancelled' => 'Cancelled', 'N/A' => 'N/A'))),
      'notes'                        => new sfValidatorPass(array('required' => false)),
      'out_check_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('OutCheck'), 'column' => 'id')),
      'ref_type'                     => new sfValidatorPass(array('required' => false)),
      'ref_id'                       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'client_type'                  => new sfValidatorPass(array('required' => false)),
      'client_id'                    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'print_count'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'print_date'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'is_cancelled'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_help_needed'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_pay_to_self'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'invoice_cross_ref_system_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CrossRefSystem'), 'column' => 'id')),
      'invoice_cross_ref_code'       => new sfValidatorPass(array('required' => false)),
      'invoice_cross_ref_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'purchase_cross_ref_system_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CrossRefSystem_2'), 'column' => 'id')),
      'purchase_cross_ref_type'      => new sfValidatorPass(array('required' => false)),
      'purchase_cross_ref_code'      => new sfValidatorPass(array('required' => false)),
      'purchase_cross_ref_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_checked'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('voucher_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Voucher';
  }

  public function getFields()
  {
    return array(
      'id'                           => 'Number',
      'no'                           => 'Text',
      'date'                         => 'Date',
      'time'                         => 'Text',
      'date_effective'               => 'Date',
      'year'                         => 'Number',
      'month'                        => 'Number',
      'account_id'                   => 'ForeignKey',
      'subaccount_id'                => 'ForeignKey',
      'voucher_type_id'              => 'ForeignKey',
      'voucher_allocation_id'        => 'ForeignKey',
      'payee'                        => 'Text',
      'amount'                       => 'Number',
      'particulars'                  => 'Text',
      'check_no'                     => 'Text',
      'check_status'                 => 'Enum',
      'notes'                        => 'Text',
      'out_check_id'                 => 'ForeignKey',
      'ref_type'                     => 'Text',
      'ref_id'                       => 'Number',
      'client_type'                  => 'Text',
      'client_id'                    => 'Number',
      'print_count'                  => 'Number',
      'print_date'                   => 'Date',
      'is_cancelled'                 => 'Number',
      'is_help_needed'               => 'Number',
      'is_pay_to_self'               => 'Number',
      'invoice_cross_ref_system_id'  => 'ForeignKey',
      'invoice_cross_ref_code'       => 'Text',
      'invoice_cross_ref_id'         => 'Number',
      'purchase_cross_ref_system_id' => 'ForeignKey',
      'purchase_cross_ref_type'      => 'Text',
      'purchase_cross_ref_code'      => 'Text',
      'purchase_cross_ref_id'        => 'Number',
      'is_checked'                   => 'Number',
    );
  }
}
