<?php

/**
 * CounterReceipt filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCounterReceiptFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'vendor_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Vendor'), 'add_empty' => true)),
      'code'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'amount'       => new sfWidgetFormFilterInput(),
      'balance'      => new sfWidgetFormFilterInput(),
      'status'       => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Paid' => 'Paid', 'Cancelled' => 'Cancelled', 'Overpaid' => 'Overpaid'))),
      'notes'        => new sfWidgetFormFilterInput(),
      'is_tally'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_inspected' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'vendor_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Vendor'), 'column' => 'id')),
      'code'         => new sfValidatorPass(array('required' => false)),
      'date'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'amount'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'balance'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'status'       => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Paid' => 'Paid', 'Cancelled' => 'Cancelled', 'Overpaid' => 'Overpaid'))),
      'notes'        => new sfValidatorPass(array('required' => false)),
      'is_tally'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_inspected' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('counter_receipt_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CounterReceipt';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'vendor_id'    => 'ForeignKey',
      'code'         => 'Text',
      'date'         => 'Date',
      'amount'       => 'Number',
      'balance'      => 'Number',
      'status'       => 'Enum',
      'notes'        => 'Text',
      'is_tally'     => 'Number',
      'is_inspected' => 'Number',
    );
  }
}
