<?php

/**
 * InvoiceDr filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseInvoiceDrFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'       => new sfWidgetFormFilterInput(),
      'datetime'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'invoice_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => true)),
      'invoice_return_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('InvoiceReturn'), 'add_empty' => true)),
      'customer_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => true)),
      'warehouse_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Warehouse'), 'add_empty' => true)),
      'qty_for_release'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_for_return'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'notes'             => new sfWidgetFormFilterInput(),
      'is_return'         => new sfWidgetFormFilterInput(),
      'is_released'       => new sfWidgetFormFilterInput(),
      'is_cancelled'      => new sfWidgetFormFilterInput(),
      'created_by_id'     => new sfWidgetFormFilterInput(),
      'updated_by_id'     => new sfWidgetFormFilterInput(),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'code'              => new sfValidatorPass(array('required' => false)),
      'description'       => new sfValidatorPass(array('required' => false)),
      'datetime'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'invoice_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Invoice'), 'column' => 'id')),
      'invoice_return_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('InvoiceReturn'), 'column' => 'id')),
      'customer_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Customer'), 'column' => 'id')),
      'warehouse_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Warehouse'), 'column' => 'id')),
      'qty_for_release'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'qty_for_return'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'notes'             => new sfValidatorPass(array('required' => false)),
      'is_return'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_released'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_cancelled'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_by_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('invoice_dr_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvoiceDr';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'code'              => 'Text',
      'description'       => 'Text',
      'datetime'          => 'Date',
      'invoice_id'        => 'ForeignKey',
      'invoice_return_id' => 'ForeignKey',
      'customer_id'       => 'ForeignKey',
      'warehouse_id'      => 'ForeignKey',
      'qty_for_release'   => 'Number',
      'qty_for_return'    => 'Number',
      'notes'             => 'Text',
      'is_return'         => 'Number',
      'is_released'       => 'Number',
      'is_cancelled'      => 'Number',
      'created_by_id'     => 'Number',
      'updated_by_id'     => 'Number',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
    );
  }
}
