<?php

/**
 * ActiveStorageBlobs filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseActiveStorageBlobsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'key'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'filename'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'content_type' => new sfWidgetFormFilterInput(),
      'metadata'     => new sfWidgetFormFilterInput(),
      'byte_size'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'checksum'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'key'          => new sfValidatorPass(array('required' => false)),
      'filename'     => new sfValidatorPass(array('required' => false)),
      'content_type' => new sfValidatorPass(array('required' => false)),
      'metadata'     => new sfValidatorPass(array('required' => false)),
      'byte_size'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'checksum'     => new sfValidatorPass(array('required' => false)),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('active_storage_blobs_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ActiveStorageBlobs';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'key'          => 'Text',
      'filename'     => 'Text',
      'content_type' => 'Text',
      'metadata'     => 'Text',
      'byte_size'    => 'Number',
      'checksum'     => 'Text',
      'created_at'   => 'Date',
    );
  }
}
