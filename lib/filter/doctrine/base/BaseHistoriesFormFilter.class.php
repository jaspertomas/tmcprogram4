<?php

/**
 * Histories filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseHistoriesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'historable_id'   => new sfWidgetFormFilterInput(),
      'historable_type' => new sfWidgetFormFilterInput(),
      'description'     => new sfWidgetFormFilterInput(),
      'ref_type1'       => new sfWidgetFormFilterInput(),
      'ref_id1'         => new sfWidgetFormFilterInput(),
      'ref_type2'       => new sfWidgetFormFilterInput(),
      'ref_id2'         => new sfWidgetFormFilterInput(),
      'client_type1'    => new sfWidgetFormFilterInput(),
      'client_id1'      => new sfWidgetFormFilterInput(),
      'client_type2'    => new sfWidgetFormFilterInput(),
      'client_id2'      => new sfWidgetFormFilterInput(),
      'meta'            => new sfWidgetFormFilterInput(),
      'created_by_id'   => new sfWidgetFormFilterInput(),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'historable_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'historable_type' => new sfValidatorPass(array('required' => false)),
      'description'     => new sfValidatorPass(array('required' => false)),
      'ref_type1'       => new sfValidatorPass(array('required' => false)),
      'ref_id1'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ref_type2'       => new sfValidatorPass(array('required' => false)),
      'ref_id2'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'client_type1'    => new sfValidatorPass(array('required' => false)),
      'client_id1'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'client_type2'    => new sfValidatorPass(array('required' => false)),
      'client_id2'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'meta'            => new sfValidatorPass(array('required' => false)),
      'created_by_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('histories_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Histories';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'historable_id'   => 'Number',
      'historable_type' => 'Text',
      'description'     => 'Text',
      'ref_type1'       => 'Text',
      'ref_id1'         => 'Number',
      'ref_type2'       => 'Text',
      'ref_id2'         => 'Number',
      'client_type1'    => 'Text',
      'client_id1'      => 'Number',
      'client_type2'    => 'Text',
      'client_id2'      => 'Number',
      'meta'            => 'Text',
      'created_by_id'   => 'Number',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
