<?php

/**
 * Purchasedetail filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePurchasedetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'purchase_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Purchase'), 'add_empty' => true)),
      'description'  => new sfWidgetFormFilterInput(),
      'qty'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_received' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sellprice'    => new sfWidgetFormFilterInput(),
      'total'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tax'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'product_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
      'barcode'      => new sfWidgetFormFilterInput(),
      'discrate'     => new sfWidgetFormFilterInput(),
      'discamt'      => new sfWidgetFormFilterInput(),
      'unittotal'    => new sfWidgetFormFilterInput(),
      'is_cancelled' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'remaining'    => new sfWidgetFormFilterInput(),
      'is_inspected' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'qty_returned' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'color'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow', 'green' => 'green', 'blue' => 'blue', 'indigo' => 'indigo', 'violet' => 'violet'))),
      'slot'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'purchase_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Purchase'), 'column' => 'id')),
      'description'  => new sfValidatorPass(array('required' => false)),
      'qty'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'qty_received' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'sellprice'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'tax'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'product_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
      'barcode'      => new sfValidatorPass(array('required' => false)),
      'discrate'     => new sfValidatorPass(array('required' => false)),
      'discamt'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'unittotal'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_cancelled' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'remaining'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_inspected' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'qty_returned' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'color'        => new sfValidatorChoice(array('required' => false, 'choices' => array('red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow', 'green' => 'green', 'blue' => 'blue', 'indigo' => 'indigo', 'violet' => 'violet'))),
      'slot'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('purchasedetail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Purchasedetail';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'purchase_id'  => 'ForeignKey',
      'description'  => 'Text',
      'qty'          => 'Number',
      'qty_received' => 'Number',
      'price'        => 'Number',
      'sellprice'    => 'Number',
      'total'        => 'Number',
      'tax'          => 'Number',
      'product_id'   => 'ForeignKey',
      'barcode'      => 'Text',
      'discrate'     => 'Text',
      'discamt'      => 'Number',
      'unittotal'    => 'Number',
      'is_cancelled' => 'Number',
      'remaining'    => 'Number',
      'is_inspected' => 'Number',
      'qty_returned' => 'Number',
      'color'        => 'Enum',
      'slot'         => 'Number',
    );
  }
}
