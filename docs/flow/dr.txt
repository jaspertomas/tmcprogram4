

temporary sold to solution
this will break if the first DR (a) is deleted
stock/soldTo

------------------------------
renovate getReceivedStatus()
depending on isDrBased
return true or false based on total of released/received or 
do it the old style
show received/released status on top of invoice and purchase

----------------------
for purchase
is_dr_based = false means "closed, no changes possible especially to stock entries"

if(is_dr_based = false)
no generate dr button
no change receive date
cannot receive
no qr in stock view
(or qr causes is_dr_based to become true)
allow delete
allow cancel

right now: adjust receive date is broken, because setDateAndUpdateStockEntry is removed
--------------------
counter receipt
on click generate voucher
amount comes from total
should come from remaining balance

-------------------------
stock view
put transaction date in col 2
put dr date in col 3, 
-------------------
do sold_to in stock view for invoice_dr
-------------------------------
cannot cancel if dr exist / items released
---------------------------
server timezone is wrong
-----------------------------
todo: 
po receive - if no date, error

-----------------------
do dr cancel?
do dr delete?
only admin can delete or cancel
normally, these are only for wrong input
other changes should create new dr
---------------------------

add is_dr_based to invoice
 - create dr on close only if dr based
 - show generate dr button only if dr based

-------------------
remove stockentry functionality from invoice

-----------------------
if invoice cancelled, cannot create dr [ok]
 - need way to easily create return dr
 - - create special link for cancelled drs with parameter return_all=1
 - - special calculation with invoice_qty=0 so remaining = -released
 - - allow adjust qty 
if invoice pending, cannot create dr [ok]
if invoice fully released, cannot create dr
-----------------------
invoice cannot delete stockentry if dr_details qty !=0 and dr_detail is released
----------------------
if invoice_dr_detail qty==0 do not create / delete stockentry
----------------------

todos
InvoiceDrDetail model
line 53

---------------------------
on invoice close 
    if no unreleased exists, stay in invoice page
    if unreleased exists
        if no dr, generate
            all qtys = unreleased
        if unreleased dr exist, update
        if no unreleased dr, generate
if returns exist, show as negative
------------------------------------------
[ok]
on release, generate stock entries
on undo release, delete stock entries
-------------------------------------------
allow cancel dr - cascade cancel detail, delete stock entry - requires datetime cancelled and reason

only admin can undo release
only admin can delete dr

-----------------------------------------------


on invoice close 
    if no remaining stock exists, stay in invoice page
        - disregard services and withholding tax
    if remaining stock exists
        if no dr, generate
            InvoiceDrTable::genDrForInvoice($invoice)
            InvoiceDrTable::getUnreleasedDrForInvoice($invoice)

        if unreleased dr exist, update
        if no unreleased dr, generate
if returns exist, show as negative

on release, generate stock entries
on undo release, delete stock entries

-----------------------------------------------
$invoice->checkDrRequired()

invoicedetail qty_released != qty dictates whether new dr is needed or not
qty_released initial value is 0
utility to check for qty_released != qty 
 - check_dr_required()
 - - dr is required if qty_released does not match qty
 - if true, generate dr

-------------------------------------------------
InvoiceDrTable::genDrForInvoice($invoice)

generate dr
dr detail qtys = "remaining", meaning qty - qty_released
if qty=5 and qty_released=2, create dr_detail with qty 3

$invoice_dr->is_released=false (default)
$invoice_dr_detail->is_released=false (default)
these will be set to true when DR is "Released" (Release button clicked)

remember to set created_by_id and updated_by_id
created_at and updated_at should set themselves

------------------------------------------------------
DR release

this happens when you click the big button on top that says "Release"

for each detail
set is_released=true
create stock entry
stockentry created_at should set itself
deduct qty from invoicedetail qty_released

set dr is_released=true
set updated_by_id
updated_at should set itself
Create a historical record
--------------------------------------------
DR Undo Release

this button appears if you are an admin and the DR is Released
basically reverse everything in DR Release
Create a historical record pls

user must provide reason - save this reason in history

----------------------------
Stock view and calculation
order stock by created_at (datetime) and id

====================
in stock view, if return, show in red [ok]
-----------------
cannot relase dr if all dr_details qty=0 [ok]
-------------------------------
