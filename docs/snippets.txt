    $this->forward404Unless($request->isMethod(sfRequest::POST));
    $this->forward404Unless($student = Doctrine::getTable('Student')->find(array($request->getParameter('id'))), sprintf('Student with id (%s) not found.', $request->getParameter('id')));
    $this->forward404Unless($this->purchase=Doctrine_Query::create()
        ->from('Purchase')
      	->where('i.id = '.$request->getParameter('id'))
      	->fetchOne(), sprintf('Object purchase does not exist (%s).', $request->getParameter('id')));
    ; 
    
//receive multiple return values
//get year and semester settings
    list($year, $sem) = SettingTable::fetchYearAndSem();

//doctrine query select
    return Doctrine_Query::create()
        ->from('StudentSubject ss')
      	->where('ss.student_id = '.$this->getId())
      	->andWhere('ss.status = "Passed"')
      	->execute();


//doctrine query delete
    Doctrine_Query::create()
        ->delete('StudentSection ss')
      	->where('ss.student_id='.$this->getId().' and ss.year != '.$year)
      	->orWhere('ss.student_id='.$this->getId().' and ss.semester != '.$sem)
        ->execute();

//doctrine query update
      $products= Doctrine_Query::create()
        ->update('Product p')
        ->set('p.producttype_id','71')
  ->where('p.name LIKE ?', array("%ERA PPR Elbow%"))
  ->execute();


//print notice or error
    $message="Undo Checkout successful";
    $this->getUser()->setFlash('notice', $message);
    $this->redirect("student/view?id=".$this->student->getId());

//show notices and errors
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_msg"><font color=green><?php echo $sf_user->getFlash('notice') ?></font></div>
<?php endif ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="flash_error"><font color=red><?php echo $sf_user->getFlash('error') ?></font></div>
<?php endif ?>

//javascript
alert("");

//javascript ajax
$(document).on("click", ".add_student_section_button", function(e){
    e.preventDefault();

    var subject_id=$(this).attr("subject_id");
    $.ajax({ // create an AJAX call...
        data: $('#add_student_section_form').serialize(), // get the form data
        type: $('#add_student_section_form').attr('method'), // GET or POST
        url: $('#add_student_section_form').attr('action'), // the file to call
        success: function(response) { // on success..
            $('#add_student_section_form').html(response); // update the DIV
            //hide all
       		  $(".add_timeslot_tr").attr("hidden",true);
       		  $(".add_timeslot_td").html("");
        }
    });
});

//delete link with confirmation
 <?php echo link_to('Delete','invoice/delete?id='.$invoice->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>

//fetch one where id = x
    $this->producttype=MyModel::fetchOne("Producttype",array('id'=>$request->getParameter("id")));


------multi date action-------------------------
    //startdate
    if($request->hasParameter("startdate"))
    {
      $startdate=$request->getParameter("startdate");
    }
    elseif($request->hasParameter("startdatesplit"))
    {
      $requestparams=$request->getParameter("startdatesplit");
      $day=str_pad($requestparams["day"], 2, "0", STR_PAD_LEFT);
      $month=str_pad($requestparams["month"], 2, "0", STR_PAD_LEFT);
      $year=$requestparams["year"];
      $startdate=$year."-".$month."-".$day;
    }
    else
    {
      $startdate=MyDate::today();
    }
    $this->startdate=$startdate;

    //enddate
    if($request->hasParameter("enddate"))
    {
      $enddate=$request->getParameter("enddate");
    }
    elseif($request->hasParameter("enddatesplit"))
    {
      $requestparams=$request->getParameter("enddatesplit");
      $day=str_pad($requestparams["day"], 2, "0", STR_PAD_LEFT);
      $month=str_pad($requestparams["month"], 2, "0", STR_PAD_LEFT);
      $year=$requestparams["year"];
      $enddate=$year."-".$month."-".$day;
    }
    else
    {
      $enddate=MyDate::today();
    }
    $this->enddate=$enddate;
 
  	if($this->startdate>$this->enddate)
  	{
  	  return $this->redirect("home/error?msg=Start date cannot be later than end date");
  	}


------multi date template-----------------

<?php use_helper('I18N', 'Date') ?>
<?php 
$today=MyDateTime::frommysql($startdate); 
$yesterday=MyDateTime::frommysql($startdate); 
$yesterday->adddays(-1);
$tomorrow=MyDateTime::frommysql($startdate); 
$tomorrow->adddays(1);
?>
<?php 
//show date form
echo form_tag("voucher/dashboardMulti");
$startDateForm = new sfWidgetFormDate();
$endDateForm = new sfWidgetFormDate();
echo "From ".$startDateForm->render('startdatesplit',$startdate);
echo " to ".$endDateForm->render('enddatesplit',$enddate);
?>
<input type=submit value="View">
</form>
<?php echo link_to("Back to Voucher Dashboard","voucher/dashboard?date=".$startdate);?>

<h1>Expense Report: <?php echo MyDateTime::frommysql($startdate)->toprettydate();?> to <?php echo MyDateTime::frommysql($enddate)->toprettydate();?></h1>

Petty Cash Expenses: <?php echo MyDecimal::format($pettycashtotal)?>
<br>Cheque Expenses: <?php echo MyDecimal::format($chequetotal)?>
<br>Other Expenses: <?php echo MyDecimal::format($othertotal)?>
<br>Total Expenses: <?php echo MyDecimal::format($total)?>
<br><?php //echo link_to("Print","voucher/dashboardPdf?date=".$startdate) ?>

<hr>
-----date addition----------------------
Doctrine_Query::create()
  ->update('Invoice i')
  ->set('i.duedate', 'DATE_ADD( date, INTERVAL 1 DAY )')
  ->execute();
  
-----doctrine select specific columns only and fetch as array--------------------------------
$q = Doctrine_Query::create()
    ->from('User u')
    ->leftJoin('u.Seeker s')
    ->leftJoin('u.Owner o');
$result = $q-> fetchArray();

$q = Doctrine_Query::create()
    ->select('u.f_name, u.l_name, u.email, u.password, u.username')
    ->from('User u')
    ->leftJoin('u.Seeker s')
    ->leftJoin('u.Owner o');
$result = $q-> fetchArray();

----form snippets---------------------------------------------
    //define dropdown box options via query
    $query=Doctrine::getTable('Account')
      ->createQuery('a')
      ->addWhere('a.account_category_id = ?', 13)
      ;  
    $this->widgetSchema['account_id']->addOption("query",$query);
    
----custom combo box from database--------------------------------------
$w=new sfWidgetFormDoctrineChoice(array(
  'model'     => 'Employee',
  'add_empty' => true,
));
echo $w->render('salesman_id',$selected_value);

----check boxes------------------------------------
Form:
  $this->widgetSchema['is_updated'] = new sfWidgetFormInputCheckbox(array(), array('value'=>1));
sql:
  ALTER TABLE `producttype` ADD `is_updated` TINYINT(1) NULL;
usage:
  true = 1, false = null
